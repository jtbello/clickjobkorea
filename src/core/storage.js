
angular.module('easi.core')
  .provider('$storage', $StorageProvider);

function $StorageProvider() {
  this.$get = $storageFactory;
}

function $storageFactory($window, $q, $doryVariable) {
  return {
    set: set,
    get: get,
    remove: remove,
    getTempData: getTempData,
    setTempData: setTempData,
    clearTempData: clearTempData
  };

  function set(key, value, nonflash) {
    if (_.isUndefined(value)) {
      value = null;
    } else if (_.isObject(value) || _.isArray(value)) {
      value = JSON.stringify(value);
    }
    if (nonflash) {
      $window.localStorage.setItem(key, value);
    } else {
      $doryVariable.setData(key, value);
    }
  }

  function get(key, nonflash) {
    return nonflash &&
      $q.when(parseValue($window.localStorage.getItem(key))) ||
        $doryVariable.getData(key) .then(parseValue);
    function parseValue(value) {
      if (!value && value === 'null') {
          value = null;
        } else {
          try { value = JSON.parse(value); } catch (e) {}
        }
        return value;
    }
  }

  function remove(key) {
      $doryVariable.removeData(key);
  }

  function getTempData() {
    var key = '##tempData##';
    var d = $q.defer();
    var rv;
    get(key).then(function(v) {
      rv = _.clone(v);
      clearTempData();
      d.resolve(rv);
    })
    .catch(function(e) {
      return $q.reject('no data');
    });
    return d.promise;
  }
  function setTempData(v) {
    var d = $q.defer();
    var key = '##tempData##';
    set(key, v);
    d.resolve();
    return d.promise;
  }
  function clearTempData() {
    var key = '##tempData##';
    remove('##tempData##');
  }
}
