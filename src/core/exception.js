
angular
  .module('easi.core')
  .config($exceptionHandlerConfig);

function $exceptionHandlerConfig($provide) {
  $provide.decorator('$exceptionHandler', $exceptionHandlerDecorator);
}

function $exceptionHandlerDecorator($delegate, $log) {
  var $exceptionHandler = $delegate;
  return function(exception, cause) {
    $log.error('%cError: ' + exception.message, 'color: red');
    return $exceptionHandler(exception, cause);
  };
}
