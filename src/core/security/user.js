
angular
  .module('easi.core')
  .provider('$user', $UserProvider);

var testUser = null;

function $UserProvider() {
  this.setTestUser = setTestUser;
  this.$get = userServiceFactory;
}

function setTestUser(value) {
  throw new Error('사용 중지된 함수입니다.');
}

function userServiceFactory($rootScope, $q, $storage, $window, $auth) {
  function UserService() {
    var curThis = this;
    this.cachedUserDeferred = null;
    this.currentUser = null;
    this.resetCurrentUser = function(){
      $storage.get('userDataInfo')
        .then(function(seData){
          curThis.currentUser = new User(seData);
        })
    };
    return this;
  }

  UserService.prototype.destroy = function() {
    this.cachedUserDeferred = null;
    this.currentUser = null;
    $storage.remove('userDataInfo');
  };

  UserService.prototype.get = function() {
    var self = this;
    var d = $q.defer();
    if (self.cachedUserDeferred) {
      return self.cachedUserDeferred.promise;
    } else if (self.currentUser) {
      d.resolve(self.currentUser);
      return d.promise;
    } else {
      self.cachedUserDeferred = d;


      var dd = $q.defer();
      var dp = dd.promise;
      var rv = $storage.get('userDataInfo');
      if(!rv) {
        dd.reject('로그인이 필요합니다.');
      } else if(rv.then) {
        dp = rv;
      } else {
        dd.resolve(rv);
      }

      dp
      .then(function(token) {
        if (!token) {
          //alert('세션정보가 유효하지 않습니다.');
          return $q.reject('세션정보가 유효하지 않습니다.');
        }
        var user = new User(token);
        self.currentUser = user;
        self.cachedUserDeferred = null;
        d.resolve(user);

      })
      .catch(function(e) {
        d.reject();
//        $window.location.href = 'public.html#/login';
      });
      return d.promise;
    }
  };

  function User(data) {
    var userDataInfo = _.clone(data);
    var self = this;

    _.assign(self, {
      getUserDataInfo : function() {
        return userDataInfo;
      },
      getAdminYn: function() {
        return data.adminYn;
      },
      getComAdminYn: function() {
        return data.comAdminYn;
      },
      getAgreeInfoList: function() {
        return data.agreeInfo;
      },
      getAuthInfoList: function() {
        return data.authInfo;
      },
      getLoginKind: function() {
        return data.loginKind;
      },
      getMenuInfoList: function() {
        return data.menuInfo;
      },
      getUserInfoList: function() {
        return data.userInfo;
      }
    });
  }

  var userService = new UserService();
  return userService;
}
