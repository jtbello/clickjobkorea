
angular
  .module('easi.core')
  .provider('$auth', $AuthProvider);

function $AuthProvider() {
  this.$get = authServiceFactory;
}

function authServiceFactory() {
  function AuthService() {
    return this;
  }

  AuthService.prototype.hasRole = function(user, roleOrSceneId) {
  	// 화면권한ID가 없거나 "ts"로 시작하지 않으면 권한이 있는 것으로 간주.
  	if (!roleOrSceneId || roleOrSceneId.length < 1 || roleOrSceneId.match(/^(?!ts).*/)) {
  		return true;
  	}

  	var rsList = user.getRoleServiceList();
  	if (!rsList || rsList.indexOf('NUM-0*') != -1) {
  		return true;
  	}
  	if (rsList.indexOf(roleOrSceneId) != -1) {
  		return true;
  	}
    return false;
  };

  var authService = new AuthService();
  return authService;
}
