angular
  .module('easi.common')
  .config($cordovaFileTransferConfig)
  .factory('$bdApp', $bdAppFactory)
  .factory('$advHttp', $advHttpFactory)
  .factory('$doryPicker', $doryPickerFactory)
  .factory('$doryVariable', $doryVariableFactory)
  .factory('$inAppPurchase', $inAppPurchaseFactory)
  .factory('$bdFilechoose', $bdFilechooseFactory)
  .factory('$bdStack', $bdStackFactory)
  .factory('$bdExtension', $bdExtensionFactory)
  .factory('$bdPhone', $bdPhoneFactory)
  .factory('$bdresource', $bdresourceFactory)
    .factory('$wakeupAlarm', $wakeupAlarmFactory)
    .factory('$cordovaPushV5C', $cordovaPushV5CFactory)
    .factory('$doryCall', $doryCallFactory)

function $cordovaFileTransferConfig($provide) {
  $provide.decorator('$cordovaFileTransfer', function($delegate, $window, $q, $log) {
    var decorator = {};
    var retryMsg = {
      download: '다운로드 실패하였습니다. 재시도 하겠습니까?',
      upload: '업로드 실패하였습니다. 재시도 하겠습니까?'
    };
    _.each(['download', 'upload'], function(fn) {
      decorator[fn] = function() {
        var args = arguments;
        return $delegate[fn].apply($delegate, args)
          .then(null, error, _.debounce(notify, 500));

        function error(rejection) {
          $log.warn(rejection);
          if ($window.confirm(retryMsg[fn])) {
            return decorator[fn].apply(null, args);
          } else {
            return $q.reject(rejection);
          }
        }

        function notify(progress) {
          if (progress.lengthComputable) {
            $log.debug(['[FileTransfer Progress] loaded:', progress.loaded, 'total:', progress.total, 'percentage:', (progress.loaded / progress.total) * 100 + '%'].join(' '), progress);
          } else {
            $log.debug('[FileTransfer Progress]', progress);
          }
        }
      };
    });
    return decorator;
  });
}

function $bdAppFactory($window) {
  return {
    exit: exit
  };
  function exit() {
    if ($window.Extension) {
      $window.Extension.exit(/*type*/null, success, fail);
    } else {
      logger.warn('Extension Not Found');
    }
    function success(data) {
      logger.debug('success ', data);
    }
    function fail(data) {
    }
  }
}

function $advHttpFactory($window, $http, $q, $log, $env) {
  /* global http */
  var logger = $log('$advHttp');
  return {
    advPost: advPost,
    advGet: advGet,
      advDownload : advDownload,
      advUpload : advUpload
  };
  
  function request(options) {
    var host = $env.endPoint.service;
    var startTime = Date.now();
    var timerName = '(Stopwatch) {' + _.guid().substr(0, 8) + '} ' + options.url;
    logger.log('request:', timerName);
    console.time(timerName);
    if (window.env.platform === 'browser') {
      var request = _.merge({
          headers: {
            'Content-Type': undefined
          },
          url: '',
          data: {}
        }, options);
        if (/\.json$/.test(request.url)) {
            request.method = 'GET';
        } else {
            request.url = host + request.url;
        }
      return $http(request)
        .then(function(response) {
          logger.log('response:', timerName, Date.now() - startTime);
          console.timeEnd(timerName);
          return response.data;
        })
        .catch(function(rejection) {
          logger.error('responseError:', timerName, Date.now() - startTime);
            console.timeEnd(timerName);
          return $q.reject(rejection);
        });
    }

    var d = $q.defer();
    var defaults = {
      url: '',
      data: {},
      options: {
        headers: {
            'Accept' : 'application/json, text/plain, */*', 'Content-Type' : 'application/json'
        }
      },
      method: ''
    };
    options = _.merge(defaults, _.pick(options, _.keys(defaults)));
    options.headers = options.options.headers;

    options.url = $env.endPoint.service + options.url;

    if(options.method === 'POST'){
      logger.debug('call advHttp.post', options);
      // cordovaHTTP.post(options.url, options.data, options.headers)
      //   .then(success)
      //   .catch(error);
      cordova.plugin.http.setDataSerializer('json');
      cordova.plugin.http.post(options.url, options.data, options.headers, success, error);
    }else if(options.method === 'GET'){
      logger.debug('call advHttp.get', options);
      // cordovaHTTP.get(options.url, options.data, options.headers)
      //   .then(success)
      //   .catch(error);
        cordova.plugin.http.setDataSerializer('json');
        cordova.plugin.http.get(options.url, options.data, options.headers, success, error);
    }else if(options.method === 'DOWNLOAD'){
        logger.debug('call advHttp.downloadFile', options);
        // cordovaHTTP.downloadFile(options.url, options.data, options.headers, options.filePath)
        //     .then(success)
        //     .catch(error);
        cordova.plugin.http.get(options.url, options.data, options.headers, success, error);
    }else if(options.method === 'UPLOAD'){
        logger.debug('call advHttp.uploadFile', options);
        // cordovaHTTP.uploadFile(options.url, options.data, options.headers, options.filePath, options.name)
        //     .then(success)
        //     .catch(error);
        cordova.plugin.http.get(options.url, options.data, options.headers, success, error);
    }else{

    }

    function success(obj) {
      logger.debug(obj);
      try {
        obj.data = JSON.parse(obj.data);
      } catch(e) {
        logger.warn(e);
      }
      logger.debug('advHttp success', obj);
      d.resolve(obj.data);
    }

    function error(obj) {
      try {
          obj.data = JSON.parse(obj.data);
      } catch(e) {
        logger.warn(e);
      }
      logger.error('advHttp error ', obj);
      d.reject(obj.data);
    }

    return d.promise
      .then(function(data) {
        logger.debug('response:', timerName, Date.now() - startTime);
        console.timeEnd(timerName);
        return data;
      })
      .catch(function(rejection) {
        logger.error('responseError:', timerName, Date.now() - startTime);
        console.timeEnd(timerName);
        return $q.reject(rejection);
      });
  }



  function advPost(options) {
    // 병렬
    options.method = 'POST'
    return request(options);
    
    // 순차
//    var d = $q.defer();
//    var id = _.uniqueId('e2e#');
//    e2eQueue.push(id);
//    e2eMap[id] = {
//      defer: d,
//      options: options
//    };
//    
//    (function next() {
//      if (!e2eQueue.length || busy) return;
//      var id = e2eQueue.shift();
//      var config = e2eMap[id];
//      busy = true;
//      request(config.options)
//        .then(config.defer.resolve, config.defer.reject)
//        .finally(function() {
//          delete e2eMap[id];
//          busy = false;
//          next();
//        });
//    })();
//    
//    return d.promise;
  }

  function advGet(options) {
    // 병렬
    options.method = 'GET'
    return request(options);
  }

    function advDownload(options) {
        // 병렬
        options.method = 'DOWNLOAD'
        return request(options);
    }

    function advUpload(options) {
        // 병렬
        options.method = 'UPLOAD'
        return request(options);
    }
}

function $doryPickerFactory($log, $window, $q) {
  var logger = $log('$doryPicker');
  return {
    datePicker: datePicker,
    timePicker: timePicker,
    datetimePicker: datetimePicker
  };
  
  function datePicker(date){
    return showDateTimePicker(date, 'date');
  }
  
  function timePicker(date){
    return showDateTimePicker(date, 'time');
  }
  
  function datetimePicker(date){
    return showDateTimePicker(date, 'datetime');
  }
  
  function showDateTimePicker(date, mode) {
    var d = $q.defer();
    if (cordova.plugins.DateTimePicker) {
      var options = {
          mode: mode,
          date: date,
          allowOldDates: true,
          allowFutureDates: true,
          locale: 'KO',
          okText: '선택',
          cancelText: '취소',
          android: {
            theme: 3, // If omitted/undefined, default theme will be used.
            calendar: false,
            is24HourView: false
          }
      };

      function onSuccess(date) {
        logger.debug('Selected date: ' + date);
        d.resolve(date);
      }

      function onError(error) { // Android only
        logger.error('Error: ' + error);
        d.reject(date);
      }
      cordova.plugins.DateTimePicker.show(options, onSuccess, onError);
    } else {
      logger.warn('Picker Not Found');
      d.reject('Picker Not Found');
    }
    return d.promise;
  }
}


function $doryVariableFactory($log, $q, $window) {
  var logger = $log('$doryVariable');
    var secureStorage = null;
    if(cordova.plugins && window.env.platform !== 'browser'){
        _init();
    }
  return {
    setData: setData,
    getData: getData,
    removeData: removeData,
      _init : _init
  };

  function _init(){
      secureStorage = new cordova.plugins.SecureStorage(
          function(){
              logger.warn('SecureStorage On');
          },
          function(error){
              logger.warn('SecureStorage Error : ' + error);
              navigator.notification.alert(
                  'Please enable the screen lock on your device. This app cannot operate securely without it.',
                  function () {
                      secureStorage.secureDevice(
                          function () {
                              _init();
                          },
                          function () {
                              _init();
                          }
                      );
                  },
                  'Screen lock is disabled'
              );
          },
          'clickjobkorea');
  }

  function setData(key, value) {
    logger.debug('setData', key, value);
    if (secureStorage) {
        secureStorage.set(
          function (key) {
              logger.warn('set : ' + key);
          },
          function (error) {
              logger.warn('set error : ' + error);
          },
          key, value);
    } else {
      $window.localStorage.setItem(key, value);
    }
  }

  function getData(key) {
    logger.debug('getData', key);
    var d = $q.defer();
    if (secureStorage) {
        secureStorage.get(
          function (value) {
              logger.warn('get : ' + value);
              d.resolve(value);
          },
          function (error) {
              logger.warn('get error : ' + error);
              d.resolve(null);
          },
          key);
    } else {
      d.resolve($window.localStorage.getItem(key));
    }
    return d.promise;
  }

  function removeData(key) {
    if (secureStorage) {
        secureStorage.remove(
          function (key) {
              logger.warn('remove : ' + key);
          },
          function (error) {
              logger.warn('remove error : ' + error);
          },
          key);
    } else {
      $window.localStorage.setItem(key, '');
      $window.localStorage.removeItem(key);
    }
  }
}

function $bdStackFactory($log, $window, $q) {
  var logger = $log('$bdStack');
  return {
    main: main,
    move: move,
    jump: jump,
    pre: pre,
    getData: getData,
    getParam: getParam
  };

  function main(pageUrl) {
    logger.debug('main', arguments);
    if ($window.Stack) {
      $window.Stack.main(pageUrl);
    } else {
      logger.warn('Stack Not Found');
    }
  }

  function move(pageUrl, params, data) {
    logger.debug('move', arguments);
    if ($window.Stack) {
      $window.Stack.move(pageUrl, params, data);
    } else {
      logger.warn('Stack Not Found');
      $window.location.href = pageUrl;
    }
  }

  function jump(pageUrl) {
    logger.debug('jump', arguments);
    if ($window.Stack) {
      $window.Stack.jump(pageUrl);
    } else {
      logger.warn('Stack Not Found');
      $window.location.href = pageUrl;
    }
  }

  function pre() {
    logger.debug('pre');
    if ($window.Stack) {
      $window.Stack.pre();
    } else {
      logger.warn('Stack Not Found');
      $window.history.back();
    }
  }

  function getData() {
    var d = $q.defer();
    if ($window.Stack) {
      $window.Stack.getData(success, fail);
    } else {
      logger.warn('Stack Not Found');
      d.reject('Stack Not Found');
    }
    function success(data) {
      d.resolve(data);
    }
    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function getParam() {
    var d = $q.defer();
    if ($window.Stack) {
      $window.Stack.getParam(success, fail);
    } else {
      logger.warn('Stack Not Found');
      d.reject('Stack Not Found');
    }
    function success(data) {
      d.resolve(data);
    }
    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }
}

function $bdFilechooseFactory($log, $q, $window, $env) {
  var logger = $log('$bdFilechoose');
  return {
    open: open,
    pick: pick
  };

  function open(dir) {
    var d = $q.defer();

    if ($window.FileChoose) {
      dir = $env.fileDir + dir;// 'Allotab/' + dir;
      $window.FileChoose.open(dir, success, fail);
    } else {
      logger.warn('FileChoose Not Found');
      d.reject('FileChoose Not Found');
    }

    function success(path) {
      logger.debug(path);
      d.resolve(path);
    }

    function fail(data) {
      d.reject(data);
    }

    return d.promise;
  }

  function pick() {
    var d = $q.defer();

    if ($window.FileChoose) {
      $window.FileChoose.pick(success, fail);
    } else {
      logger.warn('FileChoose Not Found');
      d.reject('FileChoose Not Found');
    }

    function success(fileInfo) {
      try {
        fileInfo = JSON.parse(fileInfo);
      } catch(e) {}

      logger.debug(fileInfo);

      fileInfo = _.assign({
        path: '',
        mimeType: 'application/octet-stream'
      }, fileInfo);

      var ext = fileInfo.path.match(/\.[09-az]+/i);
      if (!ext) {
        $window.alert('선택할 수 없는 파일입니다.');
        return d.reject();
      }

      d.resolve(fileInfo);

    }

    function fail(data) {
      d.reject(data);
    }

    return d.promise;
  }
}

function $bdExtensionFactory($log, $q, $window, $message, $env) {
  var logger = $log('$bdExtension');
  return {
    getTotalMemory: getTotalMemory,
    getFreeDiskSpace: getFreeDiskSpace,
    runKakao: runKakao,
    sms: sms,
    email: email,
    call: call,
    getFileList: getFileList,
    getImage: getImage,
    browser: browser,
    postBrowser: postBrowser
  };

  function getTotalMemory() {
    var d = $q.defer();
    if ($window.Extension) {
      $window.Extension.getTotalMemory(/*type*/null, success, fail);
    } else {
      logger.warn('Extension Not Found');
      d.reject('Extension Not Found');
    }

    function success(data) {
      logger.debug('success ', data);
      d.resolve(data);
    }

    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function getFreeDiskSpace() {
    var d = $q.defer();
    if ($window.cordova) {
      $window.cordova.exec(function(result) {
        d.resolve(result);
      }, function(error) {
        d.reject(error);
      }, 'File', 'getFreeDiskSpace', []);
    } else {
      logger.warn('cordova Not Found');
    }
    return d.promise;
  }

  function runKakao() {
    var d = $q.defer();
    if ($window.Extension) {
      $window.Extension.runKakao(success, fail);
    } else {
      logger.warn('Extension Not Found');
    }
    function success(data) {
      logger.debug('success ', data);
      d.resolve(data);
    }

    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function sms(addr, msg) {
    if ($window.Extension) {
      $window.Extension.sms(addr, msg);
    } else {
      logger.warn('Extension Not Found');
    }
  }

  function email(tos, title, message) {
    var d = $q.defer();
    if ($window.Extension) {
      $window.Extension.email(tos, title, message, success, fail);
    } else {
      logger.warn('Extension Not Found');
      d.reject('Extension Not Found');
    }
    function success(data) {
      d.resolve(data);
    }
    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function call(number) {
    logger.debug('call', number);
    if ($window.Extension) {
      $window.Extension.call(number);
    } else {
      logger.warn('Extension Not Found');
    }
  }

  function getFileList(folderName) {
    var d = $q.defer();
    if ($window.Extension) {
      var uri = $env.fileDir + (folderName || '');//'Allotab/' + (folderName || '');
      $window.Extension.getFileList(uri, success, fail);
    } else {
      logger.warn('Extension Not Found');
      d.reject('Extension Not Found');
    }

    function success(data) {
      logger.debug('success ', data);
      d.resolve(data);
    }

    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function getImage(url, headers) {
    var d = $q.defer();
    if ($window.Extension) {
      $window.Extension.getImage(url, headers, success, fail);
    } else {
      logger.warn('Extension Not Found');
      d.reject('Extension Not Found');
    }
    function success(data) {
      logger.debug('success ', data);
      d.resolve(data);
    }

    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function browser(url) {
    if ($window.Extension) {
      $window.Extension.browser(url);
    } else {
      $window.open(url);
    }
  }

  function postBrowser(script) {
    if ($window.Extension) {
      $window.Extension.postBrowser(script);
    } else {
      $message.alert("디바이스에서 실행해주세요.");
    }
  }
}

function $bdPhoneFactory($log, $window, $bdExtension) {
  var logger = $log(this);
  return {
    call: call
  };
  function call(number) {
    $bdExtension.call(number);
  }
}

function $inAppPurchaseFactory($log, $window, $q) {
  var logger = $log('$inAppPurchaseFactory');
  return {
      getProducts: getProducts,
      buy : buy,
      consume : consume,
      restorePurchases : restorePurchases,
      getReceipt : getReceipt
  };
  function getProducts(prdts) {
      var d = $q.defer();
      if(window.env.platform !== 'browser'){
          inAppPurchase.getProducts(prdts)
              .then(function (products) {
                  logger.log(products);
                  /*
                     [{ productId: 'com.yourapp.prod1', 'title': '...', description: '...', currency: '...', price: '...', priceAsDecimal: '...' }, ...]
                  */
                  d.resolve(products);
              })
              .catch(function (err) {
                  logger.log(err);
                  d.reject(err);
              });
      }else{
          d.reject('지원하지 않는 기능입니다.');
      }
      return d.promise;
  }

  function buy(prdtId){
      var d = $q.defer();
      if(window.env.platform !== 'browser'){
          inAppPurchase.buy(prdtId)
              .then(function (data) {
                  logger.log(data);
                  d.resolve(data);
              })
              .catch(function (err) {
                  logger.log(err);
                  d.reject(err);
              });
      }else{
          d.reject('지원하지 않는 기능입니다.');
      }
      return d.promise;
  }

  function consume(data){
      var d = $q.defer();
      if(window.env.platform !== 'browser'){
          inAppPurchase.consume(data.type, data.receipt, data.signature)
              .then(function (products) {
                  logger.log(products);
                  d.resolve(products);
              })
              .catch(function (err) {
                  logger.log(err);
                  d.reject(err);
              });
      }else{
          d.reject('지원하지 않는 기능입니다.');
      }
      return d.promise;
  }

  function getReceipt(){
      var d = $q.defer();
      if(window.env.platform !== 'browser'){
          inAppPurchase.getReceipt()
              .then(function (receipt) {
                  logger.log(receipt);
                  d.resolve(receipt);
              })
              .catch(function (err) {
                  logger.log(err);
                  d.reject(err);
              });
      }else{
          d.reject('지원하지 않는 기능입니다.');
      }
      return d.promise;
  }

    function nextConsume(idx, list) {
        if (idx > list.length -1) { return list; }
        return consume(list[idx]).then(function(result) {
            return nextConsume(idx+ 1, list);
        });
    }

  function restorePurchases(){
      var d = $q.defer();
      if(window.env.platform !== 'browser'){
          inAppPurchase.restorePurchases()
              .then(function (data) {
                  logger.log(data);
                  /*
                      [{
                        transactionId: ...
                        productId: ...
                        state: ...
                        date: ...
                      }]
                    */
                  if(data.length > 0){
                    return nextConsume(0, data);
                  }else{
                      d.resolve(data);
                  }
              })
              .then(function(data){
                  d.resolve(data);
              })
              .catch(function (err) {
                  logger.log(err);
                  d.reject(err);
              });
      }else{
          d.reject('지원하지 않는 기능입니다.');
      }
      return d.promise;
  }
}


function $bdresourceFactory($log, $window, $q) {
  
  return {
    isUpdateAPKFlag: isUpdateAPKFlag,
    isUpdateResouceFlag : isUpdateResouceFlag,
    checkApplication : checkApplication,
    checkUpdate : checkUpdate
  };
  
  function isUpdateAPKFlag() {
    
    var logger = $log(this);
    var d = $q.defer();
    
    if ($window.bdresource) {
      $window.bdresource.isUpdateAPKFlag(success, fail, '', '');
    } else {
      logger.warn('bdresource Not Found');
      d.reject('bdresource Not Found');
    }
    
    function success(data) {
      logger.debug('bdresource success ', data);
      d.resolve(data);
    }

    function fail(data) {
      logger.debug('bdresource fail ', data);
      d.reject(data);
    }
    return d.promise;
  }
  
  function isUpdateResouceFlag() {
    
    var logger = $log(this);
    var d = $q.defer();
    
    if ($window.bdresource) {
      $window.bdresource.isUpdateResouceFlag(success, fail, '', '');
    } else {
      logger.warn('bdresource Not Found');
      d.reject('bdresource Not Found');
    }
    
    function success(data) {
      logger.debug('bdresource success ', data);
      d.resolve(data);
    }

    function fail(data) {
      logger.debug('bdresource fail ', data);
      d.reject(data);
    }
    return d.promise;
  }
  
  function checkApplication() {
      
      var logger = $log(this);
      var d = $q.defer();
      
      if ($window.bdresource) {
        $window.bdresource.checkApplication(success, fail, '', '');
      } else {
        logger.warn('bdresource Not Found');
        d.reject('bdresource Not Found');
      }
      
      function success(data) {
        logger.debug('bdresource success ', data);
        d.resolve(data);
      }
  
      function fail(data) {
        logger.debug('bdresource fail ', data);
        d.reject(data);
      }
      return d.promise;
    }
  
  function checkUpdate() {
    
    var logger = $log(this);
    var d = $q.defer();
    
    if ($window.bdresource) {
      $window.bdresource.checkUpdate(success, fail, '', '');
    } else {
      logger.warn('bdresource Not Found');
      d.reject('bdresource Not Found');
    }
    
    function success(data) {
      logger.debug('bdresource success ', data);
      d.resolve(data);
    }
  
    function fail(data) {
      logger.debug('bdresource fail ', data);
      d.reject(data);
    }
    return d.promise;
  }
  
  
}

// function $batteryFactory($log, $window, $q) {
  
//   return {
//     batterystatus: batterystatus,
//     batterycritical : batterycritical,
//     batterylow : batterylow
//   };
  
//   function batterystatus() {
    
//     var logger = $log(this);
//     var d = $q.defer();
    
//     if ($window.navigator.battery) {
//       $window.navigator.battery.batterystatus(success, fail, '', '');
//     } else {
//       logger.warn('bdresource Not Found');
//       d.reject('bdresource Not Found');
//     }
    
//     function success(data) {
//       logger.debug('bdresource success ', data);
//       d.resolve(data);
//     }

//     function fail(data) {
//       logger.debug('bdresource fail ', data);
//       d.reject(data);
//     }
//     return d.promise;
//   }
// }

function $wakeupAlarmFactory($log, $window, $q) {

    return {
        wakeup: wakeup,
        updateWakeup: updateWakeup,
        getAlarm: getAlarm,
        removeAlarm: removeAlarm
    };

    function wakeup(param) {

        var logger = $log(this);
        var d = $q.defer();

        if (WakeupAlarm) {
            // var param = {
            //     alarms : [{
            //         userId   : 'jongtaei',
            //         type : 'onetime',
            //         time : { at : new Date() },
            //         extra : { message : 'json containing app-specific information to be posted when alarm triggers' },
            //         message : 'Alarm has expired!'
            //     }]
            // }
            WakeupAlarm.wakeup(success, fail, param);
        } else {
            logger.warn('$wakeupAlarm Not Found');
            d.reject('$wakeupAlarm Not Found');
        }

        function success(data) {
            logger.debug('wakeup success ', data);
            d.resolve(data);
        }

        function fail(data) {
            logger.debug('wakeup fail ', data);
            d.reject(data);
        }

        return d.promise;
    }

    function updateWakeup(param) {

        var logger = $log(this);
        var d = $q.defer();

        if (WakeupAlarm) {
            // var param = {
            //     alarms : [{
            //         userId   : 'jongtaei',
            //         alarmId  : 1234231
            //         type : 'onetime',
            //         time : { at : new Date() },
            //         extra : { message : 'json containing app-specific information to be posted when alarm triggers' },
            //         message : 'Alarm has expired!'
            //     }]
            // }
            WakeupAlarm.updateWakeup(success, fail, param);
        } else {
            logger.warn('$wakeupAlarm Not Found');
            d.reject('$wakeupAlarm Not Found');
        }

        function success(data) {
            logger.debug('updateWakeup success ', data);
            d.resolve(data);
        }

        function fail(data) {
            logger.debug('updateWakeup fail ', data);
            d.reject(data);
        }

        return d.promise;
    }

    function getAlarm(param) {

        var logger = $log(this);
        var d = $q.defer();

        if (WakeupAlarm) {
            // var id = { userId : 'jongtaei'}
            WakeupAlarm.getAlarm(success, fail, param);
        } else {
            logger.warn('$wakeupAlarm Not Found');
            d.reject('$wakeupAlarm Not Found');
        }

        function success(data) {
            logger.debug('getWakeup success ', data);
            d.resolve(data);
        }

        function fail(data) {
            logger.debug('getAlarm fail ', data);
            d.reject(data);
        }

        return d.promise;
    }

    function removeAlarm(param) {

        var logger = $log(this);
        var d = $q.defer();

        if (WakeupAlarm) {
            // var id = { userId : 'jongtaei', alarmId : '2312379'}
            WakeupAlarm.removeAlarm(success, fail, param);
        } else {
            logger.warn('$wakeupAlarm Not Found');
            d.reject('$wakeupAlarm Not Found');
        }

        function success(data) {
            logger.debug('removeAlarm success ', data);
            d.resolve(data);
        }

        function fail(data) {
            logger.debug('removeAlarm fail ', data);
            d.reject(data);
        }

        return d.promise;
    }
}

function $cordovaPushV5CFactory($q, $log, $rootScope, $timeout, $doryVariable) {

    if(window.env.platform !== 'browser') {

        $doryVariable.getData('UUID').then(function(data){
            $log.log('get UUID', data);
            $rootScope.UUID = data;
        });

        var push = PushNotification.init({
            android: {
                senderID: "1:1039231952564:android:ad6d8727f100fedf",
                clearNotifications: false
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'true',
                clearBadge: true,
                clearNotifications: false
            },
            windows: {}
        });

        if (push === undefined) {
            q.reject(new Error('init must be called before any other operation'));
        } else {
            push.on('registration', function (data) {
                $rootScope.UUID = data.registrationId;
                $log.log('tokenId', data.registrationId)

                $doryVariable.setData('UUID', data.registrationId);
                // SEND THE TOKEN TO THE SERVER, best associated with your device id and user
                $timeout(function () {
                    push.on('notification', function (notification) {
                        $rootScope.$emit('$cordovaPushV5C:notificationReceived', notification);
                    });
                });
                $timeout(function () {
                    push.on('error', function (error) {
                        $rootScope.$emit('$cordovaPushV5C:errorOccurred', error);
                    });
                });
            });
        }
    }

    return {
        initialize : function (options) {
            var q = $q.defer();
            push = PushNotification.init(options);
            q.resolve(push);
            return q.promise;
        },
        onNotification : function () {
            $timeout(function () {
                push.on('notification', function (notification) {
                    $rootScope.$emit('$cordovaPushV5C:notificationReceived', notification);
                });
            });
        },
        onError : function () {
            $timeout(function () {
                push.on('error', function (error) { $rootScope.$emit('$cordovaPushV5C:errorOccurred', error);});
            });
        },
        register : function () {
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                push.on('registration', function (data) {
                    q.resolve(data.registrationId);
                });
            }
            return q.promise;
        },
        unregister : function () {
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                push.unregister(function (success) {
                    q.resolve(success);
                },function (error) {
                    q.reject(error);
                });
            }
            return q.promise;
        },
        getBadgeNumber : function () {
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                push.getApplicationIconBadgeNumber(function (success) {
                    q.resolve(success);
                }, function (error) {
                    q.reject(error);
                });
            }
            return q.promise;
        },
        setBadgeNumber : function (number) {
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                push.setApplicationIconBadgeNumber(function (success) {
                    q.resolve(success);
                }, function (error) {
                    q.reject(error);
                }, number);
            }
            return q.promise;
        },
        finish: function (){
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                push.finish(function (success) {
                    q.resolve(success);
                }, function (error) {
                    q.reject(error);
                });
            }
            return q.promise;
        },
        hasPermission: function (){
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                PushNotification.hasPermission(
                    function (success) {
                        q.resolve(success);
                    }, function (error) {
                        q.reject(error);
                    });
            }
            return q.promise;
        },
        schedule: function (options){
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                PushNotification.local.schedule(options
                    , function (success) {
                        q.resolve(success);
                    }, function (error) {
                        q.reject(error);
                    });
            }
            return q.promise;
        },
        getList: function (){
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                PushNotification.local.getList(function (success) {
                        q.resolve(success);
                    }, function (error) {
                        q.reject(error);
                    });
            }
            return q.promise;
        },
        update: function (options){
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                PushNotification.local.update(options
                    , function (success) {
                        q.resolve(success);
                    }, function (error) {
                        q.reject(error);
                    });
            }
            return q.promise;
        },
        remove: function (options){
            var q = $q.defer();
            if (push === undefined) {
                q.reject(new Error('init must be called before any other operation'));
            } else {
                PushNotification.local.remove(options
                    , function (success) {
                        q.resolve(success);
                    }, function (error) {
                        q.reject(error);
                    });
            }
            return q.promise;
        }
    };
}

function $doryCallFactory($log, $window, $q) {

  return {
      callNumber: callNumber
  };

  function callNumber(number) {

      var logger = $log(this);
      var d = $q.defer();

      if (window.plugins.CallNumber) {
        window.plugins.CallNumber.callNumber(success, fail, number, true);
      } else {
          logger.warn('$doryCall Not Found');
          d.reject('$doryCall Not Found');
      }

      function success(data) {
          logger.debug('callNumber success ', data);
          d.resolve(data);
      }

      function fail(data) {
          logger.debug('callNumber fail ', data);
          d.reject(data);
      }

      return d.promise;
  }
}