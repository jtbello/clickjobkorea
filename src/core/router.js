
angular
  .module('easi.core')
  .run(debugStateEvents)
;

function debugStateEvents($rootScope, $state, $log, $window) {

  var states = $state.get();
  var routeMap = _.chain(states)
    .indexBy('name')
    .mapValues(function(state) {
      return state.url;
    })
    .value();
  $log.info(states);
  $log.info('ROUTE MAP', routeMap);

  $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams) {
      // $log.debug('debugStateEvents::' + event.name, {
      //   event: event,
      //   toState: toState,
      //   toParams: toParams,
      //   fromState: fromState,
      //   fromParams: fromParams,
      // });
    });

  $rootScope.$on('$stateNotFound',
    function(event, unfoundState, fromState, fromParams) {
      $log.warn('debugStateEvents::' + event.name, {
        event: event,
        unfoundState: unfoundState,
        fromState: fromState,
        fromParams: fromParams,
      });
    });

  $rootScope.$on('$stateChangeSuccess',
    function(event, toState, toParams, fromState, fromParams) {
       $log.debug('debugStateEvents::' + event.name, {
         event: event,
         toState: toState,
         toParams: toParams,
         fromState: fromState,
         fromParams: fromParams,
       });
    });

  $rootScope.$on('$stateChangeError',
    function(event, toState, toParams, fromState, fromParams, error) {
      $log.error('debugStateEvents::' + event.name, {
        event: event,
        toState: toState,
        toParams: toParams,
        fromState: fromState,
        fromParams: fromParams,
        error: error,
      });
      $log.error(error);
    });

  $rootScope.$on('$viewContentLoading',
    function(event, viewConfig) {
      // $log.debug('debugStateEvents::' + event.name, {
      //   event: event,
      //   viewConfig: viewConfig,
      // });
        $( 'html, body' ).animate( { scrollTop : 0 }, 0 );
    });

  $rootScope.$on('$viewContentLoaded',
    function(event) {
      // $log.debug('debugStateEvents::' + event.name, {
      //   event: event,
      // });
    });
}
