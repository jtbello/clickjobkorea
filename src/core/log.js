/* jshint proto:true */

angular
  .module('easi.core')
  .config($logConfig);

function $logConfig($provide, $logProvider) {
  $logProvider.debugEnabled(true);
  $provide.decorator('$log', $logDecorator);
}

function $logDecorator($delegate, $window) {
  var $log = $delegate;
  return _.assign(loggerFactory, $log);

  function loggerFactory(context) {
    var name = _.isString(context) ? context : context.__proto__.constructor.name;
    var logger = {
      $log: $log
    };
    _.chain($log).methods().each(function(method) {
      logger[method] = function() {
        var args = _.toArray(arguments);
        var message = args[0];
        var time = moment().format('HH:mm:ss.SSS');
        var level = _.padRight(method.toUpperCase(), 5, ' ');
        var template = '<%=time%> [<%=level%>](<%=name%>)';
        var prefix = _.template(template)({
          time: time,
          level: level,
          name: name
        });
        if (_.isString(message)) {
          args[0] = [prefix, message].join(' ');
        } else {
          args.unshift(prefix);
        }
        $log[method].apply($log, args);
      };
    }).value();
    return logger;
  }
}
