angular
  .module('easi.common.legacy')
  .factory('ngspublic', ngspublic);


function ngspublic($q, $log, $timeout, $message, $user, $cordovaFileTransfer, $modal, $io,
                   $env, MetaSvc) {

  var logger = $log(this);
  
  var fileExtensions = ["doc","docx","pdf","xls","xlsx","ppt","pptx","txt","wav","wma","csv","png","jpg","gif","jpeg","tif"];

  return{
    // setNextKey : setNextKey,
    // setNextNumKey : setNextNumKey,
    comCodeValue : comCodeValue,
    // insActHstrLog : insActHstrLog,
    // fileDown : fileDown,
    // getFileMngSvc : getFileMngSvc,
    getByteLength : getByteLength,
    substrByte : substrByte,
    // getLrckday : getLrckday,
    openRdViewer : openRdViewer,
    getAdvInfo : getAdvInfo,
    chkFileExtensions : chkFileExtensions,
    // chkVersion : chkVersion,
    //processStop : processStop,
      getVariable : getVariable,
      nullCheck : nullCheck,
      dateCheck : dateCheck,
      fileCheck : fileCheck
  }
  
  /* 업로드 체크 */
  function fileCheck( file )
  {
          // 사이즈체크
          var maxSize  = 50 * 1024 * 1024    //50MB
          var fileSize = 0;

          // 브라우저 확인
          var browser=navigator.appName;
          
          // 익스플로러일 경우
          if (browser=="Microsoft Internet Explorer")
          {
            var oas = new ActiveXObject("Scripting.FileSystemObject");
            fileSize = oas.getFile( file.value ).size;
          }
          // 익스플로러가 아닐경우
          else
          {
            fileSize = file.files[0].size;
          }

          if(fileSize > maxSize)
          {
              alert("첨부파일 사이즈는 50MB 이내로 등록 가능합니다.");
              return false;
          }
          return true;
  }
  
  // /**
  //  * 다음 프로세스 막음
  //  */
  // function processStop(stDayTime, endDayTime, msg, exitFlag, loginPass, loginId){
  //
  //   var d = $q.defer();
  //   var passIdList = ['68094252'];
  //   var selId = '';
  //
  //   if(!loginPass){
  //     selId = $user.currentUser.getUserId() || '';
  //   }else{
  //     selId = loginId;
  //   }
  //
  //
  //   var returnParam = {};
  //   var loginStopYn = '';
  //
  //   // 현재 시간 조회
  //   MetaSvc.getServerTime().then(function(t) {
  //     returnParam.t = t;
  //     return t;
  //   })
  //   .then(function(t){
  //
  //     returnParam.ingFlag = true;
  //
  //     // 파라미터 존재시 프로퍼티조회는 패스 전달 받은 파라미터로 처리
  //     if(!_.isUndefined(stDayTime) && !_.isUndefined(endDayTime) && !_.isUndefined(msg)){
  //       returnParam.stDayTime =  stDayTime;
  //       returnParam.endDayTime = endDayTime;
  //       returnParam.msg = msg;
  //       returnParam.exitFlag = exitFlag;
  //       return returnParam;
  //     }
  //     // 청약제어 프로퍼티 조회
  //     var propertyParams = {'propertyKeys' : ['plStopDtm','plStopMsg','plStopAppExitYn','loginStopYn','stopExcetEno']};
  //     return cmDBService.selectMsdpPropertys(propertyParams).then(function(propertyData){
  //       _.forEach(propertyData, function(obj){
  //         switch (obj.user.propertyKey) {
  //         case 'plStopDtm':
  //           if(obj.user.propertyValue.length != 29 || obj.user.propertyValue.indexOf('~') == -1){
  //             returnParam.ingFlag = false;
  //           }else{
  //             returnParam.stDayTime =  obj.user.propertyValue.split('~')[0];
  //             returnParam.endDayTime = obj.user.propertyValue.split('~')[1];
  //           }
  //           break;
  //         case 'plStopMsg':
  //           returnParam.msg = obj.user.propertyValue;
  //           break;
  //         case 'plStopAppExitYn':
  //           returnParam.exitFlag = obj.user.propertyValue;
  //           break;
  //         case 'loginStopYn':
  //           loginStopYn = obj.user.propertyValue;
  //           break;
  //         case 'stopExcetEno':
  //           if(!_.isUndefined(obj.user.propertyValue)){
  //             passIdList = obj.user.propertyValue.split(',');
  //           }
  //           break;
  //         default:
  //           break;
  //         }
  //       });
  //     })
  //
  //   })
  //   .then(function(result){
  //     // 프로퍼티 값이 없으면 패스
  //     if(!returnParam.ingFlag) {
  //       d.resolve(false);
  //       return d.promise;
  //     }
  //
  //     // 예외사번은 SKIP
  //     if (passIdList.indexOf(selId) > -1 ) {
  //       d.resolve(false);
  //       return d.promise;
  //     }
  //
  //     var toDay = moment(returnParam.t).format('YYYYMMDDHHmm');
  //     if( toDay >= returnParam.stDayTime && toDay < returnParam.endDayTime){
  //
  //       if(loginStopYn == "true"){
  //         // 로그인제어가 true일경우
  //         $message.alert(returnParam.msg);
  //         $bdApp.exit();
  //       }else{
  //         // 로그인제어가 아닐경우
  //         if(_.isUndefined(loginPass)){
  //           $message.alert(returnParam.msg);
  //           if(returnParam.exitFlag == "true" || returnParam.exitFlag == true){
  //             $bdApp.exit();
  //           }
  //         }
  //       }
  //
  //       d.resolve(true);
  //     }else{
  //       d.resolve(false);
  //     }
  //
  //   })
  //   .catch(function(){
  //     d.resolve(false);
  //   });
  //
  //   return d.promise;
  // }
  

  /**
   * 파일 확장자 검증
   */
  function chkFileExtensions(fileName){
    if( _.isEmpty(fileName) || fileName.lastIndexOf(".") < -1 ) return false;
    
    var fileEx = fileName.substr(fileName.lastIndexOf(".")+1);
    
    var ckCnt = _.size(_.chain(fileExtensions).filter(function(v){ return v.toLowerCase() == fileEx; }).value());
    
    if(ckCnt > 0) return true;
    else return false;
    
  }

  // /**
  //  * 다음데이터 조회를 위한 조회키 세팅(이전다음_페이지번호)
  //  */
  // function setNextNumKey(pageRowCnt,nextPageNum){
  //   return (((pageRowCnt) * (nextPageNum-1)) + 1)+"";
  // }
  //
  //  /**
  //  * 다음데이터 조회를 위한 조회키 세팅(헤더 조회)
  //  */
  // function setNextKey(header){
  //   return header.stndKeyList[0].stndKeyVal;
  // }


  /**
   * 공통코드에서 매칭된 값 구하기
   */
  function comCodeValue(COM_CODE, KEY){
    try{
      for(var i = 0; i < COM_CODE.length; i++){
        if(COM_CODE[i].k.trim() == KEY.trim()){
          return COM_CODE[i].v;
        }
      }
      return ''
    }catch(e){return ''}
  }

  // /**
  //  * 파일 다운로드
  //  */
  // function fileDown(param, hitoryParam, totCnt, curCnt) {
  //   var d = $q.defer();
  //   logger.debug("file Param : " + JSON.stringify(param));
  //   logger.debug("file hitoryParam : " + JSON.stringify(hitoryParam));
  //
  //   // 히스토리 저장
  //   if(!_.isEmpty(hitoryParam)){
  //     insActHstrLog(hitoryParam);
  //   }
  //   //네이티브 연동
  //   $cordovaFileTransfer.download(param)
  //     .then(function(data) {
  //       logger.debug(data);
  //       if(!totCnt){
  //         $bdFile.execute(data.nativeURL);
  //       }else{
  //         if(totCnt == curCnt){
  //           if($message.confirm("다운로드가 완료되었습니다.\n다운로드 목록을 확인하시겠습니까?")){
  //
  //             $bdFilechoose.open(param.downSubFPath)
  //             .then(function (path) {
  //               $bdFile.execute(path);
  //             });
  //
  //           }
  //         }
  //       }
  //
  //       // 다운로드 완료로 아이콘 변경
  //       $timeout(function() {
  //         param.downYn = true;
  //         param.nativePath = data.nativeURL;
  //         return d.resolve(param);
  //       }, 0);
  //
  //     })
  //     .catch(function(data) {
  //       $message.alert("파일 다운로드 에러!");
  //       // 다운로드 완료로 아이콘 변경
  //       param.downYn = false;
  //       logger.error(data);
  //     });
  //
  //   return d.promise;
  // }


  // /**
  //  * 파일 매니져
  //  */
  // function getFileMngSvc(a_sFnCd, a_sFileKey, a_oFileInfo, a_subPath, na_savePath){
  //   var fsno, fname, fsavename,fsize, fpath;
  //   var d = $q.defer();
  //
  //   if(a_sFnCd == spConstant.FILE_GUBUN_S || a_sFnCd == spConstant.FILE_GUBUN_D1){
  //     if(_.isEmpty(a_sFileKey)){
  //       $message.alert("[매개변수오류]\n조회(삭제) 대상 첨부파일키 매개변수가 누락되었습니다.");
  //       return d.reject("");
  //     }
  //   }else if(a_sFnCd=="I" && !a_oFileInfo){
  //     $message.alert("[매개변수오류]\n저장 할 첨부파일정보 매개변수가 누락되었습니다.");
  //     return d.reject("");
  //   }
  //
  //   // 파라미터 생성
  //   var param = {};
  //       param.apndFileDtlList = [];
  //       param.apndFileId = a_sFileKey;
  //   if(a_sFnCd == spConstant.FILE_GUBUN_I){
  //     param.apndFileId = '';
  //   }
  //   switch(a_sFnCd){
  //     case spConstant.FILE_GUBUN_S:
  //       param.apndFileId = a_sFileKey;        // 첨부파일아이디
  //       break;
  //     case spConstant.FILE_GUBUN_D1:
  //       param.apndFileId = a_sFileKey;        // 첨부파일아이디
  //       break;
  //     case spConstant.FILE_GUBUN_D2:
  //       //==============================================================
  //       // 첨부파일정보 리스트 데이터 세팅
  //       //==============================================================
  //       var oTmp = null;
  //       fsno = fname = fsavename = fsize = fpath = "";
  //
  //       for(var i=0; i<a_oFileInfo.length; i++){
  //         oTmp = a_oFileInfo[i];
  //         fsno = oTmp.fsno;                     // 첨부파일일련번호
  //         param.apndFileDtlList.push({"apndFileSno": fsno});
  //       }
  //       break;
  //     case spConstant.FILE_GUBUN_U:
  //     case spConstant.FILE_GUBUN_I:
  //       if(a_sFnCd == spConstant.FILE_GUBUN_I){
  //         param.apndFileId = "";              // 첨부파일아이디
  //       }else{
  //         param.apndFileId = a_sFileKey;      // 첨부파일아이디
  //       }
  //
  //       //==============================================================
  //       // 첨부파일정보 리스트 데이터 세팅
  //       //==============================================================
  //       var oTmp = null;
  //       fsno = fname = fsavename = fsize = fpath = "";
  //
  //       for(var i=0; i<a_oFileInfo.length; i++){
  //
  //         oTmp = a_oFileInfo[i];
  //         fsno = (typeof(oTmp.fsno)!="undefined")? oTmp.fsno : "";  // 첨부파일일련번호
  //         fname = oTmp.fname;                                       // 첨부파일명
  //         if(oTmp.fext != "") fname += "." + oTmp.fext;
  //         fsavename = oTmp.fsavename;                               // 첨부파일저장명
  //         fsize = oTmp.fsize;                                       // 첨부파일크기
  //         fpath = oTmp.fpath;                                       // 첨부파일경로
  //
  //         param.apndFileDtlList.push({
  //           "apndFileSno": fsno,
  //           "apndFileNm":fname,
  //           "apndFileStoreNm":fsavename,
  //           "apndFileSize":fsize,
  //           "apndFilePathNm":fpath
  //         });
  //
  //       }
  //       break;
  //   }
  //
  //   var sBizCode = '';
  //   if(!_.isNull(a_subPath))  sBizCode = a_subPath;
  //   param.trtrEno = $user.currentUser.getTrtrEno();  // 처리자사번
  //   param.untyBusnScCd = sBizCode;                         // 통합업무구분
  //
  //   // 서비스 호출
  //   var retVal = null;
  //   switch(a_sFnCd){
  //     case spConstant.FILE_GUBUN_S:
  //       CIApndFileMngUtilSvc.selListApndFileInfo(param)
  //         .then(function(data){
  //           retVal = data.serviceBody.data.BackEndBody[0];
  //
  //           if(retVal){
  //             var userData = retVal.user;
  //             var sFileKey = userData.apndFileId;     // 리턴매개변수[0] - 파일키
  //             var nListData = _.isEmpty(userData.apndFileDtlList)?[]:userData.apndFileDtlList;
  //             var nListCnt = nListData.length;
  //             var oTmpArray = new Array();
  //
  //             for(var i=0; i<nListCnt; i++){
  //
  //               var fsno = nListData[i].apndFileSno;
  //               var fpath = nListData[i].apndFilePathNm;
  //               var fname = nListData[i].apndFileNm;
  //               var fsavename = nListData[i].apndFileStoreNm;
  //               var fsize = nListData[i].apndFileSize;
  //
  //               oTmpArray[i] = new Object();
  //               oTmpArray[i].fsno = fsno;               // 첨부파일일련번호
  //               oTmpArray[i].fpath = fpath;             // 첨부파일경로
  //               oTmpArray[i].fname = fname;             // 첨부파일명
  //               oTmpArray[i].fsavename = fsavename;     // 첨부파일저장명
  //               oTmpArray[i].fsize = fsize;             // 첨부파일크기
  //               oTmpArray[i].fkey = userData.apndFileId;// 첨부파일키
  //               oTmpArray[i].allDownload = false;       // 전체 다운로드 여부
  //
  //             }
  //
  //             var fileResult = {};
  //             fileResult.a_sFnCd = a_sFnCd;
  //             fileResult.sFileKey = sFileKey;
  //             fileResult.oFileInfoList = oTmpArray;
  //
  //             // 파일 존재 여부 확인
  //             logger.debug("na_savePath : " + na_savePath)
  //             if(!_.isEmpty(na_savePath)){
  //               try{
  //                 // 파일 다운로드 여부
  //                 if(fileResult.oFileInfoList.length > 0){
  //                   var allFlag = true;
  //                   $bdExtension.getFileList(na_savePath)
  //                   .then(function(files) {
  //                     angular.forEach(fileResult.oFileInfoList, function(obj,idx){
  //                       var exists = _.filter(files, {name: obj.fname});
  //                       if (exists.length) {
  //                         if(allFlag){
  //                           allFlag = true;
  //                         }
  //                         obj.downYn = true;
  //                         obj.nativePath = exists[0].path;
  //                       }else{
  //                         allFlag =  false;
  //                         obj.downYn = false;
  //                       }
  //                     });
  //                     // 전체 다운로드 여부
  //                     if(allFlag){
  //                       angular.forEach(fileResult.oFileInfoList, function(obj,idx){
  //                         obj.allDownload = true;
  //                       });
  //                     }
  //                     return d.resolve(fileResult);
  //                   })
  //                   .catch(function(){
  //                     return d.resolve(fileResult);
  //                   })
  //                 }else{
  //                   return d.resolve(fileResult);
  //                 }
  //               }catch(e){return d.resolve(fileResult);}
  //             }else{
  //               return d.resolve(fileResult);
  //             }
  //           }
  //
  //         })
  //         .catch(function(data){
  //           logger.error(data);
  //         })
  //       break;
  //     case spConstant.FILE_GUBUN_D1:
  //       CIApndFileMngUtilSvc.delApndFileInfo(param)
  //         .then(function(data){
  //           retVal = data.serviceBody.data.BackEndBody[0];
  //           return d.resolve(fileMngResult(a_sFnCd, retVal));
  //         })
  //         .catch(function(data){
  //           logger.error(data);
  //         })
  //       break;
  //     case spConstant.FILE_GUBUN_D2:
  //       CIApndFileMngUtilSvc.delApndFileInfoCse(param)
  //       .then(function(data){
  //         retVal = data.serviceBody.data.BackEndBody[0];
  //         return d.resolve(fileMngResult(a_sFnCd, retVal));
  //       })
  //       .catch(function(data){
  //         logger.error(data);
  //       })
  //       break;
  //     case spConstant.FILE_GUBUN_U:
  //       CIApndFileMngUtilSvc.updApndFileInfo(param)
  //         .then(function(data){
  //           retVal = data.serviceBody.data.BackEndBody[0];
  //           return d.resolve(fileMngResult(a_sFnCd, retVal));
  //         })
  //         .catch(function(data){
  //           logger.error(data);
  //         })
  //       break;
  //     case spConstant.FILE_GUBUN_I:
  //       CIApndFileMngUtilSvc.insApndFileInfo(param)
  //         .then(function(data){
  //           retVal = data.serviceBody.data.BackEndBody[0];
  //           return d.resolve(fileMngResult(a_sFnCd, retVal));
  //         })
  //         .catch(function(data){
  //           logger.error(data);
  //         })
  //       break;
  //   }
  //
  //   return d.promise;
  //
  // }

  // /**
  //  * 파일 삭제
  //  */
  // function delServerFile(oDeleteFileArray){
  //
  //
  //   $cordovaFileTransfer.remove(oDeleteFileArray)
  //   .then(function(result){
  //       if(result!="FAIL"){
  //         try{
  //           var oArrReponseMsg = result.split("|");
  //           var sReturnCd = oArrReponseMsg[0];                                          // "0000":Success, 그 외:Failed
  //           var sReturnMsg = oArrReponseMsg[1];                                         // 정상/에러 메시지
  //           var oDeleteFailInfo = (sReturnCd!="0000")? eval(oArrReponseMsg[2].substr(0,oArrReponseMsg[2].length-1)) : null;
  //
  //           if(sReturnMsg == "DELETE OK!!!") sReturnMsg = "정상적으로 삭제처리 되었습니다.";      //메세지 변경요청 2010.1.12 jsh
  //           $message.alert(sReturnMsg);                                                 // 메시지출력
  //           return(oDeleteFailInfo);                                                    // 삭제완료된 파일정보를 리턴한다
  //         }catch(e){$message.alert(e.description);return(null);}
  //       }else{
  //         $message.alert("파일 삭제 시 오류가 발생하였습니다. 확인 후 재전송 하십시오.");
  //       }
  //     })
  //     .catch(function(data){
  //       $message.alert("파일 삭제 시 오류가 발생하였습니다. 확인 후 재전송 하십시오.");
  //     })
  //
  // }

  /**
   * 바이트 계산
   */
  function getByteLength(str){
    var cnt = 0;
    for(var i=0;i<str.length;i++){

      if(str.charCodeAt(i) > 127){
        cnt += 3;
      }else{
        cnt++;
      }

    }

    return cnt
  }

  /**
   * 바이트 만큼 자르기
   */
  function substrByte(str,start,len){
    var ch,sPos=0,nowLen=0,chLen,buf="";
    for(var i=0;i<str.length;i++){
      ch= str.charAt(i);
      chLen= getByteLength(ch);
      sPos+=chLen;
      if(!_.isNull(len) && nowLen+chLen>len)break;
      if(sPos<=start||isKorAndEng(ch)&&sPos-1==start)continue;
      buf+=ch;
      nowLen+=chLen
    }
    return buf
  }

  function isKorAndEng(s) {
    var re = /[가-힣]/gi;
    return re.test(s);
  }

  // /*
  //  * 음력년월일과 음력여부를 받아 해당 일자의 정보를 넘겨주는 함수
  //  * a_ilnrcYmYmd  : (String) 음력년월일자
  //  * a_ipmntYn  : (String) 윤달여부
  //  *
  // */
  // function getLrckday(a_ilnrcYmYmd, a_ipmntYn) {
  //   var userParam = {
  //     lnrcYmYmd : a_ilnrcYmYmd,
  //     lpmntYn : a_ipmntYn
  //   };
  //
  //   return $io.api('/cm/CISalesWkdaySrvcUtilSvc/selLrckday', userParam)
  //     .then(function(response) {
  //       var backEndBody = response.serviceBody.data.BackEndBody[0];
  //       if (backEndBody.naf) {
  //         return $q.reject(backEndBody.naf);
  //       }
  //       return backEndBody.user.cISalesWkdayVO;
  //     });
  // }

  /**
   * Open Report Designer Viewer
   *
   * rdOptions
   *   mrdPath : mrd 파일 위치
   *   mrdData : RD 데이터 (XML)
   *
   * return : 뷰어에 넘겨진 최종 RD 데이터
   */
  function openRdViewer(rdOptions){
    var reportingServerHost = $env.endPoint.report;
    var serverUrl = reportingServerHost + '/ReportingServer2/service';    

    /*
      네트워크에서 /service 호출 시 오류 분석.
        - java.lang.Exception과 함께 "문서를 실행할 수 없습니다."가 뜨면 mrdPath에서 mrd 파일을 가져오지 못한 것임.
          이와 관련하여 RD에서 loopback IP는 처리못함. (mrdPath에 localhost 또는 127.0.0.1 사용 불가)

        - NumberFormatException 응답의 한가지 경우는 UTF-8 without BOM 관련처리하면 됨. 레거시에서 xml 읽어올 때 주의됨. (AS-IS는 파일이 ASCII 혹은 UTF-8 with BOM으로 저장되어 있음)
     */
    var mrdPath;
    if (rdOptions.mrdPath && rdOptions.mrdPath.match(/^http.*/)) {
      mrdPath = rdOptions.mrdPath;
    } else {
      mrdPath = reportingServerHost + '/' + rdOptions.mrdPath;
    }

    var mrdData = rdOptions.mrdData.replace('&lt;![CDATA[', '', 'g').replace(']]&gt;', '', 'g'); // regex was so slow in javascript
    mrdData = "<?xml version='1.0' encoding='utf-8'?><root>" + mrdData + "</root>"; // wrapping xml required by RD

    /*
    mrdParam 관련 레거시 소스 내 설명
    rimgindexing : 보고서내의 여러페이지에 동일한 이미지 사용시 메모리 사용.
    rusedevcopies : 인쇄매수가 2매 이상일 경우, 프린터의 스풀에 1매에 대한 스풀만 생성되도록 함.
   */
    // var mrdParam = ' /rpdfusesvg /rexportchartratio [100] /riprnmargin /rusedevcopies '; // azlk-qu-ext/../ngspublic.js 에서 쓰던 mrdParam
    // var mrdParam = '/rusecxlib /rpdfusesvg /rexportchartratio [100] /riprnmargin /rusedevcopies '; // ngs-comm-ui/../ngspublic.js 에서 쓰던 mrdParam
    var mrdParam = ' /rmmloverlapobj /rignorecert /rxmlreportopt [2] /rusecxlib /rpdfusesvg /rexportchartratio [100] /riprnmargin /rusedevcopies /rv keeper[1] /rmmlfooteropt [1] ';        
  
    // RD overlay 방식
    //var viewer = new m2soft.crownix.Viewer(serverUrl);
    var viewer = new m2soft.crownix.Viewer(serverUrl, { viewerWidth:1.0, viewerHeight : 1.0, enableCloseButton:false });
    m2soft.crownix.Layout.setTheme('white'); // 색상 변경
    
    
    if($env.cordova){
      // 디바이스에서
      viewer.hideToolbarItem(['print_pdf']);
      viewer.hideToolbarItem(['inquery']);  
      viewer.hideToolbarItem(['cancel']);     // 취소
      viewer.hideToolbarItem(['save']);       // 저장
      viewer.showToolbarItem(['close']);      // 닫기
    }else{
      // 크롬에서
      viewer.hideToolbarItem(['inquery']);  
      viewer.hideToolbarItem(['cancel']);     // 취소
      viewer.showToolbarItem(['save']);   // 저장
      viewer.showToolbarItem(['close']);      // 닫기
    }
    
    /*
    if($env.target == 'production') {   
      // 운영(저장버튼 닫기)
      viewer.hideToolbarItem(['save']);  
    }
    else {
      // 개발/검증..기타..(PDF만 저장)
      viewer.showToolbarItem(['save']);   // 저장
      viewer.hideToolbarItem(['doc']);    // 워드
      viewer.hideToolbarItem(['hwp']);    // 한글
      viewer.hideToolbarItem(['xls']);    // 엑셀
      viewer.hideToolbarItem(['ppt']);    // 파워포인트
    }
    */
    
//    viewer.openFile('http://125.129.124.44/report/kmns5/allianz/mrd/0529/sfqu102rIdb568.mrd', '/rfn [http://125.129.124.44/report/kmns5/allianz/mrd/0529/sfqu102rIdb467XML.xml] /rxmlreportopt [2] /rmmlfooteropt [1] /rmmloverlapobj /rignorecert /rusecxlib /rpdfusesvg /rexportchartratio [100] /riprnmargin /rusedevcopies',{enableNote : true, includeNoteInExport : false, includeNoteInPrint : true, imageDrawOption:{useCanvas:false}});
//    viewer.openFile(mrdPath, '/rdata [' + mrdData + ']' + mrdParam, {imageDrawOption:{useCanvas:false}});
    viewer.openFile(mrdPath, '/rdata [' + mrdData + ']' + mrdParam, {imageDrawOption:{useCanvas:false}, useScrollTransition:false});
    
    // 서브리포트명 전달 콜백
    if( rdOptions.subReportCallback != undefined ){
      viewer.bind('report-finished', rdOptions.subReportCallback );
    }

    return mrdData;
  }

  /*
   * 설계사 전문 자격증 조회
   */
  function getAdvInfo() {
    var userParam = {
      'sMQulfdMngSrchVO' : {
        'pesnNo' : $user.currentUser.getUserEno()
      }
    };

    return $io.api('/cm/SMQulfdMngSvc/selListQulfdMng', userParam)
      .then(function(response) {
        var backEndBody = response.serviceBody.data.BackEndBody[0];
        if (backEndBody.naf) {
          return $q.reject(backEndBody.naf);
        }
        return backEndBody.user.sMQulfdMngRespVO;
      })
      .catch(function(response) {
        return $q.reject(response.data);
      });
  }

  // function chkVersion(){
  //   var d = $q.defer();
  //   var msg = '사용중인 알로탭은 최신버전이 아닙니다. 이 경우 심각한 문제가 발생될 수 있습니다.\n알로탭을 종료한 후 재실행하시기 바랍니다. 재실행하시면 자동으로 최신버전으로 업데이트됩니다.\n종료하시려면 확인을, 일단 업무를 정리하고 잠시 후 종료하시려면 취소를 누르세요.';
  //   $bdresource.isUpdateAPKFlag()
  //   .then(function(result){
  //     if(result){
  //       if($message.confirm(msg)){
  //         $bdApp.exit();
  //       }
  //     }
  //     return $bdresource.isUpdateResouceFlag();
  //   })
  //   .then(function(result){
  //     if(result){
  //       if($message.confirm(msg)){
  //         $bdApp.exit();
  //       }
  //       $bdresource.checkUpdate().then(function(dd){alert(dd)})
  //     }
  //   })
  //
  //   return d.promise;
  //
  // }

  function getVariable (obj){
      var data = {};
      _.each(obj, function(val, key){
          if(!angular.isFunction(val)){

              //data[key] = val;

              if(!isNaN(val)) {
                  data[key] = val;
              } else if(Array.isArray(val)) {
                  data[key] = [];
                  val.map(function(value){
                      data[key].push(getVariable(value));
                  })
              } else if(typeof val === 'object'){
                  //data[key] = getVariable(val);
                  var getData = getVariable(val);
                  if(!angular.isFunction(val)){
                      data[key] = getData;
                  }
              }else{
                  data[key] = val;
              }
          }
      });
      var isNotFunction = false;
      _.each(data, function(val, key){
          if(!angular.isFunction(val)){
              isNotFunction = true;
          }
      });
      if(!isNotFunction){
          data = function empty(){}
      }
      return data;
  }

    /**
     * null체크
     * @param val
     * @param msg
     * @param defaultVal
     * @returns {boolean}
     */
  function nullCheck(val, msg, defaultVal){
      if(_.isNumber(val)) return true;

      if(_.isEmpty(val) || _.isNull(val) || val === '' || val === defaultVal){
          $message.alert(msg);
          return false;
      }
      return true;
  }

    /**
     * 날짜 체크
     * @param startDateVal
     * @param endDateVal
     * @param msg
     * @returns {boolean}
     */
  function dateCheck(startDateVal, endDateVal, msg){
      if(_.isEmpty(startDateVal)){
          $message.alert(msg+'시작일자를 정확하게 입력해주세요.');
          return false;
      }
      if(_.isEmpty(endDateVal)){
          $message.alert(msg+'종료일자를 정확하게 입력해주세요.');
          return false;
      }
      if(moment(startDateVal) > moment(endDateVal)){
          $message.alert('시작일자보나 종료일자가 작습니다.');
          return false;
      }
      return true;
  }
}