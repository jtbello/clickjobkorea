angular.module('easi.common.legacy', []);


(function($){
  
  // http://willifirulais.blogspot.kr/2007/07/jquery-table-column-break.html
  $.fn.rowspan = function(colIdx, isStats) {       
      return this.each(function(){      
          var that;     
          $('tr', this).each(function(row) {      
              $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {
                   
                  if ($(this).html() == $(that).html()
                      && (!isStats 
                              || isStats && $(this).prev().html() == $(that).prev().html()
                              )
                      ) {            
                      var rowspan = $(that).attr("rowspan") || 1;
                      rowspan = Number(rowspan)+1;
   
                      $(that).attr("rowspan",rowspan);
                       
                      // do your action for the colspan cell here            
                      $(this).hide();
                       
                      //$(this).remove(); 
                      // do your action for the old cell here
                       
                  } else {            
                      that = this;         
                  }          
                   
                  // set the that if not already set
                  that = (that == null) ? this : that;      
              });     
          });    
      });  
  };    
  $.fn.colspan = function(rowIdx) {
      return this.each(function(){
          var that;
          $('tr', this).filter(":eq("+rowIdx+")").each(function(row) {
              $(this).find('td:not([nospan])').filter(':visible').each(function(col) {
                  if ($(this).html() == $(that).html()) {
                      var colspan = $(that).attr("colSpan") || 1;
                      colspan = Number(colspan)+1;
                       
                      $(that).attr("colSpan",colspan);
                      $(this).hide(); // .remove();
                  } else {
                      that = this;
                  }
                   
                  // set the that if not already set
                  that = (that == null) ? this : that;
                   
              });
          });
      });
  }  

    /*
    2018-04 유경수
    유효성 검증 관련 함수 
    */
    String.prototype.HAS_CHAR_IDENTIFIER = {
        SPECIAL             : 0,    // 특수문자
        NUMERIC             : 1,    // 숫자
        KOREAN              : 2,    // 한글
        ENGLISH_ALL         : 3,    // 영대소문자
        ENGLISH_SMALL_CASE  : 4,    // 영소문자
        ENGLISH_LARGE_CASE  : 5,    // 영대문자
        SPACE               : 6,    // 공백 
        NEW_LINE            : 7     // 개행문자
    }

    String.prototype.has = function () {
        if (!arguments || arguments.length === 0) { return false; }
        var _str = typeof (this) === "object" ? this.valueOf() : this;
        for (var i = 0 ; i < arguments.length; i++) {
            var _regex;
            switch (arguments[i]) {
                case String.prototype.HAS_CHAR_IDENTIFIER.SPECIAL:
                    _regex = /([!\"#$%&'()*+,\-.\/:;<=>?@\[\\\]^_`{|}~])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.NUMERIC:
                    _regex = /([0-9])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.KOREAN:
                    _regex = /([ㄱ-ㅎㅏ-ㅣ가-힣])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_ALL:
                    _regex = /([a-zA-Z])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_SMALL_CASE:
                    _regex = /([a-z])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_LARGE_CASE:
                    _regex = /([A-Z])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.SPACE:
                    _regex = /\s/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.NEW_LINE:
                    _regex = /\r\n|\n|\r/g;
                    break;
            }

            if (_regex.test(_str)) { continue; }
            else { return false; }
        }
        return true;
    }
    String.prototype.hasEng = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_ALL); }
    String.prototype.hasEngLarge = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_LARGE_CASE); }
    String.prototype.hasEngSmall = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_SMALL_CASE); }
    String.prototype.hasNum = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.NUMERIC); }
    String.prototype.hasKor = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.KOREAN); }
    String.prototype.hasSpe = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.SPECIAL); }
    String.prototype.hasSpace = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.SPACE); }
    String.prototype.hasNewLine = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.NEW_LINE); }
    String.prototype.hasSpaceAndNewLine = function(){ return this.valueOf().has( String.prototype.HAS_CHAR_IDENTIFIER.SPACE , String.prototype.HAS_CHAR_IDENTIFIER.NEW_LINE); }
    String.prototype.trim = String.prototype.trim || function () {
        return this.valueOf().replace(/(^\s*)|(\s*$)/gi, "");
    }
    String.prototype.largerThan = function(len, containEqual){
        if(containEqual) return this.valueOf().length >= len;
        else return this.valueOf().length > len;
    }
    String.prototype.smallerThan = function(len , containEqual){
        if(containEqual) return this.valueOf().length <= len;
        else return this.valueOf().length < len;
    }
    String.prototype.getBytes = function(containEmptyString){
        var bytes = 0;
        var charArr = this.valueOf().split('');
        charArr.forEach(function(char){
            if(char.hasEng() || char.hasNum())
            {
                bytes++;
            }
            else if(char.hasKor() || char.hasSpe()){
                bytes += 2;
            }
            else if(containEmptyString && (char.hasSpace() || char.hasNewLine())){
                bytes++;
            }

        });

        return bytes;
    }
    String.prototype.isEmpty = function(){
        if(this.valueOf() && this.valueOf().largerThan(0)){return false;}
        else return true;
    }
})(jQuery);
