angular
  .module('easi.common')
  .directive('fileread', function format($filter) {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    };
  })
    .directive('format', function format($filter) {
        return {
            require: '?ngModel',
            link: function(scope, elem, attrs, ctrl) {
                if (!ctrl) {
                    return;
                }

                ctrl.$formatters.unshift(function(a) {
                    return $filter(attrs.format)(ctrl.$modelValue);
                });

                ctrl.$parsers.unshift(function(viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    elem.val($filter(attrs.format)(plainNumber));
                    return plainNumber;
                });
            }
        };
    })
  .directive('numberFormat', function format($filter, $log) {
    return {
      restrict : 'A',
      require: '?ngModel',
      link: function(scope, elem, attrs, ctrl) {
        if (!ctrl) { return; }

        // 소숫점 몇 자리까지 받을지
        var fraction = parseInt(attrs.numberFormat || '5');
        if(_.isNaN(fraction) || fraction < 0) { fraction = 0; }
        else if(fraction > 5) { fraction = 5; }

        var fstr = (fraction > 0) ? '(\\.?\\d{0,'+fraction+'})?' : '';

        var dn = /^(-?)0+(\d+\.?)/; // 앞에 있는 무의미한 0 제거.

        var fn = new RegExp('^-?(?:\\d+' + fstr + ')?'); // - 도 일단은 받습니다. - 표현 (format number)
        var pn =new RegExp('^-?(?:\\d+' + fstr + ')'); // 숫자만 받습니다. - 값 (parser number) -- model 값

        function parser(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          try {
            var cm = (v || '').replace(/[, ]/g,'').replace(dn, "$1$2");
            var fv = fn.exec(cm); // format
            var vv = pn.exec(cm);

            if(!_.isEmpty(fv)) { fv = fv[0]; }
            if(!_.isEmpty(vv)) { vv = vv[0]; }

            var mv = parseFloat(vv);
            if(_.isNaN(mv)) { mv = ''; }
            else { mv = (mv).toString(); }

            $log.debug('parser', mv, v);

            ctrl.$setViewValue(mv);
            ctrl.$render();

            var sn = fv.split('.');

            var dv = parseInt(sn[0]);
            if(_.isNaN(dv)) { dv = fv; }
            else { dv = ((dv == 0 && _.startsWith(fv, '-')) && '-' || '') + _.numberFormat(dv) + ((sn.length == 2) && ('.' + sn[1]) || ''); }
            //else { dv = ((dv == 0 && _.startsWith(fv, '-')) && '-' || '') + new Intl.NumberFormat('ko-KR').format(dv) + ((sn.length == 2) && ('.' + sn[1]) || ''); }

            elem.val(dv);

            return mv;
          } catch(e) {
            debugger;
            $log.debug(e);
            return v;
          }
        }
        function formatter(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          try {

            $log.debug('formatter', v);

            var cm = (v).replace(/[, ]/g,'').replace(dn, "$1$2");
            var fv = fn.exec(cm); // format

            if(!_.isEmpty(fv)) { fv = fv[0]; }
            var sn = fv.split('.');

            var dv = parseInt(sn[0]);
            if(_.isNaN(dv)) { dv = fv; }
            else { dv = ((dv == 0 && _.startsWith(fv, '-')) && '-' || '') + _.numberFormat(dv) + ((sn.length == 2) && ('.' + sn[1]) || ''); }
            //else { dv = ((dv == 0 && _.startsWith(fv, '-')) && '-' || '') + new Intl.NumberFormat('ko-KR').format(dv) + ((sn.length == 2) && ('.' + sn[1]) || ''); }

            return dv;
          } catch(e) {
            debugger;
            $log.debug(e);
            return v;
          }
        }
        ctrl.$parsers.push(parser);
        ctrl.$formatters.push(formatter);
      }
    };
  })
  .directive('ssnFormat', function format($filter, $log) {
    // 비슷하게는 동작 하는데.....
    // model change 쪽 이벤이 이상하게 걸리는 함정이 있네...
    return {
      restrict : 'A',
      require: '?ngModel',
      link: function(scope, elem, attrs, ctrl) {
        if (!ctrl) { return; }

        // 소숫점 몇 자리까지 받을지
        var fraction = parseInt(attrs.ssnFormat || '13');
        if(_.isNaN(fraction) || fraction < 7) { fraction = 7; }
        else if(fraction > 13) { fraction = 13; }

        var fn = new RegExp('^\\d{0,'+fraction+'}'); // 표현

        function parser(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          try {
            var cm = (v || '').replace(/-/g,'');
            var fv = fn.exec(cm); // format

            if(!_.isEmpty(fv)) { fv = fv[0]; }

            $log.debug('parser', fv, v);

            ctrl.$setViewValue(fv);
            ctrl.$render();

            elem.val(fv.length > 6 ? fv.substr(0, 6) + '-' + fv.substr(6) : fv);

            return fv;
          } catch(e) {
            debugger;
            $log.debug(e);
            return v;
          }
        }
        function formatter(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          try {

            var cm = (v).replace(/-/g,'');
            var fv = fn.exec(cm); // format
            if(!_.isEmpty(fv)) { fv = fv[0]; }

            $log.debug('formatter', v, fv);

            return fv.length > 6 ? fv.substr(0, 6) + '-' + fv.substr(6) : fv;
          } catch(e) {
            debugger;
            $log.debug(e);
            return v;
          }
        }
        ctrl.$parsers.push(parser);
        ctrl.$formatters.push(formatter);
      }
    };
  })
  .directive('onlyNumberic', function() {
      return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ngModelCtrl) {
          function userAction(text) {
            var permitCharater = attrs.onlyNumberic || '';
            var regexp = new RegExp('[^0-9' + permitCharater + ']', 'g');

            if (text) {
              var transformedInput = text.replace(regexp, '');
              if ( transformedInput !== text ) {
                ngModelCtrl.$setViewValue(transformedInput);
                ngModelCtrl.$render();
              }

              return transformedInput;
            }

            return undefined;
          }
          ngModelCtrl.$parsers.push(userAction);
        }
      }
  })
  .directive('inputFilter', function($log) {
    return {
      restrict : 'A',
      require: '?ngModel',
      link: function(scope, elem, attrs, ctrl) {
        if (!ctrl) { return; }

        var type = attrs.inputFilter || '';
        if(_.isEmpty(type)) { return; }

        var regstr = '';
        if(type.indexOf('n') >= 0) { regstr += '\\d'; }
        if(type.indexOf('e') >= 0) { regstr += 'A-Za-z'; }

        // 2015-06-25 강형원 : 천지인 키패드에서 모음이 입력 안되는 문제가 있어 \\ᆢ\\ᆞ 조건 추가
        if(type.indexOf('h') >= 0) { regstr += '가-힣ㄱ-ㅎㅏ-ㅣ\\ᆢ\\ᆞ'; }
        if(type.indexOf('s') >= 0) { regstr += ' '; }
        if(type.indexOf('t') >= 0) { regstr += '`!@#$%\\^&\\*\\(\\)\\_\\+\\-\\=\\[\\]\\;\'\\,\\.\\/\\{\\}\\|\\:\\"\\<\\>\\?"\\~'; }

        if(_.isEmpty(regstr)) { return; }

        var reg = new RegExp('^[' + regstr + ']+');

        function parser(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          var mv = reg.exec(v);
          mv = mv && mv[0] || '';

          $log.debug('parser', mv, v, regstr);

          if(v != mv) {
            ctrl.$setViewValue(mv);
            ctrl.$render();
            elem.val(mv);

            return mv;
          }

          return v;
        }
        ctrl.$parsers.push(parser);
      }
    };
  })
  .provider('$filterSet', $filterSetProvider)
  .config($filterSetConfig)
;

function $filterSetConfig($filterProvider, $filterSetProvider) {
  // filterSetProvider 에서 정의된 Filter list 에 대해서, 필터로 등록. -- 아래 로직 노가다보다는..
  _.forIn($filterSetProvider.getFilterList(), function(v, k) {
    $filterProvider.register(k, function() { return v; });
  });

  //  $filterProvider.register('format', function($filterSet) { return $filterSet.format; });
  //  $filterProvider.register('objArrReduce', function($filterSet) { return $filterSet.objArrReduce; });
}

function $filterSetProvider() {
  // getInstance
  this.$get = this.getFilterList = function() { return _filterList; };
  var naw = { getToday : function() { return moment().format('YYYYMMDD'); } }

  // Filter 및 Util 함수명.
  var _filterList = {
    objArrReduce : objArrReduce, // [ { K1 : V1 }, { K2 : V2 }, ... ] -> [ K1 : V1, K2 : V2, ... ]
    format : format, // C# style string fommater
    nullChk : nullChk,
    shortDate : shortDate, // short Date Formatter
    shortDateTime : shortDateTime,
    shortTxtDate : shortTxtDate,
    toEmpty : toEmpty,
    toNumber : toNumber,
    toMoment : toMoment,
    ltrim : ltrim,
    rtrim : rtrim,
    trim : trim,
    removeBlank : removeBlank,
    isFutureDate : isFutureDate,
    isPastDate : isPastDate,
    maskN : maskN,
    fmtDate : fmtDate,
    fmtMonth : fmtMonth,
    fmtNumber : fmtNumber,
    fmtSn : fmtSn,
    fmtBn : fmtBn,
    fmtPhone : fmtPhone,
    fmtRRN: fmtRRN,
    splitPhone : splitPhone,
    mergePhone : mergePhone,
    isEmail : isEmail,
    getBirthFromSn : getBirthFromSn,
    getGenderFromSn : getGenderFromSn,
    isForeignerFromSn : isForeignerFromSn,
    isKorAndEng : isKorAndEng,
    hasText : hasText, // 이러한 형식으로 필터에 등록합니다.
    check_ResidentNO : check_ResidentNO,
    birthDateByJumin : getBirthDateByJumin,
    sexByJumin : getSexByJumin,
    sexByJuminCode : getSexByJuminCode,
    birthDataAge : getBirthDataAge,
    insuranceAgeByBirthDate : getInsuranceAgeByBirthDate,
    checkDate : checkDate,
    insuranceAgeByJuminno : getInsuranceAgeByJuminno,
    rrnoAgetFromBirth : getRrnoAgetFromBirth,
    rrnoAgetByJumin : getRrnoAgetByJumin,
    rrnoFromAge : getRrnoFromAge,
    rrnoFromBirth : getRrnoFromBirth,
    decimalPointCalc : function(a, b, c) { return decimalPointCalc(b, a, c); },
    cnvrInjrRskRatScCd : function(arr) { return cnvrInjrRskRatScCd(arr[0], arr[1]); },
  };

  // C# style string fomatter
  // Usage: {{ "From model: {0}; and a constant: {1};" | format:model.property:"constant":...:... }}
  // https://gist.github.com/litera/9634958
  function format(input) {
    var args = arguments;
    return input.replace(/\{(\d+)\}/g, function (match, capture) {
      return args[1*capture + 1];
    });
  };

    function nullChk(s) {
        if(_.isNumber(s)) return s;
        if(s == '' || s == undefined || s == null) return '';
        return s;
    }

  function shortDate(s, f) {
      if(s == '' || s == undefined) return '';
      // YKS 사파리 moment parse 오류 수정
      if(isNaN(s) && s.indexOf('.') >= 0){ s = s.replace(/\./g, '-'); }
      var m = moment(s);
      if(!m.isValid() && _.isString(s) && s.length == 8) { m = moment(s, 'YYYYMMDD'); }
      return m.isValid() && m.format(f || "YYYY-MM-DD") || '';
  }


    function shortDateTime(s) {
        if(s == '' || s == undefined) return '';
        // YKS 사파리 moment parse 오류 수정
        if(isNaN(s) && s.indexOf('.') >= 0){ s = s.replace(/\./g, '-'); }
        var m = moment(s);
        return m.isValid() && m.format("YYYY-MM-DD HH:mm:ss") || '';
    }

  function shortTxtDate(d, str) {
      if(str == undefined) str = '-';
      if(d == '' || d == undefined || d == null) return ''
      if(d.length != '6' && d.length != '8') return d;
      if(d.length == '6') return d.substr(0,4) + str + d.substr(4,2);
      if(d.length == '8') return d.substr(0,4) + str + d.substr(4,2) + str + d.substr(6,2);
      if(d.length == '8') return d.substr(0,4) + str + d.substr(4,2) + str + d.substr(6,2);
  }

  // [ { K1 : V1 }, { K2 : V2 }, ... ] -> [ K1 : V1, K2 : V2, ... ]
  // 나중에 input 이 array 인지 등을 확인할것..
  function objArrReduce (input) {
    return _.isArray(input) && input.reduce(function(a,b) { return _.assign(a, b); }) || input;
  };


  // boolean toEmpty(input) -- input 값이 null, undefined 인 경우 empty string '' 반환
  function toEmpty(s) { return (s === undefined || s === null) ? '' : s; }

  function toNumber(s) {
    var i = parseFloat((s || 0).toString().replace(/[^\d\.-]/g, ''));
    return i;
    //return new Intl.NumberFormat('ko-KR',{maximumFractionDigits: n || 0}).format(_.isNaN(i) && 0 || i);
  }

  function toMoment(s) {
    return moment(s);
  }

  // string ltrim(input) -- trim left
  function ltrim(s) {return _.trimLeft(s); }

  // string rtrim(input) -- trim right
  function rtrim(s) {return _.trimRight(s); }

  // string trim(input) -- trim
  function trim(s) {return _.trim(s); }

  // string removeBlank(input) -- remove all blanks in input string.
  function removeBlank(s) { return s.replace(/ */g, ''); }

  //boolean isFutureDate(date, [from = now]) -- date 가 현재시점의 미래인지 확인. (시분초 없이, 날짜만 확인)
  function isFutureDate(d, f) {
    // f - from 에 대해서 현재 _.now() 쓰고 있는것 나중에 시스템 타임으로 변경
    var m = moment(d);
    if(!m.isValid() && _.isString(d) && d.length == 8) { m = moment(d, 'YYYYMMDD'); }
    var b = moment(f || _.now());
    if(!b.isValid() && _.isString(f) && f.length == 8) { b = moment(f, 'YYYYMMDD'); }
    return m.isAfter(b);
  }

  //boolean isPastDate(date, [from = now]) -- date 가 현재시점의 과거인지 확인. (시분초 없이, 날짜만 확인)
  function isPastDate(d, f) {
    // f - from 에 대해서 현재 _.now() 쓰고 있는것 나중에 시스템 타임으로 변경
    var m = moment(d);
    if(!m.isValid() && _.isString(d) && d.length == 8) { m = moment(d, 'YYYYMMDD'); }
    var b = moment(f || _.now());
    if(!b.isValid() && _.isString(f) && f.length == 8) { b = moment(f, 'YYYYMMDD'); }
    return m.isBefore(b);
  }

  // string maskN(input, len) -- input 문자열 n 길이 이후에 '*' 처리. ex) maskN('abcde', 2) - 'ab***'
  function maskN(s, n, m) {
    var ss = (s || '').toString();
    var tn = ss.length;
    return _(n ? ss.substr(0, n || 1) : ss ? '*' : '').padRight(tn, m || '*');
  }

  // string fmtDate(input) -- input 문자열을 yyyy-MM-dd string 으로 변환.
  // 6 자리 YYMMDD 는 파싱 불가. (1900 년, 2000 년 기준이 나오면 추후 판단.
  function fmtDate(s) {
    var m = moment(s);
    if(!m.isValid() && _.isString(s) && s.length == 8) { m = moment(s, 'YYYYMMDD'); }
    return m.isValid() && m.format('YYYY-MM-DD') || '';
  }

  // string fmtMonth(input) -- input 문자열을 yyyy-MM-dd string 으로 변환.
  // 6 자리 는, YYYYMM 로 판단.
  function fmtMonth(s) {
    var m = moment(s);
    if(!m.isValid() && _.isString(s)) {
      if(s.length == 8) { m = moment(s, 'YYYYMMDD'); }
      else if(s.length == 6) { m = moment(s, 'YYYYMM'); }
    }
    return m.isValid() && m.format('YYYY-MM') || '';
  }


  // string fmtNumber(input) -- input 문자열을 #,000 포멧으로 변환
  function fmtNumber(s, n) {
    var i = parseFloat((s || 0).toString().replace(/[^\d\.-]/g, ''));
    return _.numberFormat(_.isNaN(i) && 0 || i, n || 0);
    //return new Intl.NumberFormat('ko-KR',{maximumFractionDigits: n || 0}).format(_.isNaN(i) && 0 || i);
  }

  // string fmtSn(input) -- input 값을 주민번호 포멧으로 반환. (validation 은 하지 않음 - 필요한 경우 협의)
  function fmtSn(s) {
    //var x = s && s.toString().replace(/[^\d]/g,'');
    var x = (s || '').replace(/[- ]/g, '');
    if (x) {
      return /*(x.length == 13 || x.length == 7) && */x.substr(0, 6) + '-' + x.substr(6, 7) || '';
    }
    return '';
  }

  // string fmtBn(input) -- input 값을 사업자번호 포멧으로 반환. (validation 은 하지 않음 - 필요한 경우 협의)
  function fmtBn(s) {
    //var x = s && s.toString().replace(/[^\d]/g,'');
    var x = (s || '').replace(/[- ]/g, '');
    return x.length == 10 && x.substr(0, 3) + '-' + x.substr(3, 2) + '-' + x.substr(5, 5) || '';
  }

  // string fmtPhone(input) -- input 값을 전화번호 포멧으로 반환.
  function fmtPhone(s) {
    if(_.isBlank(s)) return '';
    //var x = s && s.toString().replace(/[^\d]/g,'');
    var x = (s || '').replace(/[- ]/g, '');
    switch(x.length) {
    // 02xxxyyyy
    case 9 : return x.substr(0, 2) + '-' + x.substr(2,3) + '-' + x.substr(5);
    // 02xxxxyyyy || 016xxxyyyy
    case 10 : return x[1] == '2' ? x.substr(0, 2) + '-' + x.substr(2,4) + '-' + x.substr(6) : x.substr(0, 3) + '-' + x.substr(4,3) + '-' + x.substr(6);
    // 0zzxxxxyyyy
    case 11: return x.substr(0, 3) + '-' + x.substr(3,4) + '-' + x.substr(7);
    default: return s;
    }
  }

  function fmtRRN(s) {
    return `${s.substr(0, 6)}-${s.substr(6, 7)}`
  }

  // model 바인딩 이슈가 있어서, 그쪽 검증이후에 모델과 함께 사용 가능합니다.
  // array(3)  splitPhone(input) -- input 값을 전화번호 포멧으로 반환.
  function splitPhone(s) {
    var x = fmtPhone(s).split('-');
    return (x.length == 3) ? x : ['','',''];
  }

  // string mergePhone(el1, el2, el3) -- input 값을 전화번호 포멧으로 반환.
  function mergePhone(s) {
    if(!_.isArray(s) || s.length != 3) { return ''; }
    return (s[0] || '') +'-' + (s[1] || '') +'-' + (s[2] || '');
  }

  // boolean isEmail(input) - input 값이 이메일 형식인지를 판한
  function isEmail(s) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(_.trim(s));
  }

  // string getBirthFromSn(input, [month = false]) -- 주민번호 형식에서, 생년월일, 또는 월 일만 조회. (기본값은 년월일)
  function getBirthFromSn(s, m) {
    if(m) { return s.substr(2,4);}
    // 성별(년도) 코드는 위키 참조했음.
    // http://ko.wikipedia.org/wiki/%EC%A3%BC%EB%AF%BC%EB%93%B1%EB%A1%9D%EB%B2%88%ED%98%B8
    var x = s && s.toString().replace(/[^\d]/g,'');
    var n = parseInt(x.substr(6,1)); // 성별코드
    var y = parseInt(((n + 1) % 10) / 2);
    var d = (18 + (y && 2 - (y & 1))).toString() + x.substr(0, 6); // bit 연산 사용 지송.

    return d; // 리턴 타입에 date 필요하면 함수 확장할것.
  }

  // string getGenderFromSn(input) -- 주민번호에서 성별 추출 ('M' / 'F')
  function getGenderFromSn(s) {
    var x = s && s.toString().replace(/[^\d]/g,'');
    var n = parseInt(x.substr(6,1)); // 성별코드
    return (n % 2) && 'M' || 'F';
  }

  // boolean isForeignerFromSn(input) -- 주민번호에서 성별 추출 ('T' / 'F')
  function isForeignerFromSn(s) {
    var returnResult = false;
    var x = s && s.toString().replace(/[^\d]/g,'');
    var n = parseInt(x.substr(6,1)); // 성별코드

    if(n === 5 || n === 6 || n === 7 || n === 8){
      returnResult = true;
    }

    return returnResult; // 5,6,7,8,9 는 외국인.
  }

  //boolean isKorAndEng(input) - input 값이 한글과영문으로 이루어졌는지 확인
  function isKorAndEng(s) {
    var re = /[가-힣ㄱ-ㅎㅏ-ㅣ\s]+$|[a-zA-Z]+$/gi;
    return re.test(s);
  }

  /**
   *
   * 공백을 제외한 문자열을 가지고 있는지 확인합니다.
   *
   * @param str : 체크할 문자열
   * @return true : 문자열의 길이가 > 0 이면 , 그밖에 false
   */
  function hasText (str) {
    if (!str)
      return false;
    str = trim(str);
    if (str == "")
      return false;
    return true;
  };

    /**
     *  //주민등록번호 체크 로직
     *
     */
    function check_ResidentNO(num1,num2){

      var rn= num1 + '' + num2;

	  	rn = rn.split("-").join('');
	    if( rn.length !== 13 ) return false;
	 
	    var checkSum = 0;
	    for(var i=0; i<12; i++) checkSum += ((rn.substr(i,1)>>0)*((i%8)+2));
	 
	    var rrnMatch = (11-(checkSum%11))%10 == rn.substr(12,1);
	    var frnMatch = (13-(checkSum%11))%10 == rn.substr(12,1);
	 
	    return rrnMatch || frnMatch;
    }

  /**
   * 주민번호로 생년월일 구하기
   */
   function getBirthDateByJumin(jumin_no, delimiter) {
    var birthday = "";
    jumin_no = jumin_no.replace("-", "");
    var len = jumin_no.length;

    if ((typeof (delimiter) == "undefined"))
      delimiter = "";

    if (len == 13) {
      var iBirth = parseFloat(jumin_no.substring(0, 6));
      var iSex = parseInt(jumin_no.substring(6, 7), 10);

      if (!isNaN(iBirth) && !isNaN(iSex)) {
        var pre_year = "";
        if (iSex == 1 || iSex == 2 || iSex == 5 || iSex == 6) {
          pre_year = "19";
        } else if (iSex == 3 || iSex == 4 || iSex == 7 || iSex == 8) {
          pre_year = "20";
        } else {
          pre_year = "18";
        }

        birthday = pre_year + jumin_no.substring(0, 2) + delimiter + jumin_no.substring(2, 4) + delimiter + jumin_no.substring(4, 6);
      }

    }
    return birthday;
  };

  /**
   * 주민번호로 성별 구하기.(한글)
   *
   * @param 주민번호.
   *
   */
  function getSexByJumin(jumin_no) {
    if (!jumin_no)
      return "";

    var sex = "";
    var len = jumin_no.length;
    //jumin_no = jumin_no.replace(/-/g, "");
    jumin_no = jumin_no.replace("-", "");

    if (len >= 7) {
      var iSex = parseInt(jumin_no.substring(6, 7), 10);

      if (!isNaN(iSex)) {
        if (iSex == 1 || iSex == 3 || iSex == 5 || iSex == 7 || iSex == 9) {
          sex = "남자";
        } else if (iSex == 2 || iSex == 4 || iSex == 6 || iSex == 8 || iSex == 0) {
          sex = "여자";
        } else {
          sex = "";
        }
      }
    }
    return sex;
  };

  /**
   * 주민번호로 성별 구하기.(코드값)
   *
   * @param 주민번호.
   *
   */
  function getSexByJuminCode(jumin_no) {

    if (!jumin_no)
      return "";

    var sex = "";
    var len = jumin_no.length;
    //jumin_no = jumin_no.replace(/-/g, "");
    jumin_no = jumin_no.replace( "-", "");

    if (len >= 7) {
      var iSex = parseInt(jumin_no.substring(6, 7), 10);

      if (!isNaN(iSex)) {
        if (iSex == 1 || iSex == 3 || iSex == 5 || iSex == 7 || iSex == 9) {
          sex = "1";
        } else if (iSex == 2 || iSex == 4 || iSex == 6 || iSex == 8 || iSex == 0) {
          sex = "2";
        } else {
          sex = "";
        }
      }
    }
    return sex;
  };
  /**
   * 상령일(현재년도 + 생일(MMDD) + 6개월)기준나이
   */
  function getBirthDataAge(strBirthDate){

    var insurAge = 0;
    var standardDate = naw.getToday();
    insurAge = getInsuranceAgeByBirthDate(strBirthDate, standardDate);
    return insurAge;
  };

  //------------------------------------------------------------------------------
  // 생년월일 입력받아 보험나이를 반환한다.
  //
  // @param birthDate 생년월일 (형식:yyyyMMdd, 예:20080915)
  // @param standardDate 표준날자 (형식:yyyyMMdd, 예:20080915)
  // @return
  // 보험나이(상령나이)
  //------------------------------------------------------------------------------
  function getInsuranceAgeByBirthDate(birthDate, standardDate) {
    if (!hasText(birthDate) || !hasText(standardDate))
      return 0;

    var age = 0;
    var comp_year = 0; // 나이계산용
    var comp_mm = 0; // 월

    if (birthDate.length >= 8) {
      var currentYear = standardDate.substring(0, 4); // 기준년
      var currentMonth = standardDate.substring(4, 6); // 기준월
      var currentDay = standardDate.substring(6, 8); // 기준일

      var year = birthDate.substring(0, 4); // 주민번호-년
      var month = birthDate.substring(4, 6); // 주민번호-월
      var day = birthDate.substring(6); // 주민번호-일

      comp_year = currentYear; // 현재년을 일단 셋트합니다.
      comp_mm = currentMonth; // 현재년도 월

      // 현재월 - 생월
      // 0 이면 12개월 경과
      // > 0 이면 12 + 1월 경과
      // < 0 이면 12개월 미만

      //일자를 먼저 계산
      // 현재일 - 생일
      if ((currentDay - day) < 0) {
        comp_mm = currentMonth - 1; // 계산된 월
      }

      // 계산된 월 - 생월
      // 계산값이 0 보다 작으면
      if ((comp_mm - month) < 0) {
        comp_year = currentYear - 1;
        //2013.01.02 ycson : 보험나이 계산 오류 수정
//        comp_mm += 12; // 년을 빼고 그 뺀것을 월에다 더함
        comp_mm = parseInt(comp_mm, 10) + 12
      }

      // 나이 = 계산된 년 - 생년
      age = comp_year - year;
      // 계산된 월 - 생월 >= 6
      if ((comp_mm - month) >= 6) {
        age += 1;
      }

    }

    return age;
  };

  /**
   * 입력받은 날짜의 정합성을 체크
   * @param String yyyMMdd
   * @return boolean
   */
  function checkDate(strDate) {

      if(strDate == null || strDate.length != 8 || isNaN(strDate)) { return false; }

      var year  = parseInt(strDate.substr(0, 4));
      var month   = parseInt(strDate.substr(4, 2));
      var day   = parseInt(strDate.substr(6, 2));

      if(month < 1 || month > 12) {
        // 월이 정확히 입력되지 않았다
        return false;
      }

      var end = new Array(31,28,31,30,31,30,31,31,30,31,30,31); //각 월의 마직막일자

     //윤년처리
      if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
          end[1] = 29;
      }

      //true or false로 처리
      if(day < 1 || day > end[month-1]) { return false; }

      return true;
  };
  /**
   * 주민번호를 입력받아 보험나이를 반환한다.
   *
   * @param juminNo 주민번호
   * @param standardDate 표준날자 (형식:yyyyMMdd, 예:20080915)
   * @return 보험나이(상령나이)
   */
  function getInsuranceAgeByJuminno(juminNo, standardDate) {
    juminNo = juminNo.replace(/-/g, "");
    var birthDate = getBirthDateByJumin(juminNo);
    return getInsuranceAgeByBirthDate(birthDate, standardDate);
  };
  /**
   * 생년월일로 만나이를 계산
   */
  function getRrnoAgetFromBirth(birth, date) {
    var rrnoAge = 0;
    var today = naw.getToday();
    if(date) today = date;
    var toYear = eval(today.substring(0,4));
    var mm = today.substring(4,6);
    var dd = today.substring(6,8);
    var toMD = parseInt(today.substring(4,8), 10);

    var birthYear   = parseInt(birth.substr(0, 4), 10);
    var birthMD   = parseInt(birth.substr(4, 4), 10);

      if(toMD >= birthMD ){
        rrnoAge = toYear - birthYear;
       } else {
         rrnoAge = toYear - birthYear - 1;
       }
    return rrnoAge;
  };

  function getRrnoAgetByJumin(rn, date) {
    return getRrnoAgetFromBirth(getBirthDateByJumin(rn), date);
  };
  function getRrnoFromAge(age, gndrCd) {
    var today = naw.getToday();
    var yyyy = "" + (eval(today.substring(0,4)) - age);
    var mm = today.substring(4,6);
    var dd = today.substring(6,8);
    var strDate = yyyy + "" + mm + "" + dd;
    return getRrnoFromBirth(strDate, gndrCd);
  };
  function getRrnoFromAgeServerTime(age, gndrCd, date) {

    var today = date;
    var yyyy = "" + (eval(today.substring(0,4)) - age);
    var mm = today.substring(4,6);
    var dd = today.substring(6,8);

    var month   = Number(mm);
    var end = new Array(31,28,31,30,31,30,31,31,30,31,30,31); //각 월의 마직막일자
     //윤년처리
      if ((yyyy % 4 == 0 && yyyy % 100 != 0) || yyyy % 400 == 0) {
          end[1] = 29;
      }

    if(Number(dd) > end[month-1]){
      dd = end[month-1];
    }

    var strDate = yyyy + "" + mm + "" + dd;
    return getRrnoFromBirth(strDate, gndrCd);
  };
  function getRrnoFromBirth(birth, gndrCd) {
    var rrNoPost = 1;
    rrNoPost = gndrCd ? gndrCd : 1;
    rrNoPost = (Number(birth.substring(0, 4)) < 2000 ? rrNoPost : eval(rrNoPost) + 2);
    var rrNoPre = birth.substring(2, 8);
    return rrNoPre + "" + rrNoPost + "111111";
  };
  /**
   * param : strMode  - 수식
   * param : nCalcVal - 처리할 값(소수점 이하 데이터 포함)
   * param : nDigit   - 연산 기준 자릿수  => -2:십단위, -1:원단위  , 0:소수점 1자리, 1:소수점 2자리, 2:소수점 3자리, 3:소수점 4자리, 4:소수점 5자리 처리
   */
  function decimalPointCalc(strMode, nCalcVal, nDigit){

    if(!_.isNumber(nDigit)) { nDigit = 0; }
    if(strMode == "ROUND") {        //반올림
        if(nDigit < 0) {
            nDigit = -(nDigit);
            nCalcVal = (nCalcVal / Math.pow(10, nDigit)).toFixed(0) * Math.pow(10, nDigit);
        } else {
            nCalcVal = nCalcVal.toFixed(nDigit)
        }
    } else if(strMode == "CEIL") {  //절상
        if(nDigit < 0) {
            nDigit = -(nDigit);
            nCalcVal = Math.ceil(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
        } else {
            nCalcVal = Math.ceil(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
        }
    } else if(strMode == "FLOOR") { //절하
        if(nDigit < 0) {
            nDigit = -(nDigit);
            nCalcVal = Math.floor(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
        } else {
            nCalcVal = Math.floor(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
        }
    } else {                        //그대로(무조건 소수점 첫째 자리에서 반올림)
        nCalcVal = nCalcVal.toFixed(0);
    }

    return nCalcVal;
  };
  /**
   * 생년월일 입력받아 보험나이를 반환한다.
   *
   * @param birthDate 생년월일 (형식:yyyyMMdd, 예:20080915)
   * @param standardDate 표준날자 (형식:yyyyMMdd, 예:20080915)
   * @return 보험나이(상령나이)
   */
  function getInsuranceAgeByBirthDate(birthDate, standardDate) {
    if (!hasText(birthDate) || !hasText(standardDate))
      return 0;

    var age = 0;
    var comp_year = 0; // 나이계산용
    var comp_mm = 0; // 월

    if (birthDate.length >= 8) {
      var currentYear = standardDate.substring(0, 4); // 기준년
      var currentMonth = standardDate.substring(4, 6); // 기준월
      var currentDay = standardDate.substring(6, 8); // 기준일

      var year = birthDate.substring(0, 4); // 주민번호-년
      var month = birthDate.substring(4, 6); // 주민번호-월
      var day = birthDate.substring(6); // 주민번호-일

      comp_year = currentYear; // 현재년을 일단 셋트합니다.
      comp_mm = currentMonth; // 현재년도 월

      // 현재월 - 생월
      // 0 이면 12개월 경과
      // > 0 이면 12 + 1월 경과
      // < 0 이면 12개월 미만

      //일자를 먼저 계산
      // 현재일 - 생일
      if ((currentDay - day) < 0) {
        comp_mm = currentMonth - 1; // 계산된 월
      }

      // 계산된 월 - 생월
      // 계산값이 0 보다 작으면
      if ((comp_mm - month) < 0) {
        comp_year = currentYear - 1;
        comp_mm = parseInt(comp_mm, 10) + 12;
      }

      // 나이 = 계산된 년 - 생년
      age = comp_year - year;
      // 계산된 월 - 생월 >= 6
      if ((comp_mm - month) >= 6) {
        age += 1;
      }

    }

    return age;
  };
  /**
   * 일자 구하기
   * startDate : YYYYMMDD
   * endDate : YYYYMMDD
   */
  function getDiffDate(startDate, endDate) {
    var start = _strToDate(startDate);
    var end = _strToDate(endDate);
    return parseInt(Math.ceil(end-start) / (24*3600*1E3));
  }

  /**
   * 스트링 날짜를 DATE 객체로 변경
   *
   */
  function _strToDate(dateStr) {
    var d = new Date();
    d.setFullYear(dateStr.substr(0, 4), parseInt(dateStr.substr(4, 2), 10) - 1, dateStr.substr(6, 2));
    return d;
  }
}
