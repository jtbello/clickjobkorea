angular
  .module('easi.common.shared')
  .controller('menuCtrl', menuCtrl)
;

function menuCtrl($log, $scope, $storage, $rootScope, $message, $modalInstance) {
  var logger = $log(this);
  var vm = this;

  _.assign(vm, {
  });
  
  _.assign($scope, {
    filterGroupA : function(m) { return _.contains(['cu'], m.id); },
    filterGroupB : function(m) { return _.contains(['ac', 'sc'], m.id); },
    filterGroupC : function(m) { return _.contains(['fp','ot'], m.id); },
    filterGroupD : function(m) { return _.contains(['qu', 'pl', 'qu-aircraft'], m.id); },
    filterGroupE : function(m) { return _.contains(['sp', 'sa'], m.id); },
    filterExceptMe : function(m) { return  m.id != "tsot2104"; }
  });
  
  $scope.closeMainMenu = function() { $modalInstance.close(); }
  
  init();

  
  
  function init() {
    logger.debug('init', vm);
        
    $scope.$on('$destroy', destroy);
    
    // hardcoded - lane 에 따라 메뉴 노출이 달라요~    
  }

  function destroy(event) {
    logger.debug(event);
  }  
}
