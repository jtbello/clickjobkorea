angular.module('easi.common.shared', [])
    .run(function($rootScope, $modal, $window){
        $rootScope.showPopup = function(param){
            //모달 오픈
            return $modal.open({
                templateUrl: 'popup/showPopup/showPopup.tpl.html',
                controller: 'showPopupCtrl',
                controllerAs: 'showPopup',
                backdrop: false,
                resolve: {
                    item: function() {
                        return param;
                    }
                }
            });
        }
        $window.showPopup = $rootScope.showPopup;

        $rootScope.openPop = function(params){
            return $modal.open({
                templateUrl : params.tplUrl,
                controller : params.ctrl,
                controllerAs : params.ctrlAs,
                backdrop : !params.backdrop ? false : params.backdrop,
                resolve : {
                    item : function(){
                        return [params.data];
                    }
                }
            });
        };

        $window.openPop = $rootScope.openPop;
    });