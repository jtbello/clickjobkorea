
angular
.module('easi.common.shared')
.controller('pnInsertPCtrl', pnInsertPCtrl);

function pnInsertPCtrl($log, $rootScope, $scope, $modal, $modalInstance, $message, item, GuestSvc,ComSvc, ngspublic, comConstant , $window , $q, $cordovaPushV5C) {
  /**
  * Private variables
  */
  var logger = $log(this);
  var vm = this;
  let _fn = {
    make : { 
      hours : function(){
        let result = [];
        for(let i = 0 ; i < 24 ;i ++){
          let mockup = { v: -1 , t : ''};
          mockup.v = i;
          if(i < 10){ mockup.t += '0'; }
          mockup.t += i;
          result.push(mockup);
        }
        return result;
      },
      mins : function(){
        let result = [];
        for(let i = 0 ; i < 60 ;i ++){
          let mockup = { v: -1 , t : ''};
          mockup.v = i;
          if(i < 10){ mockup.t += '0'; }
          mockup.t += i;
          result.push(mockup);
        }

        return result;
      }
    },
    init : function(){
      if(vm.mod.hours.length <= 0){
        vm.mod.hours = _fn.make.hours();
      }
      if(vm.mod.mins.length <= 0){
        vm.mod.mins = _fn.make.mins();
      }
      
      if(!_.isUndefined(item[0])){
        item = item[0];
      }

      vm.mod.meta.ris = item.REQUST_INFO_SEQ;
      vm.mod.meta.memberSeq = item.MEMBER_SEQ;
      vm.mod.meta.rbs = item.RCUT_BASE_SEQ;
      
      if(item.START_DATE){
        vm.mod.meta.startDate = item.START_DATE;
        vm.mod.meta.endDate = item.END_DATE;
        vm.mod.meta.pgmName = item.PGM_NM;
      }

      console.log(item);
      
      vm.mod.departure.hour = vm.mod.hours[0];
      vm.mod.wakeup.hour = vm.mod.hours[0];
      vm.mod.departure.min = vm.mod.mins[0];
      vm.mod.wakeup.min = vm.mod.mins[0];
      
      _fn.data.get()
        .then(function(data){
          if(data.user.resultData.requstMemberSubInfo.length > 0){
            var findObj = _.find(vm.mod.hours, {t : data.user.resultData.requstMemberSubInfo[0].START_HOUR});
            vm.mod.departure.hour = findObj;
            findObj = _.find(vm.mod.hours, {t : data.user.resultData.requstMemberSubInfo[0].GETUP_HOUR});
            vm.mod.wakeup.hour = findObj;
            findObj = _.find(vm.mod.mins, {t : data.user.resultData.requstMemberSubInfo[0].START_MINITE});
            vm.mod.departure.min = findObj;
            findObj = _.find(vm.mod.mins, {t : data.user.resultData.requstMemberSubInfo[0].GETUP_MINITE});
            vm.mod.wakeup.min = findObj;
            
            vm.mod.departure.location = data.user.resultData.requstMemberSubInfo[0].START_POINT;
           
            vm.mod.meta.memberSubInfoSeq = data.user.resultData.requstMemberSubInfo[0].MEMBER_SUB_INFO_SEQ;
          }
        })

    },
    data : {
      valid : {
        get : function(){
          let result = true;
          if(vm.mod.meta.memberSeq === -1){ result = false; }
          if(vm.mod.meta.ris === -1){ result = false; }

          return result;
        },
        submit : function(){
          let msg = '';
          let result = _fn.data.valid.get();
          if(!result){
            msg = '정보가 올바르지 않습니다.';
          }
          if(!vm.mod.wakeup.hour || !vm.mod.wakeup.hour.t){ msg = '기상시간이 올바르지 않습니다.'; }
          if(!vm.mod.wakeup.min || !vm.mod.wakeup.min.t){ msg = '기상시간이 올바르지 않습니다.'; }
          if(!vm.mod.departure.hour || !vm.mod.departure.hour.t){ msg = '출발시간이 올바르지 않습니다.'; }
          if(!vm.mod.departure.min || !vm.mod.departure.min.t){ msg = '출발시간이 올바르지 않습니다.'; }

          if(vm.mod.wakeup.hour.t+vm.mod.wakeup.min.t > vm.mod.departure.hour.t+vm.mod.departure.min.t){ msg = '출발시간을 기상시간 이전으로 설정 할 수 없습니다.'; }
          if(!vm.mod.departure.location || vm.mod.departure.location === ''){ msg = '출발장소를 입력해 주세요.'; }

          return msg;
        },
        cancel : function(){
          let result = _fn.data.valid.submit();
          if(!vm.mod.meta.memberSubInfoSeq || vm.mod.meta.memberSubInfoSeq === -1){ result = false; }

          return result;
        }
      },
      get : function(){
        let params = {};
        params.memberSeq = vm.mod.meta.memberSeq + '';
        params.requestInfoSeq = vm.mod.meta.ris;

        return GuestSvc.selectRequestMemberSubInfo(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            // WHATEVER 
            deferred.resolve(d);
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      },
      submit: function(){
        let params = {};
        params.memberSeq = vm.mod.meta.memberSeq + '';
        params.requestInfoSeq = vm.mod.meta.ris + '';
        params.getupHour = vm.mod.wakeup.hour.t;
        params.getupMinite = vm.mod.wakeup.min.t;
        params.startHour = vm.mod.departure.hour.t;
        params.startMinite = vm.mod.departure.min.t;
        params.startPoint = vm.mod.departure.location;

        return GuestSvc.insertRequestMemberSubInfo(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            // WHATEVER 
            deferred.resolve(d);
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      },
      update : function(){
        let params = {};
        params.memberSeq = vm.mod.meta.memberSeq + '';
        params.memberSubInfoSeq = vm.mod.meta.memberSubInfoSeq + '';
        params.getupHour = vm.mod.wakeup.hour.t;
        params.getupMinite = vm.mod.wakeup.min.t;
        params.startHour = vm.mod.departure.hour.t;
        params.startMinite = vm.mod.departure.min.t;
        params.startPoint = vm.mod.departure.location;

        return GuestSvc.updateRequestMemberSubInfo(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            // WHATEVER 
            deferred.resolve(d);
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      }
    }
  };

  /**
   * Public variables
   */
  _.assign(vm, {
    mod : {
      meta : {
        // REQUST_INFO_SEQ
        ris : -1,
        memberSeq : -1,
        memberSubInfoSeq : -1,
        startDate : -1,
        endDate : -1,
        pgmName : ''
      },
      hours : [],
      mins : [],
      wakeup : {
        hour : '',
        min : ''
      },
      departure : {
        hour : '',
        min : '',
        date : '',
        location : ''
      }
    }
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    dismiss : dismiss,
    confirm : confirm,
    evt : {
      pop : {
        close : function(){
          vm.dismiss();
        }
      },
      submit : function(){
        let result =_fn.data.valid.submit();
        if(_.isEmpty(result)){
          _fn.data.submit()
          .then(function(){
            $window.alert('저장되었습니다');

            _fn.data.get()
            .then(function(d){
              
              if(d.user.resultData.requstMemberSubInfo.length >= 0){
                let alrmSeq = d.user.resultData.requstMemberSubInfo[0].MEMBER_SUB_INFO_SEQ;
                let wakeupDt = moment(vm.mod.meta.startDate, "YYYY-MM-DD HH:mm:ss");
                  wakeupDt.hour(vm.mod.wakeup.hour.v);
                  wakeupDt.minute(vm.mod.wakeup.min.v);

                let departureDt = moment(vm.mod.meta.startDate, "YYYY-MM-DD HH:mm:ss");
                  departureDt.hour(vm.mod.departure.hour.v);
                  departureDt.minute(vm.mod.departure.min.v);

                  var wakeupParam = {
                      id : parseInt('10' + alrmSeq),
                      title : '[기상] [' + vm.mod.meta.pgmName + ']',
                      mesg : '['+ vm.mod.meta.pgmName +'] 기상 시간입니다.',
                      date : wakeupDt.toDate().getTime(),
                      type : 'onetime'
                  };

                  var departureParam = {
                      id : parseInt('20' + alrmSeq),
                      title : '[출발] [' + vm.mod.meta.pgmName + ']',
                      mesg : '['+ vm.mod.meta.pgmName +'] 출발 시간입니다.',
                      date : departureDt.toDate().getTime(),
                      type : 'onetime'
                  };

                  var alarmsParam = [];
                  var nowTime = moment.now();
                  if(nowTime <= wakeupDt.toDate().getTime()){
                    alarmsParam.push(wakeupParam);
                  }
                  if(nowTime <= departureDt.toDate().getTime()){
                    alarmsParam.push(departureParam);
                  }
                  // todo 로컬노티 변경
                  $cordovaPushV5C.schedule({
                      alarms:alarmsParam
                  }).then(function (result) {
                      $log.debug("schedule success : " + JSON.stringify(result));
                      $message.alert('알람이 등록 되었습니다.');
                      // loadAlarmList();
                  });
              }
            });
          });
        }
        else{
          $window.alert(result);
        }
      },
      update : function(){
        let result =_fn.data.valid.submit();
        if(_.isEmpty(result)){
          _fn.data.update()
          .then(function(){
            $window.alert('수정되었습니다');
            
            _fn.data.get()
            .then(function(d){
              let alrmSeq = d.user.resultData.requstMemberSubInfo[0].MEMBER_SUB_INFO_SEQ;
              // let dt = moment(vm.mod.meta.startDate);
              // dt.hour(vm.mod.wakeup.hour);
              // dt.minute(vm.mod.wakeup.min);
              // $cordovaLocalNotification.cancel(alrmSeq , function(){
              //   $cordovaLocalNotification.schedule({
              //     id : parseInt('10' + alrmSeq),
              //     at : dt.toDate(),
              //     title : '[기상] [' + vm.mod.meta.pgmName + ']',
              //     text : '['+ vm.mod.meta.pgMName +'] 기상 시간입니다.'
              //   } , vm.confirm);
              // });
                let wakeupDt = moment(vm.mod.meta.startDate, "YYYY-MM-DD HH:mm:ss");
                wakeupDt.hour(vm.mod.wakeup.hour.v);
                wakeupDt.minute(vm.mod.wakeup.min.v);

                let departureDt = moment(vm.mod.meta.startDate, "YYYY-MM-DD HH:mm:ss");
                departureDt.hour(vm.mod.departure.hour.v);
                departureDt.minute(vm.mod.departure.min.v);

                var wakeupParam = {
                    id : parseInt('10' + alrmSeq),
                    title : '[기상] [' + vm.mod.meta.pgmName + ']',
                    mesg : '['+ vm.mod.meta.pgmName +'] 기상 시간입니다.',
                    date : wakeupDt.toDate().getTime(),
                    type : 'onetime'
                };

                var departureParam = {
                    id : parseInt('20' + alrmSeq),
                    title : '[출발] [' + vm.mod.meta.pgmName + ']',
                    mesg : '['+ vm.mod.meta.pgmName +'] 출발 시간입니다.',
                    date : departureDt.toDate().getTime(),
                    type : 'onetime'
                };

                var alarmsParam = [];
                var nowTime = moment.now();
                if(nowTime <= wakeupDt.toDate().getTime()){
                  alarmsParam.push(wakeupParam);
                }
                if(nowTime <= departureDt.toDate().getTime()){
                  alarmsParam.push(departureParam);
                }
                // todo 로컬노티 변경
                $cordovaPushV5C.update({
                    alarms:alarmsParam
                }).then(function (result) {
                    $log.debug("schedule success : " + JSON.stringify(result));
                    $message.alert('알람이 수정 되었습니다.');
                    // loadAlarmList();
                });
            });
          });
        }else{
          $window.alert(result);
        }
      }
    }
  });

  /**
  * Initialize
  */
  init();

  function init() {
      logger.debug('init', vm);
      _fn.init();
      $scope.$on('$destroy', destroy);
  }

  /**
  * Event handlers
  */
  function destroy(event) {
    logger.debug(event);
  }

  /**
  * Custom functions
  */

  /**
   * 모달 닫기
   */
  function dismiss() {
    $modalInstance.dismiss(vm);
  }


  /**
   * 정상 종료
   */
  function confirm(){
    $modalInstance.close(vm);
  }


}
