
angular
.module('easi.common.shared')
.controller('iaPrivacyPCtrl', iaPrivacyPCtrl);

function iaPrivacyPCtrl($log, $rootScope , $storage, $scope, $modal, $modalInstance, $message, item, GuestSvc,MemberSvc, ngspublic, comConstant , $window , $q) {
  /**
  * Private variables
  */
  var logger = $log(this);
  var vm = this;
  let _fn = {
    set : function(agreeYn){
        let params = {};
        params.busnLinkKey = vm.meta.busnLinkKey + '';
        params.agreeKind = vm.meta.AGREE_KIND;
        params.agreeYn = agreeYn;

        return MemberSvc.insertAgree(params)
        .then(function(d){
            let deferred = $q.defer();
            if(d.user.result === 'SUCCESS'){
                deferred.resolve(d);
            }
            else{ deferred.reject(d); }
            return deferred.promise;
        })
        .catch(function(d){
            let deferred = $q.defer();
            deferred.reject(d);
            return deferred.promise;
        });
    }
  };

  /**
   * Public variables
   */
  _.assign(vm, {
    meta : {
        AGREE_KIND : 'A1',
        busnLinkKey : -1
    },
    chkVal : ''
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    dismiss : dismiss,
    confirm : confirm,
    agree : function(){
        if(vm.chkVal == ''){
          $message.alert('동의여부를 선택해주세요.');
          return false;
        }
        if(vm.chkVal == 'Y'){
          if($window.confirm('동의하시겠습니까')){
              _fn.set('Y')
              .then(function(){
                  $storage.set('IA_PRIVACY_AGREE_KIND' , 'Y');
                  $modalInstance.dismiss('Y');
              });
          }
        }else{
          if($window.confirm('미동의시 서비스 이용이 불가합니다. 미동의 하시겠습니까')){
            _fn.set('N')
            .then(function(){
                $storage.set('IA_PRIVACY_AGREE_KIND' , 'N');
                $modalInstance.dismiss('N');
            });
          }
        }
    },
    disagree : function(){
        if($window.confirm('미동의시 서비스 이용이 불가합니다. 미동의 하시겠습니까')){
            _fn.set('N')
            .then(function(){
                $storage.set('IA_PRIVACY_AGREE_KIND' , 'N');
                $modalInstance.dismiss('N');
            });
        }
    }
  });

  /**
  * Initialize
  */
  init();

  function init() {
    logger.debug('init', vm);

    console.log(item);
    
    if(!item[0].busnLinkKey){
        $modalInstance.close();
    }
    else{
        vm.meta.busnLinkKey = item[0].busnLinkKey;
    }

    $scope.$on('$destroy', destroy);
  }

  /**
  * Event handlers
  */
  function destroy(event) {
    logger.debug(event);
  }

  /**
  * Custom functions
  */

  /**
   * 모달 닫기
   */
  function dismiss() {
    $modalInstance.dismiss(vm);
  }


  /**
   * 정상 종료
   */
  function confirm(){
    $modalInstance.close(vm);
  }


}
