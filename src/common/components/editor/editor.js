/* global EditorJSLoader, Editor, Trex */

angular
  .module('easi.common')
  .directive('editor', editor);

function editor($document, $timeout) {
  return {
    templateUrl: 'components/editor/editor.tpl.html',
    restrict: 'EA',
    require: 'ngModel',
    scope: {
      submit: '&onSubmit',
      triggers: '@submitTriggers',
      initHeight: '=initHeight'
    },
    link: {
      pre: function preLink() {
      },
      post: function postLink(scope, element, attrs, ngModel) {
        var id = _.uniqueId();
        var config = {
          txHost: '', /* 런타임 시 리소스들을 로딩할 때 필요한 부분으로, 경로가 변경되면 이 부분 수정이 필요. ex) http://xxx.xxx.com */
          txPath: '', /* 런타임 시 리소스들을 로딩할 때 필요한 부분으로, 경로가 변경되면 이 부분 수정이 필요. ex) /xxx/xxx/ */
          txService: 'sample', /* 수정필요없음. */
          txProject: 'sample', /* 수정필요없음. 프로젝트가 여러개일 경우만 수정한다. */
          initializedId: '', /* 대부분의 경우에 빈문자열 */
          wrapper: 'tx_trex_container' + '', /* 에디터를 둘러싸고 있는 레이어 이름(에디터 컨테이너) */
          form: 'tx_editor_form' + '', /* 등록하기 위한 Form 이름 */
          txIconPath: '../vendor/daumeditor/images/icon/editor/', /*에디터에 사용되는 이미지 디렉터리, 필요에 따라 수정한다. */
          txDecoPath: '../vendor/daumeditor/images/deco/contents/', /*본문에 사용되는 이미지 디렉터리, 서비스에서 사용할 때는 완성된 컨텐츠로 배포되기 위해 절대경로로 수정한다. */
          canvas: {
            initHeight: scope.initHeight || 800,
            styles: {
              color: '#123456', /* 기본 글자색 */
              fontFamily: '굴림', /* 기본 글자체 */
              fontSize: '10pt', /* 기본 글자크기 */
              backgroundColor: '#fff', /*기본 배경색 */
              lineHeight: '1.5', /*기본 줄간격 */
              padding: '8px' /* 위지윅 영역의 여백 */
            },
            showGuideArea: false
          },
          events: {
            preventUnload: false
          },
          sidebar: {
            attachbox: {
              show: true
            }
          },
          size: {
            // contentWidth: '100%' /* 지정된 본문영역의 넓이가 있을 경우에 설정 */
          },
          save: {
            onSave: angular.noop
          }
        };

        scope.editorConfig = config;

        EditorJSLoader.ready(function(Editor) {
          var editor = new Editor(config);
          observeJobs(editor);
          initTriggers();

          $timeout(function() {
            var meta = $document.find('meta[name="viewport"]');
            var iframe = $document.find('#' + config.wrapper + ' iframe');
            var contentDocument = angular.element(iframe.prop('contentDocument'));
            var title = contentDocument.find('head title');
            
            iframe.css("overflow", "hidden");
            title.after(meta.clone());
          }, 100);

          // Editor.switchEditor(id);

          ngModel.$render = function() {
            Editor.modify({
              content: ngModel.$viewValue || ''
            });
          };

          scope.$watch('initHeight', function(newVal, oldVal) {
            Editor.canvas.setCanvasSize({height: newVal});
          });
        });

        function observeJobs(editor) {
          var handler = _.debounce(modelChange, 0);
          _.chain(Trex.Ev)
            .omit('__CANVAS_PANEL_MOUSEMOVE', '__CANVAS_PANEL_MOUSEOVER', '__ON_SUBMIT')
            .each(function (ev) {
              Editor.canvas.observeJob(ev, handler);
            }).value();

          Editor.canvas.editor.observeJob(Trex.Ev.__ON_SUBMIT, function (editor) {
            scope.submit({editor: editor});
          });
        }

        function modelChange() {
          var newValue = Editor.getContent();
          if (ngModel.$viewValue === newValue) {
            return;
          }
          ngModel.$setViewValue(newValue);
        }

        function initTriggers() {
          var triggerNames = [];
          if(!_.isEmpty(scope.triggers)){
            triggerNames = scope.triggers.split(',');
          }
          var triggerSelectors;
          var triggerEls;

          triggerSelectors = _.map(triggerNames, function (name) {
            return '[editor-submit-trigger="' + name + '"]';
          });
          triggerEls = $document.find(triggerSelectors.join(','));
          triggerEls.on('click', function (e) {
            Editor.save();
          });
        }
      }
    }
  };
}