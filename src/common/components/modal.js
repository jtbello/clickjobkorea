
angular
  .module('easi.common')
  .factory('$modal', ModalFactory);

var stack = [];

function ModalFactory($log, $document, $rootScope, $controller, $templateCache, $compile, $q, $injector) {
  return {
    open: open
  };

  function open(options) {
    var container = $document.find('#layerSpaceWrap');
    var template = options.template || $templateCache.get(options.templateUrl);
    var elem = angular.element(template);
    var scope = (options.scope || $rootScope).$new();

    var resultDeferred = $q.defer();
    var openedDeferred = $q.defer();

    var modalInstance = {
      result: resultDeferred.promise,
      opened: openedDeferred.promise,
      close: function(result) {
        var modal = _.remove(stack, {instance: this})[0];
        scope.$destroy();
        resultDeferred.resolve(result);
        modal.options.elem.remove();
        modal.options.backdrop.remove();
        if (!stack.length) {
          container.hide();
        }
      },
      dismiss: function(reason) {
        var modal = _.remove(stack, {instance: this})[0];
        scope.$destroy();
        resultDeferred.reject(reason);
        modal.options.elem.remove();
        modal.options.backdrop.remove();
        if (!stack.length) {
          container.hide();
        }
      }
    };

    if (!template) {
      var reason = 'Error: Template is not valid';
      openedDeferred.reject(reason);
      resultDeferred.reject(reason);
      return modalInstance;
    }

    scope.$close = modalInstance.close;

    var resolvePromises = $q.all(getResolvePromises(options.resolve));

    resolvePromises.then(function() {
      openedDeferred.resolve(true);
    }, function() {
      openedDeferred.reject(false);
    });

    resolvePromises.then(function(resolved) {
      if (options.controller) {
        var locals = {
          $scope: scope,
          $modalInstance: modalInstance
        };
        var index = 0;
        _.each(options.resolve, function(value, key) {
          locals[key] = resolved[index++];
        });
        try {
          var controller = $controller(options.controller, locals);
        } catch(e) {
          $log.error('$modal', e);
          openedDeferred.$$reject(e);
          resultDeferred.reject(e);
          return modalInstance;
        }
        if (options.controllerAs) {
          scope[options.controllerAs] = controller;
        }
      }
      
      var backdrop = angular.element('<div class="layerSpaceSec"></div>');
      if($rootScope.lockBackgroundColorChange){
        backdrop = angular.element('<div class="layerSpaceSec lockBackgroundColorChange"></div>')
      }
      if (options.backdrop !== false) {
        container.append(backdrop);
      }
      container.append(elem);
      container.show();
      stack.push({instance: modalInstance, options: {
        elem: elem,
        backdrop: backdrop
      }});
        $rootScope.modalStack = stack;
      elem.show();
      $compile(elem)(scope);
    }, function(reason) {
      resultDeferred.reject(reason);
    });

    return modalInstance;
  }

  function getResolvePromises(resolves) {
    var promisesArr = [];
    angular.forEach(resolves, function (value) {
      if (angular.isFunction(value) || angular.isArray(value)) {
        promisesArr.push($q.when($injector.invoke(value)));
      }
    });
    return promisesArr;
  }
}
