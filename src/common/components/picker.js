// angular
//   .module('easi.common')
//   .config(inputConfig)
//   ;

function inputConfig($provide, $injector) {
  $provide.decorator('inputDirective', function($delegate, $compile, $injector, $window, $doryPicker) {
    var directive = $delegate[0];
    var preLink = directive.link.pre;

    directive.link.pre = function(scope, element, attrs, ngModel) {
      if (/date|time|datetime/i.test(attrs.type) && !_.isEmpty(cordova.plugins.DateTimePicker)) {
        // element.attr('type', 'text');
        element[0].readOnly = true;
        var behavior = {
          date: {
            fn: $doryPicker.datePicker
          },
          time: {
            fn: $doryPicker.timePicker
          },
          datetime: {
            fn: $doryPicker.datetimePicker
          }
        };
        element.on('click', function(e) {
          var type = behavior[attrs.type.toLowerCase()];
          type.fn(moment(element.val() || undefined).toDate())
            .then(function(date) {
              element.val(moment(date).format(type.format)).trigger('change');
            });
          e.preventDefault();
        });
        var clientH = document.documentElement.clientHeight;
        // 달력 클릭시 이벤트
        if ($('body').hasClass('ap')){ 
          element.next('button').click(function(){
            element.click();
          });
        } 
      }
      preLink(scope, element, attrs, ngModel);
    };
    return $delegate;
  });
}
