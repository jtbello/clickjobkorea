
angular
  .module('easi.common')
  .factory('$message', MessageFactory)
  .directive('easiConfirm', function() {
    return {
      priority: -1,
      restrict: 'A',
      link: function (scope, element, attrs) {
        var msg = attrs.easiConfirm;

        element.bind('click', function(e){
          var message = attrs.ngConfirmClick;
          if(msg && !confirm(msg)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    };
  });

function MessageFactory($window) {
  return {
    alert: alert,
    confirm : confirm,
    nav : {
      alert : function(opt){
        // opt.message      메시지 
        // opt.callback     콜백함수 (함수)
        // opt.title        제목
        return $window.navigator.notification.alert(opt.message , opt.callback , opt.title || '');
      },
      confirm : function(opt){
        // opt.message      메시지 
        // opt.callback     콜백함수 (함수)
        // opt.title        제목
        return $window.navigator.notification.confirm(opt.message, opt.callback, opt.title || '');
      }
    }
  };
  
  function alert(message , callback , title) {
    return $window.alert(message);
  }
  function confirm(message) {
    return $window.confirm(message);
  }
}
