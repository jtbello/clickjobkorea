
angular
  .module('easi.common', [
    'ngAnimate',
    'ngSanitize',
    'ngCordova',
    'ngTouch',
    'ui.router',
    'checklist-model',
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.cellNav',
    'ui.grid.pagination',
    'ui.grid.selection',
    'ui.grid.rowEdit',
    'ui.tree',
    'fusioncharts',
    'angularMoment',
    'xml',
    'swipe',
    'easi.core',
    'easi.api',
    'easi.common.legacy',
    'easi.common.shared',
    'angular-google-analytics'
  ])
  .constant('doryConst', _.deepFreeze({
    STR: 'str',
    ARR: [
      'arr1', 'arr2', 'arr3'
    ],
    ARR_OBJ: [
      {
        AOSTR: 'aostr1'
      },
      {
        AOSTR: 'aostr2'
      },
    ],
    OBJ: {
      OSTR: 'ostr',
      DEEP: {
        DOSTR: 'dostr'
      }
    },
      FOOTER: {
          URL: 'layout/footer/footer.tpl.html'
      }
  }))
  .run(function($rootScope, $log, $storage, $user, $q, ComSvc, $cordovaDevice, $window, $env, $state, $modal, $document, CompanySvc, 
                ngspublic, $message, $cordovaInAppBrowser, $cordovaPushV5C, $cordovaToast, $cordovaDialogs,
                $cordovaGeolocation, $http, doryConst, MemberSvc, Analytics) {

      // 지하철 버스 URL 저장
      $storage.get('MAP_INFO_URL')
      .then(function(data){
          if(_.isBlank(data)) {

            ComSvc.selectCommonInfo('MAP_INFO_URL')
            .then(function(resultData){
              // 지하철
              var findObj = _.find(resultData.user.resultData.commonData, {COMMON_INFO_VALUE1 : 'M2'});
              $rootScope.subWayUrl = findObj.COMMON_INFO_VALUE2;
              // 버스
              findObj = _.find(resultData.user.resultData.commonData, {COMMON_INFO_VALUE1 : 'M1'});
              $rootScope.busUrl = findObj.COMMON_INFO_VALUE2;

              $storage.set('MAP_INFO_URL', resultData.user.resultData.commonData);
            })

          }else{
            // 지하철
            var findObj = _.find(data, {COMMON_INFO_VALUE1 : 'M2'});
            $rootScope.subWayUrl = findObj.COMMON_INFO_VALUE2;
            // 버스
            findObj = _.find(data, {COMMON_INFO_VALUE1 : 'M1'});
            $rootScope.busUrl = findObj.COMMON_INFO_VALUE2;
          }
      })

        $rootScope.isOnBackButton = false;
      $document.on('backbutton', function (e) {

          // 모달 닫기
          if(!_.isEmpty($rootScope.modalStack) && $rootScope.modalStack.length > 0){
              $rootScope.modalStack[0].instance.dismiss();
              return;
          }

          // 메뉴 닫기
          if($rootScope.menuOpen){
              $rootScope.menuCloseBtn();
              return;
          }

          if($rootScope.stateHistory.length <= 1){

              if($rootScope.isOnBackButton){
                  if (navigator.app) {

                      // 자동로그인여부
                      $storage.get('AUTO_LOGIN')
                          .then(function(data){
                              if(data !== 'Y') {
                                  $user.destroy();
                              }
                              $rootScope.forceLogout();
                              navigator.app.exitApp();
                          })
                          .catch(function(err){
                              $user.destroy();
                              $rootScope.forceLogout();
                              navigator.app.exitApp();
                          });
                  } else if (navigator.device) {
                      // 자동로그인여부
                      $storage.get('AUTO_LOGIN')
                          .then(function(data){
                              if(data !== 'Y') {
                                  $user.destroy();
                              }
                              $rootScope.forceLogout();
                              navigator.device.exitApp();
                          })
                          .catch(function(err){
                              $user.destroy();
                              $rootScope.forceLogout();
                              navigator.device.exitApp();
                          });
                  }
                  return false;
              }

              $rootScope.isOnBackButton = true;
              $cordovaToast.showShortBottom('버튼을 한번 더 누르면 종료됩니다.');
              setTimeout(function(){
                  $rootScope.isOnBackButton = false;
              }, 2000);

          }else{
              $rootScope.prevView();
          }
          return false;
      });

      // $document.on('click', 'a[href^="http"]', function (e) {
      //     $bdExtension.browser(this.href);
      //     return false;
      // });

      $document.on('keydown', function (e) {
          // 키보드 엔터 누를때 키보드 숨기기
          if ($env.cordova && e.keyCode === 13 && /input/i.test(e.target.nodeName)) {
              setTimeout(function(){
                  $(':focus').blur();
              });
          }
      });

      // 모든 버튼에 GA Event 트래킹 설정
      $document.on('click', 'a, button', function (e) {
        try{
          var LOGINUSERID = $window.localStorage.getItem("LOGINUSERID");
          var clientId = LOGINUSERID || _.guid();
          var baseURI = e.delegateTarget.URL;
          var btnText = e.target.text;

          Analytics.trackEvent('ButtonEvent',clientId,  btnText);
        }catch(e){}
        return false;
      });

      $rootScope.stateHistory = [];
      $rootScope.menuTitle = '';
      $rootScope.doryConst = doryConst;

      $env.get()
          .then(function(data){
              $rootScope.envEndpoint = data.endPoint;
          });

      $rootScope.linkTerm01 = function(){
          var options = {
              location: 'yes',
              scrollbars: 'yes',
              resizable: 'yes',
              usewkwebview: 'no'
          };
          $cordovaInAppBrowser.open($env.endPoint.service+'/web/termInfo1.dory', '_blank', options);
      };

      $rootScope.linkTerm02 = function(){
          var options = {
              location: 'yes',
              scrollbars: 'yes',
              resizable: 'yes',
              usewkwebview: 'no'
          };
          $cordovaInAppBrowser.open($env.endPoint.service+'/web/termInfo2.dory', '_blank', options);
      };

      $rootScope.searchAddrByGeocode = function(){
          var d = $q.defer();
          var startTime = Date.now();
          var timerName = '(Stopwatch) {' + _.guid().substr(0, 8) + '} searchAddrByGeocode';
          $log.log('request:', timerName);
          console.time(timerName);

          if(window.env.platform === 'android'){
              cordova.plugins.diagnostic.isGpsLocationEnabled(function(enabled){
                  if(enabled){
                      $cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy: false})
                          .then(function(position){
                              $log.log('Latitude: '          + position.coords.latitude          + '\n' +
                                  'Longitude: '         + position.coords.longitude         + '\n' +
                                  'Altitude: '          + position.coords.altitude          + '\n' +
                                  'Accuracy: '          + position.coords.accuracy          + '\n' +
                                  'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
                                  'Heading: '           + position.coords.heading           + '\n' +
                                  'Speed: '             + position.coords.speed             + '\n' +
                                  'Timestamp: '         + position.timestamp                + '\n');

                              var request = {
                                  headers: {
                                      'Content-Type': 'application/json',
                                      'X-NCP-APIGW-API-KEY-ID' : '407hrkrth8',
                                      'X-NCP-APIGW-API-KEY' : '6bOV8FfCFYF91FXpAMQBZZAnbL4beo4VS52DMbR1'
                                  },
                                  url: 'https://naveropenapi.apigw.ntruss.com/map-reversegeocode/v2/gc?output=json&coords='+position.coords.longitude+','+position.coords.latitude,
                                  method: 'GET'
                              };
                              $http(request)
                                  .then(function(response) {
                                      $log.log('response:', timerName, Date.now() - startTime);
                                      console.timeEnd(timerName);
                                      d.resolve(response.data);
                                  })
                                  .catch(function(rejection) {
                                      $log.error('responseError:', timerName, Date.now() - startTime);
                                      console.timeEnd(timerName);
                                      d.reject(rejection);
                                  });
                          })
                          .catch(function(error){
                              $log.error('responseError:', timerName, Date.now() - startTime);
                              console.timeEnd(timerName);
                              d.reject({message : '위치정보를 가져오는데 실패하였습니다.\n위치 설정에 위치인식방식을 "높은정확도"로 설정 해 주세요.'});
                          });
                  }else{
                      d.reject({message : '위치 설정이 비활성화 되어있습니다.\n설정에서 위치 정보를 활성화 해 주세요.'});
                  }

              }, function(error){
                  d.reject(error);
              });
          }else{
              $cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy: false})
                  .then(function(position){
                      $log.log('Latitude: '          + position.coords.latitude          + '\n' +
                          'Longitude: '         + position.coords.longitude         + '\n' +
                          'Altitude: '          + position.coords.altitude          + '\n' +
                          'Accuracy: '          + position.coords.accuracy          + '\n' +
                          'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
                          'Heading: '           + position.coords.heading           + '\n' +
                          'Speed: '             + position.coords.speed             + '\n' +
                          'Timestamp: '         + position.timestamp                + '\n');

                      var request = {
                          headers: {
                              'Content-Type': 'application/json',
                              'X-NCP-APIGW-API-KEY-ID' : '407hrkrth8',
                              'X-NCP-APIGW-API-KEY' : '6bOV8FfCFYF91FXpAMQBZZAnbL4beo4VS52DMbR1'
                          },
                          url: 'https://naveropenapi.apigw.ntruss.com/map-reversegeocode/v2/gc?output=json&coords='+position.coords.longitude+','+position.coords.latitude,
                          method: 'GET'
                      };
                      $http(request)
                          .then(function(response) {
                              $log.log('response:', timerName, Date.now() - startTime);
                              console.timeEnd(timerName);
                              d.resolve(response.data);
                          })
                          .catch(function(rejection) {
                              $log.error('responseError:', timerName, Date.now() - startTime);
                              console.timeEnd(timerName);
                              d.reject(rejection);
                          });
                  })
                  .catch(function(error){
                      $log.error('responseError:', timerName, Date.now() - startTime);
                      console.timeEnd(timerName);
                      d.reject(error);
                  });
          }
          return d.promise;
      };

    // $rootScope.toggleLeft = function() {
    //   // 이전에 오픈했던 메뉴 닫기
    //   if($rootScope.menuOpen = !$rootScope.menuOpen) {
    //     try {
    //     $('#mgnb').scrollTop(0);
    //     } catch(e) {}
    //   }
    // };

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      $rootScope.menuOpen = false;
      if(toState.data) {
          $rootScope.viewDepth = toState.data.depth;
          $rootScope.menuTitle = toState.data.menuTitle;
          if($rootScope.viewDepth <= 1) {
              $rootScope.stateHistory = [];
          }
      } else {
          $rootScope.viewDepth = 0;
      }

      var findObj = _.find($rootScope.stateHistory, {name : toState.name});
      if(!findObj){
        $rootScope.stateHistory.push(toState);
        $log.log('history push:', $rootScope.stateHistory);
      }

    });

    // 디바이스 정보 저장
    try{
      $rootScope.DEVICE_MODEL = $cordovaDevice.getModel();
      $rootScope.DEVICE_VERSION = $cordovaDevice.getVersion();
    }catch(e){
      $rootScope.DEVICE_MODEL = '';
      $rootScope.DEVICE_VERSION = '';
    }

      //$rootScope.UUID = ''; // I use a localStorage variable to persist the token

      if(window.env.platform !== 'browser'){
          // $cordovaPushV5C.initialize(  // important to initialize with the multidevice structure !!
          //     {
          //         android: {
          //             senderID: "1:1039231952564:android:ad6d8727f100fedf",
          //             clearNotifications : false
          //         },
          //         ios: {
          //             alert: 'true',
          //             badge: true,
          //             sound: 'true',
          //             clearBadge: true,
          //             clearNotifications : false
          //         },
          //         windows: {}
          //     }
          // ).then(function (result) {
          //     $log.log('cordovaPushV5.initialize', result)
          //     $cordovaPushV5C.register().then(function (resultreg) {
          //         $rootScope.UUID = resultreg;
          //         $log.log('tokenId', resultreg)
          //         // SEND THE TOKEN TO THE SERVER, best associated with your device id and user
          //     }, function (err) {
          //         $log.error(err)
          //         // handle error
          //     });
          //     $cordovaPushV5C.onNotification();
          //     $cordovaPushV5C.onError();
          // });
      }

      // 푸시 이벤트
      $rootScope.$on('$cordovaPushV5C:notificationReceived', function(event, notification) {
          $log.log('Push Message', notification.message)
          if (undefined != notification.additionalData.foreground){
              if (notification.additionalData.foreground === false) {
                  // do something if the app is in foreground while receiving to push - handle in app push handling
              } else {
                  // handle push messages while app is in background or not started
                  //$cordovaToast.showShortTop(notification.message);
                  // $cordovaLocalNotification.schedule({
                  //     id: moment.now(),
                  //     title: notification.title,
                  //     text: notification.message,
                  //     foreground : true,
                  //     launch : true,
                  //     autoClear : false
                  // }).then(function (result) {
                  //
                  // });
                  $cordovaDialogs.alert(notification.message, notification.title, '확인');
                  $cordovaDialogs.beep(1);
              }
          }
          if (window.env.platform === 'ios') {
              if (notification.additionalData.badge) {
                  $cordovaPushV5C.setBadgeNumber(NewNumber).then(function (result) {
                      // OK
                  }, function (err) {
                      // handle error
                  });
              }

              $cordovaPushV5C.finish().then(function (result) {
                  // OK finished - works only with the dev-next version of pushV5.js in ngCordova as of February 8, 2016
              }, function (err) {
                  // handle error
              });
          }
      });
      $rootScope.$on('$cordovaPushV5C:errorOccurred', function(event, error) {
          $log.log("gcm error ", error);
      });
      ////////////////////////////////////////////////////////////////////////////////////////////

      $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event){
          // 주소팝업
          $cordovaInAppBrowser.executeScript({
              code: "callbackJuso();"
          })
              .then(function(data){
                  var callbackData = data[0];
                  if(callbackData.inputYn === 'Y'){
                      $cordovaInAppBrowser.close();
                      $rootScope.callbackFunction(callbackData);
                  }
              });
          
          try{

            if(event.url.match('checkPlusSuccess') || event.url.match('checkPlusFail')){
                // 본인인증팝업
                $cordovaInAppBrowser.executeScript({
                    code: "callbackAuth();"
                })
                    .then(function(data){
                        var callbackData = data[0];
                        if(callbackData.result === 'success'){
                            $rootScope.callbackFunction(callbackData);
                        }
                        $cordovaInAppBrowser.close();
                    });
            }
          }catch(e){}
      });

    $rootScope.openAddrSearchPopup = function(callbackFunction){

          // if(window.env.platform === 'browser'){
          //     //window.open($env.endPoint.service+"/include/modal/jusoPopup2.jsp","_system","width=570,height=420, scrollbars=yes, resizable=yes");
          //     var options = {
          //         location: 'yes',
          //         // clearcache: 'yes',
          //         // toolbar: 'no'
          //         scrollbars: 'yes',
          //         resizable: 'yes'
          //     };
          //     $rootScope.callbackFunction = callbackFunction;
          //     $cordovaInAppBrowser.open($env.endPoint.service+'/include/modal/jusoPopup2.jsp', '_system', options);
          //     $cordovaInAppBrowser.executeScript({
          //         code: "callbackJuso();"
          //     })
          //         .then(function(data){
          //             var callbackData = data[0];
          //             if(callbackData.inputYn === 'Y'){
          //                 $cordovaInAppBrowser.close();
          //                 $rootScope.callbackFunction(callbackData);
          //             }
          //         });
          // } else {
              var options = {
                  location: 'yes',
                  // clearcache: 'yes',
                  // toolbar: 'no'
                  scrollbars: 'yes',
                  resizable: 'yes',
                  usewkwebview: 'no'
              };
              $rootScope.callbackFunction = callbackFunction;
              $cordovaInAppBrowser.open($env.endPoint.service+'/include/modal/jusoPopup2.jsp', '_blank', options);
          // }
      };

      $rootScope.openAuthCheckPopup = function(encData, callbackFunction){

          var options = {
              location: 'yes',
              // clearcache: 'yes',
              // toolbar: 'no'
              scrollbars: 'yes',
              resizable: 'yes',
              usewkwebview: 'no'
          };
          $rootScope.callbackFunction = callbackFunction;
          $cordovaInAppBrowser.open($env.endPoint.service+'/gateCert.jsp?encData=' + encData, '_blank', options);
      };
      
      $rootScope.makeParam = function(param){
        var keySize = 128;
        var iterationCount = 10000;
        var aesUtil = new AesUtil(keySize, iterationCount);
        var encrypt = aesUtil.encrypt($rootScope.salt, $rootScope.iv, $rootScope.passPhrase, param);
        return encrypt;  
      }; 
      
      $rootScope.iv = "00000000000000000000000000000000";
      $rootScope.salt = "00000000000000000000000000000000";
      $rootScope.passPhrase = "aesalgoisbestbes";

    /**
     * 에러 로그 저장
     */
    $rootScope.insertErrLog = function(param){

      if(!param) return false;

      if($user.currentUser){
        param.USER_ID = $user.currentUser.getUserId();
      }else{
        param.USER_ID = "NO_LOGIN";
      }
      param.DEVICE_MODEL =  $rootScope.DEVICE_MODEL;
      param.DEVICE_OS_VER = $rootScope.DEVICE_VERSION;

      // 개발용
      $log.debug("insertErrLog param : " + JSON.stringify(param));

      if(!param.USER_ID) {$log.error("insertErrLog parameter err [USER_ID]" ); return false;}
      if(!param.DEVICE_MODEL) {$log.error("insertErrLog parameter err [DEVICE_MODEL]" ); return false;}
      if(!param.DEVICE_OS_VER) {$log.error("insertErrLog parameter err [DEVICE_OS_VER]" ); return false;}

        ComSvc.insertErrLog(param)
        .then(function(data){
          $log.debug("insertErrLog suc");
        })
        .catch(function(data){
          $log.error("insertErrLog err");
        })

    };
      /**
       * 알림발송
       */
      $rootScope.comPushSend = function(memList, msg, rcutBaseSeq){
          var d = $q.defer();
          // 요청
          _.forEach(memList ,function(obj, idx){

              if(obj.MEMBER_NM == '' || obj.MEMBER_NM == undefined){
                  obj.MEMBER_NM = obj.COMPANY_NM;
              }

              var param = {};
              param.memberSeq = obj.MEMBER_SEQ;						//[필수] 회원일련번호
//              param.sendCompanySeq = obj.COMPANY_SEQ;				//[필수] 회사일련번호
              param.msg = msg;                      				//[필수] 전달 메시지
              if(!_.isBlank(rcutBaseSeq)){
                param.rcutBaseSeq = rcutBaseSeq; 
              }
              CompanySvc.sendMemberPush(param)
                  .then(function(data){
                      if(idx+1 == memList.length){
                          d.resolve();
                      }
                  })
                  .catch(function(err){
                      d.reject(err);
                  });
          });

          return d.promise;
      };
      
      $rootScope.goHome = function(){
          if(!$user.currentUser){
              $rootScope.goView('main.html#/main');
              return;
          }
          var loginKind = $user.currentUser.getLoginKind();
          if(loginKind === 'L1'){
              $rootScope.goView('pesn.html#/mainp/pmMain');
          }else if(loginKind === 'L2'){
              $rootScope.goView('comp.html#/mainc/cmMain');
          }
      };

      $rootScope.goSubWay = function(){
        if(_.isBlank($rootScope.subWayUrl)) return false;

        var options = {
                location: 'yes',
                scrollbars: 'yes',
                resizable: 'yes',
                usewkwebview: 'no'
            };
            $cordovaInAppBrowser.open($rootScope.subWayUrl, '_blank', options);
      };
      
      $rootScope.goBus = function(){
        if(_.isBlank($rootScope.busUrl)) return false;

        var options = {
                location: 'yes',
                scrollbars: 'yes',
                resizable: 'yes',
                usewkwebview: 'no'
            };
            $cordovaInAppBrowser.open($rootScope.busUrl, '_blank', options);
        
      };

      $rootScope.goAppGuide = function(){
          var options = {
              location: 'yes',
              scrollbars: 'yes',
              resizable: 'yes',
              usewkwebview: 'no'
          };
          try{
              var loginKind = $user.currentUser.getUserDataInfo().loginKind;
              if(loginKind === 'L1'){
                  $cordovaInAppBrowser.open($env.endPoint.service+'/guide/person/person.html', '_blank', options);
              }else if(loginKind === 'L2'){
                  $cordovaInAppBrowser.open($env.endPoint.service+'/guide/company/company.html', '_blank', options);
              }
          }catch(e){
              $cordovaInAppBrowser.open($env.endPoint.service+'/guide/person/person.html', '_blank', options);
          }
      };

      $rootScope.goView = function(viewId, backup, param){

          if(viewId.match('html#/')){
              if(param){
                  viewId = viewId + '?param=' + JSON.stringify(param);
              }
              $window.location.href = viewId;
          }else{
              var params = {};
              if(param){
                  params.param = param;
              }
              if(!_.isEmpty(backup)){
                  var backupData = {};
                  // _.each(backup, function(val, key){
                  //     if(!angular.isFunction(val)){backupData[key] = val;};
                  // });
                  backupData = ngspublic.getVariable(backup);
                  var fromState = $rootScope.getCurState();
                  fromState.params.backupData = backupData;
              }

              $state.go(viewId, params);
          }
      };

      // 메뉴 클릭시 화면 전환 (탭이 있는 뷰)
      $rootScope.moveView = function(viewId){
          if(!$user.currentUser){
              $message.alert('로그인 후 이용 가능합니다.');
              $rootScope.goView('public.html#/login');
              return;
          }
          
          if($window.location.href.indexOf(viewId) > -1){
            $window.location.reload();
          }else{
            $window.location.href = viewId;
          }
         
      };

      $rootScope.prevView = function(param){
        $log.log('history:', $rootScope.stateHistory);
          var length = $rootScope.stateHistory.length;
          if(length >= 1){
              $rootScope.stateHistory.splice(length-1, 1);
              var toState = $rootScope.stateHistory[$rootScope.stateHistory.length -1];

              if(!_.isEmpty(param)){
                  toState.params.param = param;
              }
              $state.go(toState.name, toState.params);
          }
      };
      $rootScope.getCurState = function(){
          var length = $rootScope.stateHistory.length;
          var fromState = $rootScope.stateHistory[length-1];
          return fromState;
      };
      $rootScope.firstView = function(){
          var length = $rootScope.stateHistory.length;
          if(length > 1){
              var confirmResult = $message.confirm('첫 화면으로 이동합니다.(작업 중인 정보는 사리집니다.)\n닫으시겠습니까?');
              if(!confirmResult) return false;
              $rootScope.stateHistory.splice(1, length-1);
              var toState = $rootScope.stateHistory[$rootScope.stateHistory.length-1];
              $state.go(toState.name, toState.params);
          }
      };

      $rootScope.menuOpenBtn = function() {
          if(!$user.currentUser){
              $rootScope.loginText = 'LOGIN'
              $rootScope.loginInfoText = '';
          }else {
              $rootScope.loginText = 'LOGOUT'
              try{
                var loginKind = $user.currentUser.getUserDataInfo().loginKind;
                if(loginKind === 'L1'){
                  $rootScope.loginInfoText = $user.currentUser.getUserInfoList()[0].MEMBER_NM +  '님';
                }else if(loginKind === 'L2'){
                    $rootScope.loginInfoText = $user.currentUser.getUserInfoList()[0].COMPANY_NM;
                }
              }catch(e){
                $rootScope.loginInfoText = '';
              }
          }
          $rootScope.menuOpen = true;
          $(".sidenav-content").addClass("active");
      };

      $rootScope.menuCloseBtn = function() {
          $rootScope.menuOpen = false;
          $(".sidenav-content").removeClass("active");
      };

      $rootScope.prevBtn = function(){
          $rootScope.prevView();
      };

      $rootScope.closeBtn = function(){
          $rootScope.firstView();
      };

      $rootScope.myPageBtn = function(){
          if(!$user.currentUser){
              $message.alert('로그인 후 이용 가능합니다.');
              $rootScope.goView('public.html#/login');
              return;
          }
          var loginKind = $user.currentUser.getUserDataInfo().loginKind;
          if(loginKind === 'L1'){
              $rootScope.goView('pesn.html#/mypage/pmMyPage');
          }else if(loginKind === 'L2'){
              $rootScope.goView('comp.html#/mypagec/cmMyPage');
          }
      };

      $rootScope.logoBtn = function(){
          if(!$user.currentUser){
              return;
          }
          var loginKind = $user.currentUser.getUserDataInfo().loginKind;
          if(loginKind === 'L1'){
              $rootScope.goView('pesn.html#/mainp/pmMain');
          }else if(loginKind === 'L2'){
              $rootScope.goView('comp.html#/mainc/cmMain');
          }
      };

      $rootScope.forceLogout = function(){
          MemberSvc.logOut()
              .then(function(data){
                  // $user.destroy(); // 자동로그인이 안타서 주석.
              })
              .catch(function(err){
                  $user.destroy();
              });
      };
  })

  .factory('ActionTimer', function($log) {
    var logger = $log('ActionTimer');
    var actions = {};
    return {
      start: start,
      lap: lap,
      stop: stop
    };
    function start(name, message) {
      actions[name] = {
        start: Date.now(),
        laps: []
      };
      logger.info([name, '(start:0ms) ', message].join(''));
    }
    function lap(name, message) {
      var action = actions[name];
      if (!action) {
        logger.error(Error(name + ' is not registered.'));
        return;
      }
      var start = action.start;
      var laps = action.laps;
      var now = Date.now();
      laps.push(now);
      logger.info([name, '(lap', laps.length, ':', now - start,'ms) ', message].join(''));
    }
    function stop(name, message) {
      var action = actions[name];
      if (!action) {
        logger.error(Error(name + ' is not registered.'));
        return;
      }
      var start = action.start;
      var laps = action.laps;
      var now = Date.now();
      logger.info([name, '(stop:', now - start, 'ms) ', message].join(''));
      delete actions[name];
    }
  })
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "?ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "yy-mm-dd",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    closeText: "닫기",
                    prevText: "이전달",
                    nextText: "다음달",
                    currentText: "오늘",
                    monthNames: [ "1월","2월","3월","4월","5월","6월",
                        "7월","8월","9월","10월","11월","12월" ],
                    monthNamesShort: [ "1월","2월","3월","4월","5월","6월",
                        "7월","8월","9월","10월","11월","12월" ],
                    dayNames: [ "일요일","월요일","화요일","수요일","목요일","금요일","토요일" ],
                    dayNamesShort: [ "일","월","화","수","목","금","토" ],
                    dayNamesMin: [ "일","월","화","수","목","금","토" ],
                    weekHeader: "주",
                    dateFormat: "yy-mm-dd",
                    firstDay: 0,
                    isRTL: false,
                    showMonthAfterYear: true,
                    yearSuffix: "년",
                    //showOn:"button",
                    //buttonImage:"/images/bg_cal.png",
                    //buttonImageOnly:true,
                    showButtonPanel: true,
                    onClose: function () {
                        if ($(window.event.srcElement).hasClass('ui-datepicker-close')) {
                            // $(this).val('');
                            // updateModel('');
                        }
                    },
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                $(elem).datepicker(options);
            }
        }
    })
  // select 사용을 위한 디랙티브.
  .directive('metaOptions', function($compile, $parse, MetaSvc, $timeout, $log) {
    return {
      priority: 1001, // compiles first
      terminal: true, // prevent lower priority directives to compile after it
      replace : false,
      restrict : 'A',
      scope: false, // := {}
      compile: function(el, attr) {
        var metaCodeName = _.uniqueId('_mds'); // scope unique id 보장받음.
        var metaCode = el.attr('meta-options'); // copy metaCode
        var metaBind = el.attr('meta-options-bind'); // bind 에 사용될 parent model (filter 로 사용 됨) 명.
        var metaBindExists = _.has(attr, 'metaOptionsBind');
        var metaType = el.attr('meta-options-type'); // type 에 따라, [key] value / value
        var data = el.attr('meta-options-data') || null;
        var ignore = el.attr('meta-options-ignore') || null; // ignore code

        var sortOrder = el.attr('meta-options-sortorder') || 'a'; // [Asc | Desc]
        var sortKey = el.attr('meta-options-sortkey') || 'k'; // [Key | Value]

        el.removeAttr('meta-options'); // necessary to avoid infinite compile loop
        if(metaType) {
          el.attr('ng-options', "kv.k as ( (kv.k) ? ('[' + kv.k + '] ' + kv.v) : kv.v ) for kv in " + metaCodeName);
        } else {
          el.attr('ng-options', "kv.k as ( kv.v ) for kv in " + metaCodeName);
        }

        return function(scope, iel, attr, ctl) {
          var modelGetter = $parse(attr['ngModel']);

          var iofunc = function(newCode, oldCode) {
            var dval = _.isEmpty(modelGetter(scope)) ? modelGetter(scope) : undefined;
            if(metaBindExists) {
              if(_.isEmpty(newCode)) {
                scope[metaCodeName] = [{ k : dval, v : '상위조건을 선택하세요.' }]; // 조회중 표시
                modelGetter.assign(scope, undefined);
                return;
              }
            } else {
              scope[metaCodeName] = [{ k : dval, v : '조회중입니다.' }]; // 조회중 표시
            }

            // 데이터셋 준비하고~ // Service Call!!! - promise 모델
            MetaSvc.getBizCode(metaCode) //
            .then(function(kvo) {
              var kv = _.clone(kvo);
              if(_.startsWith(sortKey, 'v')) { kv = _.sortBy(kv, 'v'); }
              if(_.startsWith(sortOrder, 'd')) { kv = kv.reverse(); }

              // bind 여부 여기서 고려 (필터로 동작)
              if(metaBindExists) {
                kv = _.filter(kv, { s : newCode || undefined });
              }

              if(!_.isEmpty(data) && _.isString(data)) {
                _.chunk(data.split('$'), 2).reverse().forEach(function(d) {
                  kv.unshift({ k : d[0], v : d.length == 2 ? d[1] : d[0] });
                });
              }

              if(!_.isEmpty(ignore) && _.isString(ignore)) {
                var a = ignore.split('$');
                kv = _.remove(kv, function(d) { return !_.contains(a, d.k); });
              }

              // 바인딩 값이 없고, kv 값에 empty 가 없다면~~ 첫 번째 값으로 바인딩
              var sel = modelGetter(scope);

              if(!_.isEmpty(kv) && (_.isEmpty(sel) || !_.some(kv, { k : sel  || undefined }))) {
                modelGetter.assign(scope, kv[0].k);
              }

              // 바인딩.
              scope[metaCodeName] = kv;

              iel.removeAttr('meta-options');
              iel.empty();

              // option 중복 bug fix
              var newel = angular.element(iel[0].outerHTML);
              iel.after(newel);
              iel.remove();

              $compile(newel)(scope);

            })
            .catch(function(msg) {

              var kv = [{ v : msg }];

              // 바인딩 값이 없고, kv 값에 empty 가 없다면~~ 첫 번째 값으로 바인딩
              var sel = modelGetter(scope);
              if(_.isEmpty(sel)) {
                modelGetter.assign(scope, kv[0].k);
              }

              // 바인딩.
              scope[metaCodeName] = kv;
            })
            ;
          } // end iofunc


          // 상위코드 바인딩 여부에 따라서 watch 여부 결정.
          if(metaBindExists && !_.isEmpty(metaBind)) {
            scope.$watch(metaBind, function(n, o) { iofunc(n, o); });
          } else {
            iofunc();
          }

          $compile(iel)(scope);
        };
      }
    }
  })

  // Table row-span 을 위한 디랙티브 선언. (ng-repeat 걸리는 곳에서 row-span="[0,1]" 등으로 td 의 index 설정
  .directive('rowSpan',function($timeout, $parse) {
    return {
      restrict : 'A',
      compile : function(el, attr) {
        var incCol = _.has(attr, 'rowSpanCol');
        return function(scope, element, attrs) {
          if (scope.$last) {
            var modelGetter = $parse(attrs['rowSpan']);
            $timeout(function() {
              $(modelGetter(scope)).each(function(a, n) {
                $(element.parent()).rowspan(n);
              });
            }, 0);
          }
        };
      }
    };
  })
  // Table row-span 을 위한 디랙티브 선언. (ng-repeat 걸리는 곳에서 row-span="[0,1]" 등으로 td 의 index 설정
  .directive('colSpan', function($timeout, $parse) {
    return {
      restrict : 'A',
      compile : function(el, attr) {
        var incCol = _.has(attr, 'rowSpanCol');
        return function(scope, element, attrs) {
          if (scope.$last) {
            $timeout(function() {
              $('tr', element.parent()).each(function(n) {
                $(element.parent()).colspan(n);
              });
            }, 0);
          }
        };
      }
    };
  })

  .directive('ngIsolate', function() {
    return { restrict : 'A', scope: true };
  })
  .directive('maxlength', function($log, $message){
    return {
      restrict : 'A',
      link : function(scope, element, attrs) {
        var mlen = parseInt(attrs.maxlength || 0);
        if(_.isNaN(mlen) || mlen < 1) { return; }
        var nf = _.has(attrs, 'numberFormat');

        var $input = $(element);

        $input.bind('keydown', function(e) {
          $log.debug('keydown', e.keyCode, $input.val());
          var len = 0;
          if(!_.isEmpty(window.getSelection().toString())) { return; }
          if($input.val().length >= mlen && e.keyCode != 8 && e.keyCode != 9 && e.keyCode != 46 && e.keyCode != 13) {
            //if($input.val().length > mlen) { $input.val($input.val().substr(0, mlen)); $input.trigger('input'); }
            $input.blur();
            $message.alert('최대 입력 글자('+mlen+')를 초과했습니다.');
          }
        });
        $input.bind('change keyup', function(e) {
          $log.debug('keyup', e.keyCode, $input.val());
          var len = $input.val().length;
          if(len > mlen) {
            $input.val($input.val().slice(0, mlen)); $input.trigger('input');
          }
        });
      }
    };
  })
  .directive('inputAutoSelect', function() {
    return {
      restrict : 'A',
      link : function(scope, element, attrs) {
        var $input = $(element);
        function se() {
          $input.select();
          $input.unbind('click', se);
          $input.blur(function() {
            $input.bind('click', se);
          });
        }
        $input.bind('click', se);
      }
    };
  })

  .config(function($compileProvider, $httpProvider, AnalyticsProvider) {
    $httpProvider.useApplyAsync(true);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|javascript|file):/);
    // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
    
    try{
      // GA 설정
      var LOGINUSERID = window.localStorage.getItem("LOGINUSERID");
      var clientId = LOGINUSERID || _.guid();
      var trackerKey = 'UA-124593338-1';
      
      AnalyticsProvider
        .setAccount({
          tracker : trackerKey,
          name : 'tracker1',
          fields:{
            cookieDomain : 'clickjobkorea.co.kr',
            cookieName : 'clickjobkorea',
            cookieExpires : 20000,
            storage : 'none',
            clientId : clientId
          },
          trackEvent : true,
          trackPages : true,
          trackEcommerce: true
        })
        .useECommerce(true, true)
        .trackPages(true)
        .trackUrlParams(true)
        .ignoreFirstPageLoad(true)
        .setCurrency('KRW')
        .setHybridMobileSupport(true);
    }catch(e){
      // analytics 에러 발생해도 영향 없도록 처리
    }
    
  });
 ;
