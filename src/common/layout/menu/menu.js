angular
  .module('easi.common')
  .directive('leftMenu', function($io, $user, $q, $log, $modal, $message
         , $env, $window, $location, $rootScope, $storage, doryConst, $bdExtension, $state, MemberSvc, $timeout) {
    return {
      replace : true,
      restrict : 'E',
      templateUrl: 'layout/menu/menu.tpl.html',
      link: function(scope, iel, attr, ctl) {

        scope.$watch('$user.currentUser', function() {
            setTimeout(function() {
                scope.menuBuild();
            }, 500);
        });

          scope.loginBtn = function(){
              if(!$user.currentUser){
                  $rootScope.goView('public.html#/login');
              }else{
                  if($message.confirm('로그아웃 하시겠습니까?')){

                      MemberSvc.logOut()
                          .then(function(data){
                              $message.alert('로그아웃 되었습니다.');
                              $storage.remove('AUTO_LOGIN');
                              $user.destroy();
                              $rootScope.goView('main.html#/main');
                          })
                          .catch(function(err){
                              $message.alert(err);
                              $storage.remove('AUTO_LOGIN');
                              $user.destroy();
                              $rootScope.goView('main.html#/main');
                          });
                  }
              }
          };

          scope.menuBuild = function(e, $event) {
              // User/Env 객체 있는 경우에만... :)
                $q.all([$user.get(), $env.get()]).then(function() {
                    scope.loginKind = $user.currentUser.getLoginKind();
                //   $storage.remove('##menuRaw##');
                //   $storage.get('##menuRaw##').then(function(data) {
                //       if(_.isEmpty(data)) {
                //           var menuData = $user.currentUser.getMenuInfoList();
                //           $storage.set('##menuRaw##', menuData);
                //           buildMenu(menuData);
                //       } else {
                //           buildMenu(data);
                //       }
                //   })
                //       .then(function() {
                //           selectMenu($state.current);
                //       });
                    var menuData = $user.currentUser.getMenuInfoList();
                    buildMenu(menuData);
                }).catch(function(err){
                    scope.loginKind = '';
                    MemberSvc.selectMenuList()
                        .then(function(data){
                            buildMenu(data.user.resultData.menuList);
                        })
                        .catch(function(error){

                        });
                });
          };

          scope.toggle = function($event) {
              if(_.isEmpty($user.currentUser)){
                  $message.alert('로그인 후 이용 가능합니다.');
                  $rootScope.goView('public.html#/login');
                  return;
              }
              $($event.target).parent().parent().children("dd").slideToggle();
          };

          function buildMenu(result) {
                  //var loginKind = $user.currentUser.getUserDataInfo().loginKind;
                  // if(loginKind === 'L1'){
                  //     tempMenu = [
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/resume/prList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"1","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":442,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C0","COMMON_INFO_VALUE2":"신청(요청)현황","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/schedule/psMng","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"2","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":443,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C1","COMMON_INFO_VALUE2":"일정관리","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/resume/prList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"3","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":444,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C2","COMMON_INFO_VALUE2":"촬영정보관리","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/notify/pnList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"4","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":445,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C3","COMMON_INFO_VALUE2":"알림확인","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/coupon/pcShop","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"5","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":446,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C4","COMMON_INFO_VALUE2":"쿠폰구매","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/coupon/pcList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"6","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":447,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C5","COMMON_INFO_VALUE2":"쿠폰현황","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/schedule/psPnlty","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"7","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":448,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C6","COMMON_INFO_VALUE2":"패널티현황","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/mypage/pmMyPage","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"8","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":449,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C7","COMMON_INFO_VALUE2":"정보수정","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/notify/pnConfig","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"9","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":450,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C8","COMMON_INFO_VALUE2":"알림설정","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/inquiry/piList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"10","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":451,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C9","COMMON_INFO_VALUE2":"1:1문의하기","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"}
                  //     ];
                  // }else{
                  //     tempMenu = [
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"1","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":390,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0","COMMON_INFO_VALUE2":"프로그램","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/progmc/cpProgmReg","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"2","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":391,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0_M1_M2","COMMON_INFO_VALUE2":"프로그램등록","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L2"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/progmc/cpProgmList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"3","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":392,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0_M1_M1","COMMON_INFO_VALUE2":"프로그램관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L2"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/progmc/cpRcrutReg","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"4","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":393,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0_M2_M2","COMMON_INFO_VALUE2":"출연자모집정보등록","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L2"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/progmc/cpRcrutList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"5","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":394,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0_M2_M1","COMMON_INFO_VALUE2":"출연자모집정보관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L2"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"6","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":395,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1","COMMON_INFO_VALUE2":"출연자","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/mngpesn/cmSignupList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"7","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":396,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1_M2","COMMON_INFO_VALUE2":"회원가입요청승인","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/mngpesn/cmMngList?m=1","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"8","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":397,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1_M3","COMMON_INFO_VALUE2":"관심출연자","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/mngpesn/cmMngList?m=2","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"9","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":398,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1_M4","COMMON_INFO_VALUE2":"출연요청중","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/mngpesn/cmMngList?m=3","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"10","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":399,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1_M5","COMMON_INFO_VALUE2":"출연완료","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"11","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":408,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_WEB","COMMON_INFO_VALUE1":"M5","COMMON_INFO_VALUE2":"쿠폰관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/couponc/ccCopnShop","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"12","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":409,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_WEB","COMMON_INFO_VALUE1":"M5_M1","COMMON_INFO_VALUE2":"쿠폰구매","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/couponc/ccCopnList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"13","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":410,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_WEB","COMMON_INFO_VALUE1":"M5_M2","COMMON_INFO_VALUE2":"쿠폰현황","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"14","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":400,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M2","COMMON_INFO_VALUE2":"권한","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/authc/caNewReg","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"15","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":401,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M2_M2","COMMON_INFO_VALUE2":"신규등록","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/authc/caMenuMng","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"16","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":402,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M2_M1","COMMON_INFO_VALUE2":"권한별메뉴관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/authc/caPesnMng","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"17","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":403,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M2_M2","COMMON_INFO_VALUE2":"권한별사용자관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"18","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":404,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M3","COMMON_INFO_VALUE2":"정산","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/paymt/cpPaymtList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"19","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":405,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M3_M1","COMMON_INFO_VALUE2":"정산관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"20","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":406,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M4","COMMON_INFO_VALUE2":"1:1문의답변관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/qna/cqQnaList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"21","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":407,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M4_M1","COMMON_INFO_VALUE2":"1:1문의답변관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"}
                  //     ];
                  // }
              $rootScope.menu = [];
              result = _.each(result, function(val, key, obj) {
                      var idx = val.COMMON_INFO_VALUE1.split('_');
                      if(idx.length === 1){
                          val.idx = val.COMMON_INFO_VALUE1;
                          val.parentIdx = null;
                      }else{
                          val.idx = val.COMMON_INFO_VALUE1;
                          val.parentIdx = idx[0];
                      }
                  })
                  //트리 변환 메서드
                  var treeModel = function (result, rootId) {
                      var rootNodes = [];
                      var traverse = function (nodes, item, index) {
                          if (nodes instanceof Array) {
                              return nodes.some(function (node) {
                                  var idx = node.idx.split('_');
                                  if (idx[0] === item.parentIdx) {
                                      node.menu = node.menu || [];
                                      return node.menu.push(result.splice(index, 1)[0]);
                                  }

                                  return traverse(node.menu, item, index);
                              });
                          }
                      };

                      while (result.length > 0) {
                          result.some(function (item, index) {
                              if (item.parentIdx === rootId) {
                                  return rootNodes.push(result.splice(index, 1)[0]);
                              }

                              return traverse(rootNodes, item, index);
                          });
                      }

                      return rootNodes;
                  };
                  var menuJson = treeModel(result, null);

                  $rootScope.menu = menuJson; // result.serviceBody.data.menu;

                  $rootScope.menuRaw = md($rootScope.menu);
              // 한번 더~ 검증
              // if($rootScope.menuTitle == '') { $rootScope.setMenuTitle(); }
          }
      }
    }
  })

;
function clearAll(m, n) {
  var un = n || 0;
  if(un > 3) { return; }
  if(un == 0) {  }
  _.forEach(m, function(a) {
    a._show = false;
    if(a.menu) { clearAll(a.menu, un+1); }
  })
}

function selectMenu(l, m, n) {
  var un = n || 0;
  var show = false;
  
  _.forEach(un > 0 ? m.menu :  m, function(a) {
    if(_.has(a, 'uri') && _.endsWith(a.uri, l)) { return !(a._show = show = true); }
    if((show = selectMenu(l, a, un + 1))) { return false; }
  });
  return ((un && (m._show = show)), show);
}

function selectMenuById(l, m, n) {
  var un = n || 0;
  var show = false;
  
  _.forEach(un > 0 ? m.menu :  m, function(a) {
    if(_.has(a, 'id') && a.id == l) { return !(a._show = show = true); }
    if((show = selectMenuById(l, a, un + 1))) { return false; }
  });
  return ((un && (m._show = show)), show);
}



function md(v, d) {
  if(d > 10) { return []; }
  var rs = [];
  _.forEach(v, function(w) {
    if(_.has(w, 'uri')) { rs.push({id : w.id, name : w.name, uri : w.uri }); }
    if(_.has(w, 'menu') && w.menu.length) { rs = rs.concat(md(w.menu, (d || 0) + 1)); }
  });
  return rs;
}