angular
  .module('easi.public')
  .controller('findIDInfoCtrl', findIDInfoCtrl);

function findIDInfoCtrl($log, $rootScope, $scope, $stateParams, $timeout, $user, $message, $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    mod : {
      user_id  : ''
    }
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    evt : {
      login : function(){
          $rootScope.goView('public.login');
      },
      findPW : function(){
        $rootScope.goView('public.findPW');
      }
    }
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    if($stateParams.param){
        vm.mod.user_id = $stateParams.param.result.idInfo[0].USER_ID;
    }
    else{
        // 정보가 누락되거나 잘못된 경우
        $message.alert('정보가 올바르지 않습니다.');
        $rootScope.goView('public.login');
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
}