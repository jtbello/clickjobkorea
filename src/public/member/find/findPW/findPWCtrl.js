angular
  .module('easi.public')
  .controller('findPWCtrl', findPWCtrl)
  .filter('ph_nm' , function(){
    return function(input){
      if(input == 'L2') { return '대표자명'; }
      else if(input == 'L1'){ return '이름'; }
      else{ return '이름'; }
    };
  });

function findPWCtrl($log, $rootScope, $scope, $state, $timeout, $user, $message, $window, $q, $storage, ngspublic, $cordovaInAppBrowser, MemberSvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    opts : {
      phone : [
        '010',
        '011',
        '016',
        '017',
        '018',
        '019'
      ]
    },
    mod : {
      states : {
        login : ''
      },
      userName : '',
      userHpkind : '999',
      userHp : '',
      companyNm : '',
      userId : '',
      authCD : '',
      sEncData : '',
      reqSeq : ''

    }
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    evt : {
      request : {
        auth : function(){
          alert('준비중');
        },
        submit : function(){
          var params = {};

          if(vm.mod.states.login == 'L1'){
            params.findKind = '1';                                  // 1: 개인휴대폰번호찾기 2: 기업휴대폰번호찾기 3 : 기업사업자번호로대표자아이디찾기
            params.userId = vm.mod.userId;                          // 아이디
            params.userName = vm.mod.userName;                      // 회원이름
            params.userHpkind = vm.mod.userHpkind;                  // 휴대폰회사[공코:HP_KIND]
            params.userHp = vm.mod.userHp;                          // 휴대폰 번호
          }
          else if(vm.mod.states.login == 'L2'){
            params.findKind = '2';
            params.userId = vm.mod.userId;
            params.companyNm = vm.mod.companyNm;
            params.userName = vm.mod.userName;
            params.userHpkind = vm.mod.userHpkind;
            params.userHp = vm.mod.userHp;
          }

          if(valid(params)){
            MemberSvc.requestPwFindAuth(params).then(function(data){
              
              // $state.go('public.findPWChange' , {
              //   result : data.user.resultData
              // });
                if(data.user.resultData.sEncData.length > 0){
                    vm.mod.sEncData = data.user.resultData.sEncData;
                    vm.mod.reqSeq = data.user.resultData.reqSeq;

                    $rootScope.openAuthCheckPopup(vm.mod.sEncData, function (data){
                        if(data.result === 'success'){
                            var params = {
                                reqSeq : vm.mod.reqSeq,
                                userId : vm.mod.userId,
                                kind : vm.mod.states.login === 'L1' ? '1' : '2'
                            }
                            $rootScope.goView('public.findPWChange', vm, params);
                        }
                    });
                }

            } , function(error){
                logger.error(error);
            })
          }
        }
      }
    }
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    $storage.get('LOGIN_TYPE').then(function(data){
      console.log(data);
      vm.mod.states.login = data ? data : "L1";
    }, function(){
      $message.alert('데이터가 올바르지 않습니다.');
      $rootScope.goView('public.login');
    });
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
   /// 수정 해야됨  
  function valid(prms){

      var chk = true;
      if(prms.findKind == '2'){
          chk = chk && ngspublic.nullCheck(prms.companyNm,		'업체명을 입력해주세요.');
      }
      chk = chk && ngspublic.nullCheck(prms.userId,			'아이디를 입력해주세요.');
      chk = chk && ngspublic.nullCheck(prms.userName,			'이름을 입력해주세요.');
      chk = chk && ngspublic.nullCheck(prms.userHpkind,		'휴대전화회사를 선택해주세요.', '999');
      chk = chk && ngspublic.nullCheck(prms.userHp,			'휴대전화를 입력해주세요.');
      if(!chk) return false;

      return true;
   }
}