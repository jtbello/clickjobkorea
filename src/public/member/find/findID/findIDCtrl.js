angular
  .module('easi.public')
  .controller('findIDCtrl', findIDCtrl);

function findIDCtrl($log, $rootScope, $scope, $state, $timeout, $user, $message, $window, $q, $storage, ngspublic, MemberSvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    IDENTIFIERS : {
      TYPES : {
        BY_PN_NUM : 'BY_PHONE_NUMBER',
        BY_CORPS_REGIST_NUM : 'BY_CORPS_REGISTRATION_NUM'
      }
    },
    opts : {
      phone : [
        '010',
        '011',
        '016',
        '017',
        '018',
        '019'
      ]
    }, 
    mod : {
      states : {
        login : 'L1',
        type : ''
      },
      userName : '',           // 사용자 이름
      companyNm : '',          // 회사 이름
      companyCeoNm : '',            // 대표 이름
      companyNo : '',
      userHpkind : '999',
      userHp : ''
    }
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    evt : {
      submit : function(){
        var params = {}
        //console.log(vm.mod);
        if(vm.mod.states.login == 'L1'){
          // 개인
          params.findKind = '1';
          params.userName = vm.mod.userName;
          params.userHpkind = vm.mod.userHpkind;
          params.userHp = vm.mod.userHp;
        }
        else if(vm.mod.states.login == 'L2'){
          // 기업
          if(vm.mod.states.type == vm.IDENTIFIERS.TYPES.BY_PN_NUM){
            params.findKind = '2';
            params.companyNm = vm.mod.companyNm;
            params.userName = vm.mod.userName;
            params.userHpkind = vm.mod.userHpkind;
            params.userHp = vm.mod.userHp;
          }
          else if(vm.mod.states.type == vm.IDENTIFIERS.TYPES.BY_CORPS_REGIST_NUM){
            params.findKind = '3';
            params.companyNm = vm.mod.companyNm;
            params.companyNo = vm.mod.companyNo;
            params.companyCeoNm = vm.mod.companyCeoNm;
          }
        }

        // Validation 
        if(valid(params)){
          MemberSvc.findId(params)
          .then(function(data){
              logger.log('성공' , data);
            $rootScope.goView('public.findIDInfo', vm, {
                result : data.user.resultData
            });
  
            resetMod();
          }, function(data){
            console.log('실패' , data);
  
            resetMod();
          })
        }

      },
      requestAuth : function(){

      }
    }
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    vm.mod.states.type = vm.IDENTIFIERS.TYPES.BY_PN_NUM;
    $storage.get('LOGIN_TYPE').then(function(data){
      vm.mod.states.login = data;
    } , function(){
      // 개인 회원
      vm.mod.states.login = 'L1';
      
    })

    
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

   function resetMod(){
    vm.mod.states.login = 'L1';
    vm.mod.states.type = vm.IDENTIFIERS.TYPES.BY_PN_NUM;
    vm.mod.userName = '';
    vm.mod.companyNm = '';
    vm.mod.companyNo = '';
    vm.mod.userHp = '';
    vm.mod.companyCeoNm = '';
    vm.mod.userHpkind = '999';
   }

   function valid(prms){
       var chk = true;
       if(prms.findKind == '1'){
           chk = chk && ngspublic.nullCheck(prms.userName,			'이름를 입력해주세요.');
           chk = chk && ngspublic.nullCheck(prms.userHpkind,		'휴대전화회사를 선택해주세요.', '999');
           chk = chk && ngspublic.nullCheck(prms.userHp,			'휴대전화를 입력해주세요.');
       }else if(prms.findKind == '2'){
           chk = chk && ngspublic.nullCheck(prms.companyNm,		'업체명을 입력해주세요.');
           chk = chk && ngspublic.nullCheck(prms.userName,			'이름를 입력해주세요.');
           chk = chk && ngspublic.nullCheck(prms.userHpkind,		'휴대전화회사를 선택해주세요.', '999');
           chk = chk && ngspublic.nullCheck(prms.userHp,			'휴대전화를 입력해주세요.');
       }else if(prms.findKind == '3'){
           chk = chk && ngspublic.nullCheck(prms.companyNm,		'업체명을 입력해주세요.');
           chk = chk && ngspublic.nullCheck(prms.companyNo,			'사업자번호를 입력해주세요.');
           chk = chk && ngspublic.nullCheck(prms.companyCeoNm,			'대표자 이름을 입력해주세요.');
       }
       if(!chk) return false;

       return true;
   }
  
}