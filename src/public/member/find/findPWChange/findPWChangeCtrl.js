angular
  .module('easi.public')
  .controller('findPWChangeCtrl', findPWChangeCtrl);

function findPWChangeCtrl($log, $rootScope, $scope, $stateParams, $timeout, $user, $message, $window, $q, ngspublic, MemberSvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      reqSeq : '',
      kind : '',
      userId : '',
      userPw : '',
      userPwConfrim : ''
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    changePassword : changePassword
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    // 개인정보가 확인 된 경우 
    if($stateParams.param){
        vm.reqSeq = $stateParams.param.reqSeq;
        vm.kind = $stateParams.param.kind;
        vm.userId = $stateParams.param.userId;
    }
    else{
      // 정보가 누락되거나 잘못된 경우
      $message.alert('정보가 올바르지 않습니다.');
      $rootScope.goView('public.login');
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

    /**
     * 패스워드 변경
     */
    function changePassword(){

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.userPw, 	'패스워드를 입력해주세요.');
        if(chk && vm.userPw.length <= 4){
            $message.alert('패스워드를 4자 이상입력해주세요.');
            return false;
        }
        chk = chk && ngspublic.nullCheck(vm.userPwConfrim, 	'패스워드 확인을 입력해주세요.');
        if(chk && vm.userPw !== vm.userPwConfrim){
            $message.alert('패스워드확인이 정확하지않습니다.');
            return false;
        }
        if(!chk) return false;

        var param = {};
        param.reqSeq = vm.reqSeq;				// 인증요청 후 결과값에 인증키값
        param.kind = vm.kind;						// 1 : 개인 , 2 : 기업
        param.userId = vm.userId;				// 아이디
        param.userPw = vm.userPw;				// 변경 패스워드

        MemberSvc.updatePw(param)
            .then(function(data){
                $message.alert('정상적으로 변경되었습니다.');
                $rootScope.goView('public.login')
            })
            .catch(function(error){
              logger.log('updatePw err', error);
            });
    }
}