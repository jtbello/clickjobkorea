angular
  .module('easi.public')
  .controller('loginCtrl', loginCtrl);

function loginCtrl($log, $rootScope, $scope, $state, $timeout, $user, $message, $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork, MemberSvc, GuestSvc) {
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    aa : "198402251634342",
    IDENTIFIERS : {
      LOGIN : {
        PERSONAL : 'L1',
        CORPS : "L2"
      },
      ERROR : {
        NO_ACCOUNT : '* 아이디를 입력하세요',
        NO_PASSWORD : '* 비밀번호를 입력하세요',
        ACCOUNT_NOT_AVAILABLE : '* 가입되어있지 않은 아이디입니다',
        PASSWORD_NOT_AVAILABLE : '* 비밀번호가 올바르지 않습니다'
      },
      PLACEHOLDER : {
        PERSONAL : '아이디',
        CORPS : '일반 기업 ID'
      }
    },
    states : {
      login: '',
      autoLogin: false,
      isError : false
    },
    texts : {
      error : '',
      placeholder : ''
    },
    mod : {
      id : '',
      pw : ''
    }
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    // 이벤트 관련된 함수들을 모아둠 
    evt : {
      changeLoginState : function(state){
        console.log('login state changed ! before : ' + vm.states.login + ' after : ' + state);
        vm.states.login = state;
        $storage.set('LOGIN_TYPE', vm.states.login);
        if(state == vm.IDENTIFIERS.LOGIN.PERSONAL){
          vm.texts.placeholder = vm.IDENTIFIERS.PLACEHOLDER.PERSONAL;
        }
        else{
          vm.texts.placeholder = vm.IDENTIFIERS.PLACEHOLDER.CORPS;
        }
      },
      move : function(aState){
        $state.go(aState);
      },
      submit : function(){
        vm.states.isError = false;
        if(vm.mod.id.isEmpty()){
          vm.states.isError = true;
          vm.texts.error = vm.IDENTIFIERS.ERROR.NO_ACCOUNT;
        }
        else if(vm.mod.pw.isEmpty()){
          vm.states.isError = true;
          vm.texts.error = vm.IDENTIFIERS.ERROR.NO_PASSWORD;
        }
        else{
          MemberSvc.login({
            'loginKind' : vm.states.login,
            'userId' : vm.mod.id,
            'userPw' : vm.mod.pw,
            'siteKind' : 'APP'
          }).then(function(data){
            // onComplete

            $storage.set('AUTO_LOGIN', vm.states.autoLogin ? 'Y' : 'N');
            data.user.resultData.userInfo[0].MEMBER_PW = vm.mod.pw;
            $storage.set('userDataInfo', data.user.resultData);
            $window.localStorage.setItem("LOGINUSERID", vm.mod.id);

            if(vm.states.login === vm.IDENTIFIERS.LOGIN.PERSONAL){
                //$window.location.href= 'pesn.html#/mainp/pmMain';

                if(window.env.platform === 'browser'){
                    $rootScope.goView('pesn.html#/mainp/pmMain');
                }else{
                    var tempPushType = '';
                    if(window.env.platform == 'android'){
                        tempPushType = 'P1';
                    }else if(window.env.platform == 'ios'){
                        tempPushType = 'P2';
                    }
                    GuestSvc.updatePushInfo({
                        pushId : $rootScope.UUID,     // [필수] 푸쉬아이디
                        pushType : tempPushType,                    // [필수][공코:PUSH_TYPE] 푸쉬타입
                        type : 'T1',                        // [필수] 개인
                        memberSeq : data.user.resultData.userInfo[0].MEMBER_SEQ+''  // [필수] 회원 일련번호
                    }).then(function(data){
                        $rootScope.goView('pesn.html#/mainp/pmMain');
                    }).catch(function(error){
                        $rootScope.goView('pesn.html#/mainp/pmMain');
                    });
                }
            }
            else if(vm.states.login === vm.IDENTIFIERS.LOGIN.CORPS){
              //$window.location.href= 'comp.html#/comp/compMain';
              if(window.env.platform === 'browser'){
                  $rootScope.goView('comp.html#/mainc/cmMain');
              }else{
                  var tempPushType = '';
                  if(window.env.platform == 'android'){
                      tempPushType = 'P1';
                  }else if(window.env.platform == 'ios'){
                      tempPushType = 'P2';
                  }
                  GuestSvc.updatePushInfo({
                      pushId : $rootScope.UUID,     // [필수] 푸쉬아이디
                      pushType : tempPushType,                    // [필수][공코:PUSH_TYPE] 푸쉬타입
                      type : 'T2',                        // [필수] 개인
                      memberSeq : data.user.resultData.userInfo[0].COMPANY_MEMBER_SEQ+''  // [필수] 회원 일련번호
                  }).then(function(data){
                      $rootScope.goView('comp.html#/mainc/cmMain');
                  }).catch(function(error){
                      $rootScope.goView('comp.html#/mainc/cmMain');
                  });
              }  
              
             
            }
            else{
              $window.alert("로그인을 다시 시도해주세요");
            }
            
          }, function(data){
            vm.states.isError = true;
            if(data.user){
              vm.texts.error = "*" + data.user.resultMessage;
            }else{
              vm.texts.error = "* 네트워크 오류가 발생했습니다";
            }
          })
        }
      },
      signup : function(){
        if(vm.states.login == vm.IDENTIFIERS.LOGIN.PERSONAL){
          $state.go('public.signupPesn');
        }
        else{
          $state.go('public.signupComp');
        }
      }
    }
  });
  
  
  init();

  function init() {

    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    vm.evt.changeLoginState(vm.IDENTIFIERS.LOGIN.PERSONAL);
  }

  function destroy(event) {
    logger.debug(event);
  }
}