angular.module('easi.public').controller('signupPesnCtrl', signupPesnCtrl);

function signupPesnCtrl(
  $log,
  $rootScope,
  $scope,
  $state,
  $timeout,
  $user,
  $message,
  $window,
  $q,
  MemberSvc,
  $cordovaInAppBrowser,
  ComSvc,
  comConstant,
  $filterSet,
  $env,
  ngspublic,
  $stateParams
) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    bankNameInfo : [],          // 은행정보
    memberCompayInfoList: [], // 기업 리스트
    genderInfo: [],
    emailDomainOptions: [], // 이메일 도메인 리스트
    subwayTimeOptions: [], // 지하철역 소요시간 리스트

    idDupChkFlag: false, // 아이디 중복확인 결과
    rrnDupChkFlag: false, // 주민등록번호 중복확인
    hpAuthFlag: false,  // 전화번호 인증
    hpAuthDis: false,   // 전화번호 인증 성공 결과

    // 파라미터
    memberId: '', //[필수] 아이디
    memberPw: '', //[필수] 패스워드
    memberPwChk: '', //패스워드 체크
    memberNm: '', //[필수] 이름
    memberGender: '', //[필수][공코 : GENDER] 성별
    memberKind: 'M1', //[필수][공코 : MEMBER_KIND] 회원종류
    memberRrn: '', //[필수]주민번호 ('-'제거)
    memberRrn1: '', //주민번호 앞
    memberRrn2: '', //주민번호 뒤
    memberHpKind: '', //[필수][공코 : HP_KIND] 휴대전화회사종류
    memberHp: '', //[필수]휴대폰번호 ('-'제거)
    memberAddr: '', //[필수]기본 주소
    memberAddrBase: '', //기본 주소
    memberAddrNo: '', //우편번호
    memberSubwayLines: '999',//[필수]지하철호선
    memberSubwayName: '', //[필수]가까운지하철
    memberSubwayTime: '999', //[필수]소요시간
    height: '', //[필수]키
    weight: '', //[필수]몸무게
    emailId: '', //[필수]이메일id
    emailDomainId: '999', //[필수]이메일도메인id
    emailDomain: '', // 이메일 도메인
    recomMemId: '', // [선택] 추천인 ID

    photoAttachInfo : [],
    photoAttachSeqs: '', //[필수]사진 첨부파일 일련번호

    memberCompayInfo: '999', //[필수] 기업일련번호
    memberCompayInfoEtc: '', // 기업소개
    memberEntercompayInfo: '', //[선택] memberKind 종류가 M2 일때 소속 회사명
    accNo: '', //[선택]계좌번호('-'제거)
    accBankName: '999', //[선택][공코 : BANK_NAME]계좌은행명
    accName: '', //[선택]계좌예금주
    companyNoAttachSeqs: '', //[선택]사업자등록증 및 증빙서류 첨부파일 일련번호

    videoAttachInfo : [],
    videoAttachSeqs: '', //[선택]동영상 첨부파일 일련번호

    companyNofileDisabled : false,			// 사진 첨부파일 1 파일선택가능
    companyNofileOrgName : '',				// 사진 첨부파일 1 원본파일이름
    readfile : '',				        // 파일 1


    // 회원 경력
    memberCareerInfo: [] // 회원 경력
    // careerInfo.field = 'F1';								//[필수][공코 : FIELD] 경력 분류
    // careerInfo.pgmName = '무한도전';							//[필수]프로그램 명
    // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
    // careerInfo.year = '2018';								//[필수]년도
    // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
    // careerInfo.linesYn = 'Y';								//[필수]대사
    // memberCareerInfo.push(careerInfo);
    //
    // var careerInfo = {};
    // careerInfo.field = 'F2';								//[필수][공코 : FIELD] 경력 분류
    // careerInfo.pgmName = '1박2일';							//[필수]프로그램 명
    // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
    // careerInfo.year = '2018';								//[필수]년도
    // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
    // careerInfo.linesYn = 'N';								//[필수]대사
    // memberCareerInfo.push(careerInfo);
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    memberIdCheck: memberIdCheck,
    memberRrnCheck: memberRrnCheck,
    reHpAuth: reHpAuth,
    hpAuth : hpAuth,
    addrSearch: addrSearch,
    onAddPhotoButtonClick : onAddPhotoButtonClick,  // '사진 등록' 클릭시
    onAddVideoButtonClick : onAddVideoButtonClick,  // '영상 등록' 클릭시
    onAddCareerButtonClick: onAddCareerButtonClick, // '경력사항 등록' 클릭시
    onGoToDetailClick: onGoToDetailClick, // '상세정보 입력'클릭시
      fileAdd : fileAdd,                  // 파일 추가
      fileAttach : fileAttach,            // 파일 전송
      fileDel : fileDel,                  // 파일 삭제
  });

  // 프로그램 분야에 대한 모집 분야 코드 조회
  $scope.$watch('signupPesn.memberKind', function(newVal, oldVal, scope) {
    if (newVal === oldVal) return false;
    if (newVal !== 'M3') {
      vm.memberCompayInfo = '999';
      return false;
    }
  });

  // 아이디값 변경확인
  $scope.$watch('signupPesn.memberId', function(newVal, oldVal, scope) {
    if (newVal == oldVal) return false;
    if (vm.idDupChkFlag) {
      $message.alert('아이디 중복확인을 다시해주세요.');
      vm.idDupChkFlag = false;
    }
  });

  // 주빈번호값 변경확인
  $scope.$watch('signupPesn.memberRrn1', function(newVal, oldVal, scope) {
    if (newVal == oldVal) return false;
    if (vm.rrnDupChkFlag) {
      $message.alert('주민번호 중복확인을 다시해주세요.');
      vm.rrnDupChkFlag = false;
    }
  });
  $scope.$watch('signupPesn.memberRrn2', function(newVal, oldVal, scope) {
    if (newVal == oldVal) return false;
    if (vm.rrnDupChkFlag) {
      $message.alert('주민번호 중복확인을 다시해주세요.');
      vm.rrnDupChkFlag = false;
    }
  });  
  $scope.$watch('signupPesn.memberCompayInfo', function(newVal, oldVal, scope) {
    if (newVal == oldVal) return false;
        if (newVal !== '999') {
            var comp = _.find(vm.memberCompayInfoList, {v: newVal});
            vm.memberCompayInfoEtc = comp.i;
        }else{
            vm.memberCompayInfoEtc = '';
        }
  });

    // 프로그램 분야에 대한 프로그램 조회
    $scope.$watch('signupPesn.readfile', function(newVal, oldVal, scope) {

        if(undefined === newVal || "" === newVal){
            return false;
        } else {
            vm.companyNofileOrgName = newVal.name;
        }
        fileAttach();
    });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    // set email domain list
    vm.emailDomainOptions.push({ value: '999', text: '선택' });
    vm.emailDomainOptions.push({ value: 'naver.com', text: 'naver.com' });
    vm.emailDomainOptions.push({ value: 'hanmail.net', text: 'hanmail.net' });
    vm.emailDomainOptions.push({ value: 'gmail.com', text: 'gmail.com' });
    vm.emailDomainOptions.push({ value: 'hotmail.com', text: 'hotmail.com' });
    vm.emailDomainOptions.push({ value: 'nate.com', text: 'nate.com' });
    vm.emailDomainOptions.push({ value: '0', text: '직접입력' });

      // 은행명 공통 코드 조회
      ComSvc.selectCommonInfo('BANK_NAME')
          .then(function(data){
              vm.bankNameInfo = data.user.resultData.commonData;
          })

    // 지하철 소요시간 리스트
    vm.subwayTimeOptions = [{ value: '999', text: '소요시간 선택'}];
    for (var i = 1; i <= 12; i++) {
      vm.subwayTimeOptions.push({ value: `${i * 5}`, text: `${i * 5}분`});
    }

    MemberSvc.selectCompanyList()
      .then(function(data) {
        if (data.user.resultData.companyList.length > 0) {
          vm.memberCompayInfoList = [{ v: '999', k: '추천기업선택', i:'' }];
          _.forEach(data.user.resultData.companyList, function(obj) {
            vm.memberCompayInfoList.push({
              v: obj.COMPANY_SEQ+'',
              k: obj.COMPANY_NM,
              i: obj.COMPANY_INFO
            });
          });
        } else {
          vm.memberCompayInfoList = [{ v: '999', k: '추천기업선택', i:'' }];
          vm.memberCompayInfo = '999';
        }
      })
      .catch(function(error) {});

    ComSvc.selectCommonInfo(comConstant.GENDER)
      .then(function(data) {
        vm.memberGenderInfo = [];
        _.forEach(data.user.resultData.commonData, function(obj) {
          vm.memberGenderInfo.push({
            v: obj.COMMON_INFO_VALUE1,
            k: obj.COMMON_INFO_VALUE2
          });
        });
      })
      .catch(function(err) {});

    if ($stateParams.backupData) {
      _.assign(vm, $stateParams.backupData);
    }

    if ($stateParams.param) {
        if($stateParams.param.memberCareerInfo){
            vm.memberCareerInfo = $stateParams.param.memberCareerInfo;
        }

        if($stateParams.param.photoAttachInfo){
            vm.photoAttachInfo = $stateParams.param.photoAttachInfo;

            vm.photoAttachSeqs = '';
            _.forEach(vm.photoAttachInfo, function(obj){
                vm.photoAttachSeqs += obj.ATTACH_SEQ + '|';
            });
            if(!_.isEmpty(vm.photoAttachSeqs)){
                vm.photoAttachSeqs = vm.photoAttachSeqs.substr(0,vm.photoAttachSeqs.length -1);   //[필수]사진 첨부파일 일련번호
            }
        }
        if($stateParams.param.videoAttachInfo){
            vm.videoAttachInfo = $stateParams.param.videoAttachInfo;

            vm.videoAttachSeqs = '';
            _.forEach(vm.videoAttachInfo, function(obj){
                vm.videoAttachSeqs += obj.ATTACH_SEQ + '|';
            });
            if(!_.isEmpty(vm.videoAttachSeqs)){
                vm.videoAttachSeqs = vm.videoAttachSeqs.substr(0,vm.videoAttachSeqs.length -1);   //[선택]동영상 첨부파일 일련번호
            }
        }
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  /**
   * 아이디 중복체크
   */
  function memberIdCheck() {
    var chk = true;
    chk = chk && ngspublic.nullCheck(vm.memberId, '아이디를 입력해주세요.');
    if (!chk) return false;
      //영문하고 숫자만 들어갔는지 확인
      var regex = /^[a-zA-z][a-zA-z0-9]{5,15}$/gi;
      var input = vm.memberId.trim();
      if (!regex.test(input)) {
          $message.alert("ID는 영문자로 시작하여 숫자를 포함할 수 있으며(한글 및 특수문자 제외), 6자리 이상 16자리 이하입니다.");
          return false;
      }

    var param = { memberId: vm.memberId };

    MemberSvc.selectIdCheck(param)
      .then(function(data) {
        if (data.user.resultData.IDCHEK === 'Y') {
          $message.alert('사용할 수 없는 아이디입니다.');
          vm.idDupChkFlag = false;
        } else {
          $message.alert('사용 가능한 아이디입니다.');
          vm.idDupChkFlag = true;
        }
        logger.log('selectIdCheck[SUCCESS]', 'data: ', data);
      })
      .catch(function(error) {
        logger.log('selectIdCheck[FAILED]', 'error: ', error);
      });
  }

  /**
   * 주민번호 중복체크
   */
  function memberRrnCheck() {
    var chk = true;
    chk =
      chk &&
      ngspublic.nullCheck(
        vm.memberRrn1 + vm.memberRrn2,
        '주민번호 입력해주세요.'
      );
    if (!chk) return false;

    // 주민번호 정확성 체크
    if (!$filterSet.check_ResidentNO(vm.memberRrn1, vm.memberRrn2)) {
      $message.alert('주민번호를 정확하게 입력해주세요.');
      return false;
      // if (!$message.confirm('주민번호가 정확하지않습니다. 계속 진행하시겠습니까?(생년월일이 유효하지 않으면 등록이 안됩니다.)')) {
      //   vm.rrnDupChkFlag = false;
      //   return false;
      // }
    }

    var param = { memberRrn: vm.memberRrn1 + vm.memberRrn2 };

    MemberSvc.selectRrnCheck(param)
      .then(function(data) {
        if (data.user.resultData.RRNCHEK == 'Y') {
          $message.alert('사용할 수 없는 주민번호 입니다.');
          vm.rrnDupChkFlag = false;
        } else {
          $message.alert('사용가능한 주민번호 입니다.');
          vm.rrnDupChkFlag = true;
        }
        logger.log('selectRrnCheck[SUCCESS]', 'data: ', data);
      })
      .catch(function(error) {
        logger.log('selectRrnCheck[FAILED]', 'error: ', error);
      });
  }


    /**
     * 전화번호 재인증
     */
    function reHpAuth(){
        vm.hpAuthDis = false;
        vm.hpAuthFlag = false;
    }

    /**
     * 전화번호 인증
     */
    function hpAuth(){

        if(!vm.idDupChkFlag){
            $message.alert('아이디 중복확인을 먼저 해주세요.');
            return false;
        }
        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.memberNm, 	'이름을 입력해주세요.');
        chk = chk && ngspublic.nullCheck(vm.memberHpKind, 	'통신사를 선택해주세요.', '999');
        chk = chk && ngspublic.nullCheck(vm.memberHp,	'휴대폰번호를 입력해주세요.');
        if(!chk) return false;

        var param = {};
        param.findKind = '4';								// 1: 개인휴대폰번호찾기 2: 기업휴대폰번호찾기 3 : 기업사업자번호로대표자아이디찾기 4 : 휴대폰인증
        param.userId = vm.memberId;							// 아이디
        param.userName = vm.memberNm;
        param.userHpkind = vm.memberHpKind;					// 휴대폰회사[공코:HP_KIND]
        param.userHp = vm.memberHp; 							// 휴대폰 번호
        // param.mobileYn = 'N';

        MemberSvc.requestPwFindAuth(param)
            .then(function(data){
                logger.log('requestPwFindAuth[SUCCESS]', 'data: ', data);
                if(data.user.resultData.sEncData.length > 0){
                    var sEncData = data.user.resultData.sEncData;
                    // vm.reqSeq = data.user.resultData.reqSeq;
                    vm.hpAuthDis = true;

                    // 인증 팝업창 호출
                    $rootScope.openAuthCheckPopup(sEncData, function (data){
                        if(data.result === 'success'){
                            $message.alert('인증이 완료되었습니다. 계속 진행해주세요.');
                            vm.hpAuthFlag = true;
                        }
                    });
                }
            })
            .catch(function(error){
                vm.hpAuthDis = false;
                logger.log('requestPwFindAuth[FAILED]', 'error: ', error);
            })

    }

    /**
   * 주소검색
   */
  function addrSearch() {
    $rootScope.openAddrSearchPopup(function(result){
      logger.log('callback addrSearch() ', result);
      // result = {
      //     addrDetail: "107동 204호",
      //     admCd: "4136037021",
      //     bdKdcd: "1",
      //     bdMgtSn: "4136037021101830006006319",
      //     bdNm: "강남아파트",
      //     buldMnnm: "19",
      //     buldSlno: "0",
      //     detBdNmList: "101동,102동,103동,104동,105동,106동,107동,관리사무소,노인정",
      //     emdNm: "퇴계원면",
      //     emdNo: "01",
      //     engAddr: "19, Dojewon-ro, Toegyewon-myeon, Namyangju-si, Gyeonggi-do",
      //     inputYn: "Y",
      //     jibunAddr: "경기도 남양주시 퇴계원면 퇴계원리 221 강남아파트",
      //     liNm: "퇴계원리",
      //     lnbrMnnm: "221",
      //     lnbrSlno: "0",
      //     mtYn: "0",
      //     rn: "도제원로",
      //     rnMgtSn: "413603197019",
      //     roadAddrPart1: "경기도 남양주시 퇴계원면 도제원로 19",
      //     roadAddrPart2: "(강남아파트)",
      //     roadFullAddr: "경기도 남양주시 퇴계원면 도제원로 19, 107동 204호 (강남아파트)",
      //     sggNm: "남양주시",
      //     siNm: "경기도",
      //     udrtYn: "0",
      //     zipNo: "12123"
      // }

      vm.memberAddrNo = result.zipNo;
      vm.memberAddrBase = result.roadFullAddr;
      vm.memberAddr = `${result.zipNo} ${result.roadFullAddr}`;
    });
  }

  /**
   * 사진등록 클릭
   */
  function onAddPhotoButtonClick() {
    var param = { photoAttachInfo: vm.photoAttachInfo };
    $rootScope.goView('public.signupRegPhoto', vm, param);
  }
  /**
   * 영상등록 클릭
   */
  function onAddVideoButtonClick() {
    var param = { videoAttachInfo: vm.videoAttachInfo };
    $rootScope.goView('public.signupRegVideo', vm, param);
  }
  /**
   * 경력사항 입력 클릭
   */

  function onAddCareerButtonClick() {
    var param = { memberCareerInfo: vm.memberCareerInfo };
    $rootScope.goView('public.signupRegCareer', vm, param);
  }


  /**
   * 상세정보입력 클릭
   */
  function onGoToDetailClick() {

    /*======================================
      NULL CHECK
    ========================================*/
    var isValid = true;

    // 회원유형
    isValid = isValid && ngspublic.nullCheck(vm.memberKind, '회원유형을 선택해 주세요.', '999');
    // 기업선택 / 회사명 입력
    if (vm.memberKind === 'M2') {
      isValid = isValid && ngspublic.nullCheck(vm.memberEntercompayInfo, '소속사 정보를 입력해주세요.');
    } else if (vm.memberKind === 'M3') {
      isValid = isValid && ngspublic.nullCheck(vm.memberCompayInfo, '추천기업을 선택해주세요.', '999');
    }
    // 이름
    isValid = isValid && ngspublic.nullCheck(vm.memberNm, '이름을 입력해주세요.');
    // 아이디
    isValid = isValid && ngspublic.nullCheck(vm.memberId, '아이디를 입력해주세요.');
    // 비밀번호
    isValid = isValid && ngspublic.nullCheck(vm.memberPw, '비밀번호를 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.memberPwChk, '비밀번호 확인을 입력해주세요.');
    if (isValid && vm.memberPw !== vm.memberPwChk) {
      $message.alert('비밀번호가 일치하지 않습니다.');
      return false;
    }
    // 성별
    isValid = isValid && ngspublic.nullCheck(vm.memberGender, '성별을 선택해주세요.');
    // 주민등록번호
    isValid = isValid && ngspublic.nullCheck(vm.memberRrn1, '주민등록번호를 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.memberRrn2, '주민등록번호를 입력해주세요.');
    // 주소
    isValid = isValid && ngspublic.nullCheck(vm.memberAddr, '거주지를 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.memberSubwayLines, '가까운 지하철호선을 입력해주세요.', '999');
    isValid = isValid && ngspublic.nullCheck(vm.memberSubwayName, '가까운 지하철역을 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.memberSubwayTime, '가까운 지하철역 소요시간을 입력해주세요.', '999');
    // 연락처
    isValid = isValid && ngspublic.nullCheck(vm.memberHpKind, '통신사를 선택해주세요.', '999');
    isValid = isValid && ngspublic.nullCheck(vm.memberHp, '전화번호를 입력해주세요.');
    // 키/몸무게
    isValid = isValid && ngspublic.nullCheck(vm.height, '키를 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.weight, '몸무게를 입력해주세요.');
    // 이메일
    isValid = isValid && ngspublic.nullCheck(vm.emailId, '이메일을 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.emailDomainId, '이메일을 입력해주세요.', '999');
    if (isValid && vm.emailDomainId === '0' && vm.emailDomain === '') {
      $message.alert('이메일을 입력해주세요.');
      return false;
    }

    // 사진 첨부 여부
//    isValid = isValid && ngspublic.nullCheck(vm.photoAttachSeqs, '사진을 첨부해주세요.');


    if (!isValid) return false;

    /*======================================
      중복확인
    ========================================*/
    if (!vm.idDupChkFlag) {
      $message.alert('아이디 중복확인을 해주세요.');
      return false;
    }

    if (!vm.rrnDupChkFlag) {
      $message.alert('주민등록번호 중복확인을 해주세요.');
      return false;
    }

    if (!vm.hpAuthFlag) {
      $message.alert('전화번호 인증을 해주세요.');
      return false;
    }

    /*======================================
      데이터 가공
    ========================================*/
    var memberRrn = `${vm.memberRrn1}${vm.memberRrn2}`;
    var memberHp = vm.memberHp.replace(/-/g, '');
    var email = `${vm.emailId}@${vm.emailDomainId === '0' ? vm.emailDomain : vm.emailDomainId}`;
    var accNo = vm.accNo.replace(/-/g, '');
    var tempPushType = '';
    if(window.env.platform == 'android'){
      tempPushType = 'P1';
    }else if(window.env.platform == 'ios'){
      tempPushType = 'P2';
    }

    /*======================================
      파라미터 생성
    ========================================*/
    var memberBaseInfo = {
      pushId: $rootScope.UUID,
      pushType: tempPushType,
      memberKind: vm.memberKind, // [필수] 회원유형
      memberEntercompayInfo: vm.memberEntercompayInfo || '', // 소속사 (회원유형 'M2')
      memberCompayInfo: vm.memberCompayInfo || '', // 추천기업 (회원유형 'M3')
      memberNm: vm.memberNm, // [필수] 이름
      memberId: vm.memberId, // [필수] 아이디
      memberPw: vm.memberPw, // [필수] 비밀번호
      memberGender: vm.memberGender, // [필수] 성별
      memberRrn, // [필수] 주민등록 번호
      memberAddr: vm.memberAddr, // [필수] 기본주소
      memberSubwayLines : vm.memberSubwayLines, //[필수]가까운지하철호선
      memberSubwayName : vm.memberSubwayName,   //[필수]가까운지하철
      memberSubwayTime : vm.memberSubwayTime,   //[필수]소요시간
      memberHpKind: vm.memberHpKind, // [필수] 연락처 - 통신사
      recomMemId: vm.recomMemId, // [선택] 추천인 ID
      memberHp, // [필수] 연락처
      height: vm.height, // [필수] 키
      weight: vm.weight, // [필수] 몸무게
      email, // [필수] 이메일
      accBankName: vm.accBankName === '999' ? '' : _.find(vm.bankNameInfo, {COMMON_INFO_VALUE1 : vm.accBankName}).COMMON_INFO_VALUE2, // [선택] 계좌번호 - 은행
      accNo, // [선택] 계좌번호 - 계좌번호
      accName: vm.accName, // [선택] 계좌번호 - 예금주
      companyNoAttachSeqs: vm.companyNoAttachSeqs, // [선택] 사업자등록증 첨부
      photoAttachSeqs: vm.photoAttachSeqs, // [필수] 사진 첨부
      videoAttachSeqs: vm.videoAttachSeqs // [선택] 동영상 첨부
    };

    var param = {
      memberBaseInfo,
      memberCareerInfo: vm.memberCareerInfo
    };

    $rootScope.goView('public.signupPesnDetail', vm, param);
  }

    /**
     * 파일 추가
     */
    function fileAdd(){
        angular.element('#file').click();
    }

    /**
     * 파일 삭제
     */
    function fileDel(){

        var chk = ngspublic.nullCheck(vm.companyNoAttachSeqs,       '파일정보가 없습니다.');
        if(!chk){
            $('#file').val('');
            vm.companyNoAttachSeqs = '';
            vm.companyNofileDisabled = false;
            return false;
        }

        var confirmResult = $message.confirm('정말 삭제하시겠습니까?');
        if(!confirmResult) return false;

        var param = {};

        vm.companyNofileDisabled = false;
        $("#file").val("");
        param.attachSeq = vm.companyNoAttachSeqs;     // [필수]파일등록 후 결과값에 파일 일련번호
        param.fileOrgName = vm.companyNofileOrgName;       // [필수]파일등록 후 결과값에 파일 원본파일이름

        ComSvc.fileDelete(param)
            .then(function(data){
                $message.alert('정상적으로 삭제되었습니다.');

                vm.companyNoAttachSeqs = '';
                vm.companyNofileOrgName = '';
                vm.readfile = '';

                $("#form").get(0).reset();
            })
            .catch(function(err){
                logger.error('message: ' + err + '\n');
            })
    }

    /**
     * 파일전송
     */
    function fileAttach(){
        vm.companyNofileDisabled = true;

        $rootScope.safeApply(function() {
            $rootScope.requestCount++;
        });

        $("#form").ajaxForm({
            url : $env.endPoint.service + "/api/com/fileUpload.login",
            enctype : "multipart/form-data",
            dataType : "json",
            beforeSubmit: function (data, frm, opt) {
                var objFind = _.find(data, {type : 'file'});
                if(objFind){
                    if(!_.isNull(objFind.value) && objFind.value != ""){
                        return true;
                    }
                }

                $rootScope.requestCount--;
                vm.companyNofileDisabled = false;
                $message.alert('파일 선택 후 진행 해주세요.');
                return false;
            },
            error : function(){
                $scope.$apply(function(){

                    vm.companyNofileDisabled = false;
                    vm.companyNofileOrgName = '';

                    $rootScope.requestCount--;
                    $message.alert("파일 전송 실패") ;
                });
            },
            success : function(result){
                $scope.$apply(function(){
                    $rootScope.requestCount--;

                    if(result.user.result == 'Fail'){
                        $message.alert(result.user.resultMessage);
                        return false;
                    }

                    vm.companyNoAttachSeqs = result.user.resultData.attachSeq;
                    vm.companyNofileOrgName = result.user.resultData.orgName;

                    $message.alert("파일 전송 성공");
                });
            }
        });

        $("#form").submit();
    }
}
