angular
    .module('easi.public')
    .controller('signupCompCtrl', signupCompCtrl);

function signupCompCtrl($log, $rootScope, $scope, $state, $timeout, $user, 
                        $message, $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, 
                        $cordovaGeolocation, $cordovaNetwork, MemberSvc, ngspublic, ComSvc, $filterSet, $env) {
    /**
     * Private variables
     */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
      
          bankNameInfo : [],          // 은행정보
          emailDomainOptions: [], // 이메일 도메인 리스트
          
          idDupChkFlag : false,         // 아이디 중복확인 결과
          
          companyNm : '',
          companyCeoNm : '',
          companyMemberName :'',
          companyCeoHpKind : '999',
          companyCeoHp : '',
          companyNo : '',
          companyMemberId : '',
          companyMemberPwd : '',
          companyMemberPwdChk : '',
          companyTel1 : '',
          companyTel2 :'',
          companyEmail1 : '',
          companyEmail2 : '999',
          companyEmail2Etc : '',
          accBankName : '999',
          accNo : '',
          accName : '',
          
          photofileDisabled1 : false,     // 사진 첨부파일 1 파일선택가능
          photopgmImgAttachSeq1 : '',     // 사진 첨부파일 1 일련번호
          photofileOrgName1 : '',       // 사진 첨부파일 1 원본파일이름
          readfile1 : '',               // 파일 1

          photofileDisabled2 : false,     // 사진 첨부파일 2 파일선택가능
          photopgmImgAttachSeq2 : '',     // 사진 첨부파일 2 일련번호
          photofileOrgName2 : '',       // 사진 첨부파일 2 원본파일이름
          readfile2 : '',               // 파일 2

          photofileDisabled3 : false,     // 사진 첨부파일 3 파일선택가능
          photopgmImgAttachSeq3 : '',     // 사진 첨부파일 3 일련번호
          photofileOrgName3 : '',       // 사진 첨부파일 3 원본파일이름
          readfile3 : '',               // 파일 3

          photofileDisabled4 : false,     // 사진 첨부파일 4 파일선택가능
          photopgmImgAttachSeq4 : '',     // 사진 첨부파일 4 일련번호
          photofileOrgName4 : '',       // 사진 첨부파일 4 원본파일이름
          readfile4 : '',                // 파일 4
          
          photofileDisabled5 : false,     // 사진 첨부파일 5 파일선택가능
          photopgmImgAttachSeq5 : '',     // 사진 첨부파일 5 일련번호
          photofileOrgName5 : '',       // 사진 첨부파일 5 원본파일이름
          readfile5 : ''                // 파일 5
      
    });

    /**
     * Public methods
     */
    _.assign(vm, {
      memberIdCheck : memberIdCheck,      // 아이디 체크
      insertCom : insertCom,              // 등록
      fileAdd : fileAdd,                  // 파일 추가
      fileAttach : fileAttach,            // 파일 전송
      fileDel : fileDel,                  // 파일 삭제
    });
    
    // 프로그램 분야에 대한 프로그램 조회
    $scope.$watch('signupComp.readfile1', function(newVal, oldVal, scope) {
        
      if(undefined === newVal || "" === newVal){
            return false;
        } else {
            vm.photofileOrgName1 = newVal.name;
        }
        fileAttach('file1');
    });

    $scope.$watch('signupComp.readfile2', function(newVal, oldVal, scope) {
        if(undefined === newVal || "" === newVal){
          return false;
        } else {
            vm.photofileOrgName2 = newVal.name;
        }
        fileAttach('file2');
    });

    $scope.$watch('signupComp.readfile3', function(newVal, oldVal, scope) {
        if(undefined === newVal || "" === newVal){
          return false;
        } else {
            vm.photofileOrgName3 = newVal.name;
        }
        fileAttach('file3');
    });

    $scope.$watch('signupComp.readfile4', function(newVal, oldVal, scope) {
        if(undefined === newVal || "" === newVal){
          return false;
        } else {
            vm.photofileOrgName4 = newVal.name;
        }
        fileAttach('file4');
    });
    
    $scope.$watch('signupComp.readfile5', function(newVal, oldVal, scope) {
      if(undefined === newVal || "" === newVal){
        return false;
      } else {
          vm.photofileOrgName5 = newVal.name;
      }
      fileAttach('file5');
    });

    /**
     * Initialize
     */
    
    
    /**
     * 파일 추가
     */
    function fileAdd(kind){
        angular.element('#'+kind).click();
    }

    /**
     * 파일 삭제
     */
    function fileDel(kind){
      
        var chk = ngspublic.nullCheck(vm['photopgmImgAttachSeq' + kind.replace('file','')],       '파일정보가 없습니다.');
        if(!chk){
            $('#'+kind).val('');
            vm['photopgmImgAttachSeq' + kind.replace('file','')] = '';
            vm['photofileDisabled' + kind.replace('file','')] = false;
            return false;
        }

        var confirmResult = $message.confirm('정말 삭제하시겠습니까?');
        if(!confirmResult) return false;

        var param = {};
        
        vm['photofileDisabled' + kind.replace('file','')] = false;
        $("#"+kind).val("");
        param.attachSeq = vm['photopgmImgAttachSeq' + kind.replace('file','')];     // [필수]파일등록 후 결과값에 파일 일련번호
        param.fileOrgName = vm['photofileOrgName' + kind.replace('file','')];       // [필수]파일등록 후 결과값에 파일 원본파일이름
        
        ComSvc.fileDelete(param)
            .then(function(data){
                $message.alert('정상적으로 삭제되었습니다.');
                
                vm['photopgmImgAttachSeq' + kind.replace('file','')] = '';
                vm['photofileOrgName' + kind.replace('file','')] = '';
                vm['readfile' + kind.replace('file','')] = '';
                
                $("#form"+kind).get(0).reset();
            })
            .catch(function(err){
                logger.error('message: ' + err + '\n');
            })
    }

    /**
     * 파일전송
     */
    function fileAttach(kind){
        vm['photofileDisabled' + kind.replace('file','')] = true;

        $rootScope.safeApply(function() {
          $rootScope.requestCount++;
        });
        
        $("#form"+kind).ajaxForm({
            url : $env.endPoint.service + "/api/com/fileUpload.login",
            enctype : "multipart/form-data",
            dataType : "json",
            beforeSubmit: function (data, frm, opt) {
                var objFind = _.find(data, {type : 'file'});
                if(objFind){
                    if(!_.isNull(objFind.value) && objFind.value != ""){
                        return true;
                    }
                }
                
                $rootScope.requestCount--;
                vm['photofileDisabled' + kind.replace('file','')] = false;
                $message.alert('파일 선택 후 진행 해주세요.');
                return false;
            },
            error : function(){
                $scope.$apply(function(){
                  
                    vm['photofileDisabled' + kind.replace('file','')] = false;
                    vm['photofileOrgName' + kind.replace('file','')] = '';
                  
                    $rootScope.requestCount--;
                    $message.alert("파일 전송 실패") ;
                });
            },
            success : function(result){
                $scope.$apply(function(){
                    $rootScope.requestCount--;
                    
                    if(result.user.result == 'Fail'){
                        $message.alert(result.user.resultMessage);
                        return false;
                    }
                    
                    vm['photopgmImgAttachSeq' + kind.replace('file','')] = result.user.resultData.attachSeq;
                    vm['photofileOrgName' + kind.replace('file','')] = result.user.resultData.orgName;

                    $message.alert("파일 전송 성공");
                });
            }
        });

        $("#form"+kind).submit();
    }

    
    init();

    function init() {
        logger.debug('init', vm);
        
        vm.emailDomainOptions = [
                                 { value: '999', text: '선택' },
                                 { value: 'naver.com', text: 'naver.com' },
                                 { value: 'hanmail.net', text: 'hanmail.net' },
                                 { value: 'gmail.com', text: 'gmail.com' },
                                 { value: 'hotmail.com', text: 'hotmail.com' },
                                 { value: 'nate.com', text: 'nate.com' },
                                 { value: '0', text: '직접입력' }
                               ];
        
        
        // 은행명 공통 코드 조회
        ComSvc.selectCommonInfo('BANK_NAME')
          .then(function(data){
            vm.bankNameInfo = data.user.resultData.commonData;
          })
        
        
        

        $scope.$on('$destroy', destroy);


    }

    /**
     * Event handlers
     */
    function destroy(event) {
        logger.debug(event);
    }

    /**
     * Custom functions
     */
    
    // 아이디값 변경확인
    $scope.$watch('signupComp.companyMemberId', function(newVal, oldVal, scope) {
      if (newVal == oldVal) return false;
      if (vm.idDupChkFlag) {
        $message.alert('아이디 중복확인을 다시해주세요.');
        vm.idDupChkFlag = false;
      }
    });
    
    
    /**
     * 아이디 중복체크
     */
    function memberIdCheck() {
      var chk = true;
      chk = chk && ngspublic.nullCheck(vm.companyMemberId, '아이디를 입력해주세요.');
      if (!chk) return false;

      var param = { memberId: vm.companyMemberId };

      MemberSvc.selectIdCheck(param)
        .then(function(data) {
          if (data.user.resultData.IDCHEK === 'Y') {
            $message.alert('사용할 수 없는 아이디입니다.');
            vm.idDupChkFlag = false;
          } else {
            $message.alert('사용 가능한 아이디입니다.');
            vm.idDupChkFlag = true;
          }
          logger.log('selectIdCheck[SUCCESS]', 'data: ', data);
        })
        .catch(function(error) {
          logger.log('selectIdCheck[FAILED]', 'error: ', error);
        });
    }
    
    
    /**
     * 등록
     */
    function insertCom(){
     
      var chk = true;
      
      chk = chk && ngspublic.nullCheck(vm.companyNm,       '업체명을 입력해주세요.');
      chk = chk && ngspublic.nullCheck(vm.companyCeoNm,      '대표자명을 입력해주세요.');
      chk = chk && ngspublic.nullCheck(vm.companyCeoHpKind,    '대표자 연락처 회사 종류를 선택해주세요.', '999');
      chk = chk && ngspublic.nullCheck(vm.companyCeoHp,      '대표자 연락처를 입력해주세요.');
      chk = chk && ngspublic.nullCheck(vm.companyMemberName,   '담당자명을 입력해주세요.');
      chk = chk && ngspublic.nullCheck(vm.companyNo,       '사업자번호를 입력해주세요.');
      chk = chk && ngspublic.nullCheck(vm.companyMemberId,     '아이디를 입력해주세요.');
      if(chk && vm.companyMemberId.length <= 4){
        $message.alert('아이디를 4자 이상입력해주세요.');
        return false;
      }
      chk = chk && ngspublic.nullCheck(vm.companyMemberPwd,    '패스워드를 입력해주세요.');
      if(chk && vm.companyMemberPwd.length <= 4){
        $message.alert('패스워드를 4자 이상입력해주세요.');
        return false;
      }
      if(chk && vm.companyMemberPwd != vm.companyMemberPwdChk){
        $message.alert('패스워드확인이 정확하지않습니다.');
        return false;
      }
      chk = chk && ngspublic.nullCheck(vm.companyTel1,       '연락처1을 입력해주세요.');
      chk = chk && ngspublic.nullCheck(vm.companyEmail1,      '이메일을 입력해주세요.');
      if(vm.companyEmail2 != '0'){
        chk = chk && ngspublic.nullCheck(vm.companyEmail2,      '이메일을 입력해주세요.');
      }else{
        chk = chk && ngspublic.nullCheck(vm.companyEmail2Etc,      '이메일을 입력해주세요.');
      }
      if(!chk) return false;
      
      
      // 중복확인 결과
      if(!vm.idDupChkFlag){
        $message.alert('아이디 중복확인을 해주세요.');
        return false;
      }
      
      // 이메일 체크
      var tempEmail = '';
      if(vm.companyEmail != ''){
        if(vm.companyEmail2 != '0'){
          tempEmail = vm.companyEmail1 + '@' + vm.companyEmail2;
        }else{
          tempEmail = vm.companyEmail1 + '@' + vm.companyEmail2Etc;
        }
        if(!$filterSet.isEmail(tempEmail)){
          $message.alert('이메일을 정확하게 입력해주세요.');
          return false;
        }
      }
      
      var tempPushType = '';
      if(window.env.platform == 'android'){
        tempPushType = 'P1';
      }else if(window.env.platform == 'ios'){
        tempPushType = 'P2';
      }
      
      var param = {};
      param.pushId = $rootScope.UUID;
      param.pushType = tempPushType;
      param.companyNm = vm.companyNm;                         // [필수] 회사명
      param.companyCeoNm = vm.companyCeoNm;                       // [필수] 대표자명
      param.companyCeoHpKind = vm.companyCeoHpKind == '999' ? '' : vm.companyCeoHpKind; // [필수][공코 :HP_KIND] 대표자 휴대폰 회사
      param.companyCeoHp = vm.companyCeoHp;                       // [필수][- 제거] 대표자 휴대폰 번호 
      param.companyNo = vm.companyNo;                         // [필수][- 제거] 회사 사업자번호
      param.companyMemberName = vm.companyMemberName;                 // [필수] 담당자명
      param.companyTel1 = vm.companyTel1;                       // [필수] 연락처1
      param.companyTel2 = vm.companyTel2;                       // [선택] 연락처2
      param.companyEmail = tempEmail;                       // [필수] 회사(담당자) 이메일 주소 
      param.companyNoAttachSeq = vm.photopgmImgAttachSeq1;                 // [선택] 사업자등록증 첨부일련번호
      param.companyAccAttachSeq = vm.photopgmImgAttachSeq2;               // [선택] 통장사본 첨부일련번호
      param.companyPermissionAttachSeq = vm.photopgmImgAttachSeq3;         // [선택] 대중문화예술기획업 허가증 첨부일련번호
      param.companyJobProveAttachSeq = vm.photopgmImgAttachSeq4;           // [선택] 직업소개소 증빙자료 첨부일련번호
      param.companyEtcAttachSeq = vm.photopgmImgAttachSeq5;               // [선택] 기타 증빙서류 첨부일련번호
      param.accNo = vm.accNo;                             // [선택][- 제거]계좌번호
      param.accName = vm.accName;                           // [선택]예금주
      param.accBankName = vm.accBankName == '999' ? '' : _.find(vm.bankNameInfo, {COMMON_INFO_VALUE1 : vm.accBankName}).COMMON_INFO_VALUE2;       // [선택][공코 : BANK_NAME] 은행명
      param.companyMemberId = vm.companyMemberId;                   // [필수]아이디
      param.companyMemberPwd = vm.companyMemberPwd;                   // [필수]패스워드

      
      MemberSvc.insertCompanyInfo(param)
      .then(function(data) {
        $message.alert('정상적으로 등록되었습니다.');
        $rootScope.goView('public.html#/login');
        logger.log('insertCompanyInfo[SUCCESS]', 'data: ', data);
      })
      .catch(function(error) {
        logger.log('insertCompanyInfo[FAILED]', 'error: ', error);
      });
      
    }

}