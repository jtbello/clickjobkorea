angular
  .module('easi.public')
  .controller('testCtrl', testCtrl);

function testCtrl($log, $rootScope, $scope, $state, $timeout, $user, $message, $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;
  /**
   * Public variables
   */
  _.assign(vm, {
      states : [
        { name : '로그인' , state : 'public.login' , controller : 'login'},
        { name : '개인회원가입' , state : 'public.signupClientPrimary', controller : 'signupClientPrimary' },
        { name : '개인회원가입2' , state : 'public.signupClientCareer', controller : 'signupClientCareer' },
        { name : '기업회원가입' , state : 'public.signupCompany',  controller : 'signupCompany' },
        { name : '아이디 찾기' , state : 'public.findAccount' , controller : 'findAccount' },
        { name : '아이디 찾기 결과', state : 'public.findAccountResult' , controller : 'findAccountResult' },
        { name : '비밀번호 찾기', state : 'public.findPW' , controller : 'findPW' },
        { name : '비밀번호 찾기 결과', state : 'public.findPWChange' , controller : 'findPWChange' },
        { name : '경력 등록', state : 'public.registCareer' , controller : 'registCareer' },
        { name : '사진 등록', state : 'public.registImages' , controller : 'registImages' },
        { name : '영상 등록', state : 'public.registVideos' , controller : 'registVideos' },
        { name : '출연자 신청서 목록', state : 'public.guestAppicationList' , controller : 'guestAppList' },
        { name : '출연자 문의하기 목록 ', state : 'public.guestInquiryList' , controller : 'guestInquiryList' },
        { name : '출연자 문의하기 작성', state : 'public.guestInquiryForm' , controller : 'guestInquiryForm' },
        { name : '출연자 문의하기 보기', state : 'public.guestInquiryDetail' , controller : 'guestInquiryDetail' },
        { name : '출연자 메인', state : 'public.guestMain' , controller : 'guestMain' },
        { name : '출연자 프로그램 보기', state : 'public.guestProgramDetail' , controller : 'guestProgramDetail' },
        
    ]
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      move : function(state){
          
          $state.go(state);
      }
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    console.log('Hello');
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  

}