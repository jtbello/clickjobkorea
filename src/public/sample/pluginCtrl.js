angular
  .module('easi.public')
  .controller('pluginCtrl', pluginCtrl);

function pluginCtrl($log, $rootScope, $scope, $state, $timeout, $user, $message, $window, $q, $storage, MetaSvc, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork, ComSvc, comConstant, $cordovaActionSheet, $cordovaCamera) {
  var logger = $log(this);
  var vm = this;


  /**
   * Public variables
   */
  _.assign(vm, {
    aa : "198402251634342",
      imgPath : "",
      test : "",
      agreeEndDate : ""
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    sendHttp : sendHttp,
    toast : toast,
    picker : picker,
      inAppBrowser : inAppBrowser,
    location: location,
    netInfo: netInfo,
      setData : setData,
      getData : getData,
      removeData : removeData,
      actionSheet : actionSheet,
      takePicture : takePicture,
      getPicture : getPicture,
      setAlarm : setAlarm,
      searchAddr : searchAddr
  });
  

  init();

  function init() {

    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    MetaSvc.getBizCode(comConstant.CP_USE_USER).then(function (data) {
        logger.debug('comConstant.CP_USE_USER : ', data);
    });
    
  }

  function destroy(event) {
    logger.debug(event);
  }

  function sendHttp(){
    // var param = {
    //         url : 'https://xsfa.abllife.co.kr/qu/vod/getCategory',
    //         data : {},
    //         options: { headers : {'Accept' : 'application/json, text/plain, */*', 'Content-Type' : 'application/json'}}
    // };
    // $advHttp.advPost(param)
    //   .then(function(data){
    //     logger.log('success');
    //     logger.log(data);
    //   })
    //   .catch(function(error){
    //     logger.log('fail');
    //     logger.log(error);
    //   });
      ComSvc.selectCommonInfo(comConstant.HP_KIND)
          .then(function(data){
              $message.alert(JSON.stringify(data));
          });
  }
  
  function toast(){
    var options = {
            message: "message",
            duration: "type", // "short" which is 2000 ms. "long" is 4000. Or specify the nr of ms yourself. 
            position: "bottom",
//            addPixelsY: 0,  // added a negative value to move it up a bit (default 0)
//            styling: {
//              opacity: 0.75, // 0.0 (transparent) to 1.0 (opaque). Default 0.8 
//              backgroundColor: '#FF0000', // make sure you use #RRGGBB. Default #333333 
//              textColor: '#FFFF00', // Ditto. Default #FFFFFF 
//              textSize: 20.5, // Default is approx. 13. 
//              cornerRadius: 16, // minimum is 0 (square). iOS default 20, Android default 100 
//              horizontalPadding: 20, // iOS default 16, Android default 50 
//              verticalPadding: 16 // iOS default 12, Android default 30 
//            }
          }
    $cordovaToast.showShortBottom('토스트다');
  }
  
  function picker(){
    $doryPicker.datePicker(new Date())
      .then(function(data){
        logger.log('success');
        logger.log(data);
      })
      .catch(function(error){
        logger.log('fail');
        logger.log(error);
      });
  }
  
  function inAppBrowser(){
      var options = {
          location: 'yes',
          // clearcache: 'yes',
          // toolbar: 'no'
      };

    $cordovaInAppBrowser.open('http://www.naver.com', '_blank', options);
  }
  
  function location(){
      var posOptions = {timeout: 10000, enableHighAccuracy: false};

    $cordovaGeolocation.getCurrentPosition(posOptions)
      .then(function(position){
        logger.log('success');
        logger.log('Latitude: '          + position.coords.latitude          + '\n' +
                'Longitude: '         + position.coords.longitude         + '\n' +
                'Altitude: '          + position.coords.altitude          + '\n' +
                'Accuracy: '          + position.coords.accuracy          + '\n' +
                'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
                'Heading: '           + position.coords.heading           + '\n' +
                'Speed: '             + position.coords.speed             + '\n' +
                'Timestamp: '         + position.timestamp                + '\n');
      })
      .catch(function(error){
        logger.log('fail');
        logger.error('code: '    + error.code    + '\n' +
                'message: ' + error.message + '\n');
      });
  }
  
  function netInfo(){
    var networkState = $cordovaNetwork.getNetwork();

    var states = {};
    states[Connection.UNKNOWN]  = 'Unknown connection';
    states[Connection.ETHERNET] = 'Ethernet connection';
    states[Connection.WIFI]     = 'WiFi connection';
    states[Connection.CELL_2G]  = 'Cell 2G connection';
    states[Connection.CELL_3G]  = 'Cell 3G connection';
    states[Connection.CELL_4G]  = 'Cell 4G connection';
    states[Connection.CELL]     = 'Cell generic connection';
    states[Connection.NONE]     = 'No network connection';

    logger.debug('Connection type: ' + states[networkState]);
  }

  function setData(){

      var key = 'saveData';
      var value = '세이브다!';

      $storage.set(key, value);
  }

  function getData(){
      var key = 'saveData';
      $storage.get(key)
          .then(function(value){
              $message.alert(value);
          })
          .catch(function(error){
              $message.alert(error);
          });
  }

  function removeData(){
      var key = 'saveData';
      $storage.remove(key);
  }

  function actionSheet(){
      var options = {
          title: '이미지 불러오기',
          buttonLabels: ['사진 가져오기'],
          addCancelButtonWithLabel: '취소',
          androidEnableCancelButton : true,
          winphoneEnableCancelButton : true,
          addDestructiveButtonWithLabel : '카메라 촬영'
      };
      $cordovaActionSheet.show(options)
          .then(function(btnIndex) {
              var index = btnIndex;
              if(index == 1){
                  takePicture()
              }else if(index == 2){
                  getPicture()
              }
          });
  }

  function takePicture(){
      // base64 버전
      var options = {
          quality: 50,
          destinationType: Camera.DestinationType.DATA_URL,
          sourceType: Camera.PictureSourceType.CAMERA,
          allowEdit: true,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 100,
          targetHeight: 100,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false,
          correctOrientation:true
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {
          vm.imgPath = "data:image/jpeg;base64," + imageData;
      }, function(err) {
          // error
      });
      //
      // // 파일 URI 버전
      // var options = {
      //     destinationType: Camera.DestinationType.FILE_URI,
      //     sourceType: Camera.PictureSourceType.CAMERA,
      // };
      //
      // $cordovaCamera.getPicture(options).then(function(imageURI) {
      //     vm.imgPath = imageURI;
      // }, function(err) {
      //     // error
      // });
      //
      //
      // $cordovaCamera.cleanup().then(function(){
      //
      // }); // only for FILE_URI

  }

  function getPicture(){
// base64 버전
      var options = {
          quality: 50,
          destinationType: Camera.DestinationType.DATA_URL,
          sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
          allowEdit: true,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 100,
          targetHeight: 100,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false,
          correctOrientation:true
      };
      $cordovaCamera.getPicture(options).then(function(imageData) {
          vm.imgPath = "data:image/jpeg;base64," + imageData;
      }, function(err) {
          // error
      });
      //
      // // 파일 URI 버전
      // var options = {
      //     destinationType: Camera.DestinationType.FILE_URI,
      //     sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
      // };
      //
      // $cordovaCamera.getPicture(options).then(function(imageURI) {
      //     vm.imgPath = imageURI;
      // }, function(err) {
      //     // error
      // });
      //
      //
      // $cordovaCamera.cleanup().then(function(){
      //
      // }); // only for FILE_URI
  }

  function setAlarm(sec){
      var now = new Date().getTime();
      var _10SecondsFromNow = new Date(now + sec * 1000);
      // $cordovaLocalNotification.schedule({
      //     id: 1,
      //     title: 'Title here',
      //     text: 'Hello , ' + sec + 'sec ago',
      //     at: _10SecondsFromNow
      // }).then(function (result) {
      //     $message.alert(result + ' 설정 완료');
      // });
  }

  // 현재 주소 가져오기
  function searchAddr(){
      $rootScope.searchAddrByGeocode()
          .then(function(data){
              // if(data.total > 0){
              //    data.items[0].address    // 현재 주소
              //    data.items[0].addrDetail    // 현재 주소
              // }
              // addrdetail.sido + addrdetail.sigugun + addrdetail.dongmyun
              //
              logger.debug('searchAddrByGeocode', data);
          }).catch(function(err){
          logger.debug('searchAddrByGeocode err', err);
      });
  }
}