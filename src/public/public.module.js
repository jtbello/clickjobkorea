/* jshint browser:true */

document.addEventListener('deviceready', bootstrap, false);

angular
  .element(document)
  .ready(function() {
    // if (!env.cordova) {
    //   bootstrap();
    // }
  });

function bootstrap() {
  angular.bootstrap(document, ['easi.public']);
}

angular
  .module('easi.public', ['easi.common'])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('public', {
      url: '',
      abstract: true,
      template: '<ui-view/>',
      resolve: {
        Env: function($env) {
          return $env.get();
        }
      }
    })
    // .state('public.login', {
    //   url: '/login',
    //   templateUrl: 'login/login.tpl.html',
    //   controller: 'loginCtrl as login'
    // })
    // 플러그인 테스트 페이지
    .state('public.plugin' , {
      url : '/plugin',
      templateUrl : 'sample/plugin.tpl.html',
      controller : 'pluginCtrl as plugin'
    })
    // YKS
    // 테스트 페이지
    .state('public.test' , {
      url : '/test',
      templateUrl : 'member/test.tpl.html',
      controller : 'testCtrl as test'
    })
    .state('public.login' , {
        url : '/login',
        templateUrl : 'member/login/login.tpl.html',
        data: { menuTitle : '로그인', menuId : 'login', depth : 0},
        params: { param: null, backupData: null },
        controller: 'loginCtrl as login'
    })
      .state('public.findID' , {
        url : '/findID',
        templateUrl : 'member/find/findID/findID.tpl.html',
        data: { menuTitle : '아이디 찾기', menuId : 'findID', depth : 2},
        params: { param: null, backupData: null },
        controller: 'findIDCtrl as findID'
      })
      .state('public.findIDInfo' , {
        url : '/findIDInfo',
        templateUrl : 'member/find/findIDInfo/findIDInfo.tpl.html',
        data: { menuTitle : '아이디 찾기', menuId : 'findIDInfo', depth : 3},
        params: { param: null, backupData: null },
        controller: 'findIDInfoCtrl as findIDInfo'
      })
      .state('public.findPW' , {
        url : '/findPW',
        templateUrl : 'member/find/findPW/findPW.tpl.html',
        data: { menuTitle : '비밀번호 찾기', menuId : 'findPW', depth : 2},
        params: { param: null, backupData: null },
        controller: 'findPWCtrl as findPW'
      })
      .state('public.findPWChange' , {
        url : '/findPWChange',
        templateUrl : 'member/find/findPWChange/findPWChange.tpl.html',
        data: { menuTitle : '비밀번호 찾기', menuId : 'findPWChange', depth : 3},
        params: { param: null, backupData: null },
        controller: 'findPWChangeCtrl as findPWChange'
      })
    .state('public.signupComp' , {
      url : '/signupComp',
      templateUrl : 'member/join/signupComp/signupComp.tpl.html',
      data: { menuTitle : '기업회원 회원가입 요청', menuId : 'signupComp', depth : 2},
      params: { param: null, backupData: null },
      controller : 'signupCompCtrl as signupComp'
    })
    .state('public.signupPesn' , {
        url : '/signupPesn',
        templateUrl : 'member/join/signupPesn/signupPesn.tpl.html',
        data: { menuTitle : '개인회원 회원가입 요청', menuId : 'signupPesn', depth : 2},
        params: { param: null, backupData: null },
        controller : 'signupPesnCtrl as signupPesn'
    })
      .state('public.signupPesnDetail' , {
          url : '/signupPesnDetail',
          templateUrl : 'member/join/signupPesnDetail/signupPesnDetail.tpl.html',
          data: { menuTitle : '개인회원 회원가입 요청 상세', menuId : 'signupPesnDetail', depth : 3},
          params: { param: null, backupData: null },
          controller : 'signupPesnDetailCtrl as signupPesnDetail'
      })
      .state('public.signupRegCareer' , {
          url : '/signupRegCareer',
          templateUrl : 'member/join/signupRegCareer/signupRegCareer.tpl.html',
          data: { menuTitle : '경력 등록/수정', menuId : 'signupRegCareer', depth : 3},
          params: { param: null, backupData: null },
          controller : 'signupRegCareerCtrl as signupRegCareer'
      })
      .state('public.signupRegPhoto' , {
          url : '/signupRegPhoto',
          templateUrl : 'member/join/signupRegPhoto/signupRegPhoto.tpl.html',
          data: { menuTitle : '사진 등록/수정', menuId : 'signupRegPhoto', depth : 3},
          params: { param: null, backupData: null },
          controller : 'signupRegPhotoCtrl as signupRegPhoto'
      })
      .state('public.signupRegVideo' , {
          url : '/signupRegVideo',
          templateUrl : 'member/join/signupRegVideo/signupRegVideo.tpl.html',
          data: { menuTitle : '영상 등록/수정', menuId : 'signupRegVideo', depth : 3},
          params: { param: null, backupData: null },
          controller : 'signupRegVideoCtrl as signupRegVideo'
      });

  // $urlRouterProvider.otherwise('/login');
}
