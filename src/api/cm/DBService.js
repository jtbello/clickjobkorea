'use strict';

api.factory('cmService', cmService);

function cmService($q, $timeout, $io) {

  var path = '/cm/cmService';
              
  return {
    insertErrLog : insertErrLog,
    selectMsdpProperty : selectMsdpProperty,
    selectMsdpPropertys : selectMsdpPropertys
  };
  
  
  /**
   * 에러 로그 저장
   */
  function insertErrLog(param,header) {
    var config = {}
    if(header){
      config.header = header;
    }
    
    var d = $q.defer();
    $io.apiExe(path +  '/insertErrLog',param,config)
      .then(function(response) {
        // success handler
        var header = response.serviceHeader;
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(body.user == undefined || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            return d.reject(JSON.stringify(response));
          }
        }catch(e){}
        return d.resolve(body);
      })
      .catch(function(reason) {
        // fail handler
        return d.reject(reason);
      });
    return d.promise;
  }
  
  /**
   * 프로퍼티 조회
   */
  function selectMsdpProperty(param,header) {
    var config = {}
    if(header){
      config.header = header;
    }
    
    var d = $q.defer();
    $io.apiExe(path +  '/selectMsdpProperty',param,config)
      .then(function(response) {
        // success handler
        var header = response.serviceHeader;
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(body.user == undefined || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            return d.reject(JSON.stringify(response));
          }
        }catch(e){}
        return d.resolve(body);
      })
      .catch(function(reason) {
        // fail handler
        return d.reject(reason);
      });
    return d.promise;
  }
  
  /**
   * 프로퍼티 조회(다건)
   */
  function selectMsdpPropertys(param) {
    
    var promisesArray = [];
    _.forEach(param.propertyKeys,function(obj){
      var propertyParam = {'propertyKey' : obj};
      promisesArray.push(selectMsdpProperty(propertyParam))
    });
      
    return $q.all(promisesArray)
    .then(function(result){
     return result
    });
    
  }
  
}
