
angular
  .module('ma.constant', [])
  .constant('maConstant', _.deepFreeze({
    //Y
    COMMON_Y  : 'Y',
    //N
    COMMON_N  : 'N',
    //Delete
    COMMON_D  : 'D',
    //Update
    COMMON_U  : 'U',
    //Insert
    COMMON_I  : 'I',
    //ERROR
    COMMON_ERROR  : 'E'
    
  }))
