
api.factory('SmplSvc', SmplSvc);

function SmplSvc($log, $q, $io) {
  var logger = $log('SmplSvc');

  return {
    selReprPrdtList: selReprPrdtList
  };

  // SFQuEntplPrdtLstSvc.selReprPrdtList
  function selReprPrdtList(data) {
    // var param = {
    //   stndYmd: '20150127',
    //   entplSystmChnlScCd: '01',
    //   entplChnlScCd: '0201',
    //   cnsltNo: '15573426'
    // };
    return $io.user('/qu/SFQuEntplPrdtLstSvc/selReprPrdtList', data);
  }
}
