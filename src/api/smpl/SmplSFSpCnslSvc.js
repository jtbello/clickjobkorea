'use strict';

api.factory('SmplSFSpCnslSvc', SmplSFSpCnslSvc);

function SmplSFSpCnslSvc($q, $timeout, $io, $message, spConstant) {

  var path = '/smpl/sp/SFSpCnslSvc';

  return {
    selListCnsl: selListCnsl,
    insCnsl : insCnsl,
    delCnsl : delCnsl,
    updCnsl : updCnsl,
    selListCnslInfo : selListCnslInfo,
    selListAdmr : selListAdmr,
    insAdmr : insAdmr,
    selListAdmrInfo : selListAdmrInfo
  };

  /**
   * 목록 조회
   */
  function selListCnsl(param, header) {

    var config = {
      header: header
    };

    return $io.api(path +  '/selListCnsl', param, config)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try {
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        } catch (e) {}
        return body;
      });
  }

  /**
   * 등록
   */
  function insCnsl(param) {
    return $io.api(path + '/insCnsl', param)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try {
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        } catch (e) {}
        return body;
      });
  }

  /**
   * 삭제
   */
  function delCnsl(param) {
    return $io.api(path +  '/delCnsl', param)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      });
  }

  /**
   * 수정
   */
  function updCnsl(param) {
    return $io.api(path +  '/updCnsl',param)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert("서비스 오류 입니다.");
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      });
  }

  /**
   * 상세 정보 조회
   */
  function selListCnslInfo(param) {
    return $io.api(path + '/selListCnslInfo',param)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      });
  }

  /**
   * 칭찬하기 조회
   */
  function selListAdmr(param, header) {

    var config = {
      header: header
    };

    return $io.api(path +  '/selListAdmr', param, config)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      });
  }

  /**
   * 칭찬하기 등록
   */
  function insAdmr(param, header) {

    var config = {
      header: header
    };

    return $io.api(path +  '/insAdmr', param, config)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return $q.resolve(body);
      });
  }

  /**
   * 칭찬하기 조회
   */
  function selListAdmrInfo(param, header) {

    var config = {
      header: header
    };

    return $io.api(path +  '/selListAdmrInfo', param, config)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      })
  }

}
