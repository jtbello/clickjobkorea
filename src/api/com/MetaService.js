api.factory('MetaSvc', MetaSvc);

api.factory('MetaSvcCache', function($cacheFactory) {
  return $cacheFactory('MetaSvcData');
});

function MetaSvc($log, $q, $io, $timeout, $storage, MetaSvcCache, $rootScope) {
  var logger = $log('MetaSvc');
  var retry = 3;
  var getStQueue = [];
  var getStMap = {};
  var busy;

  // service end point.
  var path = '/api/com/selectCommonInfo.login';

  var _funcList = {
    getBizCode : getBizCode_sync, // 한번에 여러개 모아서 가져옴니다.
    getBizCodeAsync : getBizCode_async, // 한번에 한개씩 가져옵니다. JavaScript thread-safe 문제 생기면.  이 버전을 써야 합니다.

    // getServerTime : getServerTime

    // 기타 함수들 삭제. (실 서비스에서 코드는 조회 합니다!!)
  };

  var _bizCodeQueue = []; // code caching 용.
  var _bizCodeCallback = _.debounce(_fetchBizCode, 500, { maxWait: 1000}); // 500ms 기다린 후, 서버 요청


  return _funcList;

  // function getServerTimeApi(id) {
  //   var d = $q.defer();
  //   // $rootScope에 저장된 값이 없으면 서버시간을 조회하고, 있다면 저장된 값을 리턴한다.
  //   if( _.isUndefined( $rootScope.saveServerTime ) ) {
  //     $io.apiExe('/cm/TimeSvc/getServerTime', {})
  //     .then(function(res) {
  //       var rs = $rootScope.$eval('serviceBody.data.timestamp', res);
  //       if(!_.isEmpty($rootScope.entplanImsiDate)){
  //         rs = moment(parseInt($rootScope.entplanImsiDate)).valueOf();
  //         logger.log("기준일 변경 날짜 : " + moment(rs).format("YYYYMMDD HHmmss"));
  //       }
  //       $rootScope.saveServerTime = rs;
  //       d.resolve(rs);
  //     });
  //   } else {
  //     d.resolve($rootScope.saveServerTime);
  //   }
  //   return d.promise;
  // }

  /**
   * getServerTime 함수를 동시에 호출당할 경우 첫번째건만 실제 조회하도록 수정
   */
  // function getServerTime() {
  //   var d = $q.defer();
  //   var id = _.uniqueId('getServerTime#');
  //   getStQueue.push(id);
  //   getStMap[id] = {
  //     defer: d
  //   };
  //
  //   (function next() {
  //     if (!getStQueue.length || busy) {
  //       $rootScope.saveServerTime = undefined;    // 저장된 값 초기화
  //       return;
  //     }
  //     var id = getStQueue.shift();
  //     var config = getStMap[id];
  //     busy = true;
  //     getServerTimeApi(id)
  //       .then(config.defer.resolve, config.defer.reject)
  //       .finally(function() {
  //         delete getStMap[id];
  //         busy = false;
  //         next();
  //       });
  //   })();
  //
  //  return d.promise;
  //}

  // Promise 객체
  // function getServerDate() { return _.now(); }

  function getBizCode_sync(cd) {
    var cache = MetaSvcCache.get(cd);
    if(cache == null) {
      logger.debug('MetaCode cache not exists! - ['+cd+']');

      cache = $io.api(path, {commonInfoCode: cd})
      .then(function(res) {
        var rs = $rootScope.$eval('user.resultData.commonData', res);
        if(!_.isEmpty(rs)) {
          return _.chain(rs).map(function(d) { return { k : d.COMMON_INFO_VALUE1, v : d.COMMON_INFO_VALUE2}; }).value();
        }
        MetaSvcCache.remove(cd);
        getBizCode_sync(cd); // retry
        return [{ k : '', v : '코드오류'}];
      })
      .catch(function(rs) {
        MetaSvcCache.remove(cd);
        getBizCode_sync(cd); // retry
        return [{ k : '', v : '코드오류'}];
      })
      ;
      MetaSvcCache.put(cd, cache);
    } else {
      logger.debug('MetaCode cache hit! - ['+cd+']');
    }
    return cache;
  }

  // debounce 사용.
  function getBizCode_async(cd) {
    var cache = MetaSvcCache.get(cd);
    if(cache == null) {
      logger.debug('MetaCode cache not exists! - ['+cd+']');
      var d = $q.defer();

      // queue 에 밀어넣고~ async call
      _bizCodeQueue.unshift({ code : cd, cb : d });
      setTimeout(_bizCodeCallback(), 0);

      cache = d.promise;
      MetaSvcCache.put(cd, cache);
    } else {

      logger.debug('MetaCode cache hit! - ['+cd+']');
    }
    return cache;
  }

  function _fetchBizCode() {
    var set = [];
    var cached = [];
    while(_bizCodeQueue.length > 0) {
      set.unshift(_bizCodeQueue.shift());
    }

    $storage.get('commonInfoCode')
    .then(function(list) {
      var notFounds = [];
      cached = _.chain(set)
        .map(function(item) {
          var found = _.find(list, {commonInfoCode: item.code});
          if (!found) {
            notFounds.push(item.code);
          }
          return found;
        })
        .compact()
        .value();
      return notFounds;
    })
    .then(function(notFounds) {
      if (!notFounds.length) {
        return cached;
      }
      var qry = _.map(notFounds, function(d) { return {commonInfoCode: d}; }); // set := { code, defer }
      return  $io.api(path, qry[0])
      .then(function(res) {
        return cached.concat($rootScope.$eval('user.resultData.commonData', res));
      });
    })
    .then(function(rs) {
      if(!_.isEmpty(rs)) { // 그룹목록 취득 완료

        _.forEach(set, function(s) {

          var c = _.find(rs, function(r) { return !_.isEmpty(r.commonCodeValue) &&  r.commonCodeValue[0].commonInfoCode == s.code; });

          if(!_.isEmpty(c) && !_.isEmpty(c.commonCodeValue)) {
            var rv = _.chain(c.commonCodeValue)
              .map(function(d) { return { k : d.cdcValt, v : d.cdcValNm, s : d.uppCdcValt}; }).value();
            s.cb.resolve(rv);
          }
          else { s.cb.reject('Code 조회 오류'); }
        });

      }
      retry = 3;
    })
    .catch(function(res) {
      if(retry-- > 0) {
        _.forEach(set, function(s) {
          _bizCodeQueue.unshift(s);
        });
        setTimeout(_bizCodeCallback(), 0);
      } else {
        _.forEach(set, function(s) {
          s.cb.reject(res);
          MetaSvcCache.remove(s.code);
        });
        retry = 3;
      }
    })
    ;
  }

}