
angular
  .module('com.constant', [])
  .constant('comConstant', _.deepFreeze({

      CP_USE_USER             : 'CP_USE_USER',              // 쿠폰사용자
      CP_BUY_KIND             : 'CP_BUY_KIND',              // 쿠폰구매종류
      CP_BUY_PAY_KIND         : 'CP_BUY_PAY_KIND',          // 결제방식
      CP_USE_STATE            : 'CP_USE_STATE',             // 쿠폰사용상태
      MESSAGE_TYPE            : 'MESSAGE_TYPE',             // 푸쉬종류타입
      STATE_CODE              : 'STATE_CODE',               // 푸쉬발송상태
      MSG_TYPE                : 'MSG_TYPE',                 // 푸쉬메시지타입
      RESULT_CODE             : 'RESULT_CODE',              // 푸쉬결과코드
      AUTH_INFO_KIND          : 'AUTH_INFO_KIND',           // 권한종류
      HP_KIND                 : 'HP_KIND',                  // 휴대전화회사종류
      COMPANY_STATE           : 'COMPANY_STATE',            // 기업회원상태
      PUSH_TYPE               : 'PUSH_TYPE',                // PSIDORCUID
      QNA_KIND                : 'QNA_KIND',                 // QNA종류
      REQUEST_KIND            : 'REQUEST_KIND',             // 요청종류
      REQUEST_STATE           : 'REQUEST_STATE',            // 요청상태
      CANCEL_KIND             : 'CANCEL_KIND',              // 취소종류
      ATTEND_NO_STATE         : 'ATTEND_NO_STATE',          // 미출석상태
      ATTEND_NO_LEAVE_STATE   : 'ATTEND_NO_LEAVE_STATE',    // 출석,무단이탈상태
      OPEN_YN                 : 'OPEN_YN',                  // 푸쉬오픈여부
      RCUT_BASE_STATE         : 'RCUT_BASE_STATE',          // 모집상태값
      HIS_KIND                : 'HIS_KIND',                 // 이력종류
      PGM_REUT_FIELD          : 'PGM_REUT_FIELD',           // 지원분야
      MEMBER_KIND             : 'MEMBER_KIND',              // 회원종류
      JOB                     : 'JOB',                      // 직업
      SUPPORT_FIELD           : 'SUPPORT_FIELD',            // 지원분야
      HOPE_CAST               : 'HOPE_CAST',                // 희방배역
      BODY_INFO               : 'BODY_INFO',                // 신체사항
      HAIR_STATE              : 'HAIR_STATE',               // 헤어상태
      HAIR_HEIGHT             : 'HAIR_HEIGHT',              // 헤어길이
      TOOTH_CORREC            : 'TOOTH_CORREC',             // 치아교정
      EXPOSURE_INFO           : 'EXPOSURE_INFO',            // 노출가능여부
      TATOO_INFO              : 'TATOO_INFO',               // 문신
      PIER_INFO               : 'PIER_INFO',                // 피어싱
      BEARD_INFO              : 'BEARD_INFO',               // 수염
      DIMPLE_INFO             : 'DIMPLE_INFO',              // 보조개
      DOUBLE_EYE_INFO         : 'DOUBLE_EYE_INFO',          // 눈쌍꺼플
      BOTTOMS_SIZE            : 'BOTTOMS_SIZE',             // 하의사이즈
      TOP_SIZE                : 'TOP_SIZE',                 // 상의사이즈
      DRIVE_BICYCLE_YN        : 'DRIVE_BICYCLE_YN',         // 자전거운전
      DRIVE_MOTERCYCLE_YN     : 'DRIVE_MOTERCYCLE_YN',      // 오토바이운전
      SMOKE_YN                : 'SMOKE_YN',                 // 흡연여부
      FIELD                   : 'FIELD',                    // 경력 분류
      BROADCASTING_STATION    : 'BROADCASTING_STATION',     // 방송국
      PART                    : 'PART',                     // 배역
      PENALTY_CODE            : 'PENALTY_CODE',             // 패널티종류코드
      PENALTY_KIND            : 'PENALTY_KIND',             // 패널티종류
      AGREE_KIND              : 'AGREE_KIND',               // 정보동의종류
      SAMPLE_VIDEO            : 'SAMPLE_VIDEO',             // 샘플영상정보
      MAP_INFO_URL            : 'MAP_INFO_URL',             // 노선 검색
      PHOTO_INFO_URL          : 'PHOTO_INFO_URL',           // 사진 및 영상 용량 축소 방법 가이드 주소
      AGREE_INFO              : 'AGREE_INFO',               // 약관
      DRESS_INFO              : 'DRESS_INFO',               // 보유 의상
      GENDER                  : 'GENDER',                   // 성별
      BANK_NAME               : 'BANK_NAME',                // 은행명
      CLASS_INFO              : 'CLASS_INFO',               // 보유방송등급
      PUSH_ADMIN_NO           : 'PUSH_ADMIN_NO',            // [푸쉬발송용도]총관리자일련번호
      CHARGE_KIND             : 'CHARGE_KIND',              // 담당자종류
      MY_RCUT_SEL             : 'MY_RCUT_SEL'               // 내게맞춪정보조회조건

  }))
