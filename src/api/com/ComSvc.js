'use strict';

api.factory('ComSvc', ComSvc);

function ComSvc($q, $timeout, $io, $message) {

    var path = '/api/com';

    return {
        selectCommonInfo: selectCommonInfo,
        insertErrLog : insertErrLog,
        fileDelete : fileDelete,
        fileUpload : fileUpload,
        selectBannerList : selectBannerList,
        updateBannerCntUp : updateBannerCntUp,
        updateNoti : updateNoti,
        insertNoti : insertNoti,
        selectNotiList : selectNotiList
    };

    /**
     * 공통코드 조회
     */
    function selectCommonInfo(code) {
        var param = {};
        param.commonInfoCode = code;					// 공통코드 코드
        return $io.api(path + '/selectCommonInfo.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 에러 메시지 저장
     */
    function insertErrLog(param) {
        // var param = {};
        // param.busnLinkKey = 'admin';							// [필수] 로그인 아이디
        // param.busnLinkKeyType = 'B1';							// [필수][공코:BUSN_LINK_KEY_TYPE] 관련회원종류
        // param.deviceModel = 'SM-P580';						// [선택] 모델명
        // param.deviceOsVer = '6.0';							// [선택] OS버전정보
        // param.svcUrl = '/login/login';						// [필수] 호출 URL
        // param.errMsgDesc = '에러메시지';							// [선택] 에러메시지
        // param.pageName = '/page.html';						// [선택] 호출 페이지
        // param.errType = '1';									// [선택] 에러 타입 [1: APP, 2: WEB]
        // param.errMsg = '상세에러내용';							// [필수] 에러 상세 내용
        // param.svcParam = '{param :22}';						// [필수] 호출 파라미터
        return $io.api(path + '/insertErrLog.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 파일삭제
     */
    function fileDelete(param) {
        // var param = {};
        // param.attachSeq = '1';			// [필수]파일등록 후 결과값에 파일 일련번호
        // param.fileOrgName = "test.jpg";	// [필수]파일등록 후 결과값에 파일 원본파일이름
        return $io.api(path + '/fileDelete.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 파일등록
     */
    function fileUpload(param) {
        // <form action="/api/com/fileUpload.login" method="post" enctype="multipart/form-data" >
        //     <input type = "file" name="uploadFile">
        //     <input type="hidden" name="busnLinkKey" value="-1"/>	<!-- 로그인 회원 일련번호 없을경우 -1-->
        //     <input type="submit">
        // </form>
        return $io.api(path + '/fileUpload.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 배너조회
     */
    function selectBannerList(param) {
        return $io.api('/api/selectBannerList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 배너수정
     */
    function updateBannerCntUp(param) {
        return $io.api('/api/updateBannerCntUp.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    function updateNoti(param){
        // var param = {};
        //
        // param.topNotiYn = 'Y';							// [선택] Y: 상단공지, N: 일반공지
        // param.notiKind = 'P';								// [선택] P: 출연자공지, C : 기업 공지(어플관리자권한에만 노출), CC: 기업내 공지  A: 전체 공지(어플관리자권한에만 노출)
        // param.title = '공지 테스트 제목 수정';					// [선택] 공지 제목
        // param.detailCntn = '공지 테스트 내용수정';				// [선택] 공지 내용
        // param.notiStartDtm = "2019-07-11 00:00:00";		// [선택] 공지 개시 시작일 [일시 패턴 : 0000-00-00 00:00:00]
        // param.notiEndDtm = "2019-07-11 00:00:00";			// [선택] 공지 개시 종료일 [일시 패턴 : 0000-00-00 00:00:00]
        // param.notiSeq = '1';								// [필수] 공지 일련번호
        return $io.api(path + '/updateNoti.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    function insertNoti(param){
        // var param = {};
        // param.topNotiYn = 'Y';							// [필수] Y: 상단공지, N: 일반공지
        // param.notiKind = 'A';								// [필수] P: 출연자공지, C : 기업 공지(어플관리자권한에만 노출), CC: 기업내 공지  A: 전체 공지(어플관리자권한에만 노출)
        // param.title = '공지 테스트 제목';						// [필수] 공지 제목
        // param.detailCntn = '공지 테스트 내용';					// [필수] 공지 내용
        // param.notiStartDtm = "2019-07-11 00:00:00";		// [필수] 공지 개시 시작일 [일시 패턴 : 0000-00-00 00:00:00]
        // param.notiEndDtm = "2019-07-11 00:00:00";			// [필수] 공지 개시 종료일 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/insertNoti.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    function selectNotiList(param){
        // var param = {};
        // param.searchNotiKind = '';		// [선택]기업 공지에서 필터링 할때 사용하는 변수
        // // C : 어플관리자가 기업에게 공지한 공지, CC: 기업관리자가 본인 기업에게 한 공지(담당자들에게 공지)
        // // P: 기업관리자가 출연자에게 공지한 공지, A: 어플관리자가 출연자, 기업 모두에게 공지한 공지,
        // param.startNo = 0;				// [필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;					// [필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectNotiList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
}
