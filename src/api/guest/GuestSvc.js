'use strict';

api.factory('GuestSvc', GuestSvc);

function GuestSvc($q, $timeout, $io, $message) {

    var path = '/api/mem';

    return {
        updatePushInfo: updatePushInfo,
        insertNoSchedule : insertNoSchedule,
        selectEmerRcutList : selectEmerRcutList,
        selectMyRcutList : selectMyRcutList,
        selectAllRcutList : selectAllRcutList,
        selectWatingAllRcutList : selectWatingAllRcutList,
        selectRcutDetailInfo : selectRcutDetailInfo,
        requestRcutMember : requestRcutMember,
        selectRequestMemberSubInfo : selectRequestMemberSubInfo,
        insertRequestMemberSubInfo : insertRequestMemberSubInfo,
        updateRequestMemberSubInfo : updateRequestMemberSubInfo,
        cancelRequestRcutMember : cancelRequestRcutMember,
        updateRequestCompanyInfo : updateRequestCompanyInfo,
        updateRequestCompanyCancel : updateRequestCompanyCancel,
        insertQna : insertQna,
        selectMyQna : selectMyQna,
        selectCompanyRequestInfo : selectCompanyRequestInfo,
        selectMyRequestInfo : selectMyRequestInfo,
        selectMyCancelRequestInfo : selectMyCancelRequestInfo,
        selectNoSchedule : selectNoSchedule,
        selectWatingSchedule : selectWatingSchedule,
        selectIngSchedule : selectIngSchedule,
        selectEndSchedule : selectEndSchedule,
        selectScheduleDetail : selectScheduleDetail,
        updateMeInfo : updateMeInfo,
        selectPushInfo : selectPushInfo,
        updatePushOpen : updatePushOpen,
        selectCouponList : selectCouponList,
        selectPaymentKind : selectPaymentKind,
        insertPaymentInfo : insertPaymentInfo,
        insertPaymentProcInfo : insertPaymentProcInfo,
        insertBuyCoupon : insertBuyCoupon,
        selectMyCouponList : selectMyCouponList,
        selectMyPenaltyInfo : selectMyPenaltyInfo,
        selectMyInfo : selectMyInfo,
        updateMyInfo : updateMyInfo,
        selectAgreeInfo : selectAgreeInfo,
        selectPgmList : selectPgmList,
        selectModifyRcutPushInfo : selectModifyRcutPushInfo,
        updateModifyRcutPushInfo : updateModifyRcutPushInfo
    };

    /**
     * 푸쉬정보입력
     */
    function updatePushInfo(param) {
        // var param = {};
        // param.pushId = '1234564564646544564';				// [필수] 푸쉬아이디
        // param.pushType = 'P1';							// [필수][공코:PUSH_TYPE] 푸쉬타입
        // param.memberSeq = '1';							// [필수] 회원 일련번호
        return $io.api(path + '/updatePushInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 불가일정 등록
     */
    function insertNoSchedule(param) {
        // var scheduleInfo = [];
        // var data = {};
        // data.busnLinkKey = '1';						//[필수] 회원 일련번호
        // data.delIns = 'I';							//[필수] 삭제,입력 여부 [I: 입력, D: 삭제][2018-05-08수정]
        // data.scheduleDtm = '2018-05-10 00:00:00';		//[필수] 불가일정 [일시 패턴 : 0000-00-00 00:00:00]
        // scheduleInfo.push(data);
        //
        // var data = {};
        // data.busnLinkKey = '1';						//[필수] 회원 일련번호
        // data.delIns = 'D';							//[필수] 삭제,입력 여부 [I: 입력, D: 삭제][2018-05-08수정]
        // data.scheduleDtm = '2018-05-13 00:00:00';		//[필수] 불가일정 [일시 패턴 : 0000-00-00 00:00:00]
        // scheduleInfo.push(data);
        //
        // var param = {};
        // param.scheduleInfo = scheduleInfo;				// [필수] 불가 일정 정보
        return $io.api(path + '/insertNoSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 급구 출연자 모집 조회
     */
    function selectEmerRcutList() {
        var param = {};
        return $io.api(path + '/selectEmerRcutList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 내게맞춤정보모집조회
     */
    function selectMyRcutList(param) {
        // var param = {};
        // param.memberSeq = '1';					//[필수] 회원 일련번호
        // param.orderType = "1";					//[필수] 정렬 순서[1,2,3,4] 랜덤으로 보냄(앱실행시마다변경)
        // param.startNo = 0;						//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;						//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectMyRcutList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 추천모집조회
     */
    function selectAllRcutList(param) {
        // var param = {};
        // param.orderType = "1";					//[필수] 정렬 순서[1,2,3,4] 랜덤으로 보냄(앱실행시마다변경)
        // param.startNo = 0;						//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;						//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectAllRcutList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연대기 신청 가능 모집 조회
     */
    function selectWatingAllRcutList(param) {
        // var param = {};
        // param.orderType = "1";					//[필수] 정렬 순서[1,2,3,4] 랜덤으로 보냄(앱실행시마다변경)
        // param.startNo = 0;						//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;						//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectWatingAllRcutList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자모집상세조회
     */
    function selectRcutDetailInfo(param) {
        // var param = {};
        // param.rcutBaseSeq = "1";					//[필수] 모집일련번호
        return $io.api(path + '/selectRcutDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자모집신청
     */
    function requestRcutMember(param) {
        // var param = {};
        // param.memberSeq = "1";						//[필수] 회원 일련번호
        // param.rcutBaseSeq = "1";					//[필수] 모집일련번호
        // param.requestState = 'R4'                //[선택] 요청상태 (출연대기신청일때만 필수)
        return $io.api(path + '/requestRcutMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집  신청 후 알림 조회
     */
    function selectRequestMemberSubInfo(param) {
        // var param = {};
        // param.memberSeq = "1";					//[필수] 회원 일련번호
        // param.requestInfoSeq = "1";				//[필수] 모집신청일련번호
        return $io.api(path + '/selectRequestMemberSubInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 알림 등록(기상시간,  출발 장소)
     */
    function insertRequestMemberSubInfo(param) {
        // var param = {};
        // param.memberSeq = "1";					//[필수] 회원 일련번호
        // param.requestInfoSeq = "1";				//[필수] 모집신청일련번호
        // param.getupHour = "06";					//[필수] 기상시간 시(2자리)
        // param.getupMinite = "00";					//[필수] 기상시간 분(2자리)
        // param.startHour = "07";					//[필수] 출발시간 시(2자리)
        // param.startMinite = "00";					//[필수] 출발시간 분(2자리)
        // param.startPoint = "부천시 송내대로";			//[필수] 출발장소(GPS현재장소)
        return $io.api(path + '/insertRequestMemberSubInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 알림 수정(기상시간,  출발 장소)
     */
    function updateRequestMemberSubInfo(param) {
        // var param = {};
        // param.memberSeq = "1";					//[필수] 회원 일련번호
        // param.memberSubInfoSeq = "3";				//[필수] 알림일련번호
        // param.getupHour = "08";					//[필수] 기상시간 시(2자리)
        // param.getupMinite = "30";					//[필수] 기상시간 분(2자리)
        // param.startHour = "09";					//[필수] 출발시간 시(2자리)
        // param.startMinite = "30";					//[필수] 출발시간 분(2자리)
        // param.startPoint = "부천시 송내대로 1";			//[필수] 출발장소(GPS현재장소)
        return $io.api(path + '/updateRequestMemberSubInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 신청 후 취소
     */
    function cancelRequestRcutMember(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 회원일련번호
        // param.requestInfoSeq = "2";							//[필수] 취소 신청 일련번호
        return $io.api(path + '/cancelRequestRcutMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업요청 수락
     */
    function updateRequestCompanyInfo(param) {
        // var param = {};
        // param.memberSeq = "2";								//[필수] 회원일련번호
        // param.requestInfoSeq = "5";							//[필수] 신청 일련번호
        // param.rcutBaseSeq = "10";								//[필수] 모집 일련번호
        return $io.api(path + '/updateRequestCompanyInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업요청 수락 후 취소
     */
    function updateRequestCompanyCancel(param) {
        // var param = {};
        // param.memberSeq = "2";								//[필수] 회원일련번호
        // param.requestInfoSeq = "5";							//[필수] 신청 일련번호
        // param.rcutBaseSeq = "10";								//[필수] 모집 일련번호
        return $io.api(path + '/updateRequestCompanyCancel.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 1대1문의 등록
     */
    function insertQna(param) {
        // var param = {};
        // param.companySeq = "1";								//[필수][회원정보 :MEMBER_COMPANY_INFO]회원소속회사일련번호
        // param.qnaKind = "Q1";									//[필수][공코 : QNA_KIND] QNA유형
        // param.pgmBaseSeq = "5";								//[필수] 프로그램 일련번호
        // param.title = "문의드립니다.";							//[필수] 문의제목
        // param.detailCntn = "이프로그램 담당자 이름은??";				//[필수] 문의내용
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.writeName = "홍길동";								//[필수] 작성자 이름
        // param.writePhone = "01012411234";						//[필수] 작성자전화번호
        // param.writeEmail = "test@naver.com";					//[필수] 작성자이메일
        return $io.api(path + '/insertQna.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 나의문의내역조회
     */
    function selectMyQna(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectMyQna.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 요청현황조회
     */
    function selectCompanyRequestInfo(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어
        return $io.api(path + '/selectCompanyRequestInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 신청현황조회
     */
    function selectMyRequestInfo(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어
        // param.startDate = '';								//[선택] 촬영시작일 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영종료일 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectMyRequestInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 취소현황조회
     */
    function selectMyCancelRequestInfo(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectMyCancelRequestInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 촬영불가일정조회
     */
    function selectNoSchedule(param) {
        // var param = {};
        // param.busnLinkKey = "1";								//[필수] 회원일련번호
        return $io.api(path + '/selectNoSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 대기중스케줄조회
     */
    function selectWatingSchedule(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어
        // param.startDate = '';									//[선택] 촬영시작일 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영종료일 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectWatingSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 진행중스케줄조회
     */
    function selectIngSchedule(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectIngSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 완료스케줄조회
     */
    function selectEndSchedule(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어
        // param.startDate = '';									//[선택] 촬영시작일 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영종료일 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectEndSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 스케줄상세조회
     */
    function selectScheduleDetail(param) {
        // var param = {};
        // param.rcutBaseSeq = '10';										//[필수] 모집일련번호
        // param.requestInfoSeq = '3';									//[필수] 모집신청일련번호
        return $io.api(path + '/selectScheduleDetail.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 본인 출석,종료,기상,출발 입력
     */
    function updateMeInfo(param) {
        // var param = {};
        // param.updateKind = 'U4';										//[필수] 입력종류[U1 : 출석, U2:종료, U3:기상, U4:출발]
        // param.memberSeq = '1';										//[필수] 회원일련번호
        // param.requestInfoSeq = '3';									//[필수] 모집신청일련번호
        //
        // // 출석 필수 파라미터
        // if(param.updateKind == 'U1'){
        //     param.meAttendPoint = '서울 강남';							//[필수] 출석 클릭한 장소(GPS연동결과값)
        // }
        //
        // // 종료 필수 파라미터
        // if(param.updateKind == 'U2'){
        //     param.meEndPoint = '서울 홍대';								//[필수] 종료 클릭한 장소(GPS연동결과값)
        // }
        //
        // // 기상 필수 파라미터
        // if(param.updateKind == 'U3'){
        //     param.meGetupPoint = '서울 신도림';							//[필수] 기상 클릭한 장소(GPS연동결과값)
        // }
        //
        // // 출발 필수 파라미터
        // if(param.updateKind == 'U4'){
        //     param.meStartPoint = '서울 신도림';							//[필수] 출발 클릭한 장소(GPS연동결과값)
        // }
        return $io.api(path + '/updateMeInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 미확인,확인 알림 조회
     */
    function selectPushInfo(param) {
        // var param = {};
        // param.busnLinkKey = "1";								//[필수] 조회 회원 일련번호
        // param.openYn = "P2";									//[필수][공코:OPEN_YN] 미확인,알림여부
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.messageType = '';								//[선택][공코:MESSAGE_TYPE] 푸쉬종류
        return $io.api(path + '/selectPushInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 알림 확인 처리
     */
    function updatePushOpen(param) {
        // var param = {};
        // param.pushResultSeq = "1";								//[필수] 푸쉬결과일련번호
        return $io.api(path + '/updatePushOpen.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 판매쿠폰리스트 조회
     */
    function selectCouponList(param) {
        // var param = {};
        // param.cpUseUser = "C1";								//[필수][공코:CP_USE_USER] 쿠폰사용자
        return $io.api(path + '/selectCouponList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 결제수단 조회
     */
    function selectPaymentKind(param) {
        // var param = {};
        // param.cpUseUser = "C1";								//[필수][공코:CP_USE_USER] 쿠폰사용자
        return $io.api(path + '/selectPaymentKind.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 결제 이력 입력
     */
    function insertPaymentInfo(param) {
        // var param = {};
        // param.paymentStatus = 'P1';			//[필수][공코:PAYMENT_STATUS] 결제결과
        // param.cpBuyPayKind = 'C2';			//[필수][공코:CP_BUY_PAY_KIND] 결제방식
        // param.cpListSeq = '2';				//[필수] 판매상품일련번호
        return $io.api(path + '/insertPaymentInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 결제 단계 이력
     */
    function insertPaymentProcInfo(param) {
        // var param = {};
        // param.param.paymentHisStatus = '10';	//[필수]10 : 구매API 호출전, 20 : 구매API 호출 후 성공
                                                // 30 : 구매API 호출 후 실패, 21 : 소비API 호출 후 성공
                                                // 31 : 소비API 호출 후 실패, 40 : 쿠폰입력 전
                                                // 50 : 쿠폰 입력 정상, 60 : 쿠폰 입력 실패
        // param.cpProcHisSeq = 'APAYyyyymmddhhssmmsss';	//[필수][공코:CP_BUY_PAY_KIND] 결제방식
        // param.cpListSeq = '2';				//[필수] 판매상품일련번호
        // param.hisVal1 ~ hisVal10 = ''        // 필요한 DATA
        return $io.api(path + '/insertPaymentProcInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 쿠폰구매
     */
    function insertBuyCoupon(param) {
        // var param = {};
        // param.cpPaymentSeq = '2';			//[필수] 결제이력 일련번호
        // param.cpListSeq = '2';				//[필수] 판매상품일련번호
        return $io.api(path + '/insertBuyCoupon.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 쿠폰구매사용내역 조회
     */
    function selectMyCouponList(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;									//[필수] 페이징 페이지당보여질수
        //
        // param.startDate = '2018-05-01 00:00:00';			//[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';				//[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectMyCouponList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 패널티 정보 조회
     */
    function selectMyPenaltyInfo(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.penaltyKind = 'P1';								//[선택][공코 : PENALTY_KIND]
        // param.penaltyStDtm = '2018-05-01 00:00:00';			//[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.penaltyEndDtm = '2018-05-31 00:00:00';			//[선택] ㅍ [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectMyPenaltyInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 나의 정보 조회
     */
    function selectMyInfo() {
        var param = {};
        return $io.api(path + '/selectMyInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 나의 정보 수정
     */
    function updateMyInfo(param) {
        // var memberBaseInfo = {};
        // var memberDetailInfo = {};
        // var memberCareerInfo = [];
        //
        // // 회원 기본
        // memberBaseInfo.memberHpKind = 'H2';						//[선택][공코 : HP_KIND] 휴대전화회사종류
        // memberBaseInfo.memberHp = '0101111111';					//[선택]휴대폰번호 ('-'제거)
        // memberBaseInfo.memberAddr = '경기도 부천시(수정)';			//[선택]기본 주소
        // memberBaseInfo.memberAddrDetail = '푸르지오 아파트(수정)';		//[선택]상세 주소
        // memberBaseInfo.memberAddrNo = '01111';					//[선택]우편번호
        // memberBaseInfo.memberSubwayName = '송내역(수정)';			//[선택]가까운지하철
        // memberBaseInfo.memberSubwayTime = '10';					//[선택]소요시간
        // memberBaseInfo.height = '170';							//[선택]키
        // memberBaseInfo.weight = '55';							//[선택]몸무게
        // memberBaseInfo.email = 'mo@naver.com';					//[선택]이메일
        // memberBaseInfo.photoAttachSeqs = '2|3|4';				//[선택]사진 첨부파일 일련번호
        //
        // memberBaseInfo.accNo = '1234564545';					//[선택]계좌번호('-'제거)
        // memberBaseInfo.accBankName = '국민';						//[선택][공코 : BANK_NAME]계좌은행명
        // memberBaseInfo.accName = '수정자';							//[선택]계좌예금주
        // memberBaseInfo.companyNoAttachSeqs = '5';				//[선택]사업자등록증 및 증빙서류 첨부파일 일련번호
        // memberBaseInfo.videoAttachSeqs = '6';					//[선택]동영상 첨부파일 일련번호
        //
        // // 회원 상세
        // memberDetailInfo.supportField = 'S2';					//[선택][공코 : SUPPORT_FIELD] 지원분야
        // memberDetailInfo.hopeCast = 'H2';						//[선택][공코 : HOPE_CAST] 희망배역
        //
        // memberDetailInfo.job = 'J2';							//[선택][공코 : JOB] 직업
        // memberDetailInfo.hopeCastEtc = '희망배역기타';				//[선택]희망배역기타
        // memberDetailInfo.bodyInfo = 'B2';						//[선택][공코 : BODY_INFO] 신체사항
        // memberDetailInfo.classInfo = 'C2';						//[선택][공코 : CLASS_INFO] 보유방송등급
        // memberDetailInfo.hairState = 'H2';						//[선택][공코 : HAIR_STATE] 헤어상태
        // memberDetailInfo.hairHeight = 'H2';						//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // memberDetailInfo.toothCorrec = 'T2';					//[선택][공코 : TOOTH_CORREC] 치아교정
        // memberDetailInfo.exposureInfo = 'E2';					//[선택][공코 : EXPOSURE_INFO] 노출가능여부
        // memberDetailInfo.tatooInfo = 'T2';						//[선택][공코 : TATOO_INFO] 문신
        // memberDetailInfo.pierInfo = 'P2';						//[선택][공코 : PIER_INFO] 피어싱
        // memberDetailInfo.beardInfo = 'B2';						//[선택][공코 : BEARD_INFO] 수염
        // memberDetailInfo.dimpleInfo = 'D2';						//[선택][공코 : DIMPLE_INFO] 보조개
        // memberDetailInfo.doubleEyeInfo = 'D2';					//[선택][공코 : DOUBLE_EYE_INFO] 눈쌍꺼플
        // memberDetailInfo.bottomsSize = 'B2';					//[선택][공코 : BOTTOMS_SIZE] 하의사이즈
        // memberDetailInfo.topSize = 'T2';						//[선택][공코 : TOP_SIZE] 상의사이즈
        // memberDetailInfo.footSize = '260';						//[선택]발사이즈 [숫자만]
        // memberDetailInfo.driveBicycleYn = 'D2';					//[선택][공코 : DRIVE_BICYCLE_YN] 자전거운전
        // memberDetailInfo.driveCarYn = 'D2';						//[선택][공코 : DRIVE_CAR_YN] 자동차운전
        // memberDetailInfo.driveMotercycleYn = 'D2';				//[선택][공코 : DRIVE_MOTERCYCLE_YN] 오토바이운전
        // memberDetailInfo.smokeYn = 'D2';						//[선택][공코 : SMOKE_YN] 흡연여부
        // memberDetailInfo.dressBaseCount = '1';					//[선택] 기본정장 벌수 [숫자만]
        // memberDetailInfo.dressBaseColor = '검정';					//[선택] 기본정장 색상
        // memberDetailInfo.dressSemiCount = '1';					//[선택] 세미정장 벌수 [숫자만]
        // memberDetailInfo.dressSemiColor = '검정';					//[선택] 세미정장 색상
        // memberDetailInfo.dressBlackShoes = 'N';					//[선택] 검정구두
        // memberDetailInfo.dressSwimsuit = 'N';					//[선택] 수영복
        // memberDetailInfo.dressSchoolUniform = 'N';				//[선택] 교복
        // memberDetailInfo.dressMilitaryBoots = 'N';				//[선택] 군화
        //
        // // 회원 경력(수정이 있을경우 모든 Data 보내야합니다. 모두 지우고 다시 입력함)
        // var careerInfo = {};
        // careerInfo.field = 'F2';								//[필수][공코 : FIELD] 경력 분류
        // careerInfo.pgmName = '무한도전(수정)';						//[필수]프로그램 명
        // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
        // careerInfo.year = '2018';								//[필수]년도
        // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
        // careerInfo.linesYn = 'Y';								//[필수]대사
        // memberCareerInfo.push(careerInfo);
        //
        // var careerInfo = {};
        // careerInfo.field = 'F2';								//[필수][공코 : FIELD] 경력 분류
        // careerInfo.pgmName = '1박2일(수정)';						//[필수]프로그램 명
        // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
        // careerInfo.year = '2018';								//[필수]년도
        // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
        // careerInfo.linesYn = 'N';								//[필수]대사
        // memberCareerInfo.push(careerInfo);
        //
        //
        // var param = {};
        // param.memberBaseInfo = memberBaseInfo;				// 회원 기본 정보
        // param.memberDetailInfo = memberDetailInfo;			// 회원 상세 정보
        // param.memberCareerInfo = memberCareerInfo;			// 회원 경력 정보
        return $io.api(path + '/updateMyInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정보동의조회(알림설정)
     */
    function selectAgreeInfo() {
        var param = {};
        return $io.api(path + '/selectAgreeInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램조회 (1:1 문의에서 필요한 목록)
     */
    function selectPgmList() {
        var param = {};
        return $io.api(path + '/selectPgmList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 스케줄 변경 정보 조회
     */
    function selectModifyRcutPushInfo() {
        var param = {};
        return $io.api(path + '/selectModifyRcutPushInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 스케줄 변경 정보 수정
     */
    function updateModifyRcutPushInfo(param) {
        // var param = {};
        // param.rcutBaseSeq = "10";								//[필수] 모집 일련번호
        return $io.api(path + '/updateModifyRcutPushInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연대기 신청 가능 모집 조회
     */
    function selectWatingAllRcutList(param) {
        // var param = {};
        // param.orderType = "1";					//[필수] 정렬 순서[1,2,3,4] 랜덤으로 보냄(앱실행시마다변경)
        // param.startNo = 0;						//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;						//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectWatingAllRcutList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
}
