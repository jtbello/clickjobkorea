/* jshint browser:true */

var api = angular.module('easi.api', ['ma.constant', 'com.constant', 'member.constant', 'guest.constant', 'company.constant']);

window.api = api;