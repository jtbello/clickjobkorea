'use strict';

api.factory('MemberSvc', MemberSvc);

function MemberSvc($q, $timeout, $io, $message) {

    var path = '/api';

    return {
        selectMenuList: selectMenuList,
        login: login,
        logOut : logOut,
        findId : findId,
        requestPwFindAuth : requestPwFindAuth,
        authNoCheck : authNoCheck,
        updatePw : updatePw,
        insertMemberRequest : insertMemberRequest,
        selectCompanyList : selectCompanyList,
        selectIdCheck : selectIdCheck,
        selectRrnCheck : selectRrnCheck,
        selectSampleVideo : selectSampleVideo,
        selectTermInfo : selectTermInfo,
        insertCompanyInfo : insertCompanyInfo,
        insertAgree : insertAgree,
        selectAgreeInfo : selectAgreeInfo,
        selectCouponList : selectCouponList
    };

    /**
     * 로그인 전 메뉴 조회
     */
    function selectMenuList() {
        var param = {};
        param.menuKind = 'M1';					// M1:출연자 전체메뉴, M2:기업전체메뉴
        return $io.api(path + '/selectMenuList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }


    /**
     * 로그인
     */
    function login(param) {
        // var param = {};
        // param.loginKind = "L2";		// L1 : 개인, L2 : 기업
        // param.userId = "admin";		// 회원 아이디
        // param.userPw = "1234";		// 회원 패스워드
        // param.siteKind = 'APP';		// 웹/APP 구분
        return $io.api(path + '/login.login', param)
            .then(function(data){
                // ################출연자#####################
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                //     API : 정보동의 조회
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                //         로그인 결과 값에 존재(agreeInfo)
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                //     API : 메뉴 정보
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                //         로그인 결과 값에 존재(menuInfo)
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 로그아웃
     */
    function logOut() {
        var param = {};
        return $io.api(path + '/logOut.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 아이디 찾기(개인,기업)
     */
    function findId(param) {
        // var param = {};
        // param.findKind = '3';				// 1: 개인휴대폰번호찾기 2: 기업휴대폰번호찾기 3 : 기업사업자번호로대표자아이디찾기
        //
        // if(param.findKind == '1'){
        //     param.userName = '정준서';		// 회원이름
        //     param.userHpkind = 'H1';		// 휴대폰회사[공코:HP_KIND]
        //     param.userHp = '01031214545'; // 휴대폰 번호
        // }else if(param.findKind == '2'){
        //     param.companyNm = '총관리자회사';	// 회사이름
        //     param.userName = '정준서';		// 담당자이름
        //     param.userHpkind = 'H1';		// 담당자휴대폰회사[공코:HP_KIND]
        //     param.userHp = '01031214545';	// 휴대폰 번호
        // }else{
        //     param.companyNm = '총관리자회사';	// 회사이름
        //     param.companyNo = '021234';		// 사업자번호
        //     param.companyCeoNm = '대표자';	// 대표자이름
        // }
        return $io.api(path + '/findId.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 패스워드 찾기(개인,기업) 인증번호발송
     */
    function requestPwFindAuth(param) {
        // var param = {};
        // param.findKind = '2';				// 1: 개인비밀번호찾기 2: 기업비밀번호찾기
        //
        // if(param.findKind == '1'){
        //     param.userId = 'admin';		// 아이디정보
        //     param.userName = '정준서';		// 회원이름
        //     param.userHpkind = 'H1';		// 휴대폰회사[공코:HP_KIND]
        //     param.userHp = '01031214545'; // 휴대폰 번호
        // }else{
        //     param.companyNm = '총관리자회사';	// 회사이름
        //     param.userId = 'admin';		// 아이디정보
        //     param.userName = '정준서';		// 회사이름
        //     param.userHpkind = 'H1';		// 휴대폰회사[공코:HP_KIND]
        //     param.userHp = '01031214545'; // 휴대폰 번호
        // }
        return $io.api(path + '/requestPwFindAuth.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 패스워드 찾기(개인,기업) 인증번호확인
     */
    function authNoCheck(param) {
        // var param = {};
        // param.authKey = 'abcdedf';			// 인증요청 후 결과값에 인증키값
        // param.authNo = '12345';				// 화면에서 입력한 인증번호
        return $io.api(path + '/authNoCheck.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 패스워드 변경처리
     */
    function updatePw(param) {
        // var param = {};
        // param.authKey = 'abcdedf';			// 인증요청 후 결과값에 인증키값
        // param.kind = '2';						// 1 : 개인 , 2 : 기업
        // param.userId = 'admin';				// 아이디
        // param.userPw = '12345';			// 변경 패스워드
        return $io.api(path + '/updatePw.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 개인 회원 요청
     */
    function insertMemberRequest(param) {
        // var memberBaseInfo = {};
        // var memberDetailInfo = {};
        // var memberCareerInfo = [];
        //
        // // 회원 기본
        // memberBaseInfo.memberId = 'memberId';					//[필수] 아이디
        // memberBaseInfo.memberPw = '12345';						//[필수] 패스워드
        // memberBaseInfo.memberNm = '홍길동';						//[필수] 이름
        // memberBaseInfo.memberGender = 'G1';						//[필수][공코 : GENDER] 성별
        // memberBaseInfo.memberKind = 'M1';						//[필수][공코 : MEMBER_KIND] 회원종류
        // memberBaseInfo.memberRrn = '8402251212121';				//[필수]주민번호 ('-'제거)
        // memberBaseInfo.memberHpKind = 'H1';						//[필수][공코 : HP_KIND] 휴대전화회사종류
        // memberBaseInfo.memberHp = '01012341234';				//[필수]휴대폰번호 ('-'제거)
        // memberBaseInfo.memberAddr = '경기도 부천시';				//[필수]기본 주소
        // memberBaseInfo.memberAddrDetail = '푸르지오 아파트';		//[필수]상세 주소
        // memberBaseInfo.memberAddrNo = '0321211';				//[필수]우편번호
        // memberBaseInfo.memberSubwayName = '송내역';				//[필수]가까운지하철
        // memberBaseInfo.memberSubwayTime = '05';					//[필수]소요시간
        // memberBaseInfo.height = '180';							//[필수]키
        // memberBaseInfo.weight = '65';							//[필수]몸무게
        // memberBaseInfo.email = 'test@naver.com';				//[필수]이메일
        // memberBaseInfo.photoAttachSeqs = '1|2|3|4';				//[필수]사진 첨부파일 일련번호
        //
        // memberBaseInfo.memberEntercompayInfo = '엔터기업명';	//[선택] memberKind 종류가 M2 일때 소속 회사명 [20180502수정]
        // memberBaseInfo.memberCompayInfo = '1';					//[필수] 기업일련번호 [20180502수정][개인,소속일때 메인회사(어플관리자)로 연결[코드 : 1]]
        // memberBaseInfo.accNo = '1234564545';					//[선택]계좌번호('-'제거)
        // memberBaseInfo.accBankName = '농협';						//[선택][공코 : BANK_NAME]계좌은행명
        // memberBaseInfo.accName = '예금주';						//[선택]계좌예금주
        // memberBaseInfo.companyNoAttachSeqs = '5';				//[선택]사업자등록증 및 증빙서류 첨부파일 일련번호
        // memberBaseInfo.videoAttachSeqs = '6';					//[선택]동영상 첨부파일 일련번호
        //
        // // 회원 상세
        // memberDetailInfo.supportField = 'S1';					//[필수][공코 : SUPPORT_FIELD] 지원분야
        // memberDetailInfo.hopeCast = 'H1';						//[필수][공코 : HOPE_CAST] 희망배역
        // memberDetailInfo.visitDtm = '2018-04-20 18:00:00';		//[필수] 방문가능일 [일시 패턴 : 0000-00-00 00:00:00]
        //
        // memberDetailInfo.job = 'J1';							//[선택][공코 : JOB] 직업
        // memberDetailInfo.hopeCastEtc = '희망배역기타';				//[선택]희망배역기타
        // memberDetailInfo.bodyInfo = 'B1';						//[선택][공코 : BODY_INFO] 신체사항
        // memberDetailInfo.classInfo = 'C1';						//[선택][공코 : CLASS_INFO] 보유방송등급
        // memberDetailInfo.hairState = 'H1';						//[선택][공코 : HAIR_STATE] 헤어상태
        // memberDetailInfo.hairHeight = 'H1';						//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // memberDetailInfo.toothCorrec = 'T1';					//[선택][공코 : TOOTH_CORREC] 치아교정
        // memberDetailInfo.exposureInfo = 'E1';					//[선택][공코 : EXPOSURE_INFO] 노출가능여부
        // memberDetailInfo.tatooInfo = 'T1';						//[선택][공코 : TATOO_INFO] 문신
        // memberDetailInfo.pierInfo = 'P1';						//[선택][공코 : PIER_INFO] 피어싱
        // memberDetailInfo.beardInfo = 'B1';						//[선택][공코 : BEARD_INFO] 수염
        // memberDetailInfo.dimpleInfo = 'D1';						//[선택][공코 : DIMPLE_INFO] 보조개
        // memberDetailInfo.doubleEyeInfo = 'D1';					//[선택][공코 : DOUBLE_EYE_INFO] 눈쌍꺼플
        // memberDetailInfo.bottomsSize = 'B1';					//[선택][공코 : BOTTOMS_SIZE] 하의사이즈
        // memberDetailInfo.topSize = 'T1';						//[선택][공코 : TOP_SIZE] 상의사이즈
        // memberDetailInfo.footSize = '265';						//[선택]발사이즈 [숫자만]
        // memberDetailInfo.driveBicycleYn = 'D1';					//[선택][공코 : DRIVE_BICYCLE_YN] 자전거운전
        // memberDetailInfo.driveCarYn = 'D1';						//[선택][공코 : DRIVE_CAR_YN] 자동차운전
        // memberDetailInfo.driveMotercycleYn = 'D1';				//[선택][공코 : DRIVE_MOTERCYCLE_YN] 오토바이운전
        // memberDetailInfo.smokeYn = 'D1';						//[선택][공코 : SMOKE_YN] 흡연여부
        // memberDetailInfo.dressBaseCount = '2';					//[선택] 기본정장 벌수 [숫자만]
        // memberDetailInfo.dressBaseColor = '검정';				//[선택] 기본정장 색상
        // memberDetailInfo.dressSemiCount = '2';					//[선택] 세미정장 벌수 [숫자만]
        // memberDetailInfo.dressSemiColor = '검정';				//[선택] 세미정장 색상
        // memberDetailInfo.dressBlackShoes = 'Y';					//[선택] 검정구두
        // memberDetailInfo.dressSwimsuit = 'Y';					//[선택] 수영복
        // memberDetailInfo.dressSchoolUniform = 'Y';				//[선택] 교복
        // memberDetailInfo.dressMilitaryBoots = 'Y';				//[선택] 군화
        //
        // // 회원 경력
        // var careerInfo = {};
        // careerInfo.field = 'F1';								//[필수][공코 : FIELD] 경력 분류
        // careerInfo.pgmName = '무한도전';							//[필수]프로그램 명
        // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
        // careerInfo.year = '2018';								//[필수]년도
        // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
        // careerInfo.linesYn = 'Y';								//[필수]대사
        // memberCareerInfo.push(careerInfo);
        //
        // var careerInfo = {};
        // careerInfo.field = 'F2';								//[필수][공코 : FIELD] 경력 분류
        // careerInfo.pgmName = '1박2일';							//[필수]프로그램 명
        // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
        // careerInfo.year = '2018';								//[필수]년도
        // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
        // careerInfo.linesYn = 'N';								//[필수]대사
        // memberCareerInfo.push(careerInfo);
        //
        //
        // var param = {};
        // param.memberBaseInfo = memberBaseInfo;				// 회원 기본 정보
        // param.memberDetailInfo = memberDetailInfo;			// 회원 상세 정보
        // param.memberCareerInfo = memberCareerInfo;			// 회원 경력 정보
        return $io.api(path + '/insertMemberRequest.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업 리스트 조회
     */
    function selectCompanyList() {
        var param = {};
        return $io.api(path + '/selectCompanyList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 아이디 중복 조회
     */
    function selectIdCheck(param) {
        // var param = {};
        // param.memberId = 'admin1';
        return $io.api(path + '/selectIdCheck.login', param)
            .then(function(data){
                // if(data.user.resultData.IDCHEK == 'Y'){
                //     alert('중복');
                // }else{
                //     alert('등록가능');
                // }
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 주민번호 중복 조회
     */
    function selectRrnCheck(param) {
        // var param = {};
        // param.memberRrn = '8402251212121';
        return $io.api(path + '/selectRrnCheck.login', param)
            .then(function(data){
                // if(data.user.resultData.RRNCHEK == 'Y'){
                //     alert('중복');
                // }else{
                //     alert('등록가능');
                // }
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 샘플동영상정보 조회
     */
    function selectSampleVideo() {
        var param = {};
        return $io.api(path + '/selectSampleVideo.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 약관정보 조회
     */
    function selectTermInfo() {
        var param = {};
        return $io.api(path + '/selectTermInfo.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업회원등록
     */
    function insertCompanyInfo(param) {
        // var param = {};
        // param.companyNm = '(주)테스트';							// [필수] 회사명
        // param.companyCeoNm = '홍순이';							// [필수] 대표자명
        // param.companyCeoHpKind = 'H1';							// [필수][공코 :HP_KIND] 대표자 휴대폰 회사
        // param.companyCeoHp = '01012341234';						// [필수][- 제거] 대표자 휴대폰 번호
        // param.companyNo = '123456';								// [필수][- 제거] 회사 사업자번호
        // param.companyMemberName = '담당자';						// [필수] 담당자명
        // param.companyTel1 = '0212341234';						// [필수] 연락처1
        // param.companyTel2 = '0212341234';						// [선택] 연락처2
        // param.companyEmail = 'admin@email.com';					// [필수] 회사(담당자) 이메일 주소
        // param.companyNoAttachSeq = '1';							// [선택] 사업자등록증 첨부일련번호
        // param.companyAccAttachSeq = '1';						// [선택] 통장사본 첨부일련번호
        // param.companyPermissionAttachSeq = '1';					// [선택] 대중문화예술기획업 허가증 첨부일련번호
        // param.companyJobProveAttachSeq = '1';					// [선택] 직업소개소 증빙자료 첨부일련번호
        // param.companyEtcAttachSeq = '1';						// [선택] 기타 증빙서류 첨부일련번호
        // param.accNo = '30212512252';							// [선택][- 제거]계좌번호
        // param.accName = '예금주';									// [선택]예금주
        // param.accBankName = 'H1';								// [선택][공코 : BANK_NAME] 은행명
        // param.companyMemberId = 'mine4001';						// [필수]아이디
        // param.companyMemberPwd = '1234';						// [필수]패스워드
        return $io.api(path + '/insertCompanyInfo.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정보동의 입력
     */
    function insertAgree(param) {
        // var param = {};
        // param.busnLinkKey = '1';			// 회원,기업 일련번호
        // param.agreeKind = 'A1';				// 정보 동의 종류[공코:AGREE_KIND]
        // param.agreeYn = 'Y';				// 동의 여부
        return $io.api(path + '/insertAgree.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 정보동의 조회
     */
    function selectAgreeInfo(param) {
        return $io.api(path + '/mem/selectAgreeInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 쿠폰 리스트
     */
    function selectCouponList(param) {
        return $io.api(path + '/mem/selectCouponList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    
    
}
