'use strict';

api.factory('CompanySvc', CompanySvc);

function CompanySvc($q, $timeout, $io, $message) {

    var path = '/api/comp';

    return {
        insertPgm: insertPgm,
        insertRcut : insertRcut,
        selectMemberList : selectMemberList,
        selectPgmInfo : selectPgmInfo,
        selectPgmRcutListInfo : selectPgmRcutListInfo,
        setInterset : setInterset,
        requestMember : requestMember,
        setBkLst : setBkLst,
        selectMemberDetailInfo : selectMemberDetailInfo,
        selectPgmLstInfo : selectPgmLstInfo,
        selectPgmDetailInfo : selectPgmDetailInfo,
        modifyPgm : modifyPgm,
        deletePgm : deletePgm,
        modifyRcut : modifyRcut,
        deleteRcut : deleteRcut,
        selectRcutList : selectRcutList,
        selectRcutDetailInfo : selectRcutDetailInfo,
        selectPgmHopeCast : selectPgmHopeCast,
        selectRcutrRequestMemberList : selectRcutrRequestMemberList,
        selectRcutrRequestLastSchedulePushInfo : selectRcutrRequestLastSchedulePushInfo,
        sendMemberPush : sendMemberPush,
        procRequest : procRequest,
        updateAttendEndInfo : updateAttendEndInfo,
        updateNoLeaveState : updateNoLeaveState,
        insertMemberReview : insertMemberReview,
        rcutEndProc : rcutEndProc,
        selectMemberJoinInfolist : selectMemberJoinInfolist,
        procJoin : procJoin,
        procPenalTy : procPenalTy,
        reShootProc : reShootProc,
        selectIntersetMemberlist : selectIntersetMemberlist,
        selectRcutMemberlist : selectRcutMemberlist,
        selectRcutEndMemberlist : selectRcutEndMemberlist,
        selectRcutSelelctBoxList : selectRcutSelelctBoxList,
        insertNewCompanyMember : insertNewCompanyMember,
        selectCompanyAuthInfoList : selectCompanyAuthInfoList,
        selectCompanyMemberListInfo : selectCompanyMemberListInfo,
        updateCompanyMember : updateCompanyMember,
        deleteCompanyMember : deleteCompanyMember,
        insertAuthInfo : insertAuthInfo,
        updateAuthInfo : updateAuthInfo,
        deleteAuthInfo : deleteAuthInfo,
        selectCalculateAllPgmList : selectCalculateAllPgmList,
        selectCalculatePgmList : selectCalculatePgmList,
        procCalculate : procCalculate,
        selectCalculateDetailList : selectCalculateDetailList,
        selectQnaList : selectQnaList,
        selectQnaDetailInfo : selectQnaDetailInfo,
        insertQnaAnswer : insertQnaAnswer,
        selectRequstPushList : selectRequstPushList,
        selectCompanyCeoInfo : selectCompanyCeoInfo,
        updateCompanyCeoInfo : updateCompanyCeoInfo,
        selectMenuList : selectMenuList,
        selectRcutrRequestWatingMemberList : selectRcutrRequestWatingMemberList
    };

    /**
     * 프로그램 등록
     */
    function insertPgm(param) {
        // var param = {};
        // param.companySeq = "100000000";							//[필수] 회사 일련번호
        // param.pgmStartDtm = "2018-04-20 00:00:00";		//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmEndDtm = "2018-08-20 00:00:00";			//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmReutField = "P1";						//[필수][공코:PGM_REUT_FIELD] 지원분야
        // param.pgmName = "니혼자사나?";						//[필수] 프로그램명
        // param.companyMemberSeq = "300000000";					//[필수] 로그인한 담당자 일련번호
        // param.mainInfo = "혼자사시는분만";						//[필수] 주요사항
        //
        // param.etcInfo = "연기잘하시는분";						//[선택] 기타사항
        // param.pgmImgAttachSeq = "1";						//[선택] 대표이미지 파일일련번호
        //
        // // 담당자정보
        // var chargeList = [];
        // var chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000000"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 		 		//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000001"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "3000000000"; 				//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // param.chargeList = chargeList;					//[필수] 담당자정보
        return $io.api(path + '/insertPgm.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 모집 등록
     */
    function insertRcut(param) {
        // var param = {};
        // param.pgmBaseSeq = "5";							//[필수] 프로그램 일련번호
        // param.companySeq = "100000000";							//[필수] 회사 일련번호
        // param.pgmStartDtm = "2018-04-20 00:00:00";		//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmEndDtm = "2018-08-20 00:00:00";			//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmHopeCast = "H1";							//[필수][공코 : HOPE_CAST] 배역
        // param.pgmPoint = "경기도 안양시 공원";					//[필수] 촬영장소
        // param.pgmMeetingHour = "07";						//[필수] 촬영시간 (시)
        // param.pgmMeetingMinite = "30";					//[필수] 촬영시간 (분)
        // param.pgmSupplies = "도시락";						//[필수] 준비물
        // param.pgmMainScene = "공원에선 산책하는 씬";				//[필수] 주요장면
        // param.pgmEtcInfo = "없음";							//[필수] 기타사항
        // param.companyMemberSeq = "300000000";				//[필수] 로그인한 담당자 일련번호
        // param.pgmImgAttachSeq = "1";						//[선택] 대표이미지 파일일련번호
        //
        // // 출연인원정보
        // var rcutSubInfoList = [];
        // var rcutSbuInfo = {};
        // rcutSbuInfo.ageMin = '20';						//[필수] 시작나이
        // rcutSbuInfo.ageMax = '60';						//[필수] 종료나이
        // rcutSbuInfo.gender = 'G2';						//[필수][공코: GENDER] 성별
        // rcutSbuInfo.peopleNum = '10';						//[필수] 인원수
        // rcutSbuInfo.dressInfo = 'D1';						//[필수][공코 : DRESS_INFO] 의상
        // rcutSbuInfo.partInfo = '지나가는사람1';				//[필수] 역활
        // rcutSubInfoList.push(rcutSbuInfo);
        //
        // rcutSbuInfo = {};
        // rcutSbuInfo.ageMin = '50';						//[필수] 시작나이
        // rcutSbuInfo.ageMax = '80';						//[필수] 종료나이
        // rcutSbuInfo.gender = 'G2';						//[필수][공코: GENDER] 성별
        // rcutSbuInfo.peopleNum = '20';						//[필수] 인원수
        // rcutSbuInfo.dressInfo = 'D3';						//[필수][공코 : DRESS_INFO] 의상
        // rcutSbuInfo.partInfo = '공원에서산책하는사람';			//[필수] 역활
        // rcutSubInfoList.push(rcutSbuInfo);
        // param.rcutSubInfoList = rcutSubInfoList;			//[필수] 모집연령/성별/인원정보
        //
        //
        // // 담당자정보
        // var chargeList = [];
        // var chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000001"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 		 	//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000002"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 			//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        // param.chargeList = chargeList;							//[필수] 담당자정보
        return $io.api(path + '/insertRcut.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 조회(조회 조건 긴급 포함)
     */
    function selectMemberList(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.emergYn = 'N';									//[선택] 긴급여부
        // param.startDate = '2018-05-21 00:00:00';				//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';				//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchKind = '2';								//[선택] 조회조건 [1:지역 2:이름 3:역명]
        // param.searchData = '홍';								//[선택] 검색어
        //
        // param.supportField = '';								//[선택][공코 : SUPPORT_FIELD] 지원분야
        // param.hopeCast = '';									//[선택][공코 : HOPE_CAST] 희망배역
        // param.startAge = '';									//[선택] 연령 시작
        // param.endAge = '';									//[선택] 연령 끝
        // param.memberGender = '';								//[선택][공코 : MEMBER_GENDER] 성별
        // param.heightStart = '';								//[선택] 키 시작
        // param.heightEnd = '';									//[선택] 키 끝
        // param.weightStart = '';								//[선택] 몸무게 시작
        // param.weightEnd = '';									//[선택] 몸무게 끝
        // param.requestCount = '';								//[선택] 출연횟수
        // param.bodyInfo = '';									//[선택][공코 : BODY_INFO] 신체사항
        // param.classInfo = '';									//[선택][공코 : CLASS_INFO] 보유 방송등급
        // param.hairHeight = '';								//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // param.tatooInfo = '';									//[선택][공코 : TATOO_INFO] 타투여부
        // param.pierInfo = '';									//[선택][공코 : PIER_INFO] 피어싱여부
        return $io.api(path + '/selectMemberList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 진행중 프로그램 조회
     */
    function selectPgmInfo(param) {
        // var param = {};
        // param.pgmReutField= newVal;						//[필수][공코: PGM_REUT_FIELD] 분야
        // param.pgmEndDtmSet = 'Y';							//진행중프로그램만 조회
        return $io.api(path + '/selectPgmInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램일련번호로 모집정보조회
     */
    function selectPgmRcutListInfo(param) {
        // var param = {};
        // param.pgmBaseSeq = '5';
        return $io.api(path + '/selectPgmRcutListInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 관심회원 설정
     */
    function setInterset(param) {
        // var param = {};
        // param.procKind = 'D';									//[필수] (I : 등록, D : 삭제)
        // param.memberSeq = 5;									//[필수] 관심회원 일련번호
        return $io.api(path + '/setInterset.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업 출연자 출연요청
     */
    function requestMember(param) {
        // var param = {};
        // param.memberSeq = '5';									//[필수] 요청 회원 일련번호
        // param.rcutBaseSeq = '17';								//[필수] 요청 모집 일련번호
        return $io.api(path + '/requestMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 블랙리스트 등록 및 해제
     */
    function setBkLst(param) {
        // var param = {};
        // param.procKind = 'I';								//[필수] (I : 등록, D : 삭제)
        // param.memberSeq = 5;									//[필수] 회원 일련번호
        return $io.api(path + '/setBkLst.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 상세조회
     */
    function selectMemberDetailInfo(param) {
        // var param = {};
        // param.memberSeq = '5';									//[필수] 요청 회원 일련번호
        // param.rcutBaseSeq = '17';								//[선택] 모집상세에서 출연자상세조회시 추가항목(출연자확인사항,관리자확인사항)에 필요
        //                                                         // 모집일련번호
        return $io.api(path + '/selectMemberDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 리스트 조회
     */
    function selectPgmLstInfo(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.pgmReutField = '';								//[선택][공코:PGM_REUT_FIELD] 분야
        // param.pgmStateKind = '';								//[선택] '' : 전체 ,  'P1' : 상태 대기, 'P2' : 상태 진행중, 'P3' : 상태 종료
        // param.startDate = '';									//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchData = '';								//[선택] 검색어(프로그램명)
        return $io.api(path + '/selectPgmLstInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 상세 조회
     */
    function selectPgmDetailInfo(param){
        // var param = {};
        // param.pgmBaseSeq = '5';								//[필수] 프로그램 일련번호
        return $io.api(path + '/selectPgmDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 수정
     */
    function modifyPgm(param) {
        // var param = {};
        // param.pgmBaseSeq = '5';								//[필수] 프로그램 일련번호
        //
        // param.pgmStartDtm = "2018-04-20 00:00:00";			//[선택] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmEndDtm = "2018-09-20 00:00:00";				//[선택] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmReutField = "P2";							//[선택][공코:PGM_REUT_FIELD] 지원분야
        // param.pgmName = "니혼자사나?(수정)";						//[선택] 프로그램명
        // param.mainInfo = "혼자사시는분만(수정)";					//[선택] 주요사항
        // param.etcInfo = "연기잘하시는분(수정)";						//[선택] 기타사항
        // param.pgmImgAttachSeq = "1";							//[선택] 대표이미지 파일일련번호
        //
        // // 담당자정보( 기존 정보 모두 삭제후 재입력입니다. 모든 정보 담아서 보내주세요)
        // // 수정이 없을경우에는 [chargeList]담지마세요.
        // var chargeList = [];
        // var chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000001"; 		//[필수] 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000002"; 		//[필수] 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // param.chargeList = chargeList;							//[필수] 담당자정보
        return $io.api(path + '/modifyPgm.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 삭제
     */
    function deletePgm(param) {
        // var param = {};
        // param.pgmBaseSeq = '6';								//[필수] 프로그램 일련번호
        return $io.api(path + '/deletePgm.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 수정
     */
    function modifyRcut(param) {
        // var param = {};
        // param.rcutBaseSeq = '18';								//[필수] 모집 일련번호
        //
        // param.pgmStartDtm = "2018-04-20 00:00:00";		//[선택] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmEndDtm = "2018-10-20 00:00:00";			//[선택] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmHopeCast = "H2";							//[선택][공코 : HOPE_CAST] 배역
        // param.pgmPoint = "경기도 안양시 공원(수정)";				//[선택] 촬영장소
        // param.pgmMeetingHour = "08";						//[선택] 촬영시간 (시)
        // param.pgmMeetingMinite = "40";					//[선택] 촬영시간 (분)
        // param.pgmSupplies = "도시락(수정)";					//[선택] 준비물
        // param.pgmMainScene = "공원에선 산책하는 씬(수정)";			//[선택] 주요장면
        // param.pgmEtcInfo = "없음(수정)";						//[선택] 기타사항
        // param.pgmImgAttachSeq = "1";						//[선택] 대표이미지 파일일련번호
        //
        // // 출연인원정보(수정이 있을 경우 모든 정보를 담아주세요. 삭제 후 새로 입력합니다.)
        // // 수정이 없을 경우 rcutSubInfoList 를 담지 마세요
        // var rcutSubInfoList = [];
        // var rcutSbuInfo = {};
        // rcutSbuInfo.ageMin = '20';						//[필수] 시작나이
        // rcutSbuInfo.ageMax = '60';						//[필수] 종료나이
        // rcutSbuInfo.gender = 'G2';						//[필수][공코: GENDER] 성별
        // rcutSbuInfo.peopleNum = '10';						//[필수] 인원수
        // rcutSbuInfo.dressInfo = 'D1';						//[필수][공코 : DRESS_INFO] 의상
        // rcutSbuInfo.partInfo = '지나가는사람1';				//[필수] 역활
        // rcutSubInfoList.push(rcutSbuInfo);
        //
        // rcutSbuInfo = {};
        // rcutSbuInfo.ageMin = '50';						//[필수] 시작나이
        // rcutSbuInfo.ageMax = '80';						//[필수] 종료나이
        // rcutSbuInfo.gender = 'G2';						//[필수][공코: GENDER] 성별
        // rcutSbuInfo.peopleNum = '20';						//[필수] 인원수
        // rcutSbuInfo.dressInfo = 'D3';						//[필수][공코 : DRESS_INFO] 의상
        // rcutSbuInfo.partInfo = '공원에서산책하는사람';			//[필수] 역활
        // rcutSubInfoList.push(rcutSbuInfo);
        // param.rcutSubInfoList = rcutSubInfoList;			//[필수] 모집연령/성별/인원정보
        //
        //
        // // 담당자정보( 기존 정보 모두 삭제후 재입력입니다. 모든 정보 담아서 보내주세요)
        // // 수정이 없을경우에는 [chargeList]담지마세요.
        // var chargeList = [];
        // var chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000000"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 		 		//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000001"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 				//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        // param.chargeList = chargeList;					//[필수] 담당자정보
        return $io.api(path + '/modifyRcut.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 삭제
     */
    function deleteRcut(param) {
        // var param = {};
        // param.rcutBaseSeq = '13';								//[필수] 출연자 모집 일련번호
        return $io.api(path + '/deleteRcut.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 리스트 조회
     */
    function selectRcutList(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.stateKind = '';									//[선택] '' : 전체 ,  'S1' : 예정(모집중)프로그램, 'S2' : 예정(모집완료)프로그램, 'S3' : 진행중프로그램, 'S4' : 종료프로그램
        // param.startDate = '';									//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchData = '';								//[선택] 검색어(프로그램명)
        return $io.api(path + '/selectRcutList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자모집상세조회
     */
    function selectRcutDetailInfo(param) {
        // var param = {};
        // param.rcutBaseSeq = "1";					//[필수] 모집일련번호
        return $io.api('/api/mem' + '/selectRcutDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 분야에 따른 배역 조회
     */
    function selectPgmHopeCast(param) {
        // var param = {};
        // param.pgmReutField= 'P1';			//[필수][공코: PGM_REUT_FIELD] 분야
        return $io.api(path + '/selectPgmHopeCast.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출연자 리스트 조회 )
     */
    function selectRcutrRequestMemberList(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        // param.rcutBaseSeq = "10";								//[필수] 모집일련번호
        //
        // param.requestState = '';								//[선택] '' : 전체 ,  'R1' : (확정), 'R2' : (신청), 'R3' :  (요청중)
        // param.memberStateCode = '';							//[선택] '' : 전체 ,  출석(지각):A1/무단이탈:A2/기상:A3/출발:A4/출석:A5/미출석:A6/확정:A7/신청:A8/요청중:A9
        // param.searchData = '';								//[선택] 검색어(이름)
        return $io.api(path + '/selectRcutrRequestMemberList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출연자 리스트 조회  - 스케줄변경알림조회)
     */
    function selectRcutrRequestLastSchedulePushInfo(param) {
        // var param = {};
        // param.rcutBaseSeq = "20";								//[필수] 모집일련번호
        return $io.api(path + '/selectRcutrRequestLastSchedulePushInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출연자 리스트 조회  - 알림발송)
     */
    function sendMemberPush(param) {
        // var param = {};
        // param.memberSeq = "5";								//[필수] 회원일련번호
        // param.rcutBaseSeq = "20";								//[필수] 모집일련번호
        // param.msg = "늦지말고 모임장소에 오세요.";						//[필수] 전달 메시지
        return $io.api(path + '/sendMemberPush.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(모집에 대한 요청 취소, 요청수락 ,수락취소)
     */
    function procRequest(param) {
        // var param = {};
        // param.proKind = "P3";									//[필수] 처리종류 (P1: 요청 취소 , P2: 요청수락, P3: 수락취소, P4: 대기인원수락)
        // param.requestInfoSeq = "15";							//[필수] 모집신청일련번호
        return $io.api(path + '/procRequest.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출석, 종료, 지각 입력)
     */
    function updateAttendEndInfo(param) {
        // var param = {};
        // param.companyMemberAttendDtm = "2018-05-16 18:30:00";		//[필수] 출석일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.companyMemberAttendPoint = "용산";					    //[필수] 출석장소
        //
        // param.companyMemberEndDtm = "2018-05-16 23:30:00";			//[필수] 종료일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.companyMemberEndPoint = "용산";						    //[필수] 종료장소
        //
        // param.attendNoState = "A1";									//[필수] 출석,지각 코드 ( 출석 : A1, 지각: A3)
        // param.requestInfoSeq = "15";								    //[필수] 모집신청일련번호
        return $io.api(path + '/updateAttendEndInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(미출석, 무단이탈 처리)
     */
    function updateNoLeaveState(param) {
        // var param = {};
        // param.attendNoLeaveState = "A1";							//[필수][공코:ATTEND_NO_LEAVE_STATE] 출석,지각 코드 ( 미출석 : A1, 무단이탈: A2)
        // param.attendNoLeaveNote = "이사람 도망감";					//[필수] 미출석 및 무단이탈 관리자 확인사항
        // param.requestInfoSeq = "15";								//[필수] 모집신청일련번호
        return $io.api(path + '/updateNoLeaveState.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(평가입력)
     */
    function insertMemberReview(param) {
        // var param = {};
        // param.requestInfoSeq = "15";									//[필수] 모집신청일련번호
        // param.reviewCntn = "연기잘하고 이쁨";							//[필수] 평가 내용
        return $io.api(path + '/insertMemberReview.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출연자 모집 촬영 종료 처리, 출연자 모집 완료 처리 )
     */
    function rcutEndProc(param) {
        // var param = {};
        // param.rcutBaseSeq = "20";									//[필수] 모집일련번호
        // param.rcutBaseState = "R2";									//[필수][공코:RCUT_BASE_STATE] R2 : 모집종료 R3 : 촬영종료
        return $io.api(path + '/rcutEndProc.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 회원가입 승인 , 반려 목록
     */
    function selectMemberJoinInfolist(param) {
        // var param = {};
        // param.memberState = 'M1';								//[필수][공코:MEMBER_STATE] (M1 : 승인대기, M3 : 반려 )
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';									//[선택] 검색어(이름)
        // param.startDate = '';									//[선택] 가입일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';										//[선택] 가입일시 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectMemberJoinInfolist.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 회원가입 승인 반려 처리
     */
    function procJoin(param) {
        // var param = {};
        // param.memberSeq = "5";									//[필수] 회원일련번호
        // param.memberState = "M2";								//[필수][공코:MEMBER_STATE] (M2 : 승인처리 M3: 반려처리)
        return $io.api(path + '/procJoin.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 패널티 부여
     */
    function procPenalTy(param) {
        // var param = {};
        // param.memberSeq = "5";									//[필수] 회원일련번호
        // param.companyStopInfo = "";								//[필수] 패널티 사유
        // param.penaltyCount = "";								    //[필수] 패널티 카운트
        return $io.api(path + '/procPenalTy.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 재촬영
     */
    function reShootProc(param) {
        // var param = {};
        // param.rcutBaseSeq = '';									//[필수] 프로그램 일련번호
        return $io.api(path + '/reShootProc.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 관심출연자 조회
     */
    function selectIntersetMemberlist(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.startDate = '2018-05-21 00:00:00';				    //[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';					//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchKind = '2';									//[선택] 조회조건 [1:지역 2:이름 3:역명]
        // param.searchData = '홍';								    //[선택] 검색어
        //
        // param.supportField = '';								    //[선택][공코 : SUPPORT_FIELD] 지원분야
        // param.hopeCast = '';									    //[선택][공코 : HOPE_CAST] 희망배역
        // param.startAge = '';									    //[선택] 연령 시작
        // param.endAge = '';										//[선택] 연령 끝
        // param.memberGender = '';								    //[선택][공코 : MEMBER_GENDER] 성별
        // param.heightStart = '';									//[선택] 키 시작
        // param.heightEnd = '';									//[선택] 키 끝
        // param.weightStart = '';									//[선택] 몸무게 시작
        // param.weightEnd = '';									//[선택] 몸무게 끝
        // param.requestCount = '';								    //[선택] 출연횟수
        // param.bodyInfo = '';									    //[선택][공코 : BODY_INFO] 신체사항
        // param.classInfo = '';									//[선택][공코 : CLASS_INFO] 보유 방송등급
        // param.hairHeight = '';									//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // param.tatooInfo = '';									//[선택][공코 : TATOO_INFO] 타투여부
        // param.pierInfo = '';									    //[선택][공코 : PIER_INFO] 피어싱여부
        return $io.api(path + '/selectIntersetMemberlist.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연수락/요청중 출연자 조회
     */
    function selectRcutMemberlist(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '홍';								    //[선택] 검색어
        // param.rcutBaseSeq = '';									//[선택] 프로그램 일련번호
        return $io.api(path + '/selectRcutMemberlist.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연완료 회원 조회
     */
    function selectRcutEndMemberlist(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.rcutBaseSeq = '';									//[선택] 프로그램 일련번호
        //
        // param.startDate = '2018-05-21 00:00:00';				    //[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';					//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchKind = '2';									//[선택] 조회조건 [1:지역 2:이름 3:역명]
        // param.searchData = '홍';								    //[선택] 검색어
        //
        // param.supportField = '';								    //[선택][공코 : SUPPORT_FIELD] 지원분야
        // param.hopeCast = '';									    //[선택][공코 : HOPE_CAST] 희망배역
        // param.startAge = '';									    //[선택] 연령 시작
        // param.endAge = '';										//[선택] 연령 끝
        // param.memberGender = '';								    //[선택][공코 : MEMBER_GENDER] 성별
        // param.heightStart = '';									//[선택] 키 시작
        // param.heightEnd = '';									//[선택] 키 끝
        // param.weightStart = '';									//[선택] 몸무게 시작
        // param.weightEnd = '';									//[선택] 몸무게 끝
        // param.requestCount = '';								    //[선택] 출연횟수
        // param.bodyInfo = '';									    //[선택][공코 : BODY_INFO] 신체사항
        // param.classInfo = '';									//[선택][공코 : CLASS_INFO] 보유 방송등급
        // param.hairHeight = '';									//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // param.tatooInfo = '';									//[선택][공코 : TATOO_INFO] 타투여부
        // param.pierInfo = '';									    //[선택][공코 : PIER_INFO] 피어싱여부
        return $io.api(path + '/selectRcutEndMemberlist.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 조회 화면  모집 리스트 조회
     */
    function selectRcutSelelctBoxList() {
        var param = {};
        return $io.api(path + '/selectRcutSelelctBoxList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 신규 담당자 등록
     */
    function insertNewCompanyMember(param) {
        // var param = {};
        //
        // param.companyMemberName = '길순이';							//[필수] 담당자이름
        // param.companyMemberPosition = '과장';						    //[필수] 담당자직책
        // param.companyMemberId = 'test1';							    //[필수] 담당자아이디
        // param.companyMemberPwd = '12345';							//[필수] 담당자패스워드
        // param.companyMemberPhoneKind = 'H1';						    //[필수][공코:HP_KIND] 담당자휴대폰회사정보
        // param.companyMemberPhoneNo = '01012341234';					//[필수] 담당자휴대폰번호
        // param.autnInfoSeq = '5';									    //[필수] 권한일련번호
        //
        //
        //
        // param.companyMemberTel = '0212341234';						//[선택] 담당자전화번호
        // param.companyMemberEmail = 'test@naver.com';			    	//[선택] 담당자이메일
        return $io.api(path + '/insertNewCompanyMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 소속(권한)정보
     */
    function selectCompanyAuthInfoList() {
        var param = {};
        return $io.api(path + '/selectCompanyAuthInfoList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 담당자 리스트조회(상세포함)
     */
    function selectCompanyMemberListInfo(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어(이름)
        // param.autnInfoSeq = '';								//[선택] 권한일련번호
        return $io.api(path + '/selectCompanyMemberListInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 담당자 수정
     */
    function updateCompanyMember(param) {
        // var param = {};
        //
        // param.modifyCompanyMemberSeq = '300000002';					//[필수] 수정담당자일련번호
        //
        // param.companyMemberPosition = '과장(수정)';					    //[선택] 담당자직책
        // param.companyMemberPwd = '12345';							//[선택] 담당자패스워드
        // param.companyMemberPhoneKind = 'H2';						    //[선택][공코:HP_KIND] 담당자휴대폰회사정보
        // param.companyMemberPhoneNo = '01011111111';					//[선택] 담당자휴대폰번호
        // param.autnInfoSeq = '5';									    //[선택] 권한일련번호
        // param.companyMemberTel = '0212341234';						//[선택] 담당자전화번호
        // param.companyMemberEmail = 'tes1t@naver.com';				//[선택] 담당자이메일
        return $io.api(path + '/updateCompanyMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 담당자 삭제
     */
    function deleteCompanyMember(param) {
        // var param = {};
        //
        // param.deleteCompanyMemberSeq = '300000005';				//[필수] 삭제담당자일련번호
        return $io.api(path + '/deleteCompanyMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 권한 등록
     */
    function insertAuthInfo(param) {
        // var param = {};
        //
        // param.authInfoName = '총무부';						    //[필수] 권한이름
        // param.authInfoMenuCode = 'M0_M1';					//[필수] 메뉴권한코드
        // param.authInfoAlarmCode = 'M0_M1';					//[필수] 알림권한코드
        return $io.api(path + '/insertAuthInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 권한 수정
     */
    function updateAuthInfo(param) {
        // var param = {};
        //
        // param.authInfoSeq = '5';								//[필수]권한일련번호
        //
        // param.authInfoName = '총무부(수정';						//[선택] 권한이름
        // param.authInfoMenuCode = 'M0_M2';						//[선택] 메뉴권한코드
        // param.authInfoAlarmCode = 'M0_M2';						//[선택] 알림권한코드
        return $io.api(path + '/updateAuthInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 권한 삭제
     */
    function deleteAuthInfo(param) {
        // var param = {};
        //
        // param.authInfoSeq = '5';								//[필수]권한일련번호
        return $io.api(path + '/deleteAuthInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 종료된 프로그램 목록 조회
     */
    function selectCalculateAllPgmList() {
        var param = {};
        return $io.api(path + '/selectCalculateAllPgmList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정산 필요한 리스트 조회
     */
    function selectCalculatePgmList(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.startDate = '2018-05-21 00:00:00';				    //[선택] 촬영 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';					//[선택] 촬영 일시 [일시 패턴 : 0000-00-00 00:00:00]
        //
        // param.pgmBaseSeq = '';									//[선택] 프로그램일련번호
        // param.searchData = '';									//[선택] 검색어(프로그램명)
        return $io.api(path + '/selectCalculatePgmList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정산 완료 처리
     */
    function procCalculate(param) {
        // var param = {};
        // param.memberSeq = 0;								    	//[필수] 회원 일련번호
        // param.requestInfoSeq = 5;								//[필수] 모집 신청 일련번호
        return $io.api(path + '/procCalculate.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정산 목록 상세 조회
     */
    function selectCalculateDetailList(param) {
        // var param = {};
        // param.rcutBaseSeq = 5;									//[필수] 모집 일련번호
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectCalculateDetailList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * QnA리스트
     */
    function selectQnaList(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;								    		//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectQnaList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * QnA상세
     */
    function selectQnaDetailInfo(param) {
        // var param = {};
        // param.qnaQueSeq = 0;									    //[필수] Qna질문 일련번호
        return $io.api(path + '/selectQnaDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * QnA 답변처리
     */
    function insertQnaAnswer(param) {
        // var param = {};
        // param.qnaQueSeq = '0';										//[필수] Qna질문 일련번호
        // param.detailCntn = '답변내용';								    //[필수] 답변내용
        return $io.api(path + '/insertQnaAnswer.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 보낸 알림 조회
     */
    function selectRequstPushList(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.messageType = '5';								    //[선택][공코: MESSAGE_TYPE] 메시지타입
        // param.rcutBaseSeq = '5';								    //[선택] 출연자 모집일련번호
        return $io.api(path + '/selectRequstPushList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업 정보(대표담당자) 조회
     */
    function selectCompanyCeoInfo() {
        var param = {};
        return $io.api(path + '/selectCompanyCeoInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업 정보(대표담당자) 수정
     */
    function updateCompanyCeoInfo(param) {
        // var param = {};
        // param.companyNoAttachSeq = 0;								//[선택] 사업자등록증첨부번호
        // param.companyAccAttachSeq = 0;								//[선택] 통장사본첨부번호
        // param.companyPermissionAttachSeq = 0;						//[선택] 대중문화예술기획업 허가증 첨부번호
        // param.companyJobProveAttachSeq = 0;							//[선택] 직업소개소 증빙자료 첨부번호
        // param.companyEtcAttachSeq = 0;								//[선택] 기타증빙서류첨부번호
        // param.accNo = 0;											    //[선택]계좌번호('-'제거)
        // param.accBankName = '';										//택][공코 : BANK_NAME]계좌은행명
        // param.accName = '';											//[선택]계좌예금주
        //
        // param.companyTel1 = '';										//[선택] 연락처1
        // param.companyTel2 = '';										//[선택] 연락처2
        // param.companyEmail = '';									    //[선택] 회사(담당자) 이메일 주소
        // param.companyMemeberPwd= '';								    //[선택] 대표담당자패스워드
        return $io.api(path + '/updateCompanyCeoInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 메뉴 정보 조회
     */
    function selectMenuList(param) {
        return $io.api('/api/selectMenuList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 촬영순번대기인원 조회
     */
    function selectRcutrRequestWatingMemberList(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        // param.rcutBaseSeq = '5';								    //[선택] 출연자 모집일련번호
        return $io.api(path + '/selectRcutrRequestWatingMemberList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

}
