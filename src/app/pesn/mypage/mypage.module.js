
angular
.module('pesn.mypage', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.mypage', {
        url: '/mypage',
        abstract: false,
        template: '<div ui-view><h1>pesn.mypage</h1></div>',
        controller: 'mypageCtrl as mypage'
    })

    .state('pesn.mypage.pmMyPage', {
        url: '/pmMyPage',
        templateUrl: 'mypage/pmMyPage/pmMyPage.tpl.html',
        data: { menuTitle : '마이페이지', menuId : 'pmMyPage' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'pmMyPageCtrl as pmMyPage'
    })
    ;   

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
