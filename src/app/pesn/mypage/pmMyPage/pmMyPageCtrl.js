
angular
.module('pesn.mypage')
.controller('pmMyPageCtrl', pmMyPageCtrl);

function pmMyPageCtrl($log, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork, GuestSvc , guestConstant , User) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

var _val = {

};
var _fn = {
  
};

_.assign(vm , {
  mod : {}
});

_.assign(vm , {
  evt : {
    goTo : {
      resume : function(){
        $rootScope.goView('pesn.resume.prList');
      },
      idleSche : function(){
        $rootScope.goView('pesn.schedule.psMng');
      },
      sche : function(){
        $rootScope.goView('pesn.schedule.psList');
      },
      noti : function(){
        $rootScope.goView('pesn.notify.pnList');
      },
      coupList : function(){
        $rootScope.goView('pesn.coupon.pcList');
      },
      coupShop : function(){
        $rootScope.goView('pesn.coupon.pcShop');
      },
      pnlty : function(){
        $rootScope.goView('pesn.schedule.psPnlty');
      }
    }
  }
});

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  $scope.$on('$destroy', destroy);

}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */

}
