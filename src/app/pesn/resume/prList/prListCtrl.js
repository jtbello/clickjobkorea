
angular
.module('pesn.resume')
.controller('prListCtrl', prListCtrl);

function prListCtrl($log, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, $window, $q, $modal, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork, GuestSvc , guestConstant , User) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;
var fn = {
  user :{
    memberSeq : (function(){
      return User.getUserInfoList()[0].MEMBER_SEQ;
    })()
  },
  contact : {
    submit : function(ris , rbs){
      
      //모달 오픈
      var modalInstance = $modal.open({
          templateUrl: 'program/ppLaborContractP/ppLaborContractP.tpl.html',
          controller: 'ppLaborContractPCtrl',
          controllerAs: 'ppLaborContractP',
          scope: $scope,
          resolve: {
              item: function() {
                  return {};
              }
          }
      });

      modalInstance.opened
          .then(function() {
              // 모달 오픈 성공
          })
          .catch(function(data) {
              alert(data);
              // 모달 오픈 실패
          });

      modalInstance.result
          .then(function(result) {
              // 정상 종료
              logger.log('ppLaborContractP Close', result);
              
              let params = {};
              params.memberSeq = User.getUserInfoList()[0].MEMBER_SEQ + '';
              params.requestInfoSeq = ris + '';
              params.rcutBaseSeq = rbs + '';
              GuestSvc.updateRequestCompanyInfo(params)
              .then(function(d){
                logger.log('updateRequestCompanyInfo [SUCCESS]' ,' resultData : ' , d.user);
                fn.tab.get(vm.mod.tab)
              } , function(d){
                logger.log('updateRequestCompanyInfo [FAILED]' ,' resultData : ' , d.user);
                fn.tab.get(vm.mod.tab);
              });

          })
          .catch(function(reason) {
              // 취소
              logger.log('ppLaborContractP Dismiss', reason);
          });
      
    }
  },
  req : {
    cancel : function(ris , rbs){

      if(!$message.confirm('취소 하시겠습니까?')){
        return false;
      }

      let params = {};
      params.memberSeq = User.getUserInfoList()[0].MEMBER_SEQ + '';
      params.requestInfoSeq = ris + '';
      params.rcutBaseSeq = rbs + '';
      return GuestSvc.cancelRequestRcutMember(params)
      .then(function(d){
        logger.log('updateRequestCompanyCancel [SUCCESS]' ,' resultData : ' , d.user);
        return fn.tab.get(vm.mod.tab);
      } , function(d){
        logger.log('updateRequestCompanyCancel [FAILED]' ,' resultData : ' , d.user);
        return fn.tab.get(vm.mod.tab);
      });
    }
  },
  cancel : {

  },
  tab : {
    get : function(type){
      let opt = {
        memberSeq : fn.user.memberSeq + '',
        startNo : vm.mod.page.startNo,
        endNo : vm.mod.page.endNo
      };
      // 신청 현황
      if(type === vm.IDENTIFIER.TAB_SEL.COM_REQUEST){
        opt.searchData = vm.mod.search.searchData;
        return GuestSvc.selectCompanyRequestInfo(opt)
        .then(function(d){
          if(opt.startNo === 0){ vm.mod.list = d.user.resultData.companyRequestInfo; }
          else{
            d.user.resultData.companyRequestInfo.map(item => {
              vm.mod.list.push(item);
            });
          }
          vm.mod.listMaxCnt = d.user.resultData.rcutDataCount;
          if(vm.mod.listMaxCnt === vm.mod.list.length){
            vm.mod.states.moreVisible = false;
          }
          else{
            vm.mod.states.moreVisible = true;
          }

          let deffered = $q.defer();
          if(d.user.result === 'SUCCESS'){
            deffered.resolve(d);
          }
          else{
            deffered.reject(d);
          }
          return deffered.promise;
        } , function(d){
          $window.alert('일시적인 오류가 발생했습니다');
          let deffered = $q.defer();
          deffered.reject(d);

          return deffered.promise;
        });
      }
      // 요청 현황
      else if(type === vm.IDENTIFIER.TAB_SEL.MY_REQUEST){
        opt.searchData = vm.mod.search.searchData;
        opt.startDate = vm.mod.search.startDate;
        opt.endDate = vm.mod.search.endDate;
        return GuestSvc.selectMyRequestInfo(opt)
        .then(function(d){
          if(opt.startNo === 0){ vm.mod.list = d.user.resultData.myRequestInfo; }
          else{
            d.user.resultData.myRequestInfo.map(item => {
              vm.mod.list.push(item);
            });
          }

          vm.mod.listMaxCnt = d.user.resultData.rcutDataCount;
          if(vm.mod.listMaxCnt === vm.mod.list.length){
            vm.mod.states.moreVisible = false;
          }
          else{
            vm.mod.states.moreVisible = true;
          }

          let deffered = $q.defer();
          if(d.user.result === 'SUCCESS'){
            deffered.resolve(d);
          }
          else{
            deffered.reject(d);
          }
          return deffered.promise;
        } , function(d){
          $window.alert('일시적인 오류가 발생했습니다');
          let deffered = $q.defer();
          $q.reject(d);
          return deffered.promise();
        });
      }
      // 취소 현황
      else if(type === vm.IDENTIFIER.TAB_SEL.MY_CANCEL){
        opt.startDate = vm.mod.search.startDate;
        opt.endDate = vm.mod.search.endDate;
        return GuestSvc.selectMyCancelRequestInfo(opt)
        .then(function(d){
          if(opt.startNo === 0){ vm.mod.list = d.user.resultData.myCancelInfo; }
          else{
            d.user.resultData.myCancelInfo.map(item => {
              vm.mod.list.push(item);
            });
          }
          vm.mod.listMaxCnt = d.user.resultData.myCancelInfoCount;
          if(vm.mod.listMaxCnt === vm.mod.list.length){
            vm.mod.states.moreVisible = false;
          }
          else{
            vm.mod.states.moreVisible = true;
          }

          let deffered = $q.defer();
          if(d.user.result === 'SUCCESS'){
            deffered.resolve(d);
          }
          else{
            deffered.reject(d);
          }
          return deffered.promise;
        } , function(d){
          $window.alert('일시적인 오류가 발생했습니다');
          let deffered = $q.defer();
          deffered.reject(d);
          return deffered.promise;
        });
      }
      else{
        console.log('???');
        return undefined;
      }
    },
    init : function(){
      vm.mod.page.startNo = 0;
      vm.mod.page.endNo = 5;
      vm.mod.search.searchData = '';
      vm.mod.search.startDate = '';
      vm.mod.search.endDate = '';
    },
    more : function(){
      // vm.mod.page.startNo = (vm.mod.page.startNo + 1) * vm.mod.page.endNo;
      vm.mod.page.startNo = ((vm.mod.page.startNo / vm.mod.page.endNo) + 1) * vm.mod.page.endNo;
      return fn.tab.get(vm.mod.tab);
    }
  }
};

// MODEL
_.assign(vm, {
  IDENTIFIER : {
    TAB_SEL : {
      COM_REQUEST : 'COM_REQUEST',
      MY_REQUEST : 'MY_REQUEST',
      MY_CANCEL : 'MY_CANCEL'
    }
  },
  mod : {
    tab : 'COM_REQUEST',
    page : {
      startNo : 0,
      endNo : 0,
    },
    search : {
      searchData : '',
      startDate : '',
      endDate : ''
    },
    list : [],
    listMaxCnt : 0,
    states : {
      moreVisible : false
    }
  }
});

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);
  
  if ($stateParams.backupData) {
    _.assign(vm, $stateParams.backupData);
    fn.tab.init();
    fn.tab.get(vm.mod.tab);
  }else{
    vm.mod.tab = vm.IDENTIFIER.TAB_SEL.COM_REQUEST;
    fn.tab.init();
    fn.tab.get(vm.mod.tab);
  }

  
  
  $scope.$on('$destroy', destroy);
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */


// EVENT METHODS
_.assign(vm ,{
  evt : {
    tab : function(type){
      // 같으면 넘어감
      if(type === vm.mod.tab){ return; }
      vm.mod.tab = type;
      fn.tab.init();
      fn.tab.get(type);
      
      console.log(vm.mod);
    },
    cont : {
      submit : function(ris , rbs){
        fn.contact.submit(ris , rbs);
      }
    },
    req : {
      cancel : function(ris , rbs){
        fn.req.cancel(ris, rbs);
      }
    },
    search : function(){
      if(vm.mod.tab !== vm.IDENTIFIER.TAB_SEL.MY_REQUEST){
        vm.mod.search.startDate = '';
        vm.mod.search.endDate = '';
      }
      fn.tab.get(vm.mod.tab);
    },
    loadMore : function(){
      fn.tab.more()
      .then(function(d){
        if(vm.mod.tab === vm.IDENTIFIER.TAB_SEL.COM_REQUEST){
          // com_request
          if(d.user.resultData.rcutDataCount === 0){
            $window.alert('더 이상 조회할 항목이 없습니다');
          }
        }
        else if(vm.mod.tab === vm.IDENTIFIER.TAB_SEL.MY_REQUEST){
          if(d.user.resultData.rcutDataCount === 0){
            $window.alert('더 이상 조회할 항목이 없습니다');
          }
        }
        else if(vm.mod.tab === vm.IDENTIFIER.TAB_SEL.MY_CANCEL){
          if(d.user.resultData.myCancelInfoCount === 0){
            $window.alert('더 이상 조회할 항목이 없습니다');
          }
        }
      });
    },
      go : function(rbs, ris){
          // rbs : RCUT_BASE_SEQ
          $rootScope.goView('pesn.program.ppDetail' , vm , {
              RCUT_BASE_SEQ : rbs,
              REQUST_INFO_SEQ : ris,
              VIEW_TYPES_IDENTIFIERS : vm.IDENTIFIER.TAB_SEL,
              VIEW_TYPES : vm.mod.tab,
              CATE : 'RESUME'
          });
      }
  }
});
}
