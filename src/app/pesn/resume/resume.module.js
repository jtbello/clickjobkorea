
angular
.module('pesn.resume', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.resume', {
        url: '/resume',
        abstract: false,
        template: '<div ui-view><h1>pesn.resume</h1></div>',
        controller: 'resumeCtrl as resume'
    })

    .state('pesn.resume.prList', {
        url: '/prList',
        templateUrl: 'resume/prList/prList.tpl.html',
        data: { menuTitle : '지원 목록 ', menuId : 'prList' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'prListCtrl as prList'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
