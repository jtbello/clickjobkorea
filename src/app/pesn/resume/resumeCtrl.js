
angular
.module('pesn.resume')
.controller('resumeCtrl', resumeCtrl);

function resumeCtrl($log, $scope) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  $scope.$on('$destroy', destroy);
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */

}
