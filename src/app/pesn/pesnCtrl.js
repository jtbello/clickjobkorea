
angular
  .module('pesn')
  .controller('pesnCtrl', pesnCtrl);

function pesnCtrl($log, $scope, $rootScope) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
  }

  vm.fn = {
    evt : {
      mypage : function(){
        $rootScope.goView('pesn.pesnMyPage');
      }
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

}
