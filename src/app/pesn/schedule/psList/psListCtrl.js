
angular
.module('pesn.schedule')
.controller('psListCtrl', psListCtrl)
.filter('dateStr',function(){
  return function(d){
    var _date = new Date(d);
    var str = '';
    str += _date.getFullYear() + '-';
    str += ((_date.getMonth() + 1) < 10 ? '0' + (_date.getMonth() + 1): (_date.getMonth() + 1)) + '-';
    str += (_date.getDate() < 10) ? '0' +  _date.getDate() : _date.getDate();
    return str;
  };
}); 

function psListCtrl($log, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork, GuestSvc , guestConstant , User) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;
  
  var fn = {
    init : function(){
      vm.mod.search.searchData = '';
      vm.mod.search.startDate = '';
      vm.mod.search.endDate = '';
      vm.mod.page.start = 0;
      vm.mod.items = [];
      vm.mod.currentDate = 0;

    },
    get : function(vt){
      if(vt === vm._IDENTIFIERS.VIEWTYPE.WAITING){
        return fn.waiting.get();
      }
      else if(vt === vm._IDENTIFIERS.VIEWTYPE.BEING){
          vm.mod.currentDate = moment(_.now())._i;
        return fn.being.get();
      }
      else if(vt === vm._IDENTIFIERS.VIEWTYPE.FINISHED){
        return fn.finished.get();
      }
    },
    loadMore : function(vt){
      if(vt === vm._IDENTIFIERS.VIEWTYPE.WAITING){
        return fn.waiting.more();
      }
      else if(vt === vm._IDENTIFIERS.VIEWTYPE.BEING){
        return fn.being.more();
      }
      else if(vt === vm._IDENTIFIERS.VIEWTYPE.FINISHED){
        return fn.finished.more();
      }
    },
    waiting : {
      get : function(){
        let params = {};
        params.memberSeq = vm.meta.memberSeq + '';
        params.startNo = vm.mod.page.start ;
        params.endNo = vm.mod.page.cnt ;
        params.searchData = vm.mod.search.keyword;
        params.startDate = vm.mod.search.startDate;
        params.endDate = vm.mod.search.endDate;

        return GuestSvc.selectWatingSchedule(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            if(params.startNo > 0){
              d.user.resultData.watingScheduleInfo.map(item => {
                vm.mod.items.push(item);
              });
            }
            else{
              vm.mod.items = d.user.resultData.watingScheduleInfo;
            }
            vm.mod.itemsMaxCnt = d.user.resultData.watingScheduleInfoCount;
            if(vm.mod.itemsMaxCnt === vm.mod.items.length){
              vm.mod.visibility.more = false;
            }
            else{ 
              vm.mod.visibility.more = true;
             }
            deferred.resolve(d);
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      },
      more : function(){
        if(vm.mod.itemsMaxCnt === vm.mod.items.length){
          $window.alert('더 이상 조회할 항목이 없습니다');
        }
        else {
          vm.mod.page.start += vm.mod.page.cnt;
          fn.wating.get();
        }
      },
      cancel : function(ris){
        return GuestSvc.cancelRequestRcutMember({
          memberSeq : vm.meta.memberSeq + '',
          requestInfoSeq : ris + ''
        })
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            fn.get(vm.mod.viewType)
            .then(function(){
              deferred.resolve(d);
            });
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      }
    },
    being : {
      get : function(){
        let params = {};
        params.memberSeq = vm.meta.memberSeq + '';
        params.startNo = vm.mod.page.start ;
        params.endNo = vm.mod.page.cnt;

        return GuestSvc.selectIngSchedule(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            if(params.startNo > 0){
              d.user.resultData.ingScheduleInfo.map(item =>{
                vm.mod.list.push(item);
              });
            }
            else{
              vm.mod.items = d.user.resultData.ingScheduleInfo;
            }
            vm.mod.itemsMaxCnt = d.user.resultData.ingScheduleInfoCount;
            if(vm.mod.itemsMaxCnt === vm.mod.items.length){
              vm.mod.visibility.more = false;
            }
            else{ 
              vm.mod.visibility.more = true;
             }
            deferred.resolve(d);
          }
          else{
            deferred.reject(d);
          }

          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      },
      update : function(codes , ris){

          $rootScope.searchAddrByGeocode()
              .then(function(data){
                  let deferred = $q.defer();
                  logger.debug('searchAddrByGeocode', data);
                  if(data.status.code === 0){
                      let region = data.results[0].region;    // 현재 주소
                      let point = region.area1.name + ' ' + region.area2.name + ' ' + region.area3.name;

                      let params= {};
                      params.updateKind = codes;
                      params.memberSeq = vm.meta.memberSeq + '';
                      params.requestInfoSeq = ris + '';

                      // 출석 필수 파라미터
                      if(params.updateKind == 'U1'){
                          params.meAttendPoint = point;							//[필수] 출석 클릭한 장소(GPS연동결과값)
                      }

                      // 종료 필수 파라미터
                      if(params.updateKind == 'U2'){
                          params.meEndPoint = point;								//[필수] 종료 클릭한 장소(GPS연동결과값)
                      }

                      // 기상 필수 파라미터
                      if(params.updateKind == 'U3'){
                          params.meGetupPoint = point;							//[필수] 기상 클릭한 장소(GPS연동결과값)
                      }

                      // 출발 필수 파라미터
                      if(params.updateKind == 'U4'){
                          params.meStartPoint = point;							//[필수] 출발 클릭한 장소(GPS연동결과값)
                      }

                      GuestSvc.updateMeInfo(params)
                          .then(function(d){
                              if(d.user.result === 'SUCCESS'){
                                  $message.alert('정보가 저장되었습니다.');
                                  fn.get(vm.mod.viewType)
                                      .then(function(){
                                          deferred.resolve(d);
                                      });
                              }
                              else{
                                  deferred.reject(d);
                              }
                          })
                          .catch(function(d){
                              deferred.reject(d);

                          });
                  }else{
                      $message.alert('현재 위치를 찾을 수 없습니다.');
                      deferred.reject();
                  }
                  return deferred.promise;
              }).catch(function(err){
                  let deferred = $q.defer();
                  logger.debug('searchAddrByGeocode err', err);
                    $message.alert(err.message);
                  deferred.reject(err);
                  return deferred.promise;
              });
      },
      depart : function(ris, item){
        
        if(_.isBlank(item.ME_GETUP_DTM)){
          $message.alert('기상 먼저 진행해주세요.');
          return false;
        }
        return fn.being.update(vm._IDENTIFIERS.ME_INFO.DEPARTURE , ris);
      },
      wakeup : function(ris){
        fn.being.update(vm._IDENTIFIERS.ME_INFO.GET_UP , ris);
      },
      more : function(){
        if(vm.mod.itemsMaxCnt === vm.mod.items.length){
          $window.alert('더 이상 조회할 항목이 없습니다');
        }
        else {
          vm.mod.page.start += vm.mod.page.cnt;
          fn.being.get();
        }
      }
    },
    finished : {
      get : function(){
        let params = {};
        params.memberSeq = vm.meta.memberSeq + '';
        params.startNo = vm.mod.page.start ;
        params.endNo = vm.mod.page.cnt ;
        params.searchData = vm.mod.search.keyword;
        params.startDate = vm.mod.search.startDate;
        params.endDate = vm.mod.search.endDate;

        return GuestSvc.selectEndSchedule(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            if(params.startNo > 0 ){
              d.user.resultData.endScheduleInfo.map(item =>{
                vm.mod.items.push(item);
              });
            }
            else{
              vm.mod.items = d.user.resultData.endScheduleInfo;
            }
            vm.mod.itemsMaxCnt = d.user.resultData.endScheduleInfoCount;
            if(vm.mod.itemsMaxCnt === vm.mod.items.length){
              vm.mod.visibility.more = false;
            }
            else{ 
              vm.mod.visibility.more = true;
             }
          }
          else{
            deferred.reject(d);
          }
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      },
      more : function(){
        if(vm.mod.itemsMaxCnt === vm.mod.items.length){
          $window.alert('더 이상 조회할 항목이 없습니다');
        }
        else {
          vm.mod.page.start += vm.mod.page.cnt;
          fn.finished.get();
        }
      }
    }
  };  
  
  _.assign(vm , {
    _IDENTIFIERS : {
      VIEWTYPE : {
        WAITING : 'WAITING',
        BEING : 'BEING',
        FINISHED : 'FINISHED'
      },
      ME_INFO : {
        ATTENDANCE : 'U1',
        FINISH : 'U2',
        GET_UP : 'U3',
        DEPARTURE : 'U4'
      }
    },
    meta : {
      memberSeq : User.getUserInfoList()[0].MEMBER_SEQ
    },  
    mod : {
      page : {
        start : 0,
        cnt : 5
      },
      search : {
        keyword : '',
        startDate : '',
        endDate : ''
      },
      viewType : '',
      items : [],
      itemsMaxCnt : 0,
      visibility : {
        more : false
      }
    }
  });
  
  _.assign(vm , {
    evt : {
      chngMenu : function(state){
        if(state === vm.mod.viewType){ return ; }

        fn.init();
        vm.mod.viewType = state;
        fn.get(vm.mod.viewType);
      },
      search : function(){
        // fn.init();
        fn.get(vm.mod.viewType);
      },
      loadMore : function(){
        fn.loadMore(vm.mod.viewType);
      },
      go : function(ris , rbs){
        $rootScope.goView('pesn.program.ppDetail' , vm , {
          REQUST_INFO_SEQ : ris,
          RCUT_BASE_SEQ : rbs,
          VIEW_TYPES_IDENTIFIERS : vm._IDENTIFIERS.VIEWTYPE,
          VIEW_TYPES : vm.mod.viewType,
          CATE : 'SCHEDULE'
        });
      },
      noti : function(item){
        let modalInstance = $window.openPop({
          tplUrl : 'popup/pnInsertP/pnInsertP.tpl.html',
          ctrl : 'pnInsertPCtrl',
          ctrlAs : 'pnInsertP',
          data : {
            REQUST_INFO_SEQ : item.REQUST_INFO_SEQ,
            MEMBER_SEQ : vm.meta.memberSeq,
            RCUT_BASE_SEQ : item.RCUT_BASE_SEQ,
            START_DATE : item.PGM_START_DTM2,
            END_DATE : item.PGM_END_DTM2,
            PGM_NM : item.PGM_NAME
          }
        });

        modalInstance.opened.then(function(){
          // 오픈 성공 
        })
        .catch(function(data){
          $window.alert(data);
        });

        modalInstance.result.then(function(data){

          logger.log('pnInsertPopup close' , data);
        })
        .catch(function(reason){
          logger.log('pnInsertPopup Dissmiss' , reason);
        });
        
      },   
      being : {
        wakeup : function(ris){
          console.log(ris);
          fn.being.wakeup(ris);
        },
        depart : function(ris){
          fn.being.depart(ris);
        }
      },
      waiting :{
        cancel : function(ris){
          if($window.confirm('취소가 10회 이상이면 패널티가 부과됩니다. 취소하시겠습니까?')){
            fn.waiting.cancel(ris);
          }
        }
      }
    }
  });
  
  /**
   * Initialize
   */
  init();
  
  function init() {
    logger.debug('init', vm);
  
    $scope.$on('$destroy', destroy);
    
    
    vm.mod.viewType = vm._IDENTIFIERS.VIEWTYPE.WAITING;
    
    if ($stateParams.backupData) {
      vm.mod  = $stateParams.backupData.mod;
    }
    
    fn.get(vm.mod.viewType)
    .then(function(){
      console.log(vm.mod);
    });
    
    
  }
  
  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }
  
  /**
   * Custom functions
   */
  
  }
