
angular
.module('pesn.schedule', [ ])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.schedule', {
        url: '/schedule',
        abstract: false,
        template: '<div ui-view><h1>pesn.schedule</h1></div>',
        controller: 'scheduleCtrl as schedule'
    })

    .state('pesn.schedule.psList', {
        url: '/psList',
        templateUrl: 'schedule/psList/psList.tpl.html',
        data: { menuTitle : '일정 목록 ', menuId : 'psList' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'psListCtrl as psList'
    })
    .state('pesn.schedule.psMng', {
        url: '/psMng',
        templateUrl: 'schedule/psMng/psMng.tpl.html',
        data: { menuTitle : '일정 관리 ', menuId : 'psMng' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'psMngCtrl as psMng'
    })
    .state('pesn.schedule.psPnlty', {
        url: '/psPnlty',
        templateUrl: 'schedule/psPnlty/psPnlty.tpl.html',
        data: { menuTitle : '일정 패널티 목록 ', menuId : 'psPnlty' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'psPnltyCtrl as psPnlty'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
