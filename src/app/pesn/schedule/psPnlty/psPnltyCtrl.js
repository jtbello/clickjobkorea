
angular
.module('pesn.schedule')
.controller('psPnltyCtrl', psPnltyCtrl);

function psPnltyCtrl($log, $scope, User, GuestSvc , ComSvc, $q, $window) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

const fn = {
  get : function(more){
    let params = {};
    params.startNo = vm.mod.page.startNo;
    params.endNo = vm.mod.page.endNo;
    params.penaltyKind = vm.mod.kind.selected;
    params.penaltyStDtm = vm.mod.search.startDate;
    params.penaltyEndDtm = vm.mod.search.endDate;

    return GuestSvc.selectMyPenaltyInfo(params)
    .then(function(d){
      let deferred = $q.defer();
      if(d.user.result === 'SUCCESS'){
        vm.mod.info.penaltyCnt = d.user.resultData.myPenaltyCount;
        if(d.user.resultData.myStopInfo.length > 0){
          vm.mod.info.endDate = d.user.resultData.myStopInfo[0].PENALTY_END_DTM;
          vm.mod.info.desc = d.user.resultData.myStopInfo[0].PENALTY_NAME;
        }
        if(params.startNo <= 0){
          vm.mod.info.list = d.user.resultData.myPenaltyInfo;
        }
        else{
          d.user.resultData.myPenaltyInfo.map(item => {
            vm.mod.info.list.push(item);
          });
        }
        
        vm.mod.info.listCount = d.user.resultData.myPenaltyInfoCount;

        // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
        if (vm.mod.info.list.length < d.user.resultData.myPenaltyInfoCount) {
          vm.showShowMoreButton = true;
        }else{
          vm.showShowMoreButton = false;
        }
        
        
        deferred.resolve(d);
      }
      else {
        deferred.reject(d);
      }
      return deferred.promise;
    } , function(d){
      let deferred = $q.defer();
      deferred.reject(d);
      return deferred.promise;
    });
  },
  more : function(){
    if(vm.mod.page.startNo === 0){
      vm.mod.page.startNo = 1 * vm.mod.page.endNo;
    }
    else{
      vm.mod.page.startNo = ((vm.mod.page.startNo / vm.mod.page.endNo) + 1) * vm.mod.page.endNo;
    }
    return fn.get(true)
    .then(function(d){
      if(d.user.resultData.myPenaltyInfo.length === 0){
        $window.alert('더 이상 조회할 항목이 없습니다');
      }
    });
  },
  getKinds : function(){
    return ComSvc.selectCommonInfo('PENALTY_KIND')
    .then(function(d){
      let deferred = $q.defer();
      if(d.user.result === 'SUCCESS'){
        deferred.resolve(d);
        console.log(d);
        vm.mod.kind.list = d.user.resultData.commonData;
        vm.mod.kind.selected = vm.mod.kind.list[0].COMMON_INFO_VALUE1;
      }
      else{
        deferred.reject(d);
      }
      return deferred.promise;
    } , function(d){
      let deferred = $q.defer();
      deferred.reject(d);
      return deferred.promise;
    });
  }
};

_.assign(vm, {
  user : {
    memberSeq : (function(){
      return User.getUserInfoList()[0].MEMBER_SEQ;
    })()
  },
  mod : {
    page : {
      startNo : 0,
      endNo : 5
    },
    kind : {
      list : [],
      selected : ''
    },
    search : {
      startDate : '',
      endDate : ''
    },
    info : {
      list : [],
      penaltyCnt : 0,
      type : '',
      desc : '',
      endDate : ''
    }
  }
});

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  fn.getKinds()
  .then(fn.get);

  $scope.$on('$destroy', destroy);
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */
_.assign(vm , {
  evt : {
    sel : function(){
      vm.mod.page.startNo = 0;
      vm.mod.page.endNo = 5;
      vm.mod.info.list = [];
      fn.get();
    },
    cal : function(){
      vm.mod.page.startNo = 0;
      vm.mod.page.endNo = 5;
      vm.mod.info.list = [];
      fn.get();
    },
    loadMore : function(){
      fn.more();
    }
  }
})
}
