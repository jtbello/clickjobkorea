angular
.module('pesn.schedule')
.controller('psMngCtrl', psMngCtrl);

function psMngCtrl($window,$log, $scope, GuestSvc, User) {

/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

var calendar = {
  start : '',
  end : '',
  make : function(year, month){
    calendar.start = moment([year,month-1]).startOf('month');
    calendar.end = moment([year,month-1]).endOf('month');

    const startWeek = calendar.start.week();
    const endWeek = calendar.end.week() < startWeek ? calendar.end.weeksInYear() + 1 : calendar.end.week();
    let weeks = [];
    for(var week = startWeek; week<=endWeek ;week++){
      weeks.push({
        week:week,
        days:Array(7).fill(0).map((n, i) => moment([year]).week(week).startOf('week').clone().add(n + i, 'day'))
      });
    }
    return weeks;
  },
  filter : function(cal){
    let mod = [];

    console.log(calendar);
    $window.cal = calendar;
    $window.month = cal;
    for(let i = 0 ; i < cal.length ; i++){
      // weeks
      let week = cal[i];
      let filteredWeek =[];
      for(let   j = 0; j < week.days.length; j++){
        let day = {
          text : week.days[j].dates(),
          moment : week.days[j],
          isContainedThisMonth : week.days[j].isBetween(calendar.start , calendar.end, undefined, true),
          noAttendance : false
        };

        for(let ai = 0 ; ai < vm.mod.cal.noAttDates.length ; ai++){
          console.log(vm.mod.cal.noAttDates[ai].moment.format('YYYY-MM-DD') ,day.moment.format('YYYY-MM-DD'));
          if(vm.mod.cal.noAttDates[ai].moment.format('YYYY-MM-DD') ===
            day.moment.format('YYYY-MM-DD')){
              day.noAttendance = true;
              break;
          }
        }

        if(j === 0 && day.isContainedThisMonth){
          day.cls = 'calendar_cell_red'; 
        }
        else if(j === week.days.length-1 && day.isContainedThisMonth){
          day.cls = 'calendar_cell_blue';
        }
        else{
          day.cls = 'calendar_cell';
        }
        if(!day.isContainedThisMonth){ day.cls = ' back '; }
        
        filteredWeek.push(day);
      }
      mod.push(filteredWeek);
    }
    return mod;
  },
  get :function(year, month){
    console.log('year : ' + year  + ' month : ' + month);
    var _cal = calendar.make(year,month);
    return calendar.filter(_cal);
  },
  noAttend : {
    _chk : function(item){
      let isExists = false;
      for(let i = 0 ; i < vm.mod.cal.notSaved.length ; i++){
        if(vm.mod.cal.notSaved[i].$$hashKey === item.$$hashKey){
          isExists= true;
          break;
        }
      }
      return isExists;
    },
    _insert : function(item){
      vm.mod.cal.notSaved.push(item);
    },
    _remove : function(item){
      for(let i = 0 ; i < vm.mod.cal.notSaved.length ; i++){
        if(vm.mod.cal.notSaved[i].$$hashKey === item.$$hashKey){
          vm.mod.cal.notSaved.splice(i, 1);
        }
      }
    },
    set : function(item){
      if(!calendar.noAttend._chk(item)){
        calendar.noAttend._insert(item);
      }
      else{
        calendar.noAttend._remove(item);
      }
    }
  }
};

_.assign(vm , {
  const : {
    MEMBER_SEQ : -1
  },
  mod : {
    cal : {
      month : [],
      noAttDates : [],
      notSaved : [],
      sel :{
        year : 2018,
        month : 1
      },
      years : [],
      months : [1,2,3,4,5,6,7,8,9,10,11,12]
    }
  }
});

_.assign(vm, {
  evt : {
    sel : function(type,old){
      // 개월 변경 전에 뭔가 처리해야하는 경우 
      // if(vm.mod.cal.notSaved.length > 0){
      //   if(type === 'year'){
      //     vm.mod.cal.sel.year = Number(old);
      //   }
      //   else if(type === 'month'){
      //     vm.mod.cal.sel.month = Number(old);
      //   }
      // }
      // else{
        
      // }
      vm.mod.cal.month = calendar.get(vm.mod.cal.sel.year , vm.mod.cal.sel.month);
    },
    cal : {
      setNoAttend : function(item){
        item.noAttendance = !item.noAttendance;
        calendar.noAttend.set(item);
      },  
      save : function(){
        if(vm.mod.cal.notSaved.length === 0){
          $window.alert('변경된 사항이 없습니다');
          return;
        }
        if($window.confirm('변경된 ' + vm.mod.cal.notSaved.length + '개 일정을 저장하시겠습니까?')){
          let data = [];
          while(vm.mod.cal.notSaved.length > 0){
            let item = vm.mod.cal.notSaved.pop();  
            let params = {
              busnLinkKey : vm.const.MEMBER_SEQ + '',
              delIns : item.noAttendance ? 'I' : 'D',
              scheduleDtm : item.moment.format('YYYY-MM-DD 00:00:00')
            };
            data.push(params);
          }
          
          GuestSvc.insertNoSchedule({
            scheduleInfo : data
          })
          .then(function(d){
            console.log('SUCCESSS');
            if(d.user.result === 'SUCCESS'){
              $window.alert(d.user.resultMessage);
            }
            return this;
            // vm.mod.cal.month = calendar.get(vm.mod.cal.sel.year,vm.mod.cal.sel.month);
          } , function(d){
            console.log('FAILED');
            $window.alert(d.user.resultMessage);
            // vm.mod.cal.month = calendar.get(vm.mod.cal.sel.year,vm.mod.cal.sel.month);
          })
          .then(function(){
            return GuestSvc.selectNoSchedule({
              busnLinkKey : vm.const.MEMBER_SEQ.toString()
            });
          })
          .then(function(d){
            if(d.user.result === 'SUCCESS'){
              vm.mod.cal.noAttDates = d.user.resultData.noSchedule;
              console.log(vm.mod.cal.noAttDates);
              vm.mod.cal.noAttDates.map((item) =>{
                item.moment = moment(item.SCHEDULE_DTM2);
              });

              vm.mod.cal.month = calendar.get(vm.mod.cal.sel.year,vm.mod.cal.sel.month);
            }
            else{
              $window.alert(d.user.resultMessage);
            }
          }, function(d){
            $window.alert('일시적인 오류가 발생했습니다.');
          });
        }  
      }
    }
  }
});

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  var _current = moment();
  vm.mod.cal.years = [_current.years(), _current.years() + 1];
  vm.mod.cal.sel.year = vm.mod.cal.years[0];
  vm.mod.cal.sel.month = vm.mod.cal.months[_current.months()];

  // GuestSvc.selectNoSchedule({
  //   busnLinkKey : '1'
  // }).then(function(d){
  //   console.log(d);
  // });

  vm.const.MEMBER_SEQ = User.getUserInfoList()[0].MEMBER_SEQ;
  GuestSvc.selectNoSchedule({
    busnLinkKey : vm.const.MEMBER_SEQ.toString()
  })
  .then(function(d){
    if(d.user.result === 'SUCCESS'){
      vm.mod.cal.noAttDates = d.user.resultData.noSchedule;
      console.log(vm.mod.cal.noAttDates);
      vm.mod.cal.noAttDates.map((item) =>{
        item.moment = moment(item.SCHEDULE_DTM2);
      });

      vm.mod.cal.month = calendar.get(vm.mod.cal.sel.year,vm.mod.cal.sel.month);
    }
    else{
      $window.alert(d.user.resultMessage);
    }
  }, function(d){
    $window.alert('일시적인 오류가 발생했습니다.');
  });
  

  $scope.$on('$destroy', destroy);
}



/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}


/**
 * Custom functions
 */

}
