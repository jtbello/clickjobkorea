
angular
  .module('pesn.program')
  .controller('ppLaborContractPCtrl', ppLaborContractPCtrl);

function ppLaborContractPCtrl($log, $rootScope, $scope, $modal, $modalInstance, $message, item, ComSvc, GuestSvc, ngspublic) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        view1 : '',
        view2 : '',
        viewTitle1 : '',
        viewTitle2 : '',
        viewChk1 : false,
        viewChk2 : false

    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        confirm : confirm,
    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        // 근로계약서및 정보동의 공통 코드 조회
      ComSvc.selectCommonInfo('REQUEST_AGREE')
      .then(function(data){
          _.forEach(data.user.resultData.commonData, function(obj){
            if(obj.COMMON_INFO_VALUE1 == 'R1'){
                vm.view1 = obj.COMMON_INFO_VALUE2;
                vm.viewTitle1 = obj.COMMON_INFO_NAME;
            }
            if(obj.COMMON_INFO_VALUE1 == 'R2'){
                vm.view2 = obj.COMMON_INFO_VALUE2;
                vm.viewTitle2 = obj.COMMON_INFO_NAME;
            }
          });
      })
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 정상 종료
     */
    function confirm(){

        if(!vm.viewChk1){
            $message.alert(vm.viewTitle1 + '을 확인 해 주세요.');
            return false;
        }
        
        if(!vm.viewChk2){
            $message.alert(vm.viewTitle2 + '을 확인 해 주세요.');
            return false;
        }

        $modalInstance.close();
    }


}
