
angular
.module('pesn.program')
.controller('ppDetailCtrl', ppDetailCtrl);

function ppDetailCtrl($log, $rootScope, $scope, $modal , $state, $stateParams, $timeout, $user, $message, $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork, GuestSvc , guestConstant, User, $filterSet, $doryCall)  {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

var _fn = {
  // common get 
  get : function(){
    if(vm.mod.cate === vm.IDENTIFIERS.VIEW_CATEGORY.PROGRAM){
      return _fn.prog.get(); 
    }
    else if(vm.mod.cate === vm.IDENTIFIERS.VIEW_CATEGORY.SCHEDULE){
      return _fn.sche.get();
    }
    else if(vm.mod.cate === vm.IDENTIFIERS.VIEW_CATEGORY.RESUME){
        return _fn.resume.get();
    }
    else if(vm.mod.cate === vm.IDENTIFIERS.VIEW_CATEGORY.STANDBY){
      return _fn.standby.get();
    }
    else{
      let deferred = $q.defer();
      return deferred.reject(vm.mod.cate);
    }
  },
  callNumber : function(number){
    if(!$message.confirm($filterSet.fmtPhone(number) + '\n전화 하시겠습니까?')) return false;
    $doryCall.callNumber(number);
  },
  prog : {
    get : function(){
      return GuestSvc.selectRcutDetailInfo({
        rcutBaseSeq : vm.mod.prog.RCUT_BASE_SEQ
      }).then(function(d){
        let deferred = $q.defer();

        if(d.user.result === 'SUCCESS'){
          vm.mod.prog.data.imgAttachInfo = d.user.resultData.imgAttachInfo;
          vm.mod.prog.data.requestInfo = d.user.resultData.requestInfo;
          
          // vm.mod.prog.data.subPeopleInfo = d.user.resultData.subPeopleInfo;
          // vm.mod.prog.data.chargeInfo = d.user.resultData.chargeInfo;
          // vm.mod.prog.data.detailBaseInfo = d.user.resultData.detailBaseInfo;
          // vm.mod.prog.data.modifyInfo = d.user.resultData.modifyInfo;

          vm.mod.data.subPeopleInfo = d.user.resultData.subPeopleInfo;
          vm.mod.data.chargeInfo = d.user.resultData.chargeInfo;
          vm.mod.data.detailBaseInfo = d.user.resultData.detailBaseInfo;
          vm.mod.data.modifyInfo = d.user.resultData.modifyInfo;
          
          // 요청정보없을 경우 [출연요청]
           if(!vm.mod.prog.data.requestInfo || vm.mod.prog.data.requestInfo.length <=0){
             vm.mod.prog.states.submit = true;
             vm.mod.prog.states.cancel = false;
             vm.mod.prog.states.acceptCancel = false;
             vm.mod.prog.states.accept = false;
             console.log('not visible 2');
           }
           else{
             // 기업 요청이면서 대기중 [수락]
            vm.mod.prog.states.accept = vm.mod.prog.data.requestInfo[0].REQUEST_KIND === 'R1' && 
                                        vm.mod.prog.data.requestInfo[0].REQUEST_STATE === 'R1';
            // 요청 버튼 비활성화
            vm.mod.prog.states.submit = false;
            // 개인요청이면서 요청이거나요청수락일경우 [신청취소]
            vm.mod.prog.states.cancel = (vm.mod.prog.data.requestInfo[0].REQUEST_KIND === 'R2' &&
                                        vm.mod.prog.data.requestInfo[0].REQUEST_STATE === 'R1') ||
                                        (vm.mod.prog.data.requestInfo[0].REQUEST_KIND === 'R2' &&
                                            vm.mod.prog.data.requestInfo[0].REQUEST_STATE === 'R2');
            // 기업 요청이면서 수락한 상태 [수락취소]
            vm.mod.prog.states.acceptCancel = vm.mod.prog.data.requestInfo[0].REQUEST_KIND === 'R1' &&
                                              vm.mod.prog.data.requestInfo[0].REQUEST_STATE === 'R2';
            
            
            // 요청정보가 있을경우 일련번호 업데이트
            vm.mod.prog.REQUST_INFO_SEQ = vm.mod.prog.data.requestInfo[0].REQUST_INFO_SEQ;
            
            // console.log('visible');
            // console.log(vm.mod.prog.states);
          }
          deferred.resolve(d.user);
        }
        else{
          deferred.reject(d.user);
        }

        return deferred.promise;
      }).catch(function(d){
        let deferred = $q.defer();
        deferred.reject(d.user);
        return deferred.promise;
      });
    },
    showRcrutReqPopup : function(){
        logger.log('showRcrutReqPopup Open');

        var params = {};
        params.memberSeq = vm.mod.meta.memberSeq + '';
        params.rcutBaseSeq = vm.mod.prog.RCUT_BASE_SEQ + '';
        params.subPeopleInfo = vm.mod.data.subPeopleInfo;

        //모달 오픈
        var modalInstance = $modal.open({
            templateUrl: 'program/ppRcrutReqP/ppRcrutReqP.tpl.html',
            controller: 'ppRcrutReqPCtrl',
            controllerAs: 'ppRcrutReqP',
            scope: $scope,
            resolve: {
                item: function() {
                    return params;
                }
            }
        });

        modalInstance.opened
            .then(function() {
                // 모달 오픈 성공
            })
            .catch(function(data) {
                alert(data);
                // 모달 오픈 실패
            });

        modalInstance.result
            .then(function(result) {
                // 정상 종료
                logger.log('showRcrutReqPopup Close', result);
                _fn.prog.get();
            })
            .catch(function(reason) {
                // 취소
                logger.log('showRcrutReqPopup Dismiss', reason);
            });

    },
    submit : function(){

      let params = {};
      params.memberSeq = vm.mod.meta.memberSeq + '';
      params.rcutBaseSeq = vm.mod.prog.RCUT_BASE_SEQ + '';

      return GuestSvc.requestRcutMember(params)
      .then(function(d){
        let deferred = $q.defer();
        if(d.user.result ==='SUCCESS'){
          _fn.prog.get()
          .then(function(){
            deferred.resolve(d);
          });
        }
        else{
          logger.log('requestRcutMember Error' , '' , d);
          deferred.reject(d);
        }
      })
      .catch(function(d){
        logger.log('requestRcutMember Error' , '' , d);

        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.promise;
      });
    },
    cancel : function(){
      let params = {};
      params.memberSeq = vm.mod.meta.memberSeq + '';
      params.rcutBaseSeq = vm.mod.prog.RCUT_BASE_SEQ + '';
      params.requestInfoSeq = vm.mod.prog.REQUST_INFO_SEQ + '';
      
      return GuestSvc.cancelRequestRcutMember(params)
      .then(function(d){
        let deferred = $q.defer();
        if(d.user.result ==='SUCCESS'){
          _fn.prog.get()
          .then(function(){
            deferred.resolve(d);
          });
        }
        else{
          logger.log('requestRcutMember Error' , '' , d);
          deferred.reject(d);
        }
      })
      .catch(function(d){
        logger.log('requestRcutMember Error' , '' , d);

        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.promise;
      });
    },
    accept : function(){

      //모달 오픈
      var modalInstance = $modal.open({
        templateUrl: 'program/ppLaborContractP/ppLaborContractP.tpl.html',
        controller: 'ppLaborContractPCtrl',
        controllerAs: 'ppLaborContractP',
        scope: $scope,
        resolve: {
            item: function() {
                return {};
            }
        }
    });

    modalInstance.opened
        .then(function() {
            // 모달 오픈 성공
        })
        .catch(function(data) {
            alert(data);
            // 모달 오픈 실패
        });

    modalInstance.result
        .then(function(result) {
            // 정상 종료
            logger.log('ppLaborContractP Close', result);
            
            let params = {};
            params.memberSeq = vm.mod.meta.memberSeq + '';
            params.rcutBaseSeq = vm.mod.prog.RCUT_BASE_SEQ + '';
            params.requestInfoSeq = vm.mod.prog.REQUST_INFO_SEQ + '';
      
            GuestSvc.updateRequestCompanyInfo(params)
            .then(function(d){
              if(d.user.result ==='SUCCESS'){
                _fn.prog.get();
              }
              else{
                logger.log('updateRequestCompanyInfo Error' , '' , d);
              }
            })
            .catch(function(d){
              logger.log('updateRequestCompanyInfo Error' , '' , d);
            });

        })
        .catch(function(reason) {
            // 취소
            logger.log('ppLaborContractP Dismiss', reason);
        });
    },
    acceptCancel : function(){
      let params = {};
      params.memberSeq = vm.mod.meta.memberSeq + '';
      params.rcutBaseSeq = vm.mod.prog.RCUT_BASE_SEQ + '';
      params.requestInfoSeq = vm.mod.prog.REQUST_INFO_SEQ + '';
      
      return GuestSvc.updateRequestCompanyCancel(params)
      .then(function(d){
        let deferred = $q.defer();
        if(d.user.result ==='SUCCESS'){
          // _fn.prog.get()
          // .then(function(){
          //   deferred.resolve(d);
          // });
          $message.alert('수락이 취소되었습니다.');
          $rootScope.prevView();
        }
        else{
          logger.log('requestRcutMember Error' , '' , d);
          deferred.reject(d);
        }
      })
      .catch(function(d){
        logger.log('requestRcutMember Error' , '' , d);

        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.promise;
      });
    },
    noti : {
      open : function(){
        let modalInstance = $window.openPop({
          tplUrl : 'popup/pnInsertP/pnInsertP.tpl.html',
          ctrl : 'pnInsertPCtrl',
          ctrlAs : 'pnInsertP',
          data : {
            REQUST_INFO_SEQ : vm.mod.prog.REQUST_INFO_SEQ,
            MEMBER_SEQ : vm.mod.meta.memberSeq,
            RCUT_BASE_SEQ : vm.mod.prog.RCUT_BASE_SEQ,
            START_DATE : vm.mod.data.detailBaseInfo.PGM_START_DTM2,
            END_DATE : vm.mod.data.detailBaseInfo.PGM_END_DTM2,
            PGM_NM : vm.mod.data.detailBaseInfo.PGM_NAME
          }
        });

        modalInstance.opened.then(function(){
          // 오픈 성공 
        })
        .catch(function(data){
          $window.alert(data);
        });

        modalInstance.result.then(function(data){

          logger.log('pnInsertPopup close' , data);
        })
        .catch(function(reason){
          logger.log('pnInsertPopup Dissmiss' , reason);
        });
        
      }
    }
  },
  sche : {
    get : function(){
      let params = {};
      params.rcutBaseSeq = vm.mod.sche.RCUT_BASE_SEQ;
      params.requestInfoSeq = vm.mod.sche.REQUST_INFO_SEQ;

      return GuestSvc.selectScheduleDetail(params)
      .then(function(d){
        let deferred = $q.defer();

        if(d.user.result === 'SUCCESS'){
          
          vm.mod.sche.data.managerDetailInfo = d.user.resultData.managerDetailInfo;
          
          // vm.mod.sche.data.subPeopleInfo = d.user.resultData.subPeopleInfo;
          // vm.mod.sche.data.chargeInfo = d.user.resultData.chargeInfo;
          // vm.mod.sche.data.detailBaseInfo = d.user.resultData.detailBaseInfo;
          // vm.mod.sche.data.modifyInfo = d.user.resultData.modifyInfo;

          vm.mod.data.subPeopleInfo = d.user.resultData.subPeopleInfo;
          vm.mod.data.chargeInfo = d.user.resultData.chargeInfo;
          vm.mod.data.detailBaseInfo = d.user.resultData.detailBaseInfo;
          vm.mod.data.modifyInfo = d.user.resultData.modifyInfo;

          var realEndDtm = vm.mod.data.detailBaseInfo.PGM_REAL_END_DTM;
          if(!_.isEmpty(realEndDtm) && realEndDtm != null && realEndDtm != '0000.00.00 00:00:00' && realEndDtm != 'null'){
            vm.mod.data.detailBaseInfo.realEndDtm = true;
          }else{
            vm.mod.data.detailBaseInfo.realEndDtm = false;
          }

          deferred.resolve(d);
        }
        else{
          deferred.reject(d);
        }
        return deferred.promise;
      })
      .catch(function(d){
        logger.log('requestRcutMember Error' , '' , d);

        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.promise;
      });
    },
    update : function(codes , ris){
      let params= {};
      // ATTENDANCE : 'ATTENDANCE',
      // FINISH : 'FINISH',
      // GET_UP : 'GET_UP',
      // DEPARTURE : 'DEPARTURE'
      if(!$message.confirm('처리하시겠습니까?')) return false;
      

      return $rootScope.searchAddrByGeocode()
      .then(function(data){
        logger.debug('searchAddrByGeocode', data);
        if(data.status.code === 0){
          let region = data.results[0].region;    // 현재 주소
          let point = region.area1.name + ' ' + region.area2.name + ' ' + region.area3.name;

          if(codes === vm.IDENTIFIERS.ME_INFO.ATTENDANCE){
            params.updateKind = 'U1';
            params.meAttendPoint = point;
          }
          else if(codes === vm.IDENTIFIERS.ME_INFO.FINISH){
            params.updateKind = 'U2';
            params.meEndPoint = point;								//[필수] 종료 클릭한 장소(GPS연동결과값)
          }
          else if(codes === vm.IDENTIFIERS.ME_INFO.GET_UP){
            params.updateKind = 'U3';
            params.meGetupPoint = point;							//[필수] 기상 클릭한 장소(GPS연동결과값)
          }
          else if(codes === vm.IDENTIFIERS.ME_INFO.DEPARTURE){
            params.updateKind = 'U4';
            params.meStartPoint = point;							//[필수] 출발 클릭한 장소(GPS연동결과값)
          }
        }        

        params.memberSeq = vm.mod.meta.memberSeq + '';
        params.requestInfoSeq = ris + '';

        return GuestSvc.updateMeInfo(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            _fn.sche.get()
            .then(function(){
              $message.alert('정상적으로 처리되었습니다.');
              deferred.resolve(d);
            });
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            _fn.sche.get()
            .then(function(){
              deferred.resolve(d);
            });
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        });
      })
      .catch(function(d){
        let deferred = $q.defer();
//          $message.alert(d.message);
        if(d.user.result === 'SUCCESS'){
          _fn.sche.get()
          .then(function(){
            deferred.resolve(d);
          });
        }
        else{
          deferred.reject(d);
        }
        return deferred.promise;
      });
      
    },
    attend : function(ris, rbs){
      if(_.isBlank(vm.mod.sche.data.managerDetailInfo[0].ME_STRART_DTM)){
        $message.alert('출발 먼저 진행해주세요.');
        return false;
      }
      return _fn.sche.update(vm.IDENTIFIERS.ME_INFO.ATTENDANCE , ris);
    },
    finish : function(ris, rbs){
      if(_.isBlank(vm.mod.sche.data.managerDetailInfo[0].ME_ATTEND_DTM)){
        $message.alert('출석 먼저 진행해주세요.');
        return false;
      }
      return _fn.sche.update(vm.IDENTIFIERS.ME_INFO.FINISH , ris);
    },
    depart : function(ris, rbs){
      if(_.isBlank(vm.mod.sche.data.managerDetailInfo[0].ME_GETUP_DTM)){
        $message.alert('기상 먼저 진행해주세요.');
        return false;
      }
      return _fn.sche.update(vm.IDENTIFIERS.ME_INFO.DEPARTURE , ris);
    },
    wakeup : function(ris, rbs){
      return _fn.sche.update(vm.IDENTIFIERS.ME_INFO.GET_UP , ris);
    },
    modChk : function(rbs){

        let params = {};
        params.rcutBaseSeq = rbs + '';

        return GuestSvc.updateModifyRcutPushInfo(params)
            .then(function(d){
                let deferred = $q.defer();

                if(d.user.result === 'SUCCESS'){
                    $message.alert('스케쥴 변경을 확인하였습니다.');
                    deferred.resolve(d);
                }
                else{
                    deferred.reject(d);
                }
                return deferred.promise;
            })
            .catch(function(d){
                logger.log('updateModifyRcutPushInfo Error' , '' , d);

                let deferred = $q.defer();
                deferred.reject(d);
                return deferred.promise;
            });
    },
    cancel : function(ris){
      return GuestSvc.cancelRequestRcutMember({
        memberSeq : vm.mod.meta.memberSeq + '',
        requestInfoSeq : ris + ''
      })
      .then(function(d){
        let deferred = $q.defer();
        if(d.user.result === 'SUCCESS'){

          //   vm.mod.sche.states.cancel = false;
          //   vm.mod.sche.states.attendance = false;

          // _fn.sche.get()
          // .then(function(){
          //   deferred.resolve(d);
          // });
          $message.alert('수락이 취소되었습니다.');
          $rootScope.prevView();
        }
        else{
          deferred.reject(d);
        }
        return deferred.promise;
      })
      .catch(function(d){
        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.promise;
      });
    },
    noti : {
      open : function(){
        console.log(vm.mod.data.detailBaseInfo);
        let modalInstance = $window.openPop({
          tplUrl : 'popup/pnInsertP/pnInsertP.tpl.html',
          ctrl : 'pnInsertPCtrl',
          ctrlAs : 'pnInsertP',
          data : {
            REQUST_INFO_SEQ : vm.mod.sche.REQUST_INFO_SEQ,
            MEMBER_SEQ : vm.mod.meta.memberSeq,
            RCUT_BASE_SEQ : vm.mod.sche.RCUT_BASE_SEQ,
            START_DATE : vm.mod.data.detailBaseInfo.PGM_START_DTM2,
            END_DATE : vm.mod.data.detailBaseInfo.PGM_END_DTM2,
            PGM_NM : vm.mod.data.detailBaseInfo.PGM_NAME
          }
        });

        modalInstance.opened.then(function(){
          // 오픈 성공 
        })
        .catch(function(data){
          $window.alert(data);
        });

        modalInstance.result.then(function(data){

          logger.log('pnInsertPopup close' , data);
        })
        .catch(function(reason){
          logger.log('pnInsertPopup Dissmiss' , reason);
        });
        
      }
    }
  },
  resume : {
      get : function(){
          return GuestSvc.selectRcutDetailInfo({
              rcutBaseSeq : vm.mod.resume.RCUT_BASE_SEQ,
              requestInfoSeq : vm.mod.resume.REQUST_INFO_SEQ
          }).then(function(d){
              let deferred = $q.defer();

              if(d.user.result === 'SUCCESS'){
                  vm.mod.resume.data.imgAttachInfo = d.user.resultData.imgAttachInfo;
                  vm.mod.resume.data.requestInfo = d.user.resultData.requestInfo;

                  // vm.mod.resume.data.subPeopleInfo = d.user.resultData.subPeopleInfo;
                  // vm.mod.resume.data.chargeInfo = d.user.resultData.chargeInfo;
                  // vm.mod.resume.data.detailBaseInfo = d.user.resultData.detailBaseInfo;
                  // vm.mod.resume.data.modifyInfo = d.user.resultData.modifyInfo;

                  vm.mod.data.subPeopleInfo = d.user.resultData.subPeopleInfo;
                  vm.mod.data.chargeInfo = d.user.resultData.chargeInfo;
                  vm.mod.data.detailBaseInfo = d.user.resultData.detailBaseInfo;
                  vm.mod.data.modifyInfo = d.user.resultData.modifyInfo;

                  // 요청정보없을 경우 [출연요청]
                  if(!vm.mod.resume.data.requestInfo || vm.mod.resume.data.requestInfo.length <=0){
                      vm.mod.resume.states.accept = false;
                      vm.mod.resume.states.cancel = false;
                      console.log('not visible 2');
                  }
                  else{
                      // 기업 요청이면서 대기중 [수락]
                      vm.mod.resume.states.accept = vm.mod.resume.data.requestInfo[0].REQUEST_KIND === 'R1' &&
                          vm.mod.resume.data.requestInfo[0].REQUEST_STATE === 'R1';

                      // 개인요청이면서 요청이거나요청수락일경우 [신청취소]
                      vm.mod.resume.states.cancel = (vm.mod.resume.data.requestInfo[0].REQUEST_KIND === 'R2' &&
                          vm.mod.resume.data.requestInfo[0].REQUEST_STATE === 'R1') ||
                          (vm.mod.resume.data.requestInfo[0].REQUEST_KIND === 'R2' &&
                              vm.mod.resume.data.requestInfo[0].REQUEST_STATE === 'R2');

                      // 요청정보가 있을경우 일련번호 업데이트
                      vm.mod.resume.REQUST_INFO_SEQ = vm.mod.resume.data.requestInfo[0].REQUST_INFO_SEQ;

                      // console.log('visible');
                      // console.log(vm.mod.resume.states);
                  }
                  deferred.resolve(d.user);
              }
              else{
                  deferred.reject(d.user);
              }

              return deferred.promise;
          }).catch(function(d){
              let deferred = $q.defer();
              deferred.reject(d.user);
              return deferred.promise;
          });
      },
      cancel : function(){
          let params = {};
          params.memberSeq = vm.mod.meta.memberSeq + '';
          params.rcutBaseSeq = vm.mod.resume.RCUT_BASE_SEQ + '';
          params.requestInfoSeq = vm.mod.resume.REQUST_INFO_SEQ + '';

          return GuestSvc.cancelRequestRcutMember(params)
              .then(function(d){
                  let deferred = $q.defer();
                  if(d.user.result ==='SUCCESS'){
                      // _fn.resume.get()
                      //     .then(function(){
                      //         deferred.resolve(d);
                      //     });
                      $message.alert('신청이 취소되었습니다.');
                      $rootScope.prevView();
                  }
                  else{
                      logger.log('requestRcutMember Error' , '' , d);
                      deferred.reject(d);
                  }
              })
              .catch(function(d){
                  logger.log('requestRcutMember Error' , '' , d);

                  let deferred = $q.defer();
                  deferred.reject(d);
                  return deferred.promise;
              });
      },
      accept : function(){

        //모달 오픈
      var modalInstance = $modal.open({
        templateUrl: 'program/ppLaborContractP/ppLaborContractP.tpl.html',
        controller: 'ppLaborContractPCtrl',
        controllerAs: 'ppLaborContractP',
        scope: $scope,
        resolve: {
            item: function() {
                return {};
            }
        }
    });

    modalInstance.opened
        .then(function() {
            // 모달 오픈 성공
        })
        .catch(function(data) {
            alert(data);
            // 모달 오픈 실패
        });

    modalInstance.result
        .then(function(result) {
            // 정상 종료
            logger.log('ppLaborContractP Close', result);
            
            let params = {};
          params.memberSeq = vm.mod.meta.memberSeq + '';
          params.rcutBaseSeq = vm.mod.resume.RCUT_BASE_SEQ + '';
          params.requestInfoSeq = vm.mod.resume.REQUST_INFO_SEQ + '';

          GuestSvc.updateRequestCompanyInfo(params)
              .then(function(d){
                  if(d.user.result ==='SUCCESS'){
                      $message.alert('수락이 완료되었습니다.');
                      $rootScope.prevView();
                  }
                  else{
                      logger.log('updateRequestCompanyInfo Error' , '' , d);
                  }
              })
              .catch(function(d){
                  logger.log('updateRequestCompanyInfo Error' , '' , d);
              });

        })
        .catch(function(reason) {
            // 취소
            logger.log('ppLaborContractP Dismiss', reason);
        });
      }
  },
  standby : {
    get : function(){
      return GuestSvc.selectRcutDetailInfo({
        rcutBaseSeq : vm.mod.standby.RCUT_BASE_SEQ
      }).then(function(d){
        let deferred = $q.defer();

        if(d.user.result === 'SUCCESS'){
          vm.mod.standby.data.imgAttachInfo = d.user.resultData.imgAttachInfo;
          vm.mod.standby.data.requestInfo = d.user.resultData.requestInfo;
          
          // vm.mod.prog.data.subPeopleInfo = d.user.resultData.subPeopleInfo;
          // vm.mod.prog.data.chargeInfo = d.user.resultData.chargeInfo;
          // vm.mod.prog.data.detailBaseInfo = d.user.resultData.detailBaseInfo;
          // vm.mod.prog.data.modifyInfo = d.user.resultData.modifyInfo;

          vm.mod.data.subPeopleInfo = d.user.resultData.subPeopleInfo;
          vm.mod.data.chargeInfo = d.user.resultData.chargeInfo;
          vm.mod.data.detailBaseInfo = d.user.resultData.detailBaseInfo;
          vm.mod.data.modifyInfo = d.user.resultData.modifyInfo;
          
          // 요청정보없을 경우 [출연요청]
           if(!vm.mod.standby.data.requestInfo || vm.mod.standby.data.requestInfo.length <=0){
             vm.mod.standby.states.submit = true;
           }
           else{
            vm.mod.standby.states.submit = true;
            // 요청 버튼 비활성화
            vm.mod.standby.states.cancel = vm.mod.standby.data.requestInfo[0].REQUEST_KIND === 'R2' && 
                                        vm.mod.standby.data.requestInfo[0].REQUEST_STATE === 'R4';
            // 요청정보가 있을경우 일련번호 업데이트
            vm.mod.standby.REQUST_INFO_SEQ = vm.mod.standby.data.requestInfo[0].REQUST_INFO_SEQ;
            
          }
          deferred.resolve(d.user);
        }
        else{
          deferred.reject(d.user);
        }

        return deferred.promise;
      }).catch(function(d){
        let deferred = $q.defer();
        deferred.reject(d.user);
        return deferred.promise;
      });
    },
    submit : function(){

      if(!$message.confirm('촬영순번대기 신청을 하시겠습니까?')){
        return false;
      }

      let params = {};
      params.memberSeq = vm.mod.meta.memberSeq + '';
      params.rcutBaseSeq = vm.mod.standby.RCUT_BASE_SEQ + '';
      params.requestState = 'R4';

      return GuestSvc.requestRcutMember(params)
      .then(function(d){
        let deferred = $q.defer();
        if(d.user.result ==='SUCCESS'){

          $message.alert('스케쥴 촬영 대기 되셨습니다.\n*대기 중 아무런 알림이 없을 시\n취소입니다.')

          _fn.standby.get()
          .then(function(){
            deferred.resolve(d);
          });
        }
        else{
          logger.log('requestRcutMember Error' , '' , d);
          deferred.reject(d);
        }
      })
      .catch(function(d){
        logger.log('requestRcutMember Error' , '' , d);

        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.promise;
      });
    },
    cancel : function(){
      let params = {};
      params.memberSeq = vm.mod.meta.memberSeq + '';
      params.rcutBaseSeq = vm.mod.standby.RCUT_BASE_SEQ + '';
      params.requestInfoSeq = vm.mod.standby.REQUST_INFO_SEQ + '';
      
      return GuestSvc.cancelRequestRcutMember(params)
      .then(function(d){
        let deferred = $q.defer();
        if(d.user.result ==='SUCCESS'){
          _fn.standby.get()
          .then(function(){
            deferred.resolve(d);
          });
        }
        else{
          logger.log('requestRcutMember Error' , '' , d);
          deferred.reject(d);
        }
      })
      .catch(function(d){
        logger.log('requestRcutMember Error' , '' , d);

        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.promise;
      });
    }
  },
  init : function(){
    let deferred = $q.defer();
    
    if($stateParams.param){
      if($stateParams.param.CATE === vm.IDENTIFIERS.VIEW_CATEGORY.PROGRAM){
        // PROGRAM 보기 일 경우
        vm.mod.cate = $stateParams.param.CATE;
        vm.IDENTIFIERS.VIEW_TYPES = $stateParams.param.VIEW_TYPES_IDENTIFIERS;
        vm.mod.prog.viewType = $stateParams.param.VIEW_TYPES;
        vm.mod.prog.RCUT_BASE_SEQ = $stateParams.param.RCUT_BASE_SEQ;
        vm.mod.prog.REQUST_INFO_SEQ = $stateParams.param.REQUST_INFO_SEQ;

        vm.viewReqBtn = $stateParams.param.REQ_BTN_VIEW;
        // vm.viewReqBtn = true;

        deferred.resolve(vm.mod.prog.RCUT_BASE_SEQ);
        logger.log('ppDetail initilize complete  : ' ,'' , vm.mod);
      }
      else if($stateParams.param.CATE === vm.IDENTIFIERS.VIEW_CATEGORY.SCHEDULE){
        // 스케줄 보기 일 경우 

        vm.mod.cate = $stateParams.param.CATE;
        vm.IDENTIFIERS.VIEW_TYPES = $stateParams.param.VIEW_TYPES_IDENTIFIERS;
        vm.mod.sche.viewType = $stateParams.param.VIEW_TYPES;
        vm.mod.sche.RCUT_BASE_SEQ = $stateParams.param.RCUT_BASE_SEQ;
        vm.mod.sche.REQUST_INFO_SEQ = $stateParams.param.REQUST_INFO_SEQ;

        if(vm.mod.sche.viewType === vm.IDENTIFIERS.VIEW_TYPES.WAITING){
          vm.mod.sche.states.cancel = true;
          vm.mod.sche.states.attendance = false;
        }
        else if(vm.mod.sche.viewType === vm.IDENTIFIERS.VIEW_TYPES.BEING){
          vm.mod.sche.states.cancel = false;
          vm.mod.sche.states.attendance = true;
        }

        deferred.resolve($stateParams.param);
        logger.log('ppDetail initilize complete  : ' ,'' , vm.mod);
      }
      else if($stateParams.param.CATE === vm.IDENTIFIERS.VIEW_CATEGORY.RESUME){
          // RESUME 보기 일 경우
          vm.mod.cate = $stateParams.param.CATE;
          vm.IDENTIFIERS.VIEW_TYPES = $stateParams.param.VIEW_TYPES_IDENTIFIERS;
          vm.mod.resume.viewType = $stateParams.param.VIEW_TYPES;
          vm.mod.resume.RCUT_BASE_SEQ = $stateParams.param.RCUT_BASE_SEQ;
          vm.mod.resume.REQUST_INFO_SEQ = $stateParams.param.REQUST_INFO_SEQ;

          if(vm.mod.resume.viewType === vm.IDENTIFIERS.VIEW_TYPES.COM_REQUEST){
              vm.mod.resume.states.accept = true;
              vm.mod.resume.states.cancel = false;
          }
          else if(vm.mod.resume.viewType === vm.IDENTIFIERS.VIEW_TYPES.MY_REQUEST){
              vm.mod.resume.states.accept = false;
              vm.mod.resume.states.cancel = true;
          }

          deferred.resolve(vm.mod.resume.RCUT_BASE_SEQ);
          logger.log('ppDetail initilize complete  : ' ,'' , vm.mod);
      }
      if($stateParams.param.CATE === vm.IDENTIFIERS.VIEW_CATEGORY.STANDBY){
        // STANDBY 보기 일 경우
        vm.mod.cate = $stateParams.param.CATE;
        vm.IDENTIFIERS.VIEW_TYPES = $stateParams.param.VIEW_TYPES_IDENTIFIERS;
        vm.mod.standby.viewType = $stateParams.param.VIEW_TYPES;
        vm.mod.standby.RCUT_BASE_SEQ = $stateParams.param.RCUT_BASE_SEQ;
        vm.mod.standby.REQUST_INFO_SEQ = $stateParams.param.REQUST_INFO_SEQ;

        deferred.resolve(vm.mod.standby.RCUT_BASE_SEQ);
        logger.log('ppDetail initilize complete  : ' ,'' , vm.mod);
      }
      else{
        deferred.reject($stateParams);
        logger.log('ppDetail initialize error!' , ' "CATE" is not available' , $stateParams);
      }
    }
    else {
      deferred.reject($stateParams);
      logger.log('ppDetail initialize error!' ,'' , $stateParams);
    }

    return deferred.promise;
  }
};

_.assign(vm , {
  IDENTIFIERS : {
    VIEW_CATEGORY : {
      PROGRAM : 'PROGRAM',
      SCHEDULE : 'SCHEDULE',
      RESUME : 'RESUME',
      STANDBY : 'STANDBY'
    },
    ME_INFO : {
      ATTENDANCE : 'ATTENDANCE',
      FINISH : 'FINISH',
      GET_UP : 'GET_UP',
      DEPARTURE : 'DEPARTURE'
    }
  },
  mod : {
    meta : {
      memberSeq : (function(){
        let user = User.getUserInfoList();

        if(user.length > 0){
          return user[0].MEMBER_SEQ + '';
        }
        else{ return '-1'; }
      })()
    },
    cate : '',
    prog : {
      viewType : '',
      RCUT_BASE_SEQ : -1,
      REQUST_INFO_SEQ : -1,
      states : {
        submit : false,
        accept : false,
        cancel : false,
        acceptCancel : false,
        prefix : '',
      },
      data : {
        // DATA
        requestInfo : [],
        imgAttachInfo : {},
        // chargeInfo : [],
        // detailBaseInfo : {},
        // modifyInfo : [],
        // subPeopleInfo : [],
      }
    },
    sche : {
      viewType : '',
      RCUT_BASE_SEQ : -1,
      REQUST_INFO_SEQ : -1,
      states : {
        attendance : false,
        cancel : false
      },
      data : {
        // chargeInfo : [],
        // detailBaseInfo : {},
        // modifyInfo : [],
        managerDetailInfo : []
      }
    },
    resume : {
        viewType : '',
        RCUT_BASE_SEQ : -1,
        REQUST_INFO_SEQ : -1,
        states : {
            accept : false,
            cancel : false,
            prefix : '',
        },
        data : {
            // DATA
            requestInfo : [],
            imgAttachInfo : {},
            // chargeInfo : [],
            // detailBaseInfo : {},
            // modifyInfo : [],
            // subPeopleInfo : [],
        }
    },
    standby : {
      viewType : '',
      RCUT_BASE_SEQ : -1,
      REQUST_INFO_SEQ : -1,
      states : {
        submit : false,
        cancel : false,
        prefix : '',
      },
      data : {
        // DATA
        requestInfo : [],
        imgAttachInfo : {},
        // chargeInfo : [],
        // detailBaseInfo : {},
        // modifyInfo : [],
        // subPeopleInfo : [],
      }
    },
    data : {
      chargeInfo : [],
      detailBaseInfo : {},
      modifyInfo : [],
      subPeopleInfo : []
    }
  },
  viewReqBtn : false
});
/**
 * Initialize
 */

function init() {
  logger.debug('init', vm);

  $scope.$on('$destroy', destroy);
  if(!$stateParams.param || $stateParams.param === null){
    $window.alert('프로그램 정보가 잘못되었습니다');
    // $rootScope.goView('pesn.mainp.pmMain');
    $rootScope.prevView();
  }
  _fn.init();
  _fn.get();
}

init();

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */
_.assign(vm , {
  evt : {
    callNumber : function(param){
      _fn.callNumber(param);
    },
    prog : {
        showRcrutReqPopup : function(){
          _fn.prog.showRcrutReqPopup();
        },
      submit : function(){
        _fn.prog.submit();
      },
      cancel : function(){
        _fn.prog.cancel();
      },
      accept : function(){
        _fn.prog.accept();
      },
      acceptCancel : function(){
        _fn.prog.acceptCancel();
      },
      noti : {
        open : function(){
          _fn.prog.noti.open();
        }
      }
    },
    sche : {
      attend : function(){
        // params.rcutBaseSeq = vm.mod.prog.RCUT_BASE_SEQ + '';
        // params.requestInfoSeq = vm.mod.prog.REQUST_INFO_SEQ + '';
        _fn.sche.attend(vm.mod.sche.REQUST_INFO_SEQ,vm.mod.sche.RCUT_BASE_SEQ );
      },
      finish : function(){
        _fn.sche.finish(vm.mod.sche.REQUST_INFO_SEQ,vm.mod.sche.RCUT_BASE_SEQ );
      },
      depart : function(){
        _fn.sche.depart(vm.mod.sche.REQUST_INFO_SEQ,vm.mod.sche.RCUT_BASE_SEQ);
      },
      wakeup : function(){
        _fn.sche.wakeup(vm.mod.sche.REQUST_INFO_SEQ,vm.mod.sche.RCUT_BASE_SEQ);
      },
      modChk : function(){
        _fn.sche.modChk(vm.mod.sche.RCUT_BASE_SEQ );
      },
      noti : {
        open : function(){
          _fn.sche.noti.open();
        }
      },
      cancel : function(){
        _fn.sche.cancel(vm.mod.sche.REQUST_INFO_SEQ);
      }
    },
    resume : {
        cancel : function(){
            _fn.resume.cancel();
        },
        accept : function(){
            _fn.resume.accept();
        }
    },
    standby : {
      submit : function(){
        _fn.standby.submit();
      },
      cancel : function(){
        _fn.standby.cancel();
      }
    }
  }
});
}
