
angular
  .module('pesn.program')
  .controller('ppRcrutReqPCtrl', ppRcrutReqPCtrl);

function ppRcrutReqPCtrl($log, $rootScope, $scope, $modal, $modalInstance, $message, item, ComSvc, GuestSvc, ngspublic) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        pgmRcutSubList : [],

        memberSeq : item.memberSeq,
        rcutBaseSeq : item.rcutBaseSeq,
        subPeopleInfo : item.subPeopleInfo,

        pgmRcutSubSeq : '999'
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        confirm : confirm,
    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        vm.pgmRcutSubList = [{v:'999',k:'나이_성별 선택'}];
        _.forEach(vm.subPeopleInfo ,function(obj){
            vm.pgmRcutSubList.push({v:obj.RCUT_SUB_INFO_SEQ, k:'['+ obj.AGE_MIN + '세 ~ ' + obj.AGE_MAX +'세]['+(obj.GENDER == 'G1' ? '남자' : '여자')+']'});
        })
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 정상 종료
     */
    function confirm(){

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.pgmRcutSubSeq ,   '나이_성별을 선택 해 주세요.', '999');
        if(!chk) return false;

        //모달 오픈
        var modalInstance = $modal.open({
            templateUrl: 'program/ppLaborContractP/ppLaborContractP.tpl.html',
            controller: 'ppLaborContractPCtrl',
            controllerAs: 'ppLaborContractP',
            scope: $scope,
            resolve: {
                item: function() {
                    return {};
                }
            }
        });

        modalInstance.opened
            .then(function() {
                // 모달 오픈 성공
            })
            .catch(function(data) {
                alert(data);
                // 모달 오픈 실패
            });

        modalInstance.result
            .then(function(result) {
                // 정상 종료
                logger.log('ppLaborContractP Close', result);
                
                if(!$message.confirm('출연 신청을 하시겠습니까?')){
                    return false;
                }

                let params = {};
                params.memberSeq = vm.memberSeq + '';
                params.rcutBaseSeq = vm.rcutBaseSeq + '';
                params.rcutSubInfoSeq = vm.pgmRcutSubSeq+'';
        
                GuestSvc.requestRcutMember(params)
                    .then(function(d){
                        if(d.user.result ==='SUCCESS'){
                            $message.alert('정상적으로 처리 되었습니다.');
                            $modalInstance.close();
                        }
                        else{
                            logger.log('rcrutReq Error', '', d);
        
                        }
                    })
                    .catch(function(d){
                        logger.log('rcrutReq Error' , '' , d);
        
                    });

            })
            .catch(function(reason) {
                // 취소
                logger.log('ppLaborContractP Dismiss', reason);
                $modalInstance.dismiss();
            });
    }


}
