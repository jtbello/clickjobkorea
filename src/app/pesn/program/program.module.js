
angular
.module('pesn.program', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.program', {
        url: '/program',
        abstract: false,
        template: '<div ui-view><h1>pesn.program</h1></div>',
        controller: 'programCtrl as program'
    })

    .state('pesn.program.ppAdDetail', {
        url: '/ppAdDetail',
        templateUrl: 'program/ppAdDetail/ppAdDetail.tpl.html',
        data: { menuTitle : '추천 프로그램 상세 ', menuId : 'ppAdDetail' , depth : 2},
        params: { param: null, backupData: null },
        controller: 'ppAdDetailCtrl as ppAdDetail'
    })
    .state('pesn.program.ppDetail', {
        url: '/ppDetail',
        templateUrl: 'program/ppDetail/ppDetail.tpl.html',
        data: { menuTitle : '프로그램 상세', menuId : 'ppDetail' , depth : 2},
        params: { param: null, backupData: null },
        controller: 'ppDetailCtrl as ppDetail'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
