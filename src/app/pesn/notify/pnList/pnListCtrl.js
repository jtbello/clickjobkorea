
angular
.module('pesn.notify')
.controller('pnListCtrl', pnListCtrl);

function pnListCtrl($log, $scope, User, GuestSvc , ComSvc, $q, $window) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

// private 
var fn = {
  init : function(){
    vm.mod.page.startNo = 0;
    vm.mod.page.endNo = 5;
    vm.mod.msgTypes.selected = vm.mod.msgTypes.list[0].COMMON_INFO_VALUE1;
    // vm.mod.msgOpts.selected = vm.mod.msgOpts.list[0].COMMON_INFO_VALUE1;
  },
  get : function(){
    let params = {};
    params.busnLinkKey = vm.const.user;
    params.openYn = vm.mod.msgOpts.selected;
    params.startNo = vm.mod.page.startNo;
    params.endNo = vm.mod.page.endNo;
    params.messageType = vm.mod.msgTypes.selected == '999' ? '' : vm.mod.msgTypes.selected;

    return GuestSvc.selectPushInfo(params)
    .then(function(d){
      let deffered = $q.defer();
      if(d.user.result === 'SUCCESS'){
        // vm.mod.list = d.user.resultData.pushInfo;
        if(params.startNo === 0){
          vm.mod.list = d.user.resultData.pushInfo;
        }
        else{
          d.user.resultData.pushInfo.map(item => {
            vm.mod.list.push(item);
          });
        }
        
        if(d.user.resultData.pushInfo.length == 0 ){
          vm.mod.list = [];
        }
        
        console.log(d);
        vm.mod.listCount = d.user.resultData.pushInfoCount;
        // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
        if (vm.mod.list.length < d.user.resultData.pushInfoCount) {
          vm.showShowMoreButton = true;
        }else{
          vm.showShowMoreButton = false;
        }
        
        deffered.resolve(d);
      }
      else{
        deffered.reject(d);
      }
      return deffered.promise;
    });
  },
  update : function(seq){
    let params = {};
    params.pushResultSeq = seq + '';
    params.memberSeq = vm.const.user;
    return GuestSvc.updatePushOpen(params);
  },
  more : function(){
    if(vm.mod.page.startNo <= 0){
      vm.mod.page.startNo += 1 * vm.mod.page.endNo;
    }
    else{
      vm.mod.page.startNo = ((vm.mod.page.startNo / vm.mod.page.endNo) + 1) * vm.mod.page.endNo;
    }

    return fn.get()
    .then(function(d){
      if(d.user.resultData.pushInfo.length <= 0){
        $window.alert('더 이상 조회할 항목이 없습니다');
      }
    });
  },
  codes : {
    get : {
      OPEN_YN : function(){
        return ComSvc.selectCommonInfo('OPEN_YN')
        .then(function(d){
          let deffered = $q.defer();
          if(d.user.result === 'SUCCESS'){
            vm.mod.msgOpts.list = d.user.resultData.commonData.reverse();
            console.log(vm);
            deffered.resolve(d);
            return deffered.promise;
          }
          else{
            deffered.reject(d);
            return deffered.promise;
          }
        })
        .catch(function(d){
          let deffered = $q.defer();
          console.log('ERR' , arguments);
          deffered.reject(d);
          return deffered.promise;
        });
      },
      MESSAGE_TYPE : function(){
        return ComSvc.selectCommonInfo('MESSAGE_TYPE')
        .then(function(d){
          let deffered = $q.defer();
          if(d.user.result === 'SUCCESS'){
            vm.mod.msgTypes.list = [{COMMON_INFO_VALUE1 : '999', COMMON_INFO_VALUE2 : '전체'}];
            vm.mod.msgTypes.list = vm.mod.msgTypes.list.concat(d.user.resultData.commonData);
            console.log(vm);
            deffered.resolve(d);
          }
          else{
            deffered.reject(d);
          }
          return deffered.promise;
        })
        .catch(function(d){
          let deffered = $q.defer();
          console.log('ERR' , arguments);
          deffered.reject(d);
          return deffered.promise;
        });
      }
    }
  }
};

_.assign(vm , {
  const : {
    user : (function(){
      return User.getUserInfoList()[0].MEMBER_SEQ;
    })()
  },
  mod : {
    list : [],
    page : {
      startNo : 0,
      endNo : 5,
    },
    // SELECT BOX
    msgTypes : {
      selected : '',
      list : []
    },
    // TAB
    msgOpts : {
      list : [],
      selected : ''
    }
  },
  showShowMoreButton : false,
});

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  fn.codes.get.OPEN_YN()
  .then(fn.codes.get.MESSAGE_TYPE)
  .then(function(){
    vm.mod.msgTypes.selected = vm.mod.msgTypes.list[0].COMMON_INFO_VALUE1;
    vm.mod.msgOpts.selected = vm.mod.msgOpts.list[0].COMMON_INFO_VALUE1;

    return fn.get();
  });

  $scope.$on('$destroy', destroy);
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */
_.assign(vm , {
  evt : {
    tab : function(type){
      if(type === vm.mod.msgOpts.selected){
        return;
      }
      vm.mod.msgOpts.selected = type;

      fn.init();
      fn.get();
    },
    sel : function(){
      vm.mod.page.startNo = 0;
      vm.mod.page.endNo = 5;
      fn.get();
    },
    loadMore : function(){
      fn.more();
    },
    accept : function(seq){
      fn.update(seq)
      .then(function(){
        
        vm.mod.page.endNo = vm.mod.page.startNo + vm.mod.page.endNo;
        vm.mod.page.startNo = 0;
        return fn.get();
        });
    }
  }
})
}
