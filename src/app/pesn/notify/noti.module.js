
angular
.module('pesn.notify', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.notify', {
        url: '/notify',
        abstract: false,
        template: '<div ui-view><h1>pesn.notify</h1></div>',
        controller: 'notiCtrl as noti'
    })

    .state('pesn.notify.pnConfig', {
        url: '/pnConfig',
        templateUrl: 'notify/pnConfig/pnConfig.tpl.html',
        data: { menuTitle : '알림 설정', menuId : 'pnConfig' , depth : 2},
        params: { param: null, backupData: null },
        controller: 'pnConfigCtrl as pnConfig'
    })
    .state('pesn.notify.pnList', {
        url: '/pnList',
        templateUrl: 'notify/pnList/pnList.tpl.html',
        data: { menuTitle : '알림 목록', menuId : 'pnList' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'pnListCtrl as pnList'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
