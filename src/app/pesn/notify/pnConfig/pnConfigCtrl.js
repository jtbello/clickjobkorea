
angular
.module('pesn.notify')
.controller('pnConfigCtrl', pnConfigCtrl);

function pnConfigCtrl($window,$log, $scope, $modal, maConstant, $user, MemberSvc, $cordovaPushV5C , $cordovaGeolocation) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

let _auth = {
  gps : {
    get : function(){

    },
    set : function(){

    }
  },
  push : {
    get : function(callback){
      // HASPERMISSION 
      //   $cordovaPushV5C.hasPermission(callback);
    },
    set : function(){
      console.log('clicked set button');
        //     $cordovaPushV5C.register().then(function (resultreg) {
        //         $rootScope.UUID = resultreg;
        //         $log.log('tokenId', resultreg)
        //         // SEND THE TOKEN TO THE SERVER, best associated with your device id and user
        //     }, function (err) {
        //         $log.error(err)
        //         // handle error
        //     });
    }
  }
};



/**
 * Public variables
 */
_.assign(vm, {
  gpsAgree : 'N',
  pushAgree : 'N'
});

/**
 * Public methods
 */
_.assign(vm, {
  setGps : setGps,
  setPush : setPush,
  dev : {
    push : { 
      get : _auth.push.get,
      set : _auth.push.set
    }
  }
});

/**
 * Initialize
 */
init();

function init() {
  
  logger.debug('init', vm);

  $scope.$on('$destroy', destroy);
  
  searchFn();
  
  // $(".gpsAccept .btnStyle02").on("click", function(){
  //   $(".gpsAccept input").click();
  //   $(this).removeClass("active");
  //   $(".gpsAccept .btnStyle01").addClass("active");
  // });

  // $(".gpsAccept .btnStyle01").on("click", function(){
  //   $(".gpsAccept input").click();
  //   $(this).removeClass("active");
  //   $(".gpsAccept .btnStyle02").addClass("active");
  // });

  /* push */
  // $(".pushAccept .btnStyle02").on("click", function(){
  //   $(".pushAccept input").click();
  //   $(this).removeClass("active");
  //   $(".pushAccept .btnStyle01").addClass("active");
  // });

  // $(".pushAccept .btnStyle01").on("click", function(){
  //   $(".pushAccept input").click();
  //   $(this).removeClass("active");
  //   $(".pushAccept .btnStyle02").addClass("active");
  // });
  
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */

/**
 * GPS 정보 동의 세팅
 */
function setGps() {
  $cordovaGeolocation.getCurrentPosition({timeout : 10000}).then(function(info){

    var param = {};
    param.busnLinkKey = $user.currentUser.getUserInfoList()[0].MEMBER_SEQ;      // 회원,기업 일련번호
    param.agreeKind = 'A2';                                                      // 정보 동의 종류[공코:AGREE_KIND]
    param.agreeYn = vm.gpsAgree === 'Y' ? 'N' : 'Y'
    ;                                                     // 동의 여부
    
    MemberSvc.insertAgree(param)
    .then(function(data){
      searchFn();
    })
    .catch(function(err){
      searchFn();
    });
  })
  .catch(function(info){
    if(info.code === 3){
      var param = {};
      param.busnLinkKey = $user.currentUser.getUserInfoList()[0].MEMBER_SEQ;      // 회원,기업 일련번호
      param.agreeKind = 'A2';                                                      // 정보 동의 종류[공코:AGREE_KIND]
      param.agreeYn = vm.gpsAgree === 'Y' ? 'N' : 'Y'
      ;                                                     // 동의 여부
      
      MemberSvc.insertAgree(param)
      .then(function(data){
        searchFn();
      })
      .catch(function(err){
        searchFn();
      });
    }
    else{
      // 실패 
      $window.alert('GPS 설정이 꺼져있습니다. 설정에서 확인해주세요');
      $window.cordova.plugins.settings.open('location');
    }
  });

}

/**
 * PUSH 정보 동의 세팅
 */
function setPush() {
  $cordovaPushV5C.hasPermission()
  .then(function(data){
    if(data.isEnabled){
        var param = {};
        param.busnLinkKey = $user.currentUser.getUserInfoList()[0].MEMBER_SEQ;      // 회원,기업 일련번호
        param.agreeKind = 'A3';                                                      // 정보 동의 종류[공코:AGREE_KIND]
        param.agreeYn = vm.pushAgree === 'Y' ? 'N' : 'Y';                                                     // 동의 여부

        MemberSvc.insertAgree(param)
            .then(function(data){
                searchFn();
            })
            .catch(function(err){
                searchFn();
            });
    }else{
        $window.alert('푸시 설정이 꺼져있습니다. 설정에서 확인해주세요');
        $window.cordova.plugins.settings.open('notification_id');
    }
  })
  .catch(function(){
    $window.alert('푸시 설정이 꺼져있습니다. 설정에서 확인해주세요');
    $window.cordova.plugins.settings.open('notification_id');
  });
}

/**
 * 정보 동의 조회
 * @returns
 */
function searchFn(){
  
  var param = {};
  MemberSvc.selectAgreeInfo(param)
  .then(function(data){
    if(data.user.resultData.agreeInfo.length > 0){
      // GPS 정보
      var findGpsInfo = _.find(data.user.resultData.agreeInfo, {AGREE_KIND : 'A2'});
      if(findGpsInfo){
        if(findGpsInfo.AGREE_YN === 'Y'){
          vm.gpsAgree = 'Y';
        }else{
          vm.gpsAgree = 'N';
        }
      }
      
      // PUSH 정보
      var findPushInfo = _.find(data.user.resultData.agreeInfo, {AGREE_KIND : 'A3'});
      if(findPushInfo){
        if(findPushInfo.AGREE_YN === 'Y'){
          vm.pushAgree = 'Y';
        }else{
          vm.pushAgree = 'N';
        }
      }
    }
  })
  .catch(function(err){
  });

}

}
