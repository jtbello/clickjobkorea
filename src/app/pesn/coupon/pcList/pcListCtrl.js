
angular
.module('pesn.coupon')
.controller('pcListCtrl', pcListCtrl);

function pcListCtrl($log, $scope, User, GuestSvc , ComSvc, $q, $window) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

let fn = {
  _IDENTIFIERES : {
    INIT_TYPE : {
      ALL : 'ALL',
      DATE : 'DATE'
    },
    SEARCH_DATE : {
      START_DATE : 'START_DATE',
      END_DATE : 'END_DATE'
    }
  },
  cal : {
    validate : function(type){
      if(vm.mod.search.startDate !== '' && vm.mod.search.endDate !== ''){
        const startDate = moment(vm.mod.search.startDate);
        const endDate = moment(vm.mod.search.endDate);
        vm.mod.search.startNo = 0;
        vm.mod.search.endNo = 5;
        
        if(startDate.diff(endDate) > 0){
          $window.alert('일자 선택이 잘못되었습니다');
          if(type === fn._IDENTIFIERES.SEARCH_DATE.START_DATE){
            vm.mod.search.startDate = '';
          }
          else if(type === fn._IDENTIFIERES.SEARCH_DATE.END_DATE){
            vm.mod.search.endDate = '';
          }
          return false;
        }
        else{
          return true;
        }
      }
      else{
        return true;
      }
    }
  },
  data : {
    init : function(type){
      if(type === fn._IDENTIFIERES.INIT_TYPE.ALL){
        vm.mod.search.startNo = 0;
        vm.mod.search.endNo = 5;
        vm.mod.search.startDate = '';
        vm.mod.search.endDate = '';
      }
      else if(type == fn._IDENTIFIERES.INIT_TYPE.DATE){
        vm.mod.search.startDate = '';
        vm.mod.search.endDate = '';
      }
    },
    get : function(){
      var params = {};
      params.startNo = vm.mod.search.startNo;
      params.endNo = vm.mod.search.endNo;
      params.startDate = _.isEmpty(vm.mod.search.startDate) ? '' : moment(vm.mod.search.startDate+' 00:00:00', 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');        //[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
      params.endDate = _.isEmpty(vm.mod.search.endDate) ? '' : moment(vm.mod.search.endDate+' 23:59:59', 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');          //[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]

      return GuestSvc.selectMyCouponList(params)
      .then(function(d){
        let deferred = $q.defer();

        if(d.user.result === 'SUCCESS'){
          if(params.startNo > 0){
            d.user.resultData.myCouponInfoList.map(item => {
              vm.mod.list.push(item);
            });
          }
          else{
            vm.mod.list = d.user.resultData.myCouponInfoList;
          }
          
          if(d.user.resultData.myCouponInfoList.length == 0 ){
            vm.mod.list = [];
          }
          
          vm.mod.coupAmounts = d.user.resultData.myCouponCount;
          
          vm.mod.listCount = d.user.resultData.myCouponInfoListCount;

          // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
          if (vm.mod.list.length < d.user.resultData.myCouponInfoListCount) {
            vm.showShowMoreButton = true;
          }else{
            vm.showShowMoreButton = false;
          }
          
          deferred.resolve(d);
        }
        else{
          deferred.reject(d);
        }
        return deferred.promise;
      } , function(d){
        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.reject(d);
      });
    },
    more : function(){
      if(vm.mod.search.startNo <= 0){
        vm.mod.search.startNo += 1 * vm.mod.search.endNo;
      }
      else{
        vm.mod.search.startNo = ((vm.mod.search.startNo / vm.mod.search.endNo) + 1) * vm.mod.search.endNo;
      }
      
      return fn.data.get()
      .then(function(d){
        if(d.user.resultData.myCouponInfoList.length === 0){
          $window.alert('더 이상 조회할 항목이 없습니다');
        }
      });
    }
  }
};

_.assign(vm , {
  // IDENTIFIERS
  __id : fn._IDENTIFIERES,
  // model
  mod : {
    // 내 쿠폰 갯수 
    coupAmounts : '',
    // 조회용 변수 
    search : {
      startNo : 0,
      endNo : 5,
      startDate : '',
      endDate : '',
    },
    // 목록 
    list : []
  }
});

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  fn.data.init();
  fn.data.get();

  $scope.$on('$destroy', destroy);
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */

_.assign(vm , {
  // 액션 이벤트용 함수 
  evt : {
    loadMore : function(){
      fn.data.more();
    },
    search : function(type){
      if(fn.cal.validate(type)){
        fn.data.get();
      }
    }
  }
});

}
