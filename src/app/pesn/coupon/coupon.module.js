
angular
.module('pesn.coupon', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.coupon', {
        url: '/coupon',
        abstract: false,
        template: '<div ui-view><h1>pesn.coupon</h1></div>',
        controller: 'couponCtrl as coupon'
    })

    .state('pesn.coupon.pcList', {
        url: '/pcList',
        templateUrl: 'coupon/pcList/pcList.tpl.html',
        data: { menuTitle : '쿠폰 리스트', menuId : 'pcList' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'pcListCtrl as pcList'
    })
    .state('pesn.coupon.pcShop', {
        url: '/pcShop',
        templateUrl: 'coupon/pcShop/pcShop.tpl.html',
        data: { menuTitle : '쿠폰 구매', menuId : 'pcShop' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'pcShopCtrl as pcShop'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
