
angular
.module('pesn.coupon')
.controller('pcShopCtrl', pcShopCtrl);

function pcShopCtrl($log, $scope, GuestSvc, $filterSet, $inAppPurchase, Analytics, $message) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  $scope.$on('$destroy', destroy);
  
  vm.couponMngMemList = [];
  vm.couponMngComMemList = [];
  vm.selData = 0;

  var param = {};
  param.cpUseUser = 'C1';
  GuestSvc.selectCouponList(param)
  .then(function(data){
    var couponListInfo = data.user.resultData.couponListInfo;
    var couponList = [];
    _.forEach(couponListInfo, function(obj){
      if(obj.CP_USE_USER == 'C1'){
        var temp = {};
        temp.couponSeq = $filterSet.fmtNumber(obj.CP_LIST_SEQ);
        temp.couponCnt = $filterSet.fmtNumber(obj.CP_COUNT);
        temp.couponAmt = $filterSet.fmtNumber(obj.CP_AMOUNT);
        temp.couponDiscountAmt = $filterSet.fmtNumber(obj.CP_SALE_AMOUNT);
        vm.couponMngMemList.push(temp);
          couponList.push('coupon_p'+obj.CP_COUNT);
      }
    })
      $inAppPurchase.getProducts(couponList)
          .then(function(products){
              logger.log(products);
          }).catch(function(error){
          logger.log(error);
      });
  })
  .catch(function(err){
  })

    $inAppPurchase.restorePurchases()
        .then(function(products){
            logger.log(products);
        }).catch(function(error){
            logger.log(error);
        });
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */

  vm.buy = function(){

      var selData = vm.couponMngMemList[vm.selData];

      var procHisSeq = moment(new Date()).format('YYYYMMDDHHmmssSSS');
      if(window.env.platform === 'android'){
          procHisSeq = 'APAY' + procHisSeq;
      }else if(window.env.platform === 'ios'){
          procHisSeq = 'IPAY' + procHisSeq;
      }else{
          $message.alert('결제 할 수 없습니다.');
          return;
      }

      var params = {};
      params.cpListSeq = selData.couponSeq;				//[필수] 판매상품일련번호
      params.cpProcHisSeq = procHisSeq;
      params.paymentHisStatus = '10';
      GuestSvc.insertPaymentProcInfo(params)
          .then(function(data){
              logger.log(data);
          }).catch(function(error){
              logger.log(error);
          });

      $inAppPurchase.buy('coupon_p'+selData.couponCnt)
          .then(function(productInfo){
              logger.log(productInfo);

              var params = {};
              params.cpListSeq = selData.couponSeq;				//[필수] 판매상품일련번호
              params.cpProcHisSeq = procHisSeq;
              params.paymentHisStatus = '20';
              params.hisVal1 = productInfo.receipt;
              GuestSvc.insertPaymentProcInfo(params)
                  .then(function(data){
                      logger.log(data);
                  }).catch(function(error){
                      logger.log(error);
                  });

              $inAppPurchase.consume(productInfo)
                  .then(function(products){
                      logger.log(products);
                  })
                  .catch(function(error){
                      logger.log(error);
                  });

              var param = {};
              param.paymentStatus = 'P1';			//[필수][공코:PAYMENT_STATUS] 결제결과
              param.cpBuyPayKind = 'C2';			//[필수][공코:CP_BUY_PAY_KIND] 결제방식
              param.cpListSeq = selData.couponSeq;				//[필수] 판매상품일련번호
              param.productId = 'coupon_p'+selData.couponCnt;				// 상품아이디
              param.token = productInfo.receipt;				        // 토큰
              param.transactionId = productInfo.transactionId;		// 주문아이디
              param.osType = window.env.platform;		        // OS 타입
              param.cpProcHisSeq = procHisSeq;

              GuestSvc.insertPaymentInfo(param)
                  .then(function(data){
                      logger.log(data);
                      try{
                          Analytics.addProduct('coupon_p'+selData.couponCnt, selData.couponCnt+'개', '개인쿠폰결제완료', '', '',selData.couponDiscountAmt,'1','','쿠폰구매');
                          Analytics.trackTransaction(selData.couponSeq,'개인쿠폰',selData.couponDiscountAmt,'0','0','0','','개인쿠폰','');
                          Analytics.trackCheckout('1','realTimeBank');
                          Analytics.pageView();
                      }catch(e){}

                      $message.alert('결제가 완료되었습니다.');
                  }).catch(function(error){
                      logger.log(error);
                  });

          }).catch(function(error){
              logger.log(error);
              $message.alert('결제가 실패하였습니다.');
              var params = {};
              params.cpListSeq = selData.couponSeq;				//[필수] 판매상품일련번호
              params.cpProcHisSeq = procHisSeq;
              params.paymentHisStatus = '30';
              GuestSvc.insertPaymentProcInfo(params)
                  .then(function(data){
                      logger.log(data);
                  }).catch(function(error){
                  logger.log(error);
              });
          });
  }
}