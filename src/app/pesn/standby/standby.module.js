
angular
.module('pesn.standby', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('pesn.standby', {
        url: '/standby',
        abstract: false,
        template: '<div ui-view><h1>pesn.standby</h1></div>',
        controller: 'standbyCtrl as standby'
    })
    
    .state('pesn.standby.psStandbyList', {
        url: '/psStandbyList',
        templateUrl: 'standby/psStandbyList/psStandbyList.tpl.html',
        data: { menuTitle : '대기가능 스케쥴', menuId : 'psStandbyList' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'psStandbyListCtrl as psStandbyList'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
