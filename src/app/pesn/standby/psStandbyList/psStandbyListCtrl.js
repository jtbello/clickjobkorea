
angular
  .module('pesn.standby')
  .controller('psStandbyListCtrl', psStandbyListCtrl)
  .filter('imgPath' , function(){
    return function(input){
      //return $env.endPoint.service + input;
      return input;
    };
  })

function psStandbyListCtrl($env,$log,$modal, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, 
                    $window, $q, $storage, $advHttp, GuestSvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  var _fn = {
    err : function(d){
      $window.alert('error!!');
      logger.log('ERR!' , '' , d);
      return this;
    },
    standby : {
      // 내게 맞춤
      get : function(){
        let params = {};
        params.orderType = '1';
        params.startNo = vm.mod.standby.startNo;
        params.endNo = vm.mod.standby.endNo;

        return GuestSvc.selectWatingAllRcutList(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            logger.log('selectAllRcutList [SUCCESS]' , '' , d.user);
            if(params.startNo <= 0){
              vm.mod.standby.list = d.user.resultData.allRcutData;
              vm.mod.standby.listMaxCount = d.user.resultData.allRcutDataCount;
            }
            else{
              d.user.resultData.allRcutData.map(item =>{
                vm.mod.standby.list.push(item);
              });
            }
            deferred.resolve(d.user);
          }
          else{
            logger.log('selectAllRcutList [FAILED]' , '' , d.user);
            deferred.reject(d.user);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();

          deferred.reject(d.user);
          logger.log('selectAllRcutList [FAILED]' , '' , d.user);
          return deferred.promise;
        });
      },
      more : function(){
        vm.mod.standby.startNo = ((vm.mod.standby.startNo / vm.mod.standby.endNo) + 1) * vm.mod.standby.endNo;
        return _fn.standby.get()
        .then(function(user){
          if(user.resultData.allRcutData.length <= 0){
            $window.alert('더 이상 조회할 항목이 없습니다.');
          }
        } , _fn.err);
      }
    }
  };
  
  /**
   * Public variables
   */
  _.assign(vm, {
    const : {
      endPoint : $env.endPoint.service
    },
    IDENTIFIERS : {
      ITM_TYPES : {
        URGENT : 'URGENT',
        ADVERTISE : 'ADVERTISE',
        CUSTOM_ORIENTED : 'CUSTOM_ORIENTED'
      }
    },  
    mod : {
      // 추천
      standby : {
        list : [],
        startNo : 0,
        endNo : 5,
        listMaxCount : 0,
        display : true,
      }
    }
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    evt : {
      standby : { 
        loadMore : function(){
          _fn.standby.more();
        },
        go : function(rbs,ris){
          // rbs : RCUT_BASE_SEQ
          $rootScope.goView('pesn.program.ppDetail' , vm , {
            RCUT_BASE_SEQ : rbs,
            REQUST_INFO_SEQ : ris,
            VIEW_TYPES_IDENTIFIERS : vm.IDENTIFIERS.ITM_TYPES,
            VIEW_TYPES : vm.IDENTIFIERS.ITM_TYPES.ADVERTISE,
            CATE : 'STANDBY'
          });
        },
        setDisplay : function(){
          vm.mod.standby.display = !vm.mod.standby.display;
        }
      },
      goMyPage : function(){
        $rootScope.goView('pesn.pesnMyPage');
      }
    }
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    vm.mod.standby.startNo = 0;
    vm.mod.standby.endNo = 5;

    _fn.standby.get()
    .then(function(){ 
      logger.log('init' , '' , vm.mod);
    });

    $scope.$on('$destroy', destroy);
  }
  
  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  
}
