
angular
.module('pesn.inquiry')
.controller('piDetailCtrl', piDetailCtrl);

function piDetailCtrl($log, $scope, $rootScope, $stateParams) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  $scope.$on('$destroy', destroy);
  
  if(_.isBlank($stateParams.param)){
    $rootScope.goView('pesn.inquiry.piList');
    return false;
  }
  
  if(_.isBlank($stateParams.param.data)){
    $rootScope.goView('pesn.inquiry.piList');
    return false;
  }
  
  
  vm.viewData = $stateParams.param.data;
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */

  vm.goList = function(){
    $rootScope.goView('pesn.inquiry.piList');
  }

}
