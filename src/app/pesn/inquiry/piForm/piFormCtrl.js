

angular
.module('pesn.inquiry')
.controller('piFormCtrl', piFormCtrl);

function piFormCtrl($log, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork, GuestSvc , guestConstant, User , ComSvc , CompanySvc) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

$window.__user = User;

let fn = {
  const : {
    memberSeq : (function(){
      return User.getUserInfoList()[0].MEMBER_SEQ;
    })(),
    memberCompanyInfo : (function(){
      return User.getUserInfoList()[0].MEMBER_COMPANY_INFO;
    })(),
    memberHP : (function(){
      return User.getUserInfoList()[0].MEMBER_HP;
    })(),
    memberMail : (function(){
      return User.getUserInfoList()[0].EMAIL;
    })(),
    memberName : (function(){
      return User.getUserInfoList()[0].MEMBER_NM;
    })(),
    
  },  
  data : {
    send : function(){
      $window._vm = vm.mod;
      let params = {};
      params.companySeq = fn.const.memberCompanyInfo;
      params.qnaKind = vm.mod.qnaKind.selected;
      params.pgmBaseSeq = vm.mod.programs.selected.PGM_BASE_SEQ;
      params.title = vm.mod.title;
      params.detailCntn = vm.mod.content;
      params.memberSeq = fn.const.memberSeq;
      params.writeName = fn.const.memberName;
      params.writePhone = vm.mod.hp.first.val + vm.mod.hp.second + vm.mod.hp.third;
     
      if(vm.mod.mail.domain.val === -1){
        params.writeEmail = vm.mod.mail.account + '@' + vm.mod.mail.inserted;
      }
      else{
        params.writeEmail = vm.mod.mail.account + '@' + vm.mod.mail.domain.val;
      }
      
      return GuestSvc.insertQna(params)
      .then(function(d){
        let deferred = $q.defer();
        if(d.user.result === 'SUCCESS'){
          $window.alert(d.user.resultMessage);
          deferred.resolve(d);
        }
        else{
          deferred.reject(d);
        }

        return deferred.promise;
      })
      .catch(function(d){
        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.promise;
      });
    }
  },
  form : {
    init : function(){
      vm.mod.hp.first = vm.mod.hp.list[0];
      vm.mod.mail.domain = vm.mod.mail.list[0];
      vm.mod.title = '';
      vm.mod.content = '';
      vm.mod.mail.inserted = '';
      vm.mod.mail.account = '';

      fn.codes.get.programs();
    },
    validate : {
      all : function(){
        if(!fn.form.validate.hp()){ $window.alert('휴대폰 번호를 확인해주세요'); }
        else if(!fn.form.validate.email()){ $window.alert('이메일을 확인해주세요'); }
        else if(!fn.form.validate.title()){ $window.alert('제목을 확인해주세요'); }
        else if(!fn.form.validate.content()){ $window.alert('내용을 확인해주세요'); }
        else if(!fn.form.validate.categories()){ $window.alert('분류를 확인해주세요'); }
        else { return true; }
      },
      hp : function(){
        if(vm.mod.hp.first.val === -1){ return false; }
        let hp = vm.mod.hp.first.val + vm.mod.hp.second + vm.mod.hp.third;
        // let regex = /^[\d]{3}[\d]{3,4}[\d]{4}$/;
        let regex = /^01([0|1|6|7|8|9]?)?([0-9]{3,4})?([0-9]{4})$/;

        return regex.test(hp);
      },
      email : function(){
        var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(vm.mod.mail.domain.val === -1){
          return false;
        }
        else if(vm.mod.mail.domain.val === 0){
          let mail = vm.mod.mail.account + '@' + vm.mod.mail.inserted.replace('@' , '');
          return regex.test(mail);
        }
        else{
          let mail = vm.mod.mail.account + '@' + vm.mod.mail.domain.text;
          return regex.test(mail);
        }
      },
      title : function(){
        return !(vm.mod.title.trim() === '' || vm.mod.title.trim().length <= 0);
      },
      content : function(){
        return !(vm.mod.content.trim() === '' || vm.mod.content.trim().length <= 0);
      },
      categories : function(){
        if(vm.mod.qnaKind.selected == 'Q1'){
          return !(vm.mod.programs.selected.PGM_BASE_SEQ === '999');
          
        }else{
          return true;
        }
        
      }
    }
  },
  codes : {
    get : {
      programs : function(rpf){
        return GuestSvc.selectPgmList()
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            // vm.mod.programs.list = d.user.resultData.pgmList;  
            vm.mod.programs.list.push({
              PGM_NAME : '선택',
              PGM_REUT_FIELD : '999',
              PGM_BASE_SEQ : '999'
            });
            d.user.resultData.pgmList.map(item =>{
              vm.mod.programs.list.push(item);
            });

            vm.mod.programs.selected = vm.mod.programs.list[0];
            deferred.resolve(d);
          }
          else{
            deferred.reject(d);
          }

          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      }
    }
  }
};

_.assign(vm , {
  mod : {
    hp : {
      list : [
        { val : -1 , text : '선택' },
        { val : '010' , text : '010' },
        { val : '011' , text : '011' },
        { val : '016' , text : '016' },
        { val : '017' , text : '017' },
        { val : '018' , text : '018' },
        { val : '019' , text : '019' },
        { val : '070' , text : '070' }
      ],
      first : -1,
      second : '',
      third : ''
    },
    mail : {
      list : [
        { val : -1 , text : '선택' },
        { val : 'naver.com' , text : 'naver.com' },
        { val : 'hanmail.net' , text : 'hanmail.net' },
        { val : 'gmail.com' , text : 'gmail.com' },
        { val : 'hotmail.com' , text : 'hotmail.com' },
        { val : 'nate.com' , text : 'nate.com' },
        { val : 0 , text : '직접입력' },
      ],
      domain : '',
      account : '',
      inserted : ''
    },
    qnaKind : {
      selected : '',
    },
    programs : {
      list : [],
      selected : ''
    },
    title : '',
    content : ''
  }
});
/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  fn.form.init();
  
  // ph 나눔
  if(fn.const.memberHP !== '' && 
     (fn.const.memberHP.length >= 10 && 
      fn.const.memberHP.length <= 11))
  {
    let fhp = '';
    if(fn.const.memberHP.length === 10){
      fhp = fn.const.memberHP.slice(0,3);
      vm.mod.hp.second = fn.const.memberHP.slice(3,6);
      vm.mod.hp.third = fn.const.memberHP.slice(6,10);
    }
    else if(fn.const.memberHP.length === 11){
      fhp = fn.const.memberHP.slice(0,3);
      vm.mod.hp.second = fn.const.memberHP.slice(3,7);
      vm.mod.hp.third = fn.const.memberHP.slice(7,11)
    }
    vm.mod.hp.list.map(item =>{
      if(item.val === fhp){
        vm.mod.hp.first = item;
      }
    });
  }
  // email나눔
  if(fn.const.memberMail !== '' && 
     fn.const.memberMail.indexOf('@') >= 0)
  {
    let splited = fn.const.memberMail.trim().split('@');
    vm.mod.mail.account = splited[0];
    let isInList = false;
    let selected = {};
    vm.mod.mail.list.map(item =>{
      if(item.val === splited[1]){ 
        isInList = true; 
        selected = item;
      }
    });
    if(isInList){ 
      vm.mod.mail.domain = selected;
      vm.mod.mail.inserted = '';
    }
    else{
      vm.mod.mail.domain = vm.mod.mail.list[vm.mod.mail.list.length -1];
      vm.mod.mail.inserted = splited[1];
    }
  }
  
  $scope.$on('$destroy', destroy);
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

  /**
   * Custom functions
   */
  _.assign(vm , {
    evt : {
      submit : function(){
        if(fn.form.validate.all()){
          fn.data.send()
          .then(function(d){
            if(d.user.result === 'SUCCESS'){
              $rootScope.goView('pesn.inquiry.piList');
            }
          });
        }
      },
      goToList : function(){
        $rootScope.goView('pesn.inquiry.piList');
      }
    }
  });
  
  
  $scope.$watch('piForm.mod.qnaKind.selected', function(newVal, oldVal, scope) {

      if(newVal === oldVal) return false;
      
      vm.mod.programs.selected = vm.mod.programs.list[0];

  });

}
