
angular
.module('pesn.inquiry')
.controller('piListCtrl', piListCtrl);

function piListCtrl($log, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, $cordovaGeolocation, $cordovaNetwork, GuestSvc , guestConstant, User) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

let fn = {
  const : {
    memberSeq : (function(){
      return User.getUserInfoList()[0].MEMBER_SEQ;
    })()
  },  
  data : {
    init : function(){
      vm.mod.search.startNo = 0;
      vm.mod.search.endNo = 5;
    },
    get : function(){
      let params = {};
      params.memberSeq = fn.const.memberSeq + '';
      params.startNo = vm.mod.search.startNo;
      params.endNo = vm.mod.search.endNo;
      return GuestSvc.selectMyQna(params)
      .then(function(d){
        let deferred = $q.defer();
        if(d.user.result === 'SUCCESS'){
          console.log(d.user.resultData);
          if(params.startNo > 0){
            d.user.resultData.myQnaInfo.map(item =>{
              vm.mod.list.push(item);
            });
          }
          else{
            vm.mod.list = d.user.resultData.myQnaInfo;
          }
          vm.mod.listCount = d.user.resultData.myQnaInfoCount;
          
          if(d.user.resultData.myQnaInfo.length == 0 ){
            vm.mod.list = [];
          }
          
          
          // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
          if (vm.mod.list.length < d.user.resultData.myQnaInfoCount) {
            vm.showShowMoreButton = true;
          }else{
            vm.showShowMoreButton = false;
          }
          
          
          deferred.resolve(d);
        }
        else{
          deferred.reject(d);
        }
        return deferred.promise;
      } , function(d){
        let deferred = $q.defer();
        deferred.reject(d);
        return deferred.promise;
      });
    },
    more : function(){
      if(vm.mod.search.startNo <= 0){
        vm.mod.search.startNo += 1 * vm.mod.search.endNo;
      }
      else{
        vm.mod.search.startNo = ((vm.mod.search.startNo / vm.mod.search.endNo) + 1) * vm.mod.search.endNo;
      }
      return fn.data.get()
      .then(function(d){
        if(d.user.resultData.myQnaInfo.length === 0){
          $window.alert('더 이상 조회할 항목이 없습니다');
        }
      });
    }
  }
};

// VARIABLE
_.assign(vm , {
  mod : {
    search : {
      startNo : 0,
      endNo : 5
    },
    list : []
  }
});

// FN
_.assign(vm , {
  evt : {
    loadMore : function(){
      fn.data.more();
    },
    goToDetail : function(data){
      if(data){
        $rootScope.goView('pesn.inquiry.piDetail' , vm , { data : data});
      }
    },
    goToMakeInquiry : function(){
      $rootScope.goView('pesn.inquiry.piForm');
    }
  }
});

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  $scope.$on('$destroy', destroy);

  fn.data.init();
  fn.data.get();
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */

}
