
angular
.module('pesn.inquiry', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.inquiry', {
        url: '/inquiry',
        abstract: false,
        template: '<div ui-view><h1>pesn.inquiry</h1></div>',
        controller: 'inquiryCtrl as inquiry'
    })

    .state('pesn.inquiry.piDetail', {
        url: '/piDetail',
        templateUrl: 'inquiry/piDetail/piDetail.tpl.html',
        data: { menuTitle : '1:1 문의 상세', menuId : 'piDetail' , depth : 2},
        params: { param: null, backupData: null },
        controller: 'piDetailCtrl as piDetail'
    })
    .state('pesn.inquiry.piForm', {
        url: '/piForm',
        templateUrl: 'inquiry/piForm/piForm.tpl.html',
        data: { menuTitle : '1:1문의 작성', menuId : 'piForm' , depth : 2},
        params: { param: null, backupData: null },
        controller: 'piFormCtrl as piForm'
    })
    .state('pesn.inquiry.piList', {
        url: '/piList',
        templateUrl: 'inquiry/piList/piList.tpl.html',
        data: { menuTitle : '1:1문의 목록', menuId : 'piList' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'piListCtrl as piList'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
