
angular
.module('pesn.notice')
.controller('pnNtDetailCtrl', pnNtDetailCtrl);

function pnNtDetailCtrl($log, $scope, $rootScope, $stateParams) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  $scope.$on('$destroy', destroy);
  
  if(_.isBlank($stateParams.param)){
    $rootScope.goView('pesn.notice.pnNtList');
    return false;
  }
  
  if(_.isBlank($stateParams.param.data)){
    $rootScope.goView('pesn.notice.pnNtList');
    return false;
  }
  
  
  vm.viewData = $stateParams.param.data;
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */

  vm.goList = function(){
    $rootScope.goView('pesn.notice.pnNtList');
  }

}
