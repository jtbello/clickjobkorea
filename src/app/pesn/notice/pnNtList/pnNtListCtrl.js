
angular
.module('pesn.notice')
.controller('pnNtListCtrl', pnNtListCtrl);

function pnNtListCtrl($log, $scope, $stateParams, $rootScope, User, ComSvc, $q, $window) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    stateKind : '999',                    // 답변 상태

    notiList : [],                     // 질문 리스트
    notiListCount : 0,                 // 질문 카운트

    startDate : '',	                    //[선택] 작성일시 시작 [일시 패턴 : 0000-00-00 00:00:00]
    endDate : '',	                        //[선택] 작성일시 종료 [일시 패턴 : 0000-00-00 00:00:00]
    searchData : '',						//[선택] 검색어

    // 페이징 관련
    curPage : 1,						// 현재페이지
    viewPageCnt : 10,					// 페이지당보여질수
    pagingData : [],					// 페이지번호 모듬
    totPageCnt : 0					// 전체페이지수
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    selectNotiList : selectNotiList,
    goDetail : goDetail
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    if ($stateParams.backupData) {
      _.assign(vm, $stateParams.backupData);
    }else{
      selectNotiList(false);
    }


  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  function selectNotiList(more){
    vm.curPage = !more ? 1 : vm.curPage+1;

    var param = {};
    param.startNo = (vm.curPage -1) * vm.viewPageCnt;								//[필수] (현재페이지 -1) * 페이지당보여질수
    param.endNo = vm.viewPageCnt;													//[필수] 페이징 페이지당보여질수
    param.searchNotiKind = '';
    // var param = {};
    // param.searchNotiKind = '';		// [선택]기업 공지에서 필터링 할때 사용하는 변수
    // // C : 어플관리자가 기업에게 공지한 공지, CC: 기업관리자가 본인 기업에게 한 공지(담당자들에게 공지)
    // // P: 기업관리자가 출연자에게 공지한 공지, A: 어플관리자가 출연자, 기업 모두에게 공지한 공지,
    // param.startNo = 0;				// [필수] (현재페이지 -1) * 페이지당보여질수
    // param.endNo = 5;					// [필수] 페이징 페이지당보여질수
    ComSvc.selectNotiList(param)
        .then(function(data){
          logger.log('success', data);
          if(data.user.resultData.notiList.length > 0){

            if(!more){
              vm.notiList = data.user.resultData.notiList;
            }else{
              vm.notiList = vm.notiList.concat(data.user.resultData.notiList);
            }
            vm.notiListCount = data.user.resultData.notiListCount;

            // 전체 페이지 정보입력
            vm.totPageCnt = Math.floor((vm.notiListCount / vm.viewPageCnt) + ((vm.notiListCount % vm.viewPageCnt) === 0 ? 0 : 1));
            vm.pagingData = [];
            for (var i = 1; i <= vm.totPageCnt; i++) {
              vm.pagingData.push({pageNo : i});
            }

          }else{
            if(!more){
              vm.notiList = [];
              vm.notiListCount = 0;
              vm.pagingData = [];
            }
          }
        })
        .catch(function(error){
          logger.log('fail');
          logger.error('message: ', error);
          vm.curPage = more ? vm.curPage-1 : vm.curPage;
        });
  }

  function goDetail(data){
    $rootScope.goView('pesn.notice.pnNtDetail', vm, {data : data});
  }

  /**
   * COMPANY_ SEQ: 100000021
   CREATE_DTM: 1563714210000
   CREATE_NO: "300000030"
   DETAIL_CNTN: "서버테스트1"
   NOTI_END_DTM: 1564498800000
   NOTI_HIT: 1
   NOTI_KIND: "P"
   NOTI_SEQ: 16
   NOTI_START_DTM: 1563634800000
   TITLE: "서버테스트1"
   TOP_NOTI_YN: "Y"
   TRT_DTM: 1563714230000
   TRT_NO: "300000030"
   USE_YN: "Y"
   WRITE_NAME: "류정두"
   */
}
