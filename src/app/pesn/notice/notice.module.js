
angular
.module('pesn.notice', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.notice', {
        url: '/notice',
        abstract: false,
        template: '<div ui-view><h1>pesn.notice</h1></div>',
        controller: 'noticeCtrl as notice'
    })

    .state('pesn.notice.pnNtList', {
        url: '/pnNtList',
        templateUrl: 'notice/pnNtList/pnNtList.tpl.html',
        data: { menuTitle : '공지사항', menuId : 'pnNtList' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'pnNtListCtrl as pnNtList'
    })
    .state('pesn.notice.pnNtDetail', {
        url: '/pnNtDetail',
        templateUrl: 'notice/pnNtDetail/pnNtDetail.tpl.html',
        data: { menuTitle : '공지사항 상세', menuId : 'pnNtDetail' , depth : 2},
        params: { param: null, backupData: null },
        controller: 'pnNtDetailCtrl as pnNtDetail'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
