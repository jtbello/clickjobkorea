
angular
.module('pesn.infomod', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.infomod', {
        url: '/infomod',
        abstract: false,
        template: '<div ui-view><h1>pesn.infomod</h1></div>',
        controller: 'infomodCtrl as infomod'
    })
    .state('pesn.infomod.piPesnMod', {
        url: '/piPesnMod',
        templateUrl: 'infomod/piPesnMod/piPesnMod.tpl.html',
        data: { menuTitle : '정보 수정', menuId : 'piPesnMod' , depth : 2},
        params: { param: null, backupData: null },
        controller: 'piPesnModCtrl as piPesnMod'
    })
    .state('pesn.infomod.piPesnModDetail' , {
        url : '/piPesnModDetail',
        templateUrl : 'infomod/piPesnModDetail/piPesnModDetail.tpl.html',
        data: { menuTitle : '정보 수정 상세', menuId : 'piPesnModDetail', depth : 3},
        params: { param: null, backupData: null },
        controller : 'piPesnModDetailCtrl as piPesnModDetail'
    })
    .state('pesn.infomod.piRegCareer' , {
        url : '/piRegCareer',
        templateUrl : 'infomod/piRegCareer/piRegCareer.tpl.html',
        data: { menuTitle : '경력 등록/수정', menuId : 'piRegCareer', depth : 3},
        params: { param: null, backupData: null },
        controller : 'piRegCareerCtrl as piRegCareer'
    })
    .state('pesn.infomod.piRegPhoto' , {
        url : '/piRegPhoto',
        templateUrl : 'infomod/piRegPhoto/piRegPhoto.tpl.html',
        data: { menuTitle : '사진 등록/수정', menuId : 'piRegPhoto', depth : 3},
        params: { param: null, backupData: null },
        controller : 'piRegPhotoCtrl as piRegPhoto'
    })
    .state('pesn.infomod.piRegVideo' , {
        url : '/piRegVideo',
        templateUrl : 'infomod/piRegVideo/piRegVideo.tpl.html',
        data: { menuTitle : '영상 등록/수정', menuId : 'piRegVideo', depth : 3},
        params: { param: null, backupData: null },
        controller : 'piRegVideoCtrl as piRegVideo'
    });

}
