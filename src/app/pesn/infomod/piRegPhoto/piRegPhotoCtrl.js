angular
  .module('pesn.infomod')
  .controller('piRegPhotoCtrl', piRegPhotoCtrl);

function piRegPhotoCtrl($log, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, $window, $q, $storage, 
                        ngspublic, ComSvc, $env, $cordovaInAppBrowser) {
    /**
     * Private variables
     */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        photoAttachInfo : [],
        photoAttachSeqs : '',				    // 사진첨부파일 정보

        photofileDisabled1 : false,			// 사진 첨부파일 1 파일선택가능
        photopgmImgAttachSeq1 : '',			// 사진 첨부파일 1 일련번호
        photopgmImgFileWebPath1 : '',			// 사진 첨부파일 1 경로
        photofileOrgName1 : '',				// 사진 첨부파일 1 원본파일이름
        photofileData1 : '',				    // 사진 첨부파일 1 데이터
        readfile1 : '',				        // 파일 1

        photofileDisabled2 : false,			// 사진 첨부파일 2 파일선택가능
        photopgmImgAttachSeq2 : '',			// 사진 첨부파일 2 일련번호
        photopgmImgFileWebPath2 : '',			// 사진 첨부파일 2 경로
        photofileOrgName2 : '',				// 사진 첨부파일 2 원본파일이름
        photofileData2 : '',				    // 사진 첨부파일 2 데이터
        readfile2 : '',				        // 파일 2

        photofileDisabled3 : false,			// 사진 첨부파일 3 파일선택가능
        photopgmImgAttachSeq3 : '',			// 사진 첨부파일 3 일련번호
        photopgmImgFileWebPath3 : '',			// 사진 첨부파일 3 경로
        photofileOrgName3 : '',				// 사진 첨부파일 3 원본파일이름
        photofileData3 : '',				    // 사진 첨부파일 3 데이터
        readfile3 : '',				        // 파일 3

        photofileDisabled4 : false,			// 사진 첨부파일 4 파일선택가능
        photopgmImgAttachSeq4 : '',			// 사진 첨부파일 4 일련번호
        photopgmImgFileWebPath4 : '',			// 사진 첨부파일 4 경로
        photofileOrgName4 : '',				// 사진 첨부파일 4 원본파일이름
        photofileData4 : '',				    // 사진 첨부파일 4 데이터
        readfile4 : ''				        // 파일 4
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        fileAdd : fileAdd,						// 파일 추가
        fileAttach : fileAttach,					// 파일 전송
        fileDel : fileDel,					    // 파일 삭제
        fileConfirm : fileConfirm,                // 파일 확인
        goGuide : goGuide
    });

    // 프로그램 분야에 대한 프로그램 조회
    $scope.$watch('piRegPhoto.readfile1', function(newVal, oldVal, scope) {
        if(undefined === newVal || "" === newVal){
            return false;
        } else {
            vm.photofileOrgName1 = newVal.name;
        }
        //displayImage('file1', vm.readfile1);
        fileAttach('file1');
    });

    $scope.$watch('piRegPhoto.readfile2', function(newVal, oldVal, scope) {
        if(undefined === newVal || "" === newVal){
            return false;
        } else {
            vm.photofileOrgName2 = newVal.name;
        }
        //displayImage('file2', vm.readfile2);
        fileAttach('file2');
    });

    $scope.$watch('piRegPhoto.readfile3', function(newVal, oldVal, scope) {
        if(undefined === newVal || "" === newVal){
            return false;
        } else {
            vm.photofileOrgName3 = newVal.name;
        }
        //displayImage('file3', vm.readfile3);
        fileAttach('file3');
    });

    $scope.$watch('piRegPhoto.readfile4', function(newVal, oldVal, scope) {
        if(undefined === newVal || "" === newVal){
            return false;
        } else {
            vm.photofileOrgName4 = newVal.name;
        }
        //displayImage('file4', vm.readfile4);
        fileAttach('file4');
    });

    /**
     * Initialize
     */
    init();

    function init() {
        logger.debug('init', vm);
        $scope.$on('$destroy', destroy);

        if ($stateParams.param && $stateParams.param.photoAttachInfo) {
            vm.photoAttachInfo = $stateParams.param.photoAttachInfo;

            _.forEach(vm.photoAttachInfo, function(obj, idx){
                vm['photofileDisabled'+(idx+1)] = true;			            // 사진 첨부파일 1 파일선택가능
                vm['photopgmImgAttachSeq'+(idx+1)] = obj.ATTACH_SEQ;		// 사진 첨부파일 1 일련번호
                vm['photopgmImgFileWebPath'+(idx+1)] = obj.FILE_WEB_PATH;	// 사진 첨부파일 1 경로
                vm['photofileOrgName'+(idx+1)] = obj.FILE_ORG_NAME;			// 사진 첨부파일 1 원본파일이름
            });

        }
        
        
        ComSvc.selectCommonInfo('PHOTO_INFO_URL')
        .then(function(resultData){
          
          var findObj = _.find(resultData.user.resultData.commonData, {COMMON_INFO_VALUE1 : 'P2'});
          vm.photoGuideUrl = findObj.COMMON_INFO_VALUE2;
          
        })
    }

    /**
     * Event handlers
     */
    function destroy(event) {
        logger.debug(event);
    }

    /**
     * Custom functions
     */

    
    function goGuide(){
      
      if(_.isBlank(vm.photoGuideUrl)) return false;
      
      var options = {
              location: 'yes',
              scrollbars: 'yes',
              resizable: 'yes'
          };
          $cordovaInAppBrowser.open(vm.photoGuideUrl, '_blank', options);
      
    }
    
    
    /**
     * 파일 추가
     */
    function fileAdd(kind){
        angular.element('#'+kind).click();
    }

    /**
     * 파일 삭제
     */
    function fileDel(kind){
        if(kind == 'file1'){
            var chk = ngspublic.nullCheck(vm.photopgmImgAttachSeq1, 			'파일정보가 없습니다.');
            if(!chk){
                $('#file1').val('');
                vm.photopgmImgAttachSeq1 = '';
                vm.photofileDisabled1 = false;
                return false;
            }
        }
        if(kind == 'file2'){
            var chk = ngspublic.nullCheck(vm.photopgmImgAttachSeq2, 			'파일정보가 없습니다.');
            if(!chk){
                $('#file2').val('');
                vm.photopgmImgAttachSeq2 = '';
                vm.photofileDisabled2 = false;
                return false;
            }
        }
        if(kind == 'file3'){
            var chk = ngspublic.nullCheck(vm.photopgmImgAttachSeq3, 			'파일정보가 없습니다.');
            if(!chk){
                $('#file3').val('');
                vm.photopgmImgAttachSeq3 = '';
                vm.photofileDisabled3 = false;
                return false;
            }
        }
        if(kind == 'file4'){
            var chk = ngspublic.nullCheck(vm.photopgmImgAttachSeq4, 			'파일정보가 없습니다.');
            if(!chk){
                $('#file4').val('');
                vm.photopgmImgAttachSeq4 = '';
                vm.photofileDisabled4 = false;
                return false;
            }
        }

        var confirmResult = $message.confirm('정말 삭제하시겠습니까?');
        if(!confirmResult) return false;

        var param = {};
        if(kind == 'file1'){
            $("#file1").val("");
            vm.photofileDisabled1 = false;
            param.attachSeq = vm.photopgmImgAttachSeq1;			// [필수]파일등록 후 결과값에 파일 일련번호
            param.fileOrgName = vm.photofileOrgName1;				// [필수]파일등록 후 결과값에 파일 원본파일이름
        }
        if(kind == 'file2'){
            $("#file2").val("");
            vm.photofileDisabled2 = false;
            param.attachSeq = vm.photopgmImgAttachSeq2;			// [필수]파일등록 후 결과값에 파일 일련번호
            param.fileOrgName = vm.photofileOrgName2;				// [필수]파일등록 후 결과값에 파일 원본파일이름
        }
        if(kind == 'file3'){
            $("#file3").val("");
            vm.photofileDisabled3 = false;
            param.attachSeq = vm.photopgmImgAttachSeq3;			// [필수]파일등록 후 결과값에 파일 일련번호
            param.fileOrgName = vm.photofileOrgName3;				// [필수]파일등록 후 결과값에 파일 원본파일이름
        }
        if(kind == 'file4'){
            $("#file4").val("");
            vm.photofileDisabled4 = false;
            param.attachSeq = vm.photopgmImgAttachSeq4;			// [필수]파일등록 후 결과값에 파일 일련번호
            param.fileOrgName = vm.photofileOrgName4;				// [필수]파일등록 후 결과값에 파일 원본파일이름
        }

        ComSvc.fileDelete(param)
            .then(function(data){
                $message.alert('정상적으로 삭제되었습니다.');
                $message.alert('아래 확인 버튼을 꼭 클릭 해주세요. 확인 버튼 클릭없이 진행하시면 적용이안 됩니다.');
                if(kind == 'file1'){
                    vm.photopgmImgAttachSeq1 = '';
                    vm.photopgmImgFileWebPath1 = '';
                    vm.photofileOrgName1 = '';
                    vm.photofileData1 = '';
                    vm.readfile1 = '';
                }
                if(kind == 'file2'){
                    vm.photopgmImgAttachSeq2 = '';
                    vm.photopgmImgFileWebPath2 = '';
                    vm.photofileOrgName2 = '';
                    vm.photofileData2 = '';
                    vm.readfile2 = '';
                }
                if(kind == 'file3'){
                    vm.photopgmImgAttachSeq3 = '';
                    vm.photopgmImgFileWebPath3 = '';
                    vm.photofileOrgName3 = '';
                    vm.photofileData3 = '';
                    vm.readfile3 = '';
                }
                if(kind == 'file4'){
                    vm.photopgmImgAttachSeq4 = '';
                    vm.photopgmImgFileWebPath4 = '';
                    vm.photofileOrgName4 = '';
                    vm.photofileData4 = '';
                    vm.readfile4 = '';
                }
                $("#form"+kind).get(0).reset();
            })
            .catch(function(err){
              if(kind == 'file1'){
                vm.photopgmImgAttachSeq1 = '';
                vm.photopgmImgFileWebPath1 = '';
                vm.photofileOrgName1 = '';
                vm.photofileData1 = '';
                vm.readfile1 = '';
            }
            if(kind == 'file2'){
                vm.photopgmImgAttachSeq2 = '';
                vm.photopgmImgFileWebPath2 = '';
                vm.photofileOrgName2 = '';
                vm.photofileData2 = '';
                vm.readfile2 = '';
            }
            if(kind == 'file3'){
                vm.photopgmImgAttachSeq3 = '';
                vm.photopgmImgFileWebPath3 = '';
                vm.photofileOrgName3 = '';
                vm.photofileData3 = '';
                vm.readfile3 = '';
            }
            if(kind == 'file4'){
                vm.photopgmImgAttachSeq4 = '';
                vm.photopgmImgFileWebPath4 = '';
                vm.photofileOrgName4 = '';
                vm.photofileData4 = '';
                vm.readfile4 = '';
            }
            $("#form"+kind).get(0).reset();
                logger.error(err);
            })
    }

    /**
     * 파일전송
     */
    function fileAttach(kind){
        if(kind == 'file1'){
            vm.photofileDisabled1 = true;
        }
        if(kind == 'file2'){
            vm.photofileDisabled2 = true;
        }
        if(kind == 'file3'){
            vm.photofileDisabled3 = true;
        }
        if(kind == 'file4'){
            vm.photofileDisabled4 = true;
        }

        $rootScope.requestCount++;
        $("#form"+kind).ajaxForm({
            url : $env.endPoint.service + "/api/com/fileUpload.login",
            enctype : "multipart/form-data",
            dataType : "json",
            beforeSubmit: function (data, frm, opt) {
                var objFind = _.find(data, {type : 'file'});
                if(objFind){
                    if(!_.isNull(objFind.value) && objFind.value != ""){
                        return true;
                    }
                }
                if(kind == 'file1'){
                    vm.photofileDisabled1 = false;
                }
                if(kind == 'file2'){
                    vm.photofileDisabled2 = false;
                }
                if(kind == 'file3'){
                    vm.photofileDisabled3 = false;
                }
                if(kind == 'file4'){
                    vm.photofileDisabled4 = false;
                }
                $rootScope.requestCount--;
                $message.alert('파일 선택 후 진행 해주세요.');
                return false;
            },
            error : function(){
                $scope.$apply(function(){
                    if(kind == 'file1'){
                        vm.photofileDisabled1 = false;
                    }
                    if(kind == 'file2'){
                        vm.photofileDisabled2 = false;
                    }
                    if(kind == 'file3'){
                        vm.photofileDisabled3 = false;
                    }
                    if(kind == 'file4'){
                        vm.photofileDisabled4 = false;
                    }
                    $rootScope.requestCount--;
                    $message.alert("파일 전송 실패") ;
                });
            },
            success : function(result){
                $scope.$apply(function(){
                    $rootScope.requestCount--;

                    if(result.user.result == 'Fail'){
                        $message.alert(result.user.resultMessage);
                        return false;
                    }

                    if(kind == 'file1'){
                        vm.photopgmImgAttachSeq1 = result.user.resultData.attachSeq;
                        vm.photofileOrgName1 = result.user.resultData.orgName;
                        vm.photopgmImgFileWebPath1 = result.user.resultData.fileWebPath;
                    }
                    if(kind == 'file2'){
                        vm.photopgmImgAttachSeq2 = result.user.resultData.attachSeq;
                        vm.photofileOrgName2 = result.user.resultData.orgName;
                        vm.photopgmImgFileWebPath2 = result.user.resultData.fileWebPath;
                    }
                    if(kind == 'file3'){
                        vm.photopgmImgAttachSeq3 = result.user.resultData.attachSeq;
                        vm.photofileOrgName3 = result.user.resultData.orgName;
                        vm.photopgmImgFileWebPath3 = result.user.resultData.fileWebPath;
                    }
                    if(kind == 'file4'){
                        vm.photopgmImgAttachSeq4 = result.user.resultData.attachSeq;
                        vm.photofileOrgName4 = result.user.resultData.orgName;
                        vm.photopgmImgFileWebPath4 = result.user.resultData.fileWebPath;
                    }

                    $message.alert("파일 전송 성공");
                    $message.alert('아래 확인 버튼을 꼭 클릭 해주세요. 확인 버튼 클릭없이 진행하시면 적용이안 됩니다.');
                });
            }
        });

        $("#form"+kind).submit();
    }

    /**
     * 파일 확인
     */
    function fileConfirm(){
        if ($message.confirm('사진을 등록하시겠습니까?')) {

            if(vm.photopgmImgAttachSeq1 === '' &&
                vm.photopgmImgAttachSeq2 === '' &&
                vm.photopgmImgAttachSeq3 === '' &&
                vm.photopgmImgAttachSeq4 === ''){
                $message.alert('사진을 추가해주세요.');
                return false;
            }

            vm.photoAttachInfo = [];
            if(vm.photopgmImgAttachSeq1 !== ''){
                vm.photoAttachInfo.push({
                    ATTACH_SEQ : vm.photopgmImgAttachSeq1,
                    FILE_WEB_PATH : vm.photopgmImgFileWebPath1,
                    FILE_ORG_NAME : vm.photofileOrgName1
                });
            }
            if(vm.photopgmImgAttachSeq2 !== ''){
                vm.photoAttachInfo.push({
                    ATTACH_SEQ : vm.photopgmImgAttachSeq2,
                    FILE_WEB_PATH : vm.photopgmImgFileWebPath2,
                    FILE_ORG_NAME : vm.photofileOrgName2
                });
            }
            if(vm.photopgmImgAttachSeq3 !== ''){
                vm.photoAttachInfo.push({
                    ATTACH_SEQ : vm.photopgmImgAttachSeq3,
                    FILE_WEB_PATH : vm.photopgmImgFileWebPath3,
                    FILE_ORG_NAME : vm.photofileOrgName3
                });
            }
            if(vm.photopgmImgAttachSeq4 !== ''){
                vm.photoAttachInfo.push({
                    ATTACH_SEQ : vm.photopgmImgAttachSeq4,
                    FILE_WEB_PATH : vm.photopgmImgFileWebPath4,
                    FILE_ORG_NAME : vm.photofileOrgName4
                });
            }

            var param = { photoAttachInfo: vm.photoAttachInfo };
            $rootScope.prevView(param);
        }
    }

    /**
     * 파일 읽기
     */
    function displayImage(kind, file){
        var reader = new FileReader();
        reader.onload = function(frEvent) {
            if(kind == 'file1'){
                vm.photofileData1 = frEvent.target.result;
            }
            if(kind == 'file2'){
                vm.photofileData2 = frEvent.target.result;
            }
            if(kind == 'file3'){
                vm.photofileData3 = frEvent.target.result;
            }
            if(kind == 'file4'){
                vm.photofileData4 = frEvent.target.result;
            }
        }
        reader.readAsDataURL(file);
    }
}