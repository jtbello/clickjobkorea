angular
  .module('pesn.infomod')
  .controller('piRegVideoCtrl', piRegVideoCtrl);

function piRegVideoCtrl($log, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, $window, $q, $storage,
                         ComSvc, ngspublic, $env, $cordovaInAppBrowser) {
    /**
     * Private variables
     */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        videoAttachInfo : [],
        videoFileWebPath : '',
        videoAttachSeqs: '', //[선택]동영상 첨부파일 일련번호
        videofileDisabled : false,
        videofileOrgName : '',				// 사진 첨부파일 1 원본파일이름
        readfile : ''
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        playVideo : playVideo,
        fileAdd : fileAdd,						// 파일 추가
        fileAttach : fileAttach,
        fileDel : fileDel,					    // 파일 삭제
        displayImage : displayImage,
        fileConfirm : fileConfirm,                // 파일 확인
        goGuide : goGuide,
    });

    // 프로그램 분야에 대한 프로그램 조회
    $scope.$watch('piRegVideo.readfile', function(newVal, oldVal, scope) {
        if(undefined === newVal || "" === newVal){
            return false;
        } else {
            vm.videofileOrgName = newVal.name;
        }

        //displayImage(vm.readfile);
        fileAttach();
    });

    /**
     * Initialize
     */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        if ($stateParams.param && $stateParams.param.videoAttachInfo) {
            vm.videoAttachInfo = $stateParams.param.videoAttachInfo;

            _.forEach(vm.videoAttachInfo, function(obj, idx){
                vm.videofileDisabled = true;			            // 사진 첨부파일 1 파일선택가능
                vm.videoAttachSeqs = obj.ATTACH_SEQ;		// 사진 첨부파일 1 일련번호
                vm.videoFileWebPath = obj.FILE_WEB_PATH;	// 사진 첨부파일 1 경로
                vm.videofileOrgName = obj.FILE_ORG_NAME;			// 사진 첨부파일 1 원본파일이름
            });

            $("#myVideo").attr("src", $env.endPoint.service + vm.videoFileWebPath);
            $("#myVideo").load();
        }

        ComSvc.selectCommonInfo('PHOTO_INFO_URL')
        .then(function(resultData){
          
          var findObj = _.find(resultData.user.resultData.commonData, {COMMON_INFO_VALUE1 : 'P1'});
          vm.photoGuideUrl = findObj.COMMON_INFO_VALUE2;
          
        })
        
        ComSvc.selectCommonInfo('SAMPLE_VIDEO')
        .then(function(resultData){
          
          var findObj = _.find(resultData.user.resultData.commonData, {COMMON_INFO_VALUE1 : 'S1'});
          vm.sampleVdeUrl = $env.endPoint.service + findObj.COMMON_INFO_VALUE2;
          
        })
        
    }

    /**
     * Event handlers
     */
    function destroy(event) {
        logger.debug(event);
    }

    /**
     * Custom functions
     */
    
    function goGuide(kind){
      
      
      
      var options = {
              location: 'yes',
              scrollbars: 'yes',
              resizable: 'yes'
          };
      if(kind == 'K1'){
        if(_.isBlank(vm.photoGuideUrl)) return false;
        $cordovaInAppBrowser.open(vm.photoGuideUrl, '_blank', options);
      }else{
        if(_.isBlank(vm.sampleVdeUrl)) return false;
        $cordovaInAppBrowser.open(vm.sampleVdeUrl, '_blank', options);
      }
          
      
    }

    /**
     * 파일 추가
     */
    function fileAdd(){
        angular.element("#file").click();
    }

    /**
     * 재생
     */
    function playVideo(){
        // var video = $("#myVideo")[0];
        // if (video.paused) {
        //     video.play();
        // }
        // else {
        //     video.pause();
        // }
    }

    /**
     * 파일 삭제
     */
    function fileDel(kind){
        var chk = ngspublic.nullCheck(vm.videoAttachSeqs, 			'파일정보가 없습니다.');
        if(!chk){
            $('#file').val('');
            vm.videoAttachSeqs = '';
            vm.videofileDisabled = false;
            return false;
        }

        var confirmResult = $message.confirm('정말 삭제하시겠습니까?');
        if(!confirmResult) return false;

        var param = {};
        $("#file").val("");
        vm.videofileDisabled = false;
        param.attachSeq = vm.videoAttachSeqs;			// [필수]파일등록 후 결과값에 파일 일련번호
        param.fileOrgName = vm.videofileOrgName;				// [필수]파일등록 후 결과값에 파일 원본파일이름

        ComSvc.fileDelete(param)
            .then(function(data){
                $message.alert('정상적으로 삭제되었습니다.');
                $message.alert('아래 확인 버튼을 꼭 클릭 해주세요. 확인 버튼 클릭없이 진행하시면 적용이안 됩니다.');
                vm.videoAttachSeqs = '';
                vm.videofileOrgName = '';
                vm.readfile = '';
                vm.videoFileWebPath = '';

                $("#form").get(0).reset();

                $("#myVideo").attr("src", "");
            })
            .catch(function(err){
              vm.videoAttachSeqs = '';
              vm.videofileOrgName = '';
              vm.readfile = '';
              vm.videoFileWebPath = '';

              $("#form").get(0).reset();

              $("#myVideo").attr("src", "");
                logger.error('message: ' + err + '\n');
            })
    }

    /**
     * 파일전송
     */
    function fileAttach(){
      
        if(!ngspublic.fileCheck(window.document.getElementById('file'))) return false;
      
        vm.videofileDisabled = true;

        $rootScope.requestCount++;
        $("#form").ajaxForm({
            url : $env.endPoint.service + "/api/com/fileUpload.login",
            enctype : "multipart/form-data",
            dataType : "json",
            beforeSubmit: function (data, frm, opt) {
                var objFind = _.find(data, {type : 'file'});
                if(objFind){
                    if(!_.isNull(objFind.value) && objFind.value != ""){
                        return true;
                    }
                }
                
                vm.videofileDisabled = false;
                $rootScope.requestCount--;
                $message.alert('파일 선택 후 진행 해주세요.');
                return false;
            },
            error : function(){
                $scope.$apply(function(){
                    vm.videofileDisabled = false;

                    $rootScope.requestCount--;
                    $message.alert("파일 전송 실패") ;
                });
            },
            success : function(result){
                $scope.$apply(function(){
                    $rootScope.requestCount--;

                    if(result.user.result == 'Fail'){
                        $message.alert(result.user.resultMessage);
                        return false;
                    }

                    vm.videoAttachSeqs = result.user.resultData.attachSeq;
                    vm.videofileOrgName = result.user.resultData.orgName;
                    vm.videoFileWebPath = result.user.resultData.fileWebPath;

                    $("#myVideo").attr("src", $env.endPoint.service + vm.videoFileWebPath);
                    $("#myVideo").load();

                    $message.alert("파일 전송 성공");
                    $message.alert('아래 확인 버튼을 꼭 클릭 해주세요. 확인 버튼 클릭없이 진행하시면 적용이안 됩니다.');
                });
            }
        });

        $("#form").submit();
    }

    /**
     * 파일 읽기
     */
    function displayImage(file){
        $("#myVideo").attr("src", URL.createObjectURL(file));
        $("#myVideo").load();
    }

    /**
     * 파일 확인
     */
    function fileConfirm(){
        if ($message.confirm('영상을 등록하시겠습니까?')) {

            vm.videoAttachInfo = [];
            if(vm.videoAttachSeqs !== ''){
                vm.videoAttachInfo.push({
                    ATTACH_SEQ : vm.videoAttachSeqs,
                    FILE_WEB_PATH : vm.videoFileWebPath,
                    FILE_ORG_NAME : vm.videofileOrgName
                });
            }

            var param = { videoAttachInfo: vm.videoAttachInfo };
            $rootScope.prevView(param);
        }
    }
}