angular
  .module('pesn.infomod')
  .controller('piRegCareerCtrl', piRegCareerCtrl);

function piRegCareerCtrl($log, $scope, $message, ngspublic, $rootScope, $stateParams) {
    /**
     * Private variables
     */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        // SELECT OPTIONS
        yearOptions: [{ value: '999', text: '연도 선택'}], // 연도 선택 리스트
        linesYnOptions: [
            { value: 'N', text: '대사 없음' },
            { value: 'Y', text: '대사 있음' },
        ], // 대사유무 선택 리스트

        // 현재 입력폼
        field: '999', // [필수] 분야
        pgmName: '', // [필수] 프로그램 이름
        broadcastingStation: '999', // [필수] 방송국
        year: '999', // [필수] 연도
        part: '999', // [필수] 배역
        linesYn: 'N', // [필수] 대사 유무

        // 입력된 경력 리스트
        memberCareerInfo: [
            // {
            //   broadcastingStation: "B3",
            //   field: "F1",
            //   linesYn: "N",
            //   part: "P6",
            //   pgmName: "asdf",
            //   year: "2011"
            // },
            // {
            //   broadcastingStation: "B1",
            //   field: "F3",
            //   linesYn: "Y",
            //   part: "P2",
            //   pgmName: "asdadsdf",
            //   year: "2015"
            // }
        ],
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        resetForm: resetForm, // 입력 폼 초기화
        addMemberCareerInfo: addMemberCareerInfo, // 입력 값 저장
        removeAll: removeAll, // 입력된 경력 리스트 전체 제거
        removeCareerInfo: removeCareerInfo, // 선택된 경력 제거
        onClickSave: onClickSave // 경력 사항 저장
    });

    /**
     * Initialize
     */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        // 연도 선택 리스트 생성
        var thisYear = new Date().getFullYear();
        for (let i = thisYear - 10; i <= thisYear + 10; i++) {
            vm.yearOptions.push({ value: `${i}`, text: `${i}년` });
        }

        // 기존 입력값 복원
        if ($stateParams.param && $stateParams.param.memberCareerInfo) {
            vm.memberCareerInfo = $stateParams.param.memberCareerInfo;
        }

    }

    /**
     * Event handlers
     */
    function destroy(event) {
        logger.debug(event);
    }

    /**
     * Custom functions
     */

    // 입력 폼 초기화
    function resetForm() {
        vm.field = '999';
        vm.pgmName = '';
        vm.broadcastingStation = '999';
        vm.year = '999';
        vm.part = '999';
        vm.linesYn = 'N';
    }

    // 입력 값 저장
    function addMemberCareerInfo() {
        /*======================================
          NULL CHECK
        ========================================*/
        var isValid = true;

        isValid = isValid && ngspublic.nullCheck(vm.field, '분야를 선택해 주세요.', '999');
        isValid = isValid && ngspublic.nullCheck(vm.pgmName, '프로그램명을 입력해 주세요.');
        isValid = isValid && ngspublic.nullCheck(vm.broadcastingStation, '방송사를 선택해 주세요.', '999');
        isValid = isValid && ngspublic.nullCheck(vm.year, '연도를 선택해 주세요.', '999');
        isValid = isValid && ngspublic.nullCheck(vm.part, '배역을 선택해 주세요.', '999');

        if (!isValid) return false;

        /*======================================
          입력 값 리스트에 추가
        ========================================*/
        vm.memberCareerInfo.push({
            field: vm.field,
            pgmName: vm.pgmName,
            broadcastingStation: vm.broadcastingStation,
            year: vm.year,
            part: vm.part,
            linesYn: vm.linesYn
        });

        resetForm();
    }

    // 경력 모두 제거
    function removeAll() {
        if (vm.memberCareerInfo.length === 0) {
            $message.alert('삭제할 내용이 없습니다.');
        } else if ($message.confirm('등록된 모든 내용이 삭제됩니다. 삭제하시겠습니까?')) {
            vm.memberCareerInfo = [];
        }
    }

    // 선택된 경력 제거
    function removeCareerInfo(index) {
        if ($message.confirm('삭제하시겠습니까?')) {
            vm.memberCareerInfo.splice(index, 1);
        }
    }

    // 경력 사항 저장
    function onClickSave() {
        if ($message.confirm('입력하신 경력사항을 저장하시겠습니까?')) {
            var param = { memberCareerInfo: vm.memberCareerInfo };
            $rootScope.prevView(param);
        }
    }
}
