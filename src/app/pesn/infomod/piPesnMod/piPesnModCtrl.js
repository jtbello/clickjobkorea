angular.module('pesn.infomod').controller('piPesnModCtrl', piPesnModCtrl);

function piPesnModCtrl(
  $log,
  $rootScope,
  $scope,
  $state,
  $timeout,
  $user,
  $message,
  $window,
  $q,
  MemberSvc,
  $cordovaInAppBrowser,
  ComSvc,
  comConstant,
  $filterSet,
  $env,
  ngspublic,
  GuestSvc,
  $stateParams
) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    bankNameInfo : [],          // 은행정보
    memberCompayInfoList: [], // 기업 리스트
    genderInfo: [],
    emailDomainOptions: [], // 이메일 도메인 리스트
    subwayTimeOptions: [], // 지하철역 소요시간 리스트

    hpAuthFlag: false,  // 전화번호 인증
    hpAuthDis: false,   // 전화번호 인증 성공 결과

    // 파라미터
    memberId: '', //[필수] 아이디
    memberPw: '', //[필수] 패스워드
    memberPwChk: '', //패스워드 체크
    memberNm: '', //[필수] 이름
    memberGender: '', //[필수][공코 : GENDER] 성별
    memberKind: '', //[필수][공코 : MEMBER_KIND] 회원종류
    memberHpKind: '', //[필수][공코 : HP_KIND] 휴대전화회사종류
    memberHp: '', //[필수]휴대폰번호 ('-'제거)
    memberAddr: '', //[필수]기본 주소
    memberAddrBase: '', //기본 주소
    memberAddrDetail: '', //상세 주소
    memberAddrNo: '', //우편번호
    memberSubwayLines : '999', //[필수]지하철호선
    memberSubwayName: '', //[필수]가까운지하철
    memberSubwayTime: '999', //[필수]소요시간
    height: '', //[필수]키
    weight: '', //[필수]몸무게
    emailId: '', //[필수]이메일id
    emailDomainId: '999', //[필수]이메일도메인id
    emailDomain: '', // 이메일 도메인

    photoAttachSeqs: '', //[필수]사진 첨부파일 일련번호

    memberCompayInfo: '999', //[필수] 기업일련번호
    memberEntercompayInfo: '', //[선택] memberKind 종류가 M2 일때 소속 회사명
    accNo: '', //[선택]계좌번호('-'제거)
    accBankName: '999', //[선택][공코 : BANK_NAME]계좌은행명
    accName: '', //[선택]계좌예금주
    companyNoAttachSeqs: '', //[선택]사업자등록증 및 증빙서류 첨부파일 일련번호
    videoAttachSeqs: '', //[선택]동영상 첨부파일 일련번호

    companyNofileDisabled : false,			// 사진 첨부파일 1 파일선택가능
    companyNofileOrgName : '',				// 사진 첨부파일 1 원본파일이름
    readfile : '',				        // 파일 1

    memberDetailInfo : {},          // 멤버 상세 입력
    supportAndHopeInfo : [],        // 지원 및 희망분야
    photoAttachInfo : [],
    videoAttachInfo : [],

    // 회원 경력
    isMemberCareerInfoChange : false,
    memberCareerInfo: [] // 회원 경력
    // careerInfo.field = 'F1';								//[필수][공코 : FIELD] 경력 분류
    // careerInfo.pgmName = '무한도전';							//[필수]프로그램 명
    // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
    // careerInfo.year = '2018';								//[필수]년도
    // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
    // careerInfo.linesYn = 'Y';								//[필수]대사
    // memberCareerInfo.push(careerInfo);
    //
    // var careerInfo = {};
    // careerInfo.field = 'F2';								//[필수][공코 : FIELD] 경력 분류
    // careerInfo.pgmName = '1박2일';							//[필수]프로그램 명
    // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
    // careerInfo.year = '2018';								//[필수]년도
    // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
    // careerInfo.linesYn = 'N';								//[필수]대사
    // memberCareerInfo.push(careerInfo);
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    addrSearch: addrSearch,
    reHpAuth: reHpAuth,
    hpAuth: hpAuth,
    onAddPhotoButtonClick : onAddPhotoButtonClick,  // '사진 등록' 클릭시
    onAddVideoButtonClick : onAddVideoButtonClick,  // '영상 등록' 클릭시
    onAddCareerButtonClick: onAddCareerButtonClick, // '경력사항 등록' 클릭시
      onUpdateInfoClick: onUpdateInfoClick, // '상세정보 입력'클릭시
      fileAdd : fileAdd,                  // 파일 추가
      fileAttach : fileAttach,            // 파일 전송
      fileDel : fileDel,                  // 파일 삭제
  });
  
  // 프로그램 분야에 대한 모집 분야 코드 조회
  $scope.$watch('piPesnMod.memberKind', function(newVal, oldVal, scope) {
    if (newVal === oldVal) return false;
    if (newVal !== 'M3') {
      vm.memberCompayInfo = '999';
      return false;
    }else{
      vm.memberCompayInfo = vm.memberCompayInfoSave;
    }
  });

    $scope.$watch('piPesnMod.readfile', function(newVal, oldVal, scope) {

        if(undefined === newVal || "" === newVal){
            return false;
        } else {
            vm.companyNofileOrgName = newVal.name;
        }
        fileAttach();
    });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    // set email domain list
    vm.emailDomainOptions.push({ value: '999', text: '선택' });
    vm.emailDomainOptions.push({ value: 'naver.com', text: 'naver.com' });
    vm.emailDomainOptions.push({ value: 'hanmail.net', text: 'hanmail.net' });
    vm.emailDomainOptions.push({ value: 'gmail.com', text: 'gmail.com' });
    vm.emailDomainOptions.push({ value: 'hotmail.com', text: 'hotmail.com' });
    vm.emailDomainOptions.push({ value: 'nate.com', text: 'nate.com' });
    vm.emailDomainOptions.push({ value: '0', text: '직접입력' });

      // 은행명 공통 코드 조회
      ComSvc.selectCommonInfo('BANK_NAME')
          .then(function(data){
              vm.bankNameInfo = data.user.resultData.commonData;
          })

    // 지하철 소요시간 리스트
    vm.subwayTimeOptions = [{ value: '999', text: '소요시간 선택'}];
    for (var i = 1; i <= 12; i++) {
      vm.subwayTimeOptions.push({ value: `${i * 5}`, text: `${i * 5}분`});
    }

    MemberSvc.selectCompanyList()
      .then(function(data) {
        if (data.user.resultData.companyList.length > 0) {
          vm.memberCompayInfoList = [{ v: '999', k: '추천기업선택' }];
          _.forEach(data.user.resultData.companyList, function(obj) {
            vm.memberCompayInfoList.push({
              v: obj.COMPANY_SEQ,
              k: obj.COMPANY_NM
            });
          });
        } else {
          vm.memberCompayInfoList = [{ v: '999', k: '추천기업선택' }];
          vm.memberCompayInfo = '999';
        }
      })
      .catch(function(error) {});

    ComSvc.selectCommonInfo(comConstant.GENDER)
      .then(function(data) {
        vm.memberGenderInfo = [];
        _.forEach(data.user.resultData.commonData, function(obj) {
          vm.memberGenderInfo.push({
            v: obj.COMMON_INFO_VALUE1,
            k: obj.COMMON_INFO_VALUE2
          });
        });
      })
      .catch(function(err) {});


    if ($stateParams.backupData) {
      _.assign(vm, $stateParams.backupData);
    }else{
        setTimeout(function(){
            GuestSvc.selectMyInfo()
                .then(function(data) {

                    var memberInfo = data.user.resultData.myInfo;
                    vm.memberId = memberInfo.MEMBER_ID; //[필수] 아이디
                    vm.memberNm = memberInfo.MEMBER_NM; //[필수] 이름
                    vm.memberGender = memberInfo.MEMBER_GENDER; //[필수][공코 : GENDER] 성별
                    vm.memberKind = memberInfo.MEMBER_KIND; //[필수][공코 : MEMBER_KIND] 회원종류
                    vm.memberHpKind = memberInfo.MEMBER_HP_KIND; //[필수][공코 : HP_KIND] 휴대전화회사종류
                    vm.memberHp = memberInfo.MEMBER_HP; //[필수]휴대폰번호 ('-'제거)
                    
                    vm.hpAuthFlag = true;
                    vm.hpAuthDis = true;

                    vm.memberAddr = memberInfo.MEMBER_ADDR; //[필수]기본 주소

                    vm.memberAddrBase =  memberInfo.MEMBER_ADDR.substr(6); //기본 주소
                    vm.memberAddrNo =  memberInfo.MEMBER_ADDR.substr(0,5); //우편번호

                    vm.memberSubwayLines = memberInfo.MEMBER_SUBWAY_LINES; //[필수]지하철호선
                    vm.memberSubwayName = memberInfo.MEMBER_SUBWAY_NAME; //[필수]가까운지하철
                    vm.memberSubwayTime = _.isEmpty(memberInfo.MEMBER_SUBWAY_TIME) ? '999' : memberInfo.MEMBER_SUBWAY_TIME; //[필수]소요시간
                    vm.height = memberInfo.HEIGHT; //[필수]키
                    vm.weight = memberInfo.WEIGHT; //[필수]몸무게

                    var email = memberInfo.EMAIL.split('@');
                    var findEmail = _.find(vm.emailDomainOptions, {value : email[1]});

                    vm.emailId = email[0]; //[필수]이메일id
                    vm.emailDomainId = _.isEmpty(findEmail) ? '0' : email[1]; //[필수]이메일도메인id
                    vm.emailDomain = _.isEmpty(findEmail) ? email[1] : ''; // 이메일 도메인

                    vm.memberCompayInfo = memberInfo.MEMBER_COMPANY_INFO; //[필수] 기업일련번호
                    vm.memberCompayInfoSave =memberInfo.MEMBER_COMPANY_INFO;
                    vm.memberRrn1 = memberInfo.MEMBER_RRN.substr(0,6);
                    vm.memberRrn2 = memberInfo.MEMBER_RRN.substr(6);
                    // 기업선택 / 회사명 입력
                    if (memberInfo.MEMBER_KIND === 'M2') {
                        vm.memberEntercompayInfo = memberInfo.MEMBER_COMPANY_INFO; //[선택] memberKind 종류가 M2 일때 소속 회사명
                    }
                    vm.accNo = _.isEmpty(memberInfo.ACC_NO) ? '' : memberInfo.ACC_NO; //[선택]계좌번호('-'제거)
                    vm.accBankName = _.isEmpty(memberInfo.ACC_BANK_NAME) ? '999' : _.find(vm.bankNameInfo, {COMMON_INFO_VALUE2 : memberInfo.ACC_BANK_NAME}).COMMON_INFO_VALUE1; //[선택][공코 : BANK_NAME]계좌은행명
                    vm.accName = _.isEmpty(memberInfo.ACC_NAME) ? '' : memberInfo.ACC_NAME; //[선택]계좌예금주

                    vm.photoAttachInfo = data.user.resultData.photoAttachInfo;
                    vm.videoAttachInfo = data.user.resultData.videoAttachInfo;
                    var companyNoAttachInfo = data.user.resultData.companyNoAttachInfo;

                    var photoAttachSeqs = '';
                    _.forEach(vm.photoAttachInfo, function(obj){
                        photoAttachSeqs += obj.ATTACH_SEQ + '|';
                    });
                    vm.photoAttachSeqs = photoAttachSeqs.substr(0,photoAttachSeqs.length -1);   //[필수]사진 첨부파일 일련번호

                    var videoAttachSeqs = '';
                    _.forEach(vm.videoAttachInfo, function(obj){
                        videoAttachSeqs += obj.ATTACH_SEQ + '|';
                    });
                    vm.videoAttachSeqs = videoAttachSeqs.substr(0,videoAttachSeqs.length -1);   //[선택]동영상 첨부파일 일련번호

                    var companyNoAttachSeqs = '';
                    var companyNofileOrgName = '';
                    _.forEach(companyNoAttachInfo, function(obj){
                        companyNoAttachSeqs += obj.ATTACH_SEQ + '|';
                        companyNofileOrgName += obj.FILE_ORG_NAME + '|';
                        vm.companyNofileDisabled = true;
                    });
                    vm.companyNoAttachSeqs = companyNoAttachSeqs.substr(0,companyNoAttachSeqs.length -1);   //[선택]사업자등록증 및 증빙서류 첨부파일 일련번호
                    vm.companyNofileOrgName = companyNofileOrgName.substr(0,companyNofileOrgName.length -1);   //[선택]사업자등록증 및 증빙서류 첨부파일 일련번호

                    // 경력정보
                    var memberCareerInfo = data.user.resultData.myCareerInfo;
                    _.forEach(memberCareerInfo, function(obj){
                        vm.memberCareerInfo.push({
                            field : obj.FIELD,								//[필수][공코 : FIELD] 경력 분류
                            pgmName : obj.PGM_NAME,							//[필수]프로그램 명
                            broadcastingStation : obj.BROADCASTING_STATION,	//[필수][공코 : BROADCASTING_STATION] 방송국
                            year : obj.YEAR,								//[필수]년도
                            part : obj.PART,								//[필수][공코 : PART] 배역
                            linesYn : obj.LINES_YN							//[필수]대사
                        });
                    });

                    vm.memberDetailInfo = {
                        job: memberInfo.JOB, // [필수] 직업
                        // supportField: memberInfo.SUPPORT_FIELD, // [필수] 지원분야
                        // hopeCast: memberInfo.HOPE_CAST, // [필수] 희망배역
                        // hopeCastEtc: _.isEmpty(memberInfo.HOPE_CAST_ETC) ? '' : memberInfo.HOPE_CAST_ETC, // 희망배역 기타
                        bodyInfo: _.isEmpty(memberInfo.BODY_INFO) ? '' : memberInfo.BODY_INFO, // 신체사항
                        classInfo: _.isEmpty(memberInfo.CLASS_INFO) ? '' : memberInfo.CLASS_INFO, // 보유 방송등급
                        hairState: _.isEmpty(memberInfo.HAIR_STATE) ? '' : memberInfo.HAIR_STATE, // 머리색
                        hairHeight: _.isEmpty(memberInfo.HAIR_HEIGHT) ? '' : memberInfo.HAIR_HEIGHT, // 머리길이
                        hairEtc: _.isEmpty(memberInfo.HAIR_ETC) ? '' : memberInfo.HAIR_ETC, // 헤어상태 상세
                        toothCorrec: _.isEmpty(memberInfo.TOOTH_CORREC) ? '' : memberInfo.TOOTH_CORREC, // 치아교정여부
                        exposureInfo: _.isEmpty(memberInfo.EXPOSURE_INFO) ? '' : memberInfo.EXPOSURE_INFO, // 노출가능여부
                        tatooInfo: _.isEmpty(memberInfo.TATOO_INFO) ? '' : memberInfo.TATOO_INFO, // 문신
                        pierInfo: _.isEmpty(memberInfo.PIER_INFO) ? '' : memberInfo.PIER_INFO, // 피어싱
                        tatooEtc: _.isEmpty(memberInfo.TATOO_ETC) ? '' : memberInfo.TATOO_ETC, // 문신, 피어싱 상세
                        beardInfo: _.isEmpty(memberInfo.BEARD_INFO) ? '' : memberInfo.BEARD_INFO, // 수염정보
                        dimpleInfo: _.isEmpty(memberInfo.DIMPLE_INFO) ? '' : memberInfo.DIMPLE_INFO, // 보조개
                        doubleEyeInfo: _.isEmpty(memberInfo.DOUBLE_EYE_INFO) ? '' : memberInfo.DOUBLE_EYE_INFO, // 쌍커풀
                        bottomsSize: _.isEmpty(memberInfo.BOTTOMS_SIZE) ? '' : memberInfo.BOTTOMS_SIZE, // 발사이즈
                        topSize: _.isEmpty(memberInfo.TOP_SIZE) ? '' : memberInfo.TOP_SIZE, // 발사이즈
                        footSize: _.isEmpty(memberInfo.FOOT_SIZE) ? '' : memberInfo.FOOT_SIZE, // 발사이즈
                        driveBicycleYn: _.isEmpty(memberInfo.DRIVE_BICYCLE_YN) ? '' : memberInfo.DRIVE_BICYCLE_YN, // 자전거 운전
                        driveCarYn: _.isEmpty(memberInfo.DRIVE_CAR_YN) ? '' : memberInfo.DRIVE_CAR_YN, // 자동차 운전
                        driveMotercycleYn: _.isEmpty(memberInfo.DRIVE_MOTERCYCLE_YN) ? '' : memberInfo.DRIVE_MOTERCYCLE_YN, // 오토바이 운전
                        smokeYn: _.isEmpty(memberInfo.SMOKE_YN) ? '' : memberInfo.SMOKE_YN, // 흡연여부
                        dressBaseCount: _.isBlank(memberInfo.DRESS_BASE_COUNT) ? '' : memberInfo.DRESS_BASE_COUNT, // 기본정장 벌수
                        dressBaseColor: _.isEmpty(memberInfo.DRESS_BASE_COLOR) ? '' : memberInfo.DRESS_BASE_COLOR, // 기본정장 색상
                        dressSemiCount: _.isBlank(memberInfo.DRESS_SEMI_COUNT) ? '' : memberInfo.DRESS_SEMI_COUNT, // 세미정장 벌수
                        dressSemiColor: _.isEmpty(memberInfo.DRESS_SEMI_COLOR) ? '' : memberInfo.DRESS_SEMI_COLOR, // 세미정장 색상
                        dressBlackShoes: _.isEmpty(memberInfo.DRESS_BLACK_SHOES) ? '' : memberInfo.DRESS_BLACK_SHOES, // 검정구두
                        dressSwimsuit: _.isEmpty(memberInfo.DRESS_SWIMSUIT) ? '' : memberInfo.DRESS_SWIMSUIT, // 검정구두
                        dressSchoolUniform: _.isEmpty(memberInfo.DRESS_SCHOOL_UNIFORM) ? '' : memberInfo.DRESS_SCHOOL_UNIFORM, // 검정구두
                        dressMilitaryBoots: _.isEmpty(memberInfo.DRESS_MILITARY_BOOTS) ? '' : memberInfo.DRESS_MILITARY_BOOTS // 검정구
                    }
                    vm.supportAndHopeInfo = data.user.resultData.supportAndHope;
                })
                .catch(function(error) {});
        }, 100);
    }

    if ($stateParams.param) {
        if($stateParams.param.memberCareerInfo){
            vm.isMemberCareerInfoChange = true;
            vm.memberCareerInfo = $stateParams.param.memberCareerInfo;
        }
        if($stateParams.param.photoAttachInfo){
            vm.photoAttachInfo = $stateParams.param.photoAttachInfo;

            vm.photoAttachSeqs = '';
            _.forEach(vm.photoAttachInfo, function(obj){
                vm.photoAttachSeqs += obj.ATTACH_SEQ + '|';
            });
            if(!_.isEmpty(vm.photoAttachSeqs)){
                vm.photoAttachSeqs = vm.photoAttachSeqs.substr(0,vm.photoAttachSeqs.length -1);   //[필수]사진 첨부파일 일련번호
            }
        }
        if($stateParams.param.videoAttachInfo){
            vm.videoAttachInfo = $stateParams.param.videoAttachInfo;

            vm.videoAttachSeqs = '';
            _.forEach(vm.videoAttachInfo, function(obj){
                vm.videoAttachSeqs += obj.ATTACH_SEQ + '|';
            });
            if(!_.isEmpty(vm.videoAttachSeqs)){
                vm.videoAttachSeqs = vm.videoAttachSeqs.substr(0,vm.videoAttachSeqs.length -1);   //[선택]동영상 첨부파일 일련번호
            }
        }
        if($stateParams.param.supportAndHopeInfo){
          vm.supportAndHopeInfo = $stateParams.param.supportAndHopeInfo;
        }
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

    /**
     * 전화번호 재인증
     */
  function reHpAuth(){
    vm.hpAuthDis = false;
    vm.hpAuthFlag = false;
  }

  /**
   * 전화번호 인증
   */
  function hpAuth(){

      var chk = true;
      chk = chk && ngspublic.nullCheck(vm.memberNm, 	'이름을 입력해주세요.');
      chk = chk && ngspublic.nullCheck(vm.memberHpKind, 	'통신사를 선택해주세요.', '999');
      chk = chk && ngspublic.nullCheck(vm.memberHp,	'휴대폰번호를 입력해주세요.');
      if(!chk) return false;

      var param = {};
      param.findKind = '4';								// 1: 개인휴대폰번호찾기 2: 기업휴대폰번호찾기 3 : 기업사업자번호로대표자아이디찾기 4 : 휴대폰인증
      param.userId = vm.memberId;							// 아이디
      param.userName = vm.memberNm;
      param.userHpkind = vm.memberHpKind;					// 휴대폰회사[공코:HP_KIND]
      param.userHp = vm.memberHp; 							// 휴대폰 번호
      // param.mobileYn = 'N';

      MemberSvc.requestPwFindAuth(param)
          .then(function(data){
              logger.log('requestPwFindAuth[SUCCESS]', 'data: ', data);
              if(data.user.resultData.sEncData.length > 0){
                  var sEncData = data.user.resultData.sEncData;
                  // vm.reqSeq = data.user.resultData.reqSeq;
                  vm.hpAuthDis = true;

                  // 인증 팝업창 호출
                  $rootScope.openAuthCheckPopup(sEncData, function (data){
                      if(data.result === 'success'){
                          $message.alert('인증이 완료되었습니다. 계속 진행해주세요.');
                          vm.hpAuthFlag = true;
                      }
                  });
              }
          })
          .catch(function(error){
              vm.hpAuthDis = false;
              logger.log('requestPwFindAuth[FAILED]', 'error: ', error);
          })

  }

  /**
   * 주소검색
   */
  function addrSearch() {
    $rootScope.openAddrSearchPopup(function(result){
      logger.log('callback addrSearch() ', result);
      // result = {
      //     addrDetail: "107동 204호",
      //     admCd: "4136037021",
      //     bdKdcd: "1",
      //     bdMgtSn: "4136037021101830006006319",
      //     bdNm: "강남아파트",
      //     buldMnnm: "19",
      //     buldSlno: "0",
      //     detBdNmList: "101동,102동,103동,104동,105동,106동,107동,관리사무소,노인정",
      //     emdNm: "퇴계원면",
      //     emdNo: "01",
      //     engAddr: "19, Dojewon-ro, Toegyewon-myeon, Namyangju-si, Gyeonggi-do",
      //     inputYn: "Y",
      //     jibunAddr: "경기도 남양주시 퇴계원면 퇴계원리 221 강남아파트",
      //     liNm: "퇴계원리",
      //     lnbrMnnm: "221",
      //     lnbrSlno: "0",
      //     mtYn: "0",
      //     rn: "도제원로",
      //     rnMgtSn: "413603197019",
      //     roadAddrPart1: "경기도 남양주시 퇴계원면 도제원로 19",
      //     roadAddrPart2: "(강남아파트)",
      //     roadFullAddr: "경기도 남양주시 퇴계원면 도제원로 19, 107동 204호 (강남아파트)",
      //     sggNm: "남양주시",
      //     siNm: "경기도",
      //     udrtYn: "0",
      //     zipNo: "12123"
      // }

      vm.memberAddrNo = result.zipNo;
      vm.memberAddrBase = result.roadFullAddr;
      vm.memberAddr = `${result.zipNo} ${result.roadFullAddr}`;
    });
  }

  /**
   * 사진등록 클릭
   */
  function onAddPhotoButtonClick() {
      if($message.confirm('사진을 수정 하시겠습니까?')){
          var param = { photoAttachInfo: vm.photoAttachInfo };
          $rootScope.goView('pesn.infomod.piRegPhoto', vm, param);
      }
  }
  /**
   * 영상등록 클릭
   */
  function onAddVideoButtonClick() {
      if($message.confirm('영상을 수정 하시겠습니까?')){
          var param = { videoAttachInfo: vm.videoAttachInfo };
          $rootScope.goView('pesn.infomod.piRegVideo', vm, param);
      }
  }
  /**
   * 경력사항 입력 클릭
   */

  function onAddCareerButtonClick() {
      if($message.confirm('경력사항을 다시 등록 하시겠습니까?')){
          var param = { memberCareerInfo: vm.memberCareerInfo };
          $rootScope.goView('pesn.infomod.piRegCareer', vm, param);
      }
  }


  /**
   * 기본정보 수정
   */
  function onUpdateInfoClick(kind) {

    /*======================================
      NULL CHECK
    ========================================*/
    var isValid = true;

    // 비밀번호
//    isValid = isValid && ngspublic.nullCheck(vm.memberPw, '비밀번호를 입력해주세요.');
    if(!_.isBlank(vm.memberPw)){
      isValid = isValid && ngspublic.nullCheck(vm.memberPwChk, '비밀번호 확인을 입력해주세요.');
      if (isValid && vm.memberPw !== vm.memberPwChk) {
        $message.alert('비밀번호가 일치하지 않습니다.');
        return false;
      }
    }
    // 성별
    isValid = isValid && ngspublic.nullCheck(vm.memberGender, '성별을 선택해주세요.');
    // 주소
    isValid = isValid && ngspublic.nullCheck(vm.memberAddr, '거주지를 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.memberSubwayLines, '가까운 지하철호선을 입력해주세요.', '999');
    isValid = isValid && ngspublic.nullCheck(vm.memberSubwayName, '가까운 지하철역을 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.memberSubwayTime, '가까운 지하철역 소요시간을 입력해주세요.', '999');
    // 연락처
    isValid = isValid && ngspublic.nullCheck(vm.memberHpKind, '통신사를 선택해주세요.', '999');
    isValid = isValid && ngspublic.nullCheck(vm.memberHp, '전화번호를 입력해주세요.');
    // 키/몸무게
    isValid = isValid && ngspublic.nullCheck(vm.height, '키를 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.weight, '몸무게를 입력해주세요.');
    // 이메일
    isValid = isValid && ngspublic.nullCheck(vm.emailId, '이메일을 입력해주세요.');
    isValid = isValid && ngspublic.nullCheck(vm.emailDomainId, '이메일을 입력해주세요.', '999');
    if (isValid && vm.emailDomainId === '0' && vm.emailDomain === '') {
      $message.alert('이메일을 입력해주세요.');
      return false;
    }

    // 사진 첨부 여부
//    isValid = isValid && ngspublic.nullCheck(vm.photoAttachSeqs, '사진을 첨부해주세요.');
    if (!isValid) return false;

    /*======================================
      중복확인
    ========================================*/
    if (!vm.hpAuthFlag) {
      $message.alert('전화번호 인증을 해주세요.');
      return false;
    }
    /*======================================
      데이터 가공
    ========================================*/
    var memberHp = vm.memberHp.replace(/-/g, '');
    var email = `${vm.emailId}@${vm.emailDomainId === '0' ? vm.emailDomain : vm.emailDomainId}`;
    var accNo = vm.accNo.replace(/-/g, '');

    /*======================================
      파라미터 생성
    ========================================*/
    var memberBaseInfo = {
      memberKind: vm.memberKind, // [필수] 회원유형
      memberEntercompayInfo: vm.memberEntercompayInfo || '', // 소속사 (회원유형 'M2')
      memberCompayInfo: vm.memberCompayInfo || '', // 추천기업 (회원유형 'M3')
      memberNm: vm.memberNm, // [필수] 이름
      memberId: vm.memberId, // [필수] 아이디
      memberGender: vm.memberGender, // [필수] 성별
      memberAddr: vm.memberAddr, // [필수] 기본주소
      memberSubwayLines : vm.memberSubwayLines, //[필수]지하철호선
      memberSubwayName : vm.memberSubwayName,   //[필수]가까운지하철
      memberSubwayTime : vm.memberSubwayTime,   //[필수]소요시간
      memberHpKind: vm.memberHpKind, // [필수] 연락처 - 통신사
      memberHp, // [필수] 연락처
      height: vm.height, // [필수] 키
      weight: vm.weight, // [필수] 몸무게
      email, // [필수] 이메일
      accBankName: vm.accBankName === '999' ? '' : _.find(vm.bankNameInfo, {COMMON_INFO_VALUE1 : vm.accBankName}).COMMON_INFO_VALUE2, // [선택] 계좌번호 - 은행
      accNo, // [선택] 계좌번호 - 계좌번호
      accName: vm.accName, // [선택] 계좌번호 - 예금주
      companyNoAttachSeqs: vm.companyNoAttachSeqs, // [선택] 사업자등록증 첨부
      photoAttachSeqs: vm.photoAttachSeqs, // [필수] 사진 첨부
      videoAttachSeqs: vm.videoAttachSeqs // [선택] 동영상 첨부
    };
    
    if(!_.isBlank(vm.memberPw)){
      memberBaseInfo.memberPw = vm.memberPw;
    }

      var param = {
          memberBaseInfo,                           // 회원 기본 정보
      };

    // 경력사항 변경 여부 체크
      if(vm.isMemberCareerInfoChange){
          param.memberCareerInfo = vm.memberCareerInfo;
      }

    if(kind === '1'){
        GuestSvc.updateMyInfo(param)
            .then(function(data){

                // 정상적으로 등록되면 촬영 불가일세팅
                $message.alert('정상적으로 수정되었습니다.');
                $rootScope.goView('pesn.mainp.pmMain');
            })
            .catch(function(err){
            })
    }else{
        param.memberDetailInfo = vm.memberDetailInfo;
        param.supportAndHopeInfo = vm.supportAndHopeInfo;
        $rootScope.goView('pesn.infomod.piPesnModDetail', vm, param);
    }
  }

    /**
     * 파일 추가
     */
    function fileAdd(){
        angular.element('#file').click();
    }

    /**
     * 파일 삭제
     */
    function fileDel(){

        var chk = ngspublic.nullCheck(vm.companyNoAttachSeqs,       '파일정보가 없습니다.');
        if(!chk){
            $('#file').val('');
            vm.companyNoAttachSeqs = '';
            vm.companyNofileDisabled = false;
            return false;
        }

        var confirmResult = $message.confirm('정말 삭제하시겠습니까?');
        if(!confirmResult) return false;

        var param = {};

        vm.companyNofileDisabled = false;
        $("#file").val("");
        param.attachSeq = vm.companyNoAttachSeqs;     // [필수]파일등록 후 결과값에 파일 일련번호
        param.fileOrgName = vm.companyNofileOrgName;       // [필수]파일등록 후 결과값에 파일 원본파일이름

        ComSvc.fileDelete(param)
            .then(function(data){
                $message.alert('정상적으로 삭제되었습니다.');

                vm.companyNoAttachSeqs = '';
                vm.companyNofileOrgName = '';
                vm.readfile = '';

                $("#form").get(0).reset();
            })
            .catch(function(err){
              vm.companyNoAttachSeqs = '';
              vm.companyNofileOrgName = '';
              vm.readfile = '';

              $("#form").get(0).reset();
                logger.error('message: ' + err + '\n');
            })
    }

    /**
     * 파일전송
     */
    function fileAttach(){
        vm.companyNofileDisabled = true;

        $rootScope.safeApply(function() {
            $rootScope.requestCount++;
        });

        $("#form").ajaxForm({
            url : $env.endPoint.service + "/api/com/fileUpload.login",
            enctype : "multipart/form-data",
            dataType : "json",
            beforeSubmit: function (data, frm, opt) {
                var objFind = _.find(data, {type : 'file'});
                if(objFind){
                    if(!_.isNull(objFind.value) && objFind.value != ""){
                        return true;
                    }
                }

                $rootScope.requestCount--;
                vm.companyNofileDisabled = false;
                $message.alert('파일 선택 후 진행 해주세요.');
                return false;
            },
            error : function(){
                $scope.$apply(function(){

                    vm.companyNofileDisabled = false;
                    vm.companyNofileOrgName = '';

                    $rootScope.requestCount--;
                    $message.alert("파일 전송 실패") ;
                });
            },
            success : function(result){
                $scope.$apply(function(){
                    $rootScope.requestCount--;

                    if(result.user.result == 'Fail'){
                        $message.alert(result.user.resultMessage);
                        return false;
                    }

                    vm.companyNoAttachSeqs = result.user.resultData.attachSeq;
                    vm.companyNofileOrgName = result.user.resultData.orgName;

                    $message.alert("파일 전송 성공");
                });
            }
        });

        $("#form").submit();
    }
}
