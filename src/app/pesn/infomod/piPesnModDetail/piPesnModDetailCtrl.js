angular
  .module('pesn.infomod')
  .controller('piPesnModDetailCtrl', piPesnModDetailCtrl);

function piPesnModDetailCtrl($log, $scope, $q, $rootScope, $message, $stateParams, ComSvc, ngspublic, GuestSvc) {
    /**
     * Private variables
     */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        // 전 단계에서 넘어온 정보
        isOnLoad : false,
        isMemberCareerInfoChange : false,
        memberBaseInfo: {},
        memberCareerInfo: [],

        // SELECT OPTIONS
        footSizeOptions: [{ value: '999', text: '발사이즈 선택'}], // 발사이즈 리스트

        // RADIO OPTIONS
        supportFieldOptions: [], // 지원분야 리스트
        hopeCastOptions: [], // 지원배역 리스트
        beardOptions: [], // 수염정보 리스트
        driveBicycleOptions: [], // 자전거운전 가능여부 리스트
        driveCarOptions: [], // 자동차 운전 가능여부 리스트
        driveMotercycleOptions: [], // 오토바이운전 가능여부 리스트
        smokeOptions: [], // 흡연여부 리스트

        hopeCastEtc : '', // 희망배역 기타

        job: '', // 직업
        supportField: '', // 지원분야
        supportFieldText: '', // 지원분야 텍스트
        hopeCast: '', // 희망배역
        bodyInfo: '999', // 신체사항
        classInfo: '999', // 보유 방송등급
        hairState: '999', // 머리색
        hairHeight: '999', // 머리길이
        hairEtc : '',						// 헤어상태 상세 설명
        toothCorrec: '999', // 치아교정여부
        exposureInfo: '999', // 노출가능여부
        tatooInfo: '999', // 문신
        pierInfo: '999', // 피어싱
        tatooEtc : '',					// 문신,피어싱 상세설명
        beardInfo: '999', // 수염정보
        dimpleInfo: '999', // 보조개
        doubleEyeInfo: '999', // 쌍커풀
        bottomsSize: '999', // 하의사이즈
        topSize: '999', // 상의사이즈
        footSize: '999', // 발사이즈
        driveBicycleYn: '', // 자전거 운전
        driveCarYn: '', // 자동차 운전
        driveMotercycleYn: '', // 오토바이 운전
        smokeYn: '', // 흡연여부
        isDressBase: false, // 기본정장 체크박스
        dressBaseCount: '', // 기본정장 벌수
        dressBaseColor: '', // 기본정장 색상
        isDressSemi: false, // 세미정장 체크박스
        dressSemiCount: '', // 세미정장 벌수
        dressSemiColor: '', // 세미정장 색상
        dressBlackShoes: '', // 검정구두 체크박스 ("Y" = true, "" = false)
        dressSwimsuit: '', // 수영복 체크박스 ("Y" = true, "" = false)
        dressSchoolUniform: '', // 교복 체크박스 ("Y" = true, "" = false)
        dressMilitaryBoots: '', // 군화 체크박스 ("Y" = true, "" = false)
        dressSamplePopup : false,
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        onSupportFieldChange: onSupportFieldChange, // 지원분야 클릭시
        onHopeCastChange: onHopeCastChange,         // 희망분야 클릭시
        onSubmit: onSubmit // 확인버튼 클릭시
    });

    // 지원분야 변경시 모집분야 가져오기
    // $scope.$watch('piPesnModDetail.supportField', function (newVal, oldVal, scope) {
    //     if (newVal === oldVal) return false;
    //     if (vm.isOnLoad) {
    //         vm.isOnLoad = false;
    //         return false;
    //     }
    //     // if (newVal === 'S12') {
    //     //     vm.hopeCastOptions = [];
    //     //     vm.hopeCast = '';
    //     //     return false;
    //     // }

    //     vm.hopeCastEtc = '';

    //     var pgmReutField = newVal.replace('S', 'P');

    //     vm.hopeCastOptions = [];
    //     vm.hopeCast = '';
    //     ComSvc.selectCommonInfo('HOPE_CAST').then(function (data) {
    //         data.user.resultData.commonData
    //             .filter(function (hopeCast) {
    //                 return pgmReutField === hopeCast.COMMON_INFO_VALUE3;
    //             })
    //             .forEach(function (hopeCast) {
    //                 vm.hopeCastOptions.push({
    //                     value: hopeCast.COMMON_INFO_VALUE1,
    //                     text: hopeCast.COMMON_INFO_VALUE2
    //                 })
    //             })
    //     });
    // });

    /**
     * Initialize
     */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        // 발사이즈 리스트
        for (var i = 100; i <= 350; i += 5) {
            vm.footSizeOptions.push({value: `${i}`, text: `${i}mm`});
        }

      /* ========== 공통코드 조회 ========== */
      getHopeCast()
          .then(getSupportField)
          .then(getCommonCodeList('BEARD_INFO', 'beardOptions'))   // 수염
          .then(getCommonCodeList('DRIVE_BICYCLE_YN', 'driveBicycleOptions')) // 자전거운전
          .then(getCommonCodeList('DRIVE_CAR_YN', 'driveCarOptions')) // 자동차운전
          .then(getCommonCodeList('DRIVE_MOTERCYCLE_YN', 'driveMotercycleOptions')) // 오토바이운전
          .then(getCommonCodeList('SMOKE_YN', 'smokeOptions')) // 흡연여부
          .then(getParameter);    // 파라미터 받기
    }
    
    function getSupportField(){
      var d = $q.defer();
      // 지원분야
      ComSvc.selectCommonInfo('SUPPORT_FIELD').then(function(data) {
        data.user.resultData.commonData.forEach(function(result) {
          var pgmReutField = result.COMMON_INFO_VALUE1.replace('S', 'P');
          var hopeCastInfo = vm.hopeCastOptions.filter(function(hopeCast) {
            return pgmReutField === hopeCast.type;
          });
          vm.supportFieldOptions.push({
            value: result.COMMON_INFO_VALUE1,
            text: result.COMMON_INFO_VALUE2,
            hopeCastOptions : hopeCastInfo,
            chk: false
          });
        });
        d.resolve();
      }).catch(function(err){
        logger.error(err);
        d.reject();
      });
      return d.promise;
    }

    function getHopeCast(){
      var d = $q.defer();
      // 희망배역
      ComSvc.selectCommonInfo('HOPE_CAST').then(function(data) {
        data.user.resultData.commonData.forEach(function(result) {
          vm.hopeCastOptions.push({
              value: result.COMMON_INFO_VALUE1,
              text: result.COMMON_INFO_VALUE2,
              type: result.COMMON_INFO_VALUE3,
              chk: false
          });
        });
        d.resolve();
      }).catch(function(err){
        logger.error(err);
        d.reject();
      });
      return d.promise;
    }

    /**
     * Event handlers
     */
    function destroy(event) {
        logger.debug(event);
    }

    /**
     * Custom functions
     */

    /**
     * 파라미터 받기
     */
    function getParameter(){
        // 이전 단계에서 넘어온 정보 저장
        if ($stateParams.param && $stateParams.param.memberBaseInfo) {
            vm.memberBaseInfo = $stateParams.param.memberBaseInfo;
        }
        if ($stateParams.param && $stateParams.param.memberCareerInfo) {
            vm.memberCareerInfo = $stateParams.param.memberCareerInfo;
            vm.isMemberCareerInfoChange = true;
        }

        if ($stateParams.param && $stateParams.param.memberDetailInfo) {
            var memberDetailInfo = $stateParams.param.memberDetailInfo;
            vm.job = memberDetailInfo.job; // [필수] 직업
            // vm.supportField = memberDetailInfo.supportField; // [필수] 지원분야
            // var findObj = _.where(vm.supportFieldOptions, {value: memberDetailInfo.supportField});
            // vm.supportFieldText = findObj[0].text;

            // if (memberDetailInfo.supportField == 'S12') {
            //     vm.hopeCastEtc = memberDetailInfo.hopeCastEtc; // 희망배역 기타
            // }

            // var pgmReutField = memberDetailInfo.supportField.replace('S', 'P');

            // ComSvc.selectCommonInfo('HOPE_CAST').then(function (data) {
            //     data.user.resultData.commonData
            //         .filter(function (hopeCast) {
            //             return pgmReutField === hopeCast.COMMON_INFO_VALUE3;
            //         })
            //         .forEach(function (hopeCast) {
            //             vm.hopeCastOptions.push({
            //                 value: hopeCast.COMMON_INFO_VALUE1,
            //                 text: hopeCast.COMMON_INFO_VALUE2
            //             })
            //         })
            //     // 희망배역 파라미터 처리
            //     //vm.hopeCast = memberDetailInfo.hopeCast; // [필수] 희망배역
            //     var hopeCast = memberDetailInfo.hopeCast.split('|');
            //     _.forEach(vm.hopeCastOptions, function (obj) {
            //         _.forEach(hopeCast, function (hopeObj) {
            //             if (hopeObj === obj.value) {
            //                 obj.chk = true;
            //             }
            //         });
            //     });
            // });

            vm.bodyInfo = _.isEmpty(memberDetailInfo.bodyInfo) ? '999' : memberDetailInfo.bodyInfo; // 신체사항
            vm.classInfo = _.isEmpty(memberDetailInfo.classInfo) ? '999' : memberDetailInfo.classInfo; // 보유 방송등급
            vm.hairState = _.isEmpty(memberDetailInfo.hairState) ? '999' : memberDetailInfo.hairState; // 머리색
            vm.hairHeight = _.isEmpty(memberDetailInfo.hairHeight) ? '999' : memberDetailInfo.hairHeight; // 머리길이
            vm.hairEtc = memberDetailInfo.hairEtc; // 헤어상태 상세
            vm.toothCorrec = _.isEmpty(memberDetailInfo.toothCorrec) ? '999' : memberDetailInfo.toothCorrec; // 치아교정여부
            vm.exposureInfo = _.isEmpty(memberDetailInfo.exposureInfo) ? '999' : memberDetailInfo.exposureInfo; // 노출가능여부
            vm.tatooInfo = _.isEmpty(memberDetailInfo.tatooInfo) ? '999' : memberDetailInfo.tatooInfo; // 문신
            vm.pierInfo = _.isEmpty(memberDetailInfo.pierInfo) ? '999' : memberDetailInfo.pierInfo; // 피어싱
            vm.tatooEtc = memberDetailInfo.tatooEtc; // 문신, 피어싱 상세
            vm.beardInfo = _.isEmpty(memberDetailInfo.beardInfo) ? '999' : memberDetailInfo.beardInfo; // 수염정보
            vm.dimpleInfo = _.isEmpty(memberDetailInfo.dimpleInfo) ? '999' : memberDetailInfo.dimpleInfo; // 보조개
            vm.doubleEyeInfo = _.isEmpty(memberDetailInfo.doubleEyeInfo) ? '999' : memberDetailInfo.doubleEyeInfo; // 쌍커풀
            vm.bottomsSize = _.isEmpty(memberDetailInfo.bottomsSize) ? '999' : memberDetailInfo.bottomsSize; // 발사이즈
            vm.topSize = _.isEmpty(memberDetailInfo.topSize) ? '999' : memberDetailInfo.topSize; // 발사이즈
            vm.footSize = _.isEmpty(memberDetailInfo.footSize) ? '999' : memberDetailInfo.footSize; // 발사이즈
            vm.driveBicycleYn = memberDetailInfo.driveBicycleYn; // 자전거 운전
            vm.driveCarYn = memberDetailInfo.driveCarYn; // 자동차 운전
            vm.driveMotercycleYn = memberDetailInfo.driveMotercycleYn; // 오토바이 운전
            vm.smokeYn = memberDetailInfo.smokeYn; // 흡연여부

            vm.dressBaseCount = memberDetailInfo.dressBaseCount; // 기본정장 벌수
            vm.dressBaseColor = memberDetailInfo.dressBaseColor; // 기본정장 색상
            vm.dressSemiCount = memberDetailInfo.dressSemiCount; // 세미정장 벌수
            vm.dressSemiColor = memberDetailInfo.dressSemiColor; // 세미정장 색상
            vm.dressBlackShoes = !_.isBlank(memberDetailInfo.dressBlackShoes) && memberDetailInfo.dressBlackShoes > 0; // 검정구두
            vm.dressSwimsuit = !_.isBlank(memberDetailInfo.dressSwimsuit) && memberDetailInfo.dressSwimsuit > 0; // 수영복
            vm.dressSchoolUniform = !_.isBlank(memberDetailInfo.dressSchoolUniform) && memberDetailInfo.dressSchoolUniform > 0; // 교복
            vm.dressMilitaryBoots = !_.isBlank(memberDetailInfo.dressMilitaryBoots) && memberDetailInfo.dressMilitaryBoots > 0; // 군복

            vm.isDressBase = !_.isBlank(memberDetailInfo.dressBaseCount) && memberDetailInfo.dressBaseCount > 0;
            vm.isDressSemi = !_.isBlank(memberDetailInfo.dressSemiCount) && memberDetailInfo.dressSemiCount > 0;

            vm.isOnLoad = true;
        }

        if ($stateParams.param && $stateParams.param.supportAndHopeInfo) {
            var supportAndHopeInfos = $stateParams.param.supportAndHopeInfo;
            _.forEach(supportAndHopeInfos, function(obj){
                _.forEach(vm.supportFieldOptions, function(obj2){
                    if(obj.SUPPORT_FIELD === obj2.value){
                        obj2.chk = true;
                        _.forEach(obj2.hopeCastOptions, function(obj3){
                            if(obj.HOPE_CAST === obj3.value){
                                obj3.chk = true;
                                if(obj3.value === 'H6'){
                                    obj2.hopeCast = obj3.value;
                                    obj2.hopeCastEtc = obj.HOPE_CAST_ETC;
                                }
                            }
                        });
                    }
                });
            });
        }
    }

    // 공통코드 조회
    function getCommonCodeList(commonCode, optionsName) {
        var d = $q.defer();
        ComSvc.selectCommonInfo(commonCode)
            .then(function(data) {
                data.user.resultData.commonData.forEach(function(result) {
                    vm[optionsName].push({
                        value: result.COMMON_INFO_VALUE1,
                        text: result.COMMON_INFO_VALUE2
                    });
                });
                d.resolve();
            })
            .catch(function(err){
                logger.error(err);
                d.reject();
            });
        return d.promise;
    }

  // 지원분야 클릭시
  function onSupportFieldChange(supportField) {
    if(supportField.chk === false){
      supportField.hopeCastOptions.forEach(function(result) {
        result.chk = false;
      });
      supportField.hopeCast = '';
      supportField.hopeCastEtc = '';
    }
  }

  // 희망배역 클릭시
  function onHopeCastChange(supportField, hopeCast) {
    if(hopeCast.chk === false){
      if(hopeCast.value === 'H6'){
        supportField.hopeCast = '';
      }
    }else{
      if(hopeCast.value === 'H6'){
        supportField.hopeCast = hopeCast.value;
      }
    }
  }
    // 확인버튼 클릭시
    function onSubmit() {

        /*======================================
          NULL CHECK
        ========================================*/
        var isValid = true;

        // 직업
        isValid = isValid && ngspublic.nullCheck(vm.job, '직업을 입력해주세요.');
        // // 지원분야
        // isValid = isValid && ngspublic.nullCheck(vm.supportField, '지원분야를 선택해주세요.');

        // // 희망배역 파라미터 처리
        // var findObj = _.where(vm.hopeCastOptions, {chk : true});
        // var hopeCast = '';
        // _.forEach(findObj,function(obj){
        //     hopeCast += obj.value + '|'
        // });

        // vm.hopeCast = hopeCast.substr(0,hopeCast.length -1);
        // isValid = isValid && ngspublic.nullCheck(vm.hopeCast, '희망배역을 선택/입력해주세요.');

        // if(vm.supportField == 'S12'){
        //     isValid = isValid && ngspublic.nullCheck(vm.hopeCastEtc, '분야[기타]는 희망배역기타를 입력하셔야합니다.');
        // }
        // 지원분야 체크
    var supportField = _.where(vm.supportFieldOptions, {chk : true});

    if(isValid){
      
      // 지원분야 체크
      if(supportField.length <= 0){
        $message.alert('지원분야를 선택해주세요.');
        isValid = false;
			  return false;
      }

      // 희망배역 체크
      _.forEach(supportField, function(supportFieldObj){
        var hopeCast = _.where(supportFieldObj.hopeCastOptions, {chk : true});
        if(hopeCast.length <= 0){
          $message.alert(supportFieldObj.text + '의 희망배역을 선택해주세요.');
          isValid = false;
          return false;
        }else{
          // 기타 체크
          var msg = '';
          _.forEach(hopeCast, function(obj){
            if(obj.value === 'H6'){
              if(obj.chk){
                if(supportFieldObj.hopeCastEtc == undefined || supportFieldObj.hopeCastEtc == ''){
                  msg = '희망배역(' + supportFieldObj.text + ')의 기타내용을 입력해주세요.';
                  return false;
                }
              }
            }
          })
          
          if(msg != ''){
            $message.alert(msg);
            isValid = false;
            return false;
          }
        }
      });

    }
        
        if (!isValid) return false;

        var supportAndHopeInfoData = [];
        _.forEach(supportField, function(obj){
          var hopeCast = _.where(obj.hopeCastOptions, {chk : true});
          _.forEach(hopeCast, function(obj2){
              var 	supportAndHopeInfo = {};
              supportAndHopeInfo.memberSeq = vm.memberBaseInfo.MEMBER_SEQ || '';
              supportAndHopeInfo.supportField = obj.value;
              supportAndHopeInfo.hopeCast = obj2.value;
              supportAndHopeInfo.hopeCastEtc = obj2.value === 'H6' ? obj.hopeCastEtc : '';
              
              supportAndHopeInfoData.push(supportAndHopeInfo);
          })
        })

        /*======================================
          파라미터 생성
        ========================================*/
        var memberBaseInfo = vm.memberBaseInfo;
        var memberCareerInfo = vm.memberCareerInfo;
        var memberDetailInfo = {
            job: vm.job, // [필수] 직업
            supportField: vm.supportField, // [필수] 지원분야
            hopeCast: vm.hopeCast, // [필수] 희망배역
            hopeCastEtc: vm.hopeCastEtc, // 희망배역 기타
            bodyInfo: vm.bodyInfo === '999' ? '' : vm.bodyInfo, // 신체사항
            classInfo: vm.classInfo === '999' ? '' : vm.classInfo, // 보유 방송등급
            hairState: vm.hairState === '999' ? '' : vm.hairState, // 머리색
            hairHeight: vm.hairHeight === '999' ? '' : vm.hairHeight, // 머리길이
            hairEtc: vm.hairEtc, // [선택] 헤어상태 상세
            toothCorrec: vm.toothCorrec === '999' ? '' : vm.toothCorrec, // 치아교정여부
            exposureInfo: vm.exposureInfo === '999' ? '' : vm.exposureInfo, // 노출가능여부
            tatooInfo: vm.tatooInfo === '999' ? '' : vm.tatooInfo, // 문신
            pierInfo: vm.pierInfo === '999' ? '' : vm.pierInfo, // 피어싱
            tatooEtc: vm.tatooEtc, // [선택] 문신, 피어싱 상세
            beardInfo: vm.beardInfo === '999' ? '' : vm.beardInfo, // 수염정보
            dimpleInfo: vm.dimpleInfo === '999' ? '' : vm.dimpleInfo, // 보조개
            doubleEyeInfo: vm.doubleEyeInfo === '999' ? '' : vm.doubleEyeInfo, // 쌍커풀
            bottomsSize: vm.bottomsSize === '999' ? '' : vm.bottomsSize, // 발사이즈
            topSize: vm.topSize === '999' ? '' : vm.topSize, // 발사이즈
            footSize: vm.footSize === '999' ? '' : vm.footSize, // 발사이즈
            driveBicycleYn: vm.driveBicycleYn, // 자전거 운전
            driveCarYn: vm.driveCarYn, // 자동차 운전
            driveMotercycleYn: vm.driveMotercycleYn, // 오토바이 운전
            smokeYn: vm.smokeYn, // 흡연여부
            dressBaseCount: vm.dressBaseCount, // 기본정장 벌수
            dressBaseColor: vm.dressBaseColor, // 기본정장 색상
            dressSemiCount: vm.dressSemiCount, // 세미정장 벌수
            dressSemiColor: vm.dressSemiColor, // 세미정장 색상
            dressBlackShoes: vm.dressBlackShoes, // 검정구두
            dressSwimsuit: vm.dressSwimsuit, // 검정구두
            dressSchoolUniform: vm.dressSchoolUniform, // 검정구두
            dressMilitaryBoots: vm.dressMilitaryBoots // 검정구두
        };
        vm.memberDetailInfo = memberDetailInfo;

        logger.log('memberBaseInfo =', memberBaseInfo);
        logger.log('memberCareerInfo =', memberCareerInfo);
        logger.log('memberDetailInfo =', memberDetailInfo);

        var param = {};
        param.memberBaseInfo = vm.memberBaseInfo;					// 회원 기본 정보
        param.memberDetailInfo =  vm.memberDetailInfo;			// 회원 상세 정보
        param.supportAndHopeInfo = supportAndHopeInfoData;							// 희망배역정보

        // 경력사항 변경 여부 체크
        if(vm.isMemberCareerInfoChange){
            param.memberCareerInfo = vm.memberCareerInfo;
        }

        GuestSvc.updateMyInfo(param)
            .then(function(data){
                logger.log('updateMyInfo success', data);
                // 정상적으로 등록되면 촬영 불가일세팅
                $message.alert('정상적으로 수정되었습니다.');
                $rootScope.goView('pesn.mainp.pmMain');
            })
            .catch(function(err){
                logger.log('updateMyInfo err', err);
                $message.alert('정보수정 중 에러가 발생 되었습니다.');
            });
    }
}
