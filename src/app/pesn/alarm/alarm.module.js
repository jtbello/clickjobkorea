
angular
.module('pesn.alarm', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('pesn.alarm', {
        url: '/alarm',
        abstract: false,
        template: '<div ui-view><h1>pesn.alarm</h1></div>',
        controller: 'alarmCtrl as alarm'
    })

    .state('pesn.alarm.paList', {
        url: '/paList',
        templateUrl: 'alarm/paList/paList.tpl.html',
        data: { menuTitle : '알람 목록', menuId : 'paList' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'paListCtrl as paList'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
