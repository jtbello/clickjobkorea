
angular
.module('pesn.alarm')
.controller('paListCtrl', paListCtrl);

function paListCtrl($log, $scope, $message, $q, $window, $user, $cordovaPushV5C) {
    /**
     * Private variables
     */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        // alarmList : [],					// 회원정보
        // alarmListCount : 0,				// 회원전체카운트
        mod : {
            list : []
        }
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        loadAlarmList : loadAlarmList,
        dev : {
            add : function(sec){
                function genId () {
                    var text = '';
                    var possible = '0123456789';
                    
                    for (var i = 0; i < 13; i++){
                        text += possible.charAt(Math.floor(Math.random() * possible.length));
                    }
                    return text;
                }

                let id = parseInt('10' + genId());

                var now = new Date().getTime();

                var _10SecondsFromNow = new Date(now + sec * 1000);
                var _10wSecondsFromNow = new Date(now + (sec+10) * 1000);

                // var paramDay = {
                //     id: id,
                //     title: 'Title here',
                //     text: 'Hello , ' + sec + 'sec ago',
                //     trigger: {
                //         every: 'minute',
                //         firstAt: _10SecondsFromNow,
                //         count: 10
                //     },
                //     actions: [
                //         { id: 'confirm', title: '알람끄기' }
                //     ],
                //     foreground: true,
                //     launch : false
                // };
                // todo 로컬노티 변경
                // $cordovaLocalNotification.schedule([
                //     paramDay
                //     // ,{
                //     //     id: id+1,
                //     //     title: 'Title here',
                //     //     text: 'Hellossssss , ' + sec + 'sec agosss',
                //     //     trigger: {
                //     //         at: _10wSecondsFromNow
                //     //     },
                //     //     actions: [
                //     //         { id: 'confirm', title: '알람끄기' }
                //     //     ],
                //     //     foreground: true
                //     // }
                //     ]).then(function (result) {
                //     $message.alert(result + ' 설정 완료');
                //     loadAlarmList();
                // });
                var param = {
                    id : id,
                    type : 'onetime',
                    date : _10wSecondsFromNow.getTime(),
                    title : 'testTitle',
                    mesg : 'json Alarm'
                };

                $cordovaPushV5C.schedule({
                    alarms:[param]
                }).then(function (result) {
                    $log.debug("schedule success : " + JSON.stringify(result));
                    $message.alert(JSON.stringify(result) + ' 설정 완료');
                    loadAlarmList();
                });
                
            }
        },

        evt : {
            del : function(id){
                // $window.alert(id);
                if($window.confirm('해당 알람을 삭제하시겠습니까')){
                    // todo 로컬노티 변경
                    var param = {
                        id : id
                    };
                    $cordovaPushV5C.remove({
                        alarms:[param]
                    })
                     .then(loadAlarmList);
                }
            }
        }
    });

    /**
     * Initialize
     */
     init();

    function init() {
      logger.debug('init', vm);

      $scope.$on('$destroy', destroy);

        loadAlarmList();
    }

    /**
     * Event handlers
     */
    function destroy(event) {
      logger.debug(event);
    }
    
    /**
     * Custom functions
     */
    function loadAlarmList(){

        $cordovaPushV5C.getList()
        .then(function (result) {
            // $message.alert(result + ' 설정 완료');
            vm.mod.list = [];
            _.forEach(result, function(data){
                var id = data.id.toString().substring(0,2);
                if('10' === id || '20' === id) {
                    vm.mod.list.push(data);
                }
            });
        });
    }
}
