
angular
  .module('pesn.calc')
  .controller('pcCalcListCtrl', pcCalcListCtrl);

function pcCalcListCtrl($log, $scope, $rootScope, $stateParams, CompanySvc, User) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    startDate : moment().add(-1, 'month').format('YYYY[-]MM[-]DD'),					// 촬영 시작일 [일시 패턴 : 0000-00-00 00:00:00]
		endDate : moment().format('YYYY[-]MM[-]DD'),						// 촬영 종료일 [일시 패턴 : 0000-00-00 00:00:00]
		  
    calculateStateCode: '999', // 정산상태코드

    searchData: '', // 검색어
    showShowMoreButton: true, // 더보기 버튼 표시

    calculatePgmList: [], // 프로그램 리스트
    calculatePgmListCount: 0,

    // 페이징 관련
    curPage: 1, // 현재페이지
    viewPageCnt: 10, // 페이지당보여질수
    pagingData: [], // 페이지번호 모듬
    totPageCnt: 0 // 전체페이지수
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    search: search,
    searchMore: searchMore,
    onListClick: onListClick
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    
    if ($stateParams.backupData) {
      _.assign(vm, $stateParams.backupData);
    }
    search();  
    
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  function onListClick(data) {
    $rootScope.goView('comp.paymt.cpPaymtInfo', vm, { rcutBaseSeq: data.RCUT_BASE_SEQ })
  }

  function searchMore() {
    vm.curPage++;
    search(false);
  }

  function search(shouldResetList = true) {
    if (shouldResetList) {
      vm.calculatePgmList = [];
      vm.calculatePgmListCount = 0;
      vm.pagingData = [];
      vm.curPage = 1;
      vm.showShowMoreButton = true;
    }

    var param = getParams();

    CompanySvc.selectCalculatePgmList(param)
      .then(function(data) {
        // 리스트 업데이트
        vm.calculatePgmList = vm.calculatePgmList.concat(data.user.resultData.calculatePgmList);
        vm.calculatePgmListCount = data.user.resultData.calculatePgmListCount;

        // 리스트에 추가할 항목이 없을 경우
        if (data.user.resultData.calculatePgmList.length === 0 && vm.curPage > 1) {
          vm.curPage--;

          // 기존 리스트에 항목이 추가됐을 경우
          // or 리스트에 처음 항목이 추가됐을 경우
        }

        // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
        if (data.user.resultData.calculatePgmList.length < vm.viewPageCnt) {
          vm.showShowMoreButton = false;
        }

        // 전체 페이지 정보입력
        vm.totPageCnt = Math.floor(
          vm.calculatePgmListCount / vm.viewPageCnt +
            (vm.calculatePgmListCount % vm.viewPageCnt == 0 ? 0 : 1)
        );
        vm.pagingData = [];
        for (var i = 1; i <= vm.totPageCnt; i++) {
          vm.pagingData.push({ pageNo: i });
        }

        logger.log('search[SUCCESS]', 'vm.calculatePgmList: ', vm.calculatePgmList);
      })
      .catch(function(error) {
        logger.log('search[FAILED]', 'error: ', error);
      });
  }

  function getParams() {
    var param = {};
	  param.startNo = (vm.curPage -1) * vm.viewPageCnt;				//[필수] (현재페이지 -1) * 페이지당보여질수
	  param.endNo = vm.viewPageCnt;									//[필수] 페이징 페이지당보여질수

    param.memberSeq = User.getUserInfoList()[0].MEMBER_SEQ+''; // [선택] 프로그램
    param.calculateStateCode = vm.calculateStateCode == '999' ? '' :	vm.calculateStateCode;		// 상태
	  param.startDate = _.isEmpty(vm.startDate) ? '' : moment(vm.startDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');				//[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
	  param.endDate = _.isEmpty(vm.endDate) ? '' : moment(vm.endDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');					//[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
    param.searchData = vm.searchData;
	  param.searchKind = '';

    return param;
  }

}
