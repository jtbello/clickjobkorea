
angular
  .module('pesn.calc', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('pesn.calc', {
      url: '/calc',
      abstract: false,
      template: '<div ui-view><h1>pesn.calc</h1></div>',
      controller: 'calcCtrl as calc'
    })

    .state('pesn.calc.pcCalcList', {
      url: '/pcCalcList',
      templateUrl: 'calc/pcCalcList/pcCalcList.tpl.html',
      data: { menuTitle : '정산 관리', menuId : 'pcCalcList', depth : 1},
        params: { param: null, backupData: null },
      controller: 'pcCalcListCtrl as pcCalcList'
    })
    ;

  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
