
angular
.module('pesn.mainp')
.controller('pmMainSchePCtrl', pmMainSchePCtrl);

function pmMainSchePCtrl($log, $rootScope , $storage, $scope, $modal, $modalInstance, $message, item, GuestSvc,ComSvc, ngspublic, comConstant , $window , $q) {
  /**
  * Private variables
  */
  var logger = $log(this);
  var vm = this;
  let _fn = {
    
  };

  /**
   * Public variables
   */
  _.assign(vm, {
    mod : {
      
    }
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    dismiss : dismiss,
    confirm : confirm,
    evt : { 
        go : function(){
            $modalInstance.dismiss();
            $rootScope.goView('pesn.schedule.psMng');
        },
        hide : {
            oneDay : function(){
                let _target = moment().add(1 , 'day');
                $storage.set('PESN_MAIN_SCHE_POPUP_INVISIBLE_DATE' , _target.valueOf() + '');
                $modalInstance.close(_target.valueOf() + '');
            },
            thirtyDays : function(){
                let _target = moment().add(30 , 'day');
                $storage.set('PESN_MAIN_SCHE_POPUP_INVISIBLE_DATE' , _target.valueOf() + '');
                $modalInstance.close(_target.valueOf()+ '');
            }
        }
    }
  });

  /**
  * Initialize
  */
  init();

  function init() {
      logger.debug('init', vm);
      
      $scope.$on('$destroy', destroy);
  }

  /**
  * Event handlers
  */
  function destroy(event) {
    logger.debug(event);
  }

  /**
  * Custom functions
  */

  /**
   * 모달 닫기
   */
  function dismiss() {
    $modalInstance.dismiss(vm);
  }


  /**
   * 정상 종료
   */
  function confirm(){
    $modalInstance.close(vm);
  }


}
