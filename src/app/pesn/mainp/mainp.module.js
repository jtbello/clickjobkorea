
angular
.module('pesn.mainp', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('pesn.mainp', {
        url: '/mainp',
        abstract: false,
        template: '<div ui-view><h1>pesn.mainp</h1></div>',
        controller: 'mainpCtrl as mainpCtrl'
    })
    
    .state('pesn.mainp.pmMain', {
        url: '/pmMain',
        templateUrl: 'mainp/pmMain/pmMain.tpl.html',
        data: { menuTitle : '출연자 메인', menuId : 'pmMain' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'pmMainCtrl as pmMain'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
