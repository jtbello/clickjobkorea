
angular
  .module('pesn.mainp')
  .controller('pmMainCtrl', pmMainCtrl)
  .filter('imgPath' , function(){
    return function(input){
      //return $env.endPoint.service + input;
      return input;
    };
  })
  .directive('onLastRepeat', function () {
    return function (scope, element, attrs) {
        if (scope.$last) setTimeout(function () {
            scope.$emit('onRepeatLast', element, attrs);
        }, 1);
    };
  })

function pmMainCtrl($env,$log,$modal, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, 
                    $window, $q, $storage, $advHttp, $cordovaInAppBrowser, $cordovaToast, $doryPicker, 
                    $cordovaGeolocation, $cordovaNetwork, GuestSvc , ComSvc , User, MemberSvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  var _fn = {
    err : function(d){
      $window.alert('error!!');
      logger.log('ERR!' , '' , d);
      return this;
    },
    popup : {
      states : {
        gps : -1,
//        privacy : -1,
        push : -1,
        sche : -1,
      },
      gps : {
        chk : function(){
          var param = {};
          return MemberSvc.selectAgreeInfo(param)
          .then(function(data){
            if(data.user.resultData.agreeInfo.length > 0){
              var agreeInfo = _.find(data.user.resultData.agreeInfo, {AGREE_KIND : 'A2'});
              if(agreeInfo){
                if(agreeInfo.AGREE_YN == 'Y'){
                  _fn.popup.states.gps = 'Y';
                }else{
                  _fn.popup.states.gps = 'N';
                }
              }else{
                _fn.popup.states.gps = 'N';
              }
            }else{
              _fn.popup.states.gps = 'N';
            }
          })
          .catch(function(err){
            _fn.popup.states.gps = 'N';
          })
          
        },
        show : function(){
          let modalInstance = $window.openPop({
            tplUrl : 'popup/iaGeoP/iaGeoP.tpl.html',
            ctrl : 'iaGeoPCtrl',
            ctrlAs : 'iaGeoP',
            data : {
              busnLinkKey : User.getUserInfoList()[0].MEMBER_SEQ
            }
          });
  
          modalInstance.opened.then(function(){
            // 오픈 성공 
          });
  
          modalInstance.result.then(function(data){
            _fn.popup.gps.chk()
            .then(_fn.popup.init);
            logger.log('privacy pop close' , data);
          })
          .catch(function(reason){
            _fn.popup.gps.chk()
            .then(_fn.popup.init);
            logger.log('privacy pop Dissmiss' , reason);
          });

          return modalInstance;
        }
      },
      push : {
        chk : function(){
          
          var param = {};
          return MemberSvc.selectAgreeInfo(param)
          .then(function(data){
            if(data.user.resultData.agreeInfo.length > 0){
              var agreeInfo = _.find(data.user.resultData.agreeInfo, {AGREE_KIND : 'A3'});
              if(agreeInfo){
                if(agreeInfo.AGREE_YN == 'Y'){
                  _fn.popup.states.push = 'Y';
                }else{
                  _fn.popup.states.push = 'N';
                }
              }else{
                _fn.popup.states.push = 'N';
              }
            }else{
              _fn.popup.states.push = 'N';
            }
          })
          .catch(function(err){
            _fn.popup.states.push = 'N';
          })
          
        },
        show : function(){
          let modalInstance = $window.openPop({
            tplUrl : 'popup/iaPushP/iaPushP.tpl.html',
            ctrl : 'iaPushPCtrl',
            ctrlAs : 'iaPushP',
            data : {
              busnLinkKey : User.getUserInfoList()[0].MEMBER_SEQ
            }
          });
  
          modalInstance.opened.then(function(){
            // 오픈 성공 
          });
  
          modalInstance.result.then(function(data){
            _fn.popup.push.chk()
            .then(_fn.popup.init);
            logger.log('privacy pop close' , data);
          })
          .catch(function(reason){
            _fn.popup.push.chk()
            .then(_fn.popup.init);
            logger.log('privacy pop Dissmiss' , reason);
          });

          return modalInstance;
        }
      },
//      privacy : {
//        chk : function(){
//          return $storage.get('IA_PRIVACY_AGREE_KIND')
//          .then(function(d){
//            let deferred = $q.defer();
//            if(d === null || d === 'N'){
//              _fn.popup.states.privacy = d;
//              deferred.resolve(true);
//            }
//            else{
//              _fn.popup.states.privacy = d;
//              deferred.resolve(false);
//            }
//
//            return deferred.promise;
//          });
//        },
//        show : function(){
//          let modalInstance = $window.openPop({
//            tplUrl : 'popup/iaPrivacyP/iaPrivacyP.tpl.html',
//            ctrl : 'iaPrivacyPCtrl',
//            ctrlAs : 'iaPrivacyP',
//            data : {
//              busnLinkKey : User.getUserInfoList()[0].MEMBER_SEQ
//            }
//          });
//  
//          modalInstance.opened.then(function(){
//            // 오픈 성공 
//          });
//  
//          modalInstance.result.then(function(data){
//            _fn.popup.privacy.chk()
//            .then(_fn.popup.init);
//            logger.log('privacy pop close' , data);
//          })
//          .catch(function(reason){
//            _fn.popup.privacy.chk()
//            .then(_fn.popup.init);
//            logger.log('privacy pop Dissmiss' , reason);
//          });
//
//          return modalInstance;
//        }
//      },
      sche : {
        chk : function(){
          return $storage.get('PESN_MAIN_SCHE_POPUP_INVISIBLE_DATE')
          .then(function(d){
            let deferred = $q.defer();
            if(d === null){
              _fn.popup.states.sche = 'N';
              deferred.resolve(false);
            }
            else{
              let m = moment(Number(d));
              let today = moment();
              if(moment.isMoment() && today.diff(m) < 0){
                _fn.popup.states.sche = 'N';
              }
              else{
                _fn.popup.states.sche = 'Y';
              }
              deferred.resolve(true);
            }

            return deferred.promise;
          });
        },
        show : function(){
          let modalInstance = $modal.open({
            templateUrl: 'mainp/pmMainScheP/pmMainScheP.tpl.html',
            controller: 'pmMainSchePCtrl',
            controllerAs: 'pmMainScheP',
            resolve: {
                item: function() {
                    return [];
                }
            }
          });
  
          modalInstance.opened.then(function(){
            // 오픈 성공 
          });
  
          modalInstance.result.then(function(data){
            logger.log('pnInsertPopup close' , data);
            // _fn.popup.init();
          })
          .catch(function(reason){
            logger.log('pnInsertPopup Dissmiss' , reason);
            // _fn.popup.init();
          });

          return modalInstance;
        },
        selModRcut : function(){
          var param = {};
          return GuestSvc.selectModifyRcutPushInfo(param)
              .then(function(data){
                logger.log('selectModifyRcutPushInfo' , data);
                if(data.user.resultData.selectModifyRcutPushInfo.length > 0){
                  _.forEach(data.user.resultData.selectModifyRcutPushInfo, function(obj){
                    $message.alert('['+obj.PGM_REUT_FIELD_NM+']['+obj.PGM_HOPE_CAST+']'+obj.PGM_NAME+(obj.TITLE_GUBUN != '' ? '('+obj.TITLE_GUBUN+')':'')+' ' + obj.PGM_POINT + ' ' + moment(obj.PGM_START_DTM).format('YYYY-MM-DD') +' 모집정보가 변경되었습니다. 반드시 체크 바랍니다.\n' +
                        '해당 메시지는 해당 모집 상세 화면에서 스케줄 변경 체크 확인 버튼을 클릭 하실때까지 표출이 됩니다.');
                  });
                }
              })
              .catch(function(err){

              })
        }
      },
      init : function(){
        _fn.popup.get()
        .then(_fn.popup.show);
      },
      get : function(){
        return _fn.popup.gps.chk()
        .then(_fn.popup.push.chk)
//        .then(_fn.popup.privacy.chk)
        .then(_fn.popup.sche.chk);
      },
      show : function(){
        if(_fn.popup.states.gps === -1 || _fn.popup.states.gps ==='N'){
          _fn.popup.gps.show();
        }
        else if(_fn.popup.states.push === -1 || _fn.popup.states.push === 'N'){
          _fn.popup.push.show();
        }
//        else if(_fn.popup.states.privacy === -1 || _fn.popup.states.privacy === 'N'){
//          _fn.popup.privacy.show();
//        }
        else if(_fn.popup.states.sche === -1 || _fn.popup.states.sche === 'N'){
          _fn.popup.sche.show();
        }else{
          _fn.popup.sche.selModRcut();
        }
      }
    },
    urg : {
      get : function(){
        // 더보기 조회 없음 
        let params = {};
        return GuestSvc.selectEmerRcutList(params)
        .then(function(d){
          let deferred = $q.defer();

          if(d.user.result === 'SUCCESS'){
            vm.mod.urg.list = d.user.resultData.emerRcutData;
            deferred.resolve(d.user);
          } 
          else{
            deferred.reject(d.user);
          }

          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();

          deferred.reject(d.user);

          return deferred.promise;
        });
      }
    },
    ad : {
      // 내게 맞춤
      get : function(){
        let params = {};
        params.orderType = vm.mod.opts.ordType;
        params.startNo = vm.mod.ad.startNo;
        params.endNo = vm.mod.ad.endNo;

        return GuestSvc.selectAllRcutList(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            logger.log('selectAllRcutList [SUCCESS]' , '' , d.user);
            if(params.startNo <= 0){
              vm.mod.ad.list = d.user.resultData.allRcutData;
              vm.mod.ad.listMaxCount = d.user.resultData.allRcutDataCount;
            }
            else{
              d.user.resultData.allRcutData.map(item =>{
                vm.mod.ad.list.push(item);
              });
            }
            deferred.resolve(d.user);
          }
          else{
            logger.log('selectAllRcutList [FAILED]' , '' , d.user);
            deferred.reject(d.user);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();

          deferred.reject(d.user);
          logger.log('selectAllRcutList [FAILED]' , '' , d.user);
          return deferred.promise;
        });
      },
      more : function(){
        vm.mod.ad.startNo = ((vm.mod.ad.startNo / vm.mod.ad.endNo) + 1) * vm.mod.ad.endNo;
        return _fn.ad.get()
        .then(function(user){
          if(user.resultData.allRcutData.length <= 0){
            $window.alert('더 이상 조회할 항목이 없습니다.');
          }
        } , _fn.err);
      }
    },
    co : {
      // 내게 맞춤
      get : function(){
        let params = {};
        params.memberSeq = User.getUserInfoList()[0].MEMBER_SEQ;
        params.orderType = vm.mod.opts.ordType;
        params.startNo = vm.mod.co.startNo;
        params.endNo = vm.mod.co.endNo;

        return GuestSvc.selectMyRcutList(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            logger.log('selectMyRcutList [SUCCESS]' , '' , d.user);
            if(params.startNo <= 0){
              vm.mod.co.list = d.user.resultData.rcutData;
              vm.mod.co.listMaxCount = d.user.resultData.rcutDataCount;

              console.log(vm , d);
            }
            else{
              d.user.resultData.rcutData.map(item =>{
                vm.mod.co.list.push(item);
              });
            }
            deferred.resolve(d.user);
          }
          else{
            logger.log('selectMyRcutList [FAILED]' , '' , d.user);
            deferred.reject(d.user);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();

          deferred.reject(d.user);
          logger.log('selectMyRcutList [FAILED]' , '' , d.user);
          return deferred.promise;
        });
      },
      more : function(){
        vm.mod.co.startNo = ((vm.mod.co.startNo / vm.mod.co.endNo) + 1) * vm.mod.co.endNo;
        return _fn.co.get()
        .then(function(user){
          if(user.resultData.rcutData.length <= 0){
            $window.alert('더 이상 조회할 항목이 없습니다.');
          }
        } , _fn.err);
      },
      accept : function(ris , rbs){
        
        //모달 오픈
        var modalInstance = $modal.open({
            templateUrl: 'program/ppLaborContractP/ppLaborContractP.tpl.html',
            controller: 'ppLaborContractPCtrl',
            controllerAs: 'ppLaborContractP',
            scope: $scope,
            resolve: {
                item: function() {
                    return {};
                }
            }
        });

        modalInstance.opened
            .then(function() {
                // 모달 오픈 성공
            })
            .catch(function(data) {
                alert(data);
                // 모달 오픈 실패
            });

        modalInstance.result
            .then(function(result) {
                // 정상 종료
                logger.log('ppLaborContractP Close', result);
                
                let params = {};
                params.memberSeq = User.getUserInfoList()[0].MEMBER_SEQ + '';
                params.requestInfoSeq = ris + '';
                params.rcutBaseSeq = rbs + '';
                return GuestSvc.updateRequestCompanyInfo(params)
                .then(function(d){
                  logger.log('updateRequestCompanyInfo [SUCCESS]' ,' resultData : ' , d.user);
                  return _fn.co.get();
                } , function(d){
                  logger.log('updateRequestCompanyInfo [FAILED]' ,' resultData : ' , d.user);
                  return _fn.co.get();
                });

            })
            .catch(function(reason) {
                // 취소
                logger.log('ppLaborContractP Dismiss', reason);
            });
        
      },
      refuse : function(ris , rbs){
        let params = {};
        params.memberSeq = User.getUserInfoList()[0].MEMBER_SEQ + '';
        params.requestInfoSeq = ris + '';
        params.rcutBaseSeq = rbs + '';
        return GuestSvc.updateRequestCompanyCancel(params)
        .then(function(d){
          logger.log('updateRequestCompanyCancel [SUCCESS]' ,' resultData : ' , d.user);
          return _fn.co.get();
        } , function(d){
          logger.log('updateRequestCompanyCancel [FAILED]' ,' resultData : ' , d.user);
          return _fn.co.get();
        });
      }
    }
  };
  
  /**
   * Public variables
   */
  _.assign(vm, {
    const : {
      endPoint : $env.endPoint.service
    },
    IDENTIFIERS : {
      ITM_TYPES : {
        URGENT : 'URGENT',
        ADVERTISE : 'ADVERTISE',
        CUSTOM_ORIENTED : 'CUSTOM_ORIENTED'
      }
    },  
    mod : {
      opts : {
        // 페이지가 로드될때마다 난수를 생성하도록함 
        ordType : Math.floor(Math.random() * 4) + 1 
      },
      // 긴급 
      urg : {
        list : []
      },
      // 추천
      ad : {
        list : [],
        startNo : 0,
        endNo : 5,
        listMaxCount : 0,
        display : true,
      },
      // 내게 맞춤 customer oriented
      co : {
        list : [],
        startNo : 0,
        endNo : 5,
        listMaxCount : 0,
      }
    }
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    goAlrmList : function(){
      // 개발용 함수
      $rootScope.goView('pesn.alarm.paList');
    },
    evt : {
      urg : {
        go : function(rbs , ris){
          // rbs : RCUT_BASE_SEQ
          $rootScope.goView('pesn.program.ppDetail' , vm , {
            RCUT_BASE_SEQ : rbs,
            REQUST_INFO_SEQ : ris,
            VIEW_TYPES_IDENTIFIERS : vm.IDENTIFIERS.ITM_TYPES,
            VIEW_TYPES : vm.IDENTIFIERS.ITM_TYPES.URGENT,
            CATE : 'PROGRAM',
            REQ_BTN_VIEW : true
          });
        },
        goWrapper : function(elem){
          let jElem = $(elem);
          vm.evt.urg.go(jElem.attr('data-rcut-base-seq'));
        }
      },  
      ad : { 
        loadMore : function(){
          _fn.ad.more();
        },
        go : function(rbs,ris){
          // rbs : RCUT_BASE_SEQ
          $rootScope.goView('pesn.program.ppDetail' , vm , {
            RCUT_BASE_SEQ : rbs,
            REQUST_INFO_SEQ : ris,
            VIEW_TYPES_IDENTIFIERS : vm.IDENTIFIERS.ITM_TYPES,
            VIEW_TYPES : vm.IDENTIFIERS.ITM_TYPES.ADVERTISE,
            CATE : 'PROGRAM',
            REQ_BTN_VIEW : false
          });
        },
        setDisplay : function(){
          vm.mod.ad.display = !vm.mod.ad.display;
        }
      },
      co : {
        loadMore : function(){
          _fn.co.more();
        },
        go : function(rbs, ris){
          // rbs : RCUT_BASE_SEQ
          $rootScope.goView('pesn.program.ppDetail' , vm , {
            RCUT_BASE_SEQ : rbs,
            REQUST_INFO_SEQ : ris,
            VIEW_TYPES_IDENTIFIERS : vm.IDENTIFIERS.ITM_TYPES,
            VIEW_TYPES : vm.IDENTIFIERS.ITM_TYPES.CUSTOM_ORIENTED,
            CATE : 'PROGRAM',
            REQ_BTN_VIEW : false
          });
        },
        accept : function(item){
          _fn.co.accept(item.REQUST_INFO_SEQ , item.RCUT_BASE_SEQ);
        },
        cancel : function(item){
          _fn.co.refuse(item.REQUST_INFO_SEQ , item.RCUT_BASE_SEQ);
        }
      },
      goMyPage : function(){
        $rootScope.goView('pesn.pesnMyPage');
      }
    }
  });
  

  var _swiper = {
    banner :{
      init : function(){
        let opt = {};
        opt.direction = 'vertical';
        opt.preventClicks = false;
        opt.preventClicksPropagation = false;
        opt.autoplayDisableOnInteraction =  false;

        if(vm.bannerList.length <= 1){ 
          opt.loop = false; 
        }
        else { 
          opt.loop = true; 
          opt.autoplay = 4000;
        }

        vm.SWIPER = vm.SWIPER || {};
        vm.SWIPER.banner = new Swiper('.banner-swiper-container' , opt);
        window.bannerClick = vm.bannerClick;
      }
    },
    urg : {
      init : function(){
        let opt = {};
        opt.direction = 'vertical';
        opt.preventClicks = false;
        opt.preventClicksPropagation = false;
        opt.autoplayDisableOnInteraction =  false;
        
        if(vm.mod.urg.list.length <= 1){ opt.loop = false; }
        else { 
          opt.loop = true; 
          opt.autoplay = 4000;
        }

        vm.SWIPER = vm.SWIPER || {};
        vm.SWIPER.urg = new Swiper('.urg-swiper-container' , opt);
        window.urgClick = vm.evt.urg.goWrapper;
      }
    }
  };


  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    vm.mod.ad.startNo = 0;
    vm.mod.ad.endNo = 5;
    vm.mod.co.startNo = 0;
    vm.mod.co.endNo = 5;

    _fn.urg.get()
    .then(_fn.ad.get)
    .then(_fn.co.get)
    .then(function(){ 
      logger.log('init' , '' , vm.mod); 

      _fn.popup.init();

      console.log('INITIALIZED', vm.mod);
    });
    
    
    // 배너관련
    var param= {};
    param.startNo = 0;                            
    param.endNo = 90; 
    ComSvc.selectBannerList(param)
    .then(function(data){
      vm.bannerList = data.user.resultData.bannerList;
      vm.bannerListCount = data.user.resultData.bannerListCount;
    });
      
    vm.bannerClick = function(elem){
      let jElem = $(elem);
      let bannerLink = jElem.attr('data-banner-link-info');
      let bannerInfoSeq = jElem.attr('info-seq');
      if(_.isBlank(bannerLink)){
        return false;
      }

      var param= {};
      param.bannerClickCount  = 'Y';
      param.bannerInfoSeq = bannerInfoSeq;
      param.companyMemberSeq = '99999999';
      ComSvc.updateBannerCntUp(param);
      var options = {
          location: 'yes',
          scrollbars: 'yes',
          resizable: 'yes'
      };
      $cordovaInAppBrowser.open(bannerLink, '_blank', options);
      
    };
    $scope.$on('$destroy', destroy);
  }


  $scope.$on('onRepeatLast', function (scope, element, attrs) {
    if(attrs.onLastRepeat === 'banner'){
      _swiper.banner.init();
    }
    else if(attrs.onLastRepeat === 'urg'){
      _swiper.urg.init();
    }
  });
  
  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  
}
