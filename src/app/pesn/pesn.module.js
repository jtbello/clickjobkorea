
angular
  .module('pesn', [
    'pesn.alarm',
    'pesn.calc',
    'pesn.coupon',
    'pesn.inquiry',
    'pesn.infomod',
    'pesn.mainp',
    'pesn.mypage',
    'pesn.notice',
    'pesn.notify',
    'pesn.program',
    'pesn.resume',
    'pesn.schedule',
    'pesn.standby'
  ])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('pesn', {
      url: '?',
      abstract: false,
      template: '<div ui-view><h1>guest.pesn</h1></div>',
      controller: 'pesnCtrl as pesn',
      resolve: {
        Env: function($env) {
          return $env.get();
        },
        User : function($user){
          return $user.get();
        }
      }
    })
    ;

  //$urlRouterProvider.when('/pesn', '/pesn/pmMain');
}
