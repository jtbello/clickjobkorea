
angular
  .module('comp.qna')
  .controller('cqQnaInfoCtrl', cqQnaInfoCtrl);

function cqQnaInfoCtrl($log, $scope, $stateParams, $message, $rootScope, CompanySvc, ngspublic) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      qnaDetailInfo : {},               // 질문 상세 내용
      qnaQueSeq : '',                   // [필수] Qna질문 일련번호
      qnaAnswerSeq : '',                // [선택] Qna질문 답변 일련번호
      detailCntn : ''                   // [필수] Qna 답변내용
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      selectQnaDetailInfo : selectQnaDetailInfo,
      insertQnaAnswer : insertQnaAnswer,
      goQnaList : goQnaList
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    if($stateParams.param){
      vm.qnaQueSeq = $stateParams.param.qnaQueSeq;

        selectQnaDetailInfo();
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  function selectQnaDetailInfo(){
      var param = {};
      param.qnaQueSeq = vm.qnaQueSeq + '';									    //[필수] Qna질문 일련번호
      CompanySvc.selectQnaDetailInfo(param)
          .then(function(data){
              logger.log('success', data);
              if(data.user.resultData.qnaDetailInfo.length > 0){
                  vm.qnaDetailInfo = data.user.resultData.qnaDetailInfo[0];
                  vm.detailCntn = vm.qnaDetailInfo.DETAIL_CNTN2;
                  if(!_.isBlank(vm.qnaDetailInfo.QNA_ANSWER_SEQ)){
                    vm.qnaAnswerSeq = vm.qnaDetailInfo.QNA_ANSWER_SEQ;
                  }
              }else{
                  vm.qnaDetailInfo = [];
              }
          })
          .catch(function(error){
              logger.log('fail');
              logger.error('message: ' + error + '\n');
          });
  }

  function insertQnaAnswer() {

      if(!$message.confirm('답변을 저장하시겠습니까?')) return false;

      var chk = true;
      chk = chk && ngspublic.nullCheck(vm.detailCntn , '답변을 입력하세요.');
      if(!chk) return false;

      var param = {};
      param.qnaQueSeq = vm.qnaQueSeq + '';									    //[필수] Qna질문 일련번호
      param.detailCntn = vm.detailCntn;									    //[필수] Qna 답변내용
      param.qnaAnswerSeq = vm.qnaAnswerSeq + '';                            //[선택] Qna질문 답변 일련번호
      
      CompanySvc.insertQnaAnswer(param)
          .then(function(data){
              logger.log('success', data);
              $message.alert('답변이 저장되었습니다.');
              goQnaList();
          })
          .catch(function(error){
              logger.log('fail');
              logger.error('message: ' + error + '\n');
          });
  }

  function goQnaList() {
      $rootScope.goView('comp.qna.cqQnaList');
  }
}
