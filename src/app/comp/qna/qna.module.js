
angular
  .module('comp.qna', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('comp.qna', {
      url: '/qna',
      abstract: false,
      template: '<div ui-view><h1>comp.qna</h1></div>',
      controller: 'qnaCtrl as qna'
    })

    .state('comp.qna.cqQnaList', {
      url: '/cqQnaList',
      templateUrl: 'qna/cqQnaList/cqQnaList.tpl.html',
      data: { menuTitle : '회원가입 요청승인', menuId : 'cqQnaList', depth : 1},
        params: { param: null, backupData: null },
      controller: 'cqQnaListCtrl as cqQnaList'
    })
      .state('comp.qna.cqQnaInfo', {
          url: '/cqQnaInfo',
          templateUrl: 'qna/cqQnaInfo/cqQnaInfo.tpl.html',
          data: { menuTitle : '출연자 정보', menuId : 'cqQnaInfo', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cqQnaInfoCtrl as cqQnaInfo'
      })
    ;

  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
