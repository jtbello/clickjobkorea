
angular
  .module('comp.qna')
  .controller('cqQnaListCtrl', cqQnaListCtrl);

function cqQnaListCtrl($log, $scope, $modal, MetaSvc, CompanySvc, $rootScope, $stateParams) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      stateKind : '999',                    // 답변 상태

      qnaList : [],                     // 질문 리스트
      qnaListCount : 0,                 // 질문 카운트

      startDate : '',	                    //[선택] 작성일시 시작 [일시 패턴 : 0000-00-00 00:00:00]
      endDate : '',	                        //[선택] 작성일시 종료 [일시 패턴 : 0000-00-00 00:00:00]
      searchData : '',						//[선택] 검색어

      // 페이징 관련
      curPage : 1,						// 현재페이지
      viewPageCnt : 10,					// 페이지당보여질수
      pagingData : [],					// 페이지번호 모듬
      totPageCnt : 0					// 전체페이지수
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      selectQnaList : selectQnaList,
      goDetail : goDetail
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    if ($stateParams.backupData) {
      _.assign(vm, $stateParams.backupData);
    }

    selectQnaList(false);
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  function selectQnaList(more){
      vm.curPage = !more ? 1 : vm.curPage+1;

      var param = {};
      param.startNo = (vm.curPage -1) * vm.viewPageCnt;								//[필수] (현재페이지 -1) * 페이지당보여질수
      param.endNo = vm.viewPageCnt;													//[필수] 페이징 페이지당보여질수
      param.stateKind = vm.stateKind == '999' ? '' : vm.stateKind;
      CompanySvc.selectQnaList(param)
          .then(function(data){
              logger.log('success', data);
              if(data.user.resultData.qnaList.length > 0){

                  if(!more){
                      vm.qnaList = data.user.resultData.qnaList;
                  }else{
                      vm.qnaList = vm.qnaList.concat(data.user.resultData.qnaList);
                  }
                  vm.qnaListCount = data.user.resultData.qnaListCount;

                  // 전체 페이지 정보입력
                  vm.totPageCnt = Math.floor((vm.qnaListCount / vm.viewPageCnt) + ((vm.qnaListCount % vm.viewPageCnt) === 0 ? 0 : 1));
                  vm.pagingData = [];
                  for (var i = 1; i <= vm.totPageCnt; i++) {
                      vm.pagingData.push({pageNo : i});
                  }

              }else{
                  if(!more){
                      vm.qnaList = [];
                      vm.qnaListCount = 0;
                      vm.pagingData = [];
                  }
              }
          })
          .catch(function(error){
              logger.log('fail');
              logger.error('message: ', error);
              vm.curPage = more ? vm.curPage-1 : vm.curPage;
          });
  }

  function goDetail(data){
      $rootScope.goView('comp.qna.cqQnaInfo', vm, {qnaQueSeq : data.QNA_QUE_SEQ});
  }
}
