angular.module('comp.mngpesn').controller('cmMngListCtrl', cmMngListCtrl);

function cmMngListCtrl(
  $log,
  $rootScope,
  $scope,
  $modal,
  $message,
  $stateParams,
  CompanySvc,
  $q
) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    // 공통 변수
    startAgeOptions: [], // 최소연령 옵션 리스트
    endAgeOptions: [], // 최대연령 옵션 리스트
    requestCountOptions: [], // 출연횟수 옵션 리스트
    activeTabNumber: 1, // 활성탭 번호

    // 관심출연자 탭
    interestedActorsTabVars: {
      isSelectedAll: false, // 전체 선택 여부
      showMoreOptions: false, // 검색 옵션 확장

      // select options
      hopeCastOptions: [], // 희망배역 옵션 리스트

      // 검색조건
      supportField: '999', // 희망분야
      hopeCast: '999', // 희망배역
      startAge: '999', // 최소연령
      endAge: '999', // 최대연령
      memberGender: '999', // 성별
      height: '999', // 키
      dressInfo: '999', // 보유의상1
      dressInfo2: '999', // 보유의상2
      weight: '999', // 몸무게
      requestCount: '999', // 출연횟수
      bodyInfo: '999', // 체형 (신체사항)
      classInfo: '999', // 보유등급
      hairState: '999', // 헤어상태
      hairHeight: '999', // 헤어길이
      tatooInfo: '999', // 문신여부
      pierInfo: '999', // 피어싱여부
      driveInfo: '999', // 운전여부
      startDate: '', // 촬영가능일시작
      endDate: '', // 촬영가능일종료
      searchKind: '999', // 검색종류
      searchData: '', // 검색어

      orderKind1: 'O1', // 정렬대상
      orderKind2: 'O1', // 정렬방법

      // 회원 표시 리스트
      memberList: [], // 회원정보
      memberListCount: 0, // 회원전체카운트

      // 페이징 관련
      curPage: 1, // 현재페이지
      viewPageCnt: 10, // 페이지당보여질수
      pagingData: [], // 페이지번호 모듬
      totPageCnt: 0 // 전체페이지수
    },

    // 출연수락 / 요청중 탭
    pendingActorsTabVars: {
      searchKind: '999', // 검색종류
      searchData: '', // 검색어

      // 회원 표시 리스트
      memberList: [], // 회원정보
      memberListCount: 0, // 회원전체카운트

      // 페이징 관련
      curPage: 1, // 현재페이지
      viewPageCnt: 10, // 페이지당보여질수
      pagingData: [], // 페이지번호 모듬
      totPageCnt: 0 // 전체페이지수
    },

    // 출연완료 탭
    completedActorsTabVars: {
      showMoreOptions: false, // 검색 옵션 확장

      // select options
      hopeCastOptions: [], // 희망배역 옵션 리스트

      // 검색조건
      supportField: '999', // 희망분야
      hopeCast: '999', // 희망배역
      startAge: '999', // 최소연령
      endAge: '999', // 최대연령
      memberGender: '999', // 성별
      height: '999', // 키
      dressInfo: '999', // 보유의상1
      dressInfo2: '999', // 보유의상2
      weight: '999', // 몸무게
      requestCount: '999', // 출연횟수
      bodyInfo: '999', // 체형 (신체사항)
      classInfo: '999', // 보유등급
      hairState: '999', // 헤어상태
      hairHeight: '999', // 헤어길이
      tatooInfo: '999', // 문신여부
      pierInfo: '999', // 피어싱여부
      driveInfo: '999', // 운전여부
      startDate: '', // 촬영가능일시작
      endDate: '', // 촬영가능일종료
      searchKind: '999', // 검색종류
      searchData: '', // 검색어

      orderKind1: 'O1', // 정렬대상
      orderKind2: 'O1', // 정렬방법

      // 회원 표시 리스트
      memberList: [], // 회원정보
      memberListCount: 0, // 회원전체카운트

      // 페이징 관련
      curPage: 1, // 현재페이지
      viewPageCnt: 10, // 페이지당보여질수
      pagingData: [], // 페이지번호 모듬
      totPageCnt: 0 // 전체페이지수
    }
  });

  /**
   * Public methods
   */
  _.assign(vm, {

    onTabClick: onTabClick,
    onMemberClick: onMemberClick,
    onRequestClick: onRequestClick,
    onCancelClick: onCancelClick,
    intersetMem : intersetMem,

    search: search,
    searchMore: searchMore,
    selectAll: selectAll
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    // 연령 Data 생성
    vm.startAgeOptions = [{ v: '999', k: '최소연령' }];
    vm.endAgeOptions = [{ v: '999', k: '최대연령' }];
    for (var i = 1; i <= 100; i++) {
      vm.startAgeOptions.push({ v: i + '', k: i });
      vm.endAgeOptions.push({ v: i + '', k: i });
    }

    // 출연횟수 Data 생성
    vm.requestCountOptions = [{ v: '999', k: '출연횟수' }];
    for (var i = 1; i <= 50; i++) {
      vm.requestCountOptions.push({ v: i + '', k: i });
    }

    vm.activeTabNumber = Number($stateParams.m) || 1;

    if ($stateParams.backupData) {
      _.assign(vm, $stateParams.backupData);
    } else {
      search(vm.activeTabNumber);
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }
  
  /**
   * 관심회원등록
   */
  function intersetMem(kind, data){
    
    if(data.MEMBER_INTERSET > 0){
      kind = 'D';
    }else{
      kind = 'I';
    }
    
    var msg = '';
    if(kind == 'I'){
       msg = '관심회원으로 등록하시겠습니까?';
    }else{
       msg = '관심출연자에서 삭제하시겠습니까?';
    }
    
    var confirmResult = $message.confirm(msg);
    if(!confirmResult) return false;
    
    var param = {};
    param.procKind = kind;                         //[필수] (I : 등록, D : 삭제)
    param.memberSeq = data.MEMBER_SEQ;              //[필수] 관심회원 일련번호
    
    CompanySvc.setInterset(param)
    .then(function(data){
      $message.alert('정상적으로  처리 되었습니다.');
      search(vm.activeTabNumber);
    })
    .catch(function(err){
    })
    
  }
  
  

  // 프로그램 분야에 대한 모집 분야 코드 조회
  // 관심출연자 탭
  $scope.$watch('cmMngList.interestedActorsTabVars.supportField', function(
    newVal,
    oldVal,
    scope
  ) {
    getHopeCastOptions('interestedActorsTabVars', newVal, oldVal, scope);
  });
  // 출연완료 탭
  $scope.$watch('cmMngList.completedActorsTabVars.supportField', function(
    newVal,
    oldVal,
    scope
  ) {
    getHopeCastOptions('completedActorsTabVars', newVal, oldVal, scope);
  });

  /**
   * Custom functions
   */

  /*=================================================================
   global functions
   =================================================================*/

  // 탭에 따른 희망배역 가져오기
  function getHopeCastOptions(tabVariables, newVal, oldVal, scope) {
    if (newVal == oldVal) return false;
    if (newVal == '999') {
      vm[tabVariables]['hopeCast'] = '999';
      vm[tabVariables]['hopeCastOptions'] = [{ v: '999', k: '희망배역' }];
      return false;
    }

    var param = {};
    param.pgmReutField = newVal; //[필수][공코: PGM_REUT_FIELD] 분야

    CompanySvc.selectPgmHopeCast(param)
      .then(function(data) {
        vm[tabVariables]['hopeCastOptions'] = [{ v: '999', k: '희망배역' }];
        vm[tabVariables]['hopeCast'] = '999';
        _.forEach(data.user.resultData.pgmHopeCast, function(obj) {
          vm[tabVariables]['hopeCastOptions'].push({
            v: obj.COMMON_INFO_VALUE1,
            k: obj.COMMON_INFO_VALUE2
          });
        });
      })
      .catch(function(err) {});
  }

  function onTabClick(tabNumber) {
    vm.activeTabNumber = tabNumber;
    search(vm.activeTabNumber);
  }

  function onMemberClick(data) {
    logger.log('onMemberClick: ', data);
    var params = {
      memberInfo: data,
      prevViewId: `cmMngList.tab${vm.activeTabNumber}`
    };
    $rootScope.goView('comp.mainc.cmPesnInfo', vm, params);
  }

  function onRequestClick(data) {
    var checkedMemberList = [];
    if (!Array.isArray(data)) {
      checkedMemberList.push(data);
      logger.log('onRequestClick [single request]: ', checkedMemberList);
    } else {
      checkedMemberList = data.filter(function(member) {
        return member.chk;
      });
      if (checkedMemberList.length === 0) {
        $message.alert('선택된 출연자가 없습니다.');
        return false;
      }
      logger.log('onRequestClick [multiple request]: ', checkedMemberList);
    }

    //모달 오픈
    var modalInstance = $modal.open({
      templateUrl: 'mainc/cmMainReqP/cmMainReqP.tpl.html',
      controller: 'cmMainReqPCtrl',
      controllerAs: 'cmMainReqP',
      scope: $scope,
      resolve: {
        item: function() {
          return checkedMemberList;
        }
      }
    });

    modalInstance.opened
      .then(function() {
        // 모달 오픈 성공
      })
      .catch(function(checkedMemberList) {
        alert(checkedMemberList);
        // 모달 오픈 실패
      });

    modalInstance.result
      .then(function(result) {
        // 정상 종료
        logger.log('onRequestClick Close', result);
      })
      .catch(function(reason) {
        // 취소
        logger.log('onRequestClick Dismiss', reason);
      });
  }

  function onCancelClick(data, kind) {
    var confirmResult = '';
    if(kind == 'P1'){
      confirmResult = $message.confirm('요청을 취소하시겠습니까?');
    }else if(kind == 'P2'){
      confirmResult = $message.confirm('요청을 수락하시겠습니까?');
    }else{
      confirmResult = $message.confirm('확정을 취소하시겠습니까?');
    }
    if (!confirmResult) return false;

    
    var param = {};
    param.proKind = kind; //[필수] 처리종류 (P1: 요청 취소 , P2: 요청수락, P3: 수락취소)
    param.requestInfoSeq = data.REQUST_INFO_SEQ; //[필수] 모집신청일련번호

    CompanySvc.procRequest(param)
      .then(function(data) {
        $message.alert('처리 되었습니다.');
        search(vm.activeTabNumber);
      })
      .catch(function(error) {
        logger.log('fail');
        logger.error(error);
      });
  }

  function selectAll(tabNumber, isSelectedAll, memberList) {
    if (isSelectedAll) {
      _.forEach(memberList, function(member) {
        member.chk = true;
      });
    } else {
      _.forEach(memberList, function(member) {
        member.chk = false;
      });
    }

//    vm.interestedActorsTabVars.memberList = memberList;

     switch(tabNumber) {
       case 1:
         vm.interestedActorsTabVars.memberList = memberList;
         break;
       case 2:
         vm.pendingActorsTabVars.memberList = memberList;
         break;
       case 3:
         vm.completedActorsTabVars.memberList = memberList;
         break;
     }
  }

  function searchMore(tabNumber) {
    switch(tabNumber) {
      case 1:
        vm.interestedActorsTabVars.curPage++;
        break;
      case 2:
        vm.pendingActorsTabVars.curPage++;
        break;
      case 3:
        vm.completedActorsTabVars.curPage++;
        break;
    }

    search(tabNumber, false);
  }

  function search(tabNumber, shouldResetMemberList = true) {
    var vars = {};
    // var tab = '';

    switch (tabNumber) {
      case 1:
        // tab = 'interestedActorsTabVars';
        vars = _.clone(vm.interestedActorsTabVars);
        break;
      case 2:
        // tab = 'pendingActorsTabVars';
        vars = _.clone(vm.pendingActorsTabVars);
        break;
      case 3:
        // tab = 'completedActorsTabVars';
        vars = _.clone(vm.completedActorsTabVars);
        break;
    }

    if (shouldResetMemberList) {
      vars.memberList = [];
      vars.memberListCount = 0;
      vars.pagingData = [];
      vars.curPage = 1;
    }

    if (!validateAges(vars)) {
      return false;
    }

    // 활성탭에 따른 api 실행
    getMemberList(tabNumber, getParams(vars))
      .then(function(resultData) {

          if(resultData.memberList.length > 0){
              // 리스트 업데이트
              if(shouldResetMemberList){
                  vars.memberList = resultData.memberList;
              }else{
                  vars.memberList = vars.memberList.concat(resultData.memberList);
              }
              vars.memberListCount = resultData.memberListCount;

              // 의상정보 가공
              _.forEach(vars.memberList, function(member) {
                  member.dressInfo = getDressInfo(member);
              });
              
              // 첨부파일 가공
              _.forEach(vars.memberList, function(member) {
                  member.url = '';
                  if(!_.isBlank(member.PHOTO_DATA)){
                    member.url = member.PHOTO_DATA[0].FILE_WEB_PATH;
                  }
              });

              // 전체 페이지 정보입력
              vars.totPageCnt = Math.floor(
                  vars.memberListCount / vars.viewPageCnt +
                  (vars.memberListCount % vars.viewPageCnt == 0 ? 0 : 1)
              );
              vars.pagingData = [];
              for (var i = 1; i <= vars.totPageCnt; i++) {
                  vars.pagingData.push({ pageNo: i });
              }
              
          }else{
              if(shouldResetMemberList){
                  vars.memberList = [];
                  vars.memberListCount = 0;
                  vars.pagingData = [];
              }
          }
          switch (tabNumber) {
              case 1:
                  vm.interestedActorsTabVars = _.clone(vars);
                  break;
              case 2:
                  vm.pendingActorsTabVars = _.clone(vars);
                  break;
              case 3:
                  vm.completedActorsTabVars = _.clone(vars);
                  break;
          }

        logger.log('search success', tabNumber, vm);
      })
      .catch(function(error) {
          vars.curPage = !shouldResetMemberList ? vars.curPage-1 : vars.curPage;

          switch (tabNumber) {
              case 1:
                  vm.interestedActorsTabVars = _.clone(vars);
                  break;
              case 2:
                  vm.pendingActorsTabVars = _.clone(vars);
                  break;
              case 3:
                  vm.completedActorsTabVars = _.clone(vars);
                  break;
          }
      });
  }

  function getMemberList(tabNumber, param) {
    var differed = $q.defer();
    switch (tabNumber) {
      case 1:
          CompanySvc.selectIntersetMemberlist(param)
              .then(function(data) {
                  return differed.resolve(data.user.resultData);
              })
              .catch(function(err) {
                  return differed.reject(err);
              });
        break;
      case 2:
          CompanySvc.selectRcutMemberlist(param)
              .then(function(data) {
                  return differed.resolve(data.user.resultData);
              })
              .catch(function(err) {
                  return differed.reject(err);
              });
        break;
      case 3:
          CompanySvc.selectRcutEndMemberlist(param)
              .then(function(data) {
                  return differed.resolve(data.user.resultData);
              })
              .catch(function(err) {
                  return differed.reject(err);
              });
        break;
    }
    return differed.promise;
  }

  // 의상정보 가공
  function getDressInfo(member) {
    var dressInfo = '';
    var DRESS_OPTIONS = [
      { prop: 'DRESS_BASE_COUNT', value: '기본정장 ' },
      { prop: 'DRESS_SEMI_COUNT', value: '세미정장 ' },
      { prop: 'DRESS_BLACK_SHOES', value: '검정구두 ' },
      { prop: 'DRESS_SWIMSUIT', value: '수영복 ' },
      { prop: 'DRESS_SCHOOL_UNIFORM', value: '교복 ' },
      { prop: 'DRESS_MILITARY_BOOTS', value: '군화 ' }
    ];

    DRESS_OPTIONS.forEach(function(option) {
      try {
        if (Number(member[option.prop]) > 0) dressInfo += option.value;
      } catch (e) {}
    });

    return dressInfo.trim() || '없음';
  }

  // 최소, 최대연령 체크
  function validateAges(vars) {
    if (vars.startAge != '999' && vars.endAge == '999') {
      $message.alert('최대연령을 선택하세요.');
      return false;
    }

    if (vars.endAge != '999' && vars.startAge == '999') {
      $message.alert('최소연령을 선택하세요.');
      return false;
    }

    if (Number(vars.startAge) > Number(vars.endAge)) {
      $message.alert('최소연령이 최대연령 값보다 작아야 합니다.');
      return false;
    }

    return true;
  }

  // param 생성
  function getParams(vars) {
    var heightStart = '';
    var heightEnd = '';
    var weightStart = '';
    var weightEnd = '';

    // 키 범위 구함
    if (vars.height && vars.height != '999') {
      heightStart = vars.height.split('~')[0];
      heightEnd = vars.height.split('~')[1];
    }

    // 몸무게 범위 구함
    if (vars.weight && vars.weight != '999') {
      weightStart = vars.weight.split('~')[0];
      weightEnd = vars.weight.split('~')[1];
    }

    var param = {};
    param.startNo = (vars.curPage - 1) * vars.viewPageCnt; //[필수] (현재페이지 -1) * 페이지당보여질수
    param.endNo = vars.viewPageCnt; //[필수] 페이징 페이지당보여질수

    param.startDate = _.isEmpty(vars.startDate)
      ? ''
      : moment(vars.startDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'); //[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
    param.endDate = _.isEmpty(vars.endDate)
      ? ''
      : moment(vars.endDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'); //[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]

    param.searchKind = vars.searchKind; //[선택] 조회조건 [1:지역 2:이름 3:역명4:연락처]
    param.searchData = vars.searchData; //[선택] 검색어

    // param.orderKind1 = vars.orderKind1;
    // param.orderKind2 = vars.orderKind2;

    param.supportField =
      vars.supportField == '999' || !vars.supportField
        ? ''
        : vars.supportField.replace('P', 'S'); //[선택][공코 : PGM_REUT_FIELD] 지원분야

    param.hopeCast = vars.hopeCast == '999' ? '' : vars.hopeCast; //[선택][공코 : HOPE_CAST] 희망배역

    param.startAge =
      vars.startAge == '999' || !vars.startAge ? '' : vars.startAge.toString(); //[선택] 연령 시작

    param.endAge =
      vars.endAge == '999' || !vars.endAge ? '' : vars.endAge.toString(); //[선택] 연령 끝

    param.memberGender = vars.memberGender == '999' ? '' : vars.memberGender; //[선택][공코 : MEMBER_GENDER] 성별

    param.heightStart = heightStart + ''; //[선택] 키 시작

    param.heightEnd = heightEnd + ''; //[선택] 키 끝

    param.weightStart = weightStart + ''; //[선택] 몸무게 시작

    param.weightEnd = weightEnd + ''; //[선택] 몸무게 끝

    param.requestCount =
      vars.requestCount == '999' || !vars.requestCount
        ? ''
        : vars.requestCount.toString(); //[선택] 출연횟수

    param.bodyInfo = vars.bodyInfo == '999' ? '' : vars.bodyInfo; //[선택][공코 : BODY_INFO] 신체사항

    param.dressInfo = vars.dressInfo == '999' ? '' : vars.dressInfo; //[선택][공코 : DRESS_INFO] 보유의상

    param.classInfo = vars.classInfo == '999' ? '' : vars.classInfo; //[선택][공코 : CLASS_INFO] 보유 방송등급

    param.hairHeight = vars.hairHeight == '999' ? '' : vars.hairHeight; //[선택][공코 : HAIR_HEIGHT] 헤어길이

    param.hairState = vars.hairState == '999' ? '' : vars.hairState; //[선택][공코 : HAIR_HEIGHT] 헤어상태

    param.tatooInfo = vars.tatooInfo == '999' ? '' : vars.tatooInfo; //[선택][공코 : TATOO_INFO] 타투여부

    param.pierInfo = vars.pierInfo == '999' ? '' : vars.pierInfo; //[선택][공코 : PIER_INFO] 피어싱여부

    param.driveInfo = vars.driveInfo == '999' ? '' : vars.driveInfo; //[선택] 운전여부

    return param;
  }

}
