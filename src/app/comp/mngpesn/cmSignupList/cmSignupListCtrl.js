angular.module('comp.mngpesn').controller('cmSignupListCtrl', cmSignupListCtrl);

function cmSignupListCtrl(
  $log,
  $scope,
  $rootScope,
  $message,
  CompanySvc,
  $stateParams
) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    // global variables
    activeTabNumber: 1,

    signupListWaitingTabVars: {
      isSelectedAll: false, // 전체선택
      startDate: '', // 검색시작 날짜
      endDate: '', // 검색종료 날짜
      searchData: '', // 검색어
      showShowMoreButton: true, // 더보기 버튼 표시

      // 회원 표시 리스트
      memberList: [], // 회원정보
      memberListCount: 0, // 회원전체카운트

      // 페이징 관련
      curPage: 1, // 현재페이지
      viewPageCnt: 10, // 페이지당보여질수
      pagingData: [], // 페이지번호 모듬
      totPageCnt: 0 // 전체페이지수
    },

    signupListDeniedTabVars: {
      isSelectedAll: false, // 전체선택
      startDate: '', // 검색시작 날짜
      endDate: '', // 검색종료 날짜
      searchData: '', // 검색어
      showShowMoreButton: true, // 더보기 버튼 표시

      // 회원 표시 리스트
      memberList: [], // 회원정보
      memberListCount: 0, // 회원전체카운트

      // 페이징 관련
      curPage: 1, // 현재페이지
      viewPageCnt: 10, // 페이지당보여질수
      pagingData: [], // 페이지번호 모듬
      totPageCnt: 0 // 전체페이지수
    }
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    search: search,
    selectAll: selectAll,
    onTabClick: onTabClick,
    onMemberClick: onMemberClick,
    handleMemberState: handleMemberState,
    onShowMoreButtonClick: onShowMoreButtonClick
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    
    if($stateParams.backupData){
      _.assign(vm, $stateParams.backupData);
    }else{
      switch (vm.activeTabNumber) {
      case 1:
        search('M1');
        break;
      case 2:
        search('M3');
        break;
    }
    }
    
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  /*=================================================================
    global functions
    =================================================================*/

    function handleMemberState(memberSeq, nextState) {
      var nextStateText = nextState === 'M2' ? '승인' : nextState === 'M3' && '반려'

      var confirmed = $message.confirm(`회원가입 ${nextStateText} 하시겠습니까?`);

      if (confirmed) {
        var param = {
          memberSeq,
          memberState: nextState
        }

        CompanySvc.procJoin(param)
          .then(function(resultData) {
            $message.alert(`회원가입 ${nextStateText} 되었습니다.`);
            
            var memberState = vm.activeTabNumber === 1 ? 'M1' : vm.activeTabNumber === 2 && 'M3';
            search(memberState);
            logger.log('handleMemberState[SUCCESS]', 'resultData: ', resultData);
          })
          .catch(function(error) {
            logger.log('handleMemberState[FAILED]', 'error: ', error);
          });
      }
    }

    function selectAll(tabNumber, isSelectedAll, memberList) {
      if (isSelectedAll) {
        _.forEach(memberList, function(member) {
          member.chk = true;
        });
      } else {
        _.forEach(memberList, function(member) {
          member.chk = false;
        });
      }

      switch(tabNumber) {
        case 1:
          vm.signupListWaitingTabVars.memberList = memberList;
          break;
        case 2:
          vm.signupListDeniedTabVars.memberList = memberList;
          break;
      }
    }

  function onTabClick(tabNumber) {
    vm.activeTabNumber = tabNumber;
    var memberState = tabNumber === 1 ? 'M1' : tabNumber === 2 && 'M3';
    search(memberState);
  }

  function onMemberClick(member, memberState) {
    var params = {
      memberInfo: member,
      prevViewId: 'cmSignupList.' + memberState
    };

    logger.log('onMemberClick: ', 'params: ', params);

    $rootScope.goView('comp.mainc.cmPesnInfo', vm, params);
  }

  function onShowMoreButtonClick(memberState) {
    switch(memberState) {
      case 'M1':
        vm.signupListWaitingTabVars.curPage++;
        break;
      case 'M3':
        vm.signupListDeniedTabVars.curPage++;
        break;
    }

    search(memberState, false);
  }

  function search(memberState, shouldResetMemberList = true) {
    var vars =
      memberState === 'M1'
        ? _.clone(vm.signupListWaitingTabVars)
        : memberState === 'M3' && _.clone(vm.signupListDeniedTabVars);

    if (shouldResetMemberList) {
      vars.memberList = [];
      vars.memberListCount = 0;
      vars.pagingData = [];
      vars.curPage = 1;
      vars.showShowMoreButton = true;
    }

    var param = getParams(vars, memberState);

    CompanySvc.selectMemberJoinInfolist(param)
      .then(function(data) {
        // 리스트 업데이트
        vars.memberList = vars.memberList.concat(
          data.user.resultData.memberList
        );
        
        vars.memberListCount = data.user.resultData.memberListCount;

        // 리스트에 추가할 항목이 없을 경우
        if (data.user.resultData.memberList.length === 0 && vars.curPage > 1) {
          vars.curPage--;

          // 기존 리스트에 항목이 추가됐을 경우
          // or 리스트에 처음 항목이 추가됐을 경우
        }

        // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
        if (data.user.resultData.memberList.length < vars.viewPageCnt) {
          vars.showShowMoreButton = false;
        }
        
        // 첨부파일 가공
        _.forEach(vars.memberList, function(member) {
            member.url = '';
            if(!_.isBlank(member.PHOTO_DATA)){
              member.url = member.PHOTO_DATA[0].FILE_WEB_PATH;
            }
        });

        // 의상정보 가공
        _.forEach(vars.memberList, function(member) {
          member.dressInfo = getDressInfo(member);
        });


        switch(memberState) {
          case 'M1':
            vm.signupListWaitingTabVars = _.clone(vars);
            break;
          case 'M3':
            vm.signupListDeniedTabVars = _.clone(vars);
            break;
        }

        logger.log('search success', 'memberState: ' , memberState, 'vm: ', vm);
      })
      .catch(function(err) { console.log(err) });
  }

  // param 생성
  function getParams(vars, memberState) {
    var param = {};
    param.memberState = memberState; //[필수][공코:MEMBER_STATE] (M1 : 승인대기, M3 : 반려 )
    param.startNo = (vars.curPage - 1) * vars.viewPageCnt; //[필수] (현재페이지 -1) * 페이지당보여질수
    param.endNo = vars.viewPageCnt; //[필수] 페이징 페이지당보여질수

    param.searchKind = '999';
    param.searchData = vars.searchData; //[선택] 검색어
    param.startDate = _.isEmpty(vars.startDate)
      ? ''
      : moment(vars.startDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'); //[선택] 가입 일시 [일시 패턴 : 0000-00-00 00:00:00]
    param.endDate = _.isEmpty(vars.endDate)
      ? ''
      : moment(vars.endDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss'); //[선택] 가입 일시 [일시 패턴 : 0000-00-00 00:00:00]

    return param;
  }

  // 의상정보 가공
  function getDressInfo(member) {
    var dressInfo = '';
    var DRESS_OPTIONS = [
      { prop: 'DRESS_BASE_COUNT', value: '기본정장 ' },
      { prop: 'DRESS_SEMI_COUNT', value: '세미정장 ' },
      { prop: 'DRESS_BLACK_SHOES', value: '검정구두 ' },
      { prop: 'DRESS_SWIMSUIT', value: '수영복 ' },
      { prop: 'DRESS_SCHOOL_UNIFORM', value: '교복 ' },
      { prop: 'DRESS_MILITARY_BOOTS', value: '군화 ' }
    ];

    DRESS_OPTIONS.forEach(function(option) {
      try {
        if (Number(member[option.prop]) > 0) dressInfo += option.value;
      } catch (e) {}
    });

    return dressInfo.trim() || '없음';
  }
}
