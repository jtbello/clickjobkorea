
angular
  .module('comp.mngpesn', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('comp.mngpesn', {
      url: '/mngpesn',
      abstract: false,
      template: '<div ui-view><h1>comp.mngpesn</h1></div>',
      controller: 'mngpesnCtrl as mngpesn'
    })

    .state('comp.mngpesn.cmMngList', {
      url: '/cmMngList?m',
      templateUrl: 'mngpesn/cmMngList/cmMngList.tpl.html',
      data: { menuTitle : '출연자 관리', menuId : 'cmMngList', depth : 1},
        params: { param: null, backupData: null, m: null },
      controller: 'cmMngListCtrl as cmMngList'
    })
      .state('comp.mngpesn.cmMngInfo', {
          url: '/cmMngInfo',
          templateUrl: 'mngpesn/cmMngInfo/cmMngInfo.tpl.html',
          data: { menuTitle : '출연자 정보', menuId : 'cmMngInfo', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cmMngInfoCtrl as cmMngInfo'
      })
      .state('comp.mngpesn.cmSignupList', {
          url: '/cmSignupList',
          templateUrl: 'mngpesn/cmSignupList/cmSignupList.tpl.html',
          data: { menuTitle : '회원가입 요청승인', menuId : 'cmSignupList', depth : 1},
          params: { param: null, backupData: null },
          controller: 'cmSignupListCtrl as cmSignupList'
      })
      .state('comp.mngpesn.cmSignupInfo', {
          url: '/cmSignupInfo',
          templateUrl: 'mngpesn/cmSignupInfo/cmSignupInfo.tpl.html',
          data: { menuTitle : '출연자 정보', menuId : 'cmSignupInfo', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cmSignupInfoCtrl as cmSignupInfo'
      })
  ;
    ;

  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
