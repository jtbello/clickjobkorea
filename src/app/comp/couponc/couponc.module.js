
angular
.module('comp.couponc', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('comp.couponc', {
        url: '/couponc',
        abstract: false,
        template: '<div ui-view><h1>comp.couponc</h1></div>',
        controller: 'couponcCtrl as couponc'
    })

    .state('comp.couponc.ccCopnList', {
        url: '/ccCopnList?param',
        templateUrl: 'couponc/ccCopnList/ccCopnList.tpl.html',
        data: { menuTitle : '쿠폰 리스트', menuId : 'ccCopnList' , depth : 1},
        controller: 'ccCopnListCtrl as ccCopnList'
    })
    .state('comp.couponc.ccCopnShop', {
        url: '/ccCopnShop?param',
        templateUrl: 'couponc/ccCopnShop/ccCopnShop.tpl.html',
        data: { menuTitle : '쿠폰 구매', menuId : 'ccCopnShop' , depth : 1},
        controller: 'ccCopnShopCtrl as ccCopnShop'
    })
    ;
}
