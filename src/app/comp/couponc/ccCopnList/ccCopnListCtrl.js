
angular
.module('comp.couponc')
.controller('ccCopnListCtrl', ccCopnListCtrl);

function ccCopnListCtrl($log, $scope, $user, GuestSvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;
  
  /**
   * Public variables
   */
  _.assign(vm, {
    stateKind : '',           // 상태
    
    searchKind : '',           //[선택] 검색어종류
    searchData : '',          //[선택] 검색어
    
    // 페이징 관련
    curPage : 1,              // 현재페이지
    viewPageCnt : 10,         // 페이지당보여질수
    viewPageingCnt : 5,       // 페이징수  (20180704)
    totPageCnt : 0,           // 전체페이지수
    
    
    myCouponInfoList :[],       // 조회결과
    myCouponInfoListCount : 0,  // 총건수
    myCouponCount : 0,          // 쿠폰보유개수
    
    showShowMoreButton: true, // 더보기 버튼 표시
  });
  
  
  /**
   * Public methods
   */
  _.assign(vm, {
    searchFn : searchFn,          // 검색
    searchMore: searchMore,

  });
  

  /**
   * Initialize
   */
  init();
  
  function init() {
    logger.debug('init', vm);
  
    $scope.$on('$destroy', destroy);
    
    // 최초 조회
    searchFn();
  }
  
  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }
  
  /**
   * Custom functions
   */
  
  function searchMore() {
    vm.curPage++;
    searchFn();
  }
  
  /**
   * 검색 이벤트
   */
  function searchFn(){
    
    var param = {};
    param.startNo = (vm.curPage -1) * vm.viewPageCnt;             //[필수] (현재페이지 -1) * 페이지당보여질수 
    param.endNo = vm.viewPageCnt;                       //[필수] 페이징 페이지당보여질수
    
    
    param.stateKind = '';                       // 상태
    param.searchKind = '';                      //[선택] 검색어종류
    param.searchData = '';                      //[선택] 검색어
    
    param.startDate = _.isEmpty(vm.startDate) ? '' : moment(vm.startDate+' 00:00:00', 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');        //[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
    param.endDate = _.isEmpty(vm.endDate) ? '' : moment(vm.endDate+' 23:59:59', 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');          //[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
    param.busnLinkKeyType = 'B2';
    param.busnLinkKey = $user.currentUser.getUserInfoList()[0].COMPANY_SEQ;
    
    
    GuestSvc.selectMyCouponList(param)
      .then(function(data){
        if(data.user.resultData.myCouponInfoList.length > 0){
          
          vm.myCouponInfoList = vm.myCouponInfoList.concat(data.user.resultData.myCouponInfoList);
          vm.myCouponInfoListCount = data.user.resultData.myCouponInfoListCount;
          vm.myCouponCount = data.user.resultData.myCouponCount;
          
          // 리스트에 추가할 항목이 없을 경우
          if (data.user.resultData.myCouponInfoList.length === 0 && vm.curPage > 1) {
            vm.curPage--;

            // 기존 리스트에 항목이 추가됐을 경우
            // or 리스트에 처음 항목이 추가됐을 경우
          }
          
          // 전체 페이지 정보입력
          vm.totPageCnt = Math.floor((vm.myCouponInfoListCount / vm.viewPageCnt) + ((vm.myCouponInfoListCount % vm.viewPageCnt) == 0 ? 0 : 1));
          
        }else{
          vm.myCouponInfoList = [];
          vm.myCouponInfoListCount = 0;
        }
        
        // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
        if (data.user.resultData.myCouponInfoList.length < vm.viewPageCnt) {
          vm.showShowMoreButton = false;
        }
        
      })
      .catch(function(err){
      })
    
  }

}
