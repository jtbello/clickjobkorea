
angular
  .module('comp', [
      'comp.authc',
      'comp.couponc',
      'comp.mainc',
      'comp.mngpesn',
      'comp.mypagec',
      'comp.notice',
      'comp.paymt',
      'comp.progmc',
      'comp.qna'
  ])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('comp', {
      url: '?',
      abstract: false,
      template: '<div ui-view><h1>comp</h1></div>',
      controller: 'compCtrl as comp',
        resolve: {
            Env: function($env) {
                return $env.get();
            },
            User : function($user){
                return $user.get();
            }
        }
    })
    ;

  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
