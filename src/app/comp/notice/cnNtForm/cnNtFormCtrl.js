

angular
.module('comp.notice')
.controller('cnNtFormCtrl', cnNtFormCtrl);

function cnNtFormCtrl($log, $rootScope, $scope, $state, $stateParams, $timeout, $user, $message, $window, $q, $storage, $filterSet, User , ComSvc, ngspublic) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    prevViewId: '',
    notiKind: 'P',
    topNotiYn: 'Y',
    title: '',
    detailCntn: '',
    notiSeq: -1
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    insert: insert,
    update: update,
    cancel: cancel
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    vm.prevViewId = $stateParams.param.prevViewId;

    if(!_.isBlank($stateParams.param.data)){
      var data = $stateParams.param.data;
      vm.notiSeq = data.NOTI_SEQ;
      vm.notiKind = data.NOTI_KIND;
      vm.topNotiYn = data.TOP_NOTI_YN;
      vm.title = data.TITLE;
      vm.detailCntn = data.DETAIL_CNTN;

      vm.notiStartDtm = $filterSet.shortDate(data.NOTI_START_DTM);
      vm.notiEndDtm = $filterSet.shortDate(data.NOTI_END_DTM);
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  function insert(){

    var chk = true;

    chk = chk && ngspublic.nullCheck(vm.title,	    '제목을 입력해 주세요.', '');
    chk = chk && ngspublic.nullCheck(vm.detailCntn,	    '내용을 입력해 주세요.', '');
    chk = chk && ngspublic.dateCheck(vm.notiStartDtm, vm.notiEndDtm, '공지');

    if(!chk) return false;

    var confirmResult = $message.confirm('등록하시겠습니까?');
    if(!confirmResult) return false;

    var param = {};
    param.topNotiYn = vm.topNotiYn;							// [필수] Y: 상단공지, N: 일반공지
    param.notiKind = vm.notiKind;							// [필수] P: 출연자공지, C : 기업 공지(어플관리자권한에만 노출), CC: 기업내 공지  A: 전체 공지(어플관리자권한에만 노출)
    param.title = vm.title;						            // [필수] 공지 제목
    param.detailCntn = vm.detailCntn;					    // [필수] 공지 내용
    param.notiStartDtm = _.isEmpty(vm.notiStartDtm) ? '' : moment(vm.notiStartDtm, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');		        // [필수] 공지 개시 시작일 [일시 패턴 : 0000-00-00 00:00:00]
    param.notiEndDtm = _.isEmpty(vm.notiEndDtm) ? '' : moment(vm.notiEndDtm, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');			    // [필수] 공지 개시 종료일 [일시 패턴 : 0000-00-00 00:00:00]

    ComSvc.insertNoti(param)
        .then(function(data){
          logger.log('success', data);
          $message.alert('정상적으로 등록되었습니다.');
          $rootScope.goView('comp.notice.cnNtList');
        })
        .catch(function(error){
          logger.error('message: ', error);
        });
  }

  function update(){
    var chk = true;

    chk = chk && ngspublic.nullCheck(vm.title,	    '제목을 입력해 주세요.', '');
    chk = chk && ngspublic.nullCheck(vm.detailCntn,	    '내용을 입력해 주세요.', '');
    chk = chk && ngspublic.dateCheck(vm.notiStartDtm, vm.notiEndDtm, '공지');

    if(!chk) return false;

    var confirmResult = $message.confirm('수정하시겠습니까?');
    if(!confirmResult) return false;

    var param = {};
    param.topNotiYn = vm.topNotiYn;							// [필수] Y: 상단공지, N: 일반공지
    param.notiKind = vm.notiKind;							// [필수] P: 출연자공지, C : 기업 공지(어플관리자권한에만 노출), CC: 기업내 공지  A: 전체 공지(어플관리자권한에만 노출)
    param.title = vm.title;						            // [필수] 공지 제목
    param.detailCntn = vm.detailCntn;					    // [필수] 공지 내용
    param.notiStartDtm = _.isEmpty(vm.notiStartDtm) ? '' : moment(vm.notiStartDtm, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');		        // [필수] 공지 개시 시작일 [일시 패턴 : 0000-00-00 00:00:00]
    param.notiEndDtm = _.isEmpty(vm.notiEndDtm) ? '' : moment(vm.notiEndDtm, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');			    // [필수] 공지 개시 종료일 [일시 패턴 : 0000-00-00 00:00:00]
    param.notiSeq = vm.notiSeq;								// [필수] 공지 일련번호

    ComSvc.updateNoti(param)
        .then(function(data){
          logger.log('success', data);
          $message.alert('정상적으로 수정되었습니다.');
          $rootScope.goView('comp.notice.cnNtList');
        })
        .catch(function(error){
          logger.error('message: ', error);
        });
  }

  function cancel(){
    var confirmResult = $message.confirm('취소하시겠습니까?');
    if(!confirmResult) return false;
    $rootScope.goView('comp.notice.cnNtList');
  }
}
/**
 * COMPANY_ SEQ: 100000021
 CREATE_DTM: 1563714210000
 CREATE_NO: "300000030"
 DETAIL_CNTN: "서버테스트1"
 NOTI_END_DTM: 1564498800000
 NOTI_HIT: 1
 NOTI_KIND: "P"
 NOTI_SEQ: 16
 NOTI_START_DTM: 1563634800000
 TITLE: "서버테스트1"
 TOP_NOTI_YN: "Y"
 TRT_DTM: 1563714230000
 TRT_NO: "300000030"
 USE_YN: "Y"
 WRITE_NAME: "류정두"
 */
