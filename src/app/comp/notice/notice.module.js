
angular
.module('comp.notice', [])
.config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
$stateProvider
    .state('comp.notice', {
        url: '/notice',
        abstract: false,
        template: '<div ui-view><h1>comp.notice</h1></div>',
        controller: 'noticeCtrl as notice'
    })

    .state('comp.notice.cnNtList', {
        url: '/cnNtList',
        templateUrl: 'notice/cnNtList/cnNtList.tpl.html',
        data: { menuTitle : '공지사항', menuId : 'cnNtList' , depth : 1},
        params: { param: null, backupData: null },
        controller: 'cnNtListCtrl as cnNtList'
    })
    .state('comp.notice.cnNtDetail', {
        url: '/cnNtDetail',
        templateUrl: 'notice/cnNtDetail/cnNtDetail.tpl.html',
        data: { menuTitle : '공지사항 상세', menuId : 'cnNtDetail' , depth : 2},
        params: { param: null, backupData: null },
        controller: 'cnNtDetailCtrl as cnNtDetail'
    })
    .state('comp.notice.cnNtForm', {
        url: '/cnNtForm',
        templateUrl: 'notice/cnNtForm/cnNtForm.tpl.html',
        data: { menuTitle : '공지사항 등록/수정', menuId : 'cnNtForm' , depth : 3},
        params: { param: null, backupData: null },
        controller: 'cnNtFormCtrl as cnNtForm'
    })
    ;

// $urlRouterProvider.when('/qna', '/qna/tssp5101m')
}
