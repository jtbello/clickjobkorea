
angular
.module('comp.notice')
.controller('cnNtDetailCtrl', cnNtDetailCtrl);

function cnNtDetailCtrl($log, $scope, $rootScope, $user, $stateParams) {
/**
 * Private variables
 */
var logger = $log(this);
var vm = this;

/**
 * Initialize
 */
init();

function init() {
  logger.debug('init', vm);

  $scope.$on('$destroy', destroy);
  
  if(_.isBlank($stateParams.param)){
    $rootScope.goView('comp.notice.cnNtList');
    return false;
  }
  
  if(_.isBlank($stateParams.param.data)){
    $rootScope.goView('comp.notice.cnNtList');
    return false;
  }
  
  
  vm.viewData = $stateParams.param.data;

  vm.writeUser = $user.currentUser.getUserInfoList()[0].COMPANY_MEMBER_NAME;
}

/**
 * Event handlers
 */
function destroy(event) {
  logger.debug(event);
}

/**
 * Custom functions
 */

  vm.goList = function(){
    $rootScope.goView('comp.notice.cnNtList');
  }

  vm.update = function(){
    var param = {
      data : vm.viewData,
      prevViewId : 'cnNtDetail'
    };
    $rootScope.goView('comp.notice.cnNtForm', vm, param);
  }

}
