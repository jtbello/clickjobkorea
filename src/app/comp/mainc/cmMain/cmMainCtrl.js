
angular
  .module('comp.mainc')
  .controller('cmMainCtrl', cmMainCtrl)
  .directive('onLastRepeat', function () {
    return function (scope, element, attrs) {
        if (scope.$last) setTimeout(function () {
            scope.$emit('onRepeatLast', element, attrs);
        }, 1);
    };
  });

function cmMainCtrl($log, $window ,User, $rootScope,$storage, $q, $scope, $modal, $message, $stateParams, MetaSvc, 
                    CompanySvc, ComSvc, $doryPicker, comConstant, MemberSvc, $cordovaInAppBrowser) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      bkLstYn : false,                      // 블랙리스트 여부
      isAllChk : false,					    // 전체 선택 여부
      preferredYn : false,                    // 검색조건 체크박스
      ageList: [],                          // 연령리스트
      hopeCastInfo : [],                    // 희망배역 리스트
      requestList : [],                     // 출연횟수 리스트

      emergYn : 'N',						//[선택] 긴급여부
      startDate : '',	                    //[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
      endDate : '',	                        //[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
      searchKind : '999',					//[선택] 조회조건 [1:지역 2:이름 3:역명]
      searchData : '',						//[선택] 검색어

      supportField : '999',                 //[선택][공코 : PGM_REUT_FIELD] 지원분야
      hopeCast : '999',                     //[선택][공코 : HOPE_CAST] 희망배역
      startAge : '999',						//[선택] 연령 시작
      endAge : '999',					    //[선택] 연령 끝
      height : '999',					// 키
      weight : '999',					// 몸무게
      memberGender : '999',                 //[선택][공코 : GENDER] 성별
      bodyInfo : '999',						//[선택][공코 : BODY_INFO] 신체사항
      classInfo : '999',					//[선택][공코 : CLASS_INFO] 보유 방송등급
      dressInfo : '999',					//[선택][공코 : DRESS_INFO] 보유 의상
      dressInfo2 : '999',					//[선택][공코 : DRESS_INFO] 보유 의상
      hairState : '999',					//[선택][공코 : HAIR_STATE] 헤어상태
      hairHeight : '999',					//[선택][공코 : HAIR_HEIGHT] 헤어길이
      tatooInfo : '999',				    //[선택][공코 : TATOO_INFO] 타투여부
      pierInfo : '999',				        //[선택][공코 : PIER_INFO] 피어싱여부
      driveInfo : '999',                    //[선택] 운전여부
      heightStart : '999',					//[선택] 키 시작
      heightEnd : '999',					//[선택] 키 끝
      weightStart : '999',					//[선택] 몸무게 시작
      weightEnd : '999',					//[선택] 몸무게 끝
      requestCount : '999',					//[선택] 출연횟수

      memberList : [],					// 회원정보
      memberListCount : 0,				// 회원전체카운트

      // 페이징 관련
      curPage : 1,						// 현재페이지
      viewPageCnt : 10,					// 페이지당보여질수
      pagingData : [],					// 페이지번호 모듬
      totPageCnt : 0					// 전체페이지수
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      allChk : allChk,
      search : search,
      goDetail : goDetail,
      showProgmSelectPopup : showProgmSelectPopup,
      showCheckedProgmSelectPopup : showCheckedProgmSelectPopup,
      intersetMem : intersetMem
  });

    // 프로그램 분야에 대한 모집 분야 코드 조회
    $scope.$watch('cmMain.supportField', function(newVal, oldVal, scope) {

        if(newVal === oldVal) return false;
        if(newVal === '999') {
            vm.hopeCast = '999';
            vm.hopeCastInfo = [{v:'999',k:'희망배역'}];
            return false;
        }

        ComSvc.selectCommonInfo(comConstant.HOPE_CAST)
            .then(function(data){
                vm.hopeCastInfo = [{v:'999',k:'희망배역'}];
                vm.hopeCast = '999';
                var obj = _.filter(data.user.resultData.commonData, {COMMON_INFO_VALUE3 : newVal});
                _.forEach(obj,function(obj){
                    vm.hopeCastInfo.push({v:obj.COMMON_INFO_VALUE1, k:obj.COMMON_INFO_VALUE2});
                })
            });
    });

    /*
        popup 관련
    */
    let popup = {
        states : {
            gps : -1,
            push : -1,
            personInf : -1
        },
        init : function(){
            $window.__POPUP_DEV = popup.dev;
            popup.get.all()
            .then(function(){
                console.log(popup);
                popup.show.all();
            });
        },
        get : {
            core : function(key){
              
              var param = {};
              return MemberSvc.selectAgreeInfo(param)
              .then(function(data){
                if(data.user.resultData.agreeInfo.length > 0){
                  var agreeInfo = _.find(data.user.resultData.agreeInfo, {AGREE_KIND : key});
                  if(agreeInfo){
                    if(agreeInfo.AGREE_YN == 'Y'){
                      return 'Y';
                    }else{
                      return 'N';
                    }
                  }else{
                    return 'N';
                  }
                }else{
                  return 'N';
                }
              })
              .catch(function(err){
                return 'N';
              })
              
            },
            all : function(){
                return popup.get.gps()
                    .then(popup.get.push)
                    .then(popup.get.personInf);
            },
            gps : function(){
                return popup.get.core('A2')
                .then(function( data){
                    popup.states.gps = data;
                });
            },
            push : function(){
                return popup.get.core('A3')
                .then(function( data){
                    popup.states.push = data;
                });
            },
            personInf : function(){
                return popup.get.core('A4')
                .then(function( data){
                    popup.states.personInf = data;
                });
            }
        },
        show : {
            core : function(opt){
                let modalInstance = $window.openPop({
                    tplUrl : opt.url,
                    ctrl : opt.ctrl,
                    ctrlAs : opt.ctrlAs,
                    data : {
                        busnLinkKey : User.getUserInfoList()[0].COMPANY_MEMBER_SEQ
                    }
                    });
        
                modalInstance.opened.then(function(){
                // 오픈 성공 
                });
        
                modalInstance.result.then(function(data){
                    opt.callback();
                    logger.log('pop close' , data);
                })
                .catch(function(reason){
                    opt.callback();
                    logger.log('pop Dissmiss' , reason);
                });

                return modalInstance;
            },
            gps : function(){
                let params = {};
                params.url = 'popup/iaGeoP/iaGeoP.tpl.html';
                params.ctrl = 'iaGeoPCtrl';
                params.ctrlAs = 'iaGeoP';
                params.callback = function(){
                    popup.get.gps()
                    .then(popup.init);
                };
                popup.show.core(params);
            },
            push : function(){
                let params = {};
                params.url = 'popup/iaPushP/iaPushP.tpl.html';
                params.ctrl = 'iaPushPCtrl';
                params.ctrlAs = 'iaPushP';
                params.callback = function(){
                    popup.get.push()
                    .then(popup.init);
                };
                popup.show.core(params);
            },
            personInf : function(){
                let params = {};
                params.url = 'popup/iaPersonInfP/iaPersonInfP.tpl.html';
                params.ctrl = 'iaPersonInfPCtrl';
                params.ctrlAs = 'iaPersonInfP';
                params.callback = function(){
                    popup.get.personInf()
                    .then(popup.init);
                };
                popup.show.core(params);
            },
            all : function(){
                if(!popup.states.gps ||popup.states.gps === -1 || popup.states.gps === 'N'){
                    popup.show.gps();
                    // console.log('show.gps', popup.states);
                }
                else if(!popup.states.push || popup.states.push === -1 || popup.states.push === 'N'){
                    popup.show.push();
                    // console.log('show.push' , popup.states);
                }
                else if(!popup.states.personInf || popup.states.personInf === -1 || popup.states.personInf === 'N'){
                    popup.show.personInf();
                    // console.log('show.personInf' , popup.states);
                }
            }
        },
        dev : {
            clear : function(){
                $storage.remove('IA_PERSON_INF_AGREE_KIND');
                $storage.remove('IA_PUSH_AGREE_KIND');
                $storage.remove('IA_PRIVACY_AGREE_KIND');
                $storage.remove('IA_GEO_AGREE_KIND');
            }
        }
    };
    

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

      // 연령 Data 생성
      vm.ageList = [{v :'999', k: '연령'}];
      for(var i = 1; i <= 100; i++){
          vm.ageList.push({v: i+'', k: i + '세'});
      }

      // 출연횟수 Data 생성
      vm.requestList = [{v :'999', k: '출연횟수'}];
      for(var i = 1; i <= 50; i++){
          vm.requestList.push({v: i+'', k: i});
      }

      if($stateParams.backupData){
          _.assign(vm, $stateParams.backupData);
      }
      else{
          if(!_.isEmpty($stateParams.param)) {
              vm.emergYn = $stateParams.param.emergYn === 'Y' ? 'Y' : 'N';
          }
          search(false);
      }

    popup.init();
    
    
    
    // 배너관련
    var param= {};
    param.startNo = 0;                            
    param.endNo = 90; 
    ComSvc.selectBannerList(param)
      .then(function(data){
        vm.bannerList = data.user.resultData.bannerList;
        vm.bannerListCount = data.user.resultData.bannerListCount;
      })
      
      vm.bannerClick = function(elem){
        let jElem = $(elem);
        let bannerLink = jElem.attr('data-banner-link-info');
        let bannerInfoSeq = jElem.attr('info-seq');
        if(_.isBlank(bannerLink)){
        return false;
        }

        var param= {};
        param.bannerClickCount  = 'Y';
        param.bannerInfoSeq = bannerInfoSeq;
        param.companyMemberSeq = '99999999';
        ComSvc.updateBannerCntUp(param);
        var options = {
            location: 'yes',
            scrollbars: 'yes',
            resizable: 'yes'
        };
        $cordovaInAppBrowser.open(bannerLink, '_blank', options);
        
    };
    
  }


  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  
  /**
   * 관심회원등록
   */
  function intersetMem(kind, data){
    
    if(data.MEMBER_INTERSET > 0){
      kind = 'D';
    }else{
      kind = 'I';
    }
    var msg = '';
    if(kind == 'I'){
       msg = '관심회원으로 등록하시겠습니까?';
    }else{
       msg = '관심출연자에서 삭제하시겠습니까?';
    }
    
    var confirmResult = $message.confirm(msg);
    if(!confirmResult) return false;
    
    var param = {};
    param.procKind = kind;                         //[필수] (I : 등록, D : 삭제)
    param.memberSeq = data.MEMBER_SEQ;              //[필수] 관심회원 일련번호
    
    CompanySvc.setInterset(param)
    .then(function(data){
      $message.alert('정상적으로  처리 되었습니다.');
      search(false, vm.curPage);
    })
    .catch(function(err){
    })
    
  }
  

  function allChk(data){
      if(vm.isAllChk){
          _.forEach(data,function(obj, idx){
              obj.chk = true;
          });
      }else{
          _.forEach(data,function(obj, idx){
              obj.chk = false;
          });
      }
  }

  function search(more, saveCurPage){
      vm.curPage = !more ? 1 : vm.curPage+1;

      var param = {};

      var heightStart = '';
      var heightEnd = '';
      var weightStart = '';
      var weightEnd = '';

      // 키 범위 구함
      if(vm.height !== '999'){
          heightStart = vm.height.split('~')[0];
          heightEnd = vm.height.split('~')[1];
      }

      // 몸무게 범위 구함
      if(vm.weight !== '999'){
          weightStart = vm.weight.split('~')[0];
          weightEnd = vm.weight.split('~')[1];
      }

      // 시작나이 종료나이 체크
      if(vm.startAge !== '999' && vm.endAge === '999'){
          $message.alert('연령을 정확하게 입력해주세요.');
          return false;
      }

      if(vm.endAge !== '999' && vm.startAge === '999'){
          $message.alert('연령을 정확하게 입력해주세요.');
          return false;
      }

      if(Number(vm.startAge) > Number(vm.endAge)){
          $message.alert('연령을 정확하게 입력해주세요.');
          return false;
      }

      param.startNo = (vm.curPage -1) * vm.viewPageCnt;								//[필수] (현재페이지 -1) * 페이지당보여질수
      param.endNo = vm.viewPageCnt;													//[필수] 페이징 페이지당보여질수

      if(saveCurPage){
        param.endNo = saveCurPage * vm.viewPageCnt; 
      }
      
      param.startDate = _.isEmpty(vm.startDate) ? '' : moment(vm.startDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');				//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
      param.endDate = _.isEmpty(vm.endDate) ? '' : moment(vm.endDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');					//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]

      param.searchKind = vm.searchKind;						//[선택] 조회조건 [1:지역 2:이름 3:역명4:연락처]
      param.searchData = vm.searchData;						//[선택] 검색어

      param.emergYn = vm.emergYn;                           // [선택] 긴급여부
      param.supportField = vm.supportField === '999' ? '' :	vm.supportField;	//[선택][공코 : PGM_REUT_FIELD] 지원분야
      param.supportField = param.supportField.replace('P','S');
      param.hopeCast = vm.hopeCast === '999' ? '' :	vm.hopeCast;			//[선택][공코 : HOPE_CAST] 희망배역
      param.startAge = vm.startAge === '999' ? '' :	vm.startAge;						//[선택] 연령 시작
      param.startAge = param.startAge + '';
      param.endAge = vm.endAge === '999' ? '' :	vm.endAge;						//[선택] 연령 끝
      param.endAge = param.endAge + '';
      param.memberGender = vm.memberGender === '999' ? '' :	vm.memberGender;				//[선택][공코 : MEMBER_GENDER] 성별
      param.heightStart = heightStart + '';						//[선택] 키 시작
      param.heightEnd = heightEnd + '';							//[선택] 키 끝
      param.weightStart = weightStart + '';						//[선택] 몸무게 시작
      param.weightEnd = weightEnd + '';							//[선택] 몸무게 끝
      param.requestCount = vm.requestCount === '999' ? '' :	vm.requestCount;				//[선택] 출연횟수
      param.requestCount = param.requestCount + '';
      param.bodyInfo = vm.bodyInfo === '999' ? '' :	vm.bodyInfo;							//[선택][공코 : BODY_INFO] 신체사항
      param.dressInfo = vm.dressInfo === '999' ? '' :	vm.dressInfo;						//[선택][공코 : DRESS_INFO] 보유의상
      param.dressInfo2 = vm.dressInfo2 === '999' ? '' :	vm.dressInfo2;						//[선택][공코 : DRESS_INFO] 보유의상
      param.classInfo = vm.classInfo === '999' ? '' :	vm.classInfo;						//[선택][공코 : CLASS_INFO] 보유 방송등급
      param.hairHeight = vm.hairHeight === '999' ? '' :	vm.hairHeight;						//[선택][공코 : HAIR_HEIGHT] 헤어길이
      param.hairState = vm.hairState === '999' ? '' :	vm.hairState;						//[선택][공코 : HAIR_HEIGHT] 헤어상태
      param.driveInfo = vm.driveInfo === '999' ? '' :	vm.driveInfo;						//[선택] 운전여부

      param.tatooInfo = vm.tatooInfo === '999' ? '' :	vm.tatooInfo;						//[선택][공코 : TATOO_INFO] 타투여부
      param.pierInfo = vm.pierInfo === '999' ? '' :	vm.pierInfo;							//[선택][공코 : PIER_INFO] 피어싱여부
      param.bkLstYn = vm.bkLstYn === true ? 'Y' : 'N';                                      //[선택] 블랙리스트

      CompanySvc.selectMemberList(param)
          .then(function(data){
              logger.log('selectMemberList success', data);
              if(data.user.resultData.memberList.length > 0){

                  var tempList = data.user.resultData.memberList;

                  // 의상정보
                  _.forEach(tempList, function(obj){
                      obj.dressInfo = '';
                      try{
                          if(Number(obj.DRESS_BASE_COUNT) > 0) obj.dressInfo += '기본정장  '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_SEMI_COUNT) > 0) obj.dressInfo += '세미정장  '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_BLACK_SHOES) > 0) obj.dressInfo += '검정구두 '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_SWIMSUIT) > 0) obj.dressInfo += '수영복 '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_SCHOOL_UNIFORM) > 0) obj.dressInfo += '교복 '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_MILITARY_BOOTS) > 0) obj.dressInfo += '군화 '
                      }catch(e){}
                      if(vm.dressInfo === '') obj.dressInfo = '없음';
                  })
                  
                  // 사진정보
                  _.forEach(tempList, function(obj){
                     if(obj.PHOTO_DATA.length > 0){
                       obj.url = obj.PHOTO_DATA[0].FILE_WEB_PATH;
                     }
                  })

                  if(!more){
                      vm.memberList = tempList;
                  }else{
                      vm.memberList = vm.memberList.concat(tempList);
                  }

                  vm.memberListCount = data.user.resultData.memberListCount;

                  // 전체 페이지 정보입력
                  vm.totPageCnt = Math.floor((vm.memberListCount / vm.viewPageCnt) + ((vm.memberListCount % vm.viewPageCnt) === 0 ? 0 : 1));
                  vm.pagingData = [];
                  for (var i = 1; i <= vm.totPageCnt; i++) {
                      vm.pagingData.push({pageNo : i});
                  }

              }else{
                  if(!more){
                      vm.memberList = [];
                      vm.memberListCount = 0;
                      vm.pagingData = [];
                  }
              }
          })
          .catch(function(error){
              logger.log('fail');
              logger.error('message: ', error);
              vm.curPage = more ? vm.curPage-1 : vm.curPage;
          });
  }

  function goDetail(data){
      logger.log('goDetail', data);
      var param = {
          memberInfo : data,
          prevViewId : 'cmMain'
      };
      $rootScope.goView('comp.mainc.cmPesnInfo', vm, param);
  }

  function showProgmSelectPopup(data){
      logger.log('showProgmSelectPopup Open', data);

      //모달 오픈
      var modalInstance = $modal.open({
          templateUrl: 'mainc/cmMainReqP/cmMainReqP.tpl.html',
          controller: 'cmMainReqPCtrl',
          controllerAs: 'cmMainReqP',
          scope: $scope,
          resolve: {
              item: function() {
                  return [data];
              }
          }
      });

      modalInstance.opened
          .then(function() {
              // 모달 오픈 성공
          })
          .catch(function(data) {
              alert(data);
              // 모달 오픈 실패
          });

      modalInstance.result
          .then(function(result) {
              // 정상 종료
              logger.log('showProgmSelectPopup Close', result);
          })
          .catch(function(reason) {
              // 취소
              logger.log('showProgmSelectPopup Dismiss', reason);
          });
  }

  function showCheckedProgmSelectPopup(){
      logger.log('showCheckedProgmSelectPopup Open');

      var list = _.filter(vm.memberList, function(obj){ return obj.chk === true; })

      if(list.length === 0){
          $message.alert('출연자를 선택 해 주세요.');
          return false;
      }

      //모달 오픈
      var modalInstance = $modal.open({
          templateUrl: 'mainc/cmMainReqP/cmMainReqP.tpl.html',
          controller: 'cmMainReqPCtrl',
          controllerAs: 'cmMainReqP',
          scope: $scope,
          resolve: {
              item: function() {
                  return list;
              }
          }
      });

      modalInstance.opened
          .then(function() {
              // 모달 오픈 성공
          })
          .catch(function(data) {
              alert(data);
              // 모달 오픈 실패
          });

      modalInstance.result
          .then(function(result) {
              // 정상 종료
              logger.log('showCheckedProgmSelectPopup Close', result);
          })
          .catch(function(reason) {
              // 취소
              logger.log('showCheckedProgmSelectPopup Dismiss', reason);
          });

  }
  // 배너 관련 
  $scope.$on('onRepeatLast', function (scope, element, attrs) {
    let opt = {};
    opt.direction = 'vertical';
    opt.preventClicks = false;
    opt.preventClicksPropagation = false;
    opt.autoplayDisableOnInteraction =  false;
    
    if(vm.bannerList.length <= 1){ 
        opt.loop = false; 
    }
    else { 
        opt.loop = true; 
        opt.autoplay = 4000;
    }

    vm.SWIPER = vm.SWIPER || {};
    vm.SWIPER.banner = new Swiper('.banner-swiper-container' , opt);
    window.bannerClick = vm.bannerClick;
  });
}
