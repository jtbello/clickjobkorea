angular.module('comp.mainc').controller('cmPesnInfoVideoCtrl', cmPesnInfoVideoCtrl);

function cmPesnInfoVideoCtrl(
  $log,
  $scope,
  $modal,
  $state,
  $stateParams,
  $message,
  $env
) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    prevViewId: '', // 이전화면 아이디
      memberVideoAttachInfo: [], // 사진 리스트

  });

  /**
   * Public methods
   */
  _.assign(vm, {
    showPage : showPage,        // 회원사진 팝업
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    if ($stateParams.param) {
        vm.memberVideoAttachInfo = $stateParams.param.memberVideoAttachInfo;
        $("#myVideo").attr("src", $env.endPoint.service + vm.memberVideoAttachInfo[0].FILE_WEB_PATH);
        $("#myVideo").load();
    }
    
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

    /**
     * 사진 보기 이동
     */
    function showPage(setPage){
        vm.currentPage = setPage;
    }
}
