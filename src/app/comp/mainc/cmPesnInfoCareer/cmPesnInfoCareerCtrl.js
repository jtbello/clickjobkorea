angular.module('comp.mainc').controller('cmPesnInfoCareerCtrl', cmPesnInfoCareerCtrl);

function cmPesnInfoCareerCtrl(
  $log,
  $scope,
  $modal,
  $state,
  $stateParams,
  $message
) {
    /**
     * Private variables
     */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        // SELECT OPTIONS
        yearOptions: [{ value: '999', text: '연도 선택'}], // 연도 선택 리스트
        linesYnOptions: [
            { value: 'N', text: '대사 없음' },
            { value: 'Y', text: '대사 있음' },
        ], // 대사유무 선택 리스트

        // 현재 입력폼
        field: '999', // [필수] 분야
        pgmName: '', // [필수] 프로그램 이름
        broadcastingStation: '999', // [필수] 방송국
        year: '999', // [필수] 연도
        part: '999', // [필수] 배역
        linesYn: 'N', // [필수] 대사 유무

        // 입력된 경력 리스트
        memberCareerInfo: [
            // {
            //   broadcastingStation: "B3",
            //   field: "F1",
            //   linesYn: "N",
            //   part: "P6",
            //   pgmName: "asdf",
            //   year: "2011"
            // },
            // {
            //   broadcastingStation: "B1",
            //   field: "F3",
            //   linesYn: "Y",
            //   part: "P2",
            //   pgmName: "asdadsdf",
            //   year: "2015"
            // }
        ],
    });

    /**
     * Public methods
     */
    _.assign(vm, {

    });

    /**
     * Initialize
     */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        // 연도 선택 리스트 생성
        var thisYear = new Date().getFullYear();
        for (let i = thisYear - 10; i <= thisYear + 10; i++) {
            vm.yearOptions.push({ value: `${i}`, text: `${i}년` });
        }

        
        // 기존 입력값 복원
        if ($stateParams.param && $stateParams.param.memberCareerInfo) {
            vm.memberCareerInfo = $stateParams.param.memberCareerInfo;
        }

    }

    /**
     * Event handlers
     */
    function destroy(event) {
        logger.debug(event);
    }

    /**
     * Custom functions
     */

}
