
angular
  .module('comp.mainc')
  .controller('cmMainReqPCtrl', cmMainReqPCtrl);

function cmMainReqPCtrl($log, $rootScope, $scope, $modal, $modalInstance, $message, item, ComSvc, CompanySvc, ngspublic, comConstant) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        memberList : item,					// 회원정보
        pgmReutFieldList : [],
        pgmList : [],
        pgmRcutList : [],
        pgmRcutSubList : [],

        pgmBaseSeq : '',
        pgmRcutSeq : '',
        pgmRcutSubSeq : ''
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        confirm : confirm,
        searchProgram : searchProgram
    });

    // 프로그램 분야에 대한 모집 분야 코드 조회
    $scope.$watch('cmMainReqP.pgmBaseSeq', function(newVal, oldVal, scope) {

        if(newVal === oldVal) return false;
        if(newVal === '999') {
            vm.pgmRcutSeq = '999';
            vm.pgmRcutList = _.clone([{v:'999',k:'모집정보 선택'}]);
            vm.pgmRcutSubSeq = '999';
            vm.pgmRcutSubList = [{v:'999',k:'나이_성별 선택'}];
            return false;
        }

        vm.pgmRcutSeq = '999';
        vm.pgmRcutList = _.clone([{v:'999',k:'모집정보 선택'}]);

        /** 프로그램일련번호로 모집정보조회 */
        var param = {};
        param.pgmBaseSeq = newVal;
        CompanySvc.selectPgmRcutListInfo(param)
            .then(function(data){
                logger.log('selectPgmRcutListInfo success', data);
                if(data.user.resultData.rcutListInfo.length > 0){
                    _.forEach(data.user.resultData.rcutListInfo,function(obj){
                        var date = moment(obj.PGM_START_DTM2).format('YYYY-MM-DD / HH:mm');
                        vm.pgmRcutList.push({v:obj.RCUT_BASE_SEQ+'', k:'['+obj.PGM_HOPE_CAST+']'+obj.PGM_REUT_FIELD+' "'+obj.PGM_NAME+(obj.TITLE_GUBUN != '' ? '('+obj.TITLE_GUBUN+')' : '')+'"\n' +
                                                                        date + '\n' + obj.AGE_MIN + '~' + obj.AGE_MAX + '세 '+obj.TOT_COUNT + '명\n' +
                                                                        obj.GENDER, subList : obj.rcutSubInfoList});
                    })
                }else{
                    vm.pgmRcutSeq = '999';
                    vm.pgmRcutList = _.clone([{v:'999',k:'모집정보 없음'}]);
                    vm.pgmRcutSubSeq = '999';
                    vm.pgmRcutSubList = [{v:'999',k:'나이_성별 없음'}];
                }

            })
            .catch(function(error){
                logger.log('fail');
                logger.error('message: ' + error + '\n');
            });
    });

    // 모집정보가 변경되면 나이성별 정보 리셋
    $scope.$watch('cmMainReqP.pgmRcutSeq', function(newVal, oldVal, scope) {
        if(newVal == oldVal) return false;
        if(newVal == '999') {
            vm.pgmRcutSubSeq = '999';
            vm.pgmRcutSubList = [{v:'999',k:'나이_성별 선택'}];
            return false;
        }

        vm.pgmRcutSubList = [{v:'999',k:'나이_성별 선택'}];
        vm.pgmRcutSubSeq = '999';

        var findObj = _.find(vm.pgmRcutList, {v : newVal});
        if(findObj){
            _.forEach(findObj.subList ,function(obj){
                vm.pgmRcutSubList.push({v:obj.RCUT_SUB_INFO_SEQ, k:'['+ obj.AGE_MIN + '세 ~ ' + obj.AGE_MAX +'세]['+obj.GENDER_NM+']'});
            })
        }

    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        ComSvc.selectCommonInfo(comConstant.PGM_REUT_FIELD)
            .then(function(data){
                _.forEach(data.user.resultData.commonData,function(obj){
                    vm.pgmReutFieldList.push({v:obj.COMMON_INFO_VALUE2, k:obj.COMMON_INFO_VALUE1});
                })
                searchProgram();
            });
    }

    function getCommonValue(key){

        var objFind = _.find(vm.pgmReutFieldList, {k : key});
        if(objFind){
            return objFind.v;
        }
    }

    /**
     * 진행중인 프로그램 조회
     */
    function searchProgram(){
        var param = {};
        param.pgmEndDtmSet = 'Y';
        CompanySvc.selectPgmInfo(param)
            .then(function(data){
                logger.log('searchProgram success', data);

                vm.pgmList = [{v:'999',k:'프로그램 선택'}];
                vm.pgmBaseSeq = '999';
                _.forEach(data.user.resultData.pgmInfo,function(obj){
                    vm.pgmList.push({v:obj.PGM_BASE_SEQ+'', k:'['+ getCommonValue(obj.PGM_REUT_FIELD) +'] ' + obj.PGM_NAME});
                })

            })
            .catch(function(error){
                logger.log('fail');
                logger.error('message: ' + error + '\n');
            });
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 정상 종료
     */
    function confirm(){

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.pgmBaseSeq ,    '프로그램을 선택 해 주세요.', '999');
        chk = chk && ngspublic.nullCheck(vm.pgmRcutSeq ,   '모집정보를 선택 해 주세요.', '999');
        chk = chk && ngspublic.nullCheck(vm.pgmRcutSubSeq ,   '나이_성별을 선택 해 주세요.', '999');
        if(!chk) return false;

        if(!$message.confirm('출연 요청을 하시겠습니까?')){
            return false;
        }

        _.forEach(vm.memberList, function(obj, idx){

            var param = {};
            param.memberSeq = obj.MEMBER_SEQ;									//[필수] 요청 회원 일련번호
            param.rcutBaseSeq = vm.pgmRcutSeq;								//[필수] 요청 모집 일련번호
            param.rcutSubInfoSeq = vm.pgmRcutSubSeq;		//[필수] 요청 모집 일련번호


            CompanySvc.requestMember(param)
                .then(function(data){
                    if(idx == vm.memberList.length -1){
                        $message.alert('정상적으로 처리 되었습니다.');
                        $modalInstance.close();
                    }
                })
                .catch(function(error){

                });

        })
    }


}
