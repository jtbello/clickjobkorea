
angular
  .module('comp.mainc')
  .controller('cmPenaltyPCtrl', cmPenaltyPCtrl);

function cmPenaltyPCtrl($log, $rootScope, $scope, $modal, $modalInstance, $message, item, ComSvc, CompanySvc, ngspublic, comConstant) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        memberSeq : item,					// 회원정보
        penaltyCount : '999',
        companyStopInfo : '',
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        confirm : confirm
    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.memberSeq , '사용자 정보가 올바르지 않습니다.');
        if(!chk) {
            dismiss();
        }
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 정상 종료
     */
    function confirm(){

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.penaltyCount ,    '페널티 횟수를 선택 해 주세요.', '999');
        chk = chk && ngspublic.nullCheck(vm.companyStopInfo ,   '페널티 사유를 입력 해 주세요.');
        if(!chk) return false;

        var confirmResult = $message.confirm('페널티를 적용 하시겠습니까?');
        if (!confirmResult) return false;

        var param = {};
        param.memberSeq = vm.memberSeq;									//[필수] 요청 회원 일련번호
        param.companyStopInfo = vm.companyStopInfo;								//[필수] 패널티 사유
        param.penaltyCount = vm.penaltyCount;								    //[필수] 패널티 카운트

        CompanySvc.procPenalTy(param)
            .then(function(data){
                $message.alert('정상적으로 처리 되었습니다.');
                $modalInstance.close();
            })
            .catch(function(error){

            });

    }


}
