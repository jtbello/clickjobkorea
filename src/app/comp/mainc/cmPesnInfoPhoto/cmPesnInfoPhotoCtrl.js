angular.module('comp.mainc').controller('cmPesnInfoPhotoCtrl', cmPesnInfoPhotoCtrl);

function cmPesnInfoPhotoCtrl(
  $log,
  $scope,
  $modal,
  $state,
  $stateParams,
  $message
) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    prevViewId: '', // 이전화면 아이디
    memberPhotoAttachInfo: [], // 사진 리스트
    currentPage : 0

  });

  /**
   * Public methods
   */
  _.assign(vm, {
    showPage : showPage,        // 회원사진 팝업
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    

    if ($stateParams.param) {
        vm.memberPhotoAttachInfo = $stateParams.param.memberPhotoAttachInfo;
    }

  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

    /**
     * 사진 보기 이동
     */
    function showPage(setPage){
        vm.currentPage = setPage;
    }
}
