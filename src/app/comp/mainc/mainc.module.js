
angular
  .module('comp.mainc', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('comp.mainc', {
      url: '/mainc',
      abstract: false,
      template: '<div ui-view><h1>comp.mainc</h1></div>',
      controller: 'maincCtrl as mainc'
    })

    .state('comp.mainc.cmMain', {
      url: '/cmMain',
      templateUrl: 'mainc/cmMain/cmMain.tpl.html',
      data: { menuTitle : '기업회원 메인', menuId : 'cmMain', depth : 1},
      params: { param: null, backupData: null },
      controller: 'cmMainCtrl as cmMain'
    })
      .state('comp.mainc.cmPesnInfo', {
          url: '/cmPesnInfo',
          templateUrl: 'mainc/cmPesnInfo/cmPesnInfo.tpl.html',
          data: { menuTitle : '출연자 정보', menuId : 'cmPesnInfo', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cmPesnInfoCtrl as cmPesnInfo'
      })
      .state('comp.mainc.cmPesnInfoPhoto', {
          url: '/cmPesnInfoPhoto',
          templateUrl: 'mainc/cmPesnInfoPhoto/cmPesnInfoPhoto.tpl.html',
          data: { menuTitle : '출연자 사진 정보', menuId : 'cmPesnInfoPhoto', depth : 3},
          params: { param: null, backupData: null },
          controller: 'cmPesnInfoPhotoCtrl as cmPesnInfoPhoto'
      })
      .state('comp.mainc.cmPesnInfoVideo', {
          url: '/cmPesnInfoVideo',
          templateUrl: 'mainc/cmPesnInfoVideo/cmPesnInfoVideo.tpl.html',
          data: { menuTitle : '출연자 영상 정보', menuId : 'cmPesnInfoVideo', depth : 3},
          params: { param: null, backupData: null },
          controller: 'cmPesnInfoVideoCtrl as cmPesnInfoVideo'
      })
      .state('comp.mainc.cmPesnInfoCareer', {
          url: '/cmPesnInfoCareer',
          templateUrl: 'mainc/cmPesnInfoCareer/cmPesnInfoCareer.tpl.html',
          data: { menuTitle : '출연자 경력 정보', menuId : 'cmPesnInfoCareer', depth : 3},
          params: { param: null, backupData: null },
          controller: 'cmPesnInfoCareerCtrl as cmPesnInfoCareer'
      })
    ;

  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
