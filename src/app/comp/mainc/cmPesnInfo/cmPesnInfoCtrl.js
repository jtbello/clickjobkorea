angular.module('comp.mainc').controller('cmPesnInfoCtrl', cmPesnInfoCtrl);

function cmPesnInfoCtrl(
  $log,
  $scope,
  $modal,
  $state,
  $stateParams,
  $message,
  MetaSvc,
  CompanySvc,
  $filterSet,
  $rootScope
) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    prevViewId: '', // 이전화면 아이디
    rcutRequestMemeberInfo: {}, // 출연요청 출연자 정보
    memberInfo: {}, // 출연자 기본 정보

    memberSeq: '',

    companyAttachInfo: [],
    memberAminEtcInfo: [], //
    memberDetailInfo: {}, // 출연자 상세 정보
    memberCareerInfo: [], // 경력 리스트
    memberLatestInfo: [], // 최근 작업
    memberReviewInfo: [], // 평가

    noScheduleInfo: [], // 스케쥴 알림정보
    penaltyInfo: [], // 패널티 정보
    attendRecommendCnt : {}, // 도망 추천 횟수

    memberPhotoAttachInfo : [],   // 회원사진
    memberVideoAttachInfo : [],   // 회원동영상

    lastSchedulePushInfo: [] // 스케줄변경알림정보
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    selectMemberDetailInfo: selectMemberDetailInfo,
    procRequest: procRequest,
    showAttendEndPopup: showAttendEndPopup,
    showNoLeavePopup: showNoLeavePopup,
    showReviewPopup: showReviewPopup,
    showReqPushPopup: showReqPushPopup,
    showProgmSelectPopup: showProgmSelectPopup,
    showPenaltyPopup : showPenaltyPopup,
    handleMemberState: handleMemberState,
    cancelRequest: cancelRequest,
    setBkLst: setBkLst,
    viewPhotoPopup : viewPhotoPopup,        // 회원사진 팝업
    viewCommonVideo : viewCommonVideo,      // 회원동영상 팝업
    viewCareer : viewCareer,      // 경력보기
    attendProc : attendProc,
    viewPenalty : viewPenalty     // 패널티보기
  });


  /**
   * 사진 보기 이동
   */
  function viewPhotoPopup(){
      if(vm.memberPhotoAttachInfo.length <= 0){
          $message.alert('사진이 없습니다.');
          return false;
      }

    $rootScope.goView('comp.mainc.cmPesnInfoPhoto', vm, {memberPhotoAttachInfo : vm.memberPhotoAttachInfo});
  }

  /**
   * 동영상 보기 이동
   */
  function viewCommonVideo(){
      if(vm.memberVideoAttachInfo.length <= 0){
          $message.alert('영상이 없습니다.');
          return false;
      }

      $rootScope.goView('comp.mainc.cmPesnInfoVideo', vm, {memberVideoAttachInfo : vm.memberVideoAttachInfo});
  }

    /**
     * 경력 보기 이동
     */
    function viewCareer(){
       if(vm.memberCareerInfo.length <= 0){
         $message.alert('경력사항이 없습니다.');
         return false;
       }

        $rootScope.goView('comp.mainc.cmPesnInfoCareer', vm, {memberCareerInfo : vm.memberCareerInfo});
    }

    /**
     * 패널티보기
     */
    function viewPenalty(){

       var penalty = _.where(vm.penaltyInfo, {PENALTY_KIND : 'P1'});

       if(_.isEmpty(penalty)){
         $message.alert('패널티가 없습니다.');
         return false;
       }
       var msg = '';
       _.forEach(penalty, function(obj){
         msg += obj.PENALTY_NAME + '\n';
       });
      $message.alert(msg);
    }

  /**
   * Initialize
   */
  init();

  function init(reload) {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    if ($stateParams.backupData && !reload) {
      _.assign(vm, $stateParams.backupData);
    } else {
      vm.memberInfo = $stateParams.param.memberInfo;
      vm.rcutRequestMemeberInfo = $stateParams.param.rcutRequestMemeberInfo;
      vm.prevViewId = $stateParams.param.prevViewId;

      selectMemberDetailInfo();
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  /**
   * 출연자 상세조회
   */
  function selectMemberDetailInfo(){
    var param = {};
    if (
        vm.prevViewId === 'cmMain' ||
        vm.prevViewId === 'cmMngList.tab1' ||
        vm.prevViewId === 'cmMngList.tab2' ||
        vm.prevViewId === 'cmMngList.tab3' ||
        vm.prevViewId === 'cmSignupList.M1' ||
        vm.prevViewId === 'cmSignupList.M3' ||
        vm.prevViewId === 'cpPaymtInfo'
    ) {
      param.memberSeq = vm.memberSeq = vm.memberInfo.MEMBER_SEQ + '';
    } else {
      param.rcutBaseSeq = vm.rcutRequestMemeberInfo.RCUT_BASE_SEQ + '';
      param.memberSeq = vm.memberSeq = vm.rcutRequestMemeberInfo.MEMBER_SEQ + '';
    }

    CompanySvc.selectMemberDetailInfo(param)
        .then(function(data) {
          logger.log('selectMemberDetailInfo success', data);
          vm.companyAttachInfo = data.user.resultData.companyAttachInfo;
          vm.memberAminEtcInfo = data.user.resultData.memberAminEtcInfo;
          vm.memberDetailInfo = data.user.resultData.memberDetailInfo;
          vm.memberCareerInfo = data.user.resultData.memberCareerInfo;
          vm.memberLatestInfo = data.user.resultData.memberLatestInfo;
          vm.memberReviewInfo = data.user.resultData.memberReviewInfo;
          vm.memberPhotoAttachInfo = data.user.resultData.memberPhotoAttachInfo;
          vm.memberVideoAttachInfo = data.user.resultData.memberVideoAttachInfo;
          vm.noScheduleInfo = data.user.resultData.noScheduleInfo;
          vm.penaltyInfo = data.user.resultData.penaltyInfo;
          vm.attendRecommendCnt = data.user.resultData.attendRecommendCnt[0];


          // 의상정보
          vm.memberDetailInfo.dressInfo = '';
          try {
            if (Number(vm.memberDetailInfo.DRESS_BASE_COUNT) > 0)
              vm.memberDetailInfo.dressInfo += '기본정장  ';
          } catch (e) {}
          try {
            if (Number(vm.memberDetailInfo.DRESS_SEMI_COUNT) > 0)
              vm.memberDetailInfo.dressInfo += '세미정장  ';
          } catch (e) {}
          try {
            if (Number(vm.memberDetailInfo.DRESS_BLACK_SHOES) > 0)
              vm.memberDetailInfo.dressInfo += '검정구두 ';
          } catch (e) {}
          try {
            if (Number(vm.memberDetailInfo.DRESS_SWIMSUIT) > 0)
              vm.memberDetailInfo.dressInfo += '수영복 ';
          } catch (e) {}
          try {
            if (Number(vm.memberDetailInfo.DRESS_SCHOOL_UNIFORM) > 0)
              vm.memberDetailInfo.dressInfo += '교복 ';
          } catch (e) {}
          try {
            if (Number(vm.memberDetailInfo.DRESS_MILITARY_BOOTS) > 0)
              vm.memberDetailInfo.dressInfo += '군화 ';
          } catch (e) {}
          if (vm.memberDetailInfo.dressInfo === '')
            vm.memberDetailInfo.dressInfo = '없음';

          // 운전정보
          vm.memberDetailInfo.driveInfo = '';
          if (!_.isUndefined(vm.memberDetailInfo.DRIVE_CAR_YN) && vm.memberDetailInfo.DRIVE_CAR_YN !== 'D1')
            vm.memberDetailInfo.driveInfo += '자동차운전 ' + vm.memberDetailInfo.DRIVE_CAR_YN_NM + ', ';
          if (!_.isUndefined(vm.memberDetailInfo.DRIVE_MOTERCYCLE_YN) && vm.memberDetailInfo.DRIVE_MOTERCYCLE_YN !== 'D1')
            vm.memberDetailInfo.driveInfo += '오토바이운전 ' + vm.memberDetailInfo.DRIVE_MOTERCYCLE_YN_NM + ', ';
          if (!_.isUndefined(vm.memberDetailInfo.DRIVE_BICYCLE_YN) && vm.memberDetailInfo.DRIVE_BICYCLE_YN !== 'D1')
            vm.memberDetailInfo.driveInfo += '자전거운전 ' + vm.memberDetailInfo.DRIVE_BICYCLE_YN_NM + ' ';
          if (vm.memberDetailInfo.driveInfo === '')
            vm.memberDetailInfo.driveInfo = '안함';
        })
        .catch(function(error) {
          logger.log('fail');
          logger.error('message: ' + error + '\n');
        });
  }

  /**
   * 모집에 대한 요청 취소, 요청수락 ,수락취소
   */
  function procRequest(data, proKind) {
    
    var msg = '';
    if (proKind === 'P1') {
      // 요청취소건만 있는지 확인
      var chkFlag = true;
      if (data.REQUEST_KIND === 'R1' && data.REQUEST_STATE === 'R1') {
        chkFlag = true;
      } else {
        chkFlag = false;
      }

      if (!chkFlag) {
        $message.alert('요청 취소 대상만 선택 후 진행해주세요.');
        return false;
      }
      msg = '요청 취소 처리 하시겠습니까?';
    }
    if (proKind === 'P2') {
      var chkFlag = true;
      if (data.REQUEST_KIND === 'R2' && data.REQUEST_STATE === 'R1') {
        chkFlag = true;
      } else {
        chkFlag = false;
      }

      if (!chkFlag) {
        $message.alert('요청 수락 대상만 선택 후 진행해주세요.');
        return false;
      }

      msg = '요청 수락 처리 하시겠습니까?';
    }
    if (proKind === 'P3') {
      var chkFlag = true;
      if (data.REQUEST_STATE === 'R2') {
        chkFlag = true;
      } else {
        chkFlag = false;
      }

      // if (!chkFlag) {
      //   $message.alert('출연 수락 취소 대상만 선택 후 진행해주세요.');
      //   return false;
      // }
      msg = '출연 수락 취소 처리 하시겠습니까?';
    }

    var confirmResult = $message.confirm(msg);
    if (!confirmResult) return false;

    var param = {};
    param.proKind = proKind; //[필수] 처리종류 (P1: 요청 취소 , P2: 요청수락, P3: 수락취소)
    param.requestInfoSeq = data.REQUST_INFO_SEQ; //[필수] 모집신청일련번호

    CompanySvc.procRequest(param)
      .then(function(data) {
        $message.alert('처리 되었습니다.');
      })
      .catch(function(error) {
        logger.log('fail');
        logger.error('message: ' + error + '\n');
      });
  }

  /**
   * 출석 팝업
   */
  function showAttendEndPopup() {
    logger.log('showAttendEndPopup Open');

    var param = {
      requestInfoSeq: vm.rcutRequestMemeberInfo.REQUST_INFO_SEQ
    };

    //모달 오픈
    var modalInstance = $modal.open({
      templateUrl: 'progmc/cpAttendEndP/cpAttendEndP.tpl.html',
      controller: 'cpAttendEndPCtrl',
      controllerAs: 'cpAttendEndP',
      scope: $scope,
      resolve: {
        item: function() {
          return param;
        }
      }
    });

    modalInstance.opened
      .then(function() {
        // 모달 오픈 성공
      })
      .catch(function(data) {
        alert(data);
        // 모달 오픈 실패
      });

    modalInstance.result
      .then(function(result) {
        // 정상 종료
        logger.log('showAttendEndPopup Close', result);
        init(true);
      })
      .catch(function(reason) {
        // 취소
        logger.log('showAttendEndPopup Dismiss', reason);
      });
  }

  /**
   * 블랙리스트 등록 및 해제
   */
  function setBkLst(type) {

    var msg = '';
    if(type == 'I'){
       msg = '블랙리스트로 등록하시겠습니까?';
    }else{
       msg = '블랙리스트를 해제하시겠습니까?';
    }
    
    var confirmResult = $message.confirm(msg);
    if(!confirmResult) return false;
    
    var param = {};
    param.procKind = type;                         //[필수] (I : 등록, D : 삭제)
    param.memberSeq = vm.memberSeq;              //[필수] 회원 일련번호
    
    CompanySvc.setBkLst(param)
    .then(function(data){
      $message.alert('정상적으로  처리 되었습니다.');
      selectMemberDetailInfo();
    })
    .catch(function(err){
    })
  }

  /**
   * 미출석, 무단이탈 팝업
   */
  function showNoLeavePopup(attendNoLeaveState) {
    logger.log('showNoLeavePopup Open');

    var param = {};
    param.attendNoLeaveState = attendNoLeaveState; //[필수][공코:ATTEND_NO_LEAVE_STATE] 출석,지각 코드 ( 미출석 : A1, 무단이탈: A2)
    param.requestInfoSeqList = [vm.rcutRequestMemeberInfo.REQUST_INFO_SEQ];

    //모달 오픈
    var modalInstance = $modal.open({
      templateUrl: 'progmc/cpNoLeaveP/cpNoLeaveP.tpl.html',
      controller: 'cpNoLeavePCtrl',
      controllerAs: 'cpNoLeaveP',
      scope: $scope,
      resolve: {
        item: function() {
          return param;
        }
      }
    });

    modalInstance.opened
      .then(function() {
        // 모달 오픈 성공
      })
      .catch(function(data) {
        alert(data);
        // 모달 오픈 실패
      });

    modalInstance.result
      .then(function(result) {
        // 정상 종료
        logger.log('showNoLeavePopup Close', result);
        init(true);
      })
      .catch(function(reason) {
        // 취소
        logger.log('showNoLeavePopup Dismiss', reason);
      });
  }

  /**
   * 평가하기 팝업
   */
  function showReviewPopup() {
    logger.log('showReviewPopup Open');

    var param = {
      requestInfoSeq: vm.rcutRequestMemeberInfo.REQUST_INFO_SEQ
    };

    //모달 오픈
    var modalInstance = $modal.open({
      templateUrl: 'progmc/cpReviewP/cpReviewP.tpl.html',
      controller: 'cpReviewPCtrl',
      controllerAs: 'cpReviewP',
      scope: $scope,
      resolve: {
        item: function() {
          return param;
        }
      }
    });

    modalInstance.opened
      .then(function() {
        // 모달 오픈 성공
      })
      .catch(function(data) {
        alert(data);
        // 모달 오픈 실패
      });

    modalInstance.result
      .then(function(result) {
        // 정상 종료
        logger.log('showReviewPopup Close', result);
        init(true);
      })
      .catch(function(reason) {
        // 취소
        logger.log('showReviewPopup Dismiss', reason);
      });
  }

  /**
   * 알림발송 팝업
   */
  function showReqPushPopup() {
    logger.log('showReqPushPopup Open');

    //모달 오픈
    var modalInstance = $modal.open({
      templateUrl: 'progmc/cpReqPushP/cpReqPushP.tpl.html',
      controller: 'cpReqPushPCtrl',
      controllerAs: 'cpReqPushP',
      scope: $scope,
      resolve: {
        item: function() {
          return;
        }
      }
    });

    modalInstance.opened
      .then(function() {
        // 모달 오픈 성공
      })
      .catch(function(data) {
        alert(data);
        // 모달 오픈 실패
      });

    modalInstance.result
      .then(function(result) {
        // 정상 종료
        logger.log('showReqPushPopup Close', result.msg);

        $rootScope
          .comPushSend([vm.rcutRequestMemeberInfo], result.msg, vm.rcutRequestMemeberInfo.RCUT_BASE_SEQ + '')
          .then(function(data) {})
          .catch(function(err) {});
      })
      .catch(function(reason) {
        // 취소
        logger.log('showReqPushPopup Dismiss', reason);
      });
  }

  /**
   * 출연요청 팝업
   */
  function showProgmSelectPopup() {
    logger.log('showProgmSelectPopup Open');

    //모달 오픈
    var modalInstance = $modal.open({
      templateUrl: 'mainc/cmMainReqP/cmMainReqP.tpl.html',
      controller: 'cmMainReqPCtrl',
      controllerAs: 'cmMainReqP',
      scope: $scope,
      resolve: {
        item: function() {
          return [vm.memberInfo];
        }
      }
    });

    modalInstance.opened
      .then(function() {
        // 모달 오픈 성공
      })
      .catch(function(data) {
        alert(data);
        // 모달 오픈 실패
      });

    modalInstance.result
      .then(function(result) {
        // 정상 종료
        logger.log('showProgmSelectPopup Close', result);
      })
      .catch(function(reason) {
        // 취소
        logger.log('showProgmSelectPopup Dismiss', reason);
      });
  }

  /**
   * 페널티 부여 팝업
   */
  function showPenaltyPopup() {
    logger.log('showPenaltyPopup Open');

    //모달 오픈
    var modalInstance = $modal.open({
      templateUrl: 'mainc/cmPenaltyP/cmPenaltyP.tpl.html',
      controller: 'cmPenaltyPCtrl',
      controllerAs: 'cmPenaltyP',
      scope: $scope,
      resolve: {
        item: function() {
          return vm.memberSeq;
        }
      }
    });

    modalInstance.opened
      .then(function() {
        // 모달 오픈 성공
      })
      .catch(function(data) {
        alert(data);
        // 모달 오픈 실패
      });

    modalInstance.result
      .then(function(result) {
        // 정상 종료
        selectMemberDetailInfo();
        logger.log('showPenaltyPopup Close', result);
      })
      .catch(function(reason) {
        // 취소
        logger.log('showPenaltyPopup Dismiss', reason);
      });
  }

  /**
   * 회원가입 승인 / 반려
   */

  function handleMemberState(nextState) {
    var nextStateText =
      nextState === 'M2' ? '승인' : nextState === 'M3' && '반려';

    var confirmed = $message.confirm(`회원가입 ${nextStateText} 하시겠습니까?`);

    if (confirmed) {
      var param = {
        memberSeq: vm.memberInfo.MEMBER_SEQ,
        memberState: nextState
      };

      CompanySvc.procJoin(param)
        .then(function(resultData) {
          $message.alert(`회원가입 ${nextStateText} 되었습니다.`);
          logger.log('handleMemberState[SUCCESS]', 'resultData: ', resultData);
        })
        .catch(function(error) {
          logger.log('handleMemberState[FAILED]', 'error: ', error);
        });
    }
  }

  /**
   * 출연 요청 취소
   */

  function cancelRequest() {
    console.log('cancelRequest()', 'vm.memberInfo: ', vm.memberInfo);
  }
  
  /**
   * 출석 종료
   */
  function attendProc(kind){
    
    $rootScope.requestCount++;
    $rootScope.searchAddrByGeocode()
    .then(function(data){
      $rootScope.requestCount--;
      logger.debug('searchAddrByGeocode', data);
      if(data.status.code === 0){
        let region = data.results[0].region;    // 현재 주소
        let point = region.area1.name + ' ' + region.area2.name + ' ' + region.area3.name;
        
        var msg = '';
        if(kind == 'A1'){
          msg = '출석(지각) 처리하시겠습니까?';
        }else if(kind == 'A2'){
          msg = '출석 처리하시겠습니까?';
        }else{
          msg = '종료 처리하시겠습니까?';
        }
      
        var confirmResult = $message.confirm(msg);
        if (!confirmResult) return false;
      
        var param = {};
        if(kind == 'A1'){
          param.attendNoState = "A3";                                                       //[필수][공코:ATTEND_NO_STATE] 출석,지각 코드 ( 출석,종료 : A1, 지각: A3)
          param.companyMemberAttendDtm = moment().format('YYYY-MM-DD HH:mm:ss');            //[필수] 출석일시 [일시 패턴 : 0000-00-00 00:00:00]
          param.companyMemberAttendPoint = point;                                           //[필수] 출석장소
        }else if(kind == 'A2'){
          param.attendNoState = "A1";                                                       //[필수][공코:ATTEND_NO_STATE] 출석,지각 코드 ( 출석,종료 : A1, 지각: A3)
          param.companyMemberAttendDtm = moment().format('YYYY-MM-DD HH:mm:ss');            //[필수] 출석일시 [일시 패턴 : 0000-00-00 00:00:00]
          param.companyMemberAttendPoint = point;                                           //[필수] 출석장소
        }else{
          param.attendNoState = "A1";                                                       //[필수][공코:ATTEND_NO_STATE] 출석,지각 코드 ( 출석,종료 : A1, 지각: A3)
          param.companyMemberEndDtm = moment().format('YYYY-MM-DD HH:mm:ss');               //[필수] 종료일시 [일시 패턴 : 0000-00-00 00:00:00]
          param.companyMemberEndPoint = point;                                              //[필수] 종료장소
        }
        param.requestInfoSeq = vm.rcutRequestMemeberInfo.REQUST_INFO_SEQ+'';                //[필수] 모집신청일련번호


        CompanySvc.updateAttendEndInfo(param)
            .then(function(data){
                $message.alert('정상적으로 처리 되었습니다.');
                init(true);
            })
            .catch(function(err){
            })
        
      }else{
        $message.alert('GPS상태 및 권한을 확인해주세요.');
      }     

    })
    .catch(function(d){
      logger.debug('searchAddrByGeocode err', d);
      $message.alert(d.message);
      return false;
    });
      
  }
  
  
  
}
