
angular
  .module('comp.mypagec')
  .controller('cmInfoModCtrl', cmInfoModCtrl);

function cmInfoModCtrl($log, $scope, $modal, CompanySvc, ComSvc, ngspublic, $message, $filterSet, $rootScope, $env) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    bankNameInfo  : [],
    companyCeoInfo : {},
    pwd : '',
    pwdChk : '',
    companyInfo : '',
    
    
    photofileDisabled1 : false,     // 사진 첨부파일 1 파일선택가능
    photopgmImgAttachSeq1 : '',     // 사진 첨부파일 1 일련번호
    photofileOrgName1 : '',       // 사진 첨부파일 1 원본파일이름

    photofileDisabled2 : false,     // 사진 첨부파일 2 파일선택가능
    photopgmImgAttachSeq2 : '',     // 사진 첨부파일 2 일련번호
    photofileOrgName2 : '',       // 사진 첨부파일 2 원본파일이름

    photofileDisabled3 : false,     // 사진 첨부파일 3 파일선택가능
    photopgmImgAttachSeq3 : '',     // 사진 첨부파일 3 일련번호
    photofileOrgName3 : '',       // 사진 첨부파일 3 원본파일이름

    photofileDisabled4 : false,     // 사진 첨부파일 4 파일선택가능
    photopgmImgAttachSeq4 : '',     // 사진 첨부파일 4 일련번호
    photofileOrgName4 : '',       // 사진 첨부파일 4 원본파일이름
    
    photofileDisabled5 : false,     // 사진 첨부파일 5 파일선택가능
    photopgmImgAttachSeq5 : '',     // 사진 첨부파일 5 일련번호
    photofileOrgName5 : '',       // 사진 첨부파일 5 원본파일이름
    
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    fileAdd : fileAdd,                  // 파일 추가
    fileAttach : fileAttach,            // 파일 전송
    fileDel : fileDel,                  // 파일 삭제
    modifyMem : modifyMem,              // 정보 수정
  });
  
  //프로그램 분야에 대한 프로그램 조회
  vm.readfile1First = true;
  $scope.$watch('cmInfoMod.readfile1', function(newVal, oldVal, scope) {
    
    
    if(undefined === newVal || "" === newVal){
          return false;
      } else {
        
        if(vm.readfile1First){
          vm.readfile1First = false;
          return false;
        }
        
          vm.photofileOrgName1 = newVal.name;
      }
      fileAttach('file1');
  });

  vm.readfile2First = true;
  $scope.$watch('cmInfoMod.readfile2', function(newVal, oldVal, scope) {
    
     
    
      if(undefined === newVal || "" === newVal){
        return false;
      } else {
        
        if(vm.readfile2First){
          vm.readfile2First = false;
          return false;
         }
        
          vm.photofileOrgName2 = newVal.name;
      }
      fileAttach('file2');
  });

  vm.readfile3First = true;
  $scope.$watch('cmInfoMod.readfile3', function(newVal, oldVal, scope) {
      
      if(undefined === newVal || "" === newVal){
        return false;
      } else {
        
        if(vm.readfile3First){
          vm.readfile3First = false;
          return false;
        }
          vm.photofileOrgName3 = newVal.name;
      }
      fileAttach('file3');
  });

  vm.readfile4First = true;
  $scope.$watch('cmInfoMod.readfile4', function(newVal, oldVal, scope) {
      
      if(undefined === newVal || "" === newVal){
        return false;
      } else {
        
        if(vm.readfile4First){
          vm.readfile4First = false;
          return false;
        }
          vm.photofileOrgName4 = newVal.name;
      }
      fileAttach('file4');
  });
  
  vm.readfile5First = true;
  $scope.$watch('cmInfoMod.readfile5', function(newVal, oldVal, scope) {
    
    if(undefined === newVal || "" === newVal){
      return false;
    } else {
      
      if(vm.readfile5First){
        vm.readfile5First = false;
        return false;
      }
        vm.photofileOrgName5 = newVal.name;
    }
    fileAttach('file5');
  });

  
  
  /**
   * 파일 추가
   */
  function fileAdd(kind){
      angular.element('#'+kind).click();
  }

  /**
   * 파일 삭제
   */
  function fileDel(kind){
    
      var chk = ngspublic.nullCheck(vm['photopgmImgAttachSeq' + kind.replace('file','')],       '파일정보가 없습니다.');
      if(!chk){
          $('#'+kind).val('');
          vm['photopgmImgAttachSeq' + kind.replace('file','')] = '';
          vm['photofileDisabled' + kind.replace('file','')] = false;
          return false;
      }

      var confirmResult = $message.confirm('정말 삭제하시겠습니까?');
      if(!confirmResult) return false;

      var param = {};
      
      vm['photofileDisabled' + kind.replace('file','')] = false;
      $("#"+kind).val("");
      param.attachSeq = vm['photopgmImgAttachSeq' + kind.replace('file','')];     // [필수]파일등록 후 결과값에 파일 일련번호
      param.fileOrgName = vm['photofileOrgName' + kind.replace('file','')];       // [필수]파일등록 후 결과값에 파일 원본파일이름
      
      ComSvc.fileDelete(param)
          .then(function(data){
              $message.alert('정상적으로 삭제되었습니다.');
              
              vm['photopgmImgAttachSeq' + kind.replace('file','')] = '';
              vm['photofileOrgName' + kind.replace('file','')] = '';
              vm['readfile' + kind.replace('file','')] = '';
              
              $("#form"+kind).get(0).reset();
          })
          .catch(function(err){
              vm['photopgmImgAttachSeq' + kind.replace('file','')] = '';
              vm['photofileOrgName' + kind.replace('file','')] = '';
              vm['readfile' + kind.replace('file','')] = '';
              
              $("#form"+kind).get(0).reset();
                logger.error('message: ' + err + '\n');
          })
  }

  /**
   * 파일전송
   */
  function fileAttach(kind){
      vm['photofileDisabled' + kind.replace('file','')] = true;

      $rootScope.safeApply(function() {
        $rootScope.requestCount++;
      });
      
      $("#form"+kind).ajaxForm({
          url : $env.endPoint.service + "/api/com/fileUpload.login",
          enctype : "multipart/form-data",
          dataType : "json",
          beforeSubmit: function (data, frm, opt) {
              var objFind = _.find(data, {type : 'file'});
              if(objFind){
                  if(!_.isNull(objFind.value) && objFind.value != ""){
                      return true;
                  }
              }
              
              $rootScope.requestCount--;
              vm['photofileDisabled' + kind.replace('file','')] = false;
              $message.alert('파일 선택 후 진행 해주세요.');
              return false;
          },
          error : function(){
              $scope.$apply(function(){
                
                  vm['photofileDisabled' + kind.replace('file','')] = false;
                  vm['photofileOrgName' + kind.replace('file','')] = '';
                
                  $rootScope.requestCount--;
                  $message.alert("파일 전송 실패") ;
              });
          },
          success : function(result){
              $scope.$apply(function(){
                  $rootScope.requestCount--;
                  
                  if(result.user.result == 'Fail'){
                      $message.alert(result.user.resultMessage);
                      return false;
                  }
                  
                  vm['photopgmImgAttachSeq' + kind.replace('file','')] = result.user.resultData.attachSeq;
                  vm['photofileOrgName' + kind.replace('file','')] = result.user.resultData.orgName;

                  $message.alert("파일 전송 성공");
              });
          }
      });

      $("#form"+kind).submit();
  }

  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    vm.emailDomainOptions = [
                             { value: '999', text: '선택' },
                             { value: 'naver.com', text: 'naver.com' },
                             { value: 'hanmail.net', text: 'hanmail.net' },
                             { value: 'gmail.com', text: 'gmail.com' },
                             { value: 'hotmail.com', text: 'hotmail.com' },
                             { value: 'nate.com', text: 'nate.com' },
                             { value: '0', text: '직접입력' }
                           ];
    
    
    // 은행명 공통 코드 조회
    ComSvc.selectCommonInfo('BANK_NAME')
      .then(function(data){
        vm.bankNameInfo = data.user.resultData.commonData;
        
        searchFn();
      })
    
   
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  /**
   * 나의 정보 조회
   * @returns
   */
  function searchFn(){
    
    var param = {};
    CompanySvc.selectCompanyCeoInfo(param)
    .then(function(data){
      if(data.user.resultData.companyCeoInfo.length > 0){
        vm.companyCeoInfo = data.user.resultData.companyCeoInfo[0];
        
        if(_.isBlank(vm.companyCeoInfo.COMPANY_CEO_HP_KIND)) vm.companyCeoInfo.COMPANY_CEO_HP_KIND = '999';
        if(_.isBlank(vm.companyCeoInfo.ACC_BANK_NAME)){
          vm.companyCeoInfo.ACC_BANK_NAME = '999';
        }else{
          vm.companyCeoInfo.ACC_BANK_NAME = _.find(vm.bankNameInfo, {COMMON_INFO_VALUE2 : vm.companyCeoInfo.ACC_BANK_NAME}).COMMON_INFO_VALUE1;
        }

        vm.companyInfo = vm.companyCeoInfo.COMPANY_INFO;

        // 이메일 정보
        vm.companyEmail1 = ''
        vm.companyEmail2 = ''
        vm.companyEmail2Etc = '';
        if(!_.isBlank(vm.companyCeoInfo.COMPANY_EMAIL)){
          var email = vm.companyCeoInfo.COMPANY_EMAIL.split('@');
          vm.companyEmail1 = email[0];
          
          var findEmailDomain = _.find(vm.emailDomainOptions , {text : email[1]});
          if(!findEmailDomain){
            vm.companyEmail2Etc = email[1];
            vm.companyEmail2 = '0';
          }else{
            vm.companyEmail2Etc = '';
            vm.companyEmail2 = findEmailDomain.value;
          }
        }
        
        // 파일 정보 세팅
        if(!_.isBlank(vm.companyCeoInfo.COMPANY_NO_ATTACH_WEB_PATH)){
          vm.photofileDisabled1 =  true,                                                 // 사진 첨부파일 1 파일선택가능
          vm.photopgmImgAttachSeq1 = vm.companyCeoInfo.COMPANY_NO_ATTACH_SEQ,            // 사진 첨부파일 1 일련번호
          vm.photofileOrgName1 = vm.companyCeoInfo.COMPANY_NO_ATTACH_FILE_ORG_NAME,      // 사진 첨부파일 1 원본파일이름
          vm.readfile1 = vm.photofileOrgName1;
        }
        
        if(!_.isBlank(vm.companyCeoInfo.COMPANY_ACC_ATTACH_WEB_PATH)){
          vm.photofileDisabled2 =  true,                                                  // 사진 첨부파일 1 파일선택가능
          vm.photopgmImgAttachSeq2 = vm.companyCeoInfo.COMPANY_ACC_ATTACH_SEQ,            // 사진 첨부파일 1 일련번호
          vm.photofileOrgName2 = vm.companyCeoInfo.COMPANY_ACC_ATTACH_FILE_ORG_NAME,      // 사진 첨부파일 1 원본파일이름
          vm.readfile2 = vm.photofileOrgName2;
        }
        
        if(!_.isBlank(vm.companyCeoInfo.COMPANY_PERMISSION_ATTACH_WEB_PATH)){
          vm.photofileDisabled3 =  true,                                                         // 사진 첨부파일 1 파일선택가능
          vm.photopgmImgAttachSeq3 = vm.companyCeoInfo.COMPANY_PERMISSION_ATTACH_SEQ,            // 사진 첨부파일 1 일련번호
          vm.photofileOrgName3 = vm.companyCeoInfo.COMPANY_PERMISSION_ATTACH_FILE_ORG_NAME,      // 사진 첨부파일 1 원본파일이름
          vm.readfile3 = vm.photofileOrgName3;
        }
        
        if(!_.isBlank(vm.companyCeoInfo.COMPANY_JOB_PROVE_ATTACH_WEB_PATH)){
          vm.photofileDisabled4 =  true,                                                  // 사진 첨부파일 1 파일선택가능
          vm.photopgmImgAttachSeq4 = vm.companyCeoInfo.COMPANY_JOB_PROVE_ATTACH_SEQ,            // 사진 첨부파일 1 일련번호
          vm.photofileOrgName4 = vm.companyCeoInfo.COMPANY_JOB_PROVE_ATTACH_FILE_ORG_NAME,      // 사진 첨부파일 1 원본파일이름
          vm.readfile4 = vm.photofileOrgName4;
        }
        
        if(!_.isBlank(vm.companyCeoInfo.COMPANY_ETC_ATTACH_WEB_PATH)){
          vm.photofileDisabled5 =  true,                                                  // 사진 첨부파일 1 파일선택가능
          vm.photopgmImgAttachSeq5 = vm.companyCeoInfo.COMPANY_ETC_ATTACH_SEQ,            // 사진 첨부파일 1 일련번호
          vm.photofileOrgName5 = vm.companyCeoInfo.COMPANY_ETC_ATTACH_FILE_ORG_NAME,      // 사진 첨부파일 1 원본파일이름
          vm.readfile5 = vm.photofileOrgName5;
        }
        
      }
    })
    .catch(function(err){
    })
    
  }
  
  
  /**
   * 수정
   */
  function modifyMem(){
   
    var chk = true;
    
    chk = chk && ngspublic.nullCheck(vm.companyCeoInfo.COMPANY_CEO_HP_KIND,     '대표자 연락처 회사 종류를 선택해주세요.', '999');
    chk = chk && ngspublic.nullCheck(vm.companyCeoInfo.COMPANY_CEO_HP,          '대표자 연락처를 입력해주세요.');
    chk = chk && ngspublic.nullCheck(vm.companyCeoInfo.COMPANY_MEMBER_NAME,     '담당자명을 입력해주세요.');
    chk = chk && ngspublic.nullCheck(vm.companyCeoInfo.COMPANY_NO,              '사업자번호를 입력해주세요.');
    
    if(!_.isBlank(vm.pwd)){
      if(chk && vm.pwd.length <= 4){
        $message.alert('패스워드를 4자 이상입력해주세요.');
        return false;
      }
      if(chk && vm.pwd != vm.pwdChk){
        $message.alert('패스워드확인이 정확하지않습니다.');
        return false;
      }
    }
    chk = chk && ngspublic.nullCheck(vm.companyCeoInfo.COMPANY_TEL1,       '연락처1을 입력해주세요.');
    chk = chk && ngspublic.nullCheck(vm.companyEmail1,      '이메일을 입력해주세요.');
    if(vm.companyEmail2 != '0'){
      chk = chk && ngspublic.nullCheck(vm.companyEmail2,      '이메일을 입력해주세요.');
    }else{
      chk = chk && ngspublic.nullCheck(vm.companyEmail2Etc,      '이메일을 입력해주세요.');
    }
    if(!chk) return false;
    
    
    // 이메일 체크
    var tempEmail = '';
    if(vm.companyEmail != ''){
      if(vm.companyEmail2 != '0'){
        tempEmail = vm.companyEmail1 + '@' + vm.companyEmail2;
      }else{
        tempEmail = vm.companyEmail1 + '@' + vm.companyEmail2Etc;
      }
      if(!$filterSet.isEmail(tempEmail)){
        $message.alert('이메일을 정확하게 입력해주세요.');
        return false;
      }
    }
    vm.companyCeoInfo.COMPANY_EMAIL = tempEmail;
    
    var confirmResult = $message.confirm('수정 하시겠습니까?');
    if(!confirmResult) return false;
    
    if(vm.companyCeoInfo.COMPANY_CEO_HP_KIND == '999')  vm.companyCeoInfo.COMPANY_CEO_HP_KIND = '';
    if(vm.companyCeoInfo.ACC_BANK_NAME == '999'){
      vm.companyCeoInfo.ACC_BANK_NAME = '';
    }else{
      vm.companyCeoInfo.ACC_BANK_NAME = _.find(vm.bankNameInfo, {COMMON_INFO_VALUE1 : vm.companyCeoInfo.ACC_BANK_NAME}).COMMON_INFO_VALUE2;
    }
    
    // 파일
    if(!_.isBlank(vm.photopgmImgAttachSeq1)){
      vm.companyCeoInfo.COMPANY_NO_ATTACH_SEQ = vm.photopgmImgAttachSeq1;
    }else{
      vm.companyCeoInfo.COMPANY_NO_ATTACH_SEQ = '';
    }
    if(!_.isBlank(vm.photopgmImgAttachSeq2)){
      vm.companyCeoInfo.COMPANY_ACC_ATTACH_SEQ = vm.photopgmImgAttachSeq2;
    }else{
      vm.companyCeoInfo.COMPANY_ACC_ATTACH_SEQ = '';
    }
    if(!_.isBlank(vm.photopgmImgAttachSeq3)){
      vm.companyCeoInfo.COMPANY_PERMISSION_ATTACH_SEQ = vm.photopgmImgAttachSeq3;
    }else{
      vm.companyCeoInfo.COMPANY_PERMISSION_ATTACH_SEQ = '';
    }
    if(!_.isBlank(vm.photopgmImgAttachSeq4)){
      vm.companyCeoInfo.COMPANY_JOB_PROVE_ATTACH_SEQ = vm.photopgmImgAttachSeq4;
    }else{
      vm.companyCeoInfo.COMPANY_JOB_PROVE_ATTACH_SEQ = ''
    }
    if(!_.isBlank(vm.photopgmImgAttachSeq5)){
      vm.companyCeoInfo.COMPANY_ETC_ATTACH_SEQ = vm.photopgmImgAttachSeq5;
    }else{
      vm.companyCeoInfo.COMPANY_ETC_ATTACH_SEQ = '';
    }
    
    
    
    var param = {};
    param.companyInfo = vm.companyInfo;                                                       //[선택] 회사소개
    param.companyNoAttachSeq = vm.companyCeoInfo.COMPANY_NO_ATTACH_SEQ;                       //[선택] 사업자등록증첨부번호
    param.companyAccAttachSeq = vm.companyCeoInfo.COMPANY_ACC_ATTACH_SEQ;                     //[선택] 통장사본첨부번호
    param.companyPermissionAttachSeq = vm.companyCeoInfo.COMPANY_PERMISSION_ATTACH_SEQ;       //[선택] 대중문화예술기획업 허가증 첨부번호
    param.companyJobProveAttachSeq = vm.companyCeoInfo.COMPANY_JOB_PROVE_ATTACH_SEQ;          //[선택] 직업소개소 증빙자료 첨부번호
    param.companyEtcAttachSeq = vm.companyCeoInfo.COMPANY_ETC_ATTACH_SEQ;                     //[선택] 기타증빙서류첨부번호
    param.accNo = vm.companyCeoInfo.ACC_NO;                                                   //[선택]계좌번호('-'제거)
    param.accBankName = vm.companyCeoInfo.ACC_BANK_NAME;                                      //[선택][공코 : BANK_NAME]계좌은행명
    param.accName = vm.companyCeoInfo.ACC_NAME;                                               //[선택]계좌예금주
    
    param.companyTel1 = vm.companyCeoInfo.COMPANY_TEL1;                                       //[선택] 연락처1
    param.companyTel2 = vm.companyCeoInfo.COMPANY_TEL2;                                       //[선택] 연락처2
    param.companyEmail = vm.companyCeoInfo.COMPANY_EMAIL;                                     //[선택] 회사(담당자) 이메일 주소 
    if(!_.isBlank(vm.pwd)){
    param.companyMemberPwd= vm.pwd;                                                           //[선택] 대표담당자패스워드
    }
    
    param.companyCeoHpKind = vm.companyCeoInfo.COMPANY_CEO_HP_KIND;
    param.companyCeoHp = vm.companyCeoInfo.COMPANY_CEO_HP;
    param.companyNo = vm.companyCeoInfo.COMPANY_NO;
    param.companyMemberName = vm.companyCeoInfo.COMPANY_MEMBER_NAME;
    
    CompanySvc.updateCompanyCeoInfo(param)
    .then(function(data){
      $message.alert('정상적으로 수정되었습니다. 메인화면으로 이동합니다.');
      vm.readfile1First = true;
      vm.readfile2First = true;
      vm.readfile3First = true;
      vm.readfile4First = true;
      vm.readfile5First = true;
      // searchFn();
      $rootScope.goView('comp.mainc.cmMain');
    })
    .catch(function(err){
    })
    
    
    
  }
  
}
