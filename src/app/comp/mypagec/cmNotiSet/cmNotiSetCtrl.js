
angular
  .module('comp.mypagec')
  .controller('cmNotiSetCtrl', cmNotiSetCtrl);

function cmNotiSetCtrl($log, $scope, $modal, maConstant, $user, MemberSvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    gpsAgree : 'N',
    pushAgree : 'N'
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    setGps : setGps,
    setPush : setPush,
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    searchFn();
    
    $(".gpsAccept .btnStyle02").on("click", function(){
      $(".gpsAccept input").click();
      $(this).removeClass("active");
      $(".gpsAccept .btnStyle01").addClass("active");
    });

    $(".gpsAccept .btnStyle01").on("click", function(){
      $(".gpsAccept input").click();
      $(this).removeClass("active");
      $(".gpsAccept .btnStyle02").addClass("active");
    });

    /* push */
    $(".pushAccept .btnStyle02").on("click", function(){
      $(".pushAccept input").click();
      $(this).removeClass("active");
      $(".pushAccept .btnStyle01").addClass("active");
    });

    $(".pushAccept .btnStyle01").on("click", function(){
      $(".pushAccept input").click();
      $(this).removeClass("active");
      $(".pushAccept .btnStyle02").addClass("active");
    });
    
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  /**
   * GPS 정보 동의 세팅
   */
  function setGps() {
    
    var param = {};
    param.busnLinkKey = $user.currentUser.getUserInfoList()[0].COMPANY_SEQ;      // 회원,기업 일련번호
    param.agreeKind = 'A2';                                                      // 정보 동의 종류[공코:AGREE_KIND]
    param.agreeYn = vm.gpsAgree;                                                     // 동의 여부
    
    MemberSvc.insertAgree(param)
    .then(function(data){
      searchFn();
    })
    .catch(function(err){
      searchFn();
    })
    
  }
  
  /**
   * PUSH 정보 동의 세팅
   */
  function setPush() {
    
    var param = {};
    param.busnLinkKey = $user.currentUser.getUserInfoList()[0].COMPANY_SEQ;      // 회원,기업 일련번호
    param.agreeKind = 'A3';                                                      // 정보 동의 종류[공코:AGREE_KIND]
    param.agreeYn = vm.pushAgree;                                                     // 동의 여부
    
    MemberSvc.insertAgree(param)
    .then(function(data){
      searchFn();
    })
    .catch(function(err){
      searchFn();
    })
    
  }
  
  /**
   * 정보 동의 조회
   * @returns
   */
  function searchFn(){
    
    var param = {};
    MemberSvc.selectAgreeInfo(param)
    .then(function(data){
      if(data.user.resultData.agreeInfo.length > 0){
        // GPS 정보
        var findGpsInfo = _.find(data.user.resultData.agreeInfo, {AGREE_KIND : 'A2'});
        if(findGpsInfo){
          if(findGpsInfo.AGREE_YN == 'Y'){
            vm.gpsAgree = 'Y';
          }else{
            vm.gpsAgree = 'N';
          }
        }
        
        // PUSH 정보
        var findGpsInfo = _.find(data.user.resultData.agreeInfo, {AGREE_KIND : 'A3'});
        if(findGpsInfo){
          if(findGpsInfo.AGREE_YN == 'Y'){
            vm.pushAgree = 'Y';
          }else{
            vm.pushAgree = 'N';
          }
        }
      }
    })
    .catch(function(err){
    })
    
  }
  
  
}
