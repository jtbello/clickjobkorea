
angular
  .module('comp.mypagec')
  .controller('mypagecCtrl', mypagecCtrl);

function mypagecCtrl($log, $scope) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

}
