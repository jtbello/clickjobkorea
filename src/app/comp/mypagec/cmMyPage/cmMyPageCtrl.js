
angular
  .module('comp.mypagec')
  .controller('cmMyPageCtrl', cmMyPageCtrl);

function cmMyPageCtrl($log, $scope, $modal, maConstant, $message, MemberSvc, $user, $rootScope) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    goPage : goPage,
    goPageInfoMod : goPageInfoMod,
    logOut : logOut
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  function goPage(info){
    $rootScope.goView(info);
  }

  function goPageInfoMod(){
    if($user.currentUser.getUserInfoList()[0].CEO_YN == 'Y'){
        $rootScope.goView('comp.mypagec.cmInfoMod');
    }else{
        var params = { companyMemberSeq: $user.currentUser.getUserInfoList()[0].COMPANY_MEMBER_SEQ };
        $rootScope.goView('comp.authc.caPesnMod', vm, params);
    }
  }
  
  function logOut(){
    
    if($message.confirm('로그아웃 하시겠습니까?')){

      MemberSvc.logOut()
          .then(function(data){
              $message.alert('로그아웃 되었습니다.');
              $user.destroy();
              $rootScope.goView('main.html#/main');
          })
          .catch(function(err){
              $message.alert(err);
              $user.destroy();
              $rootScope.goView('main.html#/main');
          });
    }
    
  }
  
  
}
