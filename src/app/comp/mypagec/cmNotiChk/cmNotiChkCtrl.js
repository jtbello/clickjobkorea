
angular
  .module('comp.mypagec')
  .controller('cmNotiChkCtrl', cmNotiChkCtrl);

function cmNotiChkCtrl($log, $scope, $modal, maConstant, CompanySvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    
    messageType : '999',
    rcutBaseSeq : '999',
    pgmList : [{v:'999',k:'프로그램 선택'}],
    
    // 페이징 관련
    curPage : 1,              // 현재페이지
    viewPageCnt : 10,         // 페이지당보여질수
    
    
    requstPushList :[],       // 조회결과
    requstPushListCount : 0,  // 총건수
    
    showShowMoreButton: true, // 더보기 버튼 표시
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    searchFn : searchFn,
    searchMore : searchMore
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    searchProgram();
    
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  function searchFn(flag){
    
    if(flag){
      vm.curPage = 1;
      vm.requstPushList = [];
      vm.requstPushListCount = 0;
    }
    
    var param = {};
    param.startNo = (vm.curPage -1) * vm.viewPageCnt;                                 //[필수] (현재페이지 -1) * 페이지당보여질수 
    param.endNo = vm.viewPageCnt;                                                     //[필수] 페이징 페이지당보여질수
    
    param.messageType = vm.messageType == '999' ? '' : vm.messageType;                //[선택][공코: MESSAGE_TYPE] 메시지타입
    param.rcutBaseSeq = vm.rcutBaseSeq == '999' ? '' : vm.rcutBaseSeq;;               //[선택] 출연자 모집일련번호
    
    CompanySvc.selectRequstPushList(param)
      .then(function(data){
        
          if(data.user.resultData.requstPushList.length > 0){
            
            vm.requstPushList = vm.requstPushList.concat(data.user.resultData.requstPushList);
            vm.requstPushListCount = data.user.resultData.requstPushListCount;
            
            // 리스트에 추가할 항목이 없을 경우
            if (data.user.resultData.requstPushList.length === 0 && vm.curPage > 1) {
              vm.curPage--;
              // 기존 리스트에 항목이 추가됐을 경우
              // or 리스트에 처음 항목이 추가됐을 경우
            }
            
          }else{
            vm.requstPushList = [];
            vm.requstPushListCount = 0;
          }
          
          // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
          if (data.user.resultData.requstPushList.length < vm.viewPageCnt) {
            vm.showShowMoreButton = false;
          }else{
            vm.showShowMoreButton = true;
          }
        
      })
    
  }
  
  /**
   * 진행중인 프로그램 조회
   */
  function searchProgram(){
      var param = {};
      param.startNo = 0;                      //[필수] (현재페이지 -1) * 페이지당보여질수 
      param.endNo = 99999;                    //[필수] 페이징 페이지당보여질수
      CompanySvc.selectRcutList(param)
          .then(function(data){

              vm.pgmList = [{v:'999',k:'프로그램 선택'}];
              vm.pgmBaseSeq = '999';
              _.forEach(data.user.resultData.rcutList,function(obj){
                  vm.pgmList.push({v:obj.RCUT_BASE_SEQ+'', k:'['+ obj.PGM_REUT_FIELD_NM +']['+obj.PGM_HOPE_CAST+'] ' + obj.PGM_NAME + (obj.TITLE_GUBUN != '' ? '('+obj.TITLE_GUBUN+')' : '')});
              })
              
              searchFn();

          })
          .catch(function(error){
              logger.log('fail');
              logger.error('message: ' + error + '\n');
          });
  }
  
  
  /**
   * 더보기
   */
  function searchMore() {
    vm.curPage++;
    searchFn();
  }
  
  
}
