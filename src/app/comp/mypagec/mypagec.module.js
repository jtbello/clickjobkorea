
angular
  .module('comp.mypagec', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('comp.mypagec', {
      url: '/mypagec',
      abstract: false,
      template: '<div ui-view><h1>comp.mypagec</h1></div>',
      controller: 'mypagecCtrl as mypagec'
    })

    .state('comp.mypagec.cmMyPage', {
      url: '/cmMyPage',
      templateUrl: 'mypagec/cmMyPage/cmMyPage.tpl.html',
      data: { menuTitle : '마이페이지', menuId : 'cmMyPage', depth : 1},
        params: { param: null, backupData: null },
      controller: 'cmMyPageCtrl as cmMyPage'
    })
      .state('comp.mypagec.cmNotiChk', {
          url: '/cmNotiChk',
          templateUrl: 'mypagec/cmNotiChk/cmNotiChk.tpl.html',
          data: { menuTitle : '알림 확인', menuId : 'cmNotiChk', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cmNotiChkCtrl as cmNotiChk'
      })
      .state('comp.mypagec.cmInfoMod', {
          url: '/cmInfoMod',
          templateUrl: 'mypagec/cmInfoMod/cmInfoMod.tpl.html',
          data: { menuTitle : '정보 수정', menuId : 'cmInfoMod', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cmInfoModCtrl as cmInfoMod'
      })
      .state('comp.mypagec.cmNotiSet', {
          url: '/cmNotiSet',
          templateUrl: 'mypagec/cmNotiSet/cmNotiSet.tpl.html',
          data: { menuTitle : '알림 설정', menuId : 'cmNotiSet', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cmNotiSetCtrl as cmNotiSet'
      })
    ;

  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
