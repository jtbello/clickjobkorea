angular.module('comp.authc').controller('caPesnModCtrl', caPesnModCtrl);

function caPesnModCtrl(
  $log,
  $scope,
  $user,
  $message,
  $rootScope,
  $stateParams,
  CompanySvc
) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    userName: '', // 사용자 이름
    userId: '', // 사용자 아이디
    password: '', // 비밀번호
    passwordConf: '', // 비밀번호 확인
    mobileProviderId: '999', // 통신사 선택값
    mobileProvider: '', // 통신사
    mobileNumber: '', // 휴대폰번호
    phoneNumber: '', // 내선번호
    emailId: '', // 이메일 아이디
    emailDomainId: '999', // 이메일 도메인 선택값
    emailDomain: '', // 이메일 도메인
    userPosition: '', // 직책
    userRoleId: '999', // 사용자 권한 선택값
    userRole: '', // 사용자 권한
    ceoYn: '', // CEO 여부
    // 옵션 리스트
    emailDomainOptions: [], // 이메일 도메인 선택 리스트
    userRoleOptions: [] // 사용자 권한 선택 리스트
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    onModifyClick: onModifyClick,
    onDeleteClick: onDeleteClick
  });

  /**
   * Initialize
   */
  setSelectOptions();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    var param = {
      selCompanyMemberSeq: $stateParams.param.companyMemberSeq
    };
    CompanySvc.selectCompanyMemberListInfo(param).then(function(data) {
      var member = data.user.resultData.companyMemberList[0];

      vm.ceoYn = member.CEO_YN;
      vm.userName = member.COMPANY_MEMBER_NAME;
      vm.userId = member.COMPANY_MEMBER_ID;
      vm.mobileProviderId = member.COMPANY_MEMBER_PHONE_KIND;
      vm.mobileNumber = member.COMPANY_MEMBER_PHONE_NO;
      vm.phoneNumber = member.COMPANY_MEMBER_TEL;
      if(member.COMPANY_MEMBER_EMAIL){
        vm.emailId = member.COMPANY_MEMBER_EMAIL.split('@')[0];
        // 이메일 도메인이 선택 리스트 중 하나일 경우
        var emailDomain = member.COMPANY_MEMBER_EMAIL.split('@')[1];
        vm.emailDomainOptions.map(function(option) {
          if (option.text === emailDomain) {
            vm.emailDomainId = option.value;
          }
        });
      }

      

      // 이메일 도메인이 선택 리스트에 없는 도메인일 경우
      if (vm.emailDomainId === '999') {
        vm.emailDomainId = '0';
        vm.emailDomain = emailDomain;
      }

      vm.userPosition = member.COMPANY_MEMBER_POSITION;
      vm.userRoleId = member.AUTH_INFO_SEQ.toString();

      logger.log('init()', 'vm: ', vm);
    });
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  function onDeleteClick() {
    if (!$message.confirm('삭제하시겠습니까?')) return false;
    
    var param = {
      deleteCompanyMemberSeq: $stateParams.param.companyMemberSeq
    }

    CompanySvc.deleteCompanyMember(param)
      .then(function(data) {
        $message.alert('정상적으로 삭제되었습니다.');
        logger.log('onDeleteClick[SUCCESS]', 'data: ', data);
        $rootScope.goView('comp.authc.caPesnMng');
      })
      .catch(function(error) {
        logger.log('onDeleteClick[FAILED]', 'error: ', error);
      });
  }

  function onModifyClick() {
    if (!$message.confirm('수정하시겠습니까?')) return false;
    if (!validateInputs()) return false;

    var param = getParams();

    CompanySvc.updateCompanyMember(param)
      .then(function(data) {
        $message.alert('정상적으로 수정되었습니다.');
        logger.log('onModifyClick[SUCCESS]', 'data: ', data);
        $rootScope.goView('comp.authc.caPesnMng');
      })
      .catch(function(error) {
        logger.log('onModifyClick[FAILED]', 'error: ', error);
      });
  }

  // param 생성
  function getParams() {
    var param = {};

    var email = `${vm.emailId}@${
      vm.emailDomainId === '0' ? vm.emailDomain : vm.emailDomainId
    }`;

    param.modifyCompanyMemberSeq = $stateParams.param.companyMemberSeq; //[필수] 수정담당자일련번호
    param.companyMemberPosition = vm.userPosition; //[선택] 담당자직책
    if (vm.password !== '') {
      param.companyMemberPwd = vm.password; //[선택] 담당자패스워드
    }
    param.companyMemberPhoneKind =
      vm.mobileProviderId == '999' ? '' : vm.mobileProviderId; //[선택][공코:HP_KIND] 담당자휴대폰회사정보
    param.companyMemberPhoneNo = vm.mobileNumber; //[선택] 담당자휴대폰번호
    param.authInfoSeq = vm.userRoleId == '999' ? '' : vm.userRoleId; //[선택] 권한일련번호
    param.companyMemberTel = vm.phoneNumber || ''; //[선택] 담당자전화번호
    param.companyMemberEmail = email; //[선택] 담당자이메일

    return param;
  }

  // 입력값 유효성 체크
  function validateInputs() {
    var FORM_INPUTS = [
      {
        varName: 'userName',
        errorType: 'empty',
        errorMessage: '이름을 입력해주세요.'
      },
      {
        varName: 'userId',
        errorType: 'empty',
        errorMessage: '아이디를 입력해주세요.'
      },
      {
        varName: 'password',
        errorType: 'invalid',
        errorMessage: '비밀번호는 영문, 숫자 조합으로 8자 이상이어야 합니다.'
      },
      {
        varName: 'passwordConf',
        errorType: 'invalid',
        errorMessage: '비밀번호가 서로 일치하지 않습니다.'
      },
      {
        varName: 'mobileProviderId',
        errorType: 'empty',
        errorMessage: '통신사를 선택해주세요.'
      },
      {
        varName: 'mobileNumber',
        errorType: 'empty',
        errorMessage: '휴대폰번호를 입력해주세요.'
      },
      {
        varName: 'emailId',
        errorType: 'empty',
        errorMessage: '이메일을 입력해주세요.'
      },
      {
        varName: 'emailDomainId',
        errorType: 'empty',
        errorMessage: '이메일을 입력해주세요.'
      },
      {
        varName: 'userPosition',
        errorType: 'empty',
        errorMessage: '직책을 입력해주세요.'
      },
      {
        varName: 'userRoleId',
        errorType: 'empty',
        errorMessage: '소속(권한)을 선택해주세요.'
      }
    ];

    // 비밀번호 정규식 (8자 이상 / 문자, 숫자 포함)
    var passwordRegExp = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;

    var validationResult = FORM_INPUTS.map(function(input) {
      var varName = input.varName;
      var errorType = input.errorType;
      var errorMessage = input.errorMessage;

      // 이름, 아이디
      if (
        varName === 'userName' ||
        varName === 'userId' ||
        varName === 'mobileNumber' ||
        varName === 'emailId' ||
        varName === 'userPosition'
      ) {
        if (vm[varName] === '') return errorMessage;

        // 비밀번호 유효성
      } else if (
        varName === 'password' &&
        vm[varName] !== '' &&
        !passwordRegExp.test(vm[varName])
      ) {
        return errorMessage;

        // 비밀번호 확인 유효성
      } else if (
        varName === 'passwordConf' &&
        vm[varName] !== '' &&
        vm[varName] !== vm.password
      ) {
        return errorMessage;

        // 통신사 선택
      } else if (varName === 'mobileProviderId' || varName === 'userRoleId') {
        if (vm[varName] === '999') return errorMessage;

        // 이메일 도메인 선택
      } else if (varName === 'emailDomainId') {
        if (
          vm[varName] === '999' ||
          (vm[varName] === '0' && vm.emailDomain === '')
        ) {
          return errorMessage;
        }
      }
    }).filter(function(value) {
      return value !== undefined;
    });

    if (validationResult.length > 0) {
      $message.alert(validationResult[0]);
      return false;
    }

    return true;
  }

  function setSelectOptions() {
    // 메일 도메인 리스트
    vm.emailDomainOptions = [
      { value: '999', text: '선택' },
      { value: 'naver.com', text: 'naver.com' },
      { value: 'hanmail.net', text: 'hanmail.net' },
      { value: 'gmail.com', text: 'gmail.com' },
      { value: 'hotmail.com', text: 'hotmail.com' },
      { value: 'nate.com', text: 'nate.com' },
      { value: '0', text: '직접입력' }
    ];

    // 사용자 권한 (소속) 리스트
    CompanySvc.selectCompanyAuthInfoList({}).then(function(data) {
      vm.userRoleOptions = [{ value: '999', text: '선택' }];

      _.forEach(data.user.resultData.companyAuthInfoList, function(authInfo) {
        // 어드민 권한 아닐경우 최고 권한 등록 패스
        if (
          !$user.currentUser.getComAdminYn() &&
          !$user.currentUser.getAdminYn()
        ) {
          if (
            authInfo.AUTH_INFO_MENU_CODE == 'ALL' ||
            authInfo.AUTH_INFO_MENU_CODE == 'COMALL' 
          ) {
            return true;
          }
        }
        
        if (
          authInfo.AUTH_INFO_MENU_CODE == 'INIT'
        ) {
          return true;
        }

        vm.userRoleOptions.push({
          value: authInfo.AUTH_INFO_SEQ.toString(),
          text: authInfo.AUTH_INFO_NAME
        });
      });
      
      init();
      
    });
  }
}
