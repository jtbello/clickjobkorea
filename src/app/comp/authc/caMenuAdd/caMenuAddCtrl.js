
angular
  .module('comp.authc')
  .controller('caMenuAddCtrl', caMenuAddCtrl);

function caMenuAddCtrl($log, $scope, $modal, maConstant) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  
}
