
angular
  .module('comp.authc')
  .controller('caPesnMngCtrl', caPesnMngCtrl);

function caPesnMngCtrl($log, $scope, $user, $rootScope, CompanySvc, $stateParams) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    userRoleId: '999', // 사용자 권한 (소속)
    searchTerm: '', // 검색어
    showShowMoreButton: true, // 더보기 버튼 표시
    companyMemberList: [], // 사용자 리스트
    companyMemberListCount: 0, // 사용자수

    userRoleOptions: [], // 사용자 권한 선택 리스트

    // 페이징 관련
	  curPage : 1,						// 현재페이지
	  viewPageCnt : 10,					// 페이지당보여질수
	  pagingData : [],					// 페이지번호 모듬
	  totPageCnt : 0,					// 전체페이지수
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    search: search,
    onMemberClick: onMemberClick,
    onShowMoreButtonClick: onShowMoreButtonClick
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    setSelectOptions();
    search();
   
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  function onShowMoreButtonClick() {
    vm.curPage++;
    search(false);
  }

  function onMemberClick(member) {
    logger.log('onMemberClick: ', member);
    var params = { companyMemberSeq: member.COMPANY_MEMBER_SEQ };
    $rootScope.goView('comp.authc.caPesnMod', vm, params);
  }

  function search(shouldResetMemberList = true) {

    if (shouldResetMemberList) {
      vm.companyMemberList = [];
      vm.companyMemberListCount = 0;
      vm.pagingData = [];
      vm.curPage = 1;
      vm.showShowMoreButton = true;
    }

    var param = getParams();

    CompanySvc.selectCompanyMemberListInfo(param)
      .then(function(data) {
        // 리스트 업데이트
        vm.companyMemberList = vm.companyMemberList.concat(data.user.resultData.companyMemberList);
        vm.companyMemberListCount = data.user.resultData.companyMemberListCount;

        // 리스트에 추가할 항목이 없을 경우
        if (data.user.resultData.companyMemberList.length === 0 && vm.curPage > 1) {
          vm.curPage--;

          // 기존 리스트에 항목이 추가됐을 경우
          // or 리스트에 처음 항목이 추가됐을 경우
        }

        // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
        if (data.user.resultData.companyMemberList.length < vm.viewPageCnt) {
          vm.showShowMoreButton = false;
        }

        // 전체 페이지 정보입력
        vm.totPageCnt = Math.floor(
          vm.companyMemberListCount / vm.viewPageCnt +
            (vm.memberListCount % vm.viewPageCnt == 0 ? 0 : 1)
        );
        vm.pagingData = [];
        for (var i = 1; i <= vm.totPageCnt; i++) {
          vm.pagingData.push({ pageNo: i });
        }

        logger.log('search[SUCCESS]', 'vm.companyMemberList: ', vm.companyMemberList);
      })
      .catch(function(error) {
        logger.log('search[FAILED]', 'error: ', error);
      });

  }

  function getParams() {
    var param = {};
    param.startNo = (vm.curPage -1) * vm.viewPageCnt;							//[필수] (현재페이지 -1) * 페이지당보여질수
    param.endNo = vm.viewPageCnt;												//[필수] 페이징 페이지당보여질수

    param.searchKind = '';                              //[선택] 검색조건
    param.searchData = vm.searchTerm;											//[선택] 검색어(이름)
    param.authInfoSeq = vm.userRoleId == '999' ? '' :	vm.userRoleId;		//[선택] 권한일련번호

    return param;
  }

  function setSelectOptions() {

   // 사용자 권한 (소속) 리스트
   return CompanySvc.selectCompanyAuthInfoList({}).then(function(data) {

     vm.userRoleOptions = [{ value: '999', text: '전체' }];

     _.forEach(data.user.resultData.companyAuthInfoList, function(authInfo) {
       // 어드민 권한 아닐경우 최고 권한 등록 패스
       if (
         !$user.currentUser.getComAdminYn() &&
         !$user.currentUser.getAdminYn()
       ) {
         if (
           authInfo.AUTH_INFO_MENU_CODE == 'ALL' ||
           authInfo.AUTH_INFO_MENU_CODE == 'COMALL' ||
           authInfo.AUTH_INFO_MENU_CODE == 'INIT'
         ) {
           return true;
         }
       }

       vm.userRoleOptions.push({
         value: authInfo.AUTH_INFO_SEQ.toString(),
         text: authInfo.AUTH_INFO_NAME
       });
     });

   });
  }

}
