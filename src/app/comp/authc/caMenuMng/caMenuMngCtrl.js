angular.module('comp.authc').controller('caMenuMngCtrl', caMenuMngCtrl);

function caMenuMngCtrl($log, $scope, $user, $message, $rootScope, CompanySvc, ngspublic) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    userRoleId: '', // 소속(권한)
    userRoleText: '', // 소속(권한) 텍스트
    userRoleMenuCode: '', // 소속(권한) 메뉴권한코드
    selAuth : null,       // 선택된 소속 Data
    userRoleTextDisabled : false,
    newInsert : false,

    // 체크리스트
    showActorsInfoAuth: false, // 출연자 정보 보기 권한
    showActorsInfoNotify: false, // 출연자 정보 보기 알림
    jobOfferToActorsAuth: false, // 출연자 출연 요청 권한
    jobOfferToActorsNotify: false, // 출연자 출연 요청 알림
    addEditProgramsAuth: false, // 프로그램 등록/수정 권한
    addEditProgramsNotify: false, // 프로그램 등록/수정 알림
    showProgramsAuth: false, // 프로그램 열람 권한
    showProgramsNotify: false, // 프로그램 열람 알림
    postEditRecruitInfoAuth: false, // 출연자모집정보 등록/수정 권한
    postEditRecruitInfoNotify: false, // 출연자모집정보 등록/수정 알림
    showRecruitInfoAuth: false, // 출연자모집정보 열람 권한
    showRecruitInfoNotify: false, // 출연자모집정보 열람 알림
    jobOfferToActorsAuth_2: false, // 출연자관리 (출연요청 및 취소) 권한 /*** 중복??? ***/
    jobOfferToActorsNotify_2: false, // 출연자관리 (출연요청 및 취소) 알림 /*** 중복??? ***/
    showActorsInfoAuth_2: false, // 출연자관리 (열람) 권한 /*** 중복??? ***/
    showActorsInfoNotify_2: false, // 출연자관리 (열람) 알림 /*** 중복??? ***/
    settlementAuth: false, // 정산관리 권한
    settlementNotify: false, // 정산관리 알림
    answerOneOnOneQuestionsAuth: false, // 1:1문의 답변관리 권한
    answerOneOnOneQuestionsNotify: false, // 1:1문의 답변관리 알림

    userRoleOptions: [], // 소속(권한) 리스트
    menuList : [],       // 전체 메뉴리스트
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    onUserRoleChange: onUserRoleChange,
    onRoleNameChangeButtonClick: onRoleNameChangeButtonClick,
    onEditButtonClick: onEditButtonClick,
    onAddButtonClick: onAddButtonClick,
    checkAuth : checkAuth,          // 체크박스선택
    insertAuth : insertAuth,        // 소속 등록(수정) 팝업
    deleteAuth : deleteAuth,        // 권힌 삭제
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    selectMenuList();
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  /**
   * 소속 등록 호출
   */
  function insertAuth(){
   
    var chk = true;
    chk = chk && ngspublic.nullCheck(vm.userRoleText,      '소속이름을 입력해주세요.');
    if(!chk) return false;
    
    
    var beforeChk = chkInsertBefore();
    if(!beforeChk) return false;
    
    var param = {};
    param.authInfoName = vm.userRoleText;                 //[필수] 권한이름
    param.authInfoMenuCode = vm.authInfoMenuCode;         //[필수] 메뉴권한코드
    param.authInfoAlarmCode = vm.authInfoAlarmCode;       //[필수] 알림권한코드
    
    
    CompanySvc.insertAuthInfo(param)
    .then(function(data) {
      $message.alert('정상적으로 등록되었습니다.');
      window.location.reload();
      
    });
    
  }


  /**
   * 소속 삭제 호출
   */
  function deleteAuth(){

    var beforeChk = $message.confirm('소속(권한)을 삭제 하시겠습니까?');
    if(!beforeChk) return false;

    var param = {};
    param.authInfoSeq = vm.userRoleId;					//[필수]권한일련번호

    CompanySvc.deleteAuthInfo(param)
        .then(function(data) {
          $message.alert('소속(권한) 정보가 정상적으로 삭제되었습니다.');
          window.location.reload();
        });

  }
  
  // 등록 수정 전 체크 및 파라미터 생성
  function chkInsertBefore(){
    
    // 권한코드 생성
    var chkMenu = _.where(vm.menuList, {menuChk : true});
    var chkPush = _.where(vm.menuList, {pushChk : true});
    
    // 권한
    vm.authInfoMenuCode = '';
    vm.authInfoAlarmCode = '';
    _.forEach(chkMenu, function(obj){
      vm.authInfoMenuCode += obj.COMMON_INFO_VALUE1 + '@@';
    })
     _.forEach(chkPush, function(obj){
       vm.authInfoAlarmCode += obj.COMMON_INFO_VALUE1 + '@@';
    })
    
    if(vm.authInfoMenuCode == ''){
      $message.alert('하나 이상의 메뉴를 선택해주세요.');
      return false;
    }
    
    // L0 이 포함된 메뉴가 있는데 하위 메뉴가 없으면 오류 체크 해야함
    var chkMenuL0 = _.where(chkMenu, {COMMON_INFO_VALUE3 : 'L0'});
    var chkFlag = true;
    _.forEach(chkMenuL0, function(obj){
      var temp = [];
      _.forEach(chkMenu, function(findObj){
        if(findObj.COMMON_INFO_VALUE3 != 'L0' &&  _.startsWith(findObj.COMMON_INFO_VALUE1,obj.COMMON_INFO_VALUE1)){
          temp.push(findObj)
        }
      });
      
       if(temp.length <= 0){
         $message.alert('대메뉴가 선택되어있지만 하위 메뉴가 없습니다. 확인해주세요.['+obj.COMMON_INFO_VALUE2+']');
         chkFlag = false;
         return false;
       }
    })
    
    if(!chkFlag) return false;
    
    
    vm.authInfoMenuCode = vm.authInfoMenuCode.substr(0, vm.authInfoMenuCode.length -2);
    vm.authInfoAlarmCode = vm.authInfoAlarmCode.substr(0, vm.authInfoAlarmCode.length -2);
    
    return true;
    
  }
  
  /**
   * 체크박스 자동 체크 로직
   */
  function checkAuth(){
   
    // 권한 체크시작
    var chkMenu = _.where(vm.menuList, {menuChk : true});
    var chkPush = _.where(vm.menuList, {pushChk : true});
    
    // 권한 자동 체크
    _.forEach(chkMenu, function(obj){
        if(obj.COMMON_INFO_VALUE3 == 'L1'){
          var splitData = obj.COMMON_INFO_VALUE1.split('_');
          // 상위 레벨 자동 체크
          _.forEach(vm.menuList, function(menuObj){
            if(menuObj.COMMON_INFO_VALUE1 == splitData[0]){
              menuObj.menuChk = true;
            }
          });
        }
        if(obj.COMMON_INFO_VALUE3 == 'L2'){
          var splitData = obj.COMMON_INFO_VALUE1.split('_');
          // 0 레벨 자동 체크
          _.forEach(vm.menuList, function(menuObj){
            if(menuObj.COMMON_INFO_VALUE1 == splitData[0]){
              menuObj.menuChk = true;
            }
          });
          // 1 레벨 자동 체크
          _.forEach(vm.menuList, function(menuObj){
            if(menuObj.COMMON_INFO_VALUE1 == splitData[0]+'_'+splitData[1]){
              menuObj.menuChk = true;
            }
          });
        }
    })
    
    // 알림 자동 체크
     _.forEach(chkPush, function(obj){
        if(obj.COMMON_INFO_VALUE3 == 'L1'){
          var splitData = obj.COMMON_INFO_VALUE1.split('_');
          // 상위 레벨 자동 체크
          _.forEach(vm.menuList, function(menuObj){
            if(menuObj.COMMON_INFO_VALUE1 == splitData[0]){
              menuObj.pushChk = true;
            }
          });
        }
        if(obj.COMMON_INFO_VALUE3 == 'L2'){
          var splitData = obj.COMMON_INFO_VALUE1.split('_');
          // 0 레벨 자동 체크
          _.forEach(vm.menuList, function(menuObj){
            if(menuObj.COMMON_INFO_VALUE1 == splitData[0]){
              menuObj.pushChk = true;
            }
          });
          // 1 레벨 자동 체크
          _.forEach(vm.menuList, function(menuObj){
            if(menuObj.COMMON_INFO_VALUE1 == splitData[0]+'_'+splitData[1]){
              menuObj.pushChk = true;
            }
          });
        }
    })
    
  }

  // '수정' 버튼 클릭시
  function onEditButtonClick() {
    
    if(vm.userRoleTextDisabled){
      $message.alert('수정할 수 없는 소속입니다.');
      return false;
    }
    
    var beforeChk = chkInsertBefore();
    if(!beforeChk) return false;
    
    var param = {
      authInfoSeq: vm.userRoleId,
      authInfoName: vm.userRoleText,
      authInfoMenuCode : vm.authInfoMenuCode,
      authInfoAlarmCode : vm.authInfoAlarmCode
    };

    CompanySvc.updateAuthInfo(param)
      .then(function(data) {
        $message.alert('소속(권한) 메뉴 정보가 정상적으로 수정되었습니다.');
        window.location.reload();
      });
  }

  // '소속추가' 버튼 클릭시
  function onAddButtonClick() {
    vm.userRoleId = '999';
    onUserRoleChange();
    $(document).scrollTop(0)
  }

  // '명칭변경' 버튼 클릭시
  function onRoleNameChangeButtonClick() {
    
    if(vm.userRoleTextDisabled){
      return false;
    }
    
    var param = {
      authInfoSeq: vm.userRoleId,
      authInfoName: vm.userRoleText
    };

    CompanySvc.updateAuthInfo(param)
      .then(function(data) {
        $message.alert('소속(권한) 명칭이 변경되었습니다.');

        // 사용자 권한 (소속) 리스트에 저장된 텍스트 값 변경
        var selectedOption = vm.userRoleOptions.forEach(function(option) {
          if (option.AUTH_INFO_SEQ === vm.userRoleId) {
            option.text = vm.AUTH_INFO_NAME;
          }
        });
      });
  }

  // 사용자 권한 (소속) 리스트 선택 변경시 텍스트 인풋 변경
  function onUserRoleChange() {
    var selectedOption = vm.userRoleOptions.find(function(option) {
      return option.AUTH_INFO_SEQ.toString() === vm.userRoleId;
    });
    vm.userRoleText = selectedOption.AUTH_INFO_NAME;
    vm.userRoleMenuCode = selectedOption.AUTH_INFO_MENU_CODE;
    
    // 신규등록 이면 초기화 후 패스
    if(selectedOption.AUTH_INFO_SEQ === '999'){
      _.forEach(vm.menuList, function(obj){
        obj.menuChk = false;
        obj.pushChk = false;
      })
      vm.userRoleTextDisabled = false;
      vm.userRoleText = '';
      vm.userRoleMenuCode = '';
      return false;
    }
    
    
    vm.selAuth = selectedOption;
    var authCode = vm.selAuth.AUTH_INFO_MENU_CODE.split('@@');
    var pushCode = vm.selAuth.AUTH_INFO_AlARM_CODE.split('@@');
    
    if(authCode[0] == 'ALL' || authCode[0] == 'COMALL' || authCode[0] == 'INIT'){
      vm.userRoleTextDisabled = true;
    }else{
      vm.userRoleTextDisabled = false;
    }
    
    
    // 체크 값 세팅
    _.forEach(vm.menuList, function(obj){
      obj.menuChk = false;
      obj.pushChk = false;
      // 초기 권한 패스
      if(authCode[0] == 'ALL' || authCode[0] == 'COMALL'){
        obj.menuChk = true;
      }
      if(pushCode[0] == 'ALL' || pushCode[0] == 'COMALL'){
        obj.pushChk = true;
      }
      
      // 메뉴
      _.forEach(authCode, function(code){
        if(code == obj.COMMON_INFO_VALUE1){
          obj.menuChk = true;
        }
      })
      // 알림메뉴
      _.forEach(pushCode, function(code){
        if(code == obj.COMMON_INFO_VALUE1){
          obj.pushChk = true;
        }
      })
    })
    
    
  }

  function setSelectOptions() {
    // 사용자 권한 (소속) 리스트
    CompanySvc.selectCompanyAuthInfoList({}).then(function(data) {
      vm.userRoleOptions = [];
      
      _.forEach(data.user.resultData.companyAuthInfoList, function(obj){
        obj.AUTH_INFO_SEQ = obj.AUTH_INFO_SEQ.toString();
        vm.userRoleOptions.push(obj);
      });
      
      vm.userRoleOptions.push({AUTH_INFO_SEQ : '999', AUTH_INFO_NAME : '신규등록'});
     
      if(vm.userRoleOptions.length > 0){
        vm.userRoleId = vm.userRoleOptions[0].AUTH_INFO_SEQ.toString();
        onUserRoleChange();
      }
      
    });
  }
  
  
  /**
   * 전체 메뉴 조회
   */
  function selectMenuList(){
    var param = {};
    param.menuKind = 'M2';
    CompanySvc.selectMenuList(param).then(function(data) {
      vm.menuList = data.user.resultData.menuList;
      _.forEach(vm.menuList, function(obj){
        obj.menuChk = false;
        obj.pushChk = false;
      })
      
      // 소속 조회
      setSelectOptions();
    });
    
  }
  
}
