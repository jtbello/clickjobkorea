
angular
  .module('comp.authc', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('comp.authc', {
      url: '/authc',
      abstract: false,
      template: '<div ui-view><h1>comp.authc</h1></div>',
      controller: 'authcCtrl as authc'
    })

    .state('comp.authc.caNewReg', {
      url: '/caNewReg',
      templateUrl: 'authc/caNewReg/caNewReg.tpl.html',
      data: { menuTitle : '사용자 신규등록', menuId : 'caNewReg', depth : 1},
        params: { param: null, backupData: null },
      controller: 'caNewRegCtrl as caNewReg'
    })
      .state('comp.authc.caMenuMng', {
          url: '/caMenuMng',
          templateUrl: 'authc/caMenuMng/caMenuMng.tpl.html',
          data: { menuTitle : '소속별 메뉴관리', menuId : 'caMenuMng', depth : 1},
          params: { param: null, backupData: null },
          controller: 'caMenuMngCtrl as caMenuMng'
      })
      .state('comp.authc.caMenuAdd', {
          url: '/caMenuAdd',
          templateUrl: 'authc/caMenuAdd/caMenuAdd.tpl.html',
          data: { menuTitle : '소속추가', menuId : 'caMenuAdd', depth : 2},
          params: { param: null, backupData: null },
          controller: 'caMenuAddCtrl as caMenuAdd'
      })
      .state('comp.authc.caPesnMng', {
          url: '/caPesnMng',
          templateUrl: 'authc/caPesnMng/caPesnMng.tpl.html',
          data: { menuTitle : '소속 사용자 관리', menuId : 'caPesnMng', depth : 1},
          params: { param: null, backupData: null },
          controller: 'caPesnMngCtrl as caPesnMng'
      })
      .state('comp.authc.caPesnMod', {
          url: '/caPesnMod',
          templateUrl: 'authc/caPesnMod/caPesnMod.tpl.html',
          data: { menuTitle : '권한 관리', menuId : 'caPesnMod', depth : 2},
          params: { param: null, backupData: null },
          controller: 'caPesnModCtrl as caPesnMod'
      })
    ;

  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
