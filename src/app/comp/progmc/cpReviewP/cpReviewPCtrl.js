
angular
  .module('comp.progmc')
  .controller('cpReviewPCtrl', cpReviewPCtrl);

function cpReviewPCtrl($log, $rootScope, $scope, $modal, $modalInstance, item, $message, ngspublic, CompanySvc) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        // 파라미터
        requestInfoSeq : item.requestInfoSeq,            //[필수] 모집신청일련번호

        // 변수
        reviewCntn : ''		           // 평가
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        confirm : confirm
    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 출연자 리스트 조회  - 평가입력
     */
    function confirm(){

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.reviewCntn , 	'평가를 입력해주세요.');
        if(!chk) return false;

        var param = {};
        param.requestInfoSeq = vm.requestInfoSeq;							//[필수] 모집신청일련번호
        param.reviewCntn = vm.reviewCntn;								//[필수] 평가 내용

        CompanySvc.insertMemberReview(param)
            .then(function(data){
                $message.alert('정상적으로 처리 되었습니다.');
                $modalInstance.close();
            })
            .catch(function(err){
            })
    }
}
