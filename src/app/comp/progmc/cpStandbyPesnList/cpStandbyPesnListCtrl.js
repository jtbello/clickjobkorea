angular
  .module('comp.progmc')
  .controller('cpStandbyPesnListCtrl', cpStandbyPesnListCtrl);

function cpStandbyPesnListCtrl($log, $window ,User, $rootScope,$storage, $q, $scope, $modal, $message, $stateParams, MetaSvc, 
                    CompanySvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      detailBaseInfo : {},
      rcutBaseSeq : -1,                   // 출연자 모집일련번호
      isAllChk : false,					    // 전체 선택 여부

      rcutRequestMemeberList : [],					// 회원정보
      rcutRequestMemeberListCount : 0,				// 회원전체카운트

      // 페이징 관련
      curPage : 1,						// 현재페이지
      viewPageCnt : 10,					// 페이지당보여질수
      pagingData : [],					// 페이지번호 모듬
      totPageCnt : 0					// 전체페이지수
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      allChk : allChk,
      search : search,
      procRequest : procRequest
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);
    $scope.$on('$destroy', destroy);

    if($stateParams.backupData){
        _.assign(vm, $stateParams.backupData);
        search();
    }else{
        if($stateParams.param){
            vm.detailBaseInfo = $stateParams.param.detailBaseInfo;
            vm.rcutBaseSeq = vm.detailBaseInfo.RCUT_BASE_SEQ;
            search();
        }else{
            $message.alert('데이터가 올바르지 않습니다.\n이전 화면으로 이동합니다.');
            $rootScope.prevView();
        }
    }
  }


  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  /**
   * 촬영대기순번 수락
   */
  function procRequest(kind, data){
    
    var confirmResult = $message.confirm('수락 하시겠습니까?');
    if(!confirmResult) return false;
    
    var param = {};
    
    param.proKind = kind;									//[필수] 처리종류 (P1: 요청 취소 , P2: 요청수락, P3: 수락취소, P4: 대기인원수락)
    param.requestInfoSeq = data.REQUST_INFO_SEQ + '';							//[필수] 모집신청일련번호
    CompanySvc.procRequest(param)
    .then(function(data){
      $message.alert('정상적으로  처리 되었습니다.');
      search(false, vm.curPage);
    })
    .catch(function(err){
    })
    
  }
  

  function allChk(data){
      if(vm.isAllChk){
          _.forEach(data,function(obj, idx){
              obj.chk = true;
          });
      }else{
          _.forEach(data,function(obj, idx){
              obj.chk = false;
          });
      }
  }

  function search(more, saveCurPage){
      vm.curPage = !more ? 1 : vm.curPage+1;

      var param = {};
      param.startNo = (vm.curPage -1) * vm.viewPageCnt;								//[필수] (현재페이지 -1) * 페이지당보여질수
      param.endNo = vm.viewPageCnt;													//[필수] 페이징 페이지당보여질수

      if(saveCurPage){
        param.endNo = saveCurPage * vm.viewPageCnt; 
      }

      param.rcutBaseSeq = vm.rcutBaseSeq;								    //[선택] 출연자 모집일련번호
      CompanySvc.selectRcutrRequestWatingMemberList(param)
          .then(function(data){
              logger.log('selectRcutrRequestWatingMemberList success', data);
              if(data.user.resultData.rcutRequestMemeberList.length > 0){
                  var tempList = data.user.resultData.rcutRequestMemeberList;

                  if(!more){
                      vm.rcutRequestMemeberList = tempList;
                  }else{
                      vm.rcutRequestMemeberList = vm.rcutRequestMemeberList.concat(tempList);
                  }

                  vm.rcutRequestMemeberListCount = data.user.resultData.rcutRequestMemeberListCount;

                  // 전체 페이지 정보입력
                  vm.totPageCnt = Math.floor((vm.rcutRequestMemeberListCount / vm.viewPageCnt) + ((vm.rcutRequestMemeberListCount % vm.viewPageCnt) === 0 ? 0 : 1));
                  vm.pagingData = [];
                  for (var i = 1; i <= vm.totPageCnt; i++) {
                      vm.pagingData.push({pageNo : i});
                  }

              }else{
                  if(!more){
                      vm.rcutRequestMemeberList = [];
                      vm.rcutRequestMemeberListCount = 0;
                      vm.pagingData = [];
                  }
              }
          })
          .catch(function(error){
              logger.log('fail');
              logger.error('message: ', error);
              vm.curPage = more ? vm.curPage-1 : vm.curPage;
          });
  }

}
