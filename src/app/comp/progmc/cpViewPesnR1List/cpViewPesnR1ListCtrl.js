
angular
  .module('comp.progmc')
  .controller('cpViewPesnR1ListCtrl', cpViewPesnR1ListCtrl);

function cpViewPesnR1ListCtrl($log, $scope, $modal, $rootScope, $stateParams, CompanySvc, ngspublic, $message, $filterSet, $doryCall) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      // 파라미터
      rcutBaseSeq : '',                 // 모집일련번호
      rcutBaseState : '',                 // 모집상태
      detailBaseInfo : {},              // 모집상세정보
      // 변수
      allChk : false,
      totPgmName : '',					// 프로그램 전체명
      statusSelelctInfo : [],			// 조회 상태 셀렉트박스 정보
      searchSelelctInfo : [],			// 조회 검색 셀렉트박스 정보

      memberStateCode : '999',
      searchData : '',
      searchKind : '999',

      rcutRequestMemeberList : [],					// 출연자 보기 리스트
      rcutRequestMemeberListCount : 0,	// 출연자 보기 전체 카운트

      requestMemberTotCnt : 0,			// 출연자 모집 상세  - 출연자 목록 보기(통계) - 총 모집 인원	
      requestCompleteCnt : 0,			// 출연자 모집 상세  - 출연자 목록 보기(통계) - 총 모집 신청 인원
      requestCompleteInfo : [],			// 출연자 모집 상세  - 출연자 목록 보기(통계) - 모집인원정보
      requestCompleteEtcInfo : [],		// 출연자 모집 상세  - 출연자 목록 보기(통계) - 모집인원정보

      lastSchedulePushInfo : [],		// 스케줄변경알림정보

      attendNoLeaveState : '',			// 미출석,무단이탈여부
      attendNoLeaveNote : '',			// 미출석 무단이탈 관리자확인사항

      attendEndInfo : {},				// 조회된 출석정보
      companyMemberAttendCode : false,	//
      companyMemberAttendDtm : '',		// 출석일
      companyMemberAttendHour : '999',	// 출석 시
      companyMemberAttendMinite : '999',// 출석 분
      companyMemberAttendPoint : '',	// 출석장소

      companyMemberEndDtm : '',			// 종료일
      companyMemberEndHour : '999',		// 종료 시
      companyMemberEndMinite : '999',	// 종료 분
      companyMemberEndPoint : '',		// 종료장소
      reviewCntn : '',					// 평가

      // 정렬 관련
      orderKind1 : 'O1',
      orderKind2 : 'O1',

      // 페이징 관련
      curPage : 1,						// 현재페이지
      viewPageCnt : 10,					// 페이지당보여질수
      pagingData : [],					// 페이지번호 모듬
      totPageCnt : 0					// 전체페이지수
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      allChkFn : allChkFn,
      showReqPushPopup : showReqPushPopup,
      showSchduleMngPopup : showSchduleMngPopup,
      rcutEndProc : rcutEndProc,
      procRequest : procRequest,
      goDetail : goDetail,
    search : search,
    callNumber : callNumber
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

      vm.statusSelelctInfo = [];
      // 예정(모집중) , 예정(모집완료)
      vm.statusSelelctInfo.push({v: '999', k: '전체'});
      vm.statusSelelctInfo.push({v: 'A7', k: '확정'});
      vm.statusSelelctInfo.push({v: 'A8', k: '신청'});
      vm.statusSelelctInfo.push({v: 'A9', k: '요청'});

      if($stateParams.backupData){
          _.assign(vm, $stateParams.backupData);
      }
      else{
          var params = $stateParams.param;
          vm.detailBaseInfo = params.detailBaseInfo;
          vm.rcutBaseSeq = params.detailBaseInfo.RCUT_BASE_SEQ+'';
          vm.rcutBaseState = params.detailBaseInfo.RCUT_BASE_STATE;
          vm.stateKind = params.detailBaseInfo.STATE_KIND;

          search();
      }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

    /**
     * 전체 체크
     */
    function allChkFn(data){
        if(vm.allChk){
            _.forEach(data,function(obj, idx){
                obj.chk = true;
            });
        }else{
            _.forEach(data,function(obj, idx){
                obj.chk = false;
            });
        }
    }

  function search(more) {
      vm.curPage = !more ? 1 : vm.curPage+1;

      var param = {};
      param.startNo = (vm.curPage -1) * vm.viewPageCnt;				//[필수] (현재페이지 -1) * 페이지당보여질수
      param.endNo = vm.viewPageCnt;											//[필수] 페이징 페이지당보여질수
      param.rcutBaseSeq = vm.rcutBaseSeq;

      param.memberStateCode = vm.memberStateCode == '999' ? '' : vm.memberStateCode;	//[선택] '' : 전체 ,  출석(지각):A1/무단이탈:A2/기상:A3/출발:A4/출석:A5/미출석:A6/확정:A7/신청:A8/요청중:A9
      param.searchData = vm.searchData;										//[선택] 검색어(이름)
      
      // 모집중일때만 신청중 요청중나오도록
      if(vm.stateKind != 'S1' && vm.stateKind != 'S2'){
        param.stateKind = 'Y';
      }

      CompanySvc.selectRcutrRequestMemberList(param)
          .then(function(data){
              if(data.user.resultData.rcutRequestMemeberList.length > 0){

                  var tempList = data.user.resultData.rcutRequestMemeberList;

                  _.forEach(tempList, function(obj){
                      // 의상정보
                      obj.dressInfo = '';
                      try{
                          if(Number(obj.DRESS_BASE_COUNT) > 0) obj.dressInfo += '기본정장  '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_SEMI_COUNT) > 0) obj.dressInfo += '세미정장  '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_BLACK_SHOES) > 0) obj.dressInfo += '검정구두 '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_SWIMSUIT) > 0) obj.dressInfo += '수영복 '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_SCHOOL_UNIFORM) > 0) obj.dressInfo += '교복 '
                      }catch(e){}
                      try{
                          if(Number(obj.DRESS_MILITARY_BOOTS) > 0) obj.dressInfo += '군화 '
                      }catch(e){}
                      if(vm.dressInfo == '') obj.dressInfo = '없음';

                  })

                  if(!more){
                      vm.rcutRequestMemeberList = tempList;
                  }else{
                      vm.rcutRequestMemeberList = vm.rcutRequestMemeberList.concat(tempList);
                  }
                  vm.rcutRequestMemeberListCount = data.user.resultData.rcutRequestMemeberListCount;


                  // 전체 페이지 정보입력
                  vm.totPageCnt = Math.floor((vm.rcutRequestMemeberListCount / vm.viewPageCnt) + ((vm.rcutRequestMemeberListCount % vm.viewPageCnt) === 0 ? 0 : 1));
                  vm.pagingData = [];
                  for (var i = 1; i <= vm.totPageCnt; i++) {
                      vm.pagingData.push({pageNo : i});
                  }

              }else{
                  if(!more){
                      vm.rcutRequestMemeberList = [];
                      vm.rcutRequestMemeberListCount = 0;
                      vm.pagingData = [];
                  }
              }

              // 출연자 모집 상세  - 출연자 목록 보기(통계) - 총 모집 인원
              vm.requestMemberTotCnt = data.user.resultData.requestMemberTotCnt;
              // 출연자 모집 상세  - 출연자 목록 보기(통계) - 총 모집 신청 인원
              vm.requestCompleteCnt = data.user.resultData.requestCompleteCnt;
              // 출연자 모집 상세  - 출연자 목록 보기(통계) - 모집인원정보
              vm.requestCompleteInfo = data.user.resultData.requestCompleteInfo;
              // 출연자 모집 상세  - 출연자 목록 보기(통계) - 기타정보
              vm.requestCompleteEtcInfo = data.user.resultData.requestCompleteEtcInfo;

          })
          .catch(function(error){
              logger.log('fail');
              logger.error('message: ', error);
              vm.curPage = more ? vm.curPage-1 : vm.curPage;
          })
  }

    /**
     * 모집에 대한 요청 취소, 요청수락 ,수락취소
     */
    function procRequest(data, proKind){

        var chk = true;
        chk = chk && ngspublic.nullCheck(proKind , '처리종류 코드가 없습니다.');
        if(!chk) return false;

        var msg = '';
        if(proKind === 'P1'){
            // 요청취소건만 있는지 확인
            var chkFlag = true;
            if(data.REQUEST_KIND === 'R1' && data.REQUEST_STATE === 'R1'){
                chkFlag = true;
            }else{
                chkFlag = false;
            }

            if(!chkFlag) {
                $message.alert('요청 취소 대상만 선택 후 진행해주세요.');
                return false;
            }
            msg = '요청 취소 처리 하시겠습니까?';
        }
        if(proKind === 'P2'){
            var chkFlag = true;
            if(data.REQUEST_KIND === 'R2' && data.REQUEST_STATE === 'R1'){
                chkFlag = true;
            }else{
                chkFlag = false;
            }

            if(!chkFlag) {
                $message.alert('요청 수락 대상만 선택 후 진행해주세요.');
                return false;
            }

            msg = '요청 수락 처리 하시겠습니까?';
        }
        if(proKind === 'P3'){
            var chkFlag = true;
            if(data.REQUEST_STATE === 'R2'){
                chkFlag = true;
            }else{
                chkFlag = false;
            }

            // if(!chkFlag) {
            //     $message.alert('확정 취소 대상만 선택 후 진행해주세요.');
            //     return false;
            // }
            msg = '확정 취소 처리 하시겠습니까?';
        }

        var confirmResult = $message.confirm(msg);
        if(!confirmResult) return false;

        var param = {};
        param.proKind = proKind;								//[필수] 처리종류 (P1: 요청 취소 , P2: 요청수락, P3: 수락취소)
        param.requestInfoSeq = data.REQUST_INFO_SEQ;			//[필수] 모집신청일련번호

        CompanySvc.procRequest(param)
            .then(function(data){
                $message.alert('처리 되었습니다.');
                search();
            })
            .catch(function(error){
                logger.log('fail');
                logger.error('message: ' + error + '\n');
            })

    }

    function showReqPushPopup(){
        logger.log('showReqPushPopup Open');

        var list = _.filter(vm.rcutRequestMemeberList, function(obj){ return obj.chk === true; })

        if(list.length === 0){
            $message.alert('출연자를 선택 해 주세요.');
            return false;
        }

        //모달 오픈
        var modalInstance = $modal.open({
            templateUrl: 'progmc/cpReqPushP/cpReqPushP.tpl.html',
            controller: 'cpReqPushPCtrl',
            controllerAs: 'cpReqPushP',
            scope: $scope,
            resolve: {
                item: function() {
                  return {RCUT_BASE_SEQ : vm.rcutBaseSeq};
                }
            }
        });

        modalInstance.opened
            .then(function() {
                // 모달 오픈 성공
            })
            .catch(function(data) {
                alert(data);
                // 모달 오픈 실패
            });

        modalInstance.result
            .then(function(result) {
                // 정상 종료
                logger.log('showReqPushPopup Close', result.msg);

                var list = _.filter(vm.rcutRequestMemeberList, function(obj){ return obj.chk === true; })
                $rootScope.comPushSend(list, result.msg)
                    .then(function(data){

                    })
                    .catch(function(err) {

                    });

            })
            .catch(function(reason) {
                // 취소
                logger.log('showReqPushPopup Dismiss', reason);
            });
    }

    function showSchduleMngPopup(){
        logger.log('showSchduleMngPopup Open');

        //모달 오픈
        var modalInstance = $modal.open({
            templateUrl: 'progmc/cpSchduleMngP/cpSchduleMngP.tpl.html',
            controller: 'cpSchduleMngPCtrl',
            controllerAs: 'cpSchduleMngP',
            scope: $scope,
            resolve: {
                item: function() {
                  return {RCUT_BASE_SEQ : vm.rcutBaseSeq};
                }
            }
        });

        modalInstance.opened
            .then(function() {
                // 모달 오픈 성공
            })
            .catch(function(data) {
                alert(data);
                // 모달 오픈 실패
            });

        modalInstance.result
            .then(function(result) {
                // 정상 종료
                logger.log('showSchduleMngPopup Close', result);

            })
            .catch(function(reason) {
                // 취소
                logger.log('showSchduleMngPopup Dismiss', reason);
            });
    }

    /**
     * 모집종료
     */
    function rcutEndProc(){

        var confirmResult = $message.confirm('모집 종료처리하시겠습니까?');
        if(!confirmResult) return false;

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.rcutBaseSeq , 	'출연자모집정보 일련번호가 없습니다.');
        if(!chk) return false;

        var param = {};
        param.rcutBaseSeq = vm.rcutBaseSeq;						//[필수] 모집일련번호
        param.rcutBaseState = 'R2';					//[필수][공코:RCUT_BASE_STATE] R2 : 모집종료 R3 : 촬영종료

        CompanySvc.rcutEndProc(param)
            .then(function(data){
                $message.alert('모집 종료 처리 되었습니다.');
                $rootScope.goView('comp.progmc.cpRcrutList');
            })
            .catch(function(error){
                logger.log('fail');
                logger.error('message: ' + error + '\n');
            })
    }

    /**
     * 상세 페이지 이동
     */
    function goDetail(data) {
        var param = {
            rcutRequestMemeberInfo : data,
            prevViewId : 'cpViewPesnR1List'
        };
        $rootScope.goView('comp.mainc.cmPesnInfo', vm, param);
    }

    function callNumber(number){
        if(!$message.confirm($filterSet.fmtPhone(number) + '\n전화 하시겠습니까?')) return false;
        $doryCall.callNumber(number);
    }
}
