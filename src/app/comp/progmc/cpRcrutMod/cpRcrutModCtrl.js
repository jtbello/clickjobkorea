
angular
  .module('comp.progmc')
  .controller('cpRcrutModCtrl', cpRcrutModCtrl);

function cpRcrutModCtrl($log, $scope, $modal, $filterSet, $rootScope, $stateParams, $message, $env, CompanySvc, 
          ngspublic, User, ComSvc) {
    
    /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      rcutBaseSeq : '',                 // [필수] 출연자모집 seq

      chargeMemberList : [],			// 담당자 리스트
      selChargeMemeList : [],			// 선택한 담당자 리스트

      rcutDetailInfo : null,              // 조회된 출연자모집 정보

      titleGubun : '999',               // 모집 구분
      pgmReutField : '999',				// 프로그램 분야 선택
      pgmHopeCastInfo : [],				// 모집 분야

      selRcutSubInfoList : [],			// 출연인원정보 리스트
      ageMin : '999',					// 출연인원정보 - 나이
      ageMax : '999',					// 출연인원정보 - 나이
      gender : '999',					// 출연인원정보 - 성별
      peopleNum : '999',				// 출연인원정보 - 인원정보
      dressInfo : '999',				// 출연인원정보 - 의상정보
      partInfo: '',						// 출연인원정보 - 역활

      selPgmInfoSeq : '999',			// 선택한 프로그램 일련번호
      selPgmInfo : {},					// 선택한 프로그램정보
      pgmList : [{v:'999',k:'선택', s :''}],		// 등록 가능한 프로그램 목록
      pgmMeetingHourList : [{v: '999', k: '시 선택'}],				// 시간 리스트
      pgmMeetingMiniteList : [{v: '999', k: '분 선택'}],			// 분 리스트
      ageList : [{v: '999', k: '연령'}],							// 연령
      personList : [{v: '999', k: '인원'}],						// 인원

      pgmStartDtm : '',					//[필수] 촬영시작시간
      pgmEndDtm : '',					//[필수] 촬영종료시간
      pgmHopeCast : '',					//[필수][공코 : HOPE_CAST] 배역
      pgmHopeCastEtc : '',				//[선택] (배역 [공코 : SUPPORT_FIELD] 지원분야 가 P12일때 필수이고  pgmHopeCast = H6 고정)
      pgmPoint : '',					//[필수] 촬영장소
      pgmMeetingHour : '999',				//[필수] 촬영시간 (시)
      pgmMeetingMinite : '999',			//[필수] 촬영시간 (분)
      pgmSupplies : '',					//[필수] 준비물
      pgmMainScene:'',					//[필수] 주요장면
      pgmEtcInfo : '',					//[필수] 기타사항
      pgmShootKind : '',

      chargeMemberListChkVal : null,	// 추가 대상 담당자
      fileDisabled : false,				// 파일선택가능
      pgmImgAttachSeq : '',				// 대표 이미지 일련번호
      fileOrgName : '파일선택',					// 첨부파일 원본파일이름
      readFile : {},

      rcutModifyInfo : {}               // 수정이력
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      selChargeMemberList : selChargeMemberList,	// 담당자리스트 조회
      chargeMemPlus : chargeMemPlus,				// 담당자 추가
      chargeMemMin : chargeMemMin,					// 담당자 삭제
      repYnClick : repYnClick,						// 대표담당자체크

      rcutSubInfoPlus : rcutSubInfoPlus,			// 인원정보 추가
      rcutSubInfoMin : rcutSubInfoMin,				// 인원정보 삭제

      pgmHopeCastClick : pgmHopeCastClick,			// 분야 클릭

      fileAttach :fileAttach,						// 파일 전송
      fileDel: fileDel,								// 파일 삭제
      modifyRcut : modifyRcut,						// 모집 수정
      cancelFn :cancelFn							// 취소
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

      if($stateParams.param){

          vm.rcutBaseSeq = $stateParams.param.RCUT_BASE_SEQ + '';

          // 담당자 리스트 조회 및 출연자 모집 상세 조회
          selChargeMemberList();
      }

      // 시 Data 생성
      for(var i = 0; i <= 23; i++){
          vm.pgmMeetingHourList.push({v: i+'', k: i+'시'});
      }

      // 분 Data 생성
      for(var i = 0; i <= 59; i++){
          vm.pgmMeetingMiniteList.push({v: i+'', k: i+'분'});
      }

      // 연령 Data 생성
      for(var i = 1; i <= 100; i++){
          vm.ageList.push({v: i+'', k: i});
      }

      // 인원 Data 생성
      for(var i = 1; i <= 800; i++){
          vm.personList.push({v: i+'', k: i});
      }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  // 프로그램 분야에 대한 프로그램 조회
  $scope.$watch('cpRcrutMod.pgmReutField', function(newVal, oldVal, scope) {
      if(newVal == oldVal) return false;
      if(newVal == '999') {
        vm.pgmList = [{v:'999', k:'선택', s:''}];
        vm.selPgmInfoSeq = '999';
        vm.pgmHopeCast = '';
        //$('input[name=pgmHopeCast]').val('');
        vm.pgmHopeCastInfo = [];
        return false;
      }

      var param = {};
      param.pgmReutField= newVal;						//[필수][공코: PGM_REUT_FIELD] 분야
      CompanySvc.selectPgmInfo(param)
          .then(function(data){
              vm.pgmList = [{v:'999', k:'선택', s:''}];
              _.forEach(data.user.resultData.pgmInfo,function(obj){
                  vm.pgmList.push({v:obj.PGM_BASE_SEQ+'', k:obj.PGM_NAME, s:obj.PGM_REUT_FIELD});
              })

          })
          .catch(function(err){
          })

      // 프로그램분야에 대한 모집분야 조회
      var param = {};
      param.pgmReutField= newVal	;						//[필수][공코: PGM_REUT_FIELD] 분야

      CompanySvc.selectPgmHopeCast(param)
          .then(function(data){
              vm.pgmHopeCastInfo = [];
              _.forEach(data.user.resultData.pgmHopeCast,function(obj){
                  var chk= false;
                  if(obj.COMMON_INFO_VALUE1 == vm.pgmHopeCast){
                    chk = true;
                  }
                  vm.pgmHopeCastInfo.push({v:obj.COMMON_INFO_VALUE1, k:obj.COMMON_INFO_VALUE2, chk : chk});
                  
              })

          })
          .catch(function(err){
          })

  });

    // 프로그램 분야에 대한 프로그램 조회
    $scope.$watch('cpRcrutMod.readFile', function(newVal, oldVal, scope) {
        if(newVal.name == oldVal.name) return false;
        vm.fileOrgName = newVal.name;
    });

    /**
     * 대표담당자체크
     */
    function repYnClick(mem){
        _.forEach(vm.selChargeMemeList, function(obj){
            obj.repYn = false;
        })
        mem.repYn = true;
    }

    /**
     * 인원정보 추가
     */
    function pgmHopeCastClick(seq){
        vm.pgmHopeCast = seq;
    }

    /**
     * 인원정보 추가
     */
    function rcutSubInfoPlus(){

        var chk = true;

        chk = chk && ngspublic.nullCheck(vm.ageMin, 	'연령을 선택해 주세요.', '999');
        chk = chk && ngspublic.nullCheck(vm.ageMax, 	'연령을 선택해 주세요.', '999');
        chk = chk && ngspublic.nullCheck(vm.gender, 	'성별을 선택해 주세요.', '999');
        chk = chk && ngspublic.nullCheck(vm.peopleNum,'인원정보를 선택해 주세요.', '999');
        // chk = chk && ngspublic.nullCheck(vm.dressInfo,'의상정보를 선택해 주세요.', '999');
        // chk = chk && ngspublic.nullCheck(vm.partInfo, '역할정보를 입력해 주세요.');


        if(!chk) return false;

        if(Number(vm.ageMin) > Number(vm.ageMax)){
            $message.alert('시작연령이 클수는 없습니다.');
            return false;
        }

        vm.selRcutSubInfoList.push({
            ageMin : vm.ageMin,
            ageMax : vm.ageMax,
            gender : vm.gender,
            peopleNum : vm.peopleNum,
            // dressInfo : vm.dressInfo,
            // partInfo: vm.partInfo
        });

    }

    /**
     * 인원정보 삭제
     */
    function rcutSubInfoMin(index){
        var selRcutSubInfoListTemp = [];
        _.forEach(vm.selRcutSubInfoList, function(obj,idx){
            if(idx != index){
                selRcutSubInfoListTemp.push(obj);
            }
        })
        vm.selRcutSubInfoList = selRcutSubInfoListTemp;
    }

    /**
     * 모집 등록 취소
     */
    function cancelFn(){
        //$rootScope.backPage('rcutListCtrl', '/web/comp/rcut/rcutList.dory');
    }


    /**
     * 모집 등록
     */
    function modifyRcut(){

        var chk = true;

        chk = chk && ngspublic.nullCheck(vm.selPgmInfoSeq,	    '프로그램을 선택해주세요.', '999');
        chk = chk && ngspublic.dateCheck(vm.pgmStartDtm, vm.pgmEndDtm, '촬영');
        chk = chk && ngspublic.nullCheck(vm.pgmPoint, 		    '촬영장소를 입력해주세요.');
        chk = chk && ngspublic.nullCheck(vm.pgmMeetingHour,     '촬영시간 (시)을 입력해주세요.', '999');
        chk = chk && ngspublic.nullCheck(vm.pgmMeetingMinite,   '촬영시간 (분)을 입력해주세요.', '999');

        if(!chk) return false;


        vm.selPgmInfo = _.find(vm.pgmList, {v : vm.selPgmInfoSeq});

        if(vm.selPgmInfo.s == 'P12'){
            if(vm.pgmHopeCastEtc == ''){
                $message.alert('프로그램 분야[기타]는 모집분야기타를 입력하셔야합니다.');
                return false;
            }
        }else{
            var chk = ngspublic.nullCheck(vm.pgmHopeCast, 		'모집분야를 입력해주세요.', '999');
            if(!chk) return false;
        }

        // 첨부파일 첨부 확인
        if($('#file').val() != '' && vm.pgmImgAttachSeq == ''){
            $message.alert('선택하신 파일을 첨부해주세요.');
            return false;
        }

        // 담당자 선택여부확인
        if(vm.selChargeMemeList.length <= 0){
            $message.alert('담당자를 선택해주세요.');
            return false;
        }

        // 대표 담당자 선택 여부 확인
        var findCharge = _.find(vm.selChargeMemeList, {repYn: true});
        if(!findCharge){
            $message.alert('대표 담당자를 선택해주세요.');
            return false;
        }

        // 로그인한 본인이 담당자 리스트에있는지 확인
        var loginCompanyMemberSeq = User.getUserInfoList()[0].COMPANY_MEMBER_SEQ;
        var loginCharge = _.find(vm.selChargeMemeList, {v : loginCompanyMemberSeq+''});
        if(!loginCharge && !User.getAdminYn() && !User.getComAdminYn()){
            $message.alert('로그인 하신 담당자가 포함되지 않았습니다.');
            return false;
        }

        // 모집출연인원정보 확인
        if(vm.selRcutSubInfoList.length <= 0){
            $message.alert('모집출연인원정보를 선택해주세요.');
            return false;
        }


        var confirmResult =$message.confirm('수정하시겠습니까?');
        if(!confirmResult) return false;
        
        if(vm.pgmMeetingHour.length < 2){
          vm.pgmMeetingHour = '0' + vm.pgmMeetingHour;
        }
        
        if(vm.pgmMeetingMinite.length < 2){
          vm.pgmMeetingMinite = '0' + vm.pgmMeetingMinite;
        }
      


        var param = {};
        param.rcutBaseSeq = vm.rcutBaseSeq;
        param.pgmBaseSeq = vm.selPgmInfo.v;																			//[필수] 프로그램 일련번호
        param.pgmStartDtm = _.isEmpty(vm.pgmStartDtm) ? '' : moment(vm.pgmStartDtm +' ' +vm.pgmMeetingHour + ':' + vm.pgmMeetingMinite + ':00').format('YYYY-MM-DD HH:mm:ss'); //[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        param.pgmEndDtm = _.isEmpty(vm.pgmEndDtm) ? '' : moment(vm.pgmEndDtm +' 23:59:00').format('YYYY-MM-DD HH:mm:ss')      //[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]

        // 프로그램 모집 분야가 기타일때와 아닐때 분기
        if(vm.selPgmInfo.s == 'P12'){
            param.pgmHopeCast = 'H6';																						//[필수][공코 : HOPE_CAST] 배역
            param.pgmHopeCastEtc = vm.pgmHopeCastEtc;																		//[선택] (배역 [공코 : SUPPORT_FIELD] 지원분야 가 P12일때 필수이고  pgmHopeCast = H6 고정)
        }else{
            param.pgmHopeCast = vm.pgmHopeCast;																			//[필수][공코 : HOPE_CAST] 배역
            param.pgmHopeCastEtc = '';																					//[선택] (배역 [공코 : SUPPORT_FIELD] 지원분야 가 P12일때 필수이고  pgmHopeCast = H6 고정)
        }

        param.titleGubun = vm.titleGubun == '999' ? '' : vm.titleGubun;         //[필수] 구분
        param.pgmPoint = vm.pgmPoint;											//[필수] 촬영장소
        param.pgmMeetingHour = vm.pgmMeetingHour;								//[필수] 촬영시간 (시)
        param.pgmMeetingMinite = vm.pgmMeetingMinite;							//[필수] 촬영시간 (분)
        param.pgmSupplies = vm.pgmSupplies;									//[필수] 준비물
        param.pgmMainScene = vm.pgmMainScene;									//[필수] 주요장면
        param.pgmEtcInfo = vm.pgmEtcInfo;										//[필수] 기타사항
        param.pgmShootKind = vm.pgmShootKind;
        
        if(!_.isEmpty(vm.pgmImgAttachSeq)){
            param.pgmImgAttachSeq = vm.pgmImgAttachSeq;						//[선택] 대표이미지 파일일련번호
        }

        // 출연인원정보
        var rcutSubInfoList = [];
        _.forEach(vm.selRcutSubInfoList, function(obj){
            var rcutSbuInfo = {};
            rcutSbuInfo.ageMin = obj.ageMin;								//[필수] 시작나이
            rcutSbuInfo.ageMax = obj.ageMax;								//[필수] 종료나이
            rcutSbuInfo.gender = obj.gender;								//[필수][공코: GENDER] 성별
            rcutSbuInfo.peopleNum = obj.peopleNum;						//[필수] 인원수
            // rcutSbuInfo.dressInfo = obj.dressInfo;						//[필수][공코 : DRESS_INFO] 의상
            // rcutSbuInfo.partInfo = obj.partInfo;							//[필수] 역활
            rcutSubInfoList.push(rcutSbuInfo);
        })
        param.rcutSubInfoList = rcutSubInfoList;							//[필수] 담당자정보

        // 담당자정보
        param.companyMemberSeq = findCharge.v;					//[필수] 대표 담당자 일련번호
        var chargeList = [];
        _.forEach(vm.selChargeMemeList, function(obj){
            chargeList.push({chargeCompanyMemberSeq : obj.v, companyMemberSeq : User.getUserInfoList()[0].COMPANY_MEMBER_SEQ});
        })
        param.chargeList = chargeList;							//[필수] 담당자정보


        CompanySvc.modifyRcut(param)
            .then(function(data){
                $message.alert('정상적으로 수정되었습니다.');
                $rootScope.prevBtn();
                //$rootScope.backPage('rcutListCtrl', '/web/comp/rcut/rcutList.dory');
            })
            .catch(function(err){
                //selectRcutDetailInfo();
                if(vm.pgmMeetingHour.startsWith('0')){
                    vm.pgmMeetingHour = vm.pgmMeetingHour.replace('0','');
                }

                if(vm.pgmMeetingMinite.startsWith('0')){
                    vm.pgmMeetingMinite = vm.pgmMeetingMinite.replace('0','');
                }
            })

    }

    /**
     * 파일삭제
     */
    function fileDel(){

        var chk = ngspublic.nullCheck(vm.pgmImgAttachSeq,       '파일정보가 없습니다.');
        if(!chk){
            $('#file').val('');
            return false;
        }

        var confirmResult =$message.confirm('정말 삭제하시겠습니까?');
        if(!confirmResult) return false;

        $("#file").val("");
        vm.fileDisabled = false;

        var param = {};
        param.attachSeq = vm.pgmImgAttachSeq;     // [필수]파일등록 후 결과값에 파일 일련번호
        param.fileOrgName = vm.fileOrgName;     // [필수]파일등록 후 결과값에 파일 원본파일이름

        ComSvc.fileDelete(param)
            .then(function(data){
                vm.pgmImgAttachSeq = '';
                vm.fileOrgName = '파일선택';
                $message.alert('정상적으로 삭제되었습니다.');
            })
            .catch(function(err){
              vm.fileOrgName = '파일선택';
              vm.pgmImgAttachSeq = '';
            })
    }


    /**
     * 파일전송
     */
    function fileAttach(){
        vm.fileDisabled = true;
        $rootScope.requestCount++;
        $("#ajaxform").ajaxForm({
            url : $env.endPoint.service + "/api/com/fileUpload.login",
            enctype : "multipart/form-data",
            dataType : "json",
            beforeSubmit: function (data, frm, opt) {
                var objFind = _.find(data, {type : 'file'});
                if(objFind){
                    if(!_.isNull(objFind.value) && objFind.value != ""){
                        return true;
                    }
                }
                vm.fileDisabled = false;
                $rootScope.requestCount--;
                $message.alert('파일 선택 후 진행 해주세요.');
                return false;
            },
            error : function(){
              $scope.$apply(function(){
                vm.fileDisabled = false;
                $rootScope.requestCount--;
              });
                $message.alert("파일 전송 실패") ;
            },
            success : function(result){
                $scope.$apply(function(){
                  $rootScope.requestCount--;
                });
                vm.pgmImgAttachSeq = result.user.resultData.attachSeq;
                vm.fileOrgName = result.user.resultData.orgName;
                $message.alert("파일 전송 성공");
            }
        });

        $("#ajaxform").submit();
    }

    /**
     * 담당자 삭제
     */
    function chargeMemMin(mem){

        vm.selChargeMemeList = _.reject(vm.selChargeMemeList,{v : mem.v});
    }

    /**
     * 담당자 추가
     */
    function chargeMemPlus(){
        if(vm.chargeMemberListChkVal == null){
            $message.alert('추가 대상 담당자가 없습니다.');
            return false;
        }

        var findObj = _.find(vm.selChargeMemeList, {v : vm.chargeMemberListChkVal});
        if(findObj){
            $message.alert('이미 추가된 담당자입니다.');
            return false;
        }

        vm.selChargeMemeList.push(_.find(vm.chargeMemberList, {v : vm.chargeMemberListChkVal}));
    }


    /**
     * 담당자 리스트 조회
     */
    function selChargeMemberList(){

        var param = {};

        CompanySvc.selectCompanyMemberListInfo(param)
            .then(function(data){
                if(data.user.resultData.companyMemberList.length > 0){
                    _.forEach(data.user.resultData.companyMemberList, function(obj){
                        vm.chargeMemberList.push({k:obj.COMPANY_MEMBER_POSITION + ' ' + obj.COMPANY_MEMBER_NAME + '('+$filterSet.fmtPhone(obj.COMPANY_MEMBER_PHONE_NO)+')',v:obj.COMPANY_MEMBER_SEQ+''});
                    })
                    vm.chargeMemberListChkVal = vm.chargeMemberList[0].v;

                    // 출연자 모집 상세 조회
                    selectRcutDetailInfo();

                }else{
                    vm.chargeMemberList = [];
                    $message.alert('담당자 리스트 조회에 실패했습니다.');
                }
            })
            .catch(function(err){
                $message.alert('담당자 리스트 조회에 실패했습니다.');
            })
    }

    /**
     * 출연자 모집 상세 조회
     */
    function selectRcutDetailInfo(){

        var chk = true;
        chk = ngspublic.nullCheck(vm.rcutBaseSeq , '출연자모집정보 일련번호가 없습니다.');
        if(!chk) return false;

        var param = {};
        param.rcutBaseSeq = vm.rcutBaseSeq;					//[필수] 모집일련번호

        CompanySvc.selectRcutDetailInfo(param)
            .then(function(data){
                vm.rcutDetailInfo = data.user.resultData;

                // 기본정보 세팅
                vm.titleGubun		= vm.rcutDetailInfo.detailBaseInfo.TITLE_GUBUN;
			  	if(vm.titleGubun == '') vm.titleGubun = '999';
                vm.pgmReutField = vm.rcutDetailInfo.detailBaseInfo.PGM_REUT_FIELD;
                vm.selPgmInfoSeq = vm.rcutDetailInfo.detailBaseInfo.PGM_BASE_SEQ+'';

                vm.pgmStartDtm  = moment(vm.rcutDetailInfo.detailBaseInfo.PGM_START_DTM2, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');			//[필수] 촬영시작시간
                vm.pgmEndDtm 	= moment(vm.rcutDetailInfo.detailBaseInfo.PGM_END_DTM2, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD');			//[필수] 촬영종료시간
                vm.pgmHopeCast	= vm.rcutDetailInfo.detailBaseInfo.PGM_HOPE_CAST;							//[필수][공코 : HOPE_CAST] 배역
                _.forEach(vm.pgmHopeCastInfo, function(obj){
                  if(obj.v == vm.pgmHopeCast){
                    obj.chk = true;
                  }else{
                    obj.chk = false;
                  }
                })
                
                vm.pgmHopeCastEtc 	= vm.rcutDetailInfo.detailBaseInfo.PGM_HOPE_CAST_ETC;					//[선택] (배역 [공코 : SUPPORT_FIELD] 지원분야 가 P12일때 필수이고  pgmHopeCast = H6 고정)
                vm.pgmPoint 		= vm.rcutDetailInfo.detailBaseInfo.PGM_POINT;							//[필수] 촬영장소
                vm.pgmMeetingHour 	= vm.rcutDetailInfo.detailBaseInfo.PGM_MEETING_HOUR+'';					//[필수] 촬영시간 (시)
                if($filterSet.nullChk(vm.pgmMeetingHour) != ''){
                    vm.pgmMeetingHour = Number(vm.pgmMeetingHour) + '';
                }
                vm.pgmMeetingMinite = vm.rcutDetailInfo.detailBaseInfo.PGM_MEETING_MINITE+'';				//[필수] 촬영시간 (분)
                if($filterSet.nullChk(vm.pgmMeetingMinite) != ''){
                    vm.pgmMeetingMinite = Number(vm.pgmMeetingMinite) + '';
                }
                vm.pgmSupplies 		= vm.rcutDetailInfo.detailBaseInfo.PGM_SUPPLIES;						//[필수] 준비물
                vm.pgmMainScene		= vm.rcutDetailInfo.detailBaseInfo.PGM_MAIN_SCENE;						//[필수] 주요장면
                vm.pgmEtcInfo 		= vm.rcutDetailInfo.detailBaseInfo.PGM_ETC_INFO;						//[필수] 기타사항
                vm.pgmShootKind   = vm.rcutDetailInfo.detailBaseInfo.PGM_SHOOT_KIND;         


                // 출연자인원정보세팅
                vm.selRcutSubInfoList = [];				// 출연인원정보 리스트
                _.forEach(vm.rcutDetailInfo.subPeopleInfo, function(obj){
                    vm.selRcutSubInfoList.push({
                        ageMin : obj.AGE_MIN+'',
                        ageMax : obj.AGE_MAX+'',
                        gender : obj.GENDER,
                        peopleNum : obj.PEOPLE_NUM+'',
                        // dressInfo : obj.DRESS_INFO,
                        // partInfo: obj.PART_INFO
                    });
                });

                // 담당자 정보 세팅
                vm.selChargeMemeList = [];
                _.forEach(vm.rcutDetailInfo.chargeInfo, function(obj){
                    var chkCharge = false;
                    if(vm.rcutDetailInfo.detailBaseInfo.COMPANY_MEMBER_SEQ2 == obj.COMPANY_MEMBER_SEQ) chkCharge = true;
                    var hp = '';
                    if(!_.isEmpty($filterSet.fmtPhone(obj.COMPANY_MEMBER_PHONE_NO))) hp =  $filterSet.fmtPhone(obj.COMPANY_MEMBER_PHONE_NO);
                    vm.selChargeMemeList.push({k:obj.COMPANY_MEMBER_POSITION + ' ' + obj.COMPANY_MEMBER_NAME + '('+hp+')',v:obj.COMPANY_MEMBER_SEQ+'', repYn :chkCharge});
                });

                // 대표이미지정보
                if(!_.isNull(vm.rcutDetailInfo.imgAttachInfo) && !_.isEmpty(vm.rcutDetailInfo.imgAttachInfo)){
                    if(vm.rcutDetailInfo.imgAttachInfo.FILE_ORG_NAME != undefined){
                        vm.pgmImgAttachSeq = vm.rcutDetailInfo.imgAttachInfo.ATTACH_SEQ;
                        vm.fileOrgName = vm.rcutDetailInfo.imgAttachInfo.FILE_ORG_NAME;
                        vm.fileDisabled = true;
                    }else{
                        vm.pgmImgAttachSeq = '';
                        vm.fileOrgName = '파일선택';
                        vm.fileDisabled = false;
                    }
                }

                // 수정이력
                if(!_.isNull(vm.rcutDetailInfo.modifyInfo) && !_.isEmpty(vm.rcutDetailInfo.modifyInfo)){
                    vm.rcutModifyInfo = vm.rcutDetailInfo.modifyInfo;
                }

            })
            .catch(function(err){
            })

    }
}
