
angular
  .module('comp.progmc')
  .controller('cpViewPesnR2ListCtrl', cpViewPesnR2ListCtrl);

function cpViewPesnR2ListCtrl($log, $scope, $modal, $rootScope, $stateParams, $q, CompanySvc, ngspublic, $message, $filterSet, $doryCall) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      // 파라미터
      rcutBaseSeq : '',                 // 모집일련번호
      rcutBaseState : '',                 // 모집상태
      detailBaseInfo : {},              // 모집상세정보

      // 변수
      memberStateCode : '999',

      requestMemberTotCnt : 0,			// 출연자 모집 상세  - 출연자 목록 보기(통계) - 총 모집 인원
      requestCompleteCnt : 0,			// 출연자 모집 상세  - 출연자 목록 보기(통계) - 총 모집 신청 인원
      requestCompleteInfo : [],			// 출연자 모집 상세  - 출연자 목록 보기(통계) - 모집인원정보
      requestCompleteEtcInfo : [],		// 출연자 모집 상세  - 출연자 목록 보기(통계) - 모집인원정보

      // 실제로 보이는 탭의 변수
      viewTab : {
          allChk : false,

          searchData : '',                  // 검색 테이터

          totPgmName : '',					// 프로그램 전체명
          statusSelelctInfo : [],			// 조회 상태 셀렉트박스 정보
          searchSelelctInfo : [],			// 조회 검색 셀렉트박스 정보

          rcutRequestMemeberList : [],					// 출연자 보기 리스트
          rcutRequestMemeberListCount : 0,	// 출연자 보기 전체 카운트

          lastSchedulePushInfo : [],		// 스케줄변경알림정보

          attendNoLeaveState : '',			// 미출석,무단이탈여부
          attendNoLeaveNote : '',			// 미출석 무단이탈 관리자확인사항

          attendEndInfo : {},				// 조회된 출석정보
          companyMemberAttendCode : false,	//
          companyMemberAttendDtm : '',		// 출석일
          companyMemberAttendHour : '999',	// 출석 시
          companyMemberAttendMinite : '999',// 출석 분
          companyMemberAttendPoint : '',	// 출석장소

          companyMemberEndDtm : '',			// 종료일
          companyMemberEndHour : '999',		// 종료 시
          companyMemberEndMinite : '999',	// 종료 분
          companyMemberEndPoint : '',		// 종료장소
          reviewCntn : '',					// 평가

          // 정렬 관련
          orderKind1 : 'O1',
          orderKind2 : 'O1',

          // 페이징 관련
          curPage : 1,						// 현재페이지
          viewPageCnt : 10,					// 페이지당보여질수
          pagingData : [],					// 페이지번호 모듬
          totPageCnt : 0					// 전체페이지수
      },
      tab999 : {},                           // 탭 1
      tabA3 : {},                           // 탭 2
      tabA4 : {},                           // 탭 3
      tabA5 : {}                            // 탭 4
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      allChkFn : allChkFn,
      search : search,
      selectMemberStateCode : selectMemberStateCode,
      procRequest : procRequest,
      attendProc : attendProc,
      showNoLeavePopup : showNoLeavePopup,
      showReqPushPopup : showReqPushPopup,
      showSchduleMngPopup : showSchduleMngPopup,
      rcutEmergency : rcutEmergency,
      goDetail : goDetail,
      callNumber : callNumber
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

      vm.tab999 = _.clone(vm.viewTab);
      vm.tabA3 = _.clone(vm.viewTab);
      vm.tabA4 = _.clone(vm.viewTab);
      vm.tabA5 = _.clone(vm.viewTab);

      if($stateParams.backupData){
          _.assign(vm, $stateParams.backupData);
      }
      else{
          var params = $stateParams.param;
          vm.detailBaseInfo = params.detailBaseInfo;
          vm.rcutBaseSeq = params.detailBaseInfo.RCUT_BASE_SEQ+'';
          vm.rcutBaseState = params.detailBaseInfo.RCUT_BASE_STATE;
          vm.stateKind = params.detailBaseInfo.STATE_KIND;

          search();
      }
    
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

    /**
     * 멤버상태 코드 선택 ( 999 : 전체, A3 : 기상, A4 : 출발, A5 : 출석 )
     */
    function selectMemberStateCode (memberStateCode){
        // 현재 탭 변수 저장
        vm['tab'+vm.memberStateCode] = _.clone(vm.viewTab);
        // 다음 탭 변수 불러오기
        vm.viewTab = _.clone(vm['tab'+memberStateCode]);
        // 상태코드 설정
        vm.memberStateCode = memberStateCode;

        vm.viewTab.allChk = false;

        search();
    }

    /**
     * 전체 체크
     */
    function allChkFn(data){
        if(vm.viewTab.allChk){
            _.forEach(data,function(obj, idx){
                obj.chk = true;
            });
        }else{
            _.forEach(data,function(obj, idx){
                obj.chk = false;
            });
        }
    }

    function search(more) {
        vm.viewTab.curPage = !more ? 1 : vm.viewTab.curPage+1;

        var param = {};
        param.startNo = (vm.viewTab.curPage -1) * vm.viewTab.viewPageCnt;				//[필수] (현재페이지 -1) * 페이지당보여질수
        param.endNo = vm.viewTab.viewPageCnt;											//[필수] 페이징 페이지당보여질수
        param.rcutBaseSeq = vm.rcutBaseSeq;

//        param.memberStateCode = vm.memberStateCode == '999' ? '' : vm.memberStateCode;	//[선택] '' : 전체 ,  출석(지각):A1/무단이탈:A2/기상:A3/출발:A4/출석:A5/미출석:A6/확정:A7/신청:A8/요청중:A9
        param.memberStateCode2 = vm.memberStateCode == '999' ? '' : vm.memberStateCode;  //[선택] '' : 전체 ,  출석(지각):A1/무단이탈:A2/기상:A3/출발:A4/출석:A5/미출석:A6/확정:A7/신청:A8/요청중:A9
        param.searchData = vm.viewTab.searchData;										//[선택] 검색어(이름)

        if(vm.stateKind != 'S1' && vm.stateKind != 'S2'){
          param.stateKind = 'Y';
        }
        
        CompanySvc.selectRcutrRequestMemberList(param)
            .then(function(data){
                if(data.user.resultData.rcutRequestMemeberList.length > 0){

                    var tempList = data.user.resultData.rcutRequestMemeberList;

                    _.forEach(tempList, function(obj){
                        // 의상정보
                        obj.dressInfo = '';
                        try{
                            if(Number(obj.DRESS_BASE_COUNT) > 0) obj.dressInfo += '기본정장  '
                        }catch(e){}
                        try{
                            if(Number(obj.DRESS_SEMI_COUNT) > 0) obj.dressInfo += '세미정장  '
                        }catch(e){}
                        try{
                            if(Number(obj.DRESS_BLACK_SHOES) > 0) obj.dressInfo += '검정구두 '
                        }catch(e){}
                        try{
                            if(Number(obj.DRESS_SWIMSUIT) > 0) obj.dressInfo += '수영복 '
                        }catch(e){}
                        try{
                            if(Number(obj.DRESS_SCHOOL_UNIFORM) > 0) obj.dressInfo += '교복 '
                        }catch(e){}
                        try{
                            if(Number(obj.DRESS_MILITARY_BOOTS) > 0) obj.dressInfo += '군화 '
                        }catch(e){}
                        if(vm.viewTab.dressInfo == '') obj.dressInfo = '없음';

                    })

                    if(!more){
                        vm.viewTab.rcutRequestMemeberList = tempList;
                    }else{
                        vm.viewTab.rcutRequestMemeberList = vm.viewTab.rcutRequestMemeberList.concat(tempList);
                    }
                    vm.viewTab.rcutRequestMemeberListCount = data.user.resultData.rcutRequestMemeberListCount;

                    // 전체 페이지 정보입력
                    vm.viewTab.totPageCnt = Math.floor((vm.viewTab.rcutRequestMemeberListCount / vm.viewTab.viewPageCnt) + ((vm.viewTab.rcutRequestMemeberListCount % vm.viewTab.viewPageCnt) === 0 ? 0 : 1));
                    vm.viewTab.pagingData = [];
                    for (var i = 1; i <= vm.viewTab.totPageCnt; i++) {
                        vm.viewTab.pagingData.push({pageNo : i});
                    }

                }else{
                    if(!more){
                        vm.viewTab.rcutRequestMemeberList = [];
                        vm.viewTab.rcutRequestMemeberListCount = 0;
                        vm.viewTab.pagingData = [];
                    }
                }

                // 출연자 모집 상세  - 출연자 목록 보기(통계) - 총 모집 인원
                vm.requestMemberTotCnt = data.user.resultData.requestMemberTotCnt;
                // 출연자 모집 상세  - 출연자 목록 보기(통계) - 총 모집 신청 인원
                vm.requestCompleteCnt = data.user.resultData.requestCompleteCnt;
                // 출연자 모집 상세  - 출연자 목록 보기(통계) - 모집인원정보
                vm.requestCompleteInfo = data.user.resultData.requestCompleteInfo;
                // 출연자 모집 상세  - 출연자 목록 보기(통계) - 기타정보
                vm.requestCompleteEtcInfo = data.user.resultData.requestCompleteEtcInfo;
                
                // 기상
                vm.requestCompleteEtcInfoA1 = _.find(data.user.resultData.requestCompleteEtcInfo, {KIND : 'A1'}).CNT;
                // 출발
                vm.requestCompleteEtcInfoA2 = _.find(data.user.resultData.requestCompleteEtcInfo, {KIND : 'A2'}).CNT;
                // 출석
                vm.requestCompleteEtcInfoA3 = _.find(data.user.resultData.requestCompleteEtcInfo, {KIND : 'A3'}).CNT;
                // 무단이탈
                vm.requestCompleteEtcInfoA4 = _.find(data.user.resultData.requestCompleteEtcInfo, {KIND : 'A4'}).CNT;
                // 미출석
                vm.requestCompleteEtcInfoA5 = _.find(data.user.resultData.requestCompleteEtcInfo, {KIND : 'A5'}).CNT;
                

            })
            .catch(function(error){
                logger.log('fail');
                logger.error('message: ', error);
                vm.viewTab.curPage = more ? vm.viewTab.curPage-1 : vm.viewTab.curPage;
            })
    }

    /**
     * 모집에 대한 요청 취소, 요청수락 ,수락취소
     */
    function procRequest(data, proKind){

        var chk = true;
        chk = chk && ngspublic.nullCheck(proKind , '처리종류 코드가 없습니다.');
        if(!chk) return false;

        var msg = '';

        if(proKind === 'P3'){
            var chkFlag = true;
            if(data.REQUEST_STATE === 'R2'){
                chkFlag = true;
            }else{
                chkFlag = false;
            }

            // if(!chkFlag) {
            //     $message.alert('확정 취소 대상만 선택 후 진행해주세요.');
            //     return false;
            // }
            msg = '확정 취소 처리 하시겠습니까?';
        }

        var confirmResult = $message.confirm(msg);
        if(!confirmResult) return false;

        var param = {};
        param.proKind = proKind;								//[필수] 처리종류 (P1: 요청 취소 , P2: 요청수락, P3: 수락취소)
        param.requestInfoSeq = data.REQUST_INFO_SEQ;			//[필수] 모집신청일련번호

        CompanySvc.procRequest(param)
            .then(function(data){
                $message.alert('처리 되었습니다.');
                search();
            })
            .catch(function(error){
                logger.log('fail');
                logger.error('message: ' + error + '\n');
            })

    }

    /**
     * 출석 종료
     */
    function attendProc(kind){

        var list = _.filter(vm.viewTab.rcutRequestMemeberList, function(obj){ return obj.chk === true; })

        if(list.length === 0){
            $message.alert('출연자를 선택 해 주세요.');
            return false;
        }

        $rootScope.requestCount++;
        $rootScope.searchAddrByGeocode()
            .then(function(data){
                $rootScope.requestCount--;
                logger.debug('searchAddrByGeocode', data);
                if(data.status.code === 0){
                    let region = data.results[0].region;    // 현재 주소
                    let point = region.area1.name + ' ' + region.area2.name + ' ' + region.area3.name;

                    var msg = '';
                    if(kind == 'A1'){
                        msg = '출석(지각) 처리하시겠습니까?';
                    }else if(kind == 'A2'){
                        msg = '출석 처리하시겠습니까?';
                    }else{
                        msg = '종료 처리하시겠습니까?';
                    }

                    var confirmResult = $message.confirm(msg);
                    if (!confirmResult) return false;

                    var list = _.filter(vm.viewTab.rcutRequestMemeberList, function(obj){ return obj.chk === true; })

                    var promisesArray = [];
                    _.forEach(list,function(obj){
                        promisesArray.push(updateAttendEndInfo(obj, kind, point));
                    });

                    $q.all(promisesArray)
                        .then(function(result){
                            $message.alert('정상적으로 처리 되었습니다.');
                            init(true);
                        })
                        .catch(function(err){
                            $message.alert(err);
                            init(true);
                        });
                }else{
                    $message.alert('GPS상태 및 권한을 확인해주세요.');
                }

            })
            .catch(function(d){
                logger.debug('searchAddrByGeocode err', d);
                $message.alert(d.message);
                return false;
            });

    }

    function updateAttendEndInfo(obj, kind, point){
        var d = $q.defer();

        var param = {};
        if(kind === 'A1'){
            param.attendNoState = "A3";                                                       //[필수][공코:ATTEND_NO_STATE] 출석,지각 코드 ( 출석,종료 : A1, 지각: A3)
            param.companyMemberAttendDtm = moment().format('YYYY-MM-DD HH:mm:ss');            //[필수] 출석일시 [일시 패턴 : 0000-00-00 00:00:00]
            param.companyMemberAttendPoint = point;                                           //[필수] 출석장소
        }else if(kind === 'A2'){
            param.attendNoState = "A1";                                                       //[필수][공코:ATTEND_NO_STATE] 출석,지각 코드 ( 출석,종료 : A1, 지각: A3)
            param.companyMemberAttendDtm = moment().format('YYYY-MM-DD HH:mm:ss');            //[필수] 출석일시 [일시 패턴 : 0000-00-00 00:00:00]
            param.companyMemberAttendPoint = point;                                           //[필수] 출석장소
        }else{
            param.attendNoState = "A1";                                                       //[필수][공코:ATTEND_NO_STATE] 출석,지각 코드 ( 출석,종료 : A1, 지각: A3)
            param.companyMemberEndDtm = moment().format('YYYY-MM-DD HH:mm:ss');               //[필수] 종료일시 [일시 패턴 : 0000-00-00 00:00:00]
            param.companyMemberEndPoint = point;                                              //[필수] 종료장소
        }
        param.requestInfoSeq = obj.REQUST_INFO_SEQ+'';                //[필수] 모집신청일련번호


        CompanySvc.updateAttendEndInfo(param)
            .then(function(data){
                // $message.alert('정상적으로 처리 되었습니다.');
                // init(true);
                d.resolve(data);
            })
            .catch(function(err){
                d.reject(err);
            });
        return d.promise;
    }

    /**
     * 미출석, 무단이탈 팝업
     */
    function showNoLeavePopup(attendNoLeaveState) {
        logger.log('showNoLeavePopup Open');

        var list = _.filter(vm.viewTab.rcutRequestMemeberList, function(obj){ return obj.chk === true; })

        if(list.length === 0){
            $message.alert('출연자를 선택 해 주세요.');
            return false;
        }

        var param = {};
        param.attendNoLeaveState = attendNoLeaveState; //[필수][공코:ATTEND_NO_LEAVE_STATE] 출석,지각 코드 ( 미출석 : A1, 무단이탈: A2)
        param.requestInfoSeqList = [];
        _.forEach(list,function(obj){
            param.requestInfoSeqList.push(obj.REQUST_INFO_SEQ);
        });

        //모달 오픈
        var modalInstance = $modal.open({
            templateUrl: 'progmc/cpNoLeaveP/cpNoLeaveP.tpl.html',
            controller: 'cpNoLeavePCtrl',
            controllerAs: 'cpNoLeaveP',
            scope: $scope,
            resolve: {
                item: function() {
                    return param;
                }
            }
        });

        modalInstance.opened
            .then(function() {
                // 모달 오픈 성공
            })
            .catch(function(data) {
                alert(data);
                // 모달 오픈 실패
            });

        modalInstance.result
            .then(function(result) {
                // 정상 종료
                logger.log('showNoLeavePopup Close', result);
                init(true);
            })
            .catch(function(reason) {
                // 취소
                logger.log('showNoLeavePopup Dismiss', reason);
            });
    }

    function showReqPushPopup(){
        logger.log('showReqPushPopup Open');

        var list = _.filter(vm.viewTab.rcutRequestMemeberList, function(obj){ return obj.chk === true; })

        if(list.length === 0){
            $message.alert('출연자를 선택 해 주세요.');
            return false;
        }

        //모달 오픈
        var modalInstance = $modal.open({
            templateUrl: 'progmc/cpReqPushP/cpReqPushP.tpl.html',
            controller: 'cpReqPushPCtrl',
            controllerAs: 'cpReqPushP',
            scope: $scope,
            resolve: {
                item: function() {
                  return {RCUT_BASE_SEQ : vm.detailBaseInfo.RCUT_BASE_SEQ};
                }
            }
        });

        modalInstance.opened
            .then(function() {
                // 모달 오픈 성공
            })
            .catch(function(data) {
                alert(data);
                // 모달 오픈 실패
            });

        modalInstance.result
            .then(function(result) {
                // 정상 종료
                logger.log('showReqPushPopup Close', result.msg);

                var list = _.filter(vm.viewTab.rcutRequestMemeberList, function(obj){ return obj.chk === true; })
                $rootScope.comPushSend(list, result.msg, vm.detailBaseInfo.RCUT_BASE_SEQ)
                    .then(function(data){

                    })
                    .catch(function(err) {

                    });

            })
            .catch(function(reason) {
                // 취소
                logger.log('showReqPushPopup Dismiss', reason);
            });
    }

    function showSchduleMngPopup(){
        logger.log('showSchduleMngPopup Open');

        //모달 오픈
        var modalInstance = $modal.open({
            templateUrl: 'progmc/cpSchduleMngP/cpSchduleMngP.tpl.html',
            controller: 'cpSchduleMngPCtrl',
            controllerAs: 'cpSchduleMngP',
            scope: $scope,
            resolve: {
                item: function() {
                    return {RCUT_BASE_SEQ : vm.detailBaseInfo.RCUT_BASE_SEQ};
                }
            }
        });

        modalInstance.opened
            .then(function() {
                // 모달 오픈 성공
            })
            .catch(function(data) {
                alert(data);
                // 모달 오픈 실패
            });

        modalInstance.result
            .then(function(result) {
                // 정상 종료
                logger.log('showSchduleMngPopup Close', result);

            })
            .catch(function(reason) {
                // 취소
                logger.log('showSchduleMngPopup Dismiss', reason);
            });
    }

    /**
     * 긴급투입
     */
    function rcutEmergency(){
        $rootScope.goView('comp.mainc.cmMain', vm, {emergYn : 'Y'});
    }

    /**
     * 상세 페이지 이동
     */
    function goDetail(data) {
        var param = {
            rcutRequestMemeberInfo : data,
            prevViewId : 'cpViewPesnR2List'
        };
        $rootScope.goView('comp.mainc.cmPesnInfo', vm, param);
    }

    function callNumber(number){
        if(!$message.confirm($filterSet.fmtPhone(number) + '\n전화 하시겠습니까?')) return false;
        $doryCall.callNumber(number);
    }
}
