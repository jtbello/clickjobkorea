
angular
  .module('comp.progmc')
  .controller('cpNoLeavePCtrl', cpNoLeavePCtrl);

function cpNoLeavePCtrl($log, $rootScope, $scope, $modal, $modalInstance, item, $q, $message, ngspublic, CompanySvc) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        // 파라미터
        requestInfoSeqList : item.requestInfoSeqList,            //[필수] 모집신청일련번호
        attendNoLeaveState : item.attendNoLeaveState,    //[필수][공코:ATTEND_NO_LEAVE_STATE] 출석,지각 코드 ( 미출석 : A1, 무단이탈: A2)

        // 변수
        attendNoLeaveNote : ''		           //[필수] 미출석 및 무단이탈 관리자 확인사항
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        confirm : confirm
    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 출연자 리스트 조회  - 미출석, 무단이탈 처리
     */
    function confirm(){

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.attendNoLeaveState , 	'코드를 입력해주세요.');
        chk = chk && ngspublic.nullCheck(vm.attendNoLeaveNote , 	'관리자 확인사항을 입력해주세요.');
        if(!chk) return false;

        var promisesArray = [];
        _.forEach(vm.requestInfoSeqList,function(obj){
            promisesArray.push(updateNoLeaveState(obj));
        });

        $q.all(promisesArray)
            .then(function(result){
                $message.alert('정상적으로 처리 되었습니다.');
                $modalInstance.close();
            })
            .catch(function(err){
                $message.alert('처리 중 오류가 발생되었습니다.');
            });

    }

    function updateNoLeaveState(obj){
        var d = $q.defer();

        var param = {};
        param.attendNoLeaveState = vm.attendNoLeaveState;	//[필수][공코:ATTEND_NO_LEAVE_STATE] 출석,지각 코드 ( 미출석 : A1, 무단이탈: A2)
        param.attendNoLeaveNote = vm.attendNoLeaveNote;	//[필수] 미출석 및 무단이탈 관리자 확인사항
        param.requestInfoSeq = obj + '';				//[필수] 모집신청일련번호

        CompanySvc.updateNoLeaveState(param)
            .then(function(data){
                // $message.alert('정상적으로 처리 되었습니다.');
                // $modalInstance.close();
                d.resolve(data);
            })
            .catch(function(err){
                d.reject(err);
            });

        return d.promise;
    }
}
