
angular
  .module('comp.progmc')
  .controller('cpProgmModCtrl', cpProgmModCtrl);

function cpProgmModCtrl($log, $scope, $state,$rootScope, $stateParams, $filterSet, $message, User, CompanySvc, ComSvc, ngspublic, $env) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      pgmBaseSeq : '',                  //[필수] 프로그램 seq

      chargeMemberList : [],			// 담당자 리스트
      selChargeMemeList : [],			// 선택한 담당자 리스트
      chargeMemberListChkVal : null,	// 추가 대상 담당자
      fileDisabled : false,				// 파일선택가능
      pgmImgAttachSeq : '',				// 대표상품이미지첨부파일번호
      fileOrgName : '파일선택',					// 첨부파일 원본파일이름
      readFile : {},

      pgmDetailInfo : null,				// 조회된 프로그램 정보

      pgmStartDtm  : '',					//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
      pgmEndDtm  : '',						//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
      pgmReutField : '',					//[필수][공코:PGM_REUT_FIELD] 지원분야
      pgmName : '',							//[필수] 프로그램명
      mainInfo : '',						//[필수] 주요사항
      etcInfo : '',							//[선택] 기타사항

      pgmModifyInfo : {}                   // 수정이력
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      selChargeMemberList : selChargeMemberList,	// 담당자리스트 조회
      chargeMemPlus : chargeMemPlus,				// 담당자 추가
      chargeMemMin : chargeMemMin,					// 담당자 삭제
      repYnClick : repYnClick,						// 대표담당자체크
      fileAttach :fileAttach,						// 파일 전송
      fileDel: fileDel,								// 파일 삭제
      modifyPgm : modifyPgm,						// 프로그램 등록
      deletePgm : deletePgm,                        // 프로그램 삭제
  });

    // 프로그램 분야에 대한 프로그램 조회
    $scope.$watch('cpProgmMod.readFile', function(newVal, oldVal, scope) {
        if(undefined === newVal){
            vm.fileOrgName = '파일선택';
            return false;
        }
        else if(undefined === oldVal){
            vm.fileOrgName = newVal.name;
        }
        else if(newVal.name == oldVal.name) {
            return false;
        }
        else {
            vm.fileOrgName = newVal.name;
        }
    });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    if($stateParams.param){
        // COMPANY_MEMBER_NAME:"담당자"
        // COMPANY_MEMBER_NAME:"담당자"
        // COMPANY_MEMBER_PHONE_NO:"01011111111"
        // COMPANY_MEMBER_POSITION:"최초등록담당자"
        // COMPANY_MEMBER_SEQ:"300000002"
        // COMPANY_MEMBER_SEQS:"300000002"
        // COMPANY_SEQ:"100000002"
        // CREATE_DTM:"1527429025000"
        // ETC_INFO:"기탕"
        // MAIN_INFO:"주용"
        // PGM_BASE_SEQ:"20"
        // PGM_END_DTM:"1527692400000"
        // PGM_IMG_ATTACH_SEQ:"52"
        // PGM_IMG_WEB_PATH:"/images/save/1527429023790.gif"
        // PGM_NAME:"재연"
        // PGM_REUT_FIELD:"P9"
        // PGM_REUT_FIELD_NM:"재연"
        // PGM_START_DTM:"1525273200000"

        vm.pgmBaseSeq = $stateParams.param.PGM_BASE_SEQ + '';

        // 담당자 리스트 조회 및 프로그램 상세 조회
        selChargeMemberList();
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

    /**
     * 대표담당자체크
     */
    function repYnClick(mem){
        _.forEach(vm.selChargeMemeList, function(obj){
            obj.repYn = false;
        })
        mem.repYn = true;
    }

    /**
     * 프로그램 수정
     */
    function modifyPgm(){

        var chk = true;

        chk = chk && ngspublic.dateCheck(vm.pgmStartDtm, vm.pgmEndDtm, '프로그램');
        chk = chk && ngspublic.nullCheck(vm.pgmReutField, '모집분야를 입력해주세요.');
        chk = chk && ngspublic.nullCheck(vm.pgmName, '프로그램명을 입력해주세요.');
        chk = chk && ngspublic.nullCheck(vm.mainInfo, '주요사항을 입력해주세요.');


        if(!chk) return false;

        // 담당자 선택여부확인
        if(vm.selChargeMemeList.length <= 0){
            $message.alert('담당자를 선택해주세요.');
            return false;
        }

        // 대표 담당자 선택 여부 확인
        var findCharge = _.find(vm.selChargeMemeList, {repYn: true});
        if(!findCharge){
            $message.alert('대표 담당자를 선택해주세요.');
            return false;
        }

        // 로그인한 본인이 담당자 리스트에있는지 확인
        var loginCompanyMemberSeq = User.getUserInfoList()[0].COMPANY_MEMBER_SEQ;
        var loginCharge = _.find(vm.selChargeMemeList, {v : loginCompanyMemberSeq+''});
        if(!loginCharge && !User.getAdminYn() && !User.getComAdminYn()){
            $message.alert('로그인 하신 담당자가 포함되지 않았습니다.');
            return false;
        }

        var confirmResult = $message.confirm('수정하시겠습니까?');
        if(!confirmResult) return false;

        var param = {};
        param.pgmBaseSeq = vm.pgmBaseSeq;
        param.pgmStartDtm = _.isEmpty(vm.pgmStartDtm) ? '' : moment(vm.pgmStartDtm, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');			//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        param.pgmEndDtm = _.isEmpty(vm.pgmEndDtm) ? '' : moment(vm.pgmEndDtm, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');					//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        param.pgmReutField = vm.pgmReutField;						//[필수][공코:PGM_REUT_FIELD] 지원분야
        param.pgmName = vm.pgmName;								//[필수] 프로그램명
        param.mainInfo = vm.mainInfo;								//[필수] 주요사항
        param.etcInfo = vm.etcInfo;								//[선택] 기타사항
        param.pgmImgAttachSeq = vm.pgmImgAttachSeq;				//[선택] 대표이미지 파일일련번호



        // 담당자정보
        param.companyMemberSeq = findCharge.v;					//[필수] 대표 담당자 일련번호
        var chargeList = [];
        _.forEach(vm.selChargeMemeList, function(obj){
            chargeList.push({chargeCompanyMemberSeq : obj.v, companyMemberSeq : User.getUserInfoList()[0].COMPANY_MEMBER_SEQ});
        })
        param.chargeList = chargeList;							//[필수] 담당자정보

        // 프로그램 수정 호출
        CompanySvc.modifyPgm(param)
            .then(function(data){
                $message.alert('정상적으로 수정되었습니다.');
                $rootScope.goView('comp.progmc.cpProgmList');
            })
            .catch(function(err){
                // selectPgmDetailInfo();
            })

    }

    /**
     * 프로그램 삭제
     */
    function deletePgm(){
        var chk = true;

        chk = ngspublic.nullCheck(vm.pgmBaseSeq , '프로그램 일려번호가 없습니다.');
        if(!chk) return false;

        var confirmResult = $message.confirm('삭제하시겠습니까?');
        if(!confirmResult) return false;

        var param = {};
        param.pgmBaseSeq = vm.pgmBaseSeq;

        // 프로그램 삭제 호출
        CompanySvc.deletePgm(param)
            .then(function(data){
                $message.alert('정상적으로 삭제되었습니다.');
                $rootScope.goView('comp.progmc.cpProgmList');
            })
            .catch(function(err){
                $message.alert('삭제하는데 실패하였습니다.');
            })
    }
    /**
     * 파일삭제
     */
    function fileDel(){

        var chk = ngspublic.nullCheck(vm.pgmImgAttachSeq, '파일정보가 없습니다.');
        if(!chk) return false;

        var confirmResult = $message.confirm('정말 삭제하시겠습니까?');
        if(confirmResult){

            $("#file").val("");
            vm.fileDisabled = false;

            var param = {};
            param.attachSeq = vm.pgmImgAttachSeq;			// [필수]파일등록 후 결과값에 파일 일련번호
            param.fileOrgName = vm.fileOrgName;			// [필수]파일등록 후 결과값에 파일 원본파일이름

            ComSvc.fileDelete(param)
                .then(function(data){
                    vm.pgmImgAttachSeq = '';
                    vm.fileOrgName = '파일선택';
                    $message.alert('정상적으로 삭제되었습니다. 수정 버튼을 반드시 클릭해주세요.');
                })
                .catch(function(err){
                  vm.pgmImgAttachSeq = '';
                  vm.fileOrgName = '파일선택';
                })

        }
    }


    /**
     * 파일전송
     */
    function fileAttach(){
        vm.fileDisabled = true;
        $rootScope.requestCount++;
        $("#ajaxform").ajaxForm({
            url : $env.endPoint.service + "/api/com/fileUpload.login",
            enctype : "multipart/form-data",
            dataType : "json",
            beforeSubmit: function (data, frm, opt) {
                var objFind = _.find(data, {type : 'file'});
                if(objFind){
                    if(!_.isNull(objFind.value) && objFind.value != ""){
                        return true;
                    }
                }
                vm.fileDisabled = false;
                $rootScope.requestCount--;
                $message.alert('파일 선택 후 진행 해주세요.');
                return false;
            },
            error : function(){
              $scope.$apply(function(){
                vm.fileDisabled = false;
                $rootScope.requestCount--;
              });
                $message.alert("파일 전송 실패") ;
            },
            success : function(result){
                $scope.$apply(function(){
                  $rootScope.requestCount--;
                });
                vm.pgmImgAttachSeq = result.user.resultData.attachSeq;
                vm.fileOrgName = result.user.resultData.orgName;
                $message.alert("파일 전송 성공");
            }
        });

        $("#ajaxform").submit();
    }

    /**
     * 담당자 삭제
     */
    function chargeMemMin(mem){

        vm.selChargeMemeList = _.reject(vm.selChargeMemeList,{v : mem.v});
    }

    /**
     * 담당자 추가
     */
    function chargeMemPlus(){
        if(vm.chargeMemberListChkVal == null){
            $message.alert('추가 대상 담당자가 없습니다.');
            return false;
        }

        var findObj = _.find(vm.selChargeMemeList, {v : vm.chargeMemberListChkVal});
        if(findObj){
            $message.alert('이미 추가된 담당자입니다.');
            return false;
        }

        vm.selChargeMemeList.push(_.find(vm.chargeMemberList, {v : vm.chargeMemberListChkVal}));
    }


    /**
     * 담당자 리스트 조회
     */
    function selChargeMemberList(){

        var param = {};

        CompanySvc.selectCompanyMemberListInfo(param)
            .then(function(data){
                if(data.user.resultData.companyMemberList.length > 0){
                    _.forEach(data.user.resultData.companyMemberList, function(obj){
                        vm.chargeMemberList.push({k:obj.COMPANY_MEMBER_POSITION + ' ' + obj.COMPANY_MEMBER_NAME + '('+$filterSet.fmtPhone(obj.COMPANY_MEMBER_PHONE_NO)+')',v:obj.COMPANY_MEMBER_SEQ+''});
                    })
                    vm.chargeMemberListChkVal = vm.chargeMemberList[0].v;

                    // 프로그램 상세 조회
                    selectPgmDetailInfo();

                }else{
                    vm.chargeMemberList = [];
                    $message.alert('담당자 및 모집 상세 조회에 실패했습니다.');
                    $rootScope.goView('comp.progmc.cpProgmList');
                }
            })
            .catch(function(err){
                $message.alert('담당자 및 모집 상세 조회에 실패했습니다.');
                $rootScope.goView('comp.progmc.cpProgmList');
            })
    }

    /**
     * 프로그램 상세 조회
     */
    function selectPgmDetailInfo(){

        var chk = true;
        chk = ngspublic.nullCheck(vm.pgmBaseSeq , '프로그램 일련번호가 없습니다.');
        if(!chk) return false;


        var param = {};
        param.pgmBaseSeq = vm.pgmBaseSeq;				//[필수] 프로그램 일련번호 

        CompanySvc.selectPgmDetailInfo(param)
            .then(function(data){
                vm.pgmDetailInfo = data.user.resultData;

                // 기본정보 세팅
                vm.pgmStartDtm  	= moment(vm.pgmDetailInfo.pgmDetailInfo.PGM_START_DTM).format('YYYY-MM-DD');		//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
                vm.pgmEndDtm  		= moment(vm.pgmDetailInfo.pgmDetailInfo.PGM_END_DTM).format('YYYY-MM-DD');			//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
                vm.pgmReutField 	= vm.pgmDetailInfo.pgmDetailInfo.PGM_REUT_FIELD;	//[필수][공코:PGM_REUT_FIELD] 지원분야
                vm.pgmName 			= vm.pgmDetailInfo.pgmDetailInfo.PGM_NAME;			//[필수] 프로그램명
                vm.mainInfo 		= vm.pgmDetailInfo.pgmDetailInfo.MAIN_INFO;			//[필수] 주요사항
                vm.etcInfo 			= vm.pgmDetailInfo.pgmDetailInfo.ETC_INFO;			//[선택] 기타사항

                // 담당자 정보 세팅
                vm.selChargeMemeList = [];
                _.forEach(vm.pgmDetailInfo.chargerInfo, function(obj){
                    var chkCharge = false;
                    if(vm.pgmDetailInfo.pgmDetailInfo.COMPANY_MEMBER_SEQ == obj.COMPANY_MEMBER_SEQ) chkCharge = true;
                    var hp = '';
                    if(!_.isEmpty($filterSet.fmtPhone(obj.COMPANY_MEMBER_PHONE_NO))) hp =  $filterSet.fmtPhone(obj.COMPANY_MEMBER_PHONE_NO);
                    vm.selChargeMemeList.push({k:obj.COMPANY_MEMBER_POSITION + ' ' + obj.COMPANY_MEMBER_NAME + '('+hp+')',v:obj.COMPANY_MEMBER_SEQ+'', repYn :chkCharge});
                });

                // 대표이미지정보
                if(!_.isNull(vm.pgmDetailInfo.imgAttachInfo) && !_.isEmpty(vm.pgmDetailInfo.imgAttachInfo)){
                    if(vm.pgmDetailInfo.imgAttachInfo.FILE_ORG_NAME != undefined){
                        vm.pgmImgAttachSeq = vm.pgmDetailInfo.imgAttachInfo.ATTACH_SEQ;
                        vm.fileOrgName = vm.pgmDetailInfo.imgAttachInfo.FILE_ORG_NAME;
                        vm.fileDisabled = true;
                    }else{
                        vm.pgmImgAttachSeq = '';
                        vm.fileOrgName = '파일선택';
                        vm.fileDisabled = false;
                    }
                }

                // 수정이력
                if(!_.isNull(vm.pgmDetailInfo.pgmModifyInfo) && !_.isEmpty(vm.pgmDetailInfo.pgmModifyInfo)){
                    vm.pgmModifyInfo = vm.pgmDetailInfo.pgmModifyInfo;
                }


            })
            .catch(function(err){
            })

    }
}
