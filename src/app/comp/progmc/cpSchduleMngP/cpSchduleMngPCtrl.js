
angular
  .module('comp.progmc')
  .controller('cpSchduleMngPCtrl', cpSchduleMngPCtrl);

function cpSchduleMngPCtrl($log, $rootScope, $scope, $modal, $modalInstance, $message, item, ngspublic, CompanySvc) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        rcutRequestMemeberInfo : item,
        lastSchedulePushInfo : []               // 스케쥴 변경 알림 조회정보
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        close : close,
        search : search
    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);
        
        search();
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    function search(){
        var param = {};
        param.rcutBaseSeq = vm.rcutRequestMemeberInfo.RCUT_BASE_SEQ+'';
        CompanySvc.selectRcutrRequestLastSchedulePushInfo(param)
            .then(function(data){
                if(data.user.resultData.lastSchedulePushInfo.length > 0){
                    vm.lastSchedulePushInfo = data.user.resultData.lastSchedulePushInfo;
                }else{
                    $message.alert('변경이력이 없거나 출연자가 없습니다.');
                    dismiss();
                }
            })
            .catch(function(error){
                logger.log('fail');
                logger.error('message: ' + error + '\n');
            })
    }

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 정상 종료
     */
    function close(){
        $modalInstance.close();
    }
}
