
angular
  .module('comp.progmc')
  .controller('cpRcrutClsPCtrl', cpRcrutClsPCtrl);

function cpRcrutClsPCtrl($log, $rootScope, $scope, $modal, $modalInstance, $message, item, ComSvc, GuestSvc, ngspublic) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        pgmRealEndHour: '999',
        pgmRealEndMinite: '999',
        pgmRealEndHourList : [{v: '999', k: '시 선택'}],				// 시간 리스트
        pgmRealEndMiniteList : [{v: '999', k: '분 선택'}],			// 분 리스트
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        confirm : confirm,
    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        // 시 Data 생성
        for(var i = 0; i <= 23; i++){
            vm.pgmRealEndHourList.push({v: i+'', k: i+'시'});
        }

        // 분 Data 생성
        for(var i = 0; i <= 59; i++){
            vm.pgmRealEndMiniteList.push({v: i+'', k: i+'분'});
        }
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 정상 종료
     */
    function confirm(){

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.pgmRealEndHour ,   '시간을 선택 해 주세요.', '999');
        chk = chk && ngspublic.nullCheck(vm.pgmRealEndMinite ,   '분을 선택 해 주세요.', '999');
        if(!chk) return false;

        // if(!$message.confirm('출연 신청을 하시겠습니까?')){
        //     return false;
        // }

        $modalInstance.close({pgmRealEndHour : vm.pgmRealEndHour, pgmRealEndMinite : vm.pgmRealEndMinite});
    }


}
