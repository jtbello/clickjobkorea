
angular
  .module('comp.progmc')
  .controller('cpRcrutListCtrl', cpRcrutListCtrl);

function cpRcrutListCtrl($log, $scope, $modal, $rootScope, $env, CompanySvc, $stateParams) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      startDate : '',					//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
      endDate : '',						//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]

      stateKind : '999',				//[선택] '' : 전체 ,  'S1' : 예정(모집중)프로그램, 'S2' : 예정(모집완료)프로그램, 'S3' : 진행중프로그램, 'S4' : 종료프로그램
      searchKind : '999',				//[선택] 검색어종류 (999: 전체   S1, 프로그램명, S2 : 담당자명)
      searchData : '',					//[선택] 검색어(프로그램명)

      // 페이징 관련
      curPage : 1,						// 현재페이지
      viewPageCnt : 10,					// 페이지당보여질수
      pagingData : [],					// 페이지번호 모듬
      totPageCnt : 0,					// 전체페이지수

      rcutList :[],						// 조회결과
      rcutListCount : 0				// 총건수
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      search : search,									// 검색
      rcutInsert : rcutInsert,								// 등록
      rcutDel : rcutDel,									// 삭제
      goDetail : goDetail,									// 상세
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);
    
    if ($stateParams.backupData) {
      _.assign(vm, $stateParams.backupData);
      search(false);
    } else {
      search(false);
    }

      console.log($env);
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

    /**
     * 상세페이지 이동
     */
    function goDetail(data){
        if(_.isNull(data)){
            $message.alert('상세데이터가 없습니다.');
            return false;
        }

        logger.log('goDetail', data);
        $rootScope.goView('comp.progmc.cpRcrutInfo', vm, data);
    }

    /**
     * 모집 삭제
     */
    function rcutDel(){

        var delFindObj = _.where(vm.rcutList, {chk: true});
        if(delFindObj.length <= 0){
            $message.alert('삭제 대상을 선택해주세요.');
            return false;
        }

        var title = '';
        _.forEach(delFindObj, function(obj){
            title += obj.PGM_NAME+ (obj.TITLE_GUBUN != '' ? '(' + obj.TITLE_GUBUN + ')' : '') + '\n';
        })


        var confirmResult = $message.confirm(title + '해당 프로그램 모집을 삭제 하시겠습니까?');
        if(confirmResult){
            // 삭제 처리
            _.forEach(delFindObj, function(obj,idx){
                var param = {};
                param.rcutBaseSeq = obj.RCUT_BASE_SEQ;								//[필수] 프로그램 일련번호

                CompanySvc.deleteRcut(param)
                    .then(function(data){
                        if(idx+1 == delFindObj.length){
                            $message.alert('정상적으로 삭제 처리 되었습니다.');
                            search(false);
                        }
                    })
                    .catch(function(error){
                        logger.log('fail');
                        logger.error('message: ' + error.resultMessage + '\n');
                    })
            })
        }

    }

    /**
     * 모집 등록 페이지 이동
     */
    function rcutInsert(){
        var confirmResult = $message.confirm('모집 등록으로 이동하시겠습니까?');
        if(confirmResult){
            logger.log('rcutInsert');
            $rootScope.goView('comp.progmc.cpRcrutReg');
        }
    }

    /**
     * 검색 이벤트
     */
    function search(more){
        vm.curPage = !more ? 1 : vm.curPage+1;

        var param = {};
        param.startNo = (vm.curPage -1) * vm.viewPageCnt;														//[필수] (현재페이지 -1) * 페이지당보여질수
        param.endNo = vm.viewPageCnt;																			//[필수] 페이징 페이지당보여질수

        param.startDate = _.isEmpty(vm.startDate) ? '' : moment(vm.startDate).format('YYYY-MM-DD HH:mm:ss');	//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
        param.endDate = _.isEmpty(vm.endDate) ? '' : moment(vm.endDate).format('YYYY-MM-DD HH:mm:ss');		//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]

        param.stateKind = vm.stateKind == '999' ? '' :	vm.stateKind;										//[선택] '' : 전체 ,  'S1' : 예정(모집중)프로그램, 'S2' : 예정(모집완료)프로그램, 'S3' : 진행중프로그램, 'S4' : 종료프로그램

        param.searchKind = vm.searchKind == '999' ? '' :	vm.searchKind;										//[선택] 검색어종류 (999: 전체   S1, 프로그램명, S2 : 담당자명)
        param.searchData = vm.searchData;																		//[선택] 검색어(프로그램명)

        CompanySvc.selectRcutList(param)
            .then(function(data){
                if(data.user.resultData.rcutList.length > 0){

                    if(!more){
                        vm.rcutList = data.user.resultData.rcutList;
                    }else{
                        vm.rcutList = vm.rcutList.concat(data.user.resultData.rcutList);
                    }
                    vm.rcutListCount = data.user.resultData.rcutListCount;

                    // 전체 페이지 정보입력
                    vm.totPageCnt = Math.floor((vm.rcutListCount / vm.viewPageCnt) + ((vm.rcutListCount % vm.viewPageCnt) == 0 ? 0 : 1));
                    vm.pagingData = [];
                    for (var i = 1; i <= vm.totPageCnt; i++) {
                        vm.pagingData.push({pageNo : i});
                    }

                }else{
                    if(!more){
                        vm.rcutList = [];
                        vm.rcutListCount = 0;
                        vm.pagingData = [];
                    }
                }
            })
            .catch(function(error){
                logger.log('fail');
                logger.error('message: ', error);
                vm.curPage = more ? vm.curPage-1 : vm.curPage;
            })

    }
  
}
