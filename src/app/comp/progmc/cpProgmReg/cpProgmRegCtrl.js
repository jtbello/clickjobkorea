
angular
  .module('comp.progmc')
  .controller('cpProgmRegCtrl', cpProgmRegCtrl);

function cpProgmRegCtrl($log, $scope, $rootScope, $modal, User, $filterSet, $message, CompanySvc, ComSvc, ngspublic, $env) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      chargeMemberList : [],			// 담당자 리스트
      selChargeMemeList : [],			// 선택한 담당자 리스트
      chargeMemberListChkVal : null,	// 추가 대상 담당자
      fileDisabled : false,				// 파일선택가능
      pgmImgAttachSeq : '',				// 대표상품이미지첨부파일번호
      fileOrgName : '파일선택',					// 첨부파일 원본파일이름
      readFile : {},

      pgmStartDtm  : '',					//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
      pgmEndDtm  : '',						//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
      pgmReutField : '',					//[필수][공코:PGM_REUT_FIELD] 지원분야
      pgmName : '',							//[필수] 프로그램명
      mainInfo : '',						//[필수] 주요사항
      etcInfo : ''							//[선택] 기타사항
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      selChargeMemberList : selChargeMemberList,	// 담당자리스트 조회
      chargeMemPlus : chargeMemPlus,				// 담당자 추가
      chargeMemMin : chargeMemMin,					// 담당자 삭제
      repYnClick : repYnClick,						// 대표담당자체크
      fileAttach :fileAttach,						// 파일 전송
      fileDel: fileDel,								// 파일 삭제
      insertPgm : insertPgm 				    	// 프로그램 등록
  });

    // 프로그램 분야에 대한 프로그램 조회
    $scope.$watch('cpProgmReg.readFile', function(newVal, oldVal, scope) {
        if(undefined === newVal){
            vm.fileOrgName = '파일선택';
            return false;
        }
        else if(undefined === oldVal){
            vm.fileOrgName = newVal.name;
        }
        else if(newVal.name == oldVal.name) {
            return false;
        }
        else {
            vm.fileOrgName = newVal.name;
        }
    });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

      // 담당자 리스트 조회
      selChargeMemberList();
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

    /**
     * 대표담당자체크
     */
    function repYnClick(mem){
        _.forEach(vm.selChargeMemeList, function(obj){
            obj.repYn = false;
        })
        mem.repYn = true;
    }

    /**
     * 프로그램 등록 취소
     */
    function cancelFn(){
        $rootScope.goView('comp.progmc.cpProgmList');
    }


    /**
     * 프로그램 등록
     */
    function insertPgm(){

        var chk = true;

        chk = chk && ngspublic.nullCheck(vm.pgmReutField, '모집분야를 입력해주세요.','999');
        chk = chk && ngspublic.nullCheck(vm.pgmName, '프로그램명을 입력해주세요.');
        chk = chk && ngspublic.dateCheck(vm.pgmStartDtm, vm.pgmEndDtm, '프로그램');
        chk = chk && ngspublic.nullCheck(vm.mainInfo, '주요사항을 입력해주세요.');

        if(!chk) return false;

        // 첨부파일 첨부 확인
        if($('#file').val() != '' && vm.pgmImgAttachSeq == ''){
            $message.alert('선택하신 파일을 첨부해주세요.');
            return false;
        }

        // 담당자 선택여부확인
        if(vm.selChargeMemeList.length <= 0){
            $message.alert('담당자를 선택해주세요.');
            return false;
        }

        // 대표 담당자 선택 여부 확인
        var findCharge = _.find(vm.selChargeMemeList, {repYn: true});
        if(!findCharge){
            $message.alert('대표 담당자를 선택해주세요.');
            return false;
        }

        // 로그인한 본인이 담당자 리스트에있는지 확인
        var loginCompanyMemberSeq = User.getUserInfoList()[0].COMPANY_MEMBER_SEQ;
        var loginCharge = _.find(vm.selChargeMemeList, {v : loginCompanyMemberSeq+''});
        if(!loginCharge && !User.getAdminYn() && !User.getComAdminYn()){
            $message.alert('로그인 하신 담당자가 포함되지 않았습니다.');
            return false;
        }

        var param = {};
        param.pgmStartDtm = _.isEmpty(vm.pgmStartDtm) ? '' : moment(vm.pgmStartDtm, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');			//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        param.pgmEndDtm = _.isEmpty(vm.pgmEndDtm) ? '' : moment(vm.pgmEndDtm, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');					//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        param.pgmReutField = vm.pgmReutField;						//[필수][공코:PGM_REUT_FIELD] 지원분야
        param.pgmName = vm.pgmName;								//[필수] 프로그램명
        param.mainInfo = vm.mainInfo;								//[필수] 주요사항
        param.etcInfo = vm.etcInfo;								//[선택] 기타사항
        param.pgmImgAttachSeq = vm.pgmImgAttachSeq;				//[선택] 대표이미지 파일일련번호



        // 담당자정보
        param.companyMemberSeq = findCharge.v;					//[필수] 대표 담당자 일련번호
        var chargeList = [];
        _.forEach(vm.selChargeMemeList, function(obj){
            chargeList.push({chargeCompanyMemberSeq : obj.v, companyMemberSeq : User.getUserInfoList()[0].COMPANY_MEMBER_SEQ});
        })
        param.chargeList = chargeList;							//[필수] 담당자정보

        // 프로그램 등록 호출
        CompanySvc.insertPgm(param)
            .then(function(data){
                $message.alert('정상적으로 등록되었습니다.');
                $rootScope.goView('comp.progmc.cpProgmList');
            })
            .catch(function(err){
                logger.error('message: ' + err + '\n');
            })

    }

    /**
     * 파일삭제
     */
    function fileDel(){

        var chk = ngspublic.nullCheck(vm.pgmImgAttachSeq, '파일정보가 없습니다.');
        if(!chk){
            $('#file').val('');
            return false;
        }

        var confirmResult = $message.confirm('정말 삭제하시겠습니까?');
        if(!confirmResult) return false;

        $("#file").val("");
        vm.fileDisabled = false;

        var param = {};
        param.attachSeq = vm.pgmImgAttachSeq;			// [필수]파일등록 후 결과값에 파일 일련번호
        param.fileOrgName = vm.fileOrgName;			// [필수]파일등록 후 결과값에 파일 원본파일이름

        ComSvc.fileDelete(param)
            .then(function(data){
                vm.pgmImgAttachSeq = '';
                vm.fileOrgName = '파일선택';
                $message.alert('정상적으로 삭제되었습니다.');
            })
            .catch(function(err){
              vm.pgmImgAttachSeq = '';
              vm.fileOrgName = '파일선택';
                logger.error('message: ' + err + '\n');
            })
    }


    /**
     * 파일전송
     */
    function fileAttach(){
        vm.fileDisabled = true;
        $rootScope.requestCount++;
        $("#ajaxform").ajaxForm({
            url : $env.endPoint.service + "/api/com/fileUpload.login",
            enctype : "multipart/form-data",
            dataType : "json",
            beforeSubmit: function (data, frm, opt) {
                var objFind = _.find(data, {type : 'file'});
                if(objFind){
                    if(!_.isNull(objFind.value) && objFind.value != ""){
                        return true;
                    }
                }
                vm.fileDisabled = false;
                $rootScope.requestCount--;
                $message.alert('파일 선택 후 진행 해주세요.');
                return false;
            },
            error : function(){
                $scope.$apply(function(){
                  vm.fileDisabled = false;
                  $rootScope.requestCount--;
                });
                $message.alert("파일 전송 실패") ;
            },
            success : function(result){
                $scope.$apply(function(){
                  $rootScope.requestCount--;
                });
                vm.pgmImgAttachSeq = result.user.resultData.attachSeq;
                vm.fileOrgName = result.user.resultData.orgName;
                $message.alert("파일 전송 성공");
            }
        });

        $("#ajaxform").submit();
    }

    /**
     * 담당자 삭제
     */
    function chargeMemMin(mem){

        vm.selChargeMemeList = _.reject(vm.selChargeMemeList,{v : mem.v});
    }

    /**
     * 담당자 추가
     */
    function chargeMemPlus(){
        if(vm.chargeMemberListChkVal == null){
            $message.alert('추가 대상 담당자가 없습니다.');
            return false;
        }

        var findObj = _.find(vm.selChargeMemeList, {v : vm.chargeMemberListChkVal});
        if(findObj){
            $message.alert('이미 추가된 담당자입니다.');
            return false;
        }

        vm.selChargeMemeList.push(_.find(vm.chargeMemberList, {v : vm.chargeMemberListChkVal}));
    }


    /**
     * 담당자 리스트 조회
     */
    function selChargeMemberList(){

        var param = {};

        CompanySvc.selectCompanyMemberListInfo(param)
            .then(function(data){
                if(data.user.resultData.companyMemberList.length > 0){
                    _.forEach(data.user.resultData.companyMemberList, function(obj){
                        vm.chargeMemberList.push({k:obj.COMPANY_MEMBER_POSITION + ' ' + obj.COMPANY_MEMBER_NAME + '('+$filterSet.fmtPhone(obj.COMPANY_MEMBER_PHONE_NO)+')',v:obj.COMPANY_MEMBER_SEQ+''});
                    })
                    vm.chargeMemberListChkVal = vm.chargeMemberList[0].v;
                }else{
                    vm.chargeMemberList = [];
                    $message.alert('담당자 리스트 조회에 실패했습니다.');
                }
            })
            .catch(function(err){
                $message.alert('담당자 리스트 조회에 실패했습니다.');
            })
    }
}
