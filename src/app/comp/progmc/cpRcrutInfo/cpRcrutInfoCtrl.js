
angular
  .module('comp.progmc')
  .controller('cpRcrutInfoCtrl', cpRcrutInfoCtrl);

function cpRcrutInfoCtrl($log, $scope, $modal, $rootScope, $message, $stateParams, CompanySvc, ngspublic) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      rcutBaseSeq : '',                         //[필수] 모집일련번호
      stateKind : '',                           // 진행상태
      chargeInfo : [],                          // 담당자 정보
      detailBaseInfo : {},                      // 모집 상세정보
      modifyInfo : [],                          // 수정 내역
      subPeopleInfo : [],                        // 모집정보
      rcutDetailInfo : {},
      pgmRealEndHour : '',
        pgmRealEndMinite : ''
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
    search : search,
      modifyRcut : modifyRcut,
      rcutDel : rcutDel,
      rcutEndProc : rcutEndProc,
      showRcrutClsPopup : showRcrutClsPopup,
      viewRequestMemeberPopup : viewRequestMemeberPopup,
      viewStandbyListMember : viewStandbyListMember
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

      if($stateParams.backupData){
          _.assign(vm, $stateParams.backupData);
          search();
      }else{
          vm.rcutBaseSeq = $stateParams.param.RCUT_BASE_SEQ;
          vm.stateKind = $stateParams.param.STATE_KIND;
          search();
      }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */
  
  function search(){
      var param = {};
      param.rcutBaseSeq = vm.rcutBaseSeq + '';					//[필수] 모집일련번호
      CompanySvc.selectRcutDetailInfo(param)
          .then(function(data){
              logger.log('selectRcutDetailInfo success', data);
              vm.rcutDetailInfo = data.user.resultData;
              vm.chargeInfo = data.user.resultData.chargeInfo;
              vm.detailBaseInfo = data.user.resultData.detailBaseInfo;
              vm.stateKind = data.user.resultData.detailBaseInfo.STATE_KIND;
              vm.modifyInfo = data.user.resultData.modifyInfo;
              vm.subPeopleInfo = data.user.resultData.subPeopleInfo;

          })
          .catch(function(error){
              logger.log('fail');
              logger.error('message: ' + error + '\n');
          });
  }

    /**
     * 출연자 (신청자)보기
     */
  function viewRequestMemeberPopup(){

      var param = {};
      param.detailBaseInfo = vm.detailBaseInfo;
      param.rcutBaseSeq = vm.detailBaseInfo.RCUT_BASE_SEQ;
      param.rcutBaseState = vm.detailBaseInfo.RCUT_BASE_STATE;


      if(vm.stateKind === 'S1' || vm.stateKind === 'S2'){
          $rootScope.goView('comp.progmc.cpViewPesnR1List', vm, param);
      } else if(vm.stateKind === 'S3') {
          $rootScope.goView('comp.progmc.cpViewPesnR2List', vm, param);
      } else if(vm.stateKind === 'S4') {
          $rootScope.goView('comp.progmc.cpViewPesnR3List', vm, param);
      }
  }

  /**
     * 촬영순번대기 인원 보기
     */
    function viewStandbyListMember(){

        var param = {};
        param.detailBaseInfo = vm.detailBaseInfo;
  
        $rootScope.goView('comp.progmc.cpStandbyPesnList', vm, param);
    }

    /**
     * 촬영종료 시간 선택 팝업
     */
    function showRcrutClsPopup(){
        logger.log('showRcrutClsPopup Open');
  
        //모달 오픈
        var modalInstance = $modal.open({
            templateUrl: 'progmc/cpRcrutClsP/cpRcrutClsP.tpl.html',
            controller: 'cpRcrutClsPCtrl',
            controllerAs: 'cpRcrutClsP',
            scope: $scope,
            resolve: {
                item: function() {
                    return {};
                }
            }
        });
  
        modalInstance.opened
            .then(function() {
                // 모달 오픈 성공
            })
            .catch(function(data) {
                alert(data);
                // 모달 오픈 실패
            });
  
        modalInstance.result
            .then(function(result) {
                // 정상 종료
                logger.log('showRcrutClsPopup Close', result);
                vm.pgmRealEndHour = result.pgmRealEndHour;
                vm.pgmRealEndMinite = result.pgmRealEndMinite;
                
                rcutEndProc('R3');
            })
            .catch(function(reason) {
                // 취소
                logger.log('showRcrutClsPopup Dismiss');
            });
  
      }
    /**
     * 촬영종료
     */
  function rcutEndProc(rcutBaseState){

        var msg = '';
        if(rcutBaseState == 'R2'){
            msg = '모집 종료처리하시겠습니까?';
        }else{
            msg = '출연자의 출석,종료 처리가 되지 않을 경우 정산 처리 되지 않습니다.\n출연자의 출석,종료 처리를 꼭 확인 후 촬영 종료를 해주세요.\n촬영 종료처리하시겠습니까?';
        }

        var confirmResult = $message.confirm(msg);
        if(!confirmResult) return false;

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.rcutBaseSeq , 			'출연자모집정보 일련번호가 없습니다.');
        chk = chk && ngspublic.nullCheck(rcutBaseState , 					'처리 종류코드가 없습니다.');
        if(!chk) return false;

        if(rcutBaseState != 'R2' && rcutBaseState != 'R3'){
            $message.alert('처리 종류코드를 정확하게 입력해주세요.');
            return false;
        }

        // 모집종료 일때 상태값 확인
        if(rcutBaseState == 'R2'){
            if(vm.stateKind != 'S1'){
                $message.alert('모집중인 상태에서만 처리할 수 있습니다.');
                return false;
            }
        }

        // 이미 종료된 건인지 확인
        // if(rcutBaseState == 'R3'){
        //     if(vm.detailBaseInfo.RCUT_BASE_STATE == 'R3' || vm.detailBaseInfo.PGM_REAL_END_DTM != '0000.00.00 00:00:00'){
        //         $message.alert('이미 촬영 종료된 상태입니다.');
        //         return false;
        //     }
        // }

        var param = {};
        param.rcutBaseSeq = vm.rcutBaseSeq;									//[필수] 모집일련번호
        param.rcutBaseState = rcutBaseState;										//[필수][공코:RCUT_BASE_STATE] R2 : 모집종료 R3 : 촬영종료

        if(rcutBaseState == 'R3'){
            if(vm.pgmRealEndHour.length < 2){
                vm.pgmRealEndHour = '0' + vm.pgmRealEndHour;
            }
            
            if(vm.pgmRealEndMinite.length < 2){
                vm.pgmRealEndMinite = '0' + vm.pgmRealEndMinite;
            }
            param.pgmRealEndDtm = moment().format('YYYY-MM-DD') + ' ' +vm.pgmRealEndHour + ':' + vm.pgmRealEndMinite + ':' + '00';
        }

        CompanySvc.rcutEndProc(param)
            .then(function(data){
                if(rcutBaseState == 'R2'){
                    $message.alert('모집 종료 처리 되었습니다.');
                    $rootScope.prevBtn();
                }
                if(rcutBaseState == 'R3'){
                    $message.alert('촬영 종료 처리 되었습니다.');
                    $rootScope.prevBtn();
                }
            })
            .catch(function(error){
                logger.log('fail');
                logger.error('message: ' + error + '\n');
            })
  }

    /**
     * 모집 수정
     */
    function modifyRcut(){

        $rootScope.goView('comp.progmc.cpRcrutMod', vm, vm.detailBaseInfo);
    }

    /**
     * 모집 삭제
     */
    function rcutDel(){

        var confirmResult = $message.confirm(vm.detailBaseInfo.PGM_NAME + (vm.detailBaseInfo.TITLE_GUBUN != '' ? '(' + vm.detailBaseInfo.TITLE_GUBUN +')' : '') +'\n' + '해당 프로그램 모집을 삭제 하시겠습니까?');
        if(confirmResult){
            // 삭제 처리
            var param = {};
            param.rcutBaseSeq = vm.rcutBaseSeq;								//[필수] 프로그램 일련번호

            CompanySvc.deleteRcut(param)
                .then(function(data){
                    $message.alert('정상적으로 삭제 처리 되었습니다.');
                    $rootScope.prevBtn();
                })
                .catch(function(error){
                    logger.log('fail');
                    logger.error('message: ' + error + '\n');
                })
        }

    }
}
