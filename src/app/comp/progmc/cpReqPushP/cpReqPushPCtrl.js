
angular
  .module('comp.progmc')
  .controller('cpReqPushPCtrl', cpReqPushPCtrl);

function cpReqPushPCtrl($log, $rootScope, $scope, $modal, $modalInstance, $message, ngspublic) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        msg : ''
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        confirm : confirm
    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 정상 종료
     */
    function confirm(){

        var chk = true;
        chk = chk && ngspublic.nullCheck(vm.msg, '메세지를 입력 해 주세요.');
        if(!chk) return false;

        var param = {};
        param.msg = vm.msg;
        $modalInstance.close(param);
    }
}
