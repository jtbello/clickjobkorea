
angular
  .module('comp.progmc', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('comp.progmc', {
      url: '/progmc',
      abstract: false,
      template: '<div ui-view><h1>comp.progmc</h1></div>',
      controller: 'progmcCtrl as progmc'
    })

    .state('comp.progmc.cpProgmList', {
      url: '/cpProgmList',
      templateUrl: 'progmc/cpProgmList/cpProgmList.tpl.html',
      data: { menuTitle : '프로그램 관리', menuId : 'cpProgmList', depth : 1},
        params: { param: null, backupData: null },
      controller: 'cpProgmListCtrl as cpProgmList'
    })
      .state('comp.progmc.cpProgmReg', {
          url: '/cpProgmReg',
          templateUrl: 'progmc/cpProgmReg/cpProgmReg.tpl.html',
          data: { menuTitle : '프로그램 등록', menuId : 'cpProgmReg', depth : 1},
          params: { param: null, backupData: null },
          controller: 'cpProgmRegCtrl as cpProgmReg'
      })
      .state('comp.progmc.cpProgmMod', {
          url: '/cpProgmMod',
          templateUrl: 'progmc/cpProgmMod/cpProgmMod.tpl.html',
          data: { menuTitle : '프로그램 수정', menuId : 'cpProgmMod', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cpProgmModCtrl as cpProgmMod'
      })
      .state('comp.progmc.cpRcrutReg', {
          url: '/cpRcrutReg',
          templateUrl: 'progmc/cpRcrutReg/cpRcrutReg.tpl.html',
          data: { menuTitle : '출연자 모집정보 등록', menuId : 'cpRcrutReg', depth : 1},
          params: { param: null, backupData: null },
          controller: 'cpRcrutRegCtrl as cpRcrutReg'
      })
      .state('comp.progmc.cpRcrutMod', {
          url: '/cpRcrutMod',
          templateUrl: 'progmc/cpRcrutMod/cpRcrutMod.tpl.html',
          data: { menuTitle : '출연자 모집정보 수정', menuId : 'cpRcrutMod', depth : 3},
          params: { param: null, backupData: null },
          controller: 'cpRcrutModCtrl as cpRcrutMod'
      })
      .state('comp.progmc.cpRcrutList', {
          url: '/cpRcrutList',
          templateUrl: 'progmc/cpRcrutList/cpRcrutList.tpl.html',
          data: { menuTitle : '출연자 모집정보 관리', menuId : 'cpRcrutList', depth : 1},
          params: { param: null, backupData: null },
          controller: 'cpRcrutListCtrl as cpRcrutList'
      })
      .state('comp.progmc.cpRcrutInfo', {
          url: '/cpRcrutInfo',
          templateUrl: 'progmc/cpRcrutInfo/cpRcrutInfo.tpl.html',
          data: { menuTitle : '출연자 모집정보', menuId : 'cpRcrutInfo', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cpRcrutInfoCtrl as cpRcrutInfo'
      })
      .state('comp.progmc.cpStandbyPesnList', {
          url: '/cpStandbyPesnList',
          templateUrl: 'progmc/cpStandbyPesnList/cpStandbyPesnList.tpl.html',
          data: { menuTitle : '촬영순번대기인원', menuId : 'cpStandbyPesnList', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cpStandbyPesnListCtrl as cpStandbyPesnList'
      })
      .state('comp.progmc.cpViewPesnR1List', {
          url: '/cpViewPesnR1List',
          templateUrl: 'progmc/cpViewPesnR1List/cpViewPesnR1List.tpl.html',
          data: { menuTitle : '출연(신청)자 보기', menuId : 'cpViewPesnR1List', depth : 3},
          params: { param: null, backupData: null },
          controller: 'cpViewPesnR1ListCtrl as cpViewPesnR1List'
      })
      .state('comp.progmc.cpViewPesnR2List', {
          url: '/cpViewPesnR2List',
          templateUrl: 'progmc/cpViewPesnR2List/cpViewPesnR2List.tpl.html',
          data: { menuTitle : '출연자 보기', menuId : 'cpViewPesnR2List', depth : 3},
          params: { param: null, backupData: null },
          controller: 'cpViewPesnR2ListCtrl as cpViewPesnR2List'
      })
      .state('comp.progmc.cpViewPesnR3List', {
          url: '/cpViewPesnR3List',
          templateUrl: 'progmc/cpViewPesnR3List/cpViewPesnR3List.tpl.html',
          data: { menuTitle : '출연자 보기', menuId : 'cpViewPesnR3List', depth : 3},
          params: { param: null, backupData: null },
          controller: 'cpViewPesnR3ListCtrl as cpViewPesnR3List'
      })
    ;

  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
