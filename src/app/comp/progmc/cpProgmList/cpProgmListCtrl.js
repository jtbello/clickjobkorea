
angular
  .module('comp.progmc')
  .controller('cpProgmListCtrl', cpProgmListCtrl);

function cpProgmListCtrl($log, $scope, $rootScope, $modal, $stateParams, CompanySvc) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
      pgmReutField : '999',                 //[선택][공코:PGM_REUT_FIELD] 분야
      pgmStateKind : '999',                    //[선택] '' : 전체 ,  'P1' : 상태 대기, 'P2' : 상태 진행중, 'P3' : 상태 종료
      startDate : '',                       //[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
      endDate : '',                         //[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
      searchData : '',                       //[선택] 검색어(프로그램명)

      pgmList : [],					// 프로그램 리스트
      pgmListCount : 0,				// 프로그램 전체카운트

      // 페이징 관련
      curPage : 1,						// 현재페이지
      viewPageCnt : 10,					// 페이지당보여질수
      pagingData : [],					// 페이지번호 모듬
      totPageCnt : 0					// 전체페이지수
  });
  
  /**
   * Public methods
   */
  _.assign(vm, {
      search : search,
      goDetail : goDetail,
      programReg : programReg
  });
  
  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    if($stateParams.backupData){
      _.assign(vm, $stateParams.backupData);
    }else{
        search(false);
    }
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  function search(more){
      vm.curPage = !more ? 1 : vm.curPage+1;

      var param = {};

      param.startNo = (vm.curPage -1) * vm.viewPageCnt;								//[필수] (현재페이지 -1) * 페이지당보여질수
      param.endNo = vm.viewPageCnt;													//[필수] 페이징 페이지당보여질수

      param.startDate = _.isEmpty(vm.startDate) ? '' : moment(vm.startDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');				//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
      param.endDate = _.isEmpty(vm.endDate) ? '' : moment(vm.endDate, 'YYYY-MM-DD HH:mm:ss').format('YYYY-MM-DD HH:mm:ss');					//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]

      param.searchData = vm.searchData;						//[선택] 검색어(프로그램명)

      param.pgmReutField = vm.pgmReutField === '999' ? '' :	vm.pgmReutField;	//[선택][공코 : PGM_REUT_FIELD] 지원분야

      param.pgmStateKind = vm.pgmStateKind === '999' ? '' :	vm.pgmStateKind;	//[선택] '' : 전체 ,  'P1' : 상태 대기, 'P2' : 상태 진행중, 'P3' : 상태 종료

      CompanySvc.selectPgmLstInfo(param)
          .then(function(data){
              logger.log('selectPgmLstInfo success', data);
              if(data.user.resultData.pgmList.length > 0){

                  // COMPANY_MEMBER_NAME:"담당자"
                  // COMPANY_MEMBER_NAME:"담당자"
                  // COMPANY_MEMBER_PHONE_NO:"01011111111"
                  // COMPANY_MEMBER_POSITION:"최초등록담당자"
                  // COMPANY_MEMBER_SEQ:"300000002"
                  // COMPANY_MEMBER_SEQS:"300000002"
                  // COMPANY_SEQ:"100000002"
                  // CREATE_DTM:"1527429025000"
                  // ETC_INFO:"기탕"
                  // MAIN_INFO:"주용"
                  // PGM_BASE_SEQ:"20"
                  // PGM_END_DTM:"1527692400000"
                  // PGM_IMG_ATTACH_SEQ:"52"
                  // PGM_IMG_WEB_PATH:"/images/save/1527429023790.gif"
                  // PGM_NAME:"재연"
                  // PGM_REUT_FIELD:"P9"
                  // PGM_REUT_FIELD_NM:"재연"
                  // PGM_START_DTM:"1525273200000"

                  if(!more){
                      vm.pgmList = data.user.resultData.pgmList;
                  }else{
                      vm.pgmList = vm.pgmList.concat(data.user.resultData.pgmList);
                  }

                  vm.pgmListCount = data.user.resultData.pgmListCount;

                  // 전체 페이지 정보입력
                  vm.totPageCnt = Math.floor((vm.pgmListCount / vm.viewPageCnt) + ((vm.pgmListCount % vm.viewPageCnt) === 0 ? 0 : 1));
                  vm.pagingData = [];
                  for (var i = 1; i <= vm.totPageCnt; i++) {
                      vm.pagingData.push({pageNo : i});
                  }

              }else{
                  if(!more){
                      vm.pgmList = [];
                      vm.pgmListCount = 0;
                      vm.pagingData = [];
                  }
              }
          })
          .catch(function(error){
              logger.log('fail');
              logger.error('message: ', error);
              vm.curPage = more ? vm.curPage-1 : vm.curPage;
          });
  }

    function goDetail(data){
        logger.log('goDetail', data);
        $rootScope.goView('comp.progmc.cpProgmMod', vm, data);
    }

    function programReg(){
        logger.log('programReg');
        $rootScope.goView('comp.progmc.cpProgmReg');
    }
}
