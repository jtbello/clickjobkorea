
angular
  .module('comp.progmc')
  .controller('cpAttendEndPCtrl', cpAttendEndPCtrl);

function cpAttendEndPCtrl($log, $rootScope, $scope, $modal, $modalInstance, item, $message, ngspublic, CompanySvc) {
    /**
    * Private variables
    */
    var logger = $log(this);
    var vm = this;

    /**
     * Public variables
     */
    _.assign(vm, {
        // 파라미터
        requestInfoSeq : item.requestInfoSeq,            //[필수] 모집신청일련번호

        // 변수
        pgmMeetingHour : '999',			//[필수] 촬영시간 (시)
        pgmMeetingMinite : '999',			//[필수] 촬영시간 (분)
        companyMemberAttendCode : false,	// 지각체크
        companyMemberAttendDtm : '',		// 출석일
        companyMemberAttendHour : '999',	// 출석 시
        companyMemberAttendMinite : '999',// 출석 분
        companyMemberAttendPoint : '',	// 출석장소

        companyMemberEndDtm : '',			// 종료일
        companyMemberEndHour : '999',		// 종료 시
        companyMemberEndMinite : '999',	// 종료 분
        companyMemberEndPoint : ''		// 종료장소
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        dismiss : dismiss,
        confirm : confirm
    });

    /**
    * Initialize
    */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        // 시 Data 생성
        vm.pgmMeetingHourList = [{v: '999', k: '선택'}];
        for(var i = 0; i <= 23; i++){
            vm.pgmMeetingHourList.push({v: i+'', k: i+'시'});
        }

        // 분 Data 생성
        vm.pgmMeetingMiniteList = [{v: '999', k: '선택'}];
        for(var i = 0; i <= 59; i++){
            vm.pgmMeetingMiniteList.push({v: i+'', k: i+'분'});
        }
    }

    /**
    * Event handlers
    */
    function destroy(event) {
      logger.debug(event);
    }

    /**
    * Custom functions
    */

    /**
     * 모달 닫기
     */
    function dismiss() {
        $modalInstance.dismiss();
    }

    /**
     * 정상 종료
     */
    function confirm(){

        var chk = true;
        if(vm.companyMemberAttendDtm !== ''){
            chk = chk && ngspublic.nullCheck(vm.companyMemberAttendHour , 	'출석시간을 입력해주세요.', '999');
            chk = chk && ngspublic.nullCheck(vm.companyMemberAttendMinite , '출석시간을 입력해주세요.', '999');
            chk = chk && ngspublic.nullCheck(vm.companyMemberAttendPoint , 	'출석장소를 입력해주세요.');
        }
        if(vm.companyMemberEndDtm !== ''){
            chk = chk && ngspublic.nullCheck(vm.companyMemberEndHour , 		'종료시간을 입력해주세요.', '999');
            chk = chk && ngspublic.nullCheck(vm.companyMemberEndMinite, 	'종료시간을 입력해주세요.', '999');
            chk = chk && ngspublic.nullCheck(vm.companyMemberEndPoint , 	'종료장소를 입력해주세요.');
        }
        if(!chk) return false;
        if(vm.companyMemberAttendDtm === '' && vm.companyMemberEndDtm === ''){
            $message.alert('출석,종료중 하나는 입력하셔야합니다.');
            return false;
        }

        var param = {};

        if(vm.companyMemberAttendDtm !== ''){
            var companyMemberAttendDtm = vm.companyMemberAttendDtm + ' ' + vm.companyMemberAttendHour + ':' + vm.companyMemberAttendMinite + ':00';
            param.companyMemberAttendDtm = companyMemberAttendDtm;						//[필수] 출석일시 [일시 패턴 : 0000-00-00 00:00:00]
            param.companyMemberAttendPoint = vm.companyMemberAttendPoint;			//[필수] 출석장소

        }
        if(vm.companyMemberEndDtm !== ''){
            var companyMemberEndDtm = vm.companyMemberEndDtm + ' ' + vm.companyMemberEndHour + ':' + vm.companyMemberEndMinite + ':00';
            param.companyMemberEndDtm = companyMemberEndDtm;								//[필수] 종료일시 [일시 패턴 : 0000-00-00 00:00:00]
            param.companyMemberEndPoint = vm.companyMemberEndPoint;				//[필수] 종료장소

        }

        if(vm.companyMemberAttendCode){
            param.attendNoState = "A3";												//[필수][공코:ATTEND_NO_STATE] 출석,지각 코드 ( 출석 : A1, 지각: A3)
        }else{
            param.attendNoState = "A1";												//[필수][공코:ATTEND_NO_STATE] 출석,지각 코드 ( 출석 : A1, 지각: A3)
        }
        param.requestInfoSeq = vm.requestInfoSeq+'';									//[필수] 모집신청일련번호

        CompanySvc.updateAttendEndInfo(param)
            .then(function(data){
                $message.alert('정상적으로 처리 되었습니다.');
                $modalInstance.close();
            })
            .catch(function(err){
            })
    }
}
