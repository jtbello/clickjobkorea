
angular
  .module('comp.paymt', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('comp.paymt', {
      url: '/paymt',
      abstract: false,
      template: '<div ui-view><h1>comp.paymt</h1></div>',
      controller: 'paymtCtrl as paymt'
    })

    .state('comp.paymt.cpPaymtList', {
      url: '/cpPaymtList',
      templateUrl: 'paymt/cpPaymtList/cpPaymtList.tpl.html',
      data: { menuTitle : '정산 관리', menuId : 'cpPaymtList', depth : 1},
        params: { param: null, backupData: null },
      controller: 'cpPaymtListCtrl as cpPaymtList'
    })
      .state('comp.paymt.cpPaymtInfo', {
          url: '/cpPaymtInfo',
          templateUrl: 'paymt/cpPaymtInfo/cpPaymtInfo.tpl.html',
          data: { menuTitle : '정산 관리', menuId : 'cpPaymtInfo', depth : 2},
          params: { param: null, backupData: null },
          controller: 'cpPaymtInfoCtrl as cpPaymtInfo'
      })
    ;

  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
