angular.module('comp.paymt').controller('cpPaymtInfoCtrl', cpPaymtInfoCtrl);

function cpPaymtInfoCtrl(
  $log,
  $scope,
  $modal,
  $user,
  $stateParams,
  $rootScope,
  $message,
  CompanySvc
) {
  /**
   * Private variables
   */
  var logger = $log(this);
  var vm = this;

  /**
   * Public variables
   */
  _.assign(vm, {
    rcutBaseSeq: '', // 정산목록 번호
    settlementStateOptions: [], // 정산상태 선택 리스트

    settlementStateId: '999', // 정산상태
    searchTerm: '', // 검색어

    detailList: [], // 정산내역 리스트
    detailListCount: 0,

    // 페이징 관련
    curPage: 1, // 현재페이지
    viewPageCnt: 10, // 페이지당보여질수
    pagingData: [], // 페이지번호 모듬
    totPageCnt: 0 // 전체페이지수
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    search: search,
    toggleSelectAll: toggleSelectAll,
    onNameClick: onNameClick,
    onCompleteButtonClick: onCompleteButtonClick,
    onSendNotificationClick: onSendNotificationClick
  });

  /**
   * Initialize
   */
  init();

  function init() {
    logger.debug('init', vm);

    $scope.$on('$destroy', destroy);

    setSelectOptions();
    if ($stateParams.backupData) {
      _.assign(vm, $stateParams.backupData);
    } else {
      vm.rcutBaseSeq = $stateParams.param.rcutBaseSeq;
    }
    search();
  }

  /**
   * Event handlers
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * Custom functions
   */

  function onSendNotificationClick() {
    var checkedDetailList = getCheckedDetailList();

    if (checkedDetailList) {
      //모달 오픈
      var modalInstance = $modal.open({
        templateUrl: 'progmc/cpReqPushP/cpReqPushP.tpl.html',
        controller: 'cpReqPushPCtrl',
        controllerAs: 'cpReqPushP',
        scope: $scope,
        resolve: {
          item: function() {
            return;
          }
        }
      });

      modalInstance.opened
        .then(function() {
          // 모달 오픈 성공
        })
        .catch(function(data) {
          alert(data);
          // 모달 오픈 실패
        });

      modalInstance.result
        .then(function(result) {
          // 정상 종료
          logger.log('showReqPushPopup Close', result.msg);

          var memList = checkedDetailList.map(function(detail) {
            return {
              MEMBER_SEQ: detail.MEMBER_SEQ,
              COMPANY_SEQ: $user.currentUser.getAuthInfoList()[0].COMPANY_SEQ
            };
          });

          $rootScope
            .comPushSend(memList, result.msg)
            .then(function(data) {
              $message.alert('메시지가 정상적으로 발송되었습니다.');
            })
            .catch(function(err) {});
        })
        .catch(function(reason) {
          // 취소
          logger.log('showReqPushPopup Dismiss', reason);
        });
    }
  }

  function onCompleteButtonClick() {
    var checkedDetailList = getCheckedDetailList();

    if (checkedDetailList) {
      // 선택된 항목 중 이미 완료 처리된 항목 제거
      var toBeCompletedDetailList = checkedDetailList.filter(function(detail) {
        return detail.CALCULATE_STATE_CODE !== 'A2';
      });

      if (toBeCompletedDetailList.length === 0) {
        $message.alert('이미 완료처리 되었습니다.');
        return false;

        // 이미 왼료처리된 항목 제거 후 남은 선택 항목만 완료 처리
      } else {
        var chk = true;
        _.forEach(toBeCompletedDetailList, function(obj){
        
          if(_.isUndefined(obj.COMPANY_MEMBER_ATTEND_DTM)){
            $message.alert(obj.MEMBER_NM + '출연자의 담당자 출석 시간이 없습니다.');
            chk = false;
            return false;
          }
          
          if(_.isUndefined(obj.COMPANY_MEMBER_END_DTM)){
            $message.alert(obj.MEMBER_NM + '출연자의 담당자 종료 시간이 없습니다.');
            chk = false;
            return false;
          }
          
        })
        
        if(!chk) return false;  
        
        toBeCompletedDetailList.forEach(function(detail, index) {  

          var param = {};
          param.memberSeq = detail.MEMBER_SEQ; //[필수] 회원 일련번호
          param.requestInfoSeq = detail.REQUST_INFO_SEQ; //[필수] 모집 신청 일련번호

          CompanySvc.procCalculate(param)
            .then(function(data) {
              if (data.user.resultMessage === '무단이탈상태입니다.') {
                $message.alert(
                  detail.MEMBER_NM + ': ' + data.user.resultMessage
                );
              } else if (toBeCompletedDetailList.length === index + 1) {
                $message.alert('정상적으로 처리되었습니다.');
                search();
              }
            })
            .catch(function(error) {});
        });
      }
    }
  }

  function getCheckedDetailList() {
    var checkedDetailList = vm.detailList.filter(function(detail) {
      return detail.chk;
    });

    // 선택된 항목이 없을 경우
    if (checkedDetailList.length === 0) {
      $message.alert('선택된 항목이 없습니다.');
      return false;
    }

    return checkedDetailList;
  }

  function onNameClick(data) {
    var params = {
      memberInfo: data,
      prevViewId: 'cpPaymtInfo'
    };
    $rootScope.goView('comp.mainc.cmPesnInfo', vm, params);
  }

  function toggleSelectAll() {
    if (vm.isSelectedAll) {
      vm.detailList.forEach(function(detail) {
        detail.chk = true;
      });
    } else {
      vm.detailList.forEach(function(detail) {
        detail.chk = false;
      });
    }
  }

  function search(shouldResetList = true) {
    if (shouldResetList) {
      vm.detailList = [];
      vm.detailListCount = 0;
      vm.pagingData = [];
      vm.curPage = 1;
      vm.showShowMoreButton = true;
    }

    var param = getParams();

    CompanySvc.selectCalculateDetailList(param)
      .then(function(data) {
        // 리스트 업데이트
        vm.detailList = vm.detailList.concat(
          data.user.resultData.calculateDetailList
        );
        
        vm.detailListCount = data.user.resultData.calculateDetailListCount;

        // 리스트에 추가할 항목이 없을 경우
        if (
          vm.detailList.length === 0 &&
          vm.curPage > 1
        ) {
          vm.curPage--;

          // 기존 리스트에 항목이 추가됐을 경우
          // or 리스트에 처음 항목이 추가됐을 경우
        }

        // 가져온 리스트 항목수가 페이지당 항목수 (10) 보다 작을 경우 더보기 버튼 숨기기
        if (vm.detailList.length < vm.viewPageCnt) {
          vm.showShowMoreButton = false;
        }

        // 전체 페이지 정보입력
        vm.totPageCnt = Math.floor(
          vm.detailListCount / vm.viewPageCnt +
            (vm.detailListCount % vm.viewPageCnt == 0 ? 0 : 1)
        );
        vm.pagingData = [];
        for (var i = 1; i <= vm.totPageCnt; i++) {
          vm.pagingData.push({ pageNo: i });
        }

        logger.log('search[SUCCESS]', 'vm.detailList: ', vm.detailList);
      })
      .catch(function(error) {
        logger.log('search[FAILED]', 'error: ', error);
      });
  }

  function getParams() {
    var param = {};
    param.startNo = (vm.curPage - 1) * vm.viewPageCnt; //[필수] (현재페이지 -1) * 페이지당보여질수
    param.endNo = vm.viewPageCnt; //[필수] 페이징 페이지당보여질수
    param.rcutBaseSeq = vm.rcutBaseSeq; // 정산목록 번호

    param.searchData = vm.searchTerm; // 검색어
    param.calculateStateCode =
      vm.settlementStateId === '999' ? '' : vm.settlementStateId; // 정산상태

    return param;
  }

  function setSelectOptions() {
    // 정산상태 선택 리스트
    vm.settlementStateOptions = [
      { value: '999', text: '전체' },
      { value: 'A1', text: '미정산' },
      { value: 'A2', text: '정산완료' },
      { value: 'A3', text: '정산불가' }
    ];
  }
}
