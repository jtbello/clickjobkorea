/* jshint browser:true */

document.addEventListener('deviceready', bootstrap, false);

angular
  .element(document)
  .ready(function() {
    // if (!window._cordovaNative) {
    //   bootstrap();
    // }
  });

function bootstrap() {
  angular.bootstrap(document, ['easi.common', '/* @echo MODULE_NAME */']);
}