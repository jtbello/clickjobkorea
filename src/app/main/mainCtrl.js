
angular
  .module('main')
  .controller('mainCtrl', mainCtrl)
  .directive('onLastRepeat', function () {
    return function (scope, element, attrs) {
        if (scope.$last) setTimeout(function () {
            scope.$emit('onRepeatLast', element, attrs);
        }, 1);
    };
  });

function mainCtrl($log, $rootScope, $scope, $modal, $message, $storage, GuestSvc, MemberSvc, $user, 
                  ComSvc, $cordovaInAppBrowser, $window) {

    /**
     * Private variables
     */
    var logger = $log(this);
    var vm = this;
    var _fn = {
        err : function(d){
            logger.debug('error : ', d);
            return this;
        },
        urg : {
            load : function(){
                return GuestSvc.selectEmerRcutList()
                    .then(function(d){
                        if(d.user.resultData.emerRcutData.length <= 0){
                            $message.alert('더 이상 조회할 항목이 없습니다.');
                        }
                    else{
                        d.user.resultData.emerRcutData.forEach(elem => {
                            vm.mod.urg.list.push(elem);
                        });
                        vm.mod.urg.page += 1;
                    }
                } , _fn.err);
            }
        },
        ad : {
            load : function(){
                return GuestSvc.selectAllRcutList({
                    'orderType' : vm.mod.opts.ordType,
                    'startNo' : ((vm.mod.ad.page - 1) + 1) * vm.mod.ad.cnt,
                    'endNo' : vm.mod.ad.cnt
                }).then(function(d){
                    vm.mod.ad.totalCnt = d.user.resultData.allRcutDataCount;
                    if(d.user.resultData.allRcutData.length <= 0){
                        $message.alert('더 이상 조회할 항목이 없습니다.');
                    }
                    else{
                        d.user.resultData.allRcutData.forEach(elem => {
                            vm.mod.ad.list.push(elem);
                        });
                        vm.mod.ad.page += 1;
                    }
                } , _fn.err);
            }
        }
    };

    /**
     * Public variables
     */
    _.assign(vm, {
        IDENTIFIERS : {
            ITM_TYPES : {
                URGENT : 'URGENT',
                ADVERTISE : 'ADVERTISE'
            }
        },
        mod : {
            opts : {
                // 페이지가 로드될때마다 난수를 생성하도록함
                ordType : Math.floor(Math.random() * 4) + 1
            },
            // urgent
            urg : {
                list : [],
                page : 1,
                cnt : 1
            },
            // advertise
            ad : {
                list : [],
                page : 1,
                cnt : 5,
                totalCnt : 0
            }
        }
    });

    /**
     * Public methods
     */
    _.assign(vm, {
        evt : {
            load : function(type){
                if(type === vm.IDENTIFIERS.ITM_TYPES.ADVERTISE){
                    _fn.ad.load();
                }
                else if(type === vm.IDENTIFIERS.ITM_TYPES.URGENT){
                    // 긴급을 추가 조회하는 경우
                    // 긴급은 모두 출력되므로 사용될 일 없음
                }
                else{
                    // 일반적 코드가 아님
                }
            },
            go : function(){
                $message.alert('로그인 후 이용 가능합니다.');
                $rootScope.goView('public.html#/login');
            }
        }
    });

    /**
     * Initialize
     */
    init();

    function init() {
        logger.debug('init', vm);

        $scope.$on('$destroy', destroy);

        // 배너관련
        var param= {};
        param.startNo = 0;                            
        param.endNo = 90; 
        ComSvc.selectBannerList(param)
          .then(function(data){
            vm.bannerList = data.user.resultData.bannerList;
            vm.bannerListCount = data.user.resultData.bannerListCount;
          })
            
        vm.bannerClick = function(elem){
            let jElem = $(elem);
            let bannerLink = jElem.attr('data-banner-link-info');
            let bannerInfoSeq = jElem.attr('info-seq');
            if(_.isBlank(bannerLink)){
            return false;
            }
    
            var param= {};
            param.bannerClickCount  = 'Y';
            param.bannerInfoSeq = bannerInfoSeq;
            param.companyMemberSeq = '99999999';
            ComSvc.updateBannerCntUp(param);
            var options = {
                location: 'yes',
                scrollbars: 'yes',
                resizable: 'yes'
            };
            $cordovaInAppBrowser.open(bannerLink, '_blank', options);
            
        };
        selList();
        getUser();
    }

    /**
     * Event handlers
     */
    function destroy(event) {
        logger.debug(event);
    }

    /**
     * Custom functions
     */

    function getUser(){
        $storage.get('AUTO_LOGIN')
            .then(function(login){
                if(login === 'Y') {
                    $user.get()
                        .then(function (user){
                            var userId = '';
                            if(user.getLoginKind() === 'L1'){       // 개인
                                userId = user.getUserInfoList()[0].MEMBER_ID;
                            }else if(user.getLoginKind() === 'L2'){ // 기업
                                userId = user.getUserInfoList()[0].COMPANY_MEMBER_ID;
                            }

                            // 자동로그인
                            MemberSvc.login({
                                'loginKind' : user.getLoginKind(),
                                'userId' : userId,
                                'userPw' : user.getUserInfoList()[0].MEMBER_PW,
                                'siteKind' : 'APP',
                                'autoLogin' : 'Y'
                            }).then(function(data){
                                // onComplete

                                data.user.resultData.userInfo[0].MEMBER_PW = user.getUserInfoList()[0].MEMBER_PW;
                                $storage.set('userDataInfo', data.user.resultData);
                                $window.localStorage.setItem("LOGINUSERID", vm.mod.id);
                                loginCheck(user);
                            })
                            .catch(function(err){
                                logger.error('message: ' + err + '\n');
                            });
                        })
                        .catch(function(err){
                            logger.error('message: ' + err + '\n');
                        });
                }else{
                    $user.destroy();
                }
            })
            .catch(function(err){
                logger.error('message: ' + err + '\n');
            });
    }

    function loginCheck(user){
        if(user.getLoginKind() === 'L1'){
            //$window.location.href= 'pesn.html#/mainp/pmMain';

            if(window.env.platform === 'browser'){
                $rootScope.goView('pesn.html#/mainp/pmMain');
            }else{
                var tempPushType = '';
                if(window.env.platform == 'android'){
                    tempPushType = 'P1';
                }else if(window.env.platform == 'ios'){
                    tempPushType = 'P2';
                }

                GuestSvc.updatePushInfo({
                    pushId : $rootScope.UUID,     // [필수] 푸쉬아이디
                    pushType : tempPushType,                    // [필수][공코:PUSH_TYPE] 푸쉬타입
                    type : 'T1',                        // [필수] 개인
                    memberSeq : user.getUserInfoList()[0].MEMBER_SEQ+''  // [필수] 회원 일련번호
                }).then(function(data){
                    $rootScope.goView('pesn.html#/mainp/pmMain');
                }).catch(function(error){
                    $rootScope.goView('pesn.html#/mainp/pmMain');
                });
            }
        }
        else if(user.getLoginKind() === 'L2'){
            //$window.location.href= 'comp.html#/comp/compMain';

            if(window.env.platform === 'browser'){
                $rootScope.goView('comp.html#/mainc/cmMain');
            }else{
                var tempPushType = '';
                if(window.env.platform == 'android'){
                    tempPushType = 'P1';
                }else if(window.env.platform == 'ios'){
                    tempPushType = 'P2';
                }

                GuestSvc.updatePushInfo({
                    pushId : $rootScope.UUID,     // [필수] 푸쉬아이디
                    pushType : tempPushType,                    // [필수][공코:PUSH_TYPE] 푸쉬타입
                    type : 'T2',                        // [필수] 개인
                    memberSeq : user.getUserInfoList()[0].COMPANY_MEMBER_SEQ+''  // [필수] 회원 일련번호
                }).then(function(data){
                    $rootScope.goView('comp.html#/mainc/cmMain');
                }).catch(function(error){
                    $rootScope.goView('comp.html#/mainc/cmMain');
                });
            }

        }
        else{
            $message.alert("로그인을 다시 시도해주세요");
        }
    }

    function selList(){
        // selectEmerRcutList
        // selectMyRcutList
        // 'memberSeq' : d.userInfo[0].MEMBER_SEQ + '',
        // 'orderType' : '1',
        // 'startNo' : 0,
        // 'endNo' : 5
        // selectAllRcutList
        // param.orderType = "1";					//[필수] 정렬 순서[1,2,3,4] 랜덤으로 보냄(앱실행시마다변경)
        // param.startNo = 0;						//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;						//[필수] 페이징 페이지당보여질수

        GuestSvc.selectAllRcutList({
            'orderType' : vm.mod.opts.ordType,
            'startNo' : (vm.mod.ad.page - 1) * vm.mod.ad.cnt,
            'endNo' : vm.mod.ad.cnt
        })
            .then(function(d){
                console.log('selectAllRcutList called');
                //console.log(d.user.resultData);

                d.user.resultData.allRcutData.forEach(function(elem){
                    vm.mod.ad.list.push(elem);
                });
                return GuestSvc.selectEmerRcutList();
            }, _fn.err)
            .then(function(d){
                console.log('selectEmerRcutList called');
                //console.log(d.user.resultData);

                d.user.resultData.emerRcutData.forEach(function(elem){
                    vm.mod.urg.list.push(elem);
                });

                logger.debug(vm.mod);
            } , _fn.err);
    }

  $scope.$on('onRepeatLast', function (scope, element, attrs) {
    // do something
    // console.log('LAST ELEMENT!!!!!!!!');
    // _swiper.init();
    vm.SWIPER = vm.SWIPER || {};
    if(attrs.onLastRepeat === 'banner'){
        let opt = {};
        opt.direction = 'vertical';
        opt.preventClicks = false;
        opt.preventClicksPropagation = false;
        opt.autoplayDisableOnInteraction =  false;
        
        if(vm.bannerList.length <= 1){ 
          opt.loop = false; 
        }
        else { 
          opt.loop = true; 
          opt.autoplay = 4000;
        }

        vm.SWIPER = vm.SWIPER || {};
        vm.SWIPER.banner = new Swiper('.banner-swiper-container' , opt);
        window.bannerClick = vm.bannerClick;
    }
    else if(attrs.onLastRepeat === 'urg'){
        let opt = {};
        opt.direction = 'vertical';
        opt.preventClicks = false;
        opt.preventClicksPropagation = false;
        opt.autoplayDisableOnInteraction =  false;
        
        if(vm.mod.urg.list.length <= 1){ opt.loop = false; }
        else { 
          opt.loop = true; 
          opt.autoplay = 4000;
        }

        vm.SWIPER = vm.SWIPER || {};
        vm.SWIPER.urg = new Swiper('.urg-swiper-container' , opt);
        window.urgClick = vm.evt.go;
    }
    

  });
}
