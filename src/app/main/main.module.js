
angular
  .module('main', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('main', {
          url: '/main',
          templateUrl: 'main.tpl.html',
          data: { menuTitle : '메인화면', menuId : 'main', depth : 1},
          params: { param: null, backupData: null },
          controller: 'mainCtrl as main'
      })
    ;
  // $urlRouterProvider.when('/aMain', '/aMain/aMain1001m')
}
