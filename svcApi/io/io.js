
angular.module('easi.core')
  .provider('$io', $IoProvider);

function $IoProvider() {
  this.$get = ioFactory;
}

function ioFactory($log, $http, $q, $rootScope, $timeout, $storage, $window, $message, $bdMfwcomm, $user, $env, $bdApp) {
  var logger = $log(this);
  $rootScope.requestCount = 0;

  $rootScope.safeApply = function(fn) {
    var phase = this.$root.$$phase;
    if(phase == '$apply' || phase == '$digest') {
      if(fn && (typeof(fn) === 'function')) {
        fn();
      }
    } else {
      this.$apply(fn);
    }
  };
  
  //로그인 날짜 세팅
  $storage.get('loinDtm').then(function(data) {
    if(!_.isNull(data)){
      $rootScope.loinDtm = data+'';
    }else{
      $rootScope.loinDtm = '';
    }
  })
  
  return {
    api: api,
    user: user
  };
  
  function parseMsg(msg) {
    return (msg || '').replace(/(.*)(\([가-힣]+ID\:\s?[a-zA-Z0-9-_]+\)\s?)(.*)/, '$1');
  }

  function api(url, data, config) {
    
    var d = $q.defer();
    var request = {
      method: 'POST',
      url: url,
      data: data || {}
    };
    
    config = _.pick(config || {}, 'method', 'plain', 'invalidSrvcTrtRsltTypCd', 'header');

    if (config && /^get$/i.test(config.method)) {
      config.params = data;
    }
    
    request = _.assign(request, config);

    $rootScope.safeApply(function() {
      $rootScope.requestCount++;
    });

    if (request.data) {
      if (!config.plain) {
        request.data = {
          sessionParam: ($user.currentUser && $user.currentUser.getSessionParam() || {}),
          userParam: data || {},
          commonHeader: _.assign({ cordova: $env.cordova }, config.header)
        };
        var userId = 'ANONYMOUS';
        if (request.data.sessionParam && request.data.sessionParam.userId) {
          userId = request.data.sessionParam.userId;
        }
        try{
          request.data.userParam.tid = [userId, _.guid()].join('.');
        }catch(e){}
      }
    }

    logger.debug('API Request', request);
    
    // 앱에서는 Y보내야함
    if(false){
      request.data.commonHeader.encryptFlag = "Y";
    }

    if (request.data && !config.plain) {
      request.data = _.mapValues(request.data, function(param) {
        return JSON.stringify(param);
      });
    }

    // 에러 발생시 저장 할 파라미터 새성
    try{
    	var errLogParam = {};
	    errLogParam.PAGE_NAME = $window.location.href || '';
    	errLogParam.ERR_MSG_DESC = '통신에러';
	    errLogParam.SEVICE_URL = url;
  	  errLogParam.SERVICE_PARAM = JSON.stringify(request);
	    errLogParam.ERR_TYPE = '1';   //'에러타입(1:전문2:스크립트3:솔루션4:기타)'
    }catch(e){}
    // $http(request)
    var httpfn = $bdMfwcomm['mfw' + _.titleize(request.method)];
    httpfn.call($bdMfwcomm, request)
      .then(function(response) {
        $rootScope.requestCount--;
        var data = response;

        // 결과 값 체크
        if(!_.isEmpty(data.user)){
          
            // 로그인 체크 후 비로그인시 로그인 페이지로 이동
          if(data.user.loginChk == 'noLogin'){
            return d.reject('로그인 후 사용 가능합니다.');
          }
          
          if(data.user.result != 'SUCCESS'){
            var msg = data.user.resultMessage || '알수없는에러';
            $message.alert(msg);
            return d.reject(data);
          }
          
        }else{
          try{
              // 에러로그 남기는 서비스는 제외
           $message.alert('통신 오류 입니다. 잠시후 다시 시도해주세요.');
              if(url.indexOf('insertErrLog') <= -1){
                errLogParam.ERR_MSG = JSON.stringify(data);
                errLogParam.ERR_MSG_DESC = 'data.user 없음';
                errLogParam.ERR_TYPE = '1';   
                $rootScope.insertErrLog(errLogParam);
              }
            }catch(e){}
          return d.reject(data);
        }
        // 정상리턴
        return d.resolve(data);

      })
      .catch(function(response) {
        try {
          response = JSON.parse(response);
        } catch(e) {
          logger.warn(e);
        }
        $rootScope.requestCount--;
        $log.error(response);
        var msg = $rootScope.$eval('header.msg', response);
        $message.alert(msg || '네트워크 오류');
        // 에러 로그 남기기
        try{
          // 에러로그 남기는 서비스는 제외
          if(url.indexOf('insertErrLog') <= -1 && msg){
            errLogParam.ERR_MSG = JSON.stringify(response);
            errLogParam.ERR_MSG_DESC = msg || '통신에러';
            $rootScope.insertErrLog(errLogParam);
          }
        }catch(e){}
        return d.reject(response.data);
      });

    return d.promise;
  }

  function user(url, data, config) {
    return api(url, data, config)
      .then(function(result) {
        try {
          if (_.isEmpty(result.data)) {
            $message.alert('알수없는 에러(관리자에게 문의하세요.)');
            return $q.reject('result.data');
          }
          return _.chain(result.data.user)
            .clone()
            .assign({
              $response: result
            })
            .value();
        } catch(error) {
          return $q.reject(error);
        }
      });
  }
}

function ErrorString(value) {
  String.call(this, value);
  this.value = value;
}
function c() {}
c.prototype = String.prototype;
ErrorString.prototype = new c();
ErrorString.prototype.constructor = ErrorString;
ErrorString.prototype.toString = function() {
  return this.value;
}
ErrorString.prototype.valueOf = function() {
  return this.value;
}
