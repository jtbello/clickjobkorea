/* jshint browser:true */
function getConfig(target) {
  var config = {
    /**
     * 개발계
     */
      development: {
        fileDir: 'GMK_dev/',
        endPoint: {
          msdp: 'http://clickjobkorea.com',
          service: 'http://clickjobkorea.com',
        }
      },
    /**
     * 검증계
     */
    staging: {
      fileDir: 'GMK_stg/',
      endPoint: {
        msdp: 'http://clickjobkorea.com',
        service: 'http://clickjobkorea.com',
      }
    },
    /**
     * 운영계
     */
    production: {
      fileDir: 'GMK/',
      endPoint: {
        msdp: 'http://clickjobkorea.com',
        service: 'http://clickjobkorea.com',
      }
    }
    
  };
  return _.deepFreeze(config[target]);
}

angular
  .module('easi.common')
  .factory('$env', function($q, $window, $timeout, $rootScope) {
    function Env() {}
    Env.prototype.get = function() {
      var d = $q.defer();
      var self = this;
      $window.env.target = '/* @echo NODE_ENV */';
      $rootScope.NODE_ENV = '/* @echo NODE_ENV */';
      _.assign($window.env, getConfig($window.env.target));
      _.merge(self, $window.env);
      $rootScope.endPoint = self.endPoint;
      d.resolve(self);
      return d.promise;
    };
    return new Env();
  });
