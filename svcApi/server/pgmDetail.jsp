<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="doryApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Click Job Korea</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="/css/common.css">
<script src="/js/common/common.js" type="text/javascript"></script>
<script src="/js/views/comp/pgm/pgmDetailCtrl.js" type="text/javascript"></script>

</head>
<body ng-controller="pgmDetailCtrl">
<div id="wrap">
	<ng-include src="'/include/top.jsp'"></ng-include>
	<ng-include src="'/include/left.jsp'"></ng-include>
	
	<div id="section">
		<ng-include src="'/include/topMenu.jsp'"></ng-include>
		<div id="article">

			<!-- title -->
			<ng-include src="'/include/contentTitle.jsp'"></ng-include>
			<!-- --title -->

			<!--  table -->
			<div class="tblBox">

				<table class="tableType1">
					<colgroup>
						<col style="width:15%;">
						<col style="width:35%;">
						<col style="width:15%;">
						<col style="width:35%;">
					</colgroup>
					<tr>
						<th>분야선택 <b class="redPoint">*</b></th>
						<td colspan="3">
							<select class="w50" meta-Options="PGM_REUT_FIELD" meta-options-data="999$분야" ng-model="pgmReutField"></select>
						</td>
					</tr>
					<tr>
						<th>프로그램명 <b class="redPoint">*</b></th>
						<td colspan="3">
							<input type="text" class="w90" ng-model="pgmName">
						</td>
					</tr>
					<tr>
						<th>촬영기간 <b class="redPoint">*</b></th>
						<td colspan="3"><input type="text" class="datepicker" datepicker readonly="readonly" ng-model="pgmStartDtm">  ~ <input type="text" class="datepicker" datepicker readonly="readonly" ng-model="pgmEndDtm"> </td>
					</tr>
					<tr>
						<th>주요사항 <b class="redPoint">*</b></th>
						<td colspan="3">
							<textarea placeholder="주요사항을 적어주세요." class="w90 h100px" ng-model="mainInfo"></textarea>
						</td>
					</tr>
					<tr>
						<th>기타사항</th>
						<td colspan="3">
							<textarea placeholder="기타사항을 적어주세요." class="w90 h100px" ng-model="etcInfo"></textarea>
						</td>
					</tr>
					<tr>
						<th>담당자 <b class="redPoint">*</b></th>
						<td colspan="3">
						<td colspan="3">
							<table class="tableTypeInnerType w100">
								<colgroup>
									<col style="width:2%;">
									<col style="width:48%;">
									<col style="width:50%;">
								</colgroup>
								<tr>
									<td></td>
									<td><select class="w100" ng-model="chargeMemberListChkVal">
											<option ng-repeat="mem in chargeMemberList track by $index" value="{{mem.v}}">{{mem.k}}</option>
										</select>
									</td>
									<td>
										<div class="plus" ng-click="chargeMemPlus()"><a >추가</a></div>
										<div class="plus">※ 체크된 담당자가 출연자화면에 대표로 노출됩니다.</div>
										
									</td>
								</tr>
								<tr ng-repeat="mem in selChargeMemeList track by $index">
									<td><input type="checkbox" ng-model="mem.repYn" ng-click="repYnClick(mem)"></td>
									<td><input type="text" class="w97" readonly="readonly" ng-model="mem.k">
									</td>
									<td><div class="minus" ng-click="chargeMemMin(mem)"><a >삭제</a></div></td>
								</tr>
							</table>
							
							
						</td>
					</tr>
					<tr>
						<th>대표이미지</th>
						<td colspan="3">
							<form id="ajaxform" action="/api/com/fileUpload.login" method="post" enctype="multipart/form-data" >
								<label ng-if="pgmImgAttachSeq != ''">{{fileOrgName}}</label>
								<input ng-if="pgmImgAttachSeq == ''" type="file" id="file" name="uploadFile" ng-disabled="fileDisabled">
								<input type="hidden" name="busnLinkKey" value="-1"/>
							<a ng-if="pgmImgAttachSeq == ''" class="sbtn" ng-click="fileAttach()" ng-if = "pgmInsertAuth">첨부</a>
							<a ng-if="pgmImgAttachSeq != ''" class="sbtn gray" ng-click="fileDel()" ng-if = "pgmInsertAuth">삭제</a>
							</form>
						</td>
					</tr>
					<tr>
						<th>수정이력</th>
						<td colspan="3">
							<div ng-repeat="data in pgmModifyInfo">{{data. CREATE_DTM | shortDateTime}} ({{$index+1}}회 수정)</div>
						</td>
					</tr>
				</table>
				
			</div>
			<!-- //메뉴관리 (등록) -->

			<!-- btn -->
			<div class="btn_area">
				<div class="btn_left">
				</div>
				<div class="btn_right">
					<a class="btn gray" ng-click="cancelFn()">취소</a>
					<a class="btn green" ng-click="modifyPgm()" ng-if = "pgmInsertAuth">수정</a>
				</div>
			</div>
			
		</div>
	</div>
</div>
</body>
</html>