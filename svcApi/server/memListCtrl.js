var myApp = angular.module('doryApp',['ngCookies']);
myApp.provider('$io', $IoProvider);
myApp.controller('memListCtrl',['$scope','$rootScope','$io','$message','$cookieStore', '$filterSet', '$q',
				 function($scope, $rootScope, $io, $message, $cookieStore, $filterSet, $q ){
	
	var vm = $scope;
	
	/**
	   * Public variables
	   */
	  _.assign(vm, {
		  allChk : false,					// 전체 선택 여부
		  ageList: [],						// 연령리스트
		  requestList : [],					// 출연횟수 리스트
		  pgmHopeCastInfo : [],				// 희망분야 리스트
		  
		  startCreateDate : '',				// 가입일자시작
		  endCreateDate : '',				// 가입일자종료
		  startDate : '',					// 촬영가능일시작
		  endDate : '',						// 촬영가능일종료
		  
		  requestCount: '999',					// 출연횟수
		  gender : '999',					// 성별
		  startAge : '999',					// 연령시작
		  endAge :'999',					// 연령종료
		  height : '999',					// 키
		  weight : '999',					// 몸무게
		  pgmReutField : '999',				// 분야
		  pgmHopeCast : '999',				// 희망배역
		  bodyInfo : '999',					// 신체사항
		  dressInfo : '999',				// 보유의상
		  classInfo : '999',				// 보유등급
		  driveInfo : '999',				// 운전여부
		  tatooInfo : '999',				// 문신여부
		  pierInfo : '999',					// 피어싱여부
		  hairState : '999',				// 헤어상태
		  hairHeight :'999',				// 헤어길이
		  exposureInfo : '999',				// 노출여부
		  dimpleInfo : '999',				// 보조개
		  doubleEyeInfo : '999',			// 쌍커플
		  careerCount : '999',				// 경력유무
		  
		  
		  searchKind :'999',				// 검색종류
		  searchData : '',					// 검색어
		  
		  orderKind1 : 'O1',				// 정렬대상
		  orderKind2 : 'O1',				// 정렬방법
		  
		  memberList : [],					// 회원정보
		  memberListCount : 0,				// 회원전체카운트
		  
		  // 페이징 관련
		  curPage : 1,						// 현재페이지
		  viewPageCnt : 10,					// 페이지당보여질수
		  pagingData : [],					// 페이지번호 모듬
		  totPageCnt : 0,					// 전체페이지수
		  
	  });

	  /**
	   * Public methods
	   */
	  _.assign(vm, {
		  searchFn : searchFn,									// 검색
		  allChkFn : allChkFn,									// 전체 선택
		  goDetail : goDetail,									// 상세
		  excelDownload : excelDownload,						// 엑셀 다운로드
		  
		  // 페이징 관련
		  firstPageingFn : firstPageingFn,
		  beforePageingFn : beforePageingFn,
		  nextPageingFn : nextPageingFn,
		  endPageingFn : endPageingFn,
		  pageingFn : pageingFn
	  });
	  
	  // 프로그램 분야에 대한 모집 분야 코드 조회 
      $scope.$watch('pgmReutField', function(newVal, oldVal, scope) {
        
    	  if(newVal == oldVal) return false;
    	  if(newVal == '999') {
    		  vm.pgmHopeCast = '999';
  		  	  vm.pgmHopeCastInfo = [{v:'999',k:'선택'}];
    		  return false;
    	  }
    	  
    	  var param = {};
		  param.pgmReutField= newVal;							//[필수][공코: PGM_REUT_FIELD] 분야
		  
		  $io.api('/api/comp/selectPgmHopeCast.api', param)
		  .then(function(data){
		  	vm.pgmHopeCastInfo = [{v:'999',k:'선택'}];
		  	vm.pgmHopeCast = '999';
		  	_.forEach(data.user.resultData.pgmHopeCast,function(obj){
		  		vm.pgmHopeCastInfo.push({v:obj.COMMON_INFO_VALUE1, k:obj.COMMON_INFO_VALUE2});
		  	})
			  
		  })
		  .catch(function(err){
		  })
    	  
      });
      
      
      /**
	   * 엑셀 다운로드
	   */
	  function excelDownload(){
		 
		  searchFn(true)
		  	.then(function(data){
		  		
		  	//Data 가공
				var excelData = [];
				
				_.forEach(data, function(obj){
					var temp = {};
					// 의상정보
					obj.dressInfo = '';
					try{
						if(Number(obj.DRESS_BASE_COUNT) > 0) obj.dressInfo += '기본정장  '
					}catch(e){}
					try{
						if(Number(obj.DRESS_SEMI_COUNT) > 0) obj.dressInfo += '세미정장  '
					}catch(e){}
					try{
						if(Number(obj.DRESS_BLACK_SHOES) > 0) obj.dressInfo += '검정구두 '
					}catch(e){}
					try{
						if(Number(obj.DRESS_SWIMSUIT) > 0) obj.dressInfo += '수영복 '
					}catch(e){}
					try{
						if(Number(obj.DRESS_SCHOOL_UNIFORM) > 0) obj.dressInfo += '교복 '
					}catch(e){}
					try{
						if(Number(obj.DRESS_MILITARY_BOOTS) > 0) obj.dressInfo += '군화 '
					}catch(e){}
					if(vm.dressInfo == '') obj.dressInfo = '없음';
				
					temp.memberNm = obj.MEMBER_NM;
					temp.memberAge = obj.MEMBER_AGE;
					temp.supportFieldNm = obj.SUPPORT_FIELD_NM + '/' + obj.HOPE_CAST_NM;
					temp.memberGender = obj.MEMBER_GENDER_NM;
					temp.memberHp = $filterSet.fmtPhone(obj.MEMBER_HP);
					temp.memberAddr = obj.MEMBER_ADDR + ' ' + obj.MEMBER_ADDR_DETAIL;
					temp.memberSubway = obj.MEMBER_SUBWAY_NAME + '/' + obj.MEMBER_SUBWAY_TIME;
					temp.memberHeight = obj.HEIGHT + '/' + obj.WEIGHT;
					temp.requestCount = obj.REQUEST_COUNT;
					temp.createDtm = $filterSet.shortDate(obj.CREATE_DTM);
					temp.requestDtm = $filterSet.shortDate(obj.REQUEST_DTM);
					temp.penalty = $filterSet.toNumber(obj.PENALTY_COUNT) + '/' + $filterSet.toNumber(obj.STOP_COUNT);
					temp.dressInfo = obj.dressInfo;
					
					excelData.push(temp);
					
				})
			  	
			  	
			  	 var mystyle = {
			        sheetid: '회원리스트',
			        headers: true,
			        columns: [
			          {columnid:'memberNm', title: '이름', width:100, style: 'background:skyblue'},
			          {columnid:'memberAge', title: '나이', width:100, style: 'background:skyblue'},
			          {columnid:'supportFieldNm', title: '지원분야', width:200, style: 'background:skyblue'},
			          {columnid:'memberGender', title: '성별', width:100, style: 'background:skyblue'},
			          {columnid:'memberHp', title: '연락처', width:200, style: 'background:skyblue'},
			          {columnid:'memberAddr', title: '사는곳', width:300, style: 'background:skyblue'},
			          {columnid:'memberSubway', title: '근처역/시간', width:200, style: 'background:skyblue'},
			          {columnid:'memberHeight', title: '키/몸무게', width:100, style: 'background:skyblue'},
			          {columnid:'requestCount', title: '출연횟수', width:100, style: 'background:skyblue'},
			          {columnid:'createDtm', title: '가입일자', width:100, style: 'background:skyblue'},
			          {columnid:'requestDtm', title: '최근출연일', width:100, style: 'background:skyblue'},
			          {columnid:'penalty', title: '패널티(패널티/출연정지일수)', width:300, style: 'background:skyblue'},
			          {columnid:'dressInfo', title: '의상', width:300, style: 'background:skyblue'}
			        ]
			    };
		  
				alasql('SELECT * INTO XLS("MemberList_'+moment().format('YYYYMMDDHmmss')+'.xls",?) FROM ?',[mystyle,excelData]);
		  		
		  		
		  	})
		  	
      }
      
      /**
	   * 상세페이지 이동
	   */
	  function goDetail(data){
		  if(_.isNull(data)){
			  $message.alert('상세데이터가 없습니다.');
			  return false;
		  }
		  
		  var moveParam = {};
		  moveParam.curPage = vm.curPage;
		  
		  
		  
		  moveParam.startCreateDate = vm.startCreateDate;			// 가입일자시작
		  moveParam.endCreateDate = vm.endCreateDate;				// 가입일자종료
		  moveParam.startDate = vm.startDate;						// 촬영가능일시작
		  moveParam.endDate = vm.endDate;							// 촬영가능일종료
		  
		  moveParam.requestCount = vm.requestCount;					// 출연횟수
		  moveParam.gender = vm.gender;								// 성별
		  moveParam.startAge = vm.startAge;							// 연령시작
		  moveParam.endAge = vm.endAge;								// 연령종료
		  moveParam.height = vm.height;								// 키
		  moveParam.weight = vm.weight;								// 몸무게
		  moveParam.pgmReutField = vm.pgmReutField;					// 분야
		  moveParam.pgmHopeCast = vm.pgmHopeCast;					// 희망배역
		  moveParam.bodyInfo = vm.bodyInfo;							// 신체사항
		  moveParam.dressInfo = vm.dressInfo;						// 보유의상
		  moveParam.classInfo = vm.classInfo;						// 보유등급
		  moveParam.driveInfo = vm.driveInfo;						// 운전여부
		  moveParam.tatooInfo = vm.tatooInfo;						// 문신여부
		  moveParam.pierInfo = vm.pierInfo;							// 피어싱여부
		  moveParam.hairState = vm.hairState;						// 헤어상태
		  moveParam.hairHeight = vm.hairHeight;						// 헤어길이
		  moveParam.exposureInfo = vm.exposureInfo;					// 노출여부
		  moveParam.dimpleInfo = vm.dimpleInfo;						// 보조개
		  moveParam.doubleEyeInfo = vm.doubleEyeInfo;				// 쌍커플
		  moveParam.careerCount = vm.careerCount;					// 경력유무
		  
		  
		  moveParam.searchKind = vm.searchKind;						// 검색종류
		  moveParam.searchData = vm.searchData;						// 검색어
		  
		  moveParam.orderKind1 = vm.orderKind1;						// 정렬대상
		  moveParam.orderKind2 = vm.orderKind2;						// 정렬방법
		  
		  
		  moveParam.ctrlName = 'memListCtrl';
		  $rootScope.backPageSaveData('memListCtrl', moveParam);
		  $rootScope.pageMove('/web/comp/mem/memDetailInfo.dory',{memSeq : data.MEMBER_SEQ});
		  
	  }
	  
	  
      /**
	   * 전체 체크
	   */
	  function allChkFn(data){
		  if(vm.allChk){
			  var stNo = (vm.curPage * vm.viewPageCnt) - 10; 
			  var endNo = (vm.curPage * vm.viewPageCnt) - 1;
			  _.forEach(data,function(obj, idx){
				  if(idx >= stNo && idx <= endNo){
					  obj.chk = true;
				  }else{
					  obj.chk = false;
				  }
			  });
		  }else{
			  _.forEach(data,function(obj, idx){
				  obj.chk = false;
			  });
		  }
	  }
	  
	  
	  
	  /**
	   * 검색 이벤트
	   */
	  function searchFn(excelFlag){
		  var d = $q.defer();
		  var heightStart = '';
		  var heightEnd = '';
		  var weightStart = '';
		  var weightEnd = '';
		  
		  // 키 범위 구함
		  if(vm.height != '999'){
			  heightStart = vm.height.split('~')[0];
			  heightEnd = vm.height.split('~')[1];
		  }
		  
		  // 몸무게 범위 구함
		  if(vm.weight != '999'){
			  weightStart = vm.weight.split('~')[0];
			  weightEnd = vm.weight.split('~')[1];
		  }
		  
		  // 시작나이 종료나이 체크
		  if(vm.startAge != '999' && vm.endAge == '999'){
			  $message.alert('연령을 정확하게 입력해주세요.');
			  return false;
		  }
		  
		  if(vm.endAge != '999' && vm.startAge == '999'){
			  $message.alert('연령을 정확하게 입력해주세요.');
			  return false;
		  }
		  
		  if(Number(vm.startAge) > Number(vm.endAge)){
			  $message.alert('연령을 정확하게 입력해주세요.');
			  return false;
		  }
		  var param = {};
		  param.startNo = (vm.curPage -1) * vm.viewPageCnt;								//[필수] (현재페이지 -1) * 페이지당보여질수 
		  param.endNo = vm.viewPageCnt;													//[필수] 페이징 페이지당보여질수
		  
		  param.startCreateDate = !_.isDate(vm.startCreateDate) ? '' : moment(vm.startCreateDate).format('YYYY-MM-DD HH:mm:SS');		//[선택] 작성 일시 [일시 패턴 : 0000-00-00 00:00:00]
		  param.endCreateDate = !_.isDate(vm.endCreateDate) ? '' : moment(vm.endCreateDate).format('YYYY-MM-DD HH:mm:SS');				//[선택] 작성 일시 [일시 패턴 : 0000-00-00 00:00:00]

		  param.startDate = !_.isDate(vm.startDate) ? '' : moment(vm.startDate).format('YYYY-MM-DD HH:mm:SS');				//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
		  param.endDate = !_.isDate(vm.endDate) ? '' : moment(vm.endDate).format('YYYY-MM-DD HH:mm:SS');					//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
		  
		  param.searchKind = vm.searchKind;						//[선택] 조회조건 [1:지역 2:이름 3:역명4:연락처]	
		  param.searchData = vm.searchData;						//[선택] 검색어
		  
		  param.supportField = vm.pgmReutField == '999' ? '' :	vm.pgmReutField;	//[선택][공코 : PGM_REUT_FIELD] 지원분야
		  param.supportField = param.supportField.replace('P','S');
		  param.hopeCast = vm.pgmHopeCast == '999' ? '' :	vm.pgmHopeCast;			//[선택][공코 : HOPE_CAST] 희망배역
		  param.startAge = vm.startAge == '999' ? '' :	vm.startAge;						//[선택] 연령 시작
		  param.startAge = param.startAge + '';
		  param.endAge = vm.endAge == '999' ? '' :	vm.endAge;						//[선택] 연령 끝
		  param.endAge = param.endAge + '';
		  param.memberGender = vm.gender == '999' ? '' :	vm.gender;				//[선택][공코 : MEMBER_GENDER] 성별
		  param.heightStart = heightStart + '';						//[선택] 키 시작
		  param.heightEnd = heightEnd + '';							//[선택] 키 끝
		  param.weightStart = weightStart + '';						//[선택] 몸무게 시작
		  param.weightEnd = weightEnd + '';							//[선택] 몸무게 끝
		  param.requestCount = vm.requestCount == '999' ? '' :	vm.requestCount;				//[선택] 출연횟수
		  param.requestCount = param.requestCount + '';
		  param.bodyInfo = vm.bodyInfo == '999' ? '' :	vm.bodyInfo;							//[선택][공코 : BODY_INFO] 신체사항
		  param.dressInfo = vm.dressInfo == '999' ? '' :	vm.dressInfo;						//[선택][공코 : DRESS_INFO] 보유의상
		  param.classInfo = vm.classInfo == '999' ? '' :	vm.classInfo;						//[선택][공코 : CLASS_INFO] 보유 방송등급
		  param.driveInfo = vm.driveInfo == '999' ? '' :	vm.driveInfo;						//[선택]운전여부
		  param.hairHeight = vm.hairHeight == '999' ? '' :	vm.hairHeight;						//[선택][공코 : HAIR_HEIGHT] 헤어길이
		  param.hairState = vm.hairState == '999' ? '' :	vm.hairState;						//[선택][공코 : HAIR_HEIGHT] 헤어상태
		  
		  param.exposureInfo = vm.exposureInfo == '999' ? '' :	vm.exposureInfo;				//[선택][공코 : EXPOSURE_INFO] 노출여부
		  param.dimpleInfo = vm.dimpleInfo == '999' ? '' :	vm.dimpleInfo;						//[선택][공코 : DIMPLE_INFO] 보조개
		  param.doubleEyeInfo = vm.doubleEyeInfo == '999' ? '' :	vm.doubleEyeInfo;			//[선택][공코 : DOUBLE_EYE_INFO] 쌍커플
		  param.tatooInfo = vm.tatooInfo == '999' ? '' :	vm.tatooInfo;						//[선택][공코 : TATOO_INFO] 타투여부
		  param.pierInfo = vm.pierInfo == '999' ? '' :	vm.pierInfo;							//[선택][공코 : PIER_INFO] 피어싱여부
		  param.careerCount = vm.careerCount == '999' ? '' :	vm.careerCount;					//[선택]경력유무
		  
		  param.orderKind1 = vm.orderKind1;					//정렬 대상
		  param.orderKind2 = vm.orderKind2;					//정렬 방법
		  
		  // 엑셀 다운로드 여부
		  if(excelFlag){
			  param.excelFlag =excelFlag;
		  }
		  
		  $io.api('/api/comp/selectMemberList.api', param)
		  .then(function(data){
			  if(data.user.resultData.memberList.length > 0){
				  
				  // 엑셀 다운로드
				  if(excelFlag){
					  d.resolve(data.user.resultData.memberList);
				  }
				  
				  vm.memberList = data.user.resultData.memberList;
				  vm.memberListCount = data.user.resultData.memberListCount;
				  
				  // 의상정보
				  _.forEach(vm.memberList, function(obj){
					  obj.dressInfo = '';
					  try{
						  if(Number(obj.DRESS_BASE_COUNT) > 0) obj.dressInfo += '기본정장  '
					  }catch(e){}
					  try{
						  if(Number(obj.DRESS_SEMI_COUNT) > 0) obj.dressInfo += '세미정장  '
					  }catch(e){}
					  try{
						  if(Number(obj.DRESS_BLACK_SHOES) > 0) obj.dressInfo += '검정구두 '
					  }catch(e){}
					  try{
						  if(Number(obj.DRESS_SWIMSUIT) > 0) obj.dressInfo += '수영복 '
					  }catch(e){}
					  try{
						  if(Number(obj.DRESS_SCHOOL_UNIFORM) > 0) obj.dressInfo += '교복 '
					  }catch(e){}
					  try{
						  if(Number(obj.DRESS_MILITARY_BOOTS) > 0) obj.dressInfo += '군화 '
					  }catch(e){}
					  if(vm.dressInfo == '') obj.dressInfo = '없음';
				  })
				  
				  
				  // 전체 페이지 정보입력
				  vm.totPageCnt = Math.floor((vm.memberListCount / vm.viewPageCnt) + ((vm.memberListCount % vm.viewPageCnt) == 0 ? 0 : 1));
				  vm.pagingData = [];
				  for (var i = 1; i <= vm.totPageCnt; i++) {
					  vm.pagingData.push({pageNo : i});
				  }
				  
			  }else{
				  vm.memberList = [];
				  vm.memberListCount = 0;
				  vm.pagingData = [];
			  }
			  
			  d.resolve();
		  })
		  .catch(function(err){
		  })
		  
		  return d.promise;
	  }
	  
	  
	  /**
	   * Event handlers
	   */
	  function destroy(event) {
		  console.log(event);
	  }
	  
	  /**
	   * 페이징 - 처음으로
	   */
	  function firstPageingFn(){
		  if(vm.curPage == 1) return false;
		  vm.curPage = 1;
		  searchFn();
	  }
	  
	  /**
	   * 페이징 - 이전으로
	   */
	  function beforePageingFn(){
		  if(vm.curPage == 1) return false;
		  vm.curPage = vm.curPage - 1;
		  if(vm.curPage < 1)  vm.curPage = 1;
		  searchFn();
	  }
	  
	  /**
	   * 페이징 - 다음으로
	   */
	  function nextPageingFn(){
		  if(vm.curPage == vm.totPageCnt) return false;
		  vm.curPage = vm.curPage + 1;
		  if(vm.curPage > vm.totPageCnt)  vm.curPage = vm.totPageCnt;
		  searchFn();
	  }
	  
	  /**
	   * 페이징 - 마지막으로
	   */
	  function endPageingFn(){
		  if(vm.curPage == vm.totPageCnt) return false;
		  vm.curPage = vm.totPageCnt;
		  searchFn();
	  }
	  
	  /**
	   * 페이징 - 페이지번호클릭
	   */
	  function pageingFn(curPage){
		  vm.curPage = curPage;
		  searchFn();
	  }
	  
	  function init() {
		  console.log('init', vm);
		  $scope.$on('$destroy', destroy);
		  
		  $rootScope.initProc(1);
		  
		  // 뒤로가기로왔을경우처리
		  try{
			  var backData = $rootScope.backPageInit('memListCtrl');
			  if(backData){
				  if(backData.backFlag){
					  _.assign(vm, backData);
					  searchFn();
				  }else{
					  searchFn();
				  }
			  }else{
				  searchFn();
			  }
		  }catch(e){
			  searchFn();
		  }
		  
		  // 연령 Data 생성
		  vm.ageList = [{v :'999', k: '선택'}]
		  for(var i = 1; i <= 100; i++){
			  vm.ageList.push({v: i+'', k: i});
		  }
		  
		  // 출연횟수 Data 생성
		  vm.requestList = [{v :'999', k: '선택'}];
		  for(var i = 1; i <= 50; i++){
			  vm.requestList.push({v: i+'', k: i});
		  }
		 
		  
	  }
	  
	  
	
	  init();
	
}]);


window.document.write('<script src="/js/common/afterInjectScript.js" type="text/javascript"></script>');