<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div id="nav">
		<h2 class="lnbTit"><span>{{rootSelBigMenu.COMMON_INFO_VALUE2}}</span></h2>
		<ul class="lnb">
			<li ng-repeat="menu1 in rootmidMenu" on-finish-render ng-click="menu1.subMenu.length == 0 && rootmidMenuClick(menu1)"><a href="#" ng-class="{'active' : menu1 == rootSelMidMenu}">{{menu1.COMMON_INFO_VALUE2}}</a><!-- 메뉴 활성화 : class="active" -->
				<ul ng-if="menu1.subMenu.length > 1">
					<li ng-repeat="menu2 in menu1.subMenu" on-finish-render  ng-click="rootmidMenuClick(menu2,menu1)"><a href="#" ng-class="{'active' : menu2 == rootSelMidMenu}">{{menu2.COMMON_INFO_VALUE2}}</a></li><!-- 메뉴 활성화 : class="active" -->
				</ul>
			</li>
		</ul>
	</div>