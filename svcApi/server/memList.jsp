<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html ng-app="doryApp">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Click Job Korea</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="/css/common.css">
<script src="/js/common/common.js" type="text/javascript"></script>
<script src="/js/views/comp/mem/memListCtrl.js" type="text/javascript"></script>

</head>
<body ng-controller="memListCtrl">
<div id="wrap">
	<ng-include src="'/include/top.jsp'"></ng-include>
	<ng-include src="'/include/left.jsp'"></ng-include>
	
	<div id="section">
		<ng-include src="'/include/topMenu.jsp'"></ng-include>
		<div id="article">

			<!-- title -->
			<ng-include src="'/include/contentTitle.jsp'"></ng-include>
			<!-- --title -->

			<!--  table -->
			<div class="tblBox">

				<table class="tableType1">
					<colgroup>
						<col style="width:10%;">
						<col style="width:40%;">
						<col style="width:10%;">
						<col style="width:40%;">
					</colgroup>
					<tr>
						<th>가입일</th>
						<td>
							<input type="date" ng-model="startCreateDate" class="w30"> <a href="#" class="date"><img src="/images/btn_calendar.png" alt="달"></a> ~ <input type="date" ng-model="endCreateDate" class="w30"> <a href="#" class="date"><img src="/images/btn_calendar.png" alt="달"></a>
						</td>
						<th>출연횟수</th>
						<td>
							<select class="w40 mb10" ng-model="requestCount">
								<option ng-repeat="data in requestList track by $index" value="{{data.v}}">{{data.k}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>촬영가능일</th>
						<td>
							<input type="date" ng-model="startDate" class="w30"> <a href="#" class="date"><img src="/images/btn_calendar.png" alt="달"></a> ~ <input type="date" ng-model="endDate" class="w30"> <a href="#" class="date"><img src="/images/btn_calendar.png" alt="달"></a>
						</td>
						<th>분야</th>
						<td>
							<select class="w40" meta-Options="PGM_REUT_FIELD" meta-options-data="999$분야" ng-model="pgmReutField"></select> &nbsp;
							<select class="w40" ng-model="pgmHopeCast" ng-disabled="pgmReutField == '999'">
								<option ng-repeat="data in pgmHopeCastInfo track by $index" value="{{data.v}}">{{data.k}}</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>성별</th>
						<td>
							<select class="w50" meta-Options="GENDER" meta-options-data="999$전체" ng-model="gender"></select> &nbsp;
						</td>
						<th>연령</th>
						<td>
							<select class="w40 mb10" ng-model="startAge">
								<option ng-repeat="data in ageList track by $index" value="{{data.v}}">{{data.k}}</option>
							</select>세 ~
							<select class="w40 mb10" ng-model="endAge">
								<option ng-repeat="data in ageList track by $index" value="{{data.v}}">{{data.k}}</option>
							</select>세
						</td>
					</tr>
					<tr>
						<th>경력</th>
						<td>
							<select class="w50" ng-model="careerCount">
								<option value="999">전체</option>
								<option value="Y">유</option>
								<option value="N">무</option>
							</select>
						</td>
						<th>키/몸무게</th>
						<td>
							<select class="w40 mb10" ng-model="height">
								<option value="999">키</option>
								<option value="100~0">100이하</option>
								<option value="100~110">100~110</option>
								<option value="110~120">110~120</option>
								<option value="120~130">120~130</option>
								<option value="130~140">130~140</option>
								<option value="140~145">140~145</option>
								<option value="145~150">145~150</option>
								<option value="150~155">150~155</option>
								<option value="155~160">155~160</option>
								<option value="160~165">160~165</option>
								<option value="165~170">165~170</option>
								<option value="170~175">170~175</option>
								<option value="175~180">175~180</option>
								<option value="180~900">180이상</option>
							</select>&nbsp;
							<select class="w40 mb10" ng-model="weight">
								<option value="999">몸무게</option>
								<option value="30~0">30이하</option>
								<option value="30~35">30~35</option>
								<option value="34~40">34~40</option>
								<option value="40~45">40~45</option>
								<option value="45~50">45~50</option>
								<option value="50~55">50~55</option>
								<option value="55~60">55~60</option>
								<option value="60~65">60~65</option>
								<option value="65~70">65~70</option>
								<option value="70~75">70~75</option>
								<option value="75~80">75~80</option>
								<option value="80~85">80~85</option>
								<option value="85~90">85~90</option>
								<option value="90~95">90~95</option>
								<option value="95~100">95~100</option>
								<option value="100~900">100이상</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>신체사항</th>
						<td>
							<select class="w50" meta-Options="BODY_INFO" meta-options-data="999$전체" ng-model="bodyInfo"></select> &nbsp;
						</td>
						<th>보유의상</th>
						<td>
							<select class="w50" meta-Options="DRESS_INFO" meta-options-data="999$의상선택" ng-model="dressInfo"></select>
						</td>
					</tr>
					<tr>
						<th>방송등급</th>
						<td>
							<select class="w50" meta-Options="CLASS_INFO" meta-options-data="999$전체" ng-model="classInfo"></select> &nbsp;
						</td>
						<th>운전여부</th>
						<td>
							<select class="w40 mb10" ng-model="driveInfo">
								<option value="999">선택</option>
								<option value="D1">자동차운전가능</option>
								<option value="D2">오토바이운전가능</option>
								<option value="D3">자전거운전가능</option>
								<option value="ALL">모든운전가능</option>
							</select>
						</td>
					</tr>
					<tr>
						<th>문신,피어싱</th>
						<td>
							<select class="w40" meta-Options="TATOO_INFO" meta-options-data="999$문신" ng-model="tatooInfo"></select> &nbsp;
							<select class="w40" meta-Options="PIER_INFO" meta-options-data="999$피어싱" ng-model="pierInfo"></select> &nbsp;
						</td>
						<th>헤어</th>
						<td>
							<select class="w40" meta-Options="HAIR_STATE" meta-options-data="999$컬러" ng-model="hairState"></select> &nbsp;
							<select class="w40" meta-Options="HAIR_HEIGHT" meta-options-data="999$길이" ng-model="hairHeight"></select> &nbsp;
						</td>
					</tr>
					<tr>
						<th>노출여부</th>
						<td>
							<select class="w100" meta-Options="EXPOSURE_INFO" meta-options-data="999$전체" ng-model="exposureInfo"></select> &nbsp;
						</td>
						<th>보조개,쌍커플</th>
						<td>
							<select class="w40" meta-Options="DIMPLE_INFO" meta-options-data="999$보조개선택" ng-model="dimpleInfo"></select> &nbsp;
							<select class="w40" meta-Options="DOUBLE_EYE_INFO" meta-options-data="999$쌍커플선택" ng-model="doubleEyeInfo"></select> &nbsp;
						</td>
					</tr>
					<tr>
						<th>상세검색</th>
						<td colspan="3">
							<select class="w20" ng-model="searchKind">
								<option value="999">전체</option>
								<option value="2">이름</option>
								<option value="4">연락처</option>
								<option value="1">사는곳</option>
								<option value="3">근처역</option>
							</select> <input type="text" class="w50" ng-model="searchData">
							<a href="#"><img src="/images/btn_search_b.png" alt="검색" ng-click="searchFn()"></a>
						</td>
					</tr>
				</table>
				
				<p class="info mt50">
					총 <strong>{{memberListCount | number}}건</strong>
					<select class="w20" ng-model="orderKind1">
						<option value="O1">이름</option>
						<option value="O2">나이</option>
						<option value="O3">성별</option>
						<option value="O4">키</option>
						<option value="O5">몸무게</option>
						<option value="O6">출연횟수</option>
					</select>
					<select class="w30" ng-model="orderKind2">
						<option value="O1">오름차순으로보기</option>
						<option value="O2">내림차순으로보기</option>
					</select>
				
				</p>
				<table class="tableType2">
					<thead>
						<tr>
							<th scope="col"><input type="checkbox" ng-model="allChk" ng-click="allChkFn(rcutList);"></th>
							<th scope="col">이름</th>
							<th scope="col">나이</th>
							<th scope="col">지원분야</th>
							<th scope="col">성별</th>
							<th scope="col">연락처</th>
							<th scope="col">사는곳</th>
							<th scope="col">근처역/시간</th>
							<th scope="col">키/몸무게</th>
							<th scope="col">출연횟수</th>
							<th scope="col">가입일자</th>
							<th scope="col">최근출연일</th>
							<th scope="col">패널티(패널티/출연정지일수)</th>
							<th scope="col">의상</th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="data in memberList" >
							<td class="txt_center"><input type="checkbox" ng-model="data.chk"></td>
							<td ng-click="goDetail(data)">{{data.MEMBER_NM}}</td>
							<td ng-click="goDetail(data)">{{data.MEMBER_AGE}}</td>
							<td ng-click="goDetail(data)">{{data.SUPPORT_FIELD_NM}}/{{data.HOPE_CAST_NM}}</td>
							<td ng-click="goDetail(data)">{{data.MEMBER_GENDER_NM}}</td>
							<td ng-click="goDetail(data)">{{data.MEMBER_HP | fmtPhone}}</td>
							<td ng-click="goDetail(data)">{{data.MEMBER_ADDR}} {{data.MEMBER_ADDR_DETAIL}}</td>
							<td ng-click="goDetail(data)">{{data.MEMBER_SUBWAY_NAME}}/{{data.MEMBER_SUBWAY_TIME}}</td>
							<td ng-click="goDetail(data)">{{data.HEIGHT}}/{{data.WEIGHT}}</td>
							<td ng-click="goDetail(data)">{{data.REQUEST_COUNT}}</td>
							<td ng-click="goDetail(data)">{{data.CREATE_DTM | shortDate}}</td>
							<td ng-click="goDetail(data)">{{data.REQUEST_DTM | shortDate}}</td>
							<td ng-click="goDetail(data)">{{data.PENALTY_COUNT | number}}/{{data.STOP_COUNT | number}}</td>
							<td ng-click="goDetail(data)">{{data.dressInfo}}</td>
						</tr>
						
						<tr ng-if="memberList.length <= 0">
							<td class="txt_center" colspan="14">조회된 정보가 없습니다.</td>
						</tr>
						
					</tbody>
				</table>
			</div>
			<!-- //메뉴관리 (등록) -->

			<!-- btn -->
			<div class="btn_area">
				<div class="btn_left">
					<a href="#" class="btn green" ng-click="excelDownload()">엑셀다운로드</a>
				</div>
				<div class="btn_right">
					<a href="#" class="btn green">알림발송</a>
					<a href="#" class="btn red">출연요청</a>
				</div>
			</div>
			
			<!-- paging -->
			<div class="paging" ng-if="pagingData.length > 0">
				<a href="#" class="img" ng-click="firstPageingFn()"><img src="/images/btn_first.gif" alt="first"></a>
				<a href="#" class="img" ng-click="beforePageingFn()"><img src="/images/btn_previous.gif" alt="previous"></a>
				<a href="#" class="" ng-repeat="data in pagingData" ng-click="pageingFn(data.pageNo)" ng-class="{'active' : data.pageNo == curPage}">{{data.pageNo}}</a>
				<a href="#" class="img" ng-click="nextPageingFn()"><img src="/images/btn_next.gif" alt="next"></a>
				<a href="#" class="img" ng-click="endPageingFn()"><img src="/images/btn_end.gif" alt="end"></a>
			</div>
			<!-- //paging -->

		</div>
	</div>
</div>
</body>
</html>