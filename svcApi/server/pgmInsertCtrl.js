var myApp = angular.module('doryApp',['ngCookies']);
myApp.provider('$io', $IoProvider);
myApp.controller('pgmInsertCtrl',['$scope','$rootScope','$io','$message','$cookieStore', '$filterSet',
				 function($scope, $rootScope, $io, $message, $cookieStore, $filterSet){
	
	var vm = $scope;
	
	/**
	   * Public variables
	   */
	  _.assign(vm, {
		  chargeMemberList : [],			// 담당자 리스트
		  selChargeMemeList : [],			// 선택한 담당자 리스트
		  chargeMemberListChkVal : null,	// 추가 대상 담당자
		  fileDisabled : false,				// 파일선택가능
		  pgmImgAttachSeq : '',				// 대표상품이미지첨부파일번호
		  fileOrgName : '',					// 첨부파일 원본파일이름
		  
		  pgmStartDtm  : '',					//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
		  pgmEndDtm  : '',						//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
		  pgmReutField : '',					//[필수][공코:PGM_REUT_FIELD] 지원분야
		  pgmName : '',							//[필수] 프로그램명
		  mainInfo : '',						//[필수] 주요사항
		  etcInfo : '',							//[선택] 기타사항
		  
	  });

	  /**
	   * Public methods
	   */
	  _.assign(vm, {
		  selChargeMemberList : selChargeMemberList,	// 담당자리스트 조회
		  chargeMemPlus : chargeMemPlus,				// 담당자 추가
		  chargeMemMin : chargeMemMin,					// 담당자 삭제
		  repYnClick : repYnClick,						// 대표담당자체크
		  fileAttach :fileAttach,						// 파일 전송
		  fileDel: fileDel,								// 파일 삭제
		  insertPgm : insertPgm,						// 프로그램 등록
		  cancelFn :cancelFn,							// 취소
	  });
	  
	  /**
	   * 대표담당자체크
	   */
	  function repYnClick(mem){
		  _.forEach(vm.selChargeMemeList, function(obj){
			  obj.repYn = false;
		  })
		  mem.repYn = true;
	  }
	  
	  /**
	   * 프로그램 등록 취소
	   */
	  function cancelFn(){
		  $rootScope.backPage('pgmListCtrl', '/web/comp/pgm/pgmList.dory');
	  }
	  
	  
	  /**
	   * 프로그램 등록
	   */
	  function insertPgm(){
		  
		  var chk = true;
		  
		  chk = chk && $rootScope.nullChk(vm.pgmReutField, 		'모집분야를 입력해주세요.',false,'999');
		  chk = chk && $rootScope.nullChk(vm.pgmName, 				'프로그램명을 입력해주세요.');
		  chk = chk && $rootScope.nullChk({st : vm.pgmStartDtm, ed : vm.pgmEndDtm}, '프로그램', true);
		  chk = chk && $rootScope.nullChk(vm.mainInfo, 			'주요사항을 입력해주세요.');
		  
		  if(!chk) return false;
		  
		  // 첨부파일 첨부 확인
		  if($('#file').val() != '' && vm.pgmImgAttachSeq == ''){
			  $message.alert('선택하신 파일을 첨부해주세요.');
			  return false;
		  }
		  
		  // 담당자 선택여부확인
		  if(vm.selChargeMemeList.length <= 0){
			  $message.alert('담당자를 선택해주세요.');
			  return false;
		  }
		  
		  // 대표 담당자 선택 여부 확인
		  var findCharge = _.find(vm.selChargeMemeList, {repYn: true});
		  if(!findCharge){
			  $message.alert('대표 담당자를 선택해주세요.');
			  return false;
		  }
		  
		  // 로그인한 본인이 담당자 리스트에있는지 확인
		  var loginCompanyMemberSeq = $rootScope.rootUSERINFO.userInfo[0].COMPANY_MEMBER_SEQ;
		  var loginCharge = _.find(vm.selChargeMemeList, {v : loginCompanyMemberSeq+''});
		  if(!loginCharge && !$rootScope.rootUSERINFO.adminYn && !$rootScope.rootUSERINFO.comAdminYn){
			  $message.alert('로그인 하신 담당자가 포함되지 않았습니다.');
			  return false;
		  }
		  
		  var param = {};
		  param.pgmStartDtm = _.isEmpty(vm.pgmStartDtm) ? '' : moment(vm.pgmStartDtm).format('YYYY-MM-DD HH:mm:SS');			//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
		  param.pgmEndDtm = _.isEmpty(vm.pgmEndDtm) ? '' : moment(vm.pgmEndDtm).format('YYYY-MM-DD HH:mm:SS');					//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
		  param.pgmReutField = vm.pgmReutField;						//[필수][공코:PGM_REUT_FIELD] 지원분야
		  param.pgmName = vm.pgmName;								//[필수] 프로그램명
		  param.mainInfo = vm.mainInfo;								//[필수] 주요사항
		  param.etcInfo = vm.etcInfo;								//[선택] 기타사항
		  param.pgmImgAttachSeq = vm.pgmImgAttachSeq;				//[선택] 대표이미지 파일일련번호
		  
		 
		  
		  // 담당자정보
		  param.companyMemberSeq = findCharge.v;					//[필수] 대표 담당자 일련번호
		  var chargeList = [];
		  _.forEach(vm.selChargeMemeList, function(obj){
			  chargeList.push({chargeCompanyMemberSeq : obj.v, companyMemberSeq : $rootScope.rootUSERINFO.userInfo[0].COMPANY_MEMBER_SEQ});
		  })
		  param.chargeList = chargeList;							//[필수] 담당자정보
		  
		  // 프로그램 등록 호출
		  $io.api('/api/comp/insertPgm.api', param)
		  .then(function(data){
		  	$message.alert('정상적으로 등록되었습니다.');
		  	$rootScope.backPage('pgmListCtrl', '/web/comp/pgm/pgmList.dory');
		  })
		  .catch(function(err){
		  })
		  
	  }
	  
	  /**
	   * 파일삭제
	   */
	  function fileDel(){
		  
		  var chk = $rootScope.nullChk(vm.pgmImgAttachSeq, 			'파일정보가 없습니다.');
		  if(!chk){
			  $('#file').val('');
			  return false;
		  }
		  
		  var confirmResult =$message.confirm('정말 삭제하시겠습니까?');
		  if(!confirmResult) return false;
		  
		  $("#file").val("");
		  vm.fileDisabled = false;
		  
		  var param = {};
		  param.attachSeq = vm.pgmImgAttachSeq;			// [필수]파일등록 후 결과값에 파일 일련번호
		  param.fileOrgName = vm.fileOrgName;			// [필수]파일등록 후 결과값에 파일 원본파일이름
		  
		  $io.api('/api/com/fileDelete.login', param)
		  .then(function(data){
			vm.pgmImgAttachSeq = '';
		  	$message.alert('정상적으로 삭제되었습니다.');
		  })
		  .catch(function(err){
		  })
	  }
	  
	  
	  /**
	   * 파일전송
	   */
	  function fileAttach(){
		  vm.fileDisabled = true;
		  $rootScope.lodingRequestCount++;
		  $("#ajaxform").ajaxForm({
	            url : "/api/com/fileUpload.login",
	            enctype : "multipart/form-data",
	            dataType : "json",
	            beforeSubmit: function (data, frm, opt) { 
	            	$rootScope.lodingRequestCount--;
	            	var objFind = _.find(data, {type : 'file'});
	            	if(objFind){
	            		if(!_.isNull(objFind.value) && objFind.value != ""){
	            			return true;		
	            		}
	            	}
	            	vm.fileDisabled = false;
	            	$message.alert('파일 선택 후 진행 해주세요.');
	            	return false;
	            },
	            error : function(){
	            	vm.fileDisabled = false;
	            	$rootScope.lodingRequestCount--;
	            	$message.alert("파일 전송 실패") ;
	            },
	            success : function(result){
	            	$rootScope.lodingRequestCount--;
	            	vm.pgmImgAttachSeq = result.user.resultData.attachSeq;
	            	vm.fileOrgName = result.user.resultData.orgName;
	            	$message.alert("파일 전송 성공");
	            }
	        });
	 
	        $("#ajaxform").submit();
	  }
	  
	  /**
	   * 담당자 삭제
	   */
	  function chargeMemMin(mem){
		  
		  vm.selChargeMemeList = _.reject(vm.selChargeMemeList,{v : mem.v});
	  }
	  
	  /**
	   * 담당자 추가
	   */
	  function chargeMemPlus(){
		  if(vm.chargeMemberListChkVal == null){
			  $message.alert('추가 대상 담당자가 없습니다.');
			  return false;
		  }
		  
		  var findObj = _.find(vm.selChargeMemeList, {v : vm.chargeMemberListChkVal});
		  if(findObj){
			  $message.alert('이미 추가된 담당자입니다.');
			  return false;
		  }
		  
		  vm.selChargeMemeList.push(_.find(vm.chargeMemberList, {v : vm.chargeMemberListChkVal}));
	  }
	  
	  
	  /**
	   * 담당자 리스트 조회
	   */
	  function selChargeMemberList(){
		  
		  var param = {};
		  
		  $io.api('/api/comp/selectCompanyMemberListInfo.api', param)
		  .then(function(data){
			  if(data.user.resultData.companyMemberList.length > 0){
				  _.forEach(data.user.resultData.companyMemberList, function(obj){
					  vm.chargeMemberList.push({k:obj.COMPANY_MEMBER_POSITION + ' ' + obj.COMPANY_MEMBER_NAME + '('+$filterSet.fmtPhone(obj.COMPANY_MEMBER_PHONE_NO)+')',v:obj.COMPANY_MEMBER_SEQ+''});
				  })
				  vm.chargeMemberListChkVal = vm.chargeMemberList[0].v;
			  }else{
				  vm.chargeMemberList = [];
				  $message.alert('담당자 리스트 조회에 실패했습니다.');
			  }
		  })
		  .catch(function(err){
			  $message.alert('담당자 리스트 조회에 실패했습니다.');
		  })
	  }
	  
	  
	  function init() {
		  console.log('init', vm);
		  $scope.$on('$destroy', destroy);
		  
		  $rootScope.initProc(0);
		  
		  // 담당자 리스트 조회
		  selChargeMemberList();
	  }

	  /**
	   * Event handlers
	   */
	  function destroy(event) {
		  console.log(event);
	  }
	  
	
	  init();
	
}]);


window.document.write('<script src="/js/common/afterInjectScript.js" type="text/javascript"></script>');