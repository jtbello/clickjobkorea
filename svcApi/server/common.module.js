myApp
  .directive('onFinishRender',['$timeout', '$parse', function ($timeout, $parse) {
    return {
        restrict: 'A',
        link: function (scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function () {
                    scope.$emit('ngRepeatFinished');
                    if(!!attr.onFinishRender){
                      $parse(attr.onFinishRender)(scope);
                    }
                });
            }
            
            if (!!attr.onStartRender) {
                if (scope.$first === true) {
                    $timeout(function () {
                        scope.$emit('ngRepeatStarted');
                        if ( !! attr.onStartRender) {
                            $parse(attr.onStartRender)(scope);
                        }
                    });
                }
            }
            
        }
    }
  }])
  .directive("datepicker", function () {
	  return {
	    restrict: "A",
	    require: "ngModel",
	    link: function (scope, elem, attrs, ngModelCtrl) {
	      var updateModel = function (dateText) {
	        scope.$apply(function () {
	          ngModelCtrl.$setViewValue(dateText);
	        });
	      };
	      var options = {
	    		  	dateFormat: "yy-mm-dd",
					showOtherMonths: true,
					selectOtherMonths: true,
					closeText: "닫기",
					prevText: "이전달",
					nextText: "다음달",
					currentText: "오늘",
					monthNames: [ "1월","2월","3월","4월","5월","6월",
					"7월","8월","9월","10월","11월","12월" ],
					monthNamesShort: [ "1월","2월","3월","4월","5월","6월",
					"7월","8월","9월","10월","11월","12월" ],
					dayNames: [ "일요일","월요일","화요일","수요일","목요일","금요일","토요일" ],
					dayNamesShort: [ "일","월","화","수","목","금","토" ],
					dayNamesMin: [ "일","월","화","수","목","금","토" ],
					weekHeader: "주",
					dateFormat: "yy-mm-dd",
					firstDay: 0,
					isRTL: false,
					showMonthAfterYear: true,
					yearSuffix: "년",
					showOn:"button",
		            buttonImage:"/images/btn_cal.png",
		            buttonImageOnly:true,
		            closeText:'삭제',
		            showButtonPanel: true,
		            onClose: function () {
		                if ($(window.event.srcElement).hasClass('ui-datepicker-close')) {
		                    $(this).val('');
		                }
		            },
			        onSelect: function (dateText) {
			          updateModel(dateText);
			        }
	      };
	      $(elem).datepicker(options);
	    }
	  }
	})
	 .directive("multidatepicker", function () {
	  return {
	    restrict: "A",
	    require: "ngModel",
	    link: function (scope, elem, attrs, ngModelCtrl) {
	      var updateModel = function (dateText) {
	        scope.$apply(function () {
	        	var setData = ''
	        	if(ngModelCtrl.$viewValue != ''){
	        		setData = ngModelCtrl.$viewValue + ',' + dateText;
	        	}else{
	        		setData = dateText;
	        	}
	        	
	          ngModelCtrl.$setViewValue(setData);
	        });
	      };
	      var options = {
	    		  	dateFormat: "yy-mm-dd",
					showOtherMonths: true,
					selectOtherMonths: true,
					closeText: "닫기",
					prevText: "이전달",
					nextText: "다음달",
					currentText: "오늘",
					monthNames: [ "1월","2월","3월","4월","5월","6월",
					"7월","8월","9월","10월","11월","12월" ],
					monthNamesShort: [ "1월","2월","3월","4월","5월","6월",
					"7월","8월","9월","10월","11월","12월" ],
					dayNames: [ "일요일","월요일","화요일","수요일","목요일","금요일","토요일" ],
					dayNamesShort: [ "일","월","화","수","목","금","토" ],
					dayNamesMin: [ "일","월","화","수","목","금","토" ],
					weekHeader: "주",
					dateFormat: "yy-mm-dd",
					firstDay: 0,
					isRTL: false,
					showMonthAfterYear: true,
					yearSuffix: "년",
					showOn:"button",
		            buttonImage:"/images/btn_calendar.png",
		            buttonImageOnly:true,
		            closeText:'삭제',
		            showButtonPanel: true,
		            onClose: function () {
		                if ($(window.event.srcElement).hasClass('ui-datepicker-close')) {
		                    $(this).val('');
		                }
		            },
			        onSelect: function (dateText) {
			          updateModel(dateText);
			        }
	      };
	      $(elem).multiDatesPicker(options)
	    }
	  }
	})
  .run(function($rootScope, $log, $message, $io) {
	
	 $rootScope.holdbackGroundZindex = 10000;
	 
	 /**
	   * 정보변경 
	   */
	 $rootScope.modifyMyinfo = function(divId){
		 window.localStorage.setItem("commonClick", "true");
		 location.href = '/web/comp/auth/chargeMemModify.dory';
	 }
	 
	 /**
	   * 팝업 닫기
	   */
	 $rootScope.viewPopupClose = function(divId){
		  $('#' + divId).hide();
		  $('body').css('position','');
		  $rootScope.holdbackGroundZindex--;
		  $rootScope.holdbackGroundZindex--;
		  $('#viewRequestMemModal .holdbackGround').css('z-index','');
		  $('#viewRequestMemModal .modalContent').css('z-index','');
		 
	 }
	 
	 /**
	   * 팝업 열기
	   */
	 $rootScope.viewPopup = function(divId){
		  $('body').css('position','fixed');
		  
		  $('#' + divId).show();
		  $rootScope.holdbackGroundZindex++;
		  $rootScope.holdbackGroundZindex++;
		  $('#viewRequestMemModal .holdbackGround').css('z-index',$rootScope.holdbackGroundZindex);
		  $('#viewRequestMemModal .modalContent').css('z-index',$rootScope.holdbackGroundZindex+1);
		 
	 }
	 
	 /**
	   * 쿠폰선물(서비스호출)
	   */
	 $rootScope.comPushSend = function(){
		 
		  var chk = true;
		  chk = chk && $rootScope.nullChk($rootScope.couponSendMemKind, 		'쿠폰 선물 대상 종류 코드가 없습니다.');
		  chk = chk && $rootScope.nullChk($rootScope.couponSendCpCount, 		'쿠폰 선물 개수 정보가 없습니다.');
		  if(!chk) return false;
		 
		  _.forEach($rootScope.couponSendMemberList, function(obj,idx){
			  
			  // 쿠폰 선물
			  var param = {};
			  if($rootScope.couponSendMemKind == 'B2'){
				  param.busnLinkKey = obj.COMPANY_SEQ;						// 회원 및 회사 일련번호
			  }else{
				  param.busnLinkKey = obj.MEMBER_SEQ;						// 회원 및 회사 일련번호
			  }
			  param.busnLinkKeyType = $rootScope.couponSendMemKind;		// B1 : 회원 B2 : 회사		
			  param.cpCount = $rootScope.couponSendCpCount;				// 선물 개수
			  $io.api('/api/comp/couponSend.api', param)
			  .then(function(data){
				  	if(idx+1 == $rootScope.couponSendMemberList.length){
				  		$message.alert('정상적으로 처리 되었습니다.');
				  	}
			  })
			  .catch(function(err){
			  })
			  
		  })
		  
 		 
	 }
	 
	 /**
	   * 쿠폰선물
	   */
	 $rootScope.comCouponSend = function(data, kind){
		 $rootScope.couponSendMemberList = data;					// 쿠폰 선물 대상자
		 $rootScope.couponSendMsg = '';								// 전달 메시지
		 $rootScope.couponSendMemKind = kind;						// 전달 메시지
		 $rootScope.couponSendCpCount = '';							// 선물 개수
		 $rootScope.busnLinkKey = '';								// 선물 대상 일련번호
		 
		 // 출연 요청 레이어 팝업 호출
		 $rootScope.viewPopup('sendCouponModal'); 
	 }
	 
     
	 /**
	   * 알림발송
	   */
	 $rootScope.comPushSend = function(data){
		 $rootScope.pushSendMemberList = data;					// 알림 대상자
		 $rootScope.pushSendMsg = '';							// 전달 메시지
		 
		// 출연 요청 레이어 팝업 호출
		 
	 }
	 
	 /**
	   * 알림발송(서비스호출)
	   */
	 $rootScope.comPushSendSvcCall = function(data){
		 
		if($rootScope.pushSendMemberList.length <= 0){
  			$message.alert('대상자 없습니다.');
  			return false;
  		} 
		
		if($rootScope.pushSendMsg == ''){
  			$message.alert('발송 메시지를 입력해주세요.');
  			return false;
  		} 
		 
		// 요청
		_.forEach($rootScope.pushSendMemberList ,function(obj, idx){
			 var param = {};
			 param.memberSeq = obj.MEMBER_SEQ;						//[필수] 회원일련번호
			 param.companySeq = obj.COMPANY_SEQ;					//[필수] 회사일련번호
			 param.msg = $rootScope.pushSendMsg;					//[필수] 전달 메시지
			   
			 $io.api('/api/comp/sendMemberPush.api', param)
			 .then(function(data){
				if(idx+1 == $rootScope.pushSendMemberList.length){
			  		$message.alert('정상적으로 발송 되었습니다.');
			  	}
			 })
			 .catch(function(err){
			 })
	  	})
		 
	 }
	 
	  
	  /**
	   * 출연요청
	   */
	 $rootScope.comRcutRequestSend = function(data){
		 
		 // 필요 변수 세팅
		 $rootScope.requestPgmList  =  [{v:'999',k:'선택'}];			// 진행중 프로그램 목록 ( 출연요청 )
		 $rootScope.requestSelPgmBaseSeq = '999';					// 선택한 프로그램 일련번호 ( 출연요청 )
		 $rootScope.requestRrcutList = [{v:'999',k:'선택'}];			// 모집 목록 ( 출연요청 )
		 $rootScope.requestSelRcutBaseSeq = '999';					// 선택한 모집 일련번호 ( 출연요청 )
		 $rootScope.requestSendMemberList = data;					// 출연요청 대상자
		 
	 	 // 회원 상태 확인
		 var findObj = _.where($rootScope.requestSendMemberList, function(obj){
			 return obj.MEMBER_STATE != 'M2';
		 })
		 
		 if(findObj){
			$message.alert('정지된 회원이 포함되어있습니다.');
  			return false;
		 }
		 
		  // 진행중 프로그램 조회
		  var param = {};
		  $io.api('/api/comp/selectPgmInfo.api', param)
		  .then(function(data){
			  	$rootScope.requestPgmList = data.user.resultData.pgmInfo;
		  	
			  	if($rootScope.requestPgmList.length <= 0){
		  			$message.alert('진행중인 프로그램이 없습니다.');
		  			return false;
		  		}
			  	
			  	_.forEach(data.user.resultData.pgmInfo,function(obj){
			  		$rootScope.requestPgmList.push({v:obj.PGM_BASE_SEQ, k:obj.PGM_NAME});
			  	})
			  
			  	// 출연 요청 레이어 팝업 호출
		  	
		  })
		  .catch(function(err){
		  })
		  
	 }
	 
	 /**
	   * 출연요청(서비스호출)
	   */
	 $rootScope.comRcutRequestSendSvcCall = function(){
		 
		if($rootScope.requestSendMemberList.length <= 0){
  			$message.alert('요청 대상자 없습니다.');
  			return false;
  		} 
		
		if($rootScope.requestSelRcutBaseSeq == '999'){
  			$message.alert('출연요청 모집을 선택해주세요.');
  			return false;
  		} 
		 
		// 요청
		_.forEach($rootScope.requestSendMemberList ,function(obj, idx){
			var param = {};
			param.memberSeq = obj.MEMBER_SEQ;						//[필수] 요청 회원 일련번호
			param.rcutBaseSeq = $rootScope.requestSelRcutBaseSeq;	//[필수] 요청 모집 일련번호
			  
			$io.api('/api/comp/requestMember.api', param)
			.then(function(data){
				if(idx+1 == $rootScope.requestSendMemberList.length){
			  		$message.alert('정상적으로 요청 되었습니다.');
			  	}
			})
			.catch(function(err){
			})
			
	  	})
		 
	 }
	 
	 // 프로그램 분야에 대한 모집 분야 코드 조회 
	 $rootScope.$watch('requestSelPgmBaseSeq', function(newVal, oldVal, scope) {
       
	   	  if(newVal == oldVal) return false;
	   	  if(newVal == '999') {
	   		  vm.requestSelRcutBaseSeq = '999';
	 		  vm.requestRrcutList = [{v:'999',k:'선택'}];
	   		  return false;
	   	  }
	   	  
	   	  var param = {};
			  param.pgmBaseSeq= newVal;								//[필수] 프로그램 일련번호
			  
		  $io.api('/api/comp/selectPgmHopeCast.api', param)
		  .then(function(data){
			  	$rootScope.requestRrcutList = [{v:'999',k:'선택'}];		
				$rootScope.requestSelRcutBaseSeq = '999';			
			  	_.forEach(data.user.resultData.rcutListInfo,function(obj){
			  		$rootScope.requestRrcutList.push({v:obj.RCUT_BASE_SEQ, k:'['+obj.PGM_REUT_FIELD+']['+obj.PGM_HOPE_CAST+']' + obj.PGM_NAME});
			  	})
		  })
		  .catch(function(err){
		  })
		  
     });
	 
	 
    /**
     * 에러 로그 저장
     */
    $rootScope.insertErrLog = function(reqParam){
    	try{
	    	var param = {};
	    	param.busnLinkKey = $rootScope.rootUSERINFO.userInfo[0].COMPANY_MEMBER_ID;		// [필수] 로그인 아이디
	    	param.busnLinkKeyType = 'B2';							// [필수][공코:BUSN_LINK_KEY_TYPE] 관련회원종류
	    	param.deviceModel = navigator.appName;					// [선택] 모델명
	    	param.deviceOsVer =  navigator.appVersion;				// [선택] OS버전정보
	    	param.svcUrl = reqParam.SEVICE_URL || 'NOURL';			// [필수] 호출 URL
	    	param.errMsgDesc = reqParam.ERR_MSG_DESC;				// [선택] 에러메시지
	    	param.pageName = location.href;							// [선택] 호출 페이지
	    	param.errType = '2';									// [선택] 에러 타입 [1: APP, 2: WEB]
	    	param.errMsg = reqParam.ERR_MSG;						// [필수] 에러 상세 내용
	    	param.svcParam = reqParam.SERVICE_PARAM;				// [필수] 호출 파라미터
	
	    	$io.api('/api/com/insertErrLog.login', param)
	    	.then(function(data){
	    	})
	    	.catch(function(err){
	    	})
    	}catch(e){}
    };
    
    /**
     * 페이지 이동
     */
    $rootScope.pageMove = function(url, param){
    	if(!_.isNull(param)){
    		window.localStorage.setItem("pageParam", JSON.stringify(param));
    	}
		window.location.href = url;
    }
    
    /**
     * 페이지 전달 받은 파라미터 세팅
     */
    $rootScope.getPageParam = function(){
    	var param =  window.localStorage.getItem("pageParam");
//    	window.localStorage.removeItem("pageParam");
    	try{
	    	if(!_.isNull(param)){
	    		param = JSON.parse(param);
	    	}
    	}catch(e){
    		param = {};
    	}
    	if(_.isEmpty(param)){
    		$message.alert('상세정보조회 오류입니다. 메인 페이지로 이동합니다.');
    		$rootScope.goHome();
    	}
    	return param;
		
    }
    
    /**
     * 뒤로가기로 왔을경우 Init에서 기본실행
     */
    $rootScope.backPageInit = function(pageName){
		  try{
			  var pgmListCtrlData = window.localStorage.getItem(pageName);
			  pgmListCtrlData = JSON.parse(pgmListCtrlData);
			  window.localStorage.removeItem(pageName);
			  
			  return pgmListCtrlData;
		  
		  }catch(e){
			  return false;
		  }
    }
    
    /**
     * 뒤로가기 파라미터 저장
     */
    $rootScope.backPageSaveData = function(pageName, data){
    	 window.localStorage.setItem(pageName, JSON.stringify(data));
    }
    
    /**
     * 뒤로가기 액션
     */
    $rootScope.backPage = function(pageName, url){
    	try{
    	var pgmListCtrlData = window.localStorage.getItem(pageName);
		pgmListCtrlData = JSON.parse(pgmListCtrlData);
		pgmListCtrlData.backFlag = true;
		  
		window.localStorage.setItem(pageName, JSON.stringify(pgmListCtrlData));
    	}catch(e){}
		window.location.href = url;
    }
    
    /**
     * 로그아웃
     */
    $rootScope.rootlogOut = function(){
    	var confrimResult = confirm('로그아웃 하시겠습니까?');
    	
    	if(confrimResult){
	      var param = {};
		  $io.api('/api/logOut.api', param)
		  .then(function(data){
			  $message.alert('로그아웃되었습니다.');
			  window.localStorage.removeItem('USERINFO');
			  window.location.href = '/web/login.dory';
		  })
		  .catch(function(err){
		  })
    	}
    };
    
    /**
     * 홈으로 이동
     */
    $rootScope.goHome = function(){
    	if($rootScope.rootUSERINFO.adminYn){
    		window.location.href = '/web/admin/mem/memList.dory';
    	}else{
    		window.location.href = '/web/comp/pgm/pgmList.dory';
    	}
    };
    
    /**
     * 공통코드 조회
     */
    $rootScope.selComCode = function(cd){
    	return $io.api('/api/com/selectCommonInfo.login', {commonInfoCode: cd})
        .then(function(res) {
        	return $rootScope.$eval('user.resultData.commonData', res);
        })
        .catch(function(rs) {
        })
    };
    
    
    
    /**
     * 대메뉴 클릭이벤트(상단)
     */
    $rootScope.rootbigMenuClick = function(menu,idx,clickFlagPass, linkFlag){
    	
    	var curMenu = _.find($rootScope.rootUSERINFO.menuInfo, {COMMON_INFO_VALUE6:location.pathname});
    	if(menu) curMenu = menu;
    	var curBigMenu;
    	if(curMenu){
    		curBigMenu = _.find($rootScope.rootUSERINFO.menuInfo, {COMMON_INFO_VALUE3 : 'L0', COMMON_INFO_VALUE1 :curMenu.COMMON_INFO_VALUE1.split("_")[0]});
    	}
    	if(!curBigMenu){
    		curBigMenu = $rootScope.rootbigMenu[idx];
    	}
    	
    	var midMenu = [];
    	_.forEach($rootScope.rootUSERINFO.menuInfo, function(obj){
    		if(obj.COMMON_INFO_VALUE1.startsWith(curBigMenu.COMMON_INFO_VALUE1+'_') && obj.COMMON_INFO_VALUE3 == 'L1'){
    			midMenu.push(obj);
    		}
    	})
    	
    	_.forEach(midMenu, function(obj){
    		obj.subMenu = [];
    		
    		_.forEach($rootScope.rootUSERINFO.menuInfo, function(obj1){
        		if(obj1.COMMON_INFO_VALUE1.startsWith(obj.COMMON_INFO_VALUE1+'_') && obj1.COMMON_INFO_VALUE3 == 'L2'){
        			obj.subMenu.push(obj1);
        		}
        	})
    		
    	})
    	
    	if(linkFlag == undefined) {
    		linkFlag = true;
    	}
    	
    	$rootScope.rootmidMenu = midMenu;
    	$rootScope.rootSelBigMenu = curBigMenu;
    	if(!clickFlagPass){
	    	if(!menu){
		    	if(curMenu.COMMON_INFO_VALUE3 == 'L2'){
		    		var parentMenu = _.find($rootScope.rootUSERINFO.menuInfo, {COMMON_INFO_VALUE3 : 'L1', COMMON_INFO_VALUE1 :curMenu.COMMON_INFO_VALUE1.split("_")[0] + '_' + curMenu.COMMON_INFO_VALUE1.split("_")[1]});
		    		$rootScope.rootmidMenuClick(linkFlag, curMenu, parentMenu);	
		    	}else{
		    		$rootScope.rootmidMenuClick(linkFlag, curMenu);
		    	}
	    	}else{
	    		if(midMenu[0].subMenu.length > 0){
	        		$rootScope.rootmidMenuClick(linkFlag, midMenu[0].subMenu[0],midMenu[0]);	
	        	}else{
	        		$rootScope.rootmidMenuClick(linkFlag, midMenu[0]);	
	        	}
	    	}
    	}
    	
    	
    	
    }
    
    /**
     * 중메뉴 클릭이벤트(옆)
     */
    $rootScope.rootmidMenuClick = function(clickFlag, menuObj,parentMenuObj){
    	var nav = '';
    	var linkUrl = '';
    	if(clickFlag){
	    	$rootScope.rootMenuNav = '';
			if(!_.isEmpty(parentMenuObj)){
				$rootScope.rootSelMidMenu = menuObj;
				nav += parentMenuObj.COMMON_INFO_VALUE2 + ' > ' + $rootScope.rootSelMidMenu.COMMON_INFO_VALUE2;
			}else{
				$rootScope.rootSelMidMenu = menuObj;
				nav += $rootScope.rootSelMidMenu.COMMON_INFO_VALUE2;
			}
	    	linkUrl = $rootScope.rootSelMidMenu.COMMON_INFO_VALUE6;
			
	    	// 메뉴 URL로 이동(본인페이지일때는 정지)
	    	if(location.pathname != linkUrl){
	    		window.localStorage.removeItem("pageParam");
	    		window.location.href = linkUrl;
	    	}
	    	
	    	// 큰타이틀
	    	$rootScope.contentTitle1 = $rootScope.rootSelMidMenu.COMMON_INFO_VALUE2;
	    	// 작은타이틀
	    	$rootScope.contentTitle2 = $rootScope.rootSelMidMenu.COMMON_INFO_VALUE5;
	    	// 네비게이터
	    	$rootScope.contentTitle3 = $rootScope.rootSelBigMenu.COMMON_INFO_VALUE2 + ' > ' + nav;
    	}
    }
    
   
    /**
     * 왼쪽 메뉴 이벤트
     */
    $rootScope.rootmidMenuEvent = function(){
    	// 메뉴 이벤트 
    	$("#nav").each(function() {
			$(".lnb > li > a").next("ul").show();
    	});
    }
    
    
    /**
     * 첫 이벤트 모음
     */
    // 유저정보 담기
	$rootScope.rootUSERINFO = JSON.parse(window.localStorage.getItem('USERINFO'));
    $rootScope.initProc = function(idx,clickPassFlag){
    	
    	$rootScope.$on('ngRepeatFinished', function (ngRepeatFinishedEvent) {
    		$rootScope.rootmidMenuEvent();
        });

    	// 대메뉴 생성
    	var rootbigMenu = _.where($rootScope.rootUSERINFO.menuInfo, {COMMON_INFO_VALUE3 : 'L0'});
    	$rootScope.rootbigMenu = rootbigMenu;
    	
    	// 첫 레프트 메뉴 생성
    	$rootScope.rootbigMenuClick(null,idx, clickPassFlag);
    	
    	// 등록 권한 확인 (M0_M1_M2 프로그램등록, M0_M2_M2 모집등록
    	$rootScope.pgmInsertAuth = false;
    	$rootScope.rcutInsertAuth = false;
    	var pgmAuth = _.find($rootScope.rootUSERINFO.menuInfo, {COMMON_INFO_VALUE1 : 'M0_M1_M2'});
    	var rcutAuth = _.find($rootScope.rootUSERINFO.menuInfo, {COMMON_INFO_VALUE1 : 'M0_M2_M2'});
    	if(pgmAuth) $rootScope.pgmInsertAuth = true;
    	if(rcutAuth) $rootScope.rcutInsertAuth = true;
    		
    	
    };
    
    
    
    $rootScope.makeParam = function(param){
    	var keySize = 128;
    	var iterationCount = 10000;
    	var aesUtil = new AesUtil(keySize, iterationCount);
    	var encrypt = aesUtil.encrypt($rootScope.salt, $rootScope.iv, $rootScope.passPhrase, param);
    	return encrypt;	 
    }; 
    
    
    $rootScope.iv = "00000000000000000000000000000000";
	$rootScope.salt = "00000000000000000000000000000000";
	$rootScope.passPhrase = "aesalgoisbestbes";
    
  })
  
  .factory('ActionTimer', function($log) {
    var logger = $log('ActionTimer');
    var actions = {};
    return {
      start: start,
      lap: lap,
      stop: stop
    };
    function start(name, message) {
      actions[name] = {
        start: Date.now(),
        laps: []
      };
      logger.info([name, '(start:0ms) ', message].join(''));
    }
    function lap(name, message) {
      var action = actions[name];
      if (!action) {
        logger.error(Error(name + ' is not registered.'));
        return;
      }
      var start = action.start;
      var laps = action.laps;
      var now = Date.now();
      laps.push(now);
      logger.info([name, '(lap', laps.length, ':', now - start,'ms) ', message].join(''));
    }
    function stop(name, message) {
      var action = actions[name];
      if (!action) {
        logger.error(Error(name + ' is not registered.'));
        return;
      }
      var start = action.start;
      var laps = action.laps;
      var now = Date.now();
      logger.info([name, '(stop:', now - start, 'ms) ', message].join(''));
      delete actions[name];
    }
  })
  
  // select 사용을 위한 디랙티브.
  .directive('metaOptions', function($compile, $parse, MetaSvc, $timeout, $log) {
    return {
      priority: 1001, // compiles first
      terminal: true, // prevent lower priority directives to compile after it
      replace : false,
      restrict : 'A',
      scope: false, // := {}
      compile: function(el, attr) {
        var metaCodeName = _.uniqueId('_mds'); // scope unique id 보장받음.
        var metaCode = el.attr('meta-options'); // copy metaCode
        var metaBind = el.attr('meta-options-bind'); // bind 에 사용될 parent model (filter 로 사용 됨) 명.
        var metaBindExists = _.has(attr, 'metaOptionsBind');
        var metaType = el.attr('meta-options-type'); // type 에 따라, [key] value / value
        var data = el.attr('meta-options-data') || null;
        var ignore = el.attr('meta-options-ignore') || null; // ignore code
        
        var sortOrder = el.attr('meta-options-sortorder') || 'a'; // [Asc | Desc]
        var sortKey = el.attr('meta-options-sortkey') || 'k'; // [Key | Value]

        el.removeAttr('meta-options'); // necessary to avoid infinite compile loop
        if(metaType) {
          el.attr('ng-options', "kv.k as ( (kv.k) ? ('[' + kv.k + '] ' + kv.v) : kv.v ) for kv in " + metaCodeName);
        } else {
          el.attr('ng-options', "kv.k as ( kv.v ) for kv in " + metaCodeName);
        }

        return function(scope, iel, attr, ctl) {
          var modelGetter = $parse(attr['ngModel']);

          var iofunc = function(newCode, oldCode) {
            var dval = _.isEmpty(modelGetter(scope)) ? modelGetter(scope) : undefined;
            if(metaBindExists) {
              if(_.isEmpty(newCode)) {
                scope[metaCodeName] = [{ k : dval, v : '상위조건을 선택하세요.' }]; // 조회중 표시
                modelGetter.assign(scope, undefined);
                return;
              }
            } else {
              scope[metaCodeName] = [{ k : dval, v : '조회중입니다.' }]; // 조회중 표시
            }

            // 데이터셋 준비하고~ // Service Call!!! - promise 모델
            MetaSvc.getBizCode(metaCode) //
            .then(function(kvo) {
              var kv = _.clone(kvo);
              if(_.startsWith(sortKey, 'v')) { kv = _.sortBy(kv, 'v'); }
              if(_.startsWith(sortOrder, 'd')) { kv = kv.reverse(); }

              // bind 여부 여기서 고려 (필터로 동작)
              if(metaBindExists) {
                kv = _.filter(kv, { s : newCode || undefined });
              }

              if(!_.isEmpty(data) && _.isString(data)) {
                _.chunk(data.split('$'), 2).reverse().forEach(function(d) {
                  kv.unshift({ k : d[0], v : d.length == 2 ? d[1] : d[0] });
                });
              }

              if(!_.isEmpty(ignore) && _.isString(ignore)) {
                var a = ignore.split('$');
                kv = _.remove(kv, function(d) { return !_.contains(a, d.k); });
              }

              // 바인딩 값이 없고, kv 값에 empty 가 없다면~~ 첫 번째 값으로 바인딩
              var sel = modelGetter(scope);

              if(!_.isEmpty(kv) && (_.isEmpty(sel) || !_.some(kv, { k : sel  || undefined }))) {
                modelGetter.assign(scope, kv[0].k);
              }

              // 바인딩.
              scope[metaCodeName] = kv;
              
              iel.removeAttr('meta-options');
              iel.empty();
              
              // option 중복 bug fix
              var newel = angular.element(iel[0].outerHTML);
              iel.after(newel);
              iel.remove();
              
              $compile(newel)(scope);
              
            })
            .catch(function(msg) {

              var kv = [{ v : msg }];

              // 바인딩 값이 없고, kv 값에 empty 가 없다면~~ 첫 번째 값으로 바인딩
              var sel = modelGetter(scope);
              if(_.isEmpty(sel)) {
                modelGetter.assign(scope, kv[0].k);
              }

              // 바인딩.
              scope[metaCodeName] = kv;
            })
            ;
          } // end iofunc


          // 상위코드 바인딩 여부에 따라서 watch 여부 결정.
          if(metaBindExists && !_.isEmpty(metaBind)) {
            scope.$watch(metaBind, function(n, o) { iofunc(n, o); });
          } else {
            iofunc();
          }

          $compile(iel)(scope);
        };
      }
    }
  })

  // Table row-span 을 위한 디랙티브 선언. (ng-repeat 걸리는 곳에서 row-span="[0,1]" 등으로 td 의 index 설정
  .directive('rowSpan',function($timeout, $parse) {
    return {
      restrict : 'A',
      compile : function(el, attr) {
        var incCol = _.has(attr, 'rowSpanCol');
        return function(scope, element, attrs) {
          if (scope.$last) {
            var modelGetter = $parse(attrs['rowSpan']);
            $timeout(function() {
              $(modelGetter(scope)).each(function(a, n) {
                $(element.parent()).rowspan(n);
              });
            }, 0);
          }
        };
      }
    };
  })
  // Table row-span 을 위한 디랙티브 선언. (ng-repeat 걸리는 곳에서 row-span="[0,1]" 등으로 td 의 index 설정
  .directive('colSpan', function($timeout, $parse) {
    return {
      restrict : 'A',
      compile : function(el, attr) {
        var incCol = _.has(attr, 'rowSpanCol');
        return function(scope, element, attrs) {
          if (scope.$last) {
            $timeout(function() {
              $('tr', element.parent()).each(function(n) {
                $(element.parent()).colspan(n);
              });
            }, 0);
          }
        };
      }
    };
  })

  .directive('ngIsolate', function() {
    return { restrict : 'A', scope: true };
  })
  .directive('maxlength', function($log, $message){
    return {
      restrict : 'A',
      link : function(scope, element, attrs) {
        var mlen = parseInt(attrs.maxlength || 0);
        if(_.isNaN(mlen) || mlen < 1) { return; }
        var nf = _.has(attrs, 'numberFormat');

        var $input = $(element);

        $input.bind('keydown', function(e) {
          $log.debug('keydown', e.keyCode, $input.val());
          var len = 0;
          if(!_.isEmpty(window.getSelection().toString())) { return; }
          if($input.val().length >= mlen && e.keyCode != 8 && e.keyCode != 9 && e.keyCode != 46 && e.keyCode != 13) {
            //if($input.val().length > mlen) { $input.val($input.val().substr(0, mlen)); $input.trigger('input'); }
            $input.blur();
            $message.alert('최대 입력 글자('+mlen+')를 초과했습니다.');
          }
        });
        $input.bind('change keyup', function(e) {
          $log.debug('keyup', e.keyCode, $input.val());
          var len = $input.val().length;
          if(len > mlen) {
            $input.val($input.val().slice(0, mlen)); $input.trigger('input');
          }
        });
      }
    };
  })
  .directive('inputAutoSelect', function() {
    return {
      restrict : 'A',
      link : function(scope, element, attrs) {
        var $input = $(element);
        function se() {
          $input.select();
          $input.unbind('click', se);
          $input.blur(function() {
            $input.bind('click', se);
          });
        }
        $input.bind('click', se);
      }
    };
  })
  
  .config(function($compileProvider, $httpProvider) {
    $httpProvider.useApplyAsync(true);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|javascript|file):/);
  });
 ;
