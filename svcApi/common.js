/*! jQuery v1.11.0 | (c) 2005, 2014 jQuery Foundation, Inc. | jquery.org/license */
!function(a,b){"object"==typeof module&&"object"==typeof module.exports?module.exports=a.document?b(a,!0):function(a){if(!a.document)throw new Error("jQuery requires a window with a document");return b(a)}:b(a)}("undefined"!=typeof window?window:this,function(a,b){var c=[],d=c.slice,e=c.concat,f=c.push,g=c.indexOf,h={},i=h.toString,j=h.hasOwnProperty,k="".trim,l={},m="1.11.0",n=function(a,b){return new n.fn.init(a,b)},o=/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,p=/^-ms-/,q=/-([\da-z])/gi,r=function(a,b){return b.toUpperCase()};n.fn=n.prototype={jquery:m,constructor:n,selector:"",length:0,toArray:function(){return d.call(this)},get:function(a){return null!=a?0>a?this[a+this.length]:this[a]:d.call(this)},pushStack:function(a){var b=n.merge(this.constructor(),a);return b.prevObject=this,b.context=this.context,b},each:function(a,b){return n.each(this,a,b)},map:function(a){return this.pushStack(n.map(this,function(b,c){return a.call(b,c,b)}))},slice:function(){return this.pushStack(d.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return this.eq(-1)},eq:function(a){var b=this.length,c=+a+(0>a?b:0);return this.pushStack(c>=0&&b>c?[this[c]]:[])},end:function(){return this.prevObject||this.constructor(null)},push:f,sort:c.sort,splice:c.splice},n.extend=n.fn.extend=function(){var a,b,c,d,e,f,g=arguments[0]||{},h=1,i=arguments.length,j=!1;for("boolean"==typeof g&&(j=g,g=arguments[h]||{},h++),"object"==typeof g||n.isFunction(g)||(g={}),h===i&&(g=this,h--);i>h;h++)if(null!=(e=arguments[h]))for(d in e)a=g[d],c=e[d],g!==c&&(j&&c&&(n.isPlainObject(c)||(b=n.isArray(c)))?(b?(b=!1,f=a&&n.isArray(a)?a:[]):f=a&&n.isPlainObject(a)?a:{},g[d]=n.extend(j,f,c)):void 0!==c&&(g[d]=c));return g},n.extend({expando:"jQuery"+(m+Math.random()).replace(/\D/g,""),isReady:!0,error:function(a){throw new Error(a)},noop:function(){},isFunction:function(a){return"function"===n.type(a)},isArray:Array.isArray||function(a){return"array"===n.type(a)},isWindow:function(a){return null!=a&&a==a.window},isNumeric:function(a){return a-parseFloat(a)>=0},isEmptyObject:function(a){var b;for(b in a)return!1;return!0},isPlainObject:function(a){var b;if(!a||"object"!==n.type(a)||a.nodeType||n.isWindow(a))return!1;try{if(a.constructor&&!j.call(a,"constructor")&&!j.call(a.constructor.prototype,"isPrototypeOf"))return!1}catch(c){return!1}if(l.ownLast)for(b in a)return j.call(a,b);for(b in a);return void 0===b||j.call(a,b)},type:function(a){return null==a?a+"":"object"==typeof a||"function"==typeof a?h[i.call(a)]||"object":typeof a},globalEval:function(b){b&&n.trim(b)&&(a.execScript||function(b){a.eval.call(a,b)})(b)},camelCase:function(a){return a.replace(p,"ms-").replace(q,r)},nodeName:function(a,b){return a.nodeName&&a.nodeName.toLowerCase()===b.toLowerCase()},each:function(a,b,c){var d,e=0,f=a.length,g=s(a);if(c){if(g){for(;f>e;e++)if(d=b.apply(a[e],c),d===!1)break}else for(e in a)if(d=b.apply(a[e],c),d===!1)break}else if(g){for(;f>e;e++)if(d=b.call(a[e],e,a[e]),d===!1)break}else for(e in a)if(d=b.call(a[e],e,a[e]),d===!1)break;return a},trim:k&&!k.call("\ufeff\xa0")?function(a){return null==a?"":k.call(a)}:function(a){return null==a?"":(a+"").replace(o,"")},makeArray:function(a,b){var c=b||[];return null!=a&&(s(Object(a))?n.merge(c,"string"==typeof a?[a]:a):f.call(c,a)),c},inArray:function(a,b,c){var d;if(b){if(g)return g.call(b,a,c);for(d=b.length,c=c?0>c?Math.max(0,d+c):c:0;d>c;c++)if(c in b&&b[c]===a)return c}return-1},merge:function(a,b){var c=+b.length,d=0,e=a.length;while(c>d)a[e++]=b[d++];if(c!==c)while(void 0!==b[d])a[e++]=b[d++];return a.length=e,a},grep:function(a,b,c){for(var d,e=[],f=0,g=a.length,h=!c;g>f;f++)d=!b(a[f],f),d!==h&&e.push(a[f]);return e},map:function(a,b,c){var d,f=0,g=a.length,h=s(a),i=[];if(h)for(;g>f;f++)d=b(a[f],f,c),null!=d&&i.push(d);else for(f in a)d=b(a[f],f,c),null!=d&&i.push(d);return e.apply([],i)},guid:1,proxy:function(a,b){var c,e,f;return"string"==typeof b&&(f=a[b],b=a,a=f),n.isFunction(a)?(c=d.call(arguments,2),e=function(){return a.apply(b||this,c.concat(d.call(arguments)))},e.guid=a.guid=a.guid||n.guid++,e):void 0},now:function(){return+new Date},support:l}),n.each("Boolean Number String Function Array Date RegExp Object Error".split(" "),function(a,b){h["[object "+b+"]"]=b.toLowerCase()});function s(a){var b=a.length,c=n.type(a);return"function"===c||n.isWindow(a)?!1:1===a.nodeType&&b?!0:"array"===c||0===b||"number"==typeof b&&b>0&&b-1 in a}var t=function(a){var b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s="sizzle"+-new Date,t=a.document,u=0,v=0,w=eb(),x=eb(),y=eb(),z=function(a,b){return a===b&&(j=!0),0},A="undefined",B=1<<31,C={}.hasOwnProperty,D=[],E=D.pop,F=D.push,G=D.push,H=D.slice,I=D.indexOf||function(a){for(var b=0,c=this.length;c>b;b++)if(this[b]===a)return b;return-1},J="checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",K="[\\x20\\t\\r\\n\\f]",L="(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",M=L.replace("w","w#"),N="\\["+K+"*("+L+")"+K+"*(?:([*^$|!~]?=)"+K+"*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|("+M+")|)|)"+K+"*\\]",O=":("+L+")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|"+N.replace(3,8)+")*)|.*)\\)|)",P=new RegExp("^"+K+"+|((?:^|[^\\\\])(?:\\\\.)*)"+K+"+$","g"),Q=new RegExp("^"+K+"*,"+K+"*"),R=new RegExp("^"+K+"*([>+~]|"+K+")"+K+"*"),S=new RegExp("="+K+"*([^\\]'\"]*?)"+K+"*\\]","g"),T=new RegExp(O),U=new RegExp("^"+M+"$"),V={ID:new RegExp("^#("+L+")"),CLASS:new RegExp("^\\.("+L+")"),TAG:new RegExp("^("+L.replace("w","w*")+")"),ATTR:new RegExp("^"+N),PSEUDO:new RegExp("^"+O),CHILD:new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\("+K+"*(even|odd|(([+-]|)(\\d*)n|)"+K+"*(?:([+-]|)"+K+"*(\\d+)|))"+K+"*\\)|)","i"),bool:new RegExp("^(?:"+J+")$","i"),needsContext:new RegExp("^"+K+"*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\("+K+"*((?:-\\d)?\\d*)"+K+"*\\)|)(?=[^-]|$)","i")},W=/^(?:input|select|textarea|button)$/i,X=/^h\d$/i,Y=/^[^{]+\{\s*\[native \w/,Z=/^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,$=/[+~]/,_=/'|\\/g,ab=new RegExp("\\\\([\\da-f]{1,6}"+K+"?|("+K+")|.)","ig"),bb=function(a,b,c){var d="0x"+b-65536;return d!==d||c?b:0>d?String.fromCharCode(d+65536):String.fromCharCode(d>>10|55296,1023&d|56320)};try{G.apply(D=H.call(t.childNodes),t.childNodes),D[t.childNodes.length].nodeType}catch(cb){G={apply:D.length?function(a,b){F.apply(a,H.call(b))}:function(a,b){var c=a.length,d=0;while(a[c++]=b[d++]);a.length=c-1}}}function db(a,b,d,e){var f,g,h,i,j,m,p,q,u,v;if((b?b.ownerDocument||b:t)!==l&&k(b),b=b||l,d=d||[],!a||"string"!=typeof a)return d;if(1!==(i=b.nodeType)&&9!==i)return[];if(n&&!e){if(f=Z.exec(a))if(h=f[1]){if(9===i){if(g=b.getElementById(h),!g||!g.parentNode)return d;if(g.id===h)return d.push(g),d}else if(b.ownerDocument&&(g=b.ownerDocument.getElementById(h))&&r(b,g)&&g.id===h)return d.push(g),d}else{if(f[2])return G.apply(d,b.getElementsByTagName(a)),d;if((h=f[3])&&c.getElementsByClassName&&b.getElementsByClassName)return G.apply(d,b.getElementsByClassName(h)),d}if(c.qsa&&(!o||!o.test(a))){if(q=p=s,u=b,v=9===i&&a,1===i&&"object"!==b.nodeName.toLowerCase()){m=ob(a),(p=b.getAttribute("id"))?q=p.replace(_,"\\$&"):b.setAttribute("id",q),q="[id='"+q+"'] ",j=m.length;while(j--)m[j]=q+pb(m[j]);u=$.test(a)&&mb(b.parentNode)||b,v=m.join(",")}if(v)try{return G.apply(d,u.querySelectorAll(v)),d}catch(w){}finally{p||b.removeAttribute("id")}}}return xb(a.replace(P,"$1"),b,d,e)}function eb(){var a=[];function b(c,e){return a.push(c+" ")>d.cacheLength&&delete b[a.shift()],b[c+" "]=e}return b}function fb(a){return a[s]=!0,a}function gb(a){var b=l.createElement("div");try{return!!a(b)}catch(c){return!1}finally{b.parentNode&&b.parentNode.removeChild(b),b=null}}function hb(a,b){var c=a.split("|"),e=a.length;while(e--)d.attrHandle[c[e]]=b}function ib(a,b){var c=b&&a,d=c&&1===a.nodeType&&1===b.nodeType&&(~b.sourceIndex||B)-(~a.sourceIndex||B);if(d)return d;if(c)while(c=c.nextSibling)if(c===b)return-1;return a?1:-1}function jb(a){return function(b){var c=b.nodeName.toLowerCase();return"input"===c&&b.type===a}}function kb(a){return function(b){var c=b.nodeName.toLowerCase();return("input"===c||"button"===c)&&b.type===a}}function lb(a){return fb(function(b){return b=+b,fb(function(c,d){var e,f=a([],c.length,b),g=f.length;while(g--)c[e=f[g]]&&(c[e]=!(d[e]=c[e]))})})}function mb(a){return a&&typeof a.getElementsByTagName!==A&&a}c=db.support={},f=db.isXML=function(a){var b=a&&(a.ownerDocument||a).documentElement;return b?"HTML"!==b.nodeName:!1},k=db.setDocument=function(a){var b,e=a?a.ownerDocument||a:t,g=e.defaultView;return e!==l&&9===e.nodeType&&e.documentElement?(l=e,m=e.documentElement,n=!f(e),g&&g!==g.top&&(g.addEventListener?g.addEventListener("unload",function(){k()},!1):g.attachEvent&&g.attachEvent("onunload",function(){k()})),c.attributes=gb(function(a){return a.className="i",!a.getAttribute("className")}),c.getElementsByTagName=gb(function(a){return a.appendChild(e.createComment("")),!a.getElementsByTagName("*").length}),c.getElementsByClassName=Y.test(e.getElementsByClassName)&&gb(function(a){return a.innerHTML="<div class='a'></div><div class='a i'></div>",a.firstChild.className="i",2===a.getElementsByClassName("i").length}),c.getById=gb(function(a){return m.appendChild(a).id=s,!e.getElementsByName||!e.getElementsByName(s).length}),c.getById?(d.find.ID=function(a,b){if(typeof b.getElementById!==A&&n){var c=b.getElementById(a);return c&&c.parentNode?[c]:[]}},d.filter.ID=function(a){var b=a.replace(ab,bb);return function(a){return a.getAttribute("id")===b}}):(delete d.find.ID,d.filter.ID=function(a){var b=a.replace(ab,bb);return function(a){var c=typeof a.getAttributeNode!==A&&a.getAttributeNode("id");return c&&c.value===b}}),d.find.TAG=c.getElementsByTagName?function(a,b){return typeof b.getElementsByTagName!==A?b.getElementsByTagName(a):void 0}:function(a,b){var c,d=[],e=0,f=b.getElementsByTagName(a);if("*"===a){while(c=f[e++])1===c.nodeType&&d.push(c);return d}return f},d.find.CLASS=c.getElementsByClassName&&function(a,b){return typeof b.getElementsByClassName!==A&&n?b.getElementsByClassName(a):void 0},p=[],o=[],(c.qsa=Y.test(e.querySelectorAll))&&(gb(function(a){a.innerHTML="<select t=''><option selected=''></option></select>",a.querySelectorAll("[t^='']").length&&o.push("[*^$]="+K+"*(?:''|\"\")"),a.querySelectorAll("[selected]").length||o.push("\\["+K+"*(?:value|"+J+")"),a.querySelectorAll(":checked").length||o.push(":checked")}),gb(function(a){var b=e.createElement("input");b.setAttribute("type","hidden"),a.appendChild(b).setAttribute("name","D"),a.querySelectorAll("[name=d]").length&&o.push("name"+K+"*[*^$|!~]?="),a.querySelectorAll(":enabled").length||o.push(":enabled",":disabled"),a.querySelectorAll("*,:x"),o.push(",.*:")})),(c.matchesSelector=Y.test(q=m.webkitMatchesSelector||m.mozMatchesSelector||m.oMatchesSelector||m.msMatchesSelector))&&gb(function(a){c.disconnectedMatch=q.call(a,"div"),q.call(a,"[s!='']:x"),p.push("!=",O)}),o=o.length&&new RegExp(o.join("|")),p=p.length&&new RegExp(p.join("|")),b=Y.test(m.compareDocumentPosition),r=b||Y.test(m.contains)?function(a,b){var c=9===a.nodeType?a.documentElement:a,d=b&&b.parentNode;return a===d||!(!d||1!==d.nodeType||!(c.contains?c.contains(d):a.compareDocumentPosition&&16&a.compareDocumentPosition(d)))}:function(a,b){if(b)while(b=b.parentNode)if(b===a)return!0;return!1},z=b?function(a,b){if(a===b)return j=!0,0;var d=!a.compareDocumentPosition-!b.compareDocumentPosition;return d?d:(d=(a.ownerDocument||a)===(b.ownerDocument||b)?a.compareDocumentPosition(b):1,1&d||!c.sortDetached&&b.compareDocumentPosition(a)===d?a===e||a.ownerDocument===t&&r(t,a)?-1:b===e||b.ownerDocument===t&&r(t,b)?1:i?I.call(i,a)-I.call(i,b):0:4&d?-1:1)}:function(a,b){if(a===b)return j=!0,0;var c,d=0,f=a.parentNode,g=b.parentNode,h=[a],k=[b];if(!f||!g)return a===e?-1:b===e?1:f?-1:g?1:i?I.call(i,a)-I.call(i,b):0;if(f===g)return ib(a,b);c=a;while(c=c.parentNode)h.unshift(c);c=b;while(c=c.parentNode)k.unshift(c);while(h[d]===k[d])d++;return d?ib(h[d],k[d]):h[d]===t?-1:k[d]===t?1:0},e):l},db.matches=function(a,b){return db(a,null,null,b)},db.matchesSelector=function(a,b){if((a.ownerDocument||a)!==l&&k(a),b=b.replace(S,"='$1']"),!(!c.matchesSelector||!n||p&&p.test(b)||o&&o.test(b)))try{var d=q.call(a,b);if(d||c.disconnectedMatch||a.document&&11!==a.document.nodeType)return d}catch(e){}return db(b,l,null,[a]).length>0},db.contains=function(a,b){return(a.ownerDocument||a)!==l&&k(a),r(a,b)},db.attr=function(a,b){(a.ownerDocument||a)!==l&&k(a);var e=d.attrHandle[b.toLowerCase()],f=e&&C.call(d.attrHandle,b.toLowerCase())?e(a,b,!n):void 0;return void 0!==f?f:c.attributes||!n?a.getAttribute(b):(f=a.getAttributeNode(b))&&f.specified?f.value:null},db.error=function(a){throw new Error("Syntax error, unrecognized expression: "+a)},db.uniqueSort=function(a){var b,d=[],e=0,f=0;if(j=!c.detectDuplicates,i=!c.sortStable&&a.slice(0),a.sort(z),j){while(b=a[f++])b===a[f]&&(e=d.push(f));while(e--)a.splice(d[e],1)}return i=null,a},e=db.getText=function(a){var b,c="",d=0,f=a.nodeType;if(f){if(1===f||9===f||11===f){if("string"==typeof a.textContent)return a.textContent;for(a=a.firstChild;a;a=a.nextSibling)c+=e(a)}else if(3===f||4===f)return a.nodeValue}else while(b=a[d++])c+=e(b);return c},d=db.selectors={cacheLength:50,createPseudo:fb,match:V,attrHandle:{},find:{},relative:{">":{dir:"parentNode",first:!0}," ":{dir:"parentNode"},"+":{dir:"previousSibling",first:!0},"~":{dir:"previousSibling"}},preFilter:{ATTR:function(a){return a[1]=a[1].replace(ab,bb),a[3]=(a[4]||a[5]||"").replace(ab,bb),"~="===a[2]&&(a[3]=" "+a[3]+" "),a.slice(0,4)},CHILD:function(a){return a[1]=a[1].toLowerCase(),"nth"===a[1].slice(0,3)?(a[3]||db.error(a[0]),a[4]=+(a[4]?a[5]+(a[6]||1):2*("even"===a[3]||"odd"===a[3])),a[5]=+(a[7]+a[8]||"odd"===a[3])):a[3]&&db.error(a[0]),a},PSEUDO:function(a){var b,c=!a[5]&&a[2];return V.CHILD.test(a[0])?null:(a[3]&&void 0!==a[4]?a[2]=a[4]:c&&T.test(c)&&(b=ob(c,!0))&&(b=c.indexOf(")",c.length-b)-c.length)&&(a[0]=a[0].slice(0,b),a[2]=c.slice(0,b)),a.slice(0,3))}},filter:{TAG:function(a){var b=a.replace(ab,bb).toLowerCase();return"*"===a?function(){return!0}:function(a){return a.nodeName&&a.nodeName.toLowerCase()===b}},CLASS:function(a){var b=w[a+" "];return b||(b=new RegExp("(^|"+K+")"+a+"("+K+"|$)"))&&w(a,function(a){return b.test("string"==typeof a.className&&a.className||typeof a.getAttribute!==A&&a.getAttribute("class")||"")})},ATTR:function(a,b,c){return function(d){var e=db.attr(d,a);return null==e?"!="===b:b?(e+="","="===b?e===c:"!="===b?e!==c:"^="===b?c&&0===e.indexOf(c):"*="===b?c&&e.indexOf(c)>-1:"$="===b?c&&e.slice(-c.length)===c:"~="===b?(" "+e+" ").indexOf(c)>-1:"|="===b?e===c||e.slice(0,c.length+1)===c+"-":!1):!0}},CHILD:function(a,b,c,d,e){var f="nth"!==a.slice(0,3),g="last"!==a.slice(-4),h="of-type"===b;return 1===d&&0===e?function(a){return!!a.parentNode}:function(b,c,i){var j,k,l,m,n,o,p=f!==g?"nextSibling":"previousSibling",q=b.parentNode,r=h&&b.nodeName.toLowerCase(),t=!i&&!h;if(q){if(f){while(p){l=b;while(l=l[p])if(h?l.nodeName.toLowerCase()===r:1===l.nodeType)return!1;o=p="only"===a&&!o&&"nextSibling"}return!0}if(o=[g?q.firstChild:q.lastChild],g&&t){k=q[s]||(q[s]={}),j=k[a]||[],n=j[0]===u&&j[1],m=j[0]===u&&j[2],l=n&&q.childNodes[n];while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if(1===l.nodeType&&++m&&l===b){k[a]=[u,n,m];break}}else if(t&&(j=(b[s]||(b[s]={}))[a])&&j[0]===u)m=j[1];else while(l=++n&&l&&l[p]||(m=n=0)||o.pop())if((h?l.nodeName.toLowerCase()===r:1===l.nodeType)&&++m&&(t&&((l[s]||(l[s]={}))[a]=[u,m]),l===b))break;return m-=e,m===d||m%d===0&&m/d>=0}}},PSEUDO:function(a,b){var c,e=d.pseudos[a]||d.setFilters[a.toLowerCase()]||db.error("unsupported pseudo: "+a);return e[s]?e(b):e.length>1?(c=[a,a,"",b],d.setFilters.hasOwnProperty(a.toLowerCase())?fb(function(a,c){var d,f=e(a,b),g=f.length;while(g--)d=I.call(a,f[g]),a[d]=!(c[d]=f[g])}):function(a){return e(a,0,c)}):e}},pseudos:{not:fb(function(a){var b=[],c=[],d=g(a.replace(P,"$1"));return d[s]?fb(function(a,b,c,e){var f,g=d(a,null,e,[]),h=a.length;while(h--)(f=g[h])&&(a[h]=!(b[h]=f))}):function(a,e,f){return b[0]=a,d(b,null,f,c),!c.pop()}}),has:fb(function(a){return function(b){return db(a,b).length>0}}),contains:fb(function(a){return function(b){return(b.textContent||b.innerText||e(b)).indexOf(a)>-1}}),lang:fb(function(a){return U.test(a||"")||db.error("unsupported lang: "+a),a=a.replace(ab,bb).toLowerCase(),function(b){var c;do if(c=n?b.lang:b.getAttribute("xml:lang")||b.getAttribute("lang"))return c=c.toLowerCase(),c===a||0===c.indexOf(a+"-");while((b=b.parentNode)&&1===b.nodeType);return!1}}),target:function(b){var c=a.location&&a.location.hash;return c&&c.slice(1)===b.id},root:function(a){return a===m},focus:function(a){return a===l.activeElement&&(!l.hasFocus||l.hasFocus())&&!!(a.type||a.href||~a.tabIndex)},enabled:function(a){return a.disabled===!1},disabled:function(a){return a.disabled===!0},checked:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&!!a.checked||"option"===b&&!!a.selected},selected:function(a){return a.parentNode&&a.parentNode.selectedIndex,a.selected===!0},empty:function(a){for(a=a.firstChild;a;a=a.nextSibling)if(a.nodeType<6)return!1;return!0},parent:function(a){return!d.pseudos.empty(a)},header:function(a){return X.test(a.nodeName)},input:function(a){return W.test(a.nodeName)},button:function(a){var b=a.nodeName.toLowerCase();return"input"===b&&"button"===a.type||"button"===b},text:function(a){var b;return"input"===a.nodeName.toLowerCase()&&"text"===a.type&&(null==(b=a.getAttribute("type"))||"text"===b.toLowerCase())},first:lb(function(){return[0]}),last:lb(function(a,b){return[b-1]}),eq:lb(function(a,b,c){return[0>c?c+b:c]}),even:lb(function(a,b){for(var c=0;b>c;c+=2)a.push(c);return a}),odd:lb(function(a,b){for(var c=1;b>c;c+=2)a.push(c);return a}),lt:lb(function(a,b,c){for(var d=0>c?c+b:c;--d>=0;)a.push(d);return a}),gt:lb(function(a,b,c){for(var d=0>c?c+b:c;++d<b;)a.push(d);return a})}},d.pseudos.nth=d.pseudos.eq;for(b in{radio:!0,checkbox:!0,file:!0,password:!0,image:!0})d.pseudos[b]=jb(b);for(b in{submit:!0,reset:!0})d.pseudos[b]=kb(b);function nb(){}nb.prototype=d.filters=d.pseudos,d.setFilters=new nb;function ob(a,b){var c,e,f,g,h,i,j,k=x[a+" "];if(k)return b?0:k.slice(0);h=a,i=[],j=d.preFilter;while(h){(!c||(e=Q.exec(h)))&&(e&&(h=h.slice(e[0].length)||h),i.push(f=[])),c=!1,(e=R.exec(h))&&(c=e.shift(),f.push({value:c,type:e[0].replace(P," ")}),h=h.slice(c.length));for(g in d.filter)!(e=V[g].exec(h))||j[g]&&!(e=j[g](e))||(c=e.shift(),f.push({value:c,type:g,matches:e}),h=h.slice(c.length));if(!c)break}return b?h.length:h?db.error(a):x(a,i).slice(0)}function pb(a){for(var b=0,c=a.length,d="";c>b;b++)d+=a[b].value;return d}function qb(a,b,c){var d=b.dir,e=c&&"parentNode"===d,f=v++;return b.first?function(b,c,f){while(b=b[d])if(1===b.nodeType||e)return a(b,c,f)}:function(b,c,g){var h,i,j=[u,f];if(g){while(b=b[d])if((1===b.nodeType||e)&&a(b,c,g))return!0}else while(b=b[d])if(1===b.nodeType||e){if(i=b[s]||(b[s]={}),(h=i[d])&&h[0]===u&&h[1]===f)return j[2]=h[2];if(i[d]=j,j[2]=a(b,c,g))return!0}}}function rb(a){return a.length>1?function(b,c,d){var e=a.length;while(e--)if(!a[e](b,c,d))return!1;return!0}:a[0]}function sb(a,b,c,d,e){for(var f,g=[],h=0,i=a.length,j=null!=b;i>h;h++)(f=a[h])&&(!c||c(f,d,e))&&(g.push(f),j&&b.push(h));return g}function tb(a,b,c,d,e,f){return d&&!d[s]&&(d=tb(d)),e&&!e[s]&&(e=tb(e,f)),fb(function(f,g,h,i){var j,k,l,m=[],n=[],o=g.length,p=f||wb(b||"*",h.nodeType?[h]:h,[]),q=!a||!f&&b?p:sb(p,m,a,h,i),r=c?e||(f?a:o||d)?[]:g:q;if(c&&c(q,r,h,i),d){j=sb(r,n),d(j,[],h,i),k=j.length;while(k--)(l=j[k])&&(r[n[k]]=!(q[n[k]]=l))}if(f){if(e||a){if(e){j=[],k=r.length;while(k--)(l=r[k])&&j.push(q[k]=l);e(null,r=[],j,i)}k=r.length;while(k--)(l=r[k])&&(j=e?I.call(f,l):m[k])>-1&&(f[j]=!(g[j]=l))}}else r=sb(r===g?r.splice(o,r.length):r),e?e(null,g,r,i):G.apply(g,r)})}function ub(a){for(var b,c,e,f=a.length,g=d.relative[a[0].type],i=g||d.relative[" "],j=g?1:0,k=qb(function(a){return a===b},i,!0),l=qb(function(a){return I.call(b,a)>-1},i,!0),m=[function(a,c,d){return!g&&(d||c!==h)||((b=c).nodeType?k(a,c,d):l(a,c,d))}];f>j;j++)if(c=d.relative[a[j].type])m=[qb(rb(m),c)];else{if(c=d.filter[a[j].type].apply(null,a[j].matches),c[s]){for(e=++j;f>e;e++)if(d.relative[a[e].type])break;return tb(j>1&&rb(m),j>1&&pb(a.slice(0,j-1).concat({value:" "===a[j-2].type?"*":""})).replace(P,"$1"),c,e>j&&ub(a.slice(j,e)),f>e&&ub(a=a.slice(e)),f>e&&pb(a))}m.push(c)}return rb(m)}function vb(a,b){var c=b.length>0,e=a.length>0,f=function(f,g,i,j,k){var m,n,o,p=0,q="0",r=f&&[],s=[],t=h,v=f||e&&d.find.TAG("*",k),w=u+=null==t?1:Math.random()||.1,x=v.length;for(k&&(h=g!==l&&g);q!==x&&null!=(m=v[q]);q++){if(e&&m){n=0;while(o=a[n++])if(o(m,g,i)){j.push(m);break}k&&(u=w)}c&&((m=!o&&m)&&p--,f&&r.push(m))}if(p+=q,c&&q!==p){n=0;while(o=b[n++])o(r,s,g,i);if(f){if(p>0)while(q--)r[q]||s[q]||(s[q]=E.call(j));s=sb(s)}G.apply(j,s),k&&!f&&s.length>0&&p+b.length>1&&db.uniqueSort(j)}return k&&(u=w,h=t),r};return c?fb(f):f}g=db.compile=function(a,b){var c,d=[],e=[],f=y[a+" "];if(!f){b||(b=ob(a)),c=b.length;while(c--)f=ub(b[c]),f[s]?d.push(f):e.push(f);f=y(a,vb(e,d))}return f};function wb(a,b,c){for(var d=0,e=b.length;e>d;d++)db(a,b[d],c);return c}function xb(a,b,e,f){var h,i,j,k,l,m=ob(a);if(!f&&1===m.length){if(i=m[0]=m[0].slice(0),i.length>2&&"ID"===(j=i[0]).type&&c.getById&&9===b.nodeType&&n&&d.relative[i[1].type]){if(b=(d.find.ID(j.matches[0].replace(ab,bb),b)||[])[0],!b)return e;a=a.slice(i.shift().value.length)}h=V.needsContext.test(a)?0:i.length;while(h--){if(j=i[h],d.relative[k=j.type])break;if((l=d.find[k])&&(f=l(j.matches[0].replace(ab,bb),$.test(i[0].type)&&mb(b.parentNode)||b))){if(i.splice(h,1),a=f.length&&pb(i),!a)return G.apply(e,f),e;break}}}return g(a,m)(f,b,!n,e,$.test(a)&&mb(b.parentNode)||b),e}return c.sortStable=s.split("").sort(z).join("")===s,c.detectDuplicates=!!j,k(),c.sortDetached=gb(function(a){return 1&a.compareDocumentPosition(l.createElement("div"))}),gb(function(a){return a.innerHTML="<a href='#'></a>","#"===a.firstChild.getAttribute("href")})||hb("type|href|height|width",function(a,b,c){return c?void 0:a.getAttribute(b,"type"===b.toLowerCase()?1:2)}),c.attributes&&gb(function(a){return a.innerHTML="<input/>",a.firstChild.setAttribute("value",""),""===a.firstChild.getAttribute("value")})||hb("value",function(a,b,c){return c||"input"!==a.nodeName.toLowerCase()?void 0:a.defaultValue}),gb(function(a){return null==a.getAttribute("disabled")})||hb(J,function(a,b,c){var d;return c?void 0:a[b]===!0?b.toLowerCase():(d=a.getAttributeNode(b))&&d.specified?d.value:null}),db}(a);n.find=t,n.expr=t.selectors,n.expr[":"]=n.expr.pseudos,n.unique=t.uniqueSort,n.text=t.getText,n.isXMLDoc=t.isXML,n.contains=t.contains;var u=n.expr.match.needsContext,v=/^<(\w+)\s*\/?>(?:<\/\1>|)$/,w=/^.[^:#\[\.,]*$/;function x(a,b,c){if(n.isFunction(b))return n.grep(a,function(a,d){return!!b.call(a,d,a)!==c});if(b.nodeType)return n.grep(a,function(a){return a===b!==c});if("string"==typeof b){if(w.test(b))return n.filter(b,a,c);b=n.filter(b,a)}return n.grep(a,function(a){return n.inArray(a,b)>=0!==c})}n.filter=function(a,b,c){var d=b[0];return c&&(a=":not("+a+")"),1===b.length&&1===d.nodeType?n.find.matchesSelector(d,a)?[d]:[]:n.find.matches(a,n.grep(b,function(a){return 1===a.nodeType}))},n.fn.extend({find:function(a){var b,c=[],d=this,e=d.length;if("string"!=typeof a)return this.pushStack(n(a).filter(function(){for(b=0;e>b;b++)if(n.contains(d[b],this))return!0}));for(b=0;e>b;b++)n.find(a,d[b],c);return c=this.pushStack(e>1?n.unique(c):c),c.selector=this.selector?this.selector+" "+a:a,c},filter:function(a){return this.pushStack(x(this,a||[],!1))},not:function(a){return this.pushStack(x(this,a||[],!0))},is:function(a){return!!x(this,"string"==typeof a&&u.test(a)?n(a):a||[],!1).length}});var y,z=a.document,A=/^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,B=n.fn.init=function(a,b){var c,d;if(!a)return this;if("string"==typeof a){if(c="<"===a.charAt(0)&&">"===a.charAt(a.length-1)&&a.length>=3?[null,a,null]:A.exec(a),!c||!c[1]&&b)return!b||b.jquery?(b||y).find(a):this.constructor(b).find(a);if(c[1]){if(b=b instanceof n?b[0]:b,n.merge(this,n.parseHTML(c[1],b&&b.nodeType?b.ownerDocument||b:z,!0)),v.test(c[1])&&n.isPlainObject(b))for(c in b)n.isFunction(this[c])?this[c](b[c]):this.attr(c,b[c]);return this}if(d=z.getElementById(c[2]),d&&d.parentNode){if(d.id!==c[2])return y.find(a);this.length=1,this[0]=d}return this.context=z,this.selector=a,this}return a.nodeType?(this.context=this[0]=a,this.length=1,this):n.isFunction(a)?"undefined"!=typeof y.ready?y.ready(a):a(n):(void 0!==a.selector&&(this.selector=a.selector,this.context=a.context),n.makeArray(a,this))};B.prototype=n.fn,y=n(z);var C=/^(?:parents|prev(?:Until|All))/,D={children:!0,contents:!0,next:!0,prev:!0};n.extend({dir:function(a,b,c){var d=[],e=a[b];while(e&&9!==e.nodeType&&(void 0===c||1!==e.nodeType||!n(e).is(c)))1===e.nodeType&&d.push(e),e=e[b];return d},sibling:function(a,b){for(var c=[];a;a=a.nextSibling)1===a.nodeType&&a!==b&&c.push(a);return c}}),n.fn.extend({has:function(a){var b,c=n(a,this),d=c.length;return this.filter(function(){for(b=0;d>b;b++)if(n.contains(this,c[b]))return!0})},closest:function(a,b){for(var c,d=0,e=this.length,f=[],g=u.test(a)||"string"!=typeof a?n(a,b||this.context):0;e>d;d++)for(c=this[d];c&&c!==b;c=c.parentNode)if(c.nodeType<11&&(g?g.index(c)>-1:1===c.nodeType&&n.find.matchesSelector(c,a))){f.push(c);break}return this.pushStack(f.length>1?n.unique(f):f)},index:function(a){return a?"string"==typeof a?n.inArray(this[0],n(a)):n.inArray(a.jquery?a[0]:a,this):this[0]&&this[0].parentNode?this.first().prevAll().length:-1},add:function(a,b){return this.pushStack(n.unique(n.merge(this.get(),n(a,b))))},addBack:function(a){return this.add(null==a?this.prevObject:this.prevObject.filter(a))}});function E(a,b){do a=a[b];while(a&&1!==a.nodeType);return a}n.each({parent:function(a){var b=a.parentNode;return b&&11!==b.nodeType?b:null},parents:function(a){return n.dir(a,"parentNode")},parentsUntil:function(a,b,c){return n.dir(a,"parentNode",c)},next:function(a){return E(a,"nextSibling")},prev:function(a){return E(a,"previousSibling")},nextAll:function(a){return n.dir(a,"nextSibling")},prevAll:function(a){return n.dir(a,"previousSibling")},nextUntil:function(a,b,c){return n.dir(a,"nextSibling",c)},prevUntil:function(a,b,c){return n.dir(a,"previousSibling",c)},siblings:function(a){return n.sibling((a.parentNode||{}).firstChild,a)},children:function(a){return n.sibling(a.firstChild)},contents:function(a){return n.nodeName(a,"iframe")?a.contentDocument||a.contentWindow.document:n.merge([],a.childNodes)}},function(a,b){n.fn[a]=function(c,d){var e=n.map(this,b,c);return"Until"!==a.slice(-5)&&(d=c),d&&"string"==typeof d&&(e=n.filter(d,e)),this.length>1&&(D[a]||(e=n.unique(e)),C.test(a)&&(e=e.reverse())),this.pushStack(e)}});var F=/\S+/g,G={};function H(a){var b=G[a]={};return n.each(a.match(F)||[],function(a,c){b[c]=!0}),b}n.Callbacks=function(a){a="string"==typeof a?G[a]||H(a):n.extend({},a);var b,c,d,e,f,g,h=[],i=!a.once&&[],j=function(l){for(c=a.memory&&l,d=!0,f=g||0,g=0,e=h.length,b=!0;h&&e>f;f++)if(h[f].apply(l[0],l[1])===!1&&a.stopOnFalse){c=!1;break}b=!1,h&&(i?i.length&&j(i.shift()):c?h=[]:k.disable())},k={add:function(){if(h){var d=h.length;!function f(b){n.each(b,function(b,c){var d=n.type(c);"function"===d?a.unique&&k.has(c)||h.push(c):c&&c.length&&"string"!==d&&f(c)})}(arguments),b?e=h.length:c&&(g=d,j(c))}return this},remove:function(){return h&&n.each(arguments,function(a,c){var d;while((d=n.inArray(c,h,d))>-1)h.splice(d,1),b&&(e>=d&&e--,f>=d&&f--)}),this},has:function(a){return a?n.inArray(a,h)>-1:!(!h||!h.length)},empty:function(){return h=[],e=0,this},disable:function(){return h=i=c=void 0,this},disabled:function(){return!h},lock:function(){return i=void 0,c||k.disable(),this},locked:function(){return!i},fireWith:function(a,c){return!h||d&&!i||(c=c||[],c=[a,c.slice?c.slice():c],b?i.push(c):j(c)),this},fire:function(){return k.fireWith(this,arguments),this},fired:function(){return!!d}};return k},n.extend({Deferred:function(a){var b=[["resolve","done",n.Callbacks("once memory"),"resolved"],["reject","fail",n.Callbacks("once memory"),"rejected"],["notify","progress",n.Callbacks("memory")]],c="pending",d={state:function(){return c},always:function(){return e.done(arguments).fail(arguments),this},then:function(){var a=arguments;return n.Deferred(function(c){n.each(b,function(b,f){var g=n.isFunction(a[b])&&a[b];e[f[1]](function(){var a=g&&g.apply(this,arguments);a&&n.isFunction(a.promise)?a.promise().done(c.resolve).fail(c.reject).progress(c.notify):c[f[0]+"With"](this===d?c.promise():this,g?[a]:arguments)})}),a=null}).promise()},promise:function(a){return null!=a?n.extend(a,d):d}},e={};return d.pipe=d.then,n.each(b,function(a,f){var g=f[2],h=f[3];d[f[1]]=g.add,h&&g.add(function(){c=h},b[1^a][2].disable,b[2][2].lock),e[f[0]]=function(){return e[f[0]+"With"](this===e?d:this,arguments),this},e[f[0]+"With"]=g.fireWith}),d.promise(e),a&&a.call(e,e),e},when:function(a){var b=0,c=d.call(arguments),e=c.length,f=1!==e||a&&n.isFunction(a.promise)?e:0,g=1===f?a:n.Deferred(),h=function(a,b,c){return function(e){b[a]=this,c[a]=arguments.length>1?d.call(arguments):e,c===i?g.notifyWith(b,c):--f||g.resolveWith(b,c)}},i,j,k;if(e>1)for(i=new Array(e),j=new Array(e),k=new Array(e);e>b;b++)c[b]&&n.isFunction(c[b].promise)?c[b].promise().done(h(b,k,c)).fail(g.reject).progress(h(b,j,i)):--f;return f||g.resolveWith(k,c),g.promise()}});var I;n.fn.ready=function(a){return n.ready.promise().done(a),this},n.extend({isReady:!1,readyWait:1,holdReady:function(a){a?n.readyWait++:n.ready(!0)},ready:function(a){if(a===!0?!--n.readyWait:!n.isReady){if(!z.body)return setTimeout(n.ready);n.isReady=!0,a!==!0&&--n.readyWait>0||(I.resolveWith(z,[n]),n.fn.trigger&&n(z).trigger("ready").off("ready"))}}});function J(){z.addEventListener?(z.removeEventListener("DOMContentLoaded",K,!1),a.removeEventListener("load",K,!1)):(z.detachEvent("onreadystatechange",K),a.detachEvent("onload",K))}function K(){(z.addEventListener||"load"===event.type||"complete"===z.readyState)&&(J(),n.ready())}n.ready.promise=function(b){if(!I)if(I=n.Deferred(),"complete"===z.readyState)setTimeout(n.ready);else if(z.addEventListener)z.addEventListener("DOMContentLoaded",K,!1),a.addEventListener("load",K,!1);else{z.attachEvent("onreadystatechange",K),a.attachEvent("onload",K);var c=!1;try{c=null==a.frameElement&&z.documentElement}catch(d){}c&&c.doScroll&&!function e(){if(!n.isReady){try{c.doScroll("left")}catch(a){return setTimeout(e,50)}J(),n.ready()}}()}return I.promise(b)};var L="undefined",M;for(M in n(l))break;l.ownLast="0"!==M,l.inlineBlockNeedsLayout=!1,n(function(){var a,b,c=z.getElementsByTagName("body")[0];c&&(a=z.createElement("div"),a.style.cssText="border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px",b=z.createElement("div"),c.appendChild(a).appendChild(b),typeof b.style.zoom!==L&&(b.style.cssText="border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1",(l.inlineBlockNeedsLayout=3===b.offsetWidth)&&(c.style.zoom=1)),c.removeChild(a),a=b=null)}),function(){var a=z.createElement("div");if(null==l.deleteExpando){l.deleteExpando=!0;try{delete a.test}catch(b){l.deleteExpando=!1}}a=null}(),n.acceptData=function(a){var b=n.noData[(a.nodeName+" ").toLowerCase()],c=+a.nodeType||1;return 1!==c&&9!==c?!1:!b||b!==!0&&a.getAttribute("classid")===b};var N=/^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,O=/([A-Z])/g;function P(a,b,c){if(void 0===c&&1===a.nodeType){var d="data-"+b.replace(O,"-$1").toLowerCase();if(c=a.getAttribute(d),"string"==typeof c){try{c="true"===c?!0:"false"===c?!1:"null"===c?null:+c+""===c?+c:N.test(c)?n.parseJSON(c):c}catch(e){}n.data(a,b,c)}else c=void 0}return c}function Q(a){var b;for(b in a)if(("data"!==b||!n.isEmptyObject(a[b]))&&"toJSON"!==b)return!1;return!0}function R(a,b,d,e){if(n.acceptData(a)){var f,g,h=n.expando,i=a.nodeType,j=i?n.cache:a,k=i?a[h]:a[h]&&h;if(k&&j[k]&&(e||j[k].data)||void 0!==d||"string"!=typeof b)return k||(k=i?a[h]=c.pop()||n.guid++:h),j[k]||(j[k]=i?{}:{toJSON:n.noop}),("object"==typeof b||"function"==typeof b)&&(e?j[k]=n.extend(j[k],b):j[k].data=n.extend(j[k].data,b)),g=j[k],e||(g.data||(g.data={}),g=g.data),void 0!==d&&(g[n.camelCase(b)]=d),"string"==typeof b?(f=g[b],null==f&&(f=g[n.camelCase(b)])):f=g,f
}}function S(a,b,c){if(n.acceptData(a)){var d,e,f=a.nodeType,g=f?n.cache:a,h=f?a[n.expando]:n.expando;if(g[h]){if(b&&(d=c?g[h]:g[h].data)){n.isArray(b)?b=b.concat(n.map(b,n.camelCase)):b in d?b=[b]:(b=n.camelCase(b),b=b in d?[b]:b.split(" ")),e=b.length;while(e--)delete d[b[e]];if(c?!Q(d):!n.isEmptyObject(d))return}(c||(delete g[h].data,Q(g[h])))&&(f?n.cleanData([a],!0):l.deleteExpando||g!=g.window?delete g[h]:g[h]=null)}}}n.extend({cache:{},noData:{"applet ":!0,"embed ":!0,"object ":"clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"},hasData:function(a){return a=a.nodeType?n.cache[a[n.expando]]:a[n.expando],!!a&&!Q(a)},data:function(a,b,c){return R(a,b,c)},removeData:function(a,b){return S(a,b)},_data:function(a,b,c){return R(a,b,c,!0)},_removeData:function(a,b){return S(a,b,!0)}}),n.fn.extend({data:function(a,b){var c,d,e,f=this[0],g=f&&f.attributes;if(void 0===a){if(this.length&&(e=n.data(f),1===f.nodeType&&!n._data(f,"parsedAttrs"))){c=g.length;while(c--)d=g[c].name,0===d.indexOf("data-")&&(d=n.camelCase(d.slice(5)),P(f,d,e[d]));n._data(f,"parsedAttrs",!0)}return e}return"object"==typeof a?this.each(function(){n.data(this,a)}):arguments.length>1?this.each(function(){n.data(this,a,b)}):f?P(f,a,n.data(f,a)):void 0},removeData:function(a){return this.each(function(){n.removeData(this,a)})}}),n.extend({queue:function(a,b,c){var d;return a?(b=(b||"fx")+"queue",d=n._data(a,b),c&&(!d||n.isArray(c)?d=n._data(a,b,n.makeArray(c)):d.push(c)),d||[]):void 0},dequeue:function(a,b){b=b||"fx";var c=n.queue(a,b),d=c.length,e=c.shift(),f=n._queueHooks(a,b),g=function(){n.dequeue(a,b)};"inprogress"===e&&(e=c.shift(),d--),e&&("fx"===b&&c.unshift("inprogress"),delete f.stop,e.call(a,g,f)),!d&&f&&f.empty.fire()},_queueHooks:function(a,b){var c=b+"queueHooks";return n._data(a,c)||n._data(a,c,{empty:n.Callbacks("once memory").add(function(){n._removeData(a,b+"queue"),n._removeData(a,c)})})}}),n.fn.extend({queue:function(a,b){var c=2;return"string"!=typeof a&&(b=a,a="fx",c--),arguments.length<c?n.queue(this[0],a):void 0===b?this:this.each(function(){var c=n.queue(this,a,b);n._queueHooks(this,a),"fx"===a&&"inprogress"!==c[0]&&n.dequeue(this,a)})},dequeue:function(a){return this.each(function(){n.dequeue(this,a)})},clearQueue:function(a){return this.queue(a||"fx",[])},promise:function(a,b){var c,d=1,e=n.Deferred(),f=this,g=this.length,h=function(){--d||e.resolveWith(f,[f])};"string"!=typeof a&&(b=a,a=void 0),a=a||"fx";while(g--)c=n._data(f[g],a+"queueHooks"),c&&c.empty&&(d++,c.empty.add(h));return h(),e.promise(b)}});var T=/[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,U=["Top","Right","Bottom","Left"],V=function(a,b){return a=b||a,"none"===n.css(a,"display")||!n.contains(a.ownerDocument,a)},W=n.access=function(a,b,c,d,e,f,g){var h=0,i=a.length,j=null==c;if("object"===n.type(c)){e=!0;for(h in c)n.access(a,b,h,c[h],!0,f,g)}else if(void 0!==d&&(e=!0,n.isFunction(d)||(g=!0),j&&(g?(b.call(a,d),b=null):(j=b,b=function(a,b,c){return j.call(n(a),c)})),b))for(;i>h;h++)b(a[h],c,g?d:d.call(a[h],h,b(a[h],c)));return e?a:j?b.call(a):i?b(a[0],c):f},X=/^(?:checkbox|radio)$/i;!function(){var a=z.createDocumentFragment(),b=z.createElement("div"),c=z.createElement("input");if(b.setAttribute("className","t"),b.innerHTML="  <link/><table></table><a href='/a'>a</a>",l.leadingWhitespace=3===b.firstChild.nodeType,l.tbody=!b.getElementsByTagName("tbody").length,l.htmlSerialize=!!b.getElementsByTagName("link").length,l.html5Clone="<:nav></:nav>"!==z.createElement("nav").cloneNode(!0).outerHTML,c.type="checkbox",c.checked=!0,a.appendChild(c),l.appendChecked=c.checked,b.innerHTML="<textarea>x</textarea>",l.noCloneChecked=!!b.cloneNode(!0).lastChild.defaultValue,a.appendChild(b),b.innerHTML="<input type='radio' checked='checked' name='t'/>",l.checkClone=b.cloneNode(!0).cloneNode(!0).lastChild.checked,l.noCloneEvent=!0,b.attachEvent&&(b.attachEvent("onclick",function(){l.noCloneEvent=!1}),b.cloneNode(!0).click()),null==l.deleteExpando){l.deleteExpando=!0;try{delete b.test}catch(d){l.deleteExpando=!1}}a=b=c=null}(),function(){var b,c,d=z.createElement("div");for(b in{submit:!0,change:!0,focusin:!0})c="on"+b,(l[b+"Bubbles"]=c in a)||(d.setAttribute(c,"t"),l[b+"Bubbles"]=d.attributes[c].expando===!1);d=null}();var Y=/^(?:input|select|textarea)$/i,Z=/^key/,$=/^(?:mouse|contextmenu)|click/,_=/^(?:focusinfocus|focusoutblur)$/,ab=/^([^.]*)(?:\.(.+)|)$/;function bb(){return!0}function cb(){return!1}function db(){try{return z.activeElement}catch(a){}}n.event={global:{},add:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n._data(a);if(r){c.handler&&(i=c,c=i.handler,e=i.selector),c.guid||(c.guid=n.guid++),(g=r.events)||(g=r.events={}),(k=r.handle)||(k=r.handle=function(a){return typeof n===L||a&&n.event.triggered===a.type?void 0:n.event.dispatch.apply(k.elem,arguments)},k.elem=a),b=(b||"").match(F)||[""],h=b.length;while(h--)f=ab.exec(b[h])||[],o=q=f[1],p=(f[2]||"").split(".").sort(),o&&(j=n.event.special[o]||{},o=(e?j.delegateType:j.bindType)||o,j=n.event.special[o]||{},l=n.extend({type:o,origType:q,data:d,handler:c,guid:c.guid,selector:e,needsContext:e&&n.expr.match.needsContext.test(e),namespace:p.join(".")},i),(m=g[o])||(m=g[o]=[],m.delegateCount=0,j.setup&&j.setup.call(a,d,p,k)!==!1||(a.addEventListener?a.addEventListener(o,k,!1):a.attachEvent&&a.attachEvent("on"+o,k))),j.add&&(j.add.call(a,l),l.handler.guid||(l.handler.guid=c.guid)),e?m.splice(m.delegateCount++,0,l):m.push(l),n.event.global[o]=!0);a=null}},remove:function(a,b,c,d,e){var f,g,h,i,j,k,l,m,o,p,q,r=n.hasData(a)&&n._data(a);if(r&&(k=r.events)){b=(b||"").match(F)||[""],j=b.length;while(j--)if(h=ab.exec(b[j])||[],o=q=h[1],p=(h[2]||"").split(".").sort(),o){l=n.event.special[o]||{},o=(d?l.delegateType:l.bindType)||o,m=k[o]||[],h=h[2]&&new RegExp("(^|\\.)"+p.join("\\.(?:.*\\.|)")+"(\\.|$)"),i=f=m.length;while(f--)g=m[f],!e&&q!==g.origType||c&&c.guid!==g.guid||h&&!h.test(g.namespace)||d&&d!==g.selector&&("**"!==d||!g.selector)||(m.splice(f,1),g.selector&&m.delegateCount--,l.remove&&l.remove.call(a,g));i&&!m.length&&(l.teardown&&l.teardown.call(a,p,r.handle)!==!1||n.removeEvent(a,o,r.handle),delete k[o])}else for(o in k)n.event.remove(a,o+b[j],c,d,!0);n.isEmptyObject(k)&&(delete r.handle,n._removeData(a,"events"))}},trigger:function(b,c,d,e){var f,g,h,i,k,l,m,o=[d||z],p=j.call(b,"type")?b.type:b,q=j.call(b,"namespace")?b.namespace.split("."):[];if(h=l=d=d||z,3!==d.nodeType&&8!==d.nodeType&&!_.test(p+n.event.triggered)&&(p.indexOf(".")>=0&&(q=p.split("."),p=q.shift(),q.sort()),g=p.indexOf(":")<0&&"on"+p,b=b[n.expando]?b:new n.Event(p,"object"==typeof b&&b),b.isTrigger=e?2:3,b.namespace=q.join("."),b.namespace_re=b.namespace?new RegExp("(^|\\.)"+q.join("\\.(?:.*\\.|)")+"(\\.|$)"):null,b.result=void 0,b.target||(b.target=d),c=null==c?[b]:n.makeArray(c,[b]),k=n.event.special[p]||{},e||!k.trigger||k.trigger.apply(d,c)!==!1)){if(!e&&!k.noBubble&&!n.isWindow(d)){for(i=k.delegateType||p,_.test(i+p)||(h=h.parentNode);h;h=h.parentNode)o.push(h),l=h;l===(d.ownerDocument||z)&&o.push(l.defaultView||l.parentWindow||a)}m=0;while((h=o[m++])&&!b.isPropagationStopped())b.type=m>1?i:k.bindType||p,f=(n._data(h,"events")||{})[b.type]&&n._data(h,"handle"),f&&f.apply(h,c),f=g&&h[g],f&&f.apply&&n.acceptData(h)&&(b.result=f.apply(h,c),b.result===!1&&b.preventDefault());if(b.type=p,!e&&!b.isDefaultPrevented()&&(!k._default||k._default.apply(o.pop(),c)===!1)&&n.acceptData(d)&&g&&d[p]&&!n.isWindow(d)){l=d[g],l&&(d[g]=null),n.event.triggered=p;try{d[p]()}catch(r){}n.event.triggered=void 0,l&&(d[g]=l)}return b.result}},dispatch:function(a){a=n.event.fix(a);var b,c,e,f,g,h=[],i=d.call(arguments),j=(n._data(this,"events")||{})[a.type]||[],k=n.event.special[a.type]||{};if(i[0]=a,a.delegateTarget=this,!k.preDispatch||k.preDispatch.call(this,a)!==!1){h=n.event.handlers.call(this,a,j),b=0;while((f=h[b++])&&!a.isPropagationStopped()){a.currentTarget=f.elem,g=0;while((e=f.handlers[g++])&&!a.isImmediatePropagationStopped())(!a.namespace_re||a.namespace_re.test(e.namespace))&&(a.handleObj=e,a.data=e.data,c=((n.event.special[e.origType]||{}).handle||e.handler).apply(f.elem,i),void 0!==c&&(a.result=c)===!1&&(a.preventDefault(),a.stopPropagation()))}return k.postDispatch&&k.postDispatch.call(this,a),a.result}},handlers:function(a,b){var c,d,e,f,g=[],h=b.delegateCount,i=a.target;if(h&&i.nodeType&&(!a.button||"click"!==a.type))for(;i!=this;i=i.parentNode||this)if(1===i.nodeType&&(i.disabled!==!0||"click"!==a.type)){for(e=[],f=0;h>f;f++)d=b[f],c=d.selector+" ",void 0===e[c]&&(e[c]=d.needsContext?n(c,this).index(i)>=0:n.find(c,this,null,[i]).length),e[c]&&e.push(d);e.length&&g.push({elem:i,handlers:e})}return h<b.length&&g.push({elem:this,handlers:b.slice(h)}),g},fix:function(a){if(a[n.expando])return a;var b,c,d,e=a.type,f=a,g=this.fixHooks[e];g||(this.fixHooks[e]=g=$.test(e)?this.mouseHooks:Z.test(e)?this.keyHooks:{}),d=g.props?this.props.concat(g.props):this.props,a=new n.Event(f),b=d.length;while(b--)c=d[b],a[c]=f[c];return a.target||(a.target=f.srcElement||z),3===a.target.nodeType&&(a.target=a.target.parentNode),a.metaKey=!!a.metaKey,g.filter?g.filter(a,f):a},props:"altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),fixHooks:{},keyHooks:{props:"char charCode key keyCode".split(" "),filter:function(a,b){return null==a.which&&(a.which=null!=b.charCode?b.charCode:b.keyCode),a}},mouseHooks:{props:"button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),filter:function(a,b){var c,d,e,f=b.button,g=b.fromElement;return null==a.pageX&&null!=b.clientX&&(d=a.target.ownerDocument||z,e=d.documentElement,c=d.body,a.pageX=b.clientX+(e&&e.scrollLeft||c&&c.scrollLeft||0)-(e&&e.clientLeft||c&&c.clientLeft||0),a.pageY=b.clientY+(e&&e.scrollTop||c&&c.scrollTop||0)-(e&&e.clientTop||c&&c.clientTop||0)),!a.relatedTarget&&g&&(a.relatedTarget=g===a.target?b.toElement:g),a.which||void 0===f||(a.which=1&f?1:2&f?3:4&f?2:0),a}},special:{load:{noBubble:!0},focus:{trigger:function(){if(this!==db()&&this.focus)try{return this.focus(),!1}catch(a){}},delegateType:"focusin"},blur:{trigger:function(){return this===db()&&this.blur?(this.blur(),!1):void 0},delegateType:"focusout"},click:{trigger:function(){return n.nodeName(this,"input")&&"checkbox"===this.type&&this.click?(this.click(),!1):void 0},_default:function(a){return n.nodeName(a.target,"a")}},beforeunload:{postDispatch:function(a){void 0!==a.result&&(a.originalEvent.returnValue=a.result)}}},simulate:function(a,b,c,d){var e=n.extend(new n.Event,c,{type:a,isSimulated:!0,originalEvent:{}});d?n.event.trigger(e,null,b):n.event.dispatch.call(b,e),e.isDefaultPrevented()&&c.preventDefault()}},n.removeEvent=z.removeEventListener?function(a,b,c){a.removeEventListener&&a.removeEventListener(b,c,!1)}:function(a,b,c){var d="on"+b;a.detachEvent&&(typeof a[d]===L&&(a[d]=null),a.detachEvent(d,c))},n.Event=function(a,b){return this instanceof n.Event?(a&&a.type?(this.originalEvent=a,this.type=a.type,this.isDefaultPrevented=a.defaultPrevented||void 0===a.defaultPrevented&&(a.returnValue===!1||a.getPreventDefault&&a.getPreventDefault())?bb:cb):this.type=a,b&&n.extend(this,b),this.timeStamp=a&&a.timeStamp||n.now(),void(this[n.expando]=!0)):new n.Event(a,b)},n.Event.prototype={isDefaultPrevented:cb,isPropagationStopped:cb,isImmediatePropagationStopped:cb,preventDefault:function(){var a=this.originalEvent;this.isDefaultPrevented=bb,a&&(a.preventDefault?a.preventDefault():a.returnValue=!1)},stopPropagation:function(){var a=this.originalEvent;this.isPropagationStopped=bb,a&&(a.stopPropagation&&a.stopPropagation(),a.cancelBubble=!0)},stopImmediatePropagation:function(){this.isImmediatePropagationStopped=bb,this.stopPropagation()}},n.each({mouseenter:"mouseover",mouseleave:"mouseout"},function(a,b){n.event.special[a]={delegateType:b,bindType:b,handle:function(a){var c,d=this,e=a.relatedTarget,f=a.handleObj;return(!e||e!==d&&!n.contains(d,e))&&(a.type=f.origType,c=f.handler.apply(this,arguments),a.type=b),c}}}),l.submitBubbles||(n.event.special.submit={setup:function(){return n.nodeName(this,"form")?!1:void n.event.add(this,"click._submit keypress._submit",function(a){var b=a.target,c=n.nodeName(b,"input")||n.nodeName(b,"button")?b.form:void 0;c&&!n._data(c,"submitBubbles")&&(n.event.add(c,"submit._submit",function(a){a._submit_bubble=!0}),n._data(c,"submitBubbles",!0))})},postDispatch:function(a){a._submit_bubble&&(delete a._submit_bubble,this.parentNode&&!a.isTrigger&&n.event.simulate("submit",this.parentNode,a,!0))},teardown:function(){return n.nodeName(this,"form")?!1:void n.event.remove(this,"._submit")}}),l.changeBubbles||(n.event.special.change={setup:function(){return Y.test(this.nodeName)?(("checkbox"===this.type||"radio"===this.type)&&(n.event.add(this,"propertychange._change",function(a){"checked"===a.originalEvent.propertyName&&(this._just_changed=!0)}),n.event.add(this,"click._change",function(a){this._just_changed&&!a.isTrigger&&(this._just_changed=!1),n.event.simulate("change",this,a,!0)})),!1):void n.event.add(this,"beforeactivate._change",function(a){var b=a.target;Y.test(b.nodeName)&&!n._data(b,"changeBubbles")&&(n.event.add(b,"change._change",function(a){!this.parentNode||a.isSimulated||a.isTrigger||n.event.simulate("change",this.parentNode,a,!0)}),n._data(b,"changeBubbles",!0))})},handle:function(a){var b=a.target;return this!==b||a.isSimulated||a.isTrigger||"radio"!==b.type&&"checkbox"!==b.type?a.handleObj.handler.apply(this,arguments):void 0},teardown:function(){return n.event.remove(this,"._change"),!Y.test(this.nodeName)}}),l.focusinBubbles||n.each({focus:"focusin",blur:"focusout"},function(a,b){var c=function(a){n.event.simulate(b,a.target,n.event.fix(a),!0)};n.event.special[b]={setup:function(){var d=this.ownerDocument||this,e=n._data(d,b);e||d.addEventListener(a,c,!0),n._data(d,b,(e||0)+1)},teardown:function(){var d=this.ownerDocument||this,e=n._data(d,b)-1;e?n._data(d,b,e):(d.removeEventListener(a,c,!0),n._removeData(d,b))}}}),n.fn.extend({on:function(a,b,c,d,e){var f,g;if("object"==typeof a){"string"!=typeof b&&(c=c||b,b=void 0);for(f in a)this.on(f,b,c,a[f],e);return this}if(null==c&&null==d?(d=b,c=b=void 0):null==d&&("string"==typeof b?(d=c,c=void 0):(d=c,c=b,b=void 0)),d===!1)d=cb;else if(!d)return this;return 1===e&&(g=d,d=function(a){return n().off(a),g.apply(this,arguments)},d.guid=g.guid||(g.guid=n.guid++)),this.each(function(){n.event.add(this,a,d,c,b)})},one:function(a,b,c,d){return this.on(a,b,c,d,1)},off:function(a,b,c){var d,e;if(a&&a.preventDefault&&a.handleObj)return d=a.handleObj,n(a.delegateTarget).off(d.namespace?d.origType+"."+d.namespace:d.origType,d.selector,d.handler),this;if("object"==typeof a){for(e in a)this.off(e,b,a[e]);return this}return(b===!1||"function"==typeof b)&&(c=b,b=void 0),c===!1&&(c=cb),this.each(function(){n.event.remove(this,a,c,b)})},trigger:function(a,b){return this.each(function(){n.event.trigger(a,b,this)})},triggerHandler:function(a,b){var c=this[0];return c?n.event.trigger(a,b,c,!0):void 0}});function eb(a){var b=fb.split("|"),c=a.createDocumentFragment();if(c.createElement)while(b.length)c.createElement(b.pop());return c}var fb="abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",gb=/ jQuery\d+="(?:null|\d+)"/g,hb=new RegExp("<(?:"+fb+")[\\s/>]","i"),ib=/^\s+/,jb=/<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,kb=/<([\w:]+)/,lb=/<tbody/i,mb=/<|&#?\w+;/,nb=/<(?:script|style|link)/i,ob=/checked\s*(?:[^=]|=\s*.checked.)/i,pb=/^$|\/(?:java|ecma)script/i,qb=/^true\/(.*)/,rb=/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,sb={option:[1,"<select multiple='multiple'>","</select>"],legend:[1,"<fieldset>","</fieldset>"],area:[1,"<map>","</map>"],param:[1,"<object>","</object>"],thead:[1,"<table>","</table>"],tr:[2,"<table><tbody>","</tbody></table>"],col:[2,"<table><tbody></tbody><colgroup>","</colgroup></table>"],td:[3,"<table><tbody><tr>","</tr></tbody></table>"],_default:l.htmlSerialize?[0,"",""]:[1,"X<div>","</div>"]},tb=eb(z),ub=tb.appendChild(z.createElement("div"));sb.optgroup=sb.option,sb.tbody=sb.tfoot=sb.colgroup=sb.caption=sb.thead,sb.th=sb.td;function vb(a,b){var c,d,e=0,f=typeof a.getElementsByTagName!==L?a.getElementsByTagName(b||"*"):typeof a.querySelectorAll!==L?a.querySelectorAll(b||"*"):void 0;if(!f)for(f=[],c=a.childNodes||a;null!=(d=c[e]);e++)!b||n.nodeName(d,b)?f.push(d):n.merge(f,vb(d,b));return void 0===b||b&&n.nodeName(a,b)?n.merge([a],f):f}function wb(a){X.test(a.type)&&(a.defaultChecked=a.checked)}function xb(a,b){return n.nodeName(a,"table")&&n.nodeName(11!==b.nodeType?b:b.firstChild,"tr")?a.getElementsByTagName("tbody")[0]||a.appendChild(a.ownerDocument.createElement("tbody")):a}function yb(a){return a.type=(null!==n.find.attr(a,"type"))+"/"+a.type,a}function zb(a){var b=qb.exec(a.type);return b?a.type=b[1]:a.removeAttribute("type"),a}function Ab(a,b){for(var c,d=0;null!=(c=a[d]);d++)n._data(c,"globalEval",!b||n._data(b[d],"globalEval"))}function Bb(a,b){if(1===b.nodeType&&n.hasData(a)){var c,d,e,f=n._data(a),g=n._data(b,f),h=f.events;if(h){delete g.handle,g.events={};for(c in h)for(d=0,e=h[c].length;e>d;d++)n.event.add(b,c,h[c][d])}g.data&&(g.data=n.extend({},g.data))}}function Cb(a,b){var c,d,e;if(1===b.nodeType){if(c=b.nodeName.toLowerCase(),!l.noCloneEvent&&b[n.expando]){e=n._data(b);for(d in e.events)n.removeEvent(b,d,e.handle);b.removeAttribute(n.expando)}"script"===c&&b.text!==a.text?(yb(b).text=a.text,zb(b)):"object"===c?(b.parentNode&&(b.outerHTML=a.outerHTML),l.html5Clone&&a.innerHTML&&!n.trim(b.innerHTML)&&(b.innerHTML=a.innerHTML)):"input"===c&&X.test(a.type)?(b.defaultChecked=b.checked=a.checked,b.value!==a.value&&(b.value=a.value)):"option"===c?b.defaultSelected=b.selected=a.defaultSelected:("input"===c||"textarea"===c)&&(b.defaultValue=a.defaultValue)}}n.extend({clone:function(a,b,c){var d,e,f,g,h,i=n.contains(a.ownerDocument,a);if(l.html5Clone||n.isXMLDoc(a)||!hb.test("<"+a.nodeName+">")?f=a.cloneNode(!0):(ub.innerHTML=a.outerHTML,ub.removeChild(f=ub.firstChild)),!(l.noCloneEvent&&l.noCloneChecked||1!==a.nodeType&&11!==a.nodeType||n.isXMLDoc(a)))for(d=vb(f),h=vb(a),g=0;null!=(e=h[g]);++g)d[g]&&Cb(e,d[g]);if(b)if(c)for(h=h||vb(a),d=d||vb(f),g=0;null!=(e=h[g]);g++)Bb(e,d[g]);else Bb(a,f);return d=vb(f,"script"),d.length>0&&Ab(d,!i&&vb(a,"script")),d=h=e=null,f},buildFragment:function(a,b,c,d){for(var e,f,g,h,i,j,k,m=a.length,o=eb(b),p=[],q=0;m>q;q++)if(f=a[q],f||0===f)if("object"===n.type(f))n.merge(p,f.nodeType?[f]:f);else if(mb.test(f)){h=h||o.appendChild(b.createElement("div")),i=(kb.exec(f)||["",""])[1].toLowerCase(),k=sb[i]||sb._default,h.innerHTML=k[1]+f.replace(jb,"<$1></$2>")+k[2],e=k[0];while(e--)h=h.lastChild;if(!l.leadingWhitespace&&ib.test(f)&&p.push(b.createTextNode(ib.exec(f)[0])),!l.tbody){f="table"!==i||lb.test(f)?"<table>"!==k[1]||lb.test(f)?0:h:h.firstChild,e=f&&f.childNodes.length;while(e--)n.nodeName(j=f.childNodes[e],"tbody")&&!j.childNodes.length&&f.removeChild(j)}n.merge(p,h.childNodes),h.textContent="";while(h.firstChild)h.removeChild(h.firstChild);h=o.lastChild}else p.push(b.createTextNode(f));h&&o.removeChild(h),l.appendChecked||n.grep(vb(p,"input"),wb),q=0;while(f=p[q++])if((!d||-1===n.inArray(f,d))&&(g=n.contains(f.ownerDocument,f),h=vb(o.appendChild(f),"script"),g&&Ab(h),c)){e=0;while(f=h[e++])pb.test(f.type||"")&&c.push(f)}return h=null,o},cleanData:function(a,b){for(var d,e,f,g,h=0,i=n.expando,j=n.cache,k=l.deleteExpando,m=n.event.special;null!=(d=a[h]);h++)if((b||n.acceptData(d))&&(f=d[i],g=f&&j[f])){if(g.events)for(e in g.events)m[e]?n.event.remove(d,e):n.removeEvent(d,e,g.handle);j[f]&&(delete j[f],k?delete d[i]:typeof d.removeAttribute!==L?d.removeAttribute(i):d[i]=null,c.push(f))}}}),n.fn.extend({text:function(a){return W(this,function(a){return void 0===a?n.text(this):this.empty().append((this[0]&&this[0].ownerDocument||z).createTextNode(a))},null,a,arguments.length)},append:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=xb(this,a);b.appendChild(a)}})},prepend:function(){return this.domManip(arguments,function(a){if(1===this.nodeType||11===this.nodeType||9===this.nodeType){var b=xb(this,a);b.insertBefore(a,b.firstChild)}})},before:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this)})},after:function(){return this.domManip(arguments,function(a){this.parentNode&&this.parentNode.insertBefore(a,this.nextSibling)})},remove:function(a,b){for(var c,d=a?n.filter(a,this):this,e=0;null!=(c=d[e]);e++)b||1!==c.nodeType||n.cleanData(vb(c)),c.parentNode&&(b&&n.contains(c.ownerDocument,c)&&Ab(vb(c,"script")),c.parentNode.removeChild(c));return this},empty:function(){for(var a,b=0;null!=(a=this[b]);b++){1===a.nodeType&&n.cleanData(vb(a,!1));while(a.firstChild)a.removeChild(a.firstChild);a.options&&n.nodeName(a,"select")&&(a.options.length=0)}return this},clone:function(a,b){return a=null==a?!1:a,b=null==b?a:b,this.map(function(){return n.clone(this,a,b)})},html:function(a){return W(this,function(a){var b=this[0]||{},c=0,d=this.length;if(void 0===a)return 1===b.nodeType?b.innerHTML.replace(gb,""):void 0;if(!("string"!=typeof a||nb.test(a)||!l.htmlSerialize&&hb.test(a)||!l.leadingWhitespace&&ib.test(a)||sb[(kb.exec(a)||["",""])[1].toLowerCase()])){a=a.replace(jb,"<$1></$2>");try{for(;d>c;c++)b=this[c]||{},1===b.nodeType&&(n.cleanData(vb(b,!1)),b.innerHTML=a);b=0}catch(e){}}b&&this.empty().append(a)},null,a,arguments.length)},replaceWith:function(){var a=arguments[0];return this.domManip(arguments,function(b){a=this.parentNode,n.cleanData(vb(this)),a&&a.replaceChild(b,this)}),a&&(a.length||a.nodeType)?this:this.remove()},detach:function(a){return this.remove(a,!0)},domManip:function(a,b){a=e.apply([],a);var c,d,f,g,h,i,j=0,k=this.length,m=this,o=k-1,p=a[0],q=n.isFunction(p);if(q||k>1&&"string"==typeof p&&!l.checkClone&&ob.test(p))return this.each(function(c){var d=m.eq(c);q&&(a[0]=p.call(this,c,d.html())),d.domManip(a,b)});if(k&&(i=n.buildFragment(a,this[0].ownerDocument,!1,this),c=i.firstChild,1===i.childNodes.length&&(i=c),c)){for(g=n.map(vb(i,"script"),yb),f=g.length;k>j;j++)d=i,j!==o&&(d=n.clone(d,!0,!0),f&&n.merge(g,vb(d,"script"))),b.call(this[j],d,j);if(f)for(h=g[g.length-1].ownerDocument,n.map(g,zb),j=0;f>j;j++)d=g[j],pb.test(d.type||"")&&!n._data(d,"globalEval")&&n.contains(h,d)&&(d.src?n._evalUrl&&n._evalUrl(d.src):n.globalEval((d.text||d.textContent||d.innerHTML||"").replace(rb,"")));i=c=null}return this}}),n.each({appendTo:"append",prependTo:"prepend",insertBefore:"before",insertAfter:"after",replaceAll:"replaceWith"},function(a,b){n.fn[a]=function(a){for(var c,d=0,e=[],g=n(a),h=g.length-1;h>=d;d++)c=d===h?this:this.clone(!0),n(g[d])[b](c),f.apply(e,c.get());return this.pushStack(e)}});var Db,Eb={};function Fb(b,c){var d=n(c.createElement(b)).appendTo(c.body),e=a.getDefaultComputedStyle?a.getDefaultComputedStyle(d[0]).display:n.css(d[0],"display");return d.detach(),e}function Gb(a){var b=z,c=Eb[a];return c||(c=Fb(a,b),"none"!==c&&c||(Db=(Db||n("<iframe frameborder='0' width='0' height='0'/>")).appendTo(b.documentElement),b=(Db[0].contentWindow||Db[0].contentDocument).document,b.write(),b.close(),c=Fb(a,b),Db.detach()),Eb[a]=c),c}!function(){var a,b,c=z.createElement("div"),d="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0";c.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",a=c.getElementsByTagName("a")[0],a.style.cssText="float:left;opacity:.5",l.opacity=/^0.5/.test(a.style.opacity),l.cssFloat=!!a.style.cssFloat,c.style.backgroundClip="content-box",c.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===c.style.backgroundClip,a=c=null,l.shrinkWrapBlocks=function(){var a,c,e,f;if(null==b){if(a=z.getElementsByTagName("body")[0],!a)return;f="border:0;width:0;height:0;position:absolute;top:0;left:-9999px",c=z.createElement("div"),e=z.createElement("div"),a.appendChild(c).appendChild(e),b=!1,typeof e.style.zoom!==L&&(e.style.cssText=d+";width:1px;padding:1px;zoom:1",e.innerHTML="<div></div>",e.firstChild.style.width="5px",b=3!==e.offsetWidth),a.removeChild(c),a=c=e=null}return b}}();var Hb=/^margin/,Ib=new RegExp("^("+T+")(?!px)[a-z%]+$","i"),Jb,Kb,Lb=/^(top|right|bottom|left)$/;a.getComputedStyle?(Jb=function(a){return a.ownerDocument.defaultView.getComputedStyle(a,null)},Kb=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Jb(a),g=c?c.getPropertyValue(b)||c[b]:void 0,c&&(""!==g||n.contains(a.ownerDocument,a)||(g=n.style(a,b)),Ib.test(g)&&Hb.test(b)&&(d=h.width,e=h.minWidth,f=h.maxWidth,h.minWidth=h.maxWidth=h.width=g,g=c.width,h.width=d,h.minWidth=e,h.maxWidth=f)),void 0===g?g:g+""}):z.documentElement.currentStyle&&(Jb=function(a){return a.currentStyle},Kb=function(a,b,c){var d,e,f,g,h=a.style;return c=c||Jb(a),g=c?c[b]:void 0,null==g&&h&&h[b]&&(g=h[b]),Ib.test(g)&&!Lb.test(b)&&(d=h.left,e=a.runtimeStyle,f=e&&e.left,f&&(e.left=a.currentStyle.left),h.left="fontSize"===b?"1em":g,g=h.pixelLeft+"px",h.left=d,f&&(e.left=f)),void 0===g?g:g+""||"auto"});function Mb(a,b){return{get:function(){var c=a();if(null!=c)return c?void delete this.get:(this.get=b).apply(this,arguments)}}}!function(){var b,c,d,e,f,g,h=z.createElement("div"),i="border:0;width:0;height:0;position:absolute;top:0;left:-9999px",j="-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0";h.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",b=h.getElementsByTagName("a")[0],b.style.cssText="float:left;opacity:.5",l.opacity=/^0.5/.test(b.style.opacity),l.cssFloat=!!b.style.cssFloat,h.style.backgroundClip="content-box",h.cloneNode(!0).style.backgroundClip="",l.clearCloneStyle="content-box"===h.style.backgroundClip,b=h=null,n.extend(l,{reliableHiddenOffsets:function(){if(null!=c)return c;var a,b,d,e=z.createElement("div"),f=z.getElementsByTagName("body")[0];if(f)return e.setAttribute("className","t"),e.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",a=z.createElement("div"),a.style.cssText=i,f.appendChild(a).appendChild(e),e.innerHTML="<table><tr><td></td><td>t</td></tr></table>",b=e.getElementsByTagName("td"),b[0].style.cssText="padding:0;margin:0;border:0;display:none",d=0===b[0].offsetHeight,b[0].style.display="",b[1].style.display="none",c=d&&0===b[0].offsetHeight,f.removeChild(a),e=f=null,c},boxSizing:function(){return null==d&&k(),d},boxSizingReliable:function(){return null==e&&k(),e},pixelPosition:function(){return null==f&&k(),f},reliableMarginRight:function(){var b,c,d,e;if(null==g&&a.getComputedStyle){if(b=z.getElementsByTagName("body")[0],!b)return;c=z.createElement("div"),d=z.createElement("div"),c.style.cssText=i,b.appendChild(c).appendChild(d),e=d.appendChild(z.createElement("div")),e.style.cssText=d.style.cssText=j,e.style.marginRight=e.style.width="0",d.style.width="1px",g=!parseFloat((a.getComputedStyle(e,null)||{}).marginRight),b.removeChild(c)}return g}});function k(){var b,c,h=z.getElementsByTagName("body")[0];h&&(b=z.createElement("div"),c=z.createElement("div"),b.style.cssText=i,h.appendChild(b).appendChild(c),c.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;display:block;padding:1px;border:1px;width:4px;margin-top:1%;top:1%",n.swap(h,null!=h.style.zoom?{zoom:1}:{},function(){d=4===c.offsetWidth}),e=!0,f=!1,g=!0,a.getComputedStyle&&(f="1%"!==(a.getComputedStyle(c,null)||{}).top,e="4px"===(a.getComputedStyle(c,null)||{width:"4px"}).width),h.removeChild(b),c=h=null)}}(),n.swap=function(a,b,c,d){var e,f,g={};for(f in b)g[f]=a.style[f],a.style[f]=b[f];e=c.apply(a,d||[]);for(f in b)a.style[f]=g[f];return e};var Nb=/alpha\([^)]*\)/i,Ob=/opacity\s*=\s*([^)]*)/,Pb=/^(none|table(?!-c[ea]).+)/,Qb=new RegExp("^("+T+")(.*)$","i"),Rb=new RegExp("^([+-])=("+T+")","i"),Sb={position:"absolute",visibility:"hidden",display:"block"},Tb={letterSpacing:0,fontWeight:400},Ub=["Webkit","O","Moz","ms"];function Vb(a,b){if(b in a)return b;var c=b.charAt(0).toUpperCase()+b.slice(1),d=b,e=Ub.length;while(e--)if(b=Ub[e]+c,b in a)return b;return d}function Wb(a,b){for(var c,d,e,f=[],g=0,h=a.length;h>g;g++)d=a[g],d.style&&(f[g]=n._data(d,"olddisplay"),c=d.style.display,b?(f[g]||"none"!==c||(d.style.display=""),""===d.style.display&&V(d)&&(f[g]=n._data(d,"olddisplay",Gb(d.nodeName)))):f[g]||(e=V(d),(c&&"none"!==c||!e)&&n._data(d,"olddisplay",e?c:n.css(d,"display"))));for(g=0;h>g;g++)d=a[g],d.style&&(b&&"none"!==d.style.display&&""!==d.style.display||(d.style.display=b?f[g]||"":"none"));return a}function Xb(a,b,c){var d=Qb.exec(b);return d?Math.max(0,d[1]-(c||0))+(d[2]||"px"):b}function Yb(a,b,c,d,e){for(var f=c===(d?"border":"content")?4:"width"===b?1:0,g=0;4>f;f+=2)"margin"===c&&(g+=n.css(a,c+U[f],!0,e)),d?("content"===c&&(g-=n.css(a,"padding"+U[f],!0,e)),"margin"!==c&&(g-=n.css(a,"border"+U[f]+"Width",!0,e))):(g+=n.css(a,"padding"+U[f],!0,e),"padding"!==c&&(g+=n.css(a,"border"+U[f]+"Width",!0,e)));return g}function Zb(a,b,c){var d=!0,e="width"===b?a.offsetWidth:a.offsetHeight,f=Jb(a),g=l.boxSizing()&&"border-box"===n.css(a,"boxSizing",!1,f);if(0>=e||null==e){if(e=Kb(a,b,f),(0>e||null==e)&&(e=a.style[b]),Ib.test(e))return e;d=g&&(l.boxSizingReliable()||e===a.style[b]),e=parseFloat(e)||0}return e+Yb(a,b,c||(g?"border":"content"),d,f)+"px"}n.extend({cssHooks:{opacity:{get:function(a,b){if(b){var c=Kb(a,"opacity");return""===c?"1":c}}}},cssNumber:{columnCount:!0,fillOpacity:!0,fontWeight:!0,lineHeight:!0,opacity:!0,order:!0,orphans:!0,widows:!0,zIndex:!0,zoom:!0},cssProps:{"float":l.cssFloat?"cssFloat":"styleFloat"},style:function(a,b,c,d){if(a&&3!==a.nodeType&&8!==a.nodeType&&a.style){var e,f,g,h=n.camelCase(b),i=a.style;if(b=n.cssProps[h]||(n.cssProps[h]=Vb(i,h)),g=n.cssHooks[b]||n.cssHooks[h],void 0===c)return g&&"get"in g&&void 0!==(e=g.get(a,!1,d))?e:i[b];if(f=typeof c,"string"===f&&(e=Rb.exec(c))&&(c=(e[1]+1)*e[2]+parseFloat(n.css(a,b)),f="number"),null!=c&&c===c&&("number"!==f||n.cssNumber[h]||(c+="px"),l.clearCloneStyle||""!==c||0!==b.indexOf("background")||(i[b]="inherit"),!(g&&"set"in g&&void 0===(c=g.set(a,c,d)))))try{i[b]="",i[b]=c}catch(j){}}},css:function(a,b,c,d){var e,f,g,h=n.camelCase(b);return b=n.cssProps[h]||(n.cssProps[h]=Vb(a.style,h)),g=n.cssHooks[b]||n.cssHooks[h],g&&"get"in g&&(f=g.get(a,!0,c)),void 0===f&&(f=Kb(a,b,d)),"normal"===f&&b in Tb&&(f=Tb[b]),""===c||c?(e=parseFloat(f),c===!0||n.isNumeric(e)?e||0:f):f}}),n.each(["height","width"],function(a,b){n.cssHooks[b]={get:function(a,c,d){return c?0===a.offsetWidth&&Pb.test(n.css(a,"display"))?n.swap(a,Sb,function(){return Zb(a,b,d)}):Zb(a,b,d):void 0},set:function(a,c,d){var e=d&&Jb(a);return Xb(a,c,d?Yb(a,b,d,l.boxSizing()&&"border-box"===n.css(a,"boxSizing",!1,e),e):0)}}}),l.opacity||(n.cssHooks.opacity={get:function(a,b){return Ob.test((b&&a.currentStyle?a.currentStyle.filter:a.style.filter)||"")?.01*parseFloat(RegExp.$1)+"":b?"1":""},set:function(a,b){var c=a.style,d=a.currentStyle,e=n.isNumeric(b)?"alpha(opacity="+100*b+")":"",f=d&&d.filter||c.filter||"";c.zoom=1,(b>=1||""===b)&&""===n.trim(f.replace(Nb,""))&&c.removeAttribute&&(c.removeAttribute("filter"),""===b||d&&!d.filter)||(c.filter=Nb.test(f)?f.replace(Nb,e):f+" "+e)}}),n.cssHooks.marginRight=Mb(l.reliableMarginRight,function(a,b){return b?n.swap(a,{display:"inline-block"},Kb,[a,"marginRight"]):void 0}),n.each({margin:"",padding:"",border:"Width"},function(a,b){n.cssHooks[a+b]={expand:function(c){for(var d=0,e={},f="string"==typeof c?c.split(" "):[c];4>d;d++)e[a+U[d]+b]=f[d]||f[d-2]||f[0];return e}},Hb.test(a)||(n.cssHooks[a+b].set=Xb)}),n.fn.extend({css:function(a,b){return W(this,function(a,b,c){var d,e,f={},g=0;if(n.isArray(b)){for(d=Jb(a),e=b.length;e>g;g++)f[b[g]]=n.css(a,b[g],!1,d);return f}return void 0!==c?n.style(a,b,c):n.css(a,b)
},a,b,arguments.length>1)},show:function(){return Wb(this,!0)},hide:function(){return Wb(this)},toggle:function(a){return"boolean"==typeof a?a?this.show():this.hide():this.each(function(){V(this)?n(this).show():n(this).hide()})}});function $b(a,b,c,d,e){return new $b.prototype.init(a,b,c,d,e)}n.Tween=$b,$b.prototype={constructor:$b,init:function(a,b,c,d,e,f){this.elem=a,this.prop=c,this.easing=e||"swing",this.options=b,this.start=this.now=this.cur(),this.end=d,this.unit=f||(n.cssNumber[c]?"":"px")},cur:function(){var a=$b.propHooks[this.prop];return a&&a.get?a.get(this):$b.propHooks._default.get(this)},run:function(a){var b,c=$b.propHooks[this.prop];return this.pos=b=this.options.duration?n.easing[this.easing](a,this.options.duration*a,0,1,this.options.duration):a,this.now=(this.end-this.start)*b+this.start,this.options.step&&this.options.step.call(this.elem,this.now,this),c&&c.set?c.set(this):$b.propHooks._default.set(this),this}},$b.prototype.init.prototype=$b.prototype,$b.propHooks={_default:{get:function(a){var b;return null==a.elem[a.prop]||a.elem.style&&null!=a.elem.style[a.prop]?(b=n.css(a.elem,a.prop,""),b&&"auto"!==b?b:0):a.elem[a.prop]},set:function(a){n.fx.step[a.prop]?n.fx.step[a.prop](a):a.elem.style&&(null!=a.elem.style[n.cssProps[a.prop]]||n.cssHooks[a.prop])?n.style(a.elem,a.prop,a.now+a.unit):a.elem[a.prop]=a.now}}},$b.propHooks.scrollTop=$b.propHooks.scrollLeft={set:function(a){a.elem.nodeType&&a.elem.parentNode&&(a.elem[a.prop]=a.now)}},n.easing={linear:function(a){return a},swing:function(a){return.5-Math.cos(a*Math.PI)/2}},n.fx=$b.prototype.init,n.fx.step={};var _b,ac,bc=/^(?:toggle|show|hide)$/,cc=new RegExp("^(?:([+-])=|)("+T+")([a-z%]*)$","i"),dc=/queueHooks$/,ec=[jc],fc={"*":[function(a,b){var c=this.createTween(a,b),d=c.cur(),e=cc.exec(b),f=e&&e[3]||(n.cssNumber[a]?"":"px"),g=(n.cssNumber[a]||"px"!==f&&+d)&&cc.exec(n.css(c.elem,a)),h=1,i=20;if(g&&g[3]!==f){f=f||g[3],e=e||[],g=+d||1;do h=h||".5",g/=h,n.style(c.elem,a,g+f);while(h!==(h=c.cur()/d)&&1!==h&&--i)}return e&&(g=c.start=+g||+d||0,c.unit=f,c.end=e[1]?g+(e[1]+1)*e[2]:+e[2]),c}]};function gc(){return setTimeout(function(){_b=void 0}),_b=n.now()}function hc(a,b){var c,d={height:a},e=0;for(b=b?1:0;4>e;e+=2-b)c=U[e],d["margin"+c]=d["padding"+c]=a;return b&&(d.opacity=d.width=a),d}function ic(a,b,c){for(var d,e=(fc[b]||[]).concat(fc["*"]),f=0,g=e.length;g>f;f++)if(d=e[f].call(c,b,a))return d}function jc(a,b,c){var d,e,f,g,h,i,j,k,m=this,o={},p=a.style,q=a.nodeType&&V(a),r=n._data(a,"fxshow");c.queue||(h=n._queueHooks(a,"fx"),null==h.unqueued&&(h.unqueued=0,i=h.empty.fire,h.empty.fire=function(){h.unqueued||i()}),h.unqueued++,m.always(function(){m.always(function(){h.unqueued--,n.queue(a,"fx").length||h.empty.fire()})})),1===a.nodeType&&("height"in b||"width"in b)&&(c.overflow=[p.overflow,p.overflowX,p.overflowY],j=n.css(a,"display"),k=Gb(a.nodeName),"none"===j&&(j=k),"inline"===j&&"none"===n.css(a,"float")&&(l.inlineBlockNeedsLayout&&"inline"!==k?p.zoom=1:p.display="inline-block")),c.overflow&&(p.overflow="hidden",l.shrinkWrapBlocks()||m.always(function(){p.overflow=c.overflow[0],p.overflowX=c.overflow[1],p.overflowY=c.overflow[2]}));for(d in b)if(e=b[d],bc.exec(e)){if(delete b[d],f=f||"toggle"===e,e===(q?"hide":"show")){if("show"!==e||!r||void 0===r[d])continue;q=!0}o[d]=r&&r[d]||n.style(a,d)}if(!n.isEmptyObject(o)){r?"hidden"in r&&(q=r.hidden):r=n._data(a,"fxshow",{}),f&&(r.hidden=!q),q?n(a).show():m.done(function(){n(a).hide()}),m.done(function(){var b;n._removeData(a,"fxshow");for(b in o)n.style(a,b,o[b])});for(d in o)g=ic(q?r[d]:0,d,m),d in r||(r[d]=g.start,q&&(g.end=g.start,g.start="width"===d||"height"===d?1:0))}}function kc(a,b){var c,d,e,f,g;for(c in a)if(d=n.camelCase(c),e=b[d],f=a[c],n.isArray(f)&&(e=f[1],f=a[c]=f[0]),c!==d&&(a[d]=f,delete a[c]),g=n.cssHooks[d],g&&"expand"in g){f=g.expand(f),delete a[d];for(c in f)c in a||(a[c]=f[c],b[c]=e)}else b[d]=e}function lc(a,b,c){var d,e,f=0,g=ec.length,h=n.Deferred().always(function(){delete i.elem}),i=function(){if(e)return!1;for(var b=_b||gc(),c=Math.max(0,j.startTime+j.duration-b),d=c/j.duration||0,f=1-d,g=0,i=j.tweens.length;i>g;g++)j.tweens[g].run(f);return h.notifyWith(a,[j,f,c]),1>f&&i?c:(h.resolveWith(a,[j]),!1)},j=h.promise({elem:a,props:n.extend({},b),opts:n.extend(!0,{specialEasing:{}},c),originalProperties:b,originalOptions:c,startTime:_b||gc(),duration:c.duration,tweens:[],createTween:function(b,c){var d=n.Tween(a,j.opts,b,c,j.opts.specialEasing[b]||j.opts.easing);return j.tweens.push(d),d},stop:function(b){var c=0,d=b?j.tweens.length:0;if(e)return this;for(e=!0;d>c;c++)j.tweens[c].run(1);return b?h.resolveWith(a,[j,b]):h.rejectWith(a,[j,b]),this}}),k=j.props;for(kc(k,j.opts.specialEasing);g>f;f++)if(d=ec[f].call(j,a,k,j.opts))return d;return n.map(k,ic,j),n.isFunction(j.opts.start)&&j.opts.start.call(a,j),n.fx.timer(n.extend(i,{elem:a,anim:j,queue:j.opts.queue})),j.progress(j.opts.progress).done(j.opts.done,j.opts.complete).fail(j.opts.fail).always(j.opts.always)}n.Animation=n.extend(lc,{tweener:function(a,b){n.isFunction(a)?(b=a,a=["*"]):a=a.split(" ");for(var c,d=0,e=a.length;e>d;d++)c=a[d],fc[c]=fc[c]||[],fc[c].unshift(b)},prefilter:function(a,b){b?ec.unshift(a):ec.push(a)}}),n.speed=function(a,b,c){var d=a&&"object"==typeof a?n.extend({},a):{complete:c||!c&&b||n.isFunction(a)&&a,duration:a,easing:c&&b||b&&!n.isFunction(b)&&b};return d.duration=n.fx.off?0:"number"==typeof d.duration?d.duration:d.duration in n.fx.speeds?n.fx.speeds[d.duration]:n.fx.speeds._default,(null==d.queue||d.queue===!0)&&(d.queue="fx"),d.old=d.complete,d.complete=function(){n.isFunction(d.old)&&d.old.call(this),d.queue&&n.dequeue(this,d.queue)},d},n.fn.extend({fadeTo:function(a,b,c,d){return this.filter(V).css("opacity",0).show().end().animate({opacity:b},a,c,d)},animate:function(a,b,c,d){var e=n.isEmptyObject(a),f=n.speed(b,c,d),g=function(){var b=lc(this,n.extend({},a),f);(e||n._data(this,"finish"))&&b.stop(!0)};return g.finish=g,e||f.queue===!1?this.each(g):this.queue(f.queue,g)},stop:function(a,b,c){var d=function(a){var b=a.stop;delete a.stop,b(c)};return"string"!=typeof a&&(c=b,b=a,a=void 0),b&&a!==!1&&this.queue(a||"fx",[]),this.each(function(){var b=!0,e=null!=a&&a+"queueHooks",f=n.timers,g=n._data(this);if(e)g[e]&&g[e].stop&&d(g[e]);else for(e in g)g[e]&&g[e].stop&&dc.test(e)&&d(g[e]);for(e=f.length;e--;)f[e].elem!==this||null!=a&&f[e].queue!==a||(f[e].anim.stop(c),b=!1,f.splice(e,1));(b||!c)&&n.dequeue(this,a)})},finish:function(a){return a!==!1&&(a=a||"fx"),this.each(function(){var b,c=n._data(this),d=c[a+"queue"],e=c[a+"queueHooks"],f=n.timers,g=d?d.length:0;for(c.finish=!0,n.queue(this,a,[]),e&&e.stop&&e.stop.call(this,!0),b=f.length;b--;)f[b].elem===this&&f[b].queue===a&&(f[b].anim.stop(!0),f.splice(b,1));for(b=0;g>b;b++)d[b]&&d[b].finish&&d[b].finish.call(this);delete c.finish})}}),n.each(["toggle","show","hide"],function(a,b){var c=n.fn[b];n.fn[b]=function(a,d,e){return null==a||"boolean"==typeof a?c.apply(this,arguments):this.animate(hc(b,!0),a,d,e)}}),n.each({slideDown:hc("show"),slideUp:hc("hide"),slideToggle:hc("toggle"),fadeIn:{opacity:"show"},fadeOut:{opacity:"hide"},fadeToggle:{opacity:"toggle"}},function(a,b){n.fn[a]=function(a,c,d){return this.animate(b,a,c,d)}}),n.timers=[],n.fx.tick=function(){var a,b=n.timers,c=0;for(_b=n.now();c<b.length;c++)a=b[c],a()||b[c]!==a||b.splice(c--,1);b.length||n.fx.stop(),_b=void 0},n.fx.timer=function(a){n.timers.push(a),a()?n.fx.start():n.timers.pop()},n.fx.interval=13,n.fx.start=function(){ac||(ac=setInterval(n.fx.tick,n.fx.interval))},n.fx.stop=function(){clearInterval(ac),ac=null},n.fx.speeds={slow:600,fast:200,_default:400},n.fn.delay=function(a,b){return a=n.fx?n.fx.speeds[a]||a:a,b=b||"fx",this.queue(b,function(b,c){var d=setTimeout(b,a);c.stop=function(){clearTimeout(d)}})},function(){var a,b,c,d,e=z.createElement("div");e.setAttribute("className","t"),e.innerHTML="  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>",a=e.getElementsByTagName("a")[0],c=z.createElement("select"),d=c.appendChild(z.createElement("option")),b=e.getElementsByTagName("input")[0],a.style.cssText="top:1px",l.getSetAttribute="t"!==e.className,l.style=/top/.test(a.getAttribute("style")),l.hrefNormalized="/a"===a.getAttribute("href"),l.checkOn=!!b.value,l.optSelected=d.selected,l.enctype=!!z.createElement("form").enctype,c.disabled=!0,l.optDisabled=!d.disabled,b=z.createElement("input"),b.setAttribute("value",""),l.input=""===b.getAttribute("value"),b.value="t",b.setAttribute("type","radio"),l.radioValue="t"===b.value,a=b=c=d=e=null}();var mc=/\r/g;n.fn.extend({val:function(a){var b,c,d,e=this[0];{if(arguments.length)return d=n.isFunction(a),this.each(function(c){var e;1===this.nodeType&&(e=d?a.call(this,c,n(this).val()):a,null==e?e="":"number"==typeof e?e+="":n.isArray(e)&&(e=n.map(e,function(a){return null==a?"":a+""})),b=n.valHooks[this.type]||n.valHooks[this.nodeName.toLowerCase()],b&&"set"in b&&void 0!==b.set(this,e,"value")||(this.value=e))});if(e)return b=n.valHooks[e.type]||n.valHooks[e.nodeName.toLowerCase()],b&&"get"in b&&void 0!==(c=b.get(e,"value"))?c:(c=e.value,"string"==typeof c?c.replace(mc,""):null==c?"":c)}}}),n.extend({valHooks:{option:{get:function(a){var b=n.find.attr(a,"value");return null!=b?b:n.text(a)}},select:{get:function(a){for(var b,c,d=a.options,e=a.selectedIndex,f="select-one"===a.type||0>e,g=f?null:[],h=f?e+1:d.length,i=0>e?h:f?e:0;h>i;i++)if(c=d[i],!(!c.selected&&i!==e||(l.optDisabled?c.disabled:null!==c.getAttribute("disabled"))||c.parentNode.disabled&&n.nodeName(c.parentNode,"optgroup"))){if(b=n(c).val(),f)return b;g.push(b)}return g},set:function(a,b){var c,d,e=a.options,f=n.makeArray(b),g=e.length;while(g--)if(d=e[g],n.inArray(n.valHooks.option.get(d),f)>=0)try{d.selected=c=!0}catch(h){d.scrollHeight}else d.selected=!1;return c||(a.selectedIndex=-1),e}}}}),n.each(["radio","checkbox"],function(){n.valHooks[this]={set:function(a,b){return n.isArray(b)?a.checked=n.inArray(n(a).val(),b)>=0:void 0}},l.checkOn||(n.valHooks[this].get=function(a){return null===a.getAttribute("value")?"on":a.value})});var nc,oc,pc=n.expr.attrHandle,qc=/^(?:checked|selected)$/i,rc=l.getSetAttribute,sc=l.input;n.fn.extend({attr:function(a,b){return W(this,n.attr,a,b,arguments.length>1)},removeAttr:function(a){return this.each(function(){n.removeAttr(this,a)})}}),n.extend({attr:function(a,b,c){var d,e,f=a.nodeType;if(a&&3!==f&&8!==f&&2!==f)return typeof a.getAttribute===L?n.prop(a,b,c):(1===f&&n.isXMLDoc(a)||(b=b.toLowerCase(),d=n.attrHooks[b]||(n.expr.match.bool.test(b)?oc:nc)),void 0===c?d&&"get"in d&&null!==(e=d.get(a,b))?e:(e=n.find.attr(a,b),null==e?void 0:e):null!==c?d&&"set"in d&&void 0!==(e=d.set(a,c,b))?e:(a.setAttribute(b,c+""),c):void n.removeAttr(a,b))},removeAttr:function(a,b){var c,d,e=0,f=b&&b.match(F);if(f&&1===a.nodeType)while(c=f[e++])d=n.propFix[c]||c,n.expr.match.bool.test(c)?sc&&rc||!qc.test(c)?a[d]=!1:a[n.camelCase("default-"+c)]=a[d]=!1:n.attr(a,c,""),a.removeAttribute(rc?c:d)},attrHooks:{type:{set:function(a,b){if(!l.radioValue&&"radio"===b&&n.nodeName(a,"input")){var c=a.value;return a.setAttribute("type",b),c&&(a.value=c),b}}}}}),oc={set:function(a,b,c){return b===!1?n.removeAttr(a,c):sc&&rc||!qc.test(c)?a.setAttribute(!rc&&n.propFix[c]||c,c):a[n.camelCase("default-"+c)]=a[c]=!0,c}},n.each(n.expr.match.bool.source.match(/\w+/g),function(a,b){var c=pc[b]||n.find.attr;pc[b]=sc&&rc||!qc.test(b)?function(a,b,d){var e,f;return d||(f=pc[b],pc[b]=e,e=null!=c(a,b,d)?b.toLowerCase():null,pc[b]=f),e}:function(a,b,c){return c?void 0:a[n.camelCase("default-"+b)]?b.toLowerCase():null}}),sc&&rc||(n.attrHooks.value={set:function(a,b,c){return n.nodeName(a,"input")?void(a.defaultValue=b):nc&&nc.set(a,b,c)}}),rc||(nc={set:function(a,b,c){var d=a.getAttributeNode(c);return d||a.setAttributeNode(d=a.ownerDocument.createAttribute(c)),d.value=b+="","value"===c||b===a.getAttribute(c)?b:void 0}},pc.id=pc.name=pc.coords=function(a,b,c){var d;return c?void 0:(d=a.getAttributeNode(b))&&""!==d.value?d.value:null},n.valHooks.button={get:function(a,b){var c=a.getAttributeNode(b);return c&&c.specified?c.value:void 0},set:nc.set},n.attrHooks.contenteditable={set:function(a,b,c){nc.set(a,""===b?!1:b,c)}},n.each(["width","height"],function(a,b){n.attrHooks[b]={set:function(a,c){return""===c?(a.setAttribute(b,"auto"),c):void 0}}})),l.style||(n.attrHooks.style={get:function(a){return a.style.cssText||void 0},set:function(a,b){return a.style.cssText=b+""}});var tc=/^(?:input|select|textarea|button|object)$/i,uc=/^(?:a|area)$/i;n.fn.extend({prop:function(a,b){return W(this,n.prop,a,b,arguments.length>1)},removeProp:function(a){return a=n.propFix[a]||a,this.each(function(){try{this[a]=void 0,delete this[a]}catch(b){}})}}),n.extend({propFix:{"for":"htmlFor","class":"className"},prop:function(a,b,c){var d,e,f,g=a.nodeType;if(a&&3!==g&&8!==g&&2!==g)return f=1!==g||!n.isXMLDoc(a),f&&(b=n.propFix[b]||b,e=n.propHooks[b]),void 0!==c?e&&"set"in e&&void 0!==(d=e.set(a,c,b))?d:a[b]=c:e&&"get"in e&&null!==(d=e.get(a,b))?d:a[b]},propHooks:{tabIndex:{get:function(a){var b=n.find.attr(a,"tabindex");return b?parseInt(b,10):tc.test(a.nodeName)||uc.test(a.nodeName)&&a.href?0:-1}}}}),l.hrefNormalized||n.each(["href","src"],function(a,b){n.propHooks[b]={get:function(a){return a.getAttribute(b,4)}}}),l.optSelected||(n.propHooks.selected={get:function(a){var b=a.parentNode;return b&&(b.selectedIndex,b.parentNode&&b.parentNode.selectedIndex),null}}),n.each(["tabIndex","readOnly","maxLength","cellSpacing","cellPadding","rowSpan","colSpan","useMap","frameBorder","contentEditable"],function(){n.propFix[this.toLowerCase()]=this}),l.enctype||(n.propFix.enctype="encoding");var vc=/[\t\r\n\f]/g;n.fn.extend({addClass:function(a){var b,c,d,e,f,g,h=0,i=this.length,j="string"==typeof a&&a;if(n.isFunction(a))return this.each(function(b){n(this).addClass(a.call(this,b,this.className))});if(j)for(b=(a||"").match(F)||[];i>h;h++)if(c=this[h],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(vc," "):" ")){f=0;while(e=b[f++])d.indexOf(" "+e+" ")<0&&(d+=e+" ");g=n.trim(d),c.className!==g&&(c.className=g)}return this},removeClass:function(a){var b,c,d,e,f,g,h=0,i=this.length,j=0===arguments.length||"string"==typeof a&&a;if(n.isFunction(a))return this.each(function(b){n(this).removeClass(a.call(this,b,this.className))});if(j)for(b=(a||"").match(F)||[];i>h;h++)if(c=this[h],d=1===c.nodeType&&(c.className?(" "+c.className+" ").replace(vc," "):"")){f=0;while(e=b[f++])while(d.indexOf(" "+e+" ")>=0)d=d.replace(" "+e+" "," ");g=a?n.trim(d):"",c.className!==g&&(c.className=g)}return this},toggleClass:function(a,b){var c=typeof a;return"boolean"==typeof b&&"string"===c?b?this.addClass(a):this.removeClass(a):this.each(n.isFunction(a)?function(c){n(this).toggleClass(a.call(this,c,this.className,b),b)}:function(){if("string"===c){var b,d=0,e=n(this),f=a.match(F)||[];while(b=f[d++])e.hasClass(b)?e.removeClass(b):e.addClass(b)}else(c===L||"boolean"===c)&&(this.className&&n._data(this,"__className__",this.className),this.className=this.className||a===!1?"":n._data(this,"__className__")||"")})},hasClass:function(a){for(var b=" "+a+" ",c=0,d=this.length;d>c;c++)if(1===this[c].nodeType&&(" "+this[c].className+" ").replace(vc," ").indexOf(b)>=0)return!0;return!1}}),n.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "),function(a,b){n.fn[b]=function(a,c){return arguments.length>0?this.on(b,null,a,c):this.trigger(b)}}),n.fn.extend({hover:function(a,b){return this.mouseenter(a).mouseleave(b||a)},bind:function(a,b,c){return this.on(a,null,b,c)},unbind:function(a,b){return this.off(a,null,b)},delegate:function(a,b,c,d){return this.on(b,a,c,d)},undelegate:function(a,b,c){return 1===arguments.length?this.off(a,"**"):this.off(b,a||"**",c)}});var wc=n.now(),xc=/\?/,yc=/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;n.parseJSON=function(b){if(a.JSON&&a.JSON.parse)return a.JSON.parse(b+"");var c,d=null,e=n.trim(b+"");return e&&!n.trim(e.replace(yc,function(a,b,e,f){return c&&b&&(d=0),0===d?a:(c=e||b,d+=!f-!e,"")}))?Function("return "+e)():n.error("Invalid JSON: "+b)},n.parseXML=function(b){var c,d;if(!b||"string"!=typeof b)return null;try{a.DOMParser?(d=new DOMParser,c=d.parseFromString(b,"text/xml")):(c=new ActiveXObject("Microsoft.XMLDOM"),c.async="false",c.loadXML(b))}catch(e){c=void 0}return c&&c.documentElement&&!c.getElementsByTagName("parsererror").length||n.error("Invalid XML: "+b),c};var zc,Ac,Bc=/#.*$/,Cc=/([?&])_=[^&]*/,Dc=/^(.*?):[ \t]*([^\r\n]*)\r?$/gm,Ec=/^(?:about|app|app-storage|.+-extension|file|res|widget):$/,Fc=/^(?:GET|HEAD)$/,Gc=/^\/\//,Hc=/^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,Ic={},Jc={},Kc="*/".concat("*");try{Ac=location.href}catch(Lc){Ac=z.createElement("a"),Ac.href="",Ac=Ac.href}zc=Hc.exec(Ac.toLowerCase())||[];function Mc(a){return function(b,c){"string"!=typeof b&&(c=b,b="*");var d,e=0,f=b.toLowerCase().match(F)||[];if(n.isFunction(c))while(d=f[e++])"+"===d.charAt(0)?(d=d.slice(1)||"*",(a[d]=a[d]||[]).unshift(c)):(a[d]=a[d]||[]).push(c)}}function Nc(a,b,c,d){var e={},f=a===Jc;function g(h){var i;return e[h]=!0,n.each(a[h]||[],function(a,h){var j=h(b,c,d);return"string"!=typeof j||f||e[j]?f?!(i=j):void 0:(b.dataTypes.unshift(j),g(j),!1)}),i}return g(b.dataTypes[0])||!e["*"]&&g("*")}function Oc(a,b){var c,d,e=n.ajaxSettings.flatOptions||{};for(d in b)void 0!==b[d]&&((e[d]?a:c||(c={}))[d]=b[d]);return c&&n.extend(!0,a,c),a}function Pc(a,b,c){var d,e,f,g,h=a.contents,i=a.dataTypes;while("*"===i[0])i.shift(),void 0===e&&(e=a.mimeType||b.getResponseHeader("Content-Type"));if(e)for(g in h)if(h[g]&&h[g].test(e)){i.unshift(g);break}if(i[0]in c)f=i[0];else{for(g in c){if(!i[0]||a.converters[g+" "+i[0]]){f=g;break}d||(d=g)}f=f||d}return f?(f!==i[0]&&i.unshift(f),c[f]):void 0}function Qc(a,b,c,d){var e,f,g,h,i,j={},k=a.dataTypes.slice();if(k[1])for(g in a.converters)j[g.toLowerCase()]=a.converters[g];f=k.shift();while(f)if(a.responseFields[f]&&(c[a.responseFields[f]]=b),!i&&d&&a.dataFilter&&(b=a.dataFilter(b,a.dataType)),i=f,f=k.shift())if("*"===f)f=i;else if("*"!==i&&i!==f){if(g=j[i+" "+f]||j["* "+f],!g)for(e in j)if(h=e.split(" "),h[1]===f&&(g=j[i+" "+h[0]]||j["* "+h[0]])){g===!0?g=j[e]:j[e]!==!0&&(f=h[0],k.unshift(h[1]));break}if(g!==!0)if(g&&a["throws"])b=g(b);else try{b=g(b)}catch(l){return{state:"parsererror",error:g?l:"No conversion from "+i+" to "+f}}}return{state:"success",data:b}}n.extend({active:0,lastModified:{},etag:{},ajaxSettings:{url:Ac,type:"GET",isLocal:Ec.test(zc[1]),global:!0,processData:!0,async:!0,contentType:"application/x-www-form-urlencoded; charset=UTF-8",accepts:{"*":Kc,text:"text/plain",html:"text/html",xml:"application/xml, text/xml",json:"application/json, text/javascript"},contents:{xml:/xml/,html:/html/,json:/json/},responseFields:{xml:"responseXML",text:"responseText",json:"responseJSON"},converters:{"* text":String,"text html":!0,"text json":n.parseJSON,"text xml":n.parseXML},flatOptions:{url:!0,context:!0}},ajaxSetup:function(a,b){return b?Oc(Oc(a,n.ajaxSettings),b):Oc(n.ajaxSettings,a)},ajaxPrefilter:Mc(Ic),ajaxTransport:Mc(Jc),ajax:function(a,b){"object"==typeof a&&(b=a,a=void 0),b=b||{};var c,d,e,f,g,h,i,j,k=n.ajaxSetup({},b),l=k.context||k,m=k.context&&(l.nodeType||l.jquery)?n(l):n.event,o=n.Deferred(),p=n.Callbacks("once memory"),q=k.statusCode||{},r={},s={},t=0,u="canceled",v={readyState:0,getResponseHeader:function(a){var b;if(2===t){if(!j){j={};while(b=Dc.exec(f))j[b[1].toLowerCase()]=b[2]}b=j[a.toLowerCase()]}return null==b?null:b},getAllResponseHeaders:function(){return 2===t?f:null},setRequestHeader:function(a,b){var c=a.toLowerCase();return t||(a=s[c]=s[c]||a,r[a]=b),this},overrideMimeType:function(a){return t||(k.mimeType=a),this},statusCode:function(a){var b;if(a)if(2>t)for(b in a)q[b]=[q[b],a[b]];else v.always(a[v.status]);return this},abort:function(a){var b=a||u;return i&&i.abort(b),x(0,b),this}};if(o.promise(v).complete=p.add,v.success=v.done,v.error=v.fail,k.url=((a||k.url||Ac)+"").replace(Bc,"").replace(Gc,zc[1]+"//"),k.type=b.method||b.type||k.method||k.type,k.dataTypes=n.trim(k.dataType||"*").toLowerCase().match(F)||[""],null==k.crossDomain&&(c=Hc.exec(k.url.toLowerCase()),k.crossDomain=!(!c||c[1]===zc[1]&&c[2]===zc[2]&&(c[3]||("http:"===c[1]?"80":"443"))===(zc[3]||("http:"===zc[1]?"80":"443")))),k.data&&k.processData&&"string"!=typeof k.data&&(k.data=n.param(k.data,k.traditional)),Nc(Ic,k,b,v),2===t)return v;h=k.global,h&&0===n.active++&&n.event.trigger("ajaxStart"),k.type=k.type.toUpperCase(),k.hasContent=!Fc.test(k.type),e=k.url,k.hasContent||(k.data&&(e=k.url+=(xc.test(e)?"&":"?")+k.data,delete k.data),k.cache===!1&&(k.url=Cc.test(e)?e.replace(Cc,"$1_="+wc++):e+(xc.test(e)?"&":"?")+"_="+wc++)),k.ifModified&&(n.lastModified[e]&&v.setRequestHeader("If-Modified-Since",n.lastModified[e]),n.etag[e]&&v.setRequestHeader("If-None-Match",n.etag[e])),(k.data&&k.hasContent&&k.contentType!==!1||b.contentType)&&v.setRequestHeader("Content-Type",k.contentType),v.setRequestHeader("Accept",k.dataTypes[0]&&k.accepts[k.dataTypes[0]]?k.accepts[k.dataTypes[0]]+("*"!==k.dataTypes[0]?", "+Kc+"; q=0.01":""):k.accepts["*"]);for(d in k.headers)v.setRequestHeader(d,k.headers[d]);if(k.beforeSend&&(k.beforeSend.call(l,v,k)===!1||2===t))return v.abort();u="abort";for(d in{success:1,error:1,complete:1})v[d](k[d]);if(i=Nc(Jc,k,b,v)){v.readyState=1,h&&m.trigger("ajaxSend",[v,k]),k.async&&k.timeout>0&&(g=setTimeout(function(){v.abort("timeout")},k.timeout));try{t=1,i.send(r,x)}catch(w){if(!(2>t))throw w;x(-1,w)}}else x(-1,"No Transport");function x(a,b,c,d){var j,r,s,u,w,x=b;2!==t&&(t=2,g&&clearTimeout(g),i=void 0,f=d||"",v.readyState=a>0?4:0,j=a>=200&&300>a||304===a,c&&(u=Pc(k,v,c)),u=Qc(k,u,v,j),j?(k.ifModified&&(w=v.getResponseHeader("Last-Modified"),w&&(n.lastModified[e]=w),w=v.getResponseHeader("etag"),w&&(n.etag[e]=w)),204===a||"HEAD"===k.type?x="nocontent":304===a?x="notmodified":(x=u.state,r=u.data,s=u.error,j=!s)):(s=x,(a||!x)&&(x="error",0>a&&(a=0))),v.status=a,v.statusText=(b||x)+"",j?o.resolveWith(l,[r,x,v]):o.rejectWith(l,[v,x,s]),v.statusCode(q),q=void 0,h&&m.trigger(j?"ajaxSuccess":"ajaxError",[v,k,j?r:s]),p.fireWith(l,[v,x]),h&&(m.trigger("ajaxComplete",[v,k]),--n.active||n.event.trigger("ajaxStop")))}return v},getJSON:function(a,b,c){return n.get(a,b,c,"json")},getScript:function(a,b){return n.get(a,void 0,b,"script")}}),n.each(["get","post"],function(a,b){n[b]=function(a,c,d,e){return n.isFunction(c)&&(e=e||d,d=c,c=void 0),n.ajax({url:a,type:b,dataType:e,data:c,success:d})}}),n.each(["ajaxStart","ajaxStop","ajaxComplete","ajaxError","ajaxSuccess","ajaxSend"],function(a,b){n.fn[b]=function(a){return this.on(b,a)}}),n._evalUrl=function(a){return n.ajax({url:a,type:"GET",dataType:"script",async:!1,global:!1,"throws":!0})},n.fn.extend({wrapAll:function(a){if(n.isFunction(a))return this.each(function(b){n(this).wrapAll(a.call(this,b))});if(this[0]){var b=n(a,this[0].ownerDocument).eq(0).clone(!0);this[0].parentNode&&b.insertBefore(this[0]),b.map(function(){var a=this;while(a.firstChild&&1===a.firstChild.nodeType)a=a.firstChild;return a}).append(this)}return this},wrapInner:function(a){return this.each(n.isFunction(a)?function(b){n(this).wrapInner(a.call(this,b))}:function(){var b=n(this),c=b.contents();c.length?c.wrapAll(a):b.append(a)})},wrap:function(a){var b=n.isFunction(a);return this.each(function(c){n(this).wrapAll(b?a.call(this,c):a)})},unwrap:function(){return this.parent().each(function(){n.nodeName(this,"body")||n(this).replaceWith(this.childNodes)}).end()}}),n.expr.filters.hidden=function(a){return a.offsetWidth<=0&&a.offsetHeight<=0||!l.reliableHiddenOffsets()&&"none"===(a.style&&a.style.display||n.css(a,"display"))},n.expr.filters.visible=function(a){return!n.expr.filters.hidden(a)};var Rc=/%20/g,Sc=/\[\]$/,Tc=/\r?\n/g,Uc=/^(?:submit|button|image|reset|file)$/i,Vc=/^(?:input|select|textarea|keygen)/i;function Wc(a,b,c,d){var e;if(n.isArray(b))n.each(b,function(b,e){c||Sc.test(a)?d(a,e):Wc(a+"["+("object"==typeof e?b:"")+"]",e,c,d)});else if(c||"object"!==n.type(b))d(a,b);else for(e in b)Wc(a+"["+e+"]",b[e],c,d)}n.param=function(a,b){var c,d=[],e=function(a,b){b=n.isFunction(b)?b():null==b?"":b,d[d.length]=encodeURIComponent(a)+"="+encodeURIComponent(b)};if(void 0===b&&(b=n.ajaxSettings&&n.ajaxSettings.traditional),n.isArray(a)||a.jquery&&!n.isPlainObject(a))n.each(a,function(){e(this.name,this.value)});else for(c in a)Wc(c,a[c],b,e);return d.join("&").replace(Rc,"+")},n.fn.extend({serialize:function(){return n.param(this.serializeArray())},serializeArray:function(){return this.map(function(){var a=n.prop(this,"elements");return a?n.makeArray(a):this}).filter(function(){var a=this.type;return this.name&&!n(this).is(":disabled")&&Vc.test(this.nodeName)&&!Uc.test(a)&&(this.checked||!X.test(a))}).map(function(a,b){var c=n(this).val();return null==c?null:n.isArray(c)?n.map(c,function(a){return{name:b.name,value:a.replace(Tc,"\r\n")}}):{name:b.name,value:c.replace(Tc,"\r\n")}}).get()}}),n.ajaxSettings.xhr=void 0!==a.ActiveXObject?function(){return!this.isLocal&&/^(get|post|head|put|delete|options)$/i.test(this.type)&&$c()||_c()}:$c;var Xc=0,Yc={},Zc=n.ajaxSettings.xhr();a.ActiveXObject&&n(a).on("unload",function(){for(var a in Yc)Yc[a](void 0,!0)}),l.cors=!!Zc&&"withCredentials"in Zc,Zc=l.ajax=!!Zc,Zc&&n.ajaxTransport(function(a){if(!a.crossDomain||l.cors){var b;return{send:function(c,d){var e,f=a.xhr(),g=++Xc;if(f.open(a.type,a.url,a.async,a.username,a.password),a.xhrFields)for(e in a.xhrFields)f[e]=a.xhrFields[e];a.mimeType&&f.overrideMimeType&&f.overrideMimeType(a.mimeType),a.crossDomain||c["X-Requested-With"]||(c["X-Requested-With"]="XMLHttpRequest");for(e in c)void 0!==c[e]&&f.setRequestHeader(e,c[e]+"");f.send(a.hasContent&&a.data||null),b=function(c,e){var h,i,j;if(b&&(e||4===f.readyState))if(delete Yc[g],b=void 0,f.onreadystatechange=n.noop,e)4!==f.readyState&&f.abort();else{j={},h=f.status,"string"==typeof f.responseText&&(j.text=f.responseText);try{i=f.statusText}catch(k){i=""}h||!a.isLocal||a.crossDomain?1223===h&&(h=204):h=j.text?200:404}j&&d(h,i,j,f.getAllResponseHeaders())},a.async?4===f.readyState?setTimeout(b):f.onreadystatechange=Yc[g]=b:b()},abort:function(){b&&b(void 0,!0)}}}});function $c(){try{return new a.XMLHttpRequest}catch(b){}}function _c(){try{return new a.ActiveXObject("Microsoft.XMLHTTP")}catch(b){}}n.ajaxSetup({accepts:{script:"text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"},contents:{script:/(?:java|ecma)script/},converters:{"text script":function(a){return n.globalEval(a),a}}}),n.ajaxPrefilter("script",function(a){void 0===a.cache&&(a.cache=!1),a.crossDomain&&(a.type="GET",a.global=!1)}),n.ajaxTransport("script",function(a){if(a.crossDomain){var b,c=z.head||n("head")[0]||z.documentElement;return{send:function(d,e){b=z.createElement("script"),b.async=!0,a.scriptCharset&&(b.charset=a.scriptCharset),b.src=a.url,b.onload=b.onreadystatechange=function(a,c){(c||!b.readyState||/loaded|complete/.test(b.readyState))&&(b.onload=b.onreadystatechange=null,b.parentNode&&b.parentNode.removeChild(b),b=null,c||e(200,"success"))},c.insertBefore(b,c.firstChild)},abort:function(){b&&b.onload(void 0,!0)}}}});var ad=[],bd=/(=)\?(?=&|$)|\?\?/;n.ajaxSetup({jsonp:"callback",jsonpCallback:function(){var a=ad.pop()||n.expando+"_"+wc++;return this[a]=!0,a}}),n.ajaxPrefilter("json jsonp",function(b,c,d){var e,f,g,h=b.jsonp!==!1&&(bd.test(b.url)?"url":"string"==typeof b.data&&!(b.contentType||"").indexOf("application/x-www-form-urlencoded")&&bd.test(b.data)&&"data");return h||"jsonp"===b.dataTypes[0]?(e=b.jsonpCallback=n.isFunction(b.jsonpCallback)?b.jsonpCallback():b.jsonpCallback,h?b[h]=b[h].replace(bd,"$1"+e):b.jsonp!==!1&&(b.url+=(xc.test(b.url)?"&":"?")+b.jsonp+"="+e),b.converters["script json"]=function(){return g||n.error(e+" was not called"),g[0]},b.dataTypes[0]="json",f=a[e],a[e]=function(){g=arguments},d.always(function(){a[e]=f,b[e]&&(b.jsonpCallback=c.jsonpCallback,ad.push(e)),g&&n.isFunction(f)&&f(g[0]),g=f=void 0}),"script"):void 0}),n.parseHTML=function(a,b,c){if(!a||"string"!=typeof a)return null;"boolean"==typeof b&&(c=b,b=!1),b=b||z;var d=v.exec(a),e=!c&&[];return d?[b.createElement(d[1])]:(d=n.buildFragment([a],b,e),e&&e.length&&n(e).remove(),n.merge([],d.childNodes))};var cd=n.fn.load;n.fn.load=function(a,b,c){if("string"!=typeof a&&cd)return cd.apply(this,arguments);var d,e,f,g=this,h=a.indexOf(" ");return h>=0&&(d=a.slice(h,a.length),a=a.slice(0,h)),n.isFunction(b)?(c=b,b=void 0):b&&"object"==typeof b&&(f="POST"),g.length>0&&n.ajax({url:a,type:f,dataType:"html",data:b}).done(function(a){e=arguments,g.html(d?n("<div>").append(n.parseHTML(a)).find(d):a)}).complete(c&&function(a,b){g.each(c,e||[a.responseText,b,a])}),this},n.expr.filters.animated=function(a){return n.grep(n.timers,function(b){return a===b.elem}).length};var dd=a.document.documentElement;function ed(a){return n.isWindow(a)?a:9===a.nodeType?a.defaultView||a.parentWindow:!1}n.offset={setOffset:function(a,b,c){var d,e,f,g,h,i,j,k=n.css(a,"position"),l=n(a),m={};"static"===k&&(a.style.position="relative"),h=l.offset(),f=n.css(a,"top"),i=n.css(a,"left"),j=("absolute"===k||"fixed"===k)&&n.inArray("auto",[f,i])>-1,j?(d=l.position(),g=d.top,e=d.left):(g=parseFloat(f)||0,e=parseFloat(i)||0),n.isFunction(b)&&(b=b.call(a,c,h)),null!=b.top&&(m.top=b.top-h.top+g),null!=b.left&&(m.left=b.left-h.left+e),"using"in b?b.using.call(a,m):l.css(m)}},n.fn.extend({offset:function(a){if(arguments.length)return void 0===a?this:this.each(function(b){n.offset.setOffset(this,a,b)});var b,c,d={top:0,left:0},e=this[0],f=e&&e.ownerDocument;if(f)return b=f.documentElement,n.contains(b,e)?(typeof e.getBoundingClientRect!==L&&(d=e.getBoundingClientRect()),c=ed(f),{top:d.top+(c.pageYOffset||b.scrollTop)-(b.clientTop||0),left:d.left+(c.pageXOffset||b.scrollLeft)-(b.clientLeft||0)}):d},position:function(){if(this[0]){var a,b,c={top:0,left:0},d=this[0];return"fixed"===n.css(d,"position")?b=d.getBoundingClientRect():(a=this.offsetParent(),b=this.offset(),n.nodeName(a[0],"html")||(c=a.offset()),c.top+=n.css(a[0],"borderTopWidth",!0),c.left+=n.css(a[0],"borderLeftWidth",!0)),{top:b.top-c.top-n.css(d,"marginTop",!0),left:b.left-c.left-n.css(d,"marginLeft",!0)}}},offsetParent:function(){return this.map(function(){var a=this.offsetParent||dd;while(a&&!n.nodeName(a,"html")&&"static"===n.css(a,"position"))a=a.offsetParent;return a||dd})}}),n.each({scrollLeft:"pageXOffset",scrollTop:"pageYOffset"},function(a,b){var c=/Y/.test(b);n.fn[a]=function(d){return W(this,function(a,d,e){var f=ed(a);return void 0===e?f?b in f?f[b]:f.document.documentElement[d]:a[d]:void(f?f.scrollTo(c?n(f).scrollLeft():e,c?e:n(f).scrollTop()):a[d]=e)},a,d,arguments.length,null)}}),n.each(["top","left"],function(a,b){n.cssHooks[b]=Mb(l.pixelPosition,function(a,c){return c?(c=Kb(a,b),Ib.test(c)?n(a).position()[b]+"px":c):void 0})}),n.each({Height:"height",Width:"width"},function(a,b){n.each({padding:"inner"+a,content:b,"":"outer"+a},function(c,d){n.fn[d]=function(d,e){var f=arguments.length&&(c||"boolean"!=typeof d),g=c||(d===!0||e===!0?"margin":"border");return W(this,function(b,c,d){var e;return n.isWindow(b)?b.document.documentElement["client"+a]:9===b.nodeType?(e=b.documentElement,Math.max(b.body["scroll"+a],e["scroll"+a],b.body["offset"+a],e["offset"+a],e["client"+a])):void 0===d?n.css(b,c,g):n.style(b,c,d,g)},b,f?d:void 0,f,null)}})}),n.fn.size=function(){return this.length},n.fn.andSelf=n.fn.addBack,"function"==typeof define&&define.amd&&define("jquery",[],function(){return n});var fd=a.jQuery,gd=a.$;return n.noConflict=function(b){return a.$===n&&(a.$=gd),b&&a.jQuery===n&&(a.jQuery=fd),n},typeof b===L&&(a.jQuery=a.$=n),n});
/*!
 * jQuery Form Plugin
 * version: 3.51.0-2014.06.20
 * Requires jQuery v1.5 or later
 * Copyright (c) 2014 M. Alsup
 * Examples and documentation at: http://malsup.com/jquery/form/
 * Project repository: https://github.com/malsup/form
 * Dual licensed under the MIT and GPL licenses.
 * https://github.com/malsup/form#copyright-and-license
 */
!function(e){"use strict";"function"==typeof define&&define.amd?define(["jquery"],e):e("undefined"!=typeof jQuery?jQuery:window.Zepto)}(function(e){"use strict";function t(t){var r=t.data;t.isDefaultPrevented()||(t.preventDefault(),e(t.target).ajaxSubmit(r))}function r(t){var r=t.target,a=e(r);if(!a.is("[type=submit],[type=image]")){var n=a.closest("[type=submit]");if(0===n.length)return;r=n[0]}var i=this;if(i.clk=r,"image"==r.type)if(void 0!==t.offsetX)i.clk_x=t.offsetX,i.clk_y=t.offsetY;else if("function"==typeof e.fn.offset){var o=a.offset();i.clk_x=t.pageX-o.left,i.clk_y=t.pageY-o.top}else i.clk_x=t.pageX-r.offsetLeft,i.clk_y=t.pageY-r.offsetTop;setTimeout(function(){i.clk=i.clk_x=i.clk_y=null},100)}function a(){if(e.fn.ajaxSubmit.debug){var t="[jquery.form] "+Array.prototype.join.call(arguments,"");window.console&&window.console.log?window.console.log(t):window.opera&&window.opera.postError&&window.opera.postError(t)}}var n={};n.fileapi=void 0!==e("<input type='file'/>").get(0).files,n.formdata=void 0!==window.FormData;var i=!!e.fn.prop;e.fn.attr2=function(){if(!i)return this.attr.apply(this,arguments);var e=this.prop.apply(this,arguments);return e&&e.jquery||"string"==typeof e?e:this.attr.apply(this,arguments)},e.fn.ajaxSubmit=function(t){function r(r){var a,n,i=e.param(r,t.traditional).split("&"),o=i.length,s=[];for(a=0;o>a;a++)i[a]=i[a].replace(/\+/g," "),n=i[a].split("="),s.push([decodeURIComponent(n[0]),decodeURIComponent(n[1])]);return s}function o(a){for(var n=new FormData,i=0;i<a.length;i++)n.append(a[i].name,a[i].value);if(t.extraData){var o=r(t.extraData);for(i=0;i<o.length;i++)o[i]&&n.append(o[i][0],o[i][1])}t.data=null;var s=e.extend(!0,{},e.ajaxSettings,t,{contentType:!1,processData:!1,cache:!1,type:u||"POST"});t.uploadProgress&&(s.xhr=function(){var r=e.ajaxSettings.xhr();return r.upload&&r.upload.addEventListener("progress",function(e){var r=0,a=e.loaded||e.position,n=e.total;e.lengthComputable&&(r=Math.ceil(a/n*100)),t.uploadProgress(e,a,n,r)},!1),r}),s.data=null;var c=s.beforeSend;return s.beforeSend=function(e,r){r.data=t.formData?t.formData:n,c&&c.call(this,e,r)},e.ajax(s)}function s(r){function n(e){var t=null;try{e.contentWindow&&(t=e.contentWindow.document)}catch(r){a("cannot get iframe.contentWindow document: "+r)}if(t)return t;try{t=e.contentDocument?e.contentDocument:e.document}catch(r){a("cannot get iframe.contentDocument: "+r),t=e.document}return t}function o(){function t(){try{var e=n(g).readyState;a("state = "+e),e&&"uninitialized"==e.toLowerCase()&&setTimeout(t,50)}catch(r){a("Server abort: ",r," (",r.name,")"),s(k),j&&clearTimeout(j),j=void 0}}var r=f.attr2("target"),i=f.attr2("action"),o="multipart/form-data",c=f.attr("enctype")||f.attr("encoding")||o;w.setAttribute("target",p),(!u||/post/i.test(u))&&w.setAttribute("method","POST"),i!=m.url&&w.setAttribute("action",m.url),m.skipEncodingOverride||u&&!/post/i.test(u)||f.attr({encoding:"multipart/form-data",enctype:"multipart/form-data"}),m.timeout&&(j=setTimeout(function(){T=!0,s(D)},m.timeout));var l=[];try{if(m.extraData)for(var d in m.extraData)m.extraData.hasOwnProperty(d)&&l.push(e.isPlainObject(m.extraData[d])&&m.extraData[d].hasOwnProperty("name")&&m.extraData[d].hasOwnProperty("value")?e('<input type="hidden" name="'+m.extraData[d].name+'">').val(m.extraData[d].value).appendTo(w)[0]:e('<input type="hidden" name="'+d+'">').val(m.extraData[d]).appendTo(w)[0]);m.iframeTarget||v.appendTo("body"),g.attachEvent?g.attachEvent("onload",s):g.addEventListener("load",s,!1),setTimeout(t,15);try{w.submit()}catch(h){var x=document.createElement("form").submit;x.apply(w)}}finally{w.setAttribute("action",i),w.setAttribute("enctype",c),r?w.setAttribute("target",r):f.removeAttr("target"),e(l).remove()}}function s(t){if(!x.aborted&&!F){if(M=n(g),M||(a("cannot access response document"),t=k),t===D&&x)return x.abort("timeout"),void S.reject(x,"timeout");if(t==k&&x)return x.abort("server abort"),void S.reject(x,"error","server abort");if(M&&M.location.href!=m.iframeSrc||T){g.detachEvent?g.detachEvent("onload",s):g.removeEventListener("load",s,!1);var r,i="success";try{if(T)throw"timeout";var o="xml"==m.dataType||M.XMLDocument||e.isXMLDoc(M);if(a("isXml="+o),!o&&window.opera&&(null===M.body||!M.body.innerHTML)&&--O)return a("requeing onLoad callback, DOM not available"),void setTimeout(s,250);var u=M.body?M.body:M.documentElement;x.responseText=u?u.innerHTML:null,x.responseXML=M.XMLDocument?M.XMLDocument:M,o&&(m.dataType="xml"),x.getResponseHeader=function(e){var t={"content-type":m.dataType};return t[e.toLowerCase()]},u&&(x.status=Number(u.getAttribute("status"))||x.status,x.statusText=u.getAttribute("statusText")||x.statusText);var c=(m.dataType||"").toLowerCase(),l=/(json|script|text)/.test(c);if(l||m.textarea){var f=M.getElementsByTagName("textarea")[0];if(f)x.responseText=f.value,x.status=Number(f.getAttribute("status"))||x.status,x.statusText=f.getAttribute("statusText")||x.statusText;else if(l){var p=M.getElementsByTagName("pre")[0],h=M.getElementsByTagName("body")[0];p?x.responseText=p.textContent?p.textContent:p.innerText:h&&(x.responseText=h.textContent?h.textContent:h.innerText)}}else"xml"==c&&!x.responseXML&&x.responseText&&(x.responseXML=X(x.responseText));try{E=_(x,c,m)}catch(y){i="parsererror",x.error=r=y||i}}catch(y){a("error caught: ",y),i="error",x.error=r=y||i}x.aborted&&(a("upload aborted"),i=null),x.status&&(i=x.status>=200&&x.status<300||304===x.status?"success":"error"),"success"===i?(m.success&&m.success.call(m.context,E,"success",x),S.resolve(x.responseText,"success",x),d&&e.event.trigger("ajaxSuccess",[x,m])):i&&(void 0===r&&(r=x.statusText),m.error&&m.error.call(m.context,x,i,r),S.reject(x,"error",r),d&&e.event.trigger("ajaxError",[x,m,r])),d&&e.event.trigger("ajaxComplete",[x,m]),d&&!--e.active&&e.event.trigger("ajaxStop"),m.complete&&m.complete.call(m.context,x,i),F=!0,m.timeout&&clearTimeout(j),setTimeout(function(){m.iframeTarget?v.attr("src",m.iframeSrc):v.remove(),x.responseXML=null},100)}}}var c,l,m,d,p,v,g,x,y,b,T,j,w=f[0],S=e.Deferred();if(S.abort=function(e){x.abort(e)},r)for(l=0;l<h.length;l++)c=e(h[l]),i?c.prop("disabled",!1):c.removeAttr("disabled");if(m=e.extend(!0,{},e.ajaxSettings,t),m.context=m.context||m,p="jqFormIO"+(new Date).getTime(),m.iframeTarget?(v=e(m.iframeTarget),b=v.attr2("name"),b?p=b:v.attr2("name",p)):(v=e('<iframe name="'+p+'" src="'+m.iframeSrc+'" />'),v.css({position:"absolute",top:"-1000px",left:"-1000px"})),g=v[0],x={aborted:0,responseText:null,responseXML:null,status:0,statusText:"n/a",getAllResponseHeaders:function(){},getResponseHeader:function(){},setRequestHeader:function(){},abort:function(t){var r="timeout"===t?"timeout":"aborted";a("aborting upload... "+r),this.aborted=1;try{g.contentWindow.document.execCommand&&g.contentWindow.document.execCommand("Stop")}catch(n){}v.attr("src",m.iframeSrc),x.error=r,m.error&&m.error.call(m.context,x,r,t),d&&e.event.trigger("ajaxError",[x,m,r]),m.complete&&m.complete.call(m.context,x,r)}},d=m.global,d&&0===e.active++&&e.event.trigger("ajaxStart"),d&&e.event.trigger("ajaxSend",[x,m]),m.beforeSend&&m.beforeSend.call(m.context,x,m)===!1)return m.global&&e.active--,S.reject(),S;if(x.aborted)return S.reject(),S;y=w.clk,y&&(b=y.name,b&&!y.disabled&&(m.extraData=m.extraData||{},m.extraData[b]=y.value,"image"==y.type&&(m.extraData[b+".x"]=w.clk_x,m.extraData[b+".y"]=w.clk_y)));var D=1,k=2,A=e("meta[name=csrf-token]").attr("content"),L=e("meta[name=csrf-param]").attr("content");L&&A&&(m.extraData=m.extraData||{},m.extraData[L]=A),m.forceSync?o():setTimeout(o,10);var E,M,F,O=50,X=e.parseXML||function(e,t){return window.ActiveXObject?(t=new ActiveXObject("Microsoft.XMLDOM"),t.async="false",t.loadXML(e)):t=(new DOMParser).parseFromString(e,"text/xml"),t&&t.documentElement&&"parsererror"!=t.documentElement.nodeName?t:null},C=e.parseJSON||function(e){return window.eval("("+e+")")},_=function(t,r,a){var n=t.getResponseHeader("content-type")||"",i="xml"===r||!r&&n.indexOf("xml")>=0,o=i?t.responseXML:t.responseText;return i&&"parsererror"===o.documentElement.nodeName&&e.error&&e.error("parsererror"),a&&a.dataFilter&&(o=a.dataFilter(o,r)),"string"==typeof o&&("json"===r||!r&&n.indexOf("json")>=0?o=C(o):("script"===r||!r&&n.indexOf("javascript")>=0)&&e.globalEval(o)),o};return S}if(!this.length)return a("ajaxSubmit: skipping submit process - no element selected"),this;var u,c,l,f=this;"function"==typeof t?t={success:t}:void 0===t&&(t={}),u=t.type||this.attr2("method"),c=t.url||this.attr2("action"),l="string"==typeof c?e.trim(c):"",l=l||window.location.href||"",l&&(l=(l.match(/^([^#]+)/)||[])[1]),t=e.extend(!0,{url:l,success:e.ajaxSettings.success,type:u||e.ajaxSettings.type,iframeSrc:/^https/i.test(window.location.href||"")?"javascript:false":"about:blank"},t);var m={};if(this.trigger("form-pre-serialize",[this,t,m]),m.veto)return a("ajaxSubmit: submit vetoed via form-pre-serialize trigger"),this;if(t.beforeSerialize&&t.beforeSerialize(this,t)===!1)return a("ajaxSubmit: submit aborted via beforeSerialize callback"),this;var d=t.traditional;void 0===d&&(d=e.ajaxSettings.traditional);var p,h=[],v=this.formToArray(t.semantic,h);if(t.data&&(t.extraData=t.data,p=e.param(t.data,d)),t.beforeSubmit&&t.beforeSubmit(v,this,t)===!1)return a("ajaxSubmit: submit aborted via beforeSubmit callback"),this;if(this.trigger("form-submit-validate",[v,this,t,m]),m.veto)return a("ajaxSubmit: submit vetoed via form-submit-validate trigger"),this;var g=e.param(v,d);p&&(g=g?g+"&"+p:p),"GET"==t.type.toUpperCase()?(t.url+=(t.url.indexOf("?")>=0?"&":"?")+g,t.data=null):t.data=g;var x=[];if(t.resetForm&&x.push(function(){f.resetForm()}),t.clearForm&&x.push(function(){f.clearForm(t.includeHidden)}),!t.dataType&&t.target){var y=t.success||function(){};x.push(function(r){var a=t.replaceTarget?"replaceWith":"html";e(t.target)[a](r).each(y,arguments)})}else t.success&&x.push(t.success);if(t.success=function(e,r,a){for(var n=t.context||this,i=0,o=x.length;o>i;i++)x[i].apply(n,[e,r,a||f,f])},t.error){var b=t.error;t.error=function(e,r,a){var n=t.context||this;b.apply(n,[e,r,a,f])}}if(t.complete){var T=t.complete;t.complete=function(e,r){var a=t.context||this;T.apply(a,[e,r,f])}}var j=e("input[type=file]:enabled",this).filter(function(){return""!==e(this).val()}),w=j.length>0,S="multipart/form-data",D=f.attr("enctype")==S||f.attr("encoding")==S,k=n.fileapi&&n.formdata;a("fileAPI :"+k);var A,L=(w||D)&&!k;t.iframe!==!1&&(t.iframe||L)?t.closeKeepAlive?e.get(t.closeKeepAlive,function(){A=s(v)}):A=s(v):A=(w||D)&&k?o(v):e.ajax(t),f.removeData("jqxhr").data("jqxhr",A);for(var E=0;E<h.length;E++)h[E]=null;return this.trigger("form-submit-notify",[this,t]),this},e.fn.ajaxForm=function(n){if(n=n||{},n.delegation=n.delegation&&e.isFunction(e.fn.on),!n.delegation&&0===this.length){var i={s:this.selector,c:this.context};return!e.isReady&&i.s?(a("DOM not ready, queuing ajaxForm"),e(function(){e(i.s,i.c).ajaxForm(n)}),this):(a("terminating; zero elements found by selector"+(e.isReady?"":" (DOM not ready)")),this)}return n.delegation?(e(document).off("submit.form-plugin",this.selector,t).off("click.form-plugin",this.selector,r).on("submit.form-plugin",this.selector,n,t).on("click.form-plugin",this.selector,n,r),this):this.ajaxFormUnbind().bind("submit.form-plugin",n,t).bind("click.form-plugin",n,r)},e.fn.ajaxFormUnbind=function(){return this.unbind("submit.form-plugin click.form-plugin")},e.fn.formToArray=function(t,r){var a=[];if(0===this.length)return a;var i,o=this[0],s=this.attr("id"),u=t?o.getElementsByTagName("*"):o.elements;if(u&&!/MSIE [678]/.test(navigator.userAgent)&&(u=e(u).get()),s&&(i=e(':input[form="'+s+'"]').get(),i.length&&(u=(u||[]).concat(i))),!u||!u.length)return a;var c,l,f,m,d,p,h;for(c=0,p=u.length;p>c;c++)if(d=u[c],f=d.name,f&&!d.disabled)if(t&&o.clk&&"image"==d.type)o.clk==d&&(a.push({name:f,value:e(d).val(),type:d.type}),a.push({name:f+".x",value:o.clk_x},{name:f+".y",value:o.clk_y}));else if(m=e.fieldValue(d,!0),m&&m.constructor==Array)for(r&&r.push(d),l=0,h=m.length;h>l;l++)a.push({name:f,value:m[l]});else if(n.fileapi&&"file"==d.type){r&&r.push(d);var v=d.files;if(v.length)for(l=0;l<v.length;l++)a.push({name:f,value:v[l],type:d.type});else a.push({name:f,value:"",type:d.type})}else null!==m&&"undefined"!=typeof m&&(r&&r.push(d),a.push({name:f,value:m,type:d.type,required:d.required}));if(!t&&o.clk){var g=e(o.clk),x=g[0];f=x.name,f&&!x.disabled&&"image"==x.type&&(a.push({name:f,value:g.val()}),a.push({name:f+".x",value:o.clk_x},{name:f+".y",value:o.clk_y}))}return a},e.fn.formSerialize=function(t){return e.param(this.formToArray(t))},e.fn.fieldSerialize=function(t){var r=[];return this.each(function(){var a=this.name;if(a){var n=e.fieldValue(this,t);if(n&&n.constructor==Array)for(var i=0,o=n.length;o>i;i++)r.push({name:a,value:n[i]});else null!==n&&"undefined"!=typeof n&&r.push({name:this.name,value:n})}}),e.param(r)},e.fn.fieldValue=function(t){for(var r=[],a=0,n=this.length;n>a;a++){var i=this[a],o=e.fieldValue(i,t);null===o||"undefined"==typeof o||o.constructor==Array&&!o.length||(o.constructor==Array?e.merge(r,o):r.push(o))}return r},e.fieldValue=function(t,r){var a=t.name,n=t.type,i=t.tagName.toLowerCase();if(void 0===r&&(r=!0),r&&(!a||t.disabled||"reset"==n||"button"==n||("checkbox"==n||"radio"==n)&&!t.checked||("submit"==n||"image"==n)&&t.form&&t.form.clk!=t||"select"==i&&-1==t.selectedIndex))return null;if("select"==i){var o=t.selectedIndex;if(0>o)return null;for(var s=[],u=t.options,c="select-one"==n,l=c?o+1:u.length,f=c?o:0;l>f;f++){var m=u[f];if(m.selected){var d=m.value;if(d||(d=m.attributes&&m.attributes.value&&!m.attributes.value.specified?m.text:m.value),c)return d;s.push(d)}}return s}return e(t).val()},e.fn.clearForm=function(t){return this.each(function(){e("input,select,textarea",this).clearFields(t)})},e.fn.clearFields=e.fn.clearInputs=function(t){var r=/^(?:color|date|datetime|email|month|number|password|range|search|tel|text|time|url|week)$/i;return this.each(function(){var a=this.type,n=this.tagName.toLowerCase();r.test(a)||"textarea"==n?this.value="":"checkbox"==a||"radio"==a?this.checked=!1:"select"==n?this.selectedIndex=-1:"file"==a?/MSIE/.test(navigator.userAgent)?e(this).replaceWith(e(this).clone(!0)):e(this).val(""):t&&(t===!0&&/hidden/.test(a)||"string"==typeof t&&e(this).is(t))&&(this.value="")})},e.fn.resetForm=function(){return this.each(function(){("function"==typeof this.reset||"object"==typeof this.reset&&!this.reset.nodeType)&&this.reset()})},e.fn.enable=function(e){return void 0===e&&(e=!0),this.each(function(){this.disabled=!e})},e.fn.selected=function(t){return void 0===t&&(t=!0),this.each(function(){var r=this.type;if("checkbox"==r||"radio"==r)this.checked=t;else if("option"==this.tagName.toLowerCase()){var a=e(this).parent("select");t&&a[0]&&"select-one"==a[0].type&&a.find("option").selected(!1),this.selected=t}})},e.fn.ajaxSubmit.debug=!1});
/*! jQuery UI - v1.12.1 - 2018-06-06
* http://jqueryui.com
* Includes: keycode.js, widgets/datepicker.js
* Copyright jQuery Foundation and other contributors; Licensed MIT */

(function( factory ) {
	if ( typeof define === "function" && define.amd ) {

		// AMD. Register as an anonymous module.
		define([ "jquery" ], factory );
	} else {

		// Browser globals
		factory( jQuery );
	}
}(function( $ ) {

$.ui = $.ui || {};

var version = $.ui.version = "1.12.1";


/*!
 * jQuery UI Keycode 1.12.1
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 */

//>>label: Keycode
//>>group: Core
//>>description: Provide keycodes as keynames
//>>docs: http://api.jqueryui.com/jQuery.ui.keyCode/


var keycode = $.ui.keyCode = {
	BACKSPACE: 8,
	COMMA: 188,
	DELETE: 46,
	DOWN: 40,
	END: 35,
	ENTER: 13,
	ESCAPE: 27,
	HOME: 36,
	LEFT: 37,
	PAGE_DOWN: 34,
	PAGE_UP: 33,
	PERIOD: 190,
	RIGHT: 39,
	SPACE: 32,
	TAB: 9,
	UP: 38
};


// jscs:disable maximumLineLength
/* jscs:disable requireCamelCaseOrUpperCaseIdentifiers */
/*!
 * jQuery UI Datepicker 1.12.1
 * http://jqueryui.com
 *
 * Copyright jQuery Foundation and other contributors
 * Released under the MIT license.
 * http://jquery.org/license
 */

//>>label: Datepicker
//>>group: Widgets
//>>description: Displays a calendar from an input or inline for selecting dates.
//>>docs: http://api.jqueryui.com/datepicker/
//>>demos: http://jqueryui.com/datepicker/
//>>css.structure: ../../themes/base/core.css
//>>css.structure: ../../themes/base/datepicker.css
//>>css.theme: ../../themes/base/theme.css



$.extend( $.ui, { datepicker: { version: "1.12.1" } } );

var datepicker_instActive;

function datepicker_getZindex( elem ) {
	var position, value;
	while ( elem.length && elem[ 0 ] !== document ) {

		// Ignore z-index if position is set to a value where z-index is ignored by the browser
		// This makes behavior of this function consistent across browsers
		// WebKit always returns auto if the element is positioned
		position = elem.css( "position" );
		if ( position === "absolute" || position === "relative" || position === "fixed" ) {

			// IE returns 0 when zIndex is not specified
			// other browsers return a string
			// we ignore the case of nested elements with an explicit value of 0
			// <div style="z-index: -10;"><div style="z-index: 0;"></div></div>
			value = parseInt( elem.css( "zIndex" ), 10 );
			if ( !isNaN( value ) && value !== 0 ) {
				return value;
			}
		}
		elem = elem.parent();
	}

	return 0;
}
/* Date picker manager.
   Use the singleton instance of this class, $.datepicker, to interact with the date picker.
   Settings for (groups of) date pickers are maintained in an instance object,
   allowing multiple different settings on the same page. */

function Datepicker() {
	this._curInst = null; // The current instance in use
	this._keyEvent = false; // If the last event was a key event
	this._disabledInputs = []; // List of date picker inputs that have been disabled
	this._datepickerShowing = false; // True if the popup picker is showing , false if not
	this._inDialog = false; // True if showing within a "dialog", false if not
	this._mainDivId = "ui-datepicker-div"; // The ID of the main datepicker division
	this._inlineClass = "ui-datepicker-inline"; // The name of the inline marker class
	this._appendClass = "ui-datepicker-append"; // The name of the append marker class
	this._triggerClass = "ui-datepicker-trigger"; // The name of the trigger marker class
	this._dialogClass = "ui-datepicker-dialog"; // The name of the dialog marker class
	this._disableClass = "ui-datepicker-disabled"; // The name of the disabled covering marker class
	this._unselectableClass = "ui-datepicker-unselectable"; // The name of the unselectable cell marker class
	this._currentClass = "ui-datepicker-current-day"; // The name of the current day marker class
	this._dayOverClass = "ui-datepicker-days-cell-over"; // The name of the day hover marker class
	this.regional = []; // Available regional settings, indexed by language code
	this.regional[ "" ] = { // Default regional settings
		closeText: "Done", // Display text for close link
		prevText: "Prev", // Display text for previous month link
		nextText: "Next", // Display text for next month link
		currentText: "Today", // Display text for current month link
		monthNames: [ "January","February","March","April","May","June",
			"July","August","September","October","November","December" ], // Names of months for drop-down and formatting
		monthNamesShort: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ], // For formatting
		dayNames: [ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" ], // For formatting
		dayNamesShort: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ], // For formatting
		dayNamesMin: [ "Su","Mo","Tu","We","Th","Fr","Sa" ], // Column headings for days starting at Sunday
		weekHeader: "Wk", // Column header for week of the year
		dateFormat: "mm/dd/yy", // See format options on parseDate
		firstDay: 0, // The first day of the week, Sun = 0, Mon = 1, ...
		isRTL: false, // True if right-to-left language, false if left-to-right
		showMonthAfterYear: false, // True if the year select precedes month, false for month then year
		yearSuffix: "" // Additional text to append to the year in the month headers
	};
	this._defaults = { // Global defaults for all the date picker instances
		showOn: "focus", // "focus" for popup on focus,
			// "button" for trigger button, or "both" for either
		showAnim: "fadeIn", // Name of jQuery animation for popup
		showOptions: {}, // Options for enhanced animations
		defaultDate: null, // Used when field is blank: actual date,
			// +/-number for offset from today, null for today
		appendText: "", // Display text following the input box, e.g. showing the format
		buttonText: "...", // Text for trigger button
		buttonImage: "", // URL for trigger button image
		buttonImageOnly: false, // True if the image appears alone, false if it appears on a button
		hideIfNoPrevNext: false, // True to hide next/previous month links
			// if not applicable, false to just disable them
		navigationAsDateFormat: false, // True if date formatting applied to prev/today/next links
		gotoCurrent: false, // True if today link goes back to current selection instead
		changeMonth: false, // True if month can be selected directly, false if only prev/next
		changeYear: false, // True if year can be selected directly, false if only prev/next
		yearRange: "c-10:c+10", // Range of years to display in drop-down,
			// either relative to today's year (-nn:+nn), relative to currently displayed year
			// (c-nn:c+nn), absolute (nnnn:nnnn), or a combination of the above (nnnn:-n)
		showOtherMonths: false, // True to show dates in other months, false to leave blank
		selectOtherMonths: false, // True to allow selection of dates in other months, false for unselectable
		showWeek: false, // True to show week of the year, false to not show it
		calculateWeek: this.iso8601Week, // How to calculate the week of the year,
			// takes a Date and returns the number of the week for it
		shortYearCutoff: "+10", // Short year values < this are in the current century,
			// > this are in the previous century,
			// string value starting with "+" for current year + value
		minDate: null, // The earliest selectable date, or null for no limit
		maxDate: null, // The latest selectable date, or null for no limit
		duration: "fast", // Duration of display/closure
		beforeShowDay: null, // Function that takes a date and returns an array with
			// [0] = true if selectable, false if not, [1] = custom CSS class name(s) or "",
			// [2] = cell title (optional), e.g. $.datepicker.noWeekends
		beforeShow: null, // Function that takes an input field and
			// returns a set of custom settings for the date picker
		onSelect: null, // Define a callback function when a date is selected
		onChangeMonthYear: null, // Define a callback function when the month or year is changed
		onClose: null, // Define a callback function when the datepicker is closed
		numberOfMonths: 1, // Number of months to show at a time
		showCurrentAtPos: 0, // The position in multipe months at which to show the current month (starting at 0)
		stepMonths: 1, // Number of months to step back/forward
		stepBigMonths: 12, // Number of months to step back/forward for the big links
		altField: "", // Selector for an alternate field to store selected dates into
		altFormat: "", // The date format to use for the alternate field
		constrainInput: true, // The input is constrained by the current date format
		showButtonPanel: false, // True to show button panel, false to not show it
		autoSize: false, // True to size the input for the date format, false to leave as is
		disabled: false // The initial disabled state
	};
	$.extend( this._defaults, this.regional[ "" ] );
	this.regional.en = $.extend( true, {}, this.regional[ "" ] );
	this.regional[ "en-US" ] = $.extend( true, {}, this.regional.en );
	this.dpDiv = datepicker_bindHover( $( "<div id='" + this._mainDivId + "' class='ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>" ) );
}

$.extend( Datepicker.prototype, {
	/* Class name added to elements to indicate already configured with a date picker. */
	markerClassName: "hasDatepicker",

	//Keep track of the maximum number of rows displayed (see #7043)
	maxRows: 4,

	// TODO rename to "widget" when switching to widget factory
	_widgetDatepicker: function() {
		return this.dpDiv;
	},

	/* Override the default settings for all instances of the date picker.
	 * @param  settings  object - the new settings to use as defaults (anonymous object)
	 * @return the manager object
	 */
	setDefaults: function( settings ) {
		datepicker_extendRemove( this._defaults, settings || {} );
		return this;
	},

	/* Attach the date picker to a jQuery selection.
	 * @param  target	element - the target input field or division or span
	 * @param  settings  object - the new settings to use for this date picker instance (anonymous)
	 */
	_attachDatepicker: function( target, settings ) {
		var nodeName, inline, inst;
		nodeName = target.nodeName.toLowerCase();
		inline = ( nodeName === "div" || nodeName === "span" );
		if ( !target.id ) {
			this.uuid += 1;
			target.id = "dp" + this.uuid;
		}
		inst = this._newInst( $( target ), inline );
		inst.settings = $.extend( {}, settings || {} );
		if ( nodeName === "input" ) {
			this._connectDatepicker( target, inst );
		} else if ( inline ) {
			this._inlineDatepicker( target, inst );
		}
	},

	/* Create a new instance object. */
	_newInst: function( target, inline ) {
		var id = target[ 0 ].id.replace( /([^A-Za-z0-9_\-])/g, "\\\\$1" ); // escape jQuery meta chars
		return { id: id, input: target, // associated target
			selectedDay: 0, selectedMonth: 0, selectedYear: 0, // current selection
			drawMonth: 0, drawYear: 0, // month being drawn
			inline: inline, // is datepicker inline or not
			dpDiv: ( !inline ? this.dpDiv : // presentation div
			datepicker_bindHover( $( "<div class='" + this._inlineClass + " ui-datepicker ui-widget ui-widget-content ui-helper-clearfix ui-corner-all'></div>" ) ) ) };
	},

	/* Attach the date picker to an input field. */
	_connectDatepicker: function( target, inst ) {
		var input = $( target );
		inst.append = $( [] );
		inst.trigger = $( [] );
		if ( input.hasClass( this.markerClassName ) ) {
			return;
		}
		this._attachments( input, inst );
		input.addClass( this.markerClassName ).on( "keydown", this._doKeyDown ).
			on( "keypress", this._doKeyPress ).on( "keyup", this._doKeyUp );
		this._autoSize( inst );
		$.data( target, "datepicker", inst );

		//If disabled option is true, disable the datepicker once it has been attached to the input (see ticket #5665)
		if ( inst.settings.disabled ) {
			this._disableDatepicker( target );
		}
	},

	/* Make attachments based on settings. */
	_attachments: function( input, inst ) {
		var showOn, buttonText, buttonImage,
			appendText = this._get( inst, "appendText" ),
			isRTL = this._get( inst, "isRTL" );

		if ( inst.append ) {
			inst.append.remove();
		}
		if ( appendText ) {
			inst.append = $( "<span class='" + this._appendClass + "'>" + appendText + "</span>" );
			input[ isRTL ? "before" : "after" ]( inst.append );
		}

		input.off( "focus", this._showDatepicker );

		if ( inst.trigger ) {
			inst.trigger.remove();
		}

		showOn = this._get( inst, "showOn" );
		if ( showOn === "focus" || showOn === "both" ) { // pop-up date picker when in the marked field
			input.on( "focus", this._showDatepicker );
		}
		if ( showOn === "button" || showOn === "both" ) { // pop-up date picker when button clicked
			buttonText = this._get( inst, "buttonText" );
			buttonImage = this._get( inst, "buttonImage" );
			inst.trigger = $( this._get( inst, "buttonImageOnly" ) ?
				$( "<img/>" ).addClass( this._triggerClass ).
					attr( { src: buttonImage, alt: buttonText, title: buttonText } ) :
				$( "<button type='button'></button>" ).addClass( this._triggerClass ).
					html( !buttonImage ? buttonText : $( "<img/>" ).attr(
					{ src:buttonImage, alt:buttonText, title:buttonText } ) ) );
			input[ isRTL ? "before" : "after" ]( inst.trigger );
			inst.trigger.on( "click", function() {
				if ( $.datepicker._datepickerShowing && $.datepicker._lastInput === input[ 0 ] ) {
					$.datepicker._hideDatepicker();
				} else if ( $.datepicker._datepickerShowing && $.datepicker._lastInput !== input[ 0 ] ) {
					$.datepicker._hideDatepicker();
					$.datepicker._showDatepicker( input[ 0 ] );
				} else {
					$.datepicker._showDatepicker( input[ 0 ] );
				}
				return false;
			} );
		}
	},

	/* Apply the maximum length for the date format. */
	_autoSize: function( inst ) {
		if ( this._get( inst, "autoSize" ) && !inst.inline ) {
			var findMax, max, maxI, i,
				date = new Date( 2009, 12 - 1, 20 ), // Ensure double digits
				dateFormat = this._get( inst, "dateFormat" );

			if ( dateFormat.match( /[DM]/ ) ) {
				findMax = function( names ) {
					max = 0;
					maxI = 0;
					for ( i = 0; i < names.length; i++ ) {
						if ( names[ i ].length > max ) {
							max = names[ i ].length;
							maxI = i;
						}
					}
					return maxI;
				};
				date.setMonth( findMax( this._get( inst, ( dateFormat.match( /MM/ ) ?
					"monthNames" : "monthNamesShort" ) ) ) );
				date.setDate( findMax( this._get( inst, ( dateFormat.match( /DD/ ) ?
					"dayNames" : "dayNamesShort" ) ) ) + 20 - date.getDay() );
			}
			inst.input.attr( "size", this._formatDate( inst, date ).length );
		}
	},

	/* Attach an inline date picker to a div. */
	_inlineDatepicker: function( target, inst ) {
		var divSpan = $( target );
		if ( divSpan.hasClass( this.markerClassName ) ) {
			return;
		}
		divSpan.addClass( this.markerClassName ).append( inst.dpDiv );
		$.data( target, "datepicker", inst );
		this._setDate( inst, this._getDefaultDate( inst ), true );
		this._updateDatepicker( inst );
		this._updateAlternate( inst );

		//If disabled option is true, disable the datepicker before showing it (see ticket #5665)
		if ( inst.settings.disabled ) {
			this._disableDatepicker( target );
		}

		// Set display:block in place of inst.dpDiv.show() which won't work on disconnected elements
		// http://bugs.jqueryui.com/ticket/7552 - A Datepicker created on a detached div has zero height
		inst.dpDiv.css( "display", "block" );
	},

	/* Pop-up the date picker in a "dialog" box.
	 * @param  input element - ignored
	 * @param  date	string or Date - the initial date to display
	 * @param  onSelect  function - the function to call when a date is selected
	 * @param  settings  object - update the dialog date picker instance's settings (anonymous object)
	 * @param  pos int[2] - coordinates for the dialog's position within the screen or
	 *					event - with x/y coordinates or
	 *					leave empty for default (screen centre)
	 * @return the manager object
	 */
	_dialogDatepicker: function( input, date, onSelect, settings, pos ) {
		var id, browserWidth, browserHeight, scrollX, scrollY,
			inst = this._dialogInst; // internal instance

		if ( !inst ) {
			this.uuid += 1;
			id = "dp" + this.uuid;
			this._dialogInput = $( "<input type='text' id='" + id +
				"' style='position: absolute; top: -100px; width: 0px;'/>" );
			this._dialogInput.on( "keydown", this._doKeyDown );
			$( "body" ).append( this._dialogInput );
			inst = this._dialogInst = this._newInst( this._dialogInput, false );
			inst.settings = {};
			$.data( this._dialogInput[ 0 ], "datepicker", inst );
		}
		datepicker_extendRemove( inst.settings, settings || {} );
		date = ( date && date.constructor === Date ? this._formatDate( inst, date ) : date );
		this._dialogInput.val( date );

		this._pos = ( pos ? ( pos.length ? pos : [ pos.pageX, pos.pageY ] ) : null );
		if ( !this._pos ) {
			browserWidth = document.documentElement.clientWidth;
			browserHeight = document.documentElement.clientHeight;
			scrollX = document.documentElement.scrollLeft || document.body.scrollLeft;
			scrollY = document.documentElement.scrollTop || document.body.scrollTop;
			this._pos = // should use actual width/height below
				[ ( browserWidth / 2 ) - 100 + scrollX, ( browserHeight / 2 ) - 150 + scrollY ];
		}

		// Move input on screen for focus, but hidden behind dialog
		this._dialogInput.css( "left", ( this._pos[ 0 ] + 20 ) + "px" ).css( "top", this._pos[ 1 ] + "px" );
		inst.settings.onSelect = onSelect;
		this._inDialog = true;
		this.dpDiv.addClass( this._dialogClass );
		this._showDatepicker( this._dialogInput[ 0 ] );
		if ( $.blockUI ) {
			$.blockUI( this.dpDiv );
		}
		$.data( this._dialogInput[ 0 ], "datepicker", inst );
		return this;
	},

	/* Detach a datepicker from its control.
	 * @param  target	element - the target input field or division or span
	 */
	_destroyDatepicker: function( target ) {
		var nodeName,
			$target = $( target ),
			inst = $.data( target, "datepicker" );

		if ( !$target.hasClass( this.markerClassName ) ) {
			return;
		}

		nodeName = target.nodeName.toLowerCase();
		$.removeData( target, "datepicker" );
		if ( nodeName === "input" ) {
			inst.append.remove();
			inst.trigger.remove();
			$target.removeClass( this.markerClassName ).
				off( "focus", this._showDatepicker ).
				off( "keydown", this._doKeyDown ).
				off( "keypress", this._doKeyPress ).
				off( "keyup", this._doKeyUp );
		} else if ( nodeName === "div" || nodeName === "span" ) {
			$target.removeClass( this.markerClassName ).empty();
		}

		if ( datepicker_instActive === inst ) {
			datepicker_instActive = null;
		}
	},

	/* Enable the date picker to a jQuery selection.
	 * @param  target	element - the target input field or division or span
	 */
	_enableDatepicker: function( target ) {
		var nodeName, inline,
			$target = $( target ),
			inst = $.data( target, "datepicker" );

		if ( !$target.hasClass( this.markerClassName ) ) {
			return;
		}

		nodeName = target.nodeName.toLowerCase();
		if ( nodeName === "input" ) {
			target.disabled = false;
			inst.trigger.filter( "button" ).
				each( function() { this.disabled = false; } ).end().
				filter( "img" ).css( { opacity: "1.0", cursor: "" } );
		} else if ( nodeName === "div" || nodeName === "span" ) {
			inline = $target.children( "." + this._inlineClass );
			inline.children().removeClass( "ui-state-disabled" );
			inline.find( "select.ui-datepicker-month, select.ui-datepicker-year" ).
				prop( "disabled", false );
		}
		this._disabledInputs = $.map( this._disabledInputs,
			function( value ) { return ( value === target ? null : value ); } ); // delete entry
	},

	/* Disable the date picker to a jQuery selection.
	 * @param  target	element - the target input field or division or span
	 */
	_disableDatepicker: function( target ) {
		var nodeName, inline,
			$target = $( target ),
			inst = $.data( target, "datepicker" );

		if ( !$target.hasClass( this.markerClassName ) ) {
			return;
		}

		nodeName = target.nodeName.toLowerCase();
		if ( nodeName === "input" ) {
			target.disabled = true;
			inst.trigger.filter( "button" ).
				each( function() { this.disabled = true; } ).end().
				filter( "img" ).css( { opacity: "0.5", cursor: "default" } );
		} else if ( nodeName === "div" || nodeName === "span" ) {
			inline = $target.children( "." + this._inlineClass );
			inline.children().addClass( "ui-state-disabled" );
			inline.find( "select.ui-datepicker-month, select.ui-datepicker-year" ).
				prop( "disabled", true );
		}
		this._disabledInputs = $.map( this._disabledInputs,
			function( value ) { return ( value === target ? null : value ); } ); // delete entry
		this._disabledInputs[ this._disabledInputs.length ] = target;
	},

	/* Is the first field in a jQuery collection disabled as a datepicker?
	 * @param  target	element - the target input field or division or span
	 * @return boolean - true if disabled, false if enabled
	 */
	_isDisabledDatepicker: function( target ) {
		if ( !target ) {
			return false;
		}
		for ( var i = 0; i < this._disabledInputs.length; i++ ) {
			if ( this._disabledInputs[ i ] === target ) {
				return true;
			}
		}
		return false;
	},

	/* Retrieve the instance data for the target control.
	 * @param  target  element - the target input field or division or span
	 * @return  object - the associated instance data
	 * @throws  error if a jQuery problem getting data
	 */
	_getInst: function( target ) {
		try {
			return $.data( target, "datepicker" );
		}
		catch ( err ) {
			throw "Missing instance data for this datepicker";
		}
	},

	/* Update or retrieve the settings for a date picker attached to an input field or division.
	 * @param  target  element - the target input field or division or span
	 * @param  name	object - the new settings to update or
	 *				string - the name of the setting to change or retrieve,
	 *				when retrieving also "all" for all instance settings or
	 *				"defaults" for all global defaults
	 * @param  value   any - the new value for the setting
	 *				(omit if above is an object or to retrieve a value)
	 */
	_optionDatepicker: function( target, name, value ) {
		var settings, date, minDate, maxDate,
			inst = this._getInst( target );

		if ( arguments.length === 2 && typeof name === "string" ) {
			return ( name === "defaults" ? $.extend( {}, $.datepicker._defaults ) :
				( inst ? ( name === "all" ? $.extend( {}, inst.settings ) :
				this._get( inst, name ) ) : null ) );
		}

		settings = name || {};
		if ( typeof name === "string" ) {
			settings = {};
			settings[ name ] = value;
		}

		if ( inst ) {
			if ( this._curInst === inst ) {
				this._hideDatepicker();
			}

			date = this._getDateDatepicker( target, true );
			minDate = this._getMinMaxDate( inst, "min" );
			maxDate = this._getMinMaxDate( inst, "max" );
			datepicker_extendRemove( inst.settings, settings );

			// reformat the old minDate/maxDate values if dateFormat changes and a new minDate/maxDate isn't provided
			if ( minDate !== null && settings.dateFormat !== undefined && settings.minDate === undefined ) {
				inst.settings.minDate = this._formatDate( inst, minDate );
			}
			if ( maxDate !== null && settings.dateFormat !== undefined && settings.maxDate === undefined ) {
				inst.settings.maxDate = this._formatDate( inst, maxDate );
			}
			if ( "disabled" in settings ) {
				if ( settings.disabled ) {
					this._disableDatepicker( target );
				} else {
					this._enableDatepicker( target );
				}
			}
			this._attachments( $( target ), inst );
			this._autoSize( inst );
			this._setDate( inst, date );
			this._updateAlternate( inst );
			this._updateDatepicker( inst );
		}
	},

	// Change method deprecated
	_changeDatepicker: function( target, name, value ) {
		this._optionDatepicker( target, name, value );
	},

	/* Redraw the date picker attached to an input field or division.
	 * @param  target  element - the target input field or division or span
	 */
	_refreshDatepicker: function( target ) {
		var inst = this._getInst( target );
		if ( inst ) {
			this._updateDatepicker( inst );
		}
	},

	/* Set the dates for a jQuery selection.
	 * @param  target element - the target input field or division or span
	 * @param  date	Date - the new date
	 */
	_setDateDatepicker: function( target, date ) {
		var inst = this._getInst( target );
		if ( inst ) {
			this._setDate( inst, date );
			this._updateDatepicker( inst );
			this._updateAlternate( inst );
		}
	},

	/* Get the date(s) for the first entry in a jQuery selection.
	 * @param  target element - the target input field or division or span
	 * @param  noDefault boolean - true if no default date is to be used
	 * @return Date - the current date
	 */
	_getDateDatepicker: function( target, noDefault ) {
		var inst = this._getInst( target );
		if ( inst && !inst.inline ) {
			this._setDateFromField( inst, noDefault );
		}
		return ( inst ? this._getDate( inst ) : null );
	},

	/* Handle keystrokes. */
	_doKeyDown: function( event ) {
		var onSelect, dateStr, sel,
			inst = $.datepicker._getInst( event.target ),
			handled = true,
			isRTL = inst.dpDiv.is( ".ui-datepicker-rtl" );

		inst._keyEvent = true;
		if ( $.datepicker._datepickerShowing ) {
			switch ( event.keyCode ) {
				case 9: $.datepicker._hideDatepicker();
						handled = false;
						break; // hide on tab out
				case 13: sel = $( "td." + $.datepicker._dayOverClass + ":not(." +
									$.datepicker._currentClass + ")", inst.dpDiv );
						if ( sel[ 0 ] ) {
							$.datepicker._selectDay( event.target, inst.selectedMonth, inst.selectedYear, sel[ 0 ] );
						}

						onSelect = $.datepicker._get( inst, "onSelect" );
						if ( onSelect ) {
							dateStr = $.datepicker._formatDate( inst );

							// Trigger custom callback
							onSelect.apply( ( inst.input ? inst.input[ 0 ] : null ), [ dateStr, inst ] );
						} else {
							$.datepicker._hideDatepicker();
						}

						return false; // don't submit the form
				case 27: $.datepicker._hideDatepicker();
						break; // hide on escape
				case 33: $.datepicker._adjustDate( event.target, ( event.ctrlKey ?
							-$.datepicker._get( inst, "stepBigMonths" ) :
							-$.datepicker._get( inst, "stepMonths" ) ), "M" );
						break; // previous month/year on page up/+ ctrl
				case 34: $.datepicker._adjustDate( event.target, ( event.ctrlKey ?
							+$.datepicker._get( inst, "stepBigMonths" ) :
							+$.datepicker._get( inst, "stepMonths" ) ), "M" );
						break; // next month/year on page down/+ ctrl
				case 35: if ( event.ctrlKey || event.metaKey ) {
							$.datepicker._clearDate( event.target );
						}
						handled = event.ctrlKey || event.metaKey;
						break; // clear on ctrl or command +end
				case 36: if ( event.ctrlKey || event.metaKey ) {
							$.datepicker._gotoToday( event.target );
						}
						handled = event.ctrlKey || event.metaKey;
						break; // current on ctrl or command +home
				case 37: if ( event.ctrlKey || event.metaKey ) {
							$.datepicker._adjustDate( event.target, ( isRTL ? +1 : -1 ), "D" );
						}
						handled = event.ctrlKey || event.metaKey;

						// -1 day on ctrl or command +left
						if ( event.originalEvent.altKey ) {
							$.datepicker._adjustDate( event.target, ( event.ctrlKey ?
								-$.datepicker._get( inst, "stepBigMonths" ) :
								-$.datepicker._get( inst, "stepMonths" ) ), "M" );
						}

						// next month/year on alt +left on Mac
						break;
				case 38: if ( event.ctrlKey || event.metaKey ) {
							$.datepicker._adjustDate( event.target, -7, "D" );
						}
						handled = event.ctrlKey || event.metaKey;
						break; // -1 week on ctrl or command +up
				case 39: if ( event.ctrlKey || event.metaKey ) {
							$.datepicker._adjustDate( event.target, ( isRTL ? -1 : +1 ), "D" );
						}
						handled = event.ctrlKey || event.metaKey;

						// +1 day on ctrl or command +right
						if ( event.originalEvent.altKey ) {
							$.datepicker._adjustDate( event.target, ( event.ctrlKey ?
								+$.datepicker._get( inst, "stepBigMonths" ) :
								+$.datepicker._get( inst, "stepMonths" ) ), "M" );
						}

						// next month/year on alt +right
						break;
				case 40: if ( event.ctrlKey || event.metaKey ) {
							$.datepicker._adjustDate( event.target, +7, "D" );
						}
						handled = event.ctrlKey || event.metaKey;
						break; // +1 week on ctrl or command +down
				default: handled = false;
			}
		} else if ( event.keyCode === 36 && event.ctrlKey ) { // display the date picker on ctrl+home
			$.datepicker._showDatepicker( this );
		} else {
			handled = false;
		}

		if ( handled ) {
			event.preventDefault();
			event.stopPropagation();
		}
	},

	/* Filter entered characters - based on date format. */
	_doKeyPress: function( event ) {
		var chars, chr,
			inst = $.datepicker._getInst( event.target );

		if ( $.datepicker._get( inst, "constrainInput" ) ) {
			chars = $.datepicker._possibleChars( $.datepicker._get( inst, "dateFormat" ) );
			chr = String.fromCharCode( event.charCode == null ? event.keyCode : event.charCode );
			return event.ctrlKey || event.metaKey || ( chr < " " || !chars || chars.indexOf( chr ) > -1 );
		}
	},

	/* Synchronise manual entry and field/alternate field. */
	_doKeyUp: function( event ) {
		var date,
			inst = $.datepicker._getInst( event.target );

		if ( inst.input.val() !== inst.lastVal ) {
			try {
				date = $.datepicker.parseDate( $.datepicker._get( inst, "dateFormat" ),
					( inst.input ? inst.input.val() : null ),
					$.datepicker._getFormatConfig( inst ) );

				if ( date ) { // only if valid
					$.datepicker._setDateFromField( inst );
					$.datepicker._updateAlternate( inst );
					$.datepicker._updateDatepicker( inst );
				}
			}
			catch ( err ) {
			}
		}
		return true;
	},

	/* Pop-up the date picker for a given input field.
	 * If false returned from beforeShow event handler do not show.
	 * @param  input  element - the input field attached to the date picker or
	 *					event - if triggered by focus
	 */
	_showDatepicker: function( input ) {
		input = input.target || input;
		if ( input.nodeName.toLowerCase() !== "input" ) { // find from button/image trigger
			input = $( "input", input.parentNode )[ 0 ];
		}

		if ( $.datepicker._isDisabledDatepicker( input ) || $.datepicker._lastInput === input ) { // already here
			return;
		}

		var inst, beforeShow, beforeShowSettings, isFixed,
			offset, showAnim, duration;

		inst = $.datepicker._getInst( input );
		if ( $.datepicker._curInst && $.datepicker._curInst !== inst ) {
			$.datepicker._curInst.dpDiv.stop( true, true );
			if ( inst && $.datepicker._datepickerShowing ) {
				$.datepicker._hideDatepicker( $.datepicker._curInst.input[ 0 ] );
			}
		}

		beforeShow = $.datepicker._get( inst, "beforeShow" );
		beforeShowSettings = beforeShow ? beforeShow.apply( input, [ input, inst ] ) : {};
		if ( beforeShowSettings === false ) {
			return;
		}
		datepicker_extendRemove( inst.settings, beforeShowSettings );

		inst.lastVal = null;
		$.datepicker._lastInput = input;
		$.datepicker._setDateFromField( inst );

		if ( $.datepicker._inDialog ) { // hide cursor
			input.value = "";
		}
		if ( !$.datepicker._pos ) { // position below input
			$.datepicker._pos = $.datepicker._findPos( input );
			$.datepicker._pos[ 1 ] += input.offsetHeight; // add the height
		}

		isFixed = false;
		$( input ).parents().each( function() {
			isFixed |= $( this ).css( "position" ) === "fixed";
			return !isFixed;
		} );

		offset = { left: $.datepicker._pos[ 0 ], top: $.datepicker._pos[ 1 ] };
		$.datepicker._pos = null;

		//to avoid flashes on Firefox
		inst.dpDiv.empty();

		// determine sizing offscreen
		inst.dpDiv.css( { position: "absolute", display: "block", top: "-1000px" } );
		$.datepicker._updateDatepicker( inst );

		// fix width for dynamic number of date pickers
		// and adjust position before showing
		offset = $.datepicker._checkOffset( inst, offset, isFixed );
		inst.dpDiv.css( { position: ( $.datepicker._inDialog && $.blockUI ?
			"static" : ( isFixed ? "fixed" : "absolute" ) ), display: "none",
			left: offset.left + "px", top: offset.top + "px" } );

		if ( !inst.inline ) {
			showAnim = $.datepicker._get( inst, "showAnim" );
			duration = $.datepicker._get( inst, "duration" );
			inst.dpDiv.css( "z-index", datepicker_getZindex( $( input ) ) + 1 );
			$.datepicker._datepickerShowing = true;

			if ( $.effects && $.effects.effect[ showAnim ] ) {
				inst.dpDiv.show( showAnim, $.datepicker._get( inst, "showOptions" ), duration );
			} else {
				inst.dpDiv[ showAnim || "show" ]( showAnim ? duration : null );
			}

			if ( $.datepicker._shouldFocusInput( inst ) ) {
				inst.input.trigger( "focus" );
			}

			$.datepicker._curInst = inst;
		}
	},

	/* Generate the date picker content. */
	_updateDatepicker: function( inst ) {
		this.maxRows = 4; //Reset the max number of rows being displayed (see #7043)
		datepicker_instActive = inst; // for delegate hover events
		inst.dpDiv.empty().append( this._generateHTML( inst ) );
		this._attachHandlers( inst );

		var origyearshtml,
			numMonths = this._getNumberOfMonths( inst ),
			cols = numMonths[ 1 ],
			width = 17,
			activeCell = inst.dpDiv.find( "." + this._dayOverClass + " a" );

		if ( activeCell.length > 0 ) {
			datepicker_handleMouseover.apply( activeCell.get( 0 ) );
		}

		inst.dpDiv.removeClass( "ui-datepicker-multi-2 ui-datepicker-multi-3 ui-datepicker-multi-4" ).width( "" );
		if ( cols > 1 ) {
			inst.dpDiv.addClass( "ui-datepicker-multi-" + cols ).css( "width", ( width * cols ) + "em" );
		}
		inst.dpDiv[ ( numMonths[ 0 ] !== 1 || numMonths[ 1 ] !== 1 ? "add" : "remove" ) +
			"Class" ]( "ui-datepicker-multi" );
		inst.dpDiv[ ( this._get( inst, "isRTL" ) ? "add" : "remove" ) +
			"Class" ]( "ui-datepicker-rtl" );

		if ( inst === $.datepicker._curInst && $.datepicker._datepickerShowing && $.datepicker._shouldFocusInput( inst ) ) {
			inst.input.trigger( "focus" );
		}

		// Deffered render of the years select (to avoid flashes on Firefox)
		if ( inst.yearshtml ) {
			origyearshtml = inst.yearshtml;
			setTimeout( function() {

				//assure that inst.yearshtml didn't change.
				if ( origyearshtml === inst.yearshtml && inst.yearshtml ) {
					inst.dpDiv.find( "select.ui-datepicker-year:first" ).replaceWith( inst.yearshtml );
				}
				origyearshtml = inst.yearshtml = null;
			}, 0 );
		}
	},

	// #6694 - don't focus the input if it's already focused
	// this breaks the change event in IE
	// Support: IE and jQuery <1.9
	_shouldFocusInput: function( inst ) {
		return inst.input && inst.input.is( ":visible" ) && !inst.input.is( ":disabled" ) && !inst.input.is( ":focus" );
	},

	/* Check positioning to remain on screen. */
	_checkOffset: function( inst, offset, isFixed ) {
		var dpWidth = inst.dpDiv.outerWidth(),
			dpHeight = inst.dpDiv.outerHeight(),
			inputWidth = inst.input ? inst.input.outerWidth() : 0,
			inputHeight = inst.input ? inst.input.outerHeight() : 0,
			viewWidth = document.documentElement.clientWidth + ( isFixed ? 0 : $( document ).scrollLeft() ),
			viewHeight = document.documentElement.clientHeight + ( isFixed ? 0 : $( document ).scrollTop() );

		offset.left -= ( this._get( inst, "isRTL" ) ? ( dpWidth - inputWidth ) : 0 );
		offset.left -= ( isFixed && offset.left === inst.input.offset().left ) ? $( document ).scrollLeft() : 0;
		offset.top -= ( isFixed && offset.top === ( inst.input.offset().top + inputHeight ) ) ? $( document ).scrollTop() : 0;

		// Now check if datepicker is showing outside window viewport - move to a better place if so.
		offset.left -= Math.min( offset.left, ( offset.left + dpWidth > viewWidth && viewWidth > dpWidth ) ?
			Math.abs( offset.left + dpWidth - viewWidth ) : 0 );
		offset.top -= Math.min( offset.top, ( offset.top + dpHeight > viewHeight && viewHeight > dpHeight ) ?
			Math.abs( dpHeight + inputHeight ) : 0 );

		return offset;
	},

	/* Find an object's position on the screen. */
	_findPos: function( obj ) {
		var position,
			inst = this._getInst( obj ),
			isRTL = this._get( inst, "isRTL" );

		while ( obj && ( obj.type === "hidden" || obj.nodeType !== 1 || $.expr.filters.hidden( obj ) ) ) {
			obj = obj[ isRTL ? "previousSibling" : "nextSibling" ];
		}

		position = $( obj ).offset();
		return [ position.left, position.top ];
	},

	/* Hide the date picker from view.
	 * @param  input  element - the input field attached to the date picker
	 */
	_hideDatepicker: function( input ) {
		var showAnim, duration, postProcess, onClose,
			inst = this._curInst;

		if ( !inst || ( input && inst !== $.data( input, "datepicker" ) ) ) {
			return;
		}

		if ( this._datepickerShowing ) {
			showAnim = this._get( inst, "showAnim" );
			duration = this._get( inst, "duration" );
			postProcess = function() {
				$.datepicker._tidyDialog( inst );
			};

			// DEPRECATED: after BC for 1.8.x $.effects[ showAnim ] is not needed
			if ( $.effects && ( $.effects.effect[ showAnim ] || $.effects[ showAnim ] ) ) {
				inst.dpDiv.hide( showAnim, $.datepicker._get( inst, "showOptions" ), duration, postProcess );
			} else {
				inst.dpDiv[ ( showAnim === "slideDown" ? "slideUp" :
					( showAnim === "fadeIn" ? "fadeOut" : "hide" ) ) ]( ( showAnim ? duration : null ), postProcess );
			}

			if ( !showAnim ) {
				postProcess();
			}
			this._datepickerShowing = false;

			onClose = this._get( inst, "onClose" );
			if ( onClose ) {
				onClose.apply( ( inst.input ? inst.input[ 0 ] : null ), [ ( inst.input ? inst.input.val() : "" ), inst ] );
			}

			this._lastInput = null;
			if ( this._inDialog ) {
				this._dialogInput.css( { position: "absolute", left: "0", top: "-100px" } );
				if ( $.blockUI ) {
					$.unblockUI();
					$( "body" ).append( this.dpDiv );
				}
			}
			this._inDialog = false;
		}
	},

	/* Tidy up after a dialog display. */
	_tidyDialog: function( inst ) {
		inst.dpDiv.removeClass( this._dialogClass ).off( ".ui-datepicker-calendar" );
	},

	/* Close date picker if clicked elsewhere. */
	_checkExternalClick: function( event ) {
		if ( !$.datepicker._curInst ) {
			return;
		}

		var $target = $( event.target ),
			inst = $.datepicker._getInst( $target[ 0 ] );

		if ( ( ( $target[ 0 ].id !== $.datepicker._mainDivId &&
				$target.parents( "#" + $.datepicker._mainDivId ).length === 0 &&
				!$target.hasClass( $.datepicker.markerClassName ) &&
				!$target.closest( "." + $.datepicker._triggerClass ).length &&
				$.datepicker._datepickerShowing && !( $.datepicker._inDialog && $.blockUI ) ) ) ||
			( $target.hasClass( $.datepicker.markerClassName ) && $.datepicker._curInst !== inst ) ) {
				$.datepicker._hideDatepicker();
		}
	},

	/* Adjust one of the date sub-fields. */
	_adjustDate: function( id, offset, period ) {
		var target = $( id ),
			inst = this._getInst( target[ 0 ] );

		if ( this._isDisabledDatepicker( target[ 0 ] ) ) {
			return;
		}
		this._adjustInstDate( inst, offset +
			( period === "M" ? this._get( inst, "showCurrentAtPos" ) : 0 ), // undo positioning
			period );
		this._updateDatepicker( inst );
	},

	/* Action for current link. */
	_gotoToday: function( id ) {
		var date,
			target = $( id ),
			inst = this._getInst( target[ 0 ] );

		if ( this._get( inst, "gotoCurrent" ) && inst.currentDay ) {
			inst.selectedDay = inst.currentDay;
			inst.drawMonth = inst.selectedMonth = inst.currentMonth;
			inst.drawYear = inst.selectedYear = inst.currentYear;
		} else {
			date = new Date();
			inst.selectedDay = date.getDate();
			inst.drawMonth = inst.selectedMonth = date.getMonth();
			inst.drawYear = inst.selectedYear = date.getFullYear();
		}
		this._notifyChange( inst );
		this._adjustDate( target );
	},

	/* Action for selecting a new month/year. */
	_selectMonthYear: function( id, select, period ) {
		var target = $( id ),
			inst = this._getInst( target[ 0 ] );

		inst[ "selected" + ( period === "M" ? "Month" : "Year" ) ] =
		inst[ "draw" + ( period === "M" ? "Month" : "Year" ) ] =
			parseInt( select.options[ select.selectedIndex ].value, 10 );

		this._notifyChange( inst );
		this._adjustDate( target );
	},

	/* Action for selecting a day. */
	_selectDay: function( id, month, year, td ) {
		var inst,
			target = $( id );

		if ( $( td ).hasClass( this._unselectableClass ) || this._isDisabledDatepicker( target[ 0 ] ) ) {
			return;
		}

		inst = this._getInst( target[ 0 ] );
		inst.selectedDay = inst.currentDay = $( "a", td ).html();
		inst.selectedMonth = inst.currentMonth = month;
		inst.selectedYear = inst.currentYear = year;
		this._selectDate( id, this._formatDate( inst,
			inst.currentDay, inst.currentMonth, inst.currentYear ) );
	},

	/* Erase the input field and hide the date picker. */
	_clearDate: function( id ) {
		var target = $( id );
		this._selectDate( target, "" );
	},

	/* Update the input field with the selected date. */
	_selectDate: function( id, dateStr ) {
		var onSelect,
			target = $( id ),
			inst = this._getInst( target[ 0 ] );

		dateStr = ( dateStr != null ? dateStr : this._formatDate( inst ) );
		if ( inst.input ) {
			inst.input.val( dateStr );
		}
		this._updateAlternate( inst );

		onSelect = this._get( inst, "onSelect" );
		if ( onSelect ) {
			onSelect.apply( ( inst.input ? inst.input[ 0 ] : null ), [ dateStr, inst ] );  // trigger custom callback
		} else if ( inst.input ) {
			inst.input.trigger( "change" ); // fire the change event
		}

		if ( inst.inline ) {
			this._updateDatepicker( inst );
		} else {
			this._hideDatepicker();
			this._lastInput = inst.input[ 0 ];
			if ( typeof( inst.input[ 0 ] ) !== "object" ) {
				inst.input.trigger( "focus" ); // restore focus
			}
			this._lastInput = null;
		}
	},

	/* Update any alternate field to synchronise with the main field. */
	_updateAlternate: function( inst ) {
		var altFormat, date, dateStr,
			altField = this._get( inst, "altField" );

		if ( altField ) { // update alternate field too
			altFormat = this._get( inst, "altFormat" ) || this._get( inst, "dateFormat" );
			date = this._getDate( inst );
			dateStr = this.formatDate( altFormat, date, this._getFormatConfig( inst ) );
			$( altField ).val( dateStr );
		}
	},

	/* Set as beforeShowDay function to prevent selection of weekends.
	 * @param  date  Date - the date to customise
	 * @return [boolean, string] - is this date selectable?, what is its CSS class?
	 */
	noWeekends: function( date ) {
		var day = date.getDay();
		return [ ( day > 0 && day < 6 ), "" ];
	},

	/* Set as calculateWeek to determine the week of the year based on the ISO 8601 definition.
	 * @param  date  Date - the date to get the week for
	 * @return  number - the number of the week within the year that contains this date
	 */
	iso8601Week: function( date ) {
		var time,
			checkDate = new Date( date.getTime() );

		// Find Thursday of this week starting on Monday
		checkDate.setDate( checkDate.getDate() + 4 - ( checkDate.getDay() || 7 ) );

		time = checkDate.getTime();
		checkDate.setMonth( 0 ); // Compare with Jan 1
		checkDate.setDate( 1 );
		return Math.floor( Math.round( ( time - checkDate ) / 86400000 ) / 7 ) + 1;
	},

	/* Parse a string value into a date object.
	 * See formatDate below for the possible formats.
	 *
	 * @param  format string - the expected format of the date
	 * @param  value string - the date in the above format
	 * @param  settings Object - attributes include:
	 *					shortYearCutoff  number - the cutoff year for determining the century (optional)
	 *					dayNamesShort	string[7] - abbreviated names of the days from Sunday (optional)
	 *					dayNames		string[7] - names of the days from Sunday (optional)
	 *					monthNamesShort string[12] - abbreviated names of the months (optional)
	 *					monthNames		string[12] - names of the months (optional)
	 * @return  Date - the extracted date value or null if value is blank
	 */
	parseDate: function( format, value, settings ) {
		if ( format == null || value == null ) {
			throw "Invalid arguments";
		}

		value = ( typeof value === "object" ? value.toString() : value + "" );
		if ( value === "" ) {
			return null;
		}

		var iFormat, dim, extra,
			iValue = 0,
			shortYearCutoffTemp = ( settings ? settings.shortYearCutoff : null ) || this._defaults.shortYearCutoff,
			shortYearCutoff = ( typeof shortYearCutoffTemp !== "string" ? shortYearCutoffTemp :
				new Date().getFullYear() % 100 + parseInt( shortYearCutoffTemp, 10 ) ),
			dayNamesShort = ( settings ? settings.dayNamesShort : null ) || this._defaults.dayNamesShort,
			dayNames = ( settings ? settings.dayNames : null ) || this._defaults.dayNames,
			monthNamesShort = ( settings ? settings.monthNamesShort : null ) || this._defaults.monthNamesShort,
			monthNames = ( settings ? settings.monthNames : null ) || this._defaults.monthNames,
			year = -1,
			month = -1,
			day = -1,
			doy = -1,
			literal = false,
			date,

			// Check whether a format character is doubled
			lookAhead = function( match ) {
				var matches = ( iFormat + 1 < format.length && format.charAt( iFormat + 1 ) === match );
				if ( matches ) {
					iFormat++;
				}
				return matches;
			},

			// Extract a number from the string value
			getNumber = function( match ) {
				var isDoubled = lookAhead( match ),
					size = ( match === "@" ? 14 : ( match === "!" ? 20 :
					( match === "y" && isDoubled ? 4 : ( match === "o" ? 3 : 2 ) ) ) ),
					minSize = ( match === "y" ? size : 1 ),
					digits = new RegExp( "^\\d{" + minSize + "," + size + "}" ),
					num = value.substring( iValue ).match( digits );
				if ( !num ) {
					throw "Missing number at position " + iValue;
				}
				iValue += num[ 0 ].length;
				return parseInt( num[ 0 ], 10 );
			},

			// Extract a name from the string value and convert to an index
			getName = function( match, shortNames, longNames ) {
				var index = -1,
					names = $.map( lookAhead( match ) ? longNames : shortNames, function( v, k ) {
						return [ [ k, v ] ];
					} ).sort( function( a, b ) {
						return -( a[ 1 ].length - b[ 1 ].length );
					} );

				$.each( names, function( i, pair ) {
					var name = pair[ 1 ];
					if ( value.substr( iValue, name.length ).toLowerCase() === name.toLowerCase() ) {
						index = pair[ 0 ];
						iValue += name.length;
						return false;
					}
				} );
				if ( index !== -1 ) {
					return index + 1;
				} else {
					throw "Unknown name at position " + iValue;
				}
			},

			// Confirm that a literal character matches the string value
			checkLiteral = function() {
				if ( value.charAt( iValue ) !== format.charAt( iFormat ) ) {
					throw "Unexpected literal at position " + iValue;
				}
				iValue++;
			};

		for ( iFormat = 0; iFormat < format.length; iFormat++ ) {
			if ( literal ) {
				if ( format.charAt( iFormat ) === "'" && !lookAhead( "'" ) ) {
					literal = false;
				} else {
					checkLiteral();
				}
			} else {
				switch ( format.charAt( iFormat ) ) {
					case "d":
						day = getNumber( "d" );
						break;
					case "D":
						getName( "D", dayNamesShort, dayNames );
						break;
					case "o":
						doy = getNumber( "o" );
						break;
					case "m":
						month = getNumber( "m" );
						break;
					case "M":
						month = getName( "M", monthNamesShort, monthNames );
						break;
					case "y":
						year = getNumber( "y" );
						break;
					case "@":
						date = new Date( getNumber( "@" ) );
						year = date.getFullYear();
						month = date.getMonth() + 1;
						day = date.getDate();
						break;
					case "!":
						date = new Date( ( getNumber( "!" ) - this._ticksTo1970 ) / 10000 );
						year = date.getFullYear();
						month = date.getMonth() + 1;
						day = date.getDate();
						break;
					case "'":
						if ( lookAhead( "'" ) ) {
							checkLiteral();
						} else {
							literal = true;
						}
						break;
					default:
						checkLiteral();
				}
			}
		}

		if ( iValue < value.length ) {
			extra = value.substr( iValue );
			if ( !/^\s+/.test( extra ) ) {
				throw "Extra/unparsed characters found in date: " + extra;
			}
		}

		if ( year === -1 ) {
			year = new Date().getFullYear();
		} else if ( year < 100 ) {
			year += new Date().getFullYear() - new Date().getFullYear() % 100 +
				( year <= shortYearCutoff ? 0 : -100 );
		}

		if ( doy > -1 ) {
			month = 1;
			day = doy;
			do {
				dim = this._getDaysInMonth( year, month - 1 );
				if ( day <= dim ) {
					break;
				}
				month++;
				day -= dim;
			} while ( true );
		}

		date = this._daylightSavingAdjust( new Date( year, month - 1, day ) );
		if ( date.getFullYear() !== year || date.getMonth() + 1 !== month || date.getDate() !== day ) {
			throw "Invalid date"; // E.g. 31/02/00
		}
		return date;
	},

	/* Standard date formats. */
	ATOM: "yy-mm-dd", // RFC 3339 (ISO 8601)
	COOKIE: "D, dd M yy",
	ISO_8601: "yy-mm-dd",
	RFC_822: "D, d M y",
	RFC_850: "DD, dd-M-y",
	RFC_1036: "D, d M y",
	RFC_1123: "D, d M yy",
	RFC_2822: "D, d M yy",
	RSS: "D, d M y", // RFC 822
	TICKS: "!",
	TIMESTAMP: "@",
	W3C: "yy-mm-dd", // ISO 8601

	_ticksTo1970: ( ( ( 1970 - 1 ) * 365 + Math.floor( 1970 / 4 ) - Math.floor( 1970 / 100 ) +
		Math.floor( 1970 / 400 ) ) * 24 * 60 * 60 * 10000000 ),

	/* Format a date object into a string value.
	 * The format can be combinations of the following:
	 * d  - day of month (no leading zero)
	 * dd - day of month (two digit)
	 * o  - day of year (no leading zeros)
	 * oo - day of year (three digit)
	 * D  - day name short
	 * DD - day name long
	 * m  - month of year (no leading zero)
	 * mm - month of year (two digit)
	 * M  - month name short
	 * MM - month name long
	 * y  - year (two digit)
	 * yy - year (four digit)
	 * @ - Unix timestamp (ms since 01/01/1970)
	 * ! - Windows ticks (100ns since 01/01/0001)
	 * "..." - literal text
	 * '' - single quote
	 *
	 * @param  format string - the desired format of the date
	 * @param  date Date - the date value to format
	 * @param  settings Object - attributes include:
	 *					dayNamesShort	string[7] - abbreviated names of the days from Sunday (optional)
	 *					dayNames		string[7] - names of the days from Sunday (optional)
	 *					monthNamesShort string[12] - abbreviated names of the months (optional)
	 *					monthNames		string[12] - names of the months (optional)
	 * @return  string - the date in the above format
	 */
	formatDate: function( format, date, settings ) {
		if ( !date ) {
			return "";
		}

		var iFormat,
			dayNamesShort = ( settings ? settings.dayNamesShort : null ) || this._defaults.dayNamesShort,
			dayNames = ( settings ? settings.dayNames : null ) || this._defaults.dayNames,
			monthNamesShort = ( settings ? settings.monthNamesShort : null ) || this._defaults.monthNamesShort,
			monthNames = ( settings ? settings.monthNames : null ) || this._defaults.monthNames,

			// Check whether a format character is doubled
			lookAhead = function( match ) {
				var matches = ( iFormat + 1 < format.length && format.charAt( iFormat + 1 ) === match );
				if ( matches ) {
					iFormat++;
				}
				return matches;
			},

			// Format a number, with leading zero if necessary
			formatNumber = function( match, value, len ) {
				var num = "" + value;
				if ( lookAhead( match ) ) {
					while ( num.length < len ) {
						num = "0" + num;
					}
				}
				return num;
			},

			// Format a name, short or long as requested
			formatName = function( match, value, shortNames, longNames ) {
				return ( lookAhead( match ) ? longNames[ value ] : shortNames[ value ] );
			},
			output = "",
			literal = false;

		if ( date ) {
			for ( iFormat = 0; iFormat < format.length; iFormat++ ) {
				if ( literal ) {
					if ( format.charAt( iFormat ) === "'" && !lookAhead( "'" ) ) {
						literal = false;
					} else {
						output += format.charAt( iFormat );
					}
				} else {
					switch ( format.charAt( iFormat ) ) {
						case "d":
							output += formatNumber( "d", date.getDate(), 2 );
							break;
						case "D":
							output += formatName( "D", date.getDay(), dayNamesShort, dayNames );
							break;
						case "o":
							output += formatNumber( "o",
								Math.round( ( new Date( date.getFullYear(), date.getMonth(), date.getDate() ).getTime() - new Date( date.getFullYear(), 0, 0 ).getTime() ) / 86400000 ), 3 );
							break;
						case "m":
							output += formatNumber( "m", date.getMonth() + 1, 2 );
							break;
						case "M":
							output += formatName( "M", date.getMonth(), monthNamesShort, monthNames );
							break;
						case "y":
							output += ( lookAhead( "y" ) ? date.getFullYear() :
								( date.getFullYear() % 100 < 10 ? "0" : "" ) + date.getFullYear() % 100 );
							break;
						case "@":
							output += date.getTime();
							break;
						case "!":
							output += date.getTime() * 10000 + this._ticksTo1970;
							break;
						case "'":
							if ( lookAhead( "'" ) ) {
								output += "'";
							} else {
								literal = true;
							}
							break;
						default:
							output += format.charAt( iFormat );
					}
				}
			}
		}
		return output;
	},

	/* Extract all possible characters from the date format. */
	_possibleChars: function( format ) {
		var iFormat,
			chars = "",
			literal = false,

			// Check whether a format character is doubled
			lookAhead = function( match ) {
				var matches = ( iFormat + 1 < format.length && format.charAt( iFormat + 1 ) === match );
				if ( matches ) {
					iFormat++;
				}
				return matches;
			};

		for ( iFormat = 0; iFormat < format.length; iFormat++ ) {
			if ( literal ) {
				if ( format.charAt( iFormat ) === "'" && !lookAhead( "'" ) ) {
					literal = false;
				} else {
					chars += format.charAt( iFormat );
				}
			} else {
				switch ( format.charAt( iFormat ) ) {
					case "d": case "m": case "y": case "@":
						chars += "0123456789";
						break;
					case "D": case "M":
						return null; // Accept anything
					case "'":
						if ( lookAhead( "'" ) ) {
							chars += "'";
						} else {
							literal = true;
						}
						break;
					default:
						chars += format.charAt( iFormat );
				}
			}
		}
		return chars;
	},

	/* Get a setting value, defaulting if necessary. */
	_get: function( inst, name ) {
		return inst.settings[ name ] !== undefined ?
			inst.settings[ name ] : this._defaults[ name ];
	},

	/* Parse existing date and initialise date picker. */
	_setDateFromField: function( inst, noDefault ) {
		if ( inst.input.val() === inst.lastVal ) {
			return;
		}

		var dateFormat = this._get( inst, "dateFormat" ),
			dates = inst.lastVal = inst.input ? inst.input.val() : null,
			defaultDate = this._getDefaultDate( inst ),
			date = defaultDate,
			settings = this._getFormatConfig( inst );

		try {
			date = this.parseDate( dateFormat, dates, settings ) || defaultDate;
		} catch ( event ) {
			dates = ( noDefault ? "" : dates );
		}
		inst.selectedDay = date.getDate();
		inst.drawMonth = inst.selectedMonth = date.getMonth();
		inst.drawYear = inst.selectedYear = date.getFullYear();
		inst.currentDay = ( dates ? date.getDate() : 0 );
		inst.currentMonth = ( dates ? date.getMonth() : 0 );
		inst.currentYear = ( dates ? date.getFullYear() : 0 );
		this._adjustInstDate( inst );
	},

	/* Retrieve the default date shown on opening. */
	_getDefaultDate: function( inst ) {
		return this._restrictMinMax( inst,
			this._determineDate( inst, this._get( inst, "defaultDate" ), new Date() ) );
	},

	/* A date may be specified as an exact value or a relative one. */
	_determineDate: function( inst, date, defaultDate ) {
		var offsetNumeric = function( offset ) {
				var date = new Date();
				date.setDate( date.getDate() + offset );
				return date;
			},
			offsetString = function( offset ) {
				try {
					return $.datepicker.parseDate( $.datepicker._get( inst, "dateFormat" ),
						offset, $.datepicker._getFormatConfig( inst ) );
				}
				catch ( e ) {

					// Ignore
				}

				var date = ( offset.toLowerCase().match( /^c/ ) ?
					$.datepicker._getDate( inst ) : null ) || new Date(),
					year = date.getFullYear(),
					month = date.getMonth(),
					day = date.getDate(),
					pattern = /([+\-]?[0-9]+)\s*(d|D|w|W|m|M|y|Y)?/g,
					matches = pattern.exec( offset );

				while ( matches ) {
					switch ( matches[ 2 ] || "d" ) {
						case "d" : case "D" :
							day += parseInt( matches[ 1 ], 10 ); break;
						case "w" : case "W" :
							day += parseInt( matches[ 1 ], 10 ) * 7; break;
						case "m" : case "M" :
							month += parseInt( matches[ 1 ], 10 );
							day = Math.min( day, $.datepicker._getDaysInMonth( year, month ) );
							break;
						case "y": case "Y" :
							year += parseInt( matches[ 1 ], 10 );
							day = Math.min( day, $.datepicker._getDaysInMonth( year, month ) );
							break;
					}
					matches = pattern.exec( offset );
				}
				return new Date( year, month, day );
			},
			newDate = ( date == null || date === "" ? defaultDate : ( typeof date === "string" ? offsetString( date ) :
				( typeof date === "number" ? ( isNaN( date ) ? defaultDate : offsetNumeric( date ) ) : new Date( date.getTime() ) ) ) );

		newDate = ( newDate && newDate.toString() === "Invalid Date" ? defaultDate : newDate );
		if ( newDate ) {
			newDate.setHours( 0 );
			newDate.setMinutes( 0 );
			newDate.setSeconds( 0 );
			newDate.setMilliseconds( 0 );
		}
		return this._daylightSavingAdjust( newDate );
	},

	/* Handle switch to/from daylight saving.
	 * Hours may be non-zero on daylight saving cut-over:
	 * > 12 when midnight changeover, but then cannot generate
	 * midnight datetime, so jump to 1AM, otherwise reset.
	 * @param  date  (Date) the date to check
	 * @return  (Date) the corrected date
	 */
	_daylightSavingAdjust: function( date ) {
		if ( !date ) {
			return null;
		}
		date.setHours( date.getHours() > 12 ? date.getHours() + 2 : 0 );
		return date;
	},

	/* Set the date(s) directly. */
	_setDate: function( inst, date, noChange ) {
		var clear = !date,
			origMonth = inst.selectedMonth,
			origYear = inst.selectedYear,
			newDate = this._restrictMinMax( inst, this._determineDate( inst, date, new Date() ) );

		inst.selectedDay = inst.currentDay = newDate.getDate();
		inst.drawMonth = inst.selectedMonth = inst.currentMonth = newDate.getMonth();
		inst.drawYear = inst.selectedYear = inst.currentYear = newDate.getFullYear();
		if ( ( origMonth !== inst.selectedMonth || origYear !== inst.selectedYear ) && !noChange ) {
			this._notifyChange( inst );
		}
		this._adjustInstDate( inst );
		if ( inst.input ) {
			inst.input.val( clear ? "" : this._formatDate( inst ) );
		}
	},

	/* Retrieve the date(s) directly. */
	_getDate: function( inst ) {
		var startDate = ( !inst.currentYear || ( inst.input && inst.input.val() === "" ) ? null :
			this._daylightSavingAdjust( new Date(
			inst.currentYear, inst.currentMonth, inst.currentDay ) ) );
			return startDate;
	},

	/* Attach the onxxx handlers.  These are declared statically so
	 * they work with static code transformers like Caja.
	 */
	_attachHandlers: function( inst ) {
		var stepMonths = this._get( inst, "stepMonths" ),
			id = "#" + inst.id.replace( /\\\\/g, "\\" );
		inst.dpDiv.find( "[data-handler]" ).map( function() {
			var handler = {
				prev: function() {
					$.datepicker._adjustDate( id, -stepMonths, "M" );
				},
				next: function() {
					$.datepicker._adjustDate( id, +stepMonths, "M" );
				},
				hide: function() {
					$.datepicker._hideDatepicker();
				},
				today: function() {
					$.datepicker._gotoToday( id );
				},
				selectDay: function() {
					$.datepicker._selectDay( id, +this.getAttribute( "data-month" ), +this.getAttribute( "data-year" ), this );
					return false;
				},
				selectMonth: function() {
					$.datepicker._selectMonthYear( id, this, "M" );
					return false;
				},
				selectYear: function() {
					$.datepicker._selectMonthYear( id, this, "Y" );
					return false;
				}
			};
			$( this ).on( this.getAttribute( "data-event" ), handler[ this.getAttribute( "data-handler" ) ] );
		} );
	},

	/* Generate the HTML for the current state of the date picker. */
	_generateHTML: function( inst ) {
		var maxDraw, prevText, prev, nextText, next, currentText, gotoDate,
			controls, buttonPanel, firstDay, showWeek, dayNames, dayNamesMin,
			monthNames, monthNamesShort, beforeShowDay, showOtherMonths,
			selectOtherMonths, defaultDate, html, dow, row, group, col, selectedDate,
			cornerClass, calender, thead, day, daysInMonth, leadDays, curRows, numRows,
			printDate, dRow, tbody, daySettings, otherMonth, unselectable,
			tempDate = new Date(),
			today = this._daylightSavingAdjust(
				new Date( tempDate.getFullYear(), tempDate.getMonth(), tempDate.getDate() ) ), // clear time
			isRTL = this._get( inst, "isRTL" ),
			showButtonPanel = this._get( inst, "showButtonPanel" ),
			hideIfNoPrevNext = this._get( inst, "hideIfNoPrevNext" ),
			navigationAsDateFormat = this._get( inst, "navigationAsDateFormat" ),
			numMonths = this._getNumberOfMonths( inst ),
			showCurrentAtPos = this._get( inst, "showCurrentAtPos" ),
			stepMonths = this._get( inst, "stepMonths" ),
			isMultiMonth = ( numMonths[ 0 ] !== 1 || numMonths[ 1 ] !== 1 ),
			currentDate = this._daylightSavingAdjust( ( !inst.currentDay ? new Date( 9999, 9, 9 ) :
				new Date( inst.currentYear, inst.currentMonth, inst.currentDay ) ) ),
			minDate = this._getMinMaxDate( inst, "min" ),
			maxDate = this._getMinMaxDate( inst, "max" ),
			drawMonth = inst.drawMonth - showCurrentAtPos,
			drawYear = inst.drawYear;

		if ( drawMonth < 0 ) {
			drawMonth += 12;
			drawYear--;
		}
		if ( maxDate ) {
			maxDraw = this._daylightSavingAdjust( new Date( maxDate.getFullYear(),
				maxDate.getMonth() - ( numMonths[ 0 ] * numMonths[ 1 ] ) + 1, maxDate.getDate() ) );
			maxDraw = ( minDate && maxDraw < minDate ? minDate : maxDraw );
			while ( this._daylightSavingAdjust( new Date( drawYear, drawMonth, 1 ) ) > maxDraw ) {
				drawMonth--;
				if ( drawMonth < 0 ) {
					drawMonth = 11;
					drawYear--;
				}
			}
		}
		inst.drawMonth = drawMonth;
		inst.drawYear = drawYear;

		prevText = this._get( inst, "prevText" );
		prevText = ( !navigationAsDateFormat ? prevText : this.formatDate( prevText,
			this._daylightSavingAdjust( new Date( drawYear, drawMonth - stepMonths, 1 ) ),
			this._getFormatConfig( inst ) ) );

		prev = ( this._canAdjustMonth( inst, -1, drawYear, drawMonth ) ?
			"<a class='ui-datepicker-prev ui-corner-all' data-handler='prev' data-event='click'" +
			" title='" + prevText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "e" : "w" ) + "'>" + prevText + "</span></a>" :
			( hideIfNoPrevNext ? "" : "<a class='ui-datepicker-prev ui-corner-all ui-state-disabled' title='" + prevText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "e" : "w" ) + "'>" + prevText + "</span></a>" ) );

		nextText = this._get( inst, "nextText" );
		nextText = ( !navigationAsDateFormat ? nextText : this.formatDate( nextText,
			this._daylightSavingAdjust( new Date( drawYear, drawMonth + stepMonths, 1 ) ),
			this._getFormatConfig( inst ) ) );

		next = ( this._canAdjustMonth( inst, +1, drawYear, drawMonth ) ?
			"<a class='ui-datepicker-next ui-corner-all' data-handler='next' data-event='click'" +
			" title='" + nextText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "w" : "e" ) + "'>" + nextText + "</span></a>" :
			( hideIfNoPrevNext ? "" : "<a class='ui-datepicker-next ui-corner-all ui-state-disabled' title='" + nextText + "'><span class='ui-icon ui-icon-circle-triangle-" + ( isRTL ? "w" : "e" ) + "'>" + nextText + "</span></a>" ) );

		currentText = this._get( inst, "currentText" );
		gotoDate = ( this._get( inst, "gotoCurrent" ) && inst.currentDay ? currentDate : today );
		currentText = ( !navigationAsDateFormat ? currentText :
			this.formatDate( currentText, gotoDate, this._getFormatConfig( inst ) ) );

		controls = ( !inst.inline ? "<button type='button' class='ui-datepicker-close ui-state-default ui-priority-primary ui-corner-all' data-handler='hide' data-event='click'>" +
			this._get( inst, "closeText" ) + "</button>" : "" );

		buttonPanel = ( showButtonPanel ) ? "<div class='ui-datepicker-buttonpane ui-widget-content'>" + ( isRTL ? controls : "" ) +
			( this._isInRange( inst, gotoDate ) ? "<button type='button' class='ui-datepicker-current ui-state-default ui-priority-secondary ui-corner-all' data-handler='today' data-event='click'" +
			">" + currentText + "</button>" : "" ) + ( isRTL ? "" : controls ) + "</div>" : "";

		firstDay = parseInt( this._get( inst, "firstDay" ), 10 );
		firstDay = ( isNaN( firstDay ) ? 0 : firstDay );

		showWeek = this._get( inst, "showWeek" );
		dayNames = this._get( inst, "dayNames" );
		dayNamesMin = this._get( inst, "dayNamesMin" );
		monthNames = this._get( inst, "monthNames" );
		monthNamesShort = this._get( inst, "monthNamesShort" );
		beforeShowDay = this._get( inst, "beforeShowDay" );
		showOtherMonths = this._get( inst, "showOtherMonths" );
		selectOtherMonths = this._get( inst, "selectOtherMonths" );
		defaultDate = this._getDefaultDate( inst );
		html = "";

		for ( row = 0; row < numMonths[ 0 ]; row++ ) {
			group = "";
			this.maxRows = 4;
			for ( col = 0; col < numMonths[ 1 ]; col++ ) {
				selectedDate = this._daylightSavingAdjust( new Date( drawYear, drawMonth, inst.selectedDay ) );
				cornerClass = " ui-corner-all";
				calender = "";
				if ( isMultiMonth ) {
					calender += "<div class='ui-datepicker-group";
					if ( numMonths[ 1 ] > 1 ) {
						switch ( col ) {
							case 0: calender += " ui-datepicker-group-first";
								cornerClass = " ui-corner-" + ( isRTL ? "right" : "left" ); break;
							case numMonths[ 1 ] - 1: calender += " ui-datepicker-group-last";
								cornerClass = " ui-corner-" + ( isRTL ? "left" : "right" ); break;
							default: calender += " ui-datepicker-group-middle"; cornerClass = ""; break;
						}
					}
					calender += "'>";
				}
				calender += "<div class='ui-datepicker-header ui-widget-header ui-helper-clearfix" + cornerClass + "'>" +
					( /all|left/.test( cornerClass ) && row === 0 ? ( isRTL ? next : prev ) : "" ) +
					( /all|right/.test( cornerClass ) && row === 0 ? ( isRTL ? prev : next ) : "" ) +
					this._generateMonthYearHeader( inst, drawMonth, drawYear, minDate, maxDate,
					row > 0 || col > 0, monthNames, monthNamesShort ) + // draw month headers
					"</div><table class='ui-datepicker-calendar'><thead>" +
					"<tr>";
				thead = ( showWeek ? "<th class='ui-datepicker-week-col'>" + this._get( inst, "weekHeader" ) + "</th>" : "" );
				for ( dow = 0; dow < 7; dow++ ) { // days of the week
					day = ( dow + firstDay ) % 7;
					thead += "<th scope='col'" + ( ( dow + firstDay + 6 ) % 7 >= 5 ? " class='ui-datepicker-week-end'" : "" ) + ">" +
						"<span title='" + dayNames[ day ] + "'>" + dayNamesMin[ day ] + "</span></th>";
				}
				calender += thead + "</tr></thead><tbody>";
				daysInMonth = this._getDaysInMonth( drawYear, drawMonth );
				if ( drawYear === inst.selectedYear && drawMonth === inst.selectedMonth ) {
					inst.selectedDay = Math.min( inst.selectedDay, daysInMonth );
				}
				leadDays = ( this._getFirstDayOfMonth( drawYear, drawMonth ) - firstDay + 7 ) % 7;
				curRows = Math.ceil( ( leadDays + daysInMonth ) / 7 ); // calculate the number of rows to generate
				numRows = ( isMultiMonth ? this.maxRows > curRows ? this.maxRows : curRows : curRows ); //If multiple months, use the higher number of rows (see #7043)
				this.maxRows = numRows;
				printDate = this._daylightSavingAdjust( new Date( drawYear, drawMonth, 1 - leadDays ) );
				for ( dRow = 0; dRow < numRows; dRow++ ) { // create date picker rows
					calender += "<tr>";
					tbody = ( !showWeek ? "" : "<td class='ui-datepicker-week-col'>" +
						this._get( inst, "calculateWeek" )( printDate ) + "</td>" );
					for ( dow = 0; dow < 7; dow++ ) { // create date picker days
						daySettings = ( beforeShowDay ?
							beforeShowDay.apply( ( inst.input ? inst.input[ 0 ] : null ), [ printDate ] ) : [ true, "" ] );
						otherMonth = ( printDate.getMonth() !== drawMonth );
						unselectable = ( otherMonth && !selectOtherMonths ) || !daySettings[ 0 ] ||
							( minDate && printDate < minDate ) || ( maxDate && printDate > maxDate );
						tbody += "<td class='" +
							( ( dow + firstDay + 6 ) % 7 >= 5 ? " ui-datepicker-week-end" : "" ) + // highlight weekends
							( otherMonth ? " ui-datepicker-other-month" : "" ) + // highlight days from other months
							( ( printDate.getTime() === selectedDate.getTime() && drawMonth === inst.selectedMonth && inst._keyEvent ) || // user pressed key
							( defaultDate.getTime() === printDate.getTime() && defaultDate.getTime() === selectedDate.getTime() ) ?

							// or defaultDate is current printedDate and defaultDate is selectedDate
							" " + this._dayOverClass : "" ) + // highlight selected day
							( unselectable ? " " + this._unselectableClass + " ui-state-disabled" : "" ) +  // highlight unselectable days
							( otherMonth && !showOtherMonths ? "" : " " + daySettings[ 1 ] + // highlight custom dates
							( printDate.getTime() === currentDate.getTime() ? " " + this._currentClass : "" ) + // highlight selected day
							( printDate.getTime() === today.getTime() ? " ui-datepicker-today" : "" ) ) + "'" + // highlight today (if different)
							( ( !otherMonth || showOtherMonths ) && daySettings[ 2 ] ? " title='" + daySettings[ 2 ].replace( /'/g, "&#39;" ) + "'" : "" ) + // cell title
							( unselectable ? "" : " data-handler='selectDay' data-event='click' data-month='" + printDate.getMonth() + "' data-year='" + printDate.getFullYear() + "'" ) + ">" + // actions
							( otherMonth && !showOtherMonths ? "&#xa0;" : // display for other months
							( unselectable ? "<span class='ui-state-default'>" + printDate.getDate() + "</span>" : "<a class='ui-state-default" +
							( printDate.getTime() === today.getTime() ? " ui-state-highlight" : "" ) +
							( printDate.getTime() === currentDate.getTime() ? " ui-state-active" : "" ) + // highlight selected day
							( otherMonth ? " ui-priority-secondary" : "" ) + // distinguish dates from other months
							"' href='#'>" + printDate.getDate() + "</a>" ) ) + "</td>"; // display selectable date
						printDate.setDate( printDate.getDate() + 1 );
						printDate = this._daylightSavingAdjust( printDate );
					}
					calender += tbody + "</tr>";
				}
				drawMonth++;
				if ( drawMonth > 11 ) {
					drawMonth = 0;
					drawYear++;
				}
				calender += "</tbody></table>" + ( isMultiMonth ? "</div>" +
							( ( numMonths[ 0 ] > 0 && col === numMonths[ 1 ] - 1 ) ? "<div class='ui-datepicker-row-break'></div>" : "" ) : "" );
				group += calender;
			}
			html += group;
		}
		html += buttonPanel;
		inst._keyEvent = false;
		return html;
	},

	/* Generate the month and year header. */
	_generateMonthYearHeader: function( inst, drawMonth, drawYear, minDate, maxDate,
			secondary, monthNames, monthNamesShort ) {

		var inMinYear, inMaxYear, month, years, thisYear, determineYear, year, endYear,
			changeMonth = this._get( inst, "changeMonth" ),
			changeYear = this._get( inst, "changeYear" ),
			showMonthAfterYear = this._get( inst, "showMonthAfterYear" ),
			html = "<div class='ui-datepicker-title'>",
			monthHtml = "";

		// Month selection
		if ( secondary || !changeMonth ) {
			monthHtml += "<span class='ui-datepicker-month'>" + monthNames[ drawMonth ] + "</span>";
		} else {
			inMinYear = ( minDate && minDate.getFullYear() === drawYear );
			inMaxYear = ( maxDate && maxDate.getFullYear() === drawYear );
			monthHtml += "<select class='ui-datepicker-month' data-handler='selectMonth' data-event='change'>";
			for ( month = 0; month < 12; month++ ) {
				if ( ( !inMinYear || month >= minDate.getMonth() ) && ( !inMaxYear || month <= maxDate.getMonth() ) ) {
					monthHtml += "<option value='" + month + "'" +
						( month === drawMonth ? " selected='selected'" : "" ) +
						">" + monthNamesShort[ month ] + "</option>";
				}
			}
			monthHtml += "</select>";
		}

		if ( !showMonthAfterYear ) {
			html += monthHtml + ( secondary || !( changeMonth && changeYear ) ? "&#xa0;" : "" );
		}

		// Year selection
		if ( !inst.yearshtml ) {
			inst.yearshtml = "";
			if ( secondary || !changeYear ) {
				html += "<span class='ui-datepicker-year'>" + drawYear + "</span>";
			} else {

				// determine range of years to display
				years = this._get( inst, "yearRange" ).split( ":" );
				thisYear = new Date().getFullYear();
				determineYear = function( value ) {
					var year = ( value.match( /c[+\-].*/ ) ? drawYear + parseInt( value.substring( 1 ), 10 ) :
						( value.match( /[+\-].*/ ) ? thisYear + parseInt( value, 10 ) :
						parseInt( value, 10 ) ) );
					return ( isNaN( year ) ? thisYear : year );
				};
				year = determineYear( years[ 0 ] );
				endYear = Math.max( year, determineYear( years[ 1 ] || "" ) );
				year = ( minDate ? Math.max( year, minDate.getFullYear() ) : year );
				endYear = ( maxDate ? Math.min( endYear, maxDate.getFullYear() ) : endYear );
				inst.yearshtml += "<select class='ui-datepicker-year' data-handler='selectYear' data-event='change'>";
				for ( ; year <= endYear; year++ ) {
					inst.yearshtml += "<option value='" + year + "'" +
						( year === drawYear ? " selected='selected'" : "" ) +
						">" + year + "</option>";
				}
				inst.yearshtml += "</select>";

				html += inst.yearshtml;
				inst.yearshtml = null;
			}
		}

		html += this._get( inst, "yearSuffix" );
		if ( showMonthAfterYear ) {
			html += ( secondary || !( changeMonth && changeYear ) ? "&#xa0;" : "" ) + monthHtml;
		}
		html += "</div>"; // Close datepicker_header
		return html;
	},

	/* Adjust one of the date sub-fields. */
	_adjustInstDate: function( inst, offset, period ) {
		var year = inst.selectedYear + ( period === "Y" ? offset : 0 ),
			month = inst.selectedMonth + ( period === "M" ? offset : 0 ),
			day = Math.min( inst.selectedDay, this._getDaysInMonth( year, month ) ) + ( period === "D" ? offset : 0 ),
			date = this._restrictMinMax( inst, this._daylightSavingAdjust( new Date( year, month, day ) ) );

		inst.selectedDay = date.getDate();
		inst.drawMonth = inst.selectedMonth = date.getMonth();
		inst.drawYear = inst.selectedYear = date.getFullYear();
		if ( period === "M" || period === "Y" ) {
			this._notifyChange( inst );
		}
	},

	/* Ensure a date is within any min/max bounds. */
	_restrictMinMax: function( inst, date ) {
		var minDate = this._getMinMaxDate( inst, "min" ),
			maxDate = this._getMinMaxDate( inst, "max" ),
			newDate = ( minDate && date < minDate ? minDate : date );
		return ( maxDate && newDate > maxDate ? maxDate : newDate );
	},

	/* Notify change of month/year. */
	_notifyChange: function( inst ) {
		var onChange = this._get( inst, "onChangeMonthYear" );
		if ( onChange ) {
			onChange.apply( ( inst.input ? inst.input[ 0 ] : null ),
				[ inst.selectedYear, inst.selectedMonth + 1, inst ] );
		}
	},

	/* Determine the number of months to show. */
	_getNumberOfMonths: function( inst ) {
		var numMonths = this._get( inst, "numberOfMonths" );
		return ( numMonths == null ? [ 1, 1 ] : ( typeof numMonths === "number" ? [ 1, numMonths ] : numMonths ) );
	},

	/* Determine the current maximum date - ensure no time components are set. */
	_getMinMaxDate: function( inst, minMax ) {
		return this._determineDate( inst, this._get( inst, minMax + "Date" ), null );
	},

	/* Find the number of days in a given month. */
	_getDaysInMonth: function( year, month ) {
		return 32 - this._daylightSavingAdjust( new Date( year, month, 32 ) ).getDate();
	},

	/* Find the day of the week of the first of a month. */
	_getFirstDayOfMonth: function( year, month ) {
		return new Date( year, month, 1 ).getDay();
	},

	/* Determines if we should allow a "next/prev" month display change. */
	_canAdjustMonth: function( inst, offset, curYear, curMonth ) {
		var numMonths = this._getNumberOfMonths( inst ),
			date = this._daylightSavingAdjust( new Date( curYear,
			curMonth + ( offset < 0 ? offset : numMonths[ 0 ] * numMonths[ 1 ] ), 1 ) );

		if ( offset < 0 ) {
			date.setDate( this._getDaysInMonth( date.getFullYear(), date.getMonth() ) );
		}
		return this._isInRange( inst, date );
	},

	/* Is the given date in the accepted range? */
	_isInRange: function( inst, date ) {
		var yearSplit, currentYear,
			minDate = this._getMinMaxDate( inst, "min" ),
			maxDate = this._getMinMaxDate( inst, "max" ),
			minYear = null,
			maxYear = null,
			years = this._get( inst, "yearRange" );
			if ( years ) {
				yearSplit = years.split( ":" );
				currentYear = new Date().getFullYear();
				minYear = parseInt( yearSplit[ 0 ], 10 );
				maxYear = parseInt( yearSplit[ 1 ], 10 );
				if ( yearSplit[ 0 ].match( /[+\-].*/ ) ) {
					minYear += currentYear;
				}
				if ( yearSplit[ 1 ].match( /[+\-].*/ ) ) {
					maxYear += currentYear;
				}
			}

		return ( ( !minDate || date.getTime() >= minDate.getTime() ) &&
			( !maxDate || date.getTime() <= maxDate.getTime() ) &&
			( !minYear || date.getFullYear() >= minYear ) &&
			( !maxYear || date.getFullYear() <= maxYear ) );
	},

	/* Provide the configuration settings for formatting/parsing. */
	_getFormatConfig: function( inst ) {
		var shortYearCutoff = this._get( inst, "shortYearCutoff" );
		shortYearCutoff = ( typeof shortYearCutoff !== "string" ? shortYearCutoff :
			new Date().getFullYear() % 100 + parseInt( shortYearCutoff, 10 ) );
		return { shortYearCutoff: shortYearCutoff,
			dayNamesShort: this._get( inst, "dayNamesShort" ), dayNames: this._get( inst, "dayNames" ),
			monthNamesShort: this._get( inst, "monthNamesShort" ), monthNames: this._get( inst, "monthNames" ) };
	},

	/* Format the given date for display. */
	_formatDate: function( inst, day, month, year ) {
		if ( !day ) {
			inst.currentDay = inst.selectedDay;
			inst.currentMonth = inst.selectedMonth;
			inst.currentYear = inst.selectedYear;
		}
		var date = ( day ? ( typeof day === "object" ? day :
			this._daylightSavingAdjust( new Date( year, month, day ) ) ) :
			this._daylightSavingAdjust( new Date( inst.currentYear, inst.currentMonth, inst.currentDay ) ) );
		return this.formatDate( this._get( inst, "dateFormat" ), date, this._getFormatConfig( inst ) );
	}
} );

/*
 * Bind hover events for datepicker elements.
 * Done via delegate so the binding only occurs once in the lifetime of the parent div.
 * Global datepicker_instActive, set by _updateDatepicker allows the handlers to find their way back to the active picker.
 */
function datepicker_bindHover( dpDiv ) {
	var selector = "button, .ui-datepicker-prev, .ui-datepicker-next, .ui-datepicker-calendar td a";
	return dpDiv.on( "mouseout", selector, function() {
			$( this ).removeClass( "ui-state-hover" );
			if ( this.className.indexOf( "ui-datepicker-prev" ) !== -1 ) {
				$( this ).removeClass( "ui-datepicker-prev-hover" );
			}
			if ( this.className.indexOf( "ui-datepicker-next" ) !== -1 ) {
				$( this ).removeClass( "ui-datepicker-next-hover" );
			}
		} )
		.on( "mouseover", selector, datepicker_handleMouseover );
}

function datepicker_handleMouseover() {
	if ( !$.datepicker._isDisabledDatepicker( datepicker_instActive.inline ? datepicker_instActive.dpDiv.parent()[ 0 ] : datepicker_instActive.input[ 0 ] ) ) {
		$( this ).parents( ".ui-datepicker-calendar" ).find( "a" ).removeClass( "ui-state-hover" );
		$( this ).addClass( "ui-state-hover" );
		if ( this.className.indexOf( "ui-datepicker-prev" ) !== -1 ) {
			$( this ).addClass( "ui-datepicker-prev-hover" );
		}
		if ( this.className.indexOf( "ui-datepicker-next" ) !== -1 ) {
			$( this ).addClass( "ui-datepicker-next-hover" );
		}
	}
}

/* jQuery extend now ignores nulls! */
function datepicker_extendRemove( target, props ) {
	$.extend( target, props );
	for ( var name in props ) {
		if ( props[ name ] == null ) {
			target[ name ] = props[ name ];
		}
	}
	return target;
}

/* Invoke the datepicker functionality.
   @param  options  string - a command, optionally followed by additional parameters or
					Object - settings for attaching new datepicker functionality
   @return  jQuery object */
$.fn.datepicker = function( options ) {

	/* Verify an empty collection wasn't passed - Fixes #6976 */
	if ( !this.length ) {
		return this;
	}

	/* Initialise the date picker. */
	if ( !$.datepicker.initialized ) {
		$( document ).on( "mousedown", $.datepicker._checkExternalClick );
		$.datepicker.initialized = true;
	}

	/* Append datepicker main container to body if not exist. */
	if ( $( "#" + $.datepicker._mainDivId ).length === 0 ) {
		$( "body" ).append( $.datepicker.dpDiv );
	}

	var otherArgs = Array.prototype.slice.call( arguments, 1 );
	if ( typeof options === "string" && ( options === "isDisabled" || options === "getDate" || options === "widget" ) ) {
		return $.datepicker[ "_" + options + "Datepicker" ].
			apply( $.datepicker, [ this[ 0 ] ].concat( otherArgs ) );
	}
	if ( options === "option" && arguments.length === 2 && typeof arguments[ 1 ] === "string" ) {
		return $.datepicker[ "_" + options + "Datepicker" ].
			apply( $.datepicker, [ this[ 0 ] ].concat( otherArgs ) );
	}
	return this.each( function() {
		typeof options === "string" ?
			$.datepicker[ "_" + options + "Datepicker" ].
				apply( $.datepicker, [ this ].concat( otherArgs ) ) :
			$.datepicker._attachDatepicker( this, options );
	} );
};

$.datepicker = new Datepicker(); // singleton instance
$.datepicker.initialized = false;
$.datepicker.uuid = new Date().getTime();
$.datepicker.version = "1.12.1";

var widgetsDatepicker = $.datepicker;




}));
/**
 * bxSlider v4.2.12
 * Copyright 2013-2015 Steven Wanderski
 * Written while drinking Belgian ales and listening to jazz
 * Licensed under MIT (http://opensource.org/licenses/MIT)
 */
!function(t){var e={mode:"horizontal",slideSelector:"",infiniteLoop:!0,hideControlOnEnd:!1,speed:500,easing:null,slideMargin:0,startSlide:0,randomStart:!1,captions:!1,ticker:!1,tickerHover:!1,adaptiveHeight:!1,adaptiveHeightSpeed:500,video:!1,useCSS:!0,preloadImages:"visible",responsive:!0,slideZIndex:50,wrapperClass:"bx-wrapper",touchEnabled:!0,swipeThreshold:50,oneToOneTouch:!0,preventDefaultSwipeX:!0,preventDefaultSwipeY:!1,ariaLive:!0,ariaHidden:!0,keyboardEnabled:!1,pager:!0,pagerType:"full",pagerShortSeparator:" / ",pagerSelector:null,buildPager:null,pagerCustom:null,controls:!0,nextText:"Next",prevText:"Prev",nextSelector:null,prevSelector:null,autoControls:!1,startText:"Start",stopText:"Stop",autoControlsCombine:!1,autoControlsSelector:null,auto:!1,pause:4e3,autoStart:!0,autoDirection:"next",stopAutoOnClick:!1,autoHover:!1,autoDelay:0,autoSlideForOnePage:!1,minSlides:1,maxSlides:1,moveSlides:0,slideWidth:0,shrinkItems:!1,onSliderLoad:function(){return!0},onSlideBefore:function(){return!0},onSlideAfter:function(){return!0},onSlideNext:function(){return!0},onSlidePrev:function(){return!0},onSliderResize:function(){return!0}};t.fn.bxSlider=function(n){if(0===this.length)return this;if(this.length>1)return this.each(function(){t(this).bxSlider(n)}),this;var s={},o=this,r=t(window).width(),a=t(window).height();if(!t(o).data("bxSlider")){var l=function(){t(o).data("bxSlider")||(s.settings=t.extend({},e,n),s.settings.slideWidth=parseInt(s.settings.slideWidth),s.children=o.children(s.settings.slideSelector),s.children.length<s.settings.minSlides&&(s.settings.minSlides=s.children.length),s.children.length<s.settings.maxSlides&&(s.settings.maxSlides=s.children.length),s.settings.randomStart&&(s.settings.startSlide=Math.floor(Math.random()*s.children.length)),s.active={index:s.settings.startSlide},s.carousel=s.settings.minSlides>1||s.settings.maxSlides>1,s.carousel&&(s.settings.preloadImages="all"),s.minThreshold=s.settings.minSlides*s.settings.slideWidth+(s.settings.minSlides-1)*s.settings.slideMargin,s.maxThreshold=s.settings.maxSlides*s.settings.slideWidth+(s.settings.maxSlides-1)*s.settings.slideMargin,s.working=!1,s.controls={},s.interval=null,s.animProp="vertical"===s.settings.mode?"top":"left",s.usingCSS=s.settings.useCSS&&"fade"!==s.settings.mode&&function(){for(var t=document.createElement("div"),e=["WebkitPerspective","MozPerspective","OPerspective","msPerspective"],i=0;i<e.length;i++)if(void 0!==t.style[e[i]])return s.cssPrefix=e[i].replace("Perspective","").toLowerCase(),s.animProp="-"+s.cssPrefix+"-transform",!0;return!1}(),"vertical"===s.settings.mode&&(s.settings.maxSlides=s.settings.minSlides),o.data("origStyle",o.attr("style")),o.children(s.settings.slideSelector).each(function(){t(this).data("origStyle",t(this).attr("style"))}),d())},d=function(){var e=s.children.eq(s.settings.startSlide);o.wrap('<div class="'+s.settings.wrapperClass+'"><div class="bx-viewport"></div></div>'),s.viewport=o.parent(),s.settings.ariaLive&&!s.settings.ticker&&s.viewport.attr("aria-live","polite"),s.loader=t('<div class="bx-loading" />'),s.viewport.prepend(s.loader),o.css({width:"horizontal"===s.settings.mode?1e3*s.children.length+215+"%":"auto",position:"relative"}),s.usingCSS&&s.settings.easing?o.css("-"+s.cssPrefix+"-transition-timing-function",s.settings.easing):s.settings.easing||(s.settings.easing="swing"),s.viewport.css({width:"100%",overflow:"hidden",position:"relative"}),s.viewport.parent().css({maxWidth:u()}),s.children.css({float:"horizontal"===s.settings.mode?"left":"none",listStyle:"none",position:"relative"}),s.children.css("width",h()),"horizontal"===s.settings.mode&&s.settings.slideMargin>0&&s.children.css("marginRight",s.settings.slideMargin),"vertical"===s.settings.mode&&s.settings.slideMargin>0&&s.children.css("marginBottom",s.settings.slideMargin),"fade"===s.settings.mode&&(s.children.css({position:"absolute",zIndex:0,display:"none"}),s.children.eq(s.settings.startSlide).css({zIndex:s.settings.slideZIndex,display:"block"})),s.controls.el=t('<div class="bx-controls" />'),s.settings.captions&&P(),s.active.last=s.settings.startSlide===f()-1,s.settings.video&&o.fitVids(),("all"===s.settings.preloadImages||s.settings.ticker)&&(e=s.children),s.settings.ticker?s.settings.pager=!1:(s.settings.controls&&C(),s.settings.auto&&s.settings.autoControls&&T(),s.settings.pager&&w(),(s.settings.controls||s.settings.autoControls||s.settings.pager)&&s.viewport.after(s.controls.el)),c(e,g)},c=function(e,i){var n=e.find('img:not([src=""]), iframe').length,s=0;return 0===n?void i():void e.find('img:not([src=""]), iframe').each(function(){t(this).one("load error",function(){++s===n&&i()}).each(function(){this.complete&&t(this).trigger("load")})})},g=function(){if(s.settings.infiniteLoop&&"fade"!==s.settings.mode&&!s.settings.ticker){var e="vertical"===s.settings.mode?s.settings.minSlides:s.settings.maxSlides,i=s.children.slice(0,e).clone(!0).addClass("bx-clone"),n=s.children.slice(-e).clone(!0).addClass("bx-clone");s.settings.ariaHidden&&(i.attr("aria-hidden",!0),n.attr("aria-hidden",!0)),o.append(i).prepend(n)}s.loader.remove(),m(),"vertical"===s.settings.mode&&(s.settings.adaptiveHeight=!0),s.viewport.height(p()),o.redrawSlider(),s.settings.onSliderLoad.call(o,s.active.index),s.initialized=!0,s.settings.responsive&&t(window).bind("resize",Z),s.settings.auto&&s.settings.autoStart&&(f()>1||s.settings.autoSlideForOnePage)&&H(),s.settings.ticker&&W(),s.settings.pager&&I(s.settings.startSlide),s.settings.controls&&D(),s.settings.touchEnabled&&!s.settings.ticker&&N(),s.settings.keyboardEnabled&&!s.settings.ticker&&t(document).keydown(F)},p=function(){var e=0,n=t();if("vertical"===s.settings.mode||s.settings.adaptiveHeight)if(s.carousel){var o=1===s.settings.moveSlides?s.active.index:s.active.index*x();for(n=s.children.eq(o),i=1;i<=s.settings.maxSlides-1;i++)n=o+i>=s.children.length?n.add(s.children.eq(i-1)):n.add(s.children.eq(o+i))}else n=s.children.eq(s.active.index);else n=s.children;return"vertical"===s.settings.mode?(n.each(function(i){e+=t(this).outerHeight()}),s.settings.slideMargin>0&&(e+=s.settings.slideMargin*(s.settings.minSlides-1))):e=Math.max.apply(Math,n.map(function(){return t(this).outerHeight(!1)}).get()),"border-box"===s.viewport.css("box-sizing")?e+=parseFloat(s.viewport.css("padding-top"))+parseFloat(s.viewport.css("padding-bottom"))+parseFloat(s.viewport.css("border-top-width"))+parseFloat(s.viewport.css("border-bottom-width")):"padding-box"===s.viewport.css("box-sizing")&&(e+=parseFloat(s.viewport.css("padding-top"))+parseFloat(s.viewport.css("padding-bottom"))),e},u=function(){var t="100%";return s.settings.slideWidth>0&&(t="horizontal"===s.settings.mode?s.settings.maxSlides*s.settings.slideWidth+(s.settings.maxSlides-1)*s.settings.slideMargin:s.settings.slideWidth),t},h=function(){var t=s.settings.slideWidth,e=s.viewport.width();if(0===s.settings.slideWidth||s.settings.slideWidth>e&&!s.carousel||"vertical"===s.settings.mode)t=e;else if(s.settings.maxSlides>1&&"horizontal"===s.settings.mode){if(e>s.maxThreshold)return t;e<s.minThreshold?t=(e-s.settings.slideMargin*(s.settings.minSlides-1))/s.settings.minSlides:s.settings.shrinkItems&&(t=Math.floor((e+s.settings.slideMargin)/Math.ceil((e+s.settings.slideMargin)/(t+s.settings.slideMargin))-s.settings.slideMargin))}return t},v=function(){var t=1,e=null;return"horizontal"===s.settings.mode&&s.settings.slideWidth>0?s.viewport.width()<s.minThreshold?t=s.settings.minSlides:s.viewport.width()>s.maxThreshold?t=s.settings.maxSlides:(e=s.children.first().width()+s.settings.slideMargin,t=Math.floor((s.viewport.width()+s.settings.slideMargin)/e)):"vertical"===s.settings.mode&&(t=s.settings.minSlides),t},f=function(){var t=0,e=0,i=0;if(s.settings.moveSlides>0)if(s.settings.infiniteLoop)t=Math.ceil(s.children.length/x());else for(;e<s.children.length;)++t,e=i+v(),i+=s.settings.moveSlides<=v()?s.settings.moveSlides:v();else t=Math.ceil(s.children.length/v());return t},x=function(){return s.settings.moveSlides>0&&s.settings.moveSlides<=v()?s.settings.moveSlides:v()},m=function(){var t,e,i;s.children.length>s.settings.maxSlides&&s.active.last&&!s.settings.infiniteLoop?"horizontal"===s.settings.mode?(e=s.children.last(),t=e.position(),S(-(t.left-(s.viewport.width()-e.outerWidth())),"reset",0)):"vertical"===s.settings.mode&&(i=s.children.length-s.settings.minSlides,t=s.children.eq(i).position(),S(-t.top,"reset",0)):(t=s.children.eq(s.active.index*x()).position(),s.active.index===f()-1&&(s.active.last=!0),void 0!==t&&("horizontal"===s.settings.mode?S(-t.left,"reset",0):"vertical"===s.settings.mode&&S(-t.top,"reset",0)))},S=function(e,i,n,r){var a,l;s.usingCSS?(l="vertical"===s.settings.mode?"translate3d(0, "+e+"px, 0)":"translate3d("+e+"px, 0, 0)",o.css("-"+s.cssPrefix+"-transition-duration",n/1e3+"s"),"slide"===i?(o.css(s.animProp,l),0!==n?o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(e){t(e.target).is(o)&&(o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),q())}):q()):"reset"===i?o.css(s.animProp,l):"ticker"===i&&(o.css("-"+s.cssPrefix+"-transition-timing-function","linear"),o.css(s.animProp,l),0!==n?o.bind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",function(e){t(e.target).is(o)&&(o.unbind("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"),S(r.resetValue,"reset",0),L())}):(S(r.resetValue,"reset",0),L()))):(a={},a[s.animProp]=e,"slide"===i?o.animate(a,n,s.settings.easing,function(){q()}):"reset"===i?o.css(s.animProp,e):"ticker"===i&&o.animate(a,n,"linear",function(){S(r.resetValue,"reset",0),L()}))},b=function(){for(var e="",i="",n=f(),o=0;o<n;o++)i="",s.settings.buildPager&&t.isFunction(s.settings.buildPager)||s.settings.pagerCustom?(i=s.settings.buildPager(o),s.pagerEl.addClass("bx-custom-pager")):(i=o+1,s.pagerEl.addClass("bx-default-pager")),e+='<div class="bx-pager-item"><a href="" data-slide-index="'+o+'" class="bx-pager-link">'+i+"</a></div>";s.pagerEl.html(e)},w=function(){s.settings.pagerCustom?s.pagerEl=t(s.settings.pagerCustom):(s.pagerEl=t('<div class="bx-pager" />'),s.settings.pagerSelector?t(s.settings.pagerSelector).html(s.pagerEl):s.controls.el.addClass("bx-has-pager").append(s.pagerEl),b()),s.pagerEl.on("click touchend","a",z)},C=function(){s.controls.next=t('<a class="bx-next" href="">'+s.settings.nextText+"</a>"),s.controls.prev=t('<a class="bx-prev" href="">'+s.settings.prevText+"</a>"),s.controls.next.bind("click touchend",E),s.controls.prev.bind("click touchend",k),s.settings.nextSelector&&t(s.settings.nextSelector).append(s.controls.next),s.settings.prevSelector&&t(s.settings.prevSelector).append(s.controls.prev),s.settings.nextSelector||s.settings.prevSelector||(s.controls.directionEl=t('<div class="bx-controls-direction" />'),s.controls.directionEl.append(s.controls.prev).append(s.controls.next),s.controls.el.addClass("bx-has-controls-direction").append(s.controls.directionEl))},T=function(){s.controls.start=t('<div class="bx-controls-auto-item"><a class="bx-start" href="">'+s.settings.startText+"</a></div>"),s.controls.stop=t('<div class="bx-controls-auto-item"><a class="bx-stop" href="">'+s.settings.stopText+"</a></div>"),s.controls.autoEl=t('<div class="bx-controls-auto" />'),s.controls.autoEl.on("click",".bx-start",M),s.controls.autoEl.on("click",".bx-stop",y),s.settings.autoControlsCombine?s.controls.autoEl.append(s.controls.start):s.controls.autoEl.append(s.controls.start).append(s.controls.stop),s.settings.autoControlsSelector?t(s.settings.autoControlsSelector).html(s.controls.autoEl):s.controls.el.addClass("bx-has-controls-auto").append(s.controls.autoEl),A(s.settings.autoStart?"stop":"start")},P=function(){s.children.each(function(e){var i=t(this).find("img:first").attr("title");void 0!==i&&(""+i).length&&t(this).append('<div class="bx-caption"><span>'+i+"</span></div>")})},E=function(t){t.preventDefault(),s.controls.el.hasClass("disabled")||(s.settings.auto&&s.settings.stopAutoOnClick&&o.stopAuto(),o.goToNextSlide())},k=function(t){t.preventDefault(),s.controls.el.hasClass("disabled")||(s.settings.auto&&s.settings.stopAutoOnClick&&o.stopAuto(),o.goToPrevSlide())},M=function(t){o.startAuto(),t.preventDefault()},y=function(t){o.stopAuto(),t.preventDefault()},z=function(e){var i,n;e.preventDefault(),s.controls.el.hasClass("disabled")||(s.settings.auto&&s.settings.stopAutoOnClick&&o.stopAuto(),i=t(e.currentTarget),void 0!==i.attr("data-slide-index")&&(n=parseInt(i.attr("data-slide-index")),n!==s.active.index&&o.goToSlide(n)))},I=function(e){var i=s.children.length;return"short"===s.settings.pagerType?(s.settings.maxSlides>1&&(i=Math.ceil(s.children.length/s.settings.maxSlides)),void s.pagerEl.html(e+1+s.settings.pagerShortSeparator+i)):(s.pagerEl.find("a").removeClass("active"),void s.pagerEl.each(function(i,n){t(n).find("a").eq(e).addClass("active")}))},q=function(){if(s.settings.infiniteLoop){var t="";0===s.active.index?t=s.children.eq(0).position():s.active.index===f()-1&&s.carousel?t=s.children.eq((f()-1)*x()).position():s.active.index===s.children.length-1&&(t=s.children.eq(s.children.length-1).position()),t&&("horizontal"===s.settings.mode?S(-t.left,"reset",0):"vertical"===s.settings.mode&&S(-t.top,"reset",0))}s.working=!1,s.settings.onSlideAfter.call(o,s.children.eq(s.active.index),s.oldIndex,s.active.index)},A=function(t){s.settings.autoControlsCombine?s.controls.autoEl.html(s.controls[t]):(s.controls.autoEl.find("a").removeClass("active"),s.controls.autoEl.find("a:not(.bx-"+t+")").addClass("active"))},D=function(){1===f()?(s.controls.prev.addClass("disabled"),s.controls.next.addClass("disabled")):!s.settings.infiniteLoop&&s.settings.hideControlOnEnd&&(0===s.active.index?(s.controls.prev.addClass("disabled"),s.controls.next.removeClass("disabled")):s.active.index===f()-1?(s.controls.next.addClass("disabled"),s.controls.prev.removeClass("disabled")):(s.controls.prev.removeClass("disabled"),s.controls.next.removeClass("disabled")))},H=function(){if(s.settings.autoDelay>0){setTimeout(o.startAuto,s.settings.autoDelay)}else o.startAuto(),t(window).focus(function(){o.startAuto()}).blur(function(){o.stopAuto()});s.settings.autoHover&&o.hover(function(){s.interval&&(o.stopAuto(!0),s.autoPaused=!0)},function(){s.autoPaused&&(o.startAuto(!0),s.autoPaused=null)})},W=function(){var e,i,n,r,a,l,d,c,g=0;"next"===s.settings.autoDirection?o.append(s.children.clone().addClass("bx-clone")):(o.prepend(s.children.clone().addClass("bx-clone")),e=s.children.first().position(),g="horizontal"===s.settings.mode?-e.left:-e.top),S(g,"reset",0),s.settings.pager=!1,s.settings.controls=!1,s.settings.autoControls=!1,s.settings.tickerHover&&(s.usingCSS?(r="horizontal"===s.settings.mode?4:5,s.viewport.hover(function(){i=o.css("-"+s.cssPrefix+"-transform"),n=parseFloat(i.split(",")[r]),S(n,"reset",0)},function(){c=0,s.children.each(function(e){c+="horizontal"===s.settings.mode?t(this).outerWidth(!0):t(this).outerHeight(!0)}),a=s.settings.speed/c,l="horizontal"===s.settings.mode?"left":"top",d=a*(c-Math.abs(parseInt(n))),L(d)})):s.viewport.hover(function(){o.stop()},function(){c=0,s.children.each(function(e){c+="horizontal"===s.settings.mode?t(this).outerWidth(!0):t(this).outerHeight(!0)}),a=s.settings.speed/c,l="horizontal"===s.settings.mode?"left":"top",d=a*(c-Math.abs(parseInt(o.css(l)))),L(d)})),L()},L=function(t){var e,i,n,r=t?t:s.settings.speed,a={left:0,top:0},l={left:0,top:0};"next"===s.settings.autoDirection?a=o.find(".bx-clone").first().position():l=s.children.first().position(),e="horizontal"===s.settings.mode?-a.left:-a.top,i="horizontal"===s.settings.mode?-l.left:-l.top,n={resetValue:i},S(e,"ticker",r,n)},O=function(e){var i=t(window),n={top:i.scrollTop(),left:i.scrollLeft()},s=e.offset();return n.right=n.left+i.width(),n.bottom=n.top+i.height(),s.right=s.left+e.outerWidth(),s.bottom=s.top+e.outerHeight(),!(n.right<s.left||n.left>s.right||n.bottom<s.top||n.top>s.bottom)},F=function(t){var e=document.activeElement.tagName.toLowerCase(),i="input|textarea",n=new RegExp(e,["i"]),s=n.exec(i);if(null==s&&O(o)){if(39===t.keyCode)return E(t),!1;if(37===t.keyCode)return k(t),!1}},N=function(){s.touch={start:{x:0,y:0},end:{x:0,y:0}},s.viewport.bind("touchstart MSPointerDown pointerdown",X),s.viewport.on("click",".bxslider a",function(t){s.viewport.hasClass("click-disabled")&&(t.preventDefault(),s.viewport.removeClass("click-disabled"))})},X=function(t){if(s.controls.el.addClass("disabled"),s.working)t.preventDefault(),s.controls.el.removeClass("disabled");else{s.touch.originalPos=o.position();var e=t.originalEvent,i="undefined"!=typeof e.changedTouches?e.changedTouches:[e];s.touch.start.x=i[0].pageX,s.touch.start.y=i[0].pageY,s.viewport.get(0).setPointerCapture&&(s.pointerId=e.pointerId,s.viewport.get(0).setPointerCapture(s.pointerId)),s.viewport.bind("touchmove MSPointerMove pointermove",V),s.viewport.bind("touchend MSPointerUp pointerup",R),s.viewport.bind("MSPointerCancel pointercancel",Y)}},Y=function(t){S(s.touch.originalPos.left,"reset",0),s.controls.el.removeClass("disabled"),s.viewport.unbind("MSPointerCancel pointercancel",Y),s.viewport.unbind("touchmove MSPointerMove pointermove",V),s.viewport.unbind("touchend MSPointerUp pointerup",R),s.viewport.get(0).releasePointerCapture&&s.viewport.get(0).releasePointerCapture(s.pointerId)},V=function(t){var e=t.originalEvent,i="undefined"!=typeof e.changedTouches?e.changedTouches:[e],n=Math.abs(i[0].pageX-s.touch.start.x),o=Math.abs(i[0].pageY-s.touch.start.y),r=0,a=0;3*n>o&&s.settings.preventDefaultSwipeX?t.preventDefault():3*o>n&&s.settings.preventDefaultSwipeY&&t.preventDefault(),"fade"!==s.settings.mode&&s.settings.oneToOneTouch&&("horizontal"===s.settings.mode?(a=i[0].pageX-s.touch.start.x,r=s.touch.originalPos.left+a):(a=i[0].pageY-s.touch.start.y,r=s.touch.originalPos.top+a),S(r,"reset",0))},R=function(t){s.viewport.unbind("touchmove MSPointerMove pointermove",V),s.controls.el.removeClass("disabled");var e=t.originalEvent,i="undefined"!=typeof e.changedTouches?e.changedTouches:[e],n=0,r=0;s.touch.end.x=i[0].pageX,s.touch.end.y=i[0].pageY,"fade"===s.settings.mode?(r=Math.abs(s.touch.start.x-s.touch.end.x),r>=s.settings.swipeThreshold&&(s.touch.start.x>s.touch.end.x?o.goToNextSlide():o.goToPrevSlide(),o.stopAuto())):("horizontal"===s.settings.mode?(r=s.touch.end.x-s.touch.start.x,n=s.touch.originalPos.left):(r=s.touch.end.y-s.touch.start.y,n=s.touch.originalPos.top),!s.settings.infiniteLoop&&(0===s.active.index&&r>0||s.active.last&&r<0)?S(n,"reset",200):Math.abs(r)>=s.settings.swipeThreshold?(r<0?o.goToNextSlide():o.goToPrevSlide(),o.stopAuto()):S(n,"reset",200)),s.viewport.unbind("touchend MSPointerUp pointerup",R),s.viewport.get(0).releasePointerCapture&&s.viewport.get(0).releasePointerCapture(s.pointerId)},Z=function(e){if(s.initialized)if(s.working)window.setTimeout(Z,10);else{var i=t(window).width(),n=t(window).height();r===i&&a===n||(r=i,a=n,o.redrawSlider(),s.settings.onSliderResize.call(o,s.active.index))}},B=function(t){var e=v();s.settings.ariaHidden&&!s.settings.ticker&&(s.children.attr("aria-hidden","true"),s.children.slice(t,t+e).attr("aria-hidden","false"))},U=function(t){return t<0?s.settings.infiniteLoop?f()-1:s.active.index:t>=f()?s.settings.infiniteLoop?0:s.active.index:t};return o.goToSlide=function(e,i){var n,r,a,l,d=!0,c=0,g={left:0,top:0},u=null;if(s.oldIndex=s.active.index,s.active.index=U(e),!s.working&&s.active.index!==s.oldIndex){if(s.working=!0,d=s.settings.onSlideBefore.call(o,s.children.eq(s.active.index),s.oldIndex,s.active.index),"undefined"!=typeof d&&!d)return s.active.index=s.oldIndex,void(s.working=!1);"next"===i?s.settings.onSlideNext.call(o,s.children.eq(s.active.index),s.oldIndex,s.active.index)||(d=!1):"prev"===i&&(s.settings.onSlidePrev.call(o,s.children.eq(s.active.index),s.oldIndex,s.active.index)||(d=!1)),s.active.last=s.active.index>=f()-1,(s.settings.pager||s.settings.pagerCustom)&&I(s.active.index),s.settings.controls&&D(),"fade"===s.settings.mode?(s.settings.adaptiveHeight&&s.viewport.height()!==p()&&s.viewport.animate({height:p()},s.settings.adaptiveHeightSpeed),s.children.filter(":visible").fadeOut(s.settings.speed).css({zIndex:0}),s.children.eq(s.active.index).css("zIndex",s.settings.slideZIndex+1).fadeIn(s.settings.speed,function(){t(this).css("zIndex",s.settings.slideZIndex),q()})):(s.settings.adaptiveHeight&&s.viewport.height()!==p()&&s.viewport.animate({height:p()},s.settings.adaptiveHeightSpeed),!s.settings.infiniteLoop&&s.carousel&&s.active.last?"horizontal"===s.settings.mode?(u=s.children.eq(s.children.length-1),g=u.position(),c=s.viewport.width()-u.outerWidth()):(n=s.children.length-s.settings.minSlides,g=s.children.eq(n).position()):s.carousel&&s.active.last&&"prev"===i?(r=1===s.settings.moveSlides?s.settings.maxSlides-x():(f()-1)*x()-(s.children.length-s.settings.maxSlides),u=o.children(".bx-clone").eq(r),g=u.position()):"next"===i&&0===s.active.index?(g=o.find("> .bx-clone").eq(s.settings.maxSlides).position(),s.active.last=!1):e>=0&&(l=e*parseInt(x()),g=s.children.eq(l).position()),"undefined"!=typeof g?(a="horizontal"===s.settings.mode?-(g.left-c):-g.top,S(a,"slide",s.settings.speed)):s.working=!1),s.settings.ariaHidden&&B(s.active.index*x())}},o.goToNextSlide=function(){if(s.settings.infiniteLoop||!s.active.last){var t=parseInt(s.active.index)+1;o.goToSlide(t,"next")}},o.goToPrevSlide=function(){if(s.settings.infiniteLoop||0!==s.active.index){var t=parseInt(s.active.index)-1;o.goToSlide(t,"prev")}},o.startAuto=function(t){s.interval||(s.interval=setInterval(function(){"next"===s.settings.autoDirection?o.goToNextSlide():o.goToPrevSlide()},s.settings.pause),s.settings.autoControls&&t!==!0&&A("stop"))},o.stopAuto=function(t){s.interval&&(clearInterval(s.interval),s.interval=null,s.settings.autoControls&&t!==!0&&A("start"))},o.getCurrentSlide=function(){return s.active.index},o.getCurrentSlideElement=function(){return s.children.eq(s.active.index)},o.getSlideElement=function(t){return s.children.eq(t)},o.getSlideCount=function(){return s.children.length},o.isWorking=function(){return s.working},o.redrawSlider=function(){s.children.add(o.find(".bx-clone")).outerWidth(h()),s.viewport.css("height",p()),s.settings.ticker||m(),s.active.last&&(s.active.index=f()-1),s.active.index>=f()&&(s.active.last=!0),s.settings.pager&&!s.settings.pagerCustom&&(b(),I(s.active.index)),s.settings.ariaHidden&&B(s.active.index*x())},o.destroySlider=function(){s.initialized&&(s.initialized=!1,t(".bx-clone",this).remove(),s.children.each(function(){void 0!==t(this).data("origStyle")?t(this).attr("style",t(this).data("origStyle")):t(this).removeAttr("style")}),void 0!==t(this).data("origStyle")?this.attr("style",t(this).data("origStyle")):t(this).removeAttr("style"),t(this).unwrap().unwrap(),s.controls.el&&s.controls.el.remove(),s.controls.next&&s.controls.next.remove(),s.controls.prev&&s.controls.prev.remove(),s.pagerEl&&s.settings.controls&&!s.settings.pagerCustom&&s.pagerEl.remove(),t(".bx-caption",this).remove(),s.controls.autoEl&&s.controls.autoEl.remove(),clearInterval(s.interval),s.settings.responsive&&t(window).unbind("resize",Z),s.settings.keyboardEnabled&&t(document).unbind("keydown",F),t(this).removeData("bxSlider"))},o.reloadSlider=function(e){void 0!==e&&(n=e),o.destroySlider(),l(),t(o).data("bxSlider",this)},l(),t(o).data("bxSlider",this),this}}}(jQuery);
/* jshint browser:true */

(function() {
  var appCache = window.applicationCache;

  // addEvents();

  // appCache.update();

  function addEvents() {
    var events = ['cached', 'checking', 'downloading', 'error', 'noupdate',
    'obsolete', 'progress', 'updateready'];
    events.forEach(function(type) {
      var handler = handleCacheEvent;
      if (/error/.test(type)) {
        handler = handleCacheError;
      }
      appCache.addEventListener(type, handler, false);
    });
  }

  function handleCacheEvent(e) {
    console.debug(e);
    if (e.type === 'updateready' && appCache.status === appCache.UPDATEREADY) {
      appCache.swapCache();
      if (window.confirm('A new version of this site is available. Load it?')) {
        window.location.reload();
      }
    }
  }

  function handleCacheError(e) {
    console.error('Error: Cache failed to update!');
  }
})();
(function() {

  _.mixin(_.assign({
    numeric: numeric,
    deepFreeze: deepFreeze,
    compactObject: compactObject,
    unflatten: unflatten,
    guid: guid
  }, s.exports()));

  function numeric(input) {
    if (!input) {
      return null;
    }
    return input.replace(/\D/g, '');
  }

  function deepFreeze(o) {
    Object.freeze(o);

    var oIsFunction = typeof o === 'function';
    var hasOwnProp = Object.prototype.hasOwnProperty;

    Object.getOwnPropertyNames(o).forEach(function(prop) {
      if (hasOwnProp.call(o, prop) && (oIsFunction ? prop !== 'caller' && prop !== 'callee' && prop !== 'arguments' : true) && o[prop] !== null && (typeof o[prop] === 'object' || typeof o[prop] === 'function') && !Object.isFrozen(o[prop])) {
        deepFreeze(o[prop]);
      }
    });

    return o;
  }

  function compactObject(object) {
    _.map(object, function(value, key, object) {
      if (!!!value || (_.isString(value) && _.trim(value).length === 0)) {
        delete object[key];
      }
    });
    return object;
  }
  
  function unflatten(list, rootIds, idKey, parentIdKey, childrenKey) {
    var data = _.cloneDeep(list), result;
    var composed = _.groupBy(data, parentIdKey);
    _.each(composed, function(children, parentId) {
      if (_.isArray(rootIds) && _.include(rootIds, parentId)) {
        return;
      }
      if (_.isString(rootIds) && rootIds === parentId) {
        return;
      }
      var query = {}, parent;
      query[idKey] = parentId;
      parent = _.find(data, query);
      if (!parent) {
        debugger;
      }
      parent[childrenKey] = children;
    });
    if (_.isArray(rootIds)) {
      result = [];
      _.each(rootIds, function(id) {
        var root = composed[id];
        if (!root) return;
        result.push(composed[id]);
      });
    } else {
      result = composed[rootIds];
    }
    return result;
  }
  
  function guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0;
      var v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
})();
/* jshint browser:true */

(function() {

  var target, platform;
  if(navigator.userAgent.match(/Android/)) {
    target = platform = 'android';
  } else if(navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
    target = platform = 'ios';
  } else {
    platform = 'browser';
  }

  // TEST CODE
   platform = 'browser';
  // TEST CODE

  window.env = {
    // cordova: /^file:/.test(document.URL) && !!target
      cordova: !!platform,
      platform : platform
  };

  if (window.env.cordova) {
    document.writeln('<script type="text/javascript" src="../'+platform+'/cordova.js"></script>');
  }

  $(document).on('hidekeyboard', function(e) {
    $('#fixLayout').height(1);
  });
  $(document).on('showkeyboard', function(e) {
    setTimeout(function() {
      $('#fixLayout').height(0);
    }, 100);
 });
})();
!function () { 'use strict';
/* common/common.module.js */

angular
  .module('easi.common', [
    'ngAnimate',
    'ngSanitize',
    'ngCordova',
    'ngTouch',
    'ui.router',
    'checklist-model',
    'ui.grid',
    'ui.grid.edit',
    'ui.grid.cellNav',
    'ui.grid.pagination',
    'ui.grid.selection',
    'ui.grid.rowEdit',
    'ui.tree',
    'fusioncharts',
    'angularMoment',
    'xml',
    'swipe',
    'easi.core',
    'easi.api',
    'easi.common.legacy',
    'easi.common.shared'
  ])
  .constant('doryConst', _.deepFreeze({
    STR: 'str',
    ARR: [
      'arr1', 'arr2', 'arr3'
    ],
    ARR_OBJ: [
      {
        AOSTR: 'aostr1'
      },
      {
        AOSTR: 'aostr2'
      },
    ],
    OBJ: {
      OSTR: 'ostr',
      DEEP: {
        DOSTR: 'dostr'
      }
    },
      FOOTER: {
          URL: 'layout/footer/footer.tpl.html'
      }
  }))
  .run(["$rootScope", "$log", "$storage", "$user", "$q", "ComSvc", "$cordovaDevice", "$window", "$env", "$state", "$document", "CompanySvc", "ngspublic", "$message", "$cordovaInAppBrowser", "$UUID", "$cordovaPushV5", "$cordovaToast", "$cordovaLocalNotification", "$cordovaGeolocation", "$http", "doryConst", function($rootScope, $log, $storage, $user, $q, ComSvc, $cordovaDevice, $window, $env, $state, $document, CompanySvc, ngspublic, $message, $cordovaInAppBrowser, $UUID, $cordovaPushV5, $cordovaToast, $cordovaLocalNotification, $cordovaGeolocation, $http, doryConst) {

      // 지하철 버스 URL 저장
      
      $storage.get('MAP_INFO_URL')
      .then(function(data){
          if(_.isBlank(data)) {
            
            ComSvc.selectCommonInfo('MAP_INFO_URL')
            .then(function(resultData){
              // 지하철
              var findObj = _.find(resultData.user.resultData.commonData, {COMMON_INFO_VALUE1 : 'M2'});
              $rootScope.subWayUrl = findObj.COMMON_INFO_VALUE2;
              // 버스
              findObj = _.find(resultData.user.resultData.commonData, {COMMON_INFO_VALUE1 : 'M1'});
              $rootScope.busUrl = findObj.COMMON_INFO_VALUE2;
              
              $storage.set('MAP_INFO_URL', resultData.user.resultData.commonData);
            })
             
          }else{
            // 지하철
            var findObj = _.find(data, {COMMON_INFO_VALUE1 : 'M2'});
            $rootScope.subWayUrl = findObj.COMMON_INFO_VALUE2;
            // 버스
            findObj = _.find(data, {COMMON_INFO_VALUE1 : 'M1'});
            $rootScope.busUrl = findObj.COMMON_INFO_VALUE2;
          }
      })
    
    
      $document.on('backbutton', function (e) {

          if($rootScope.stateHistory.length <= 1){
              if ($message.confirm('종료하시겠습니까?')) {
                  if (navigator.app) {

                      // 자동로그인여부
                      $storage.get('AUTO_LOGIN')
                          .then(function(data){
                              if(data !== 'Y') {
                                  $user.destroy();
                              }
                              navigator.app.exitApp();
                          })
                          .catch(function(err){
                              $user.destroy();
                              navigator.app.exitApp();
                          });
                  } else if (navigator.device) {
                      // 자동로그인여부
                      $storage.get('AUTO_LOGIN')
                          .then(function(data){
                              if(data !== 'Y') {
                                  $user.destroy();
                              }
                              navigator.device.exitApp();
                          })
                          .catch(function(err){
                              $user.destroy();
                              navigator.device.exitApp();
                          });
                  }
              }
          }else{
              $rootScope.prevView();
          }
          return false;
      });

      // $document.on('click', 'a[href^="http"]', function (e) {
      //     $bdExtension.browser(this.href);
      //     return false;
      // });

      $document.on('keydown', function (e) {
          // 키보드 엔터 누를때 키보드 숨기기
          if ($env.cordova && e.keyCode === 13 && /input/i.test(e.target.nodeName)) {
              setTimeout(function(){
                  $(':focus').blur();
              });
          }
      });

      $rootScope.stateHistory = [];
      $rootScope.menuTitle = '';
      $rootScope.doryConst = doryConst;

      $env.get()
          .then(function(data){
              $rootScope.envEndpoint = data.endPoint;
          });

      // $UUID.get()
      //     .then(function(uuid){
      //         //$rootScope.UUID = uuid;
      //     });

      $rootScope.searchAddrByGeocode = function(){
          var d = $q.defer();
          var startTime = Date.now();
          var timerName = '(Stopwatch) {' + _.guid().substr(0, 8) + '} searchAddrByGeocode';
          $log.log('request:', timerName);
          console.time(timerName);
          $cordovaGeolocation.getCurrentPosition({timeout: 10000, enableHighAccuracy: false})
              .then(function(position){
                  $log.log('Latitude: '          + position.coords.latitude          + '\n' +
                      'Longitude: '         + position.coords.longitude         + '\n' +
                      'Altitude: '          + position.coords.altitude          + '\n' +
                      'Accuracy: '          + position.coords.accuracy          + '\n' +
                      'Altitude Accuracy: ' + position.coords.altitudeAccuracy  + '\n' +
                      'Heading: '           + position.coords.heading           + '\n' +
                      'Speed: '             + position.coords.speed             + '\n' +
                      'Timestamp: '         + position.timestamp                + '\n');

                  var request = {
                      headers: {
                          'Content-Type': 'application/json',
                          'X-Naver-Client-Id' : 'g1KIbGg2dcuRPTvmhjxo',
                          'X-Naver-Client-Secret' : 'cPoSdMV7Ry'
                      },
                      url: 'https://openapi.naver.com/v1/map/reversegeocode?encoding=utf-8&coordType=latlng&query='+position.coords.longitude+','+position.coords.latitude,
                      method: 'GET'
                  };
                  $http(request)
                      .then(function(response) {
                          $log.log('response:', timerName, Date.now() - startTime);
                          console.timeEnd(timerName);
                          d.resolve(response.data.result);
                      })
                      .catch(function(rejection) {
                          $log.error('responseError:', timerName, Date.now() - startTime);
                          console.timeEnd(timerName);
                          d.reject(rejection);
                      });
              })
              .catch(function(error){
                  $log.error('responseError:', timerName, Date.now() - startTime);
                  console.timeEnd(timerName);
                  d.reject(error);
              });

          return d.promise;
      }

    $rootScope.toggleLeft = function() {
      // 이전에 오픈했던 메뉴 닫기
      if($rootScope.menuOpen = !$rootScope.menuOpen) {
        try {
        $('#mgnb').scrollTop(0);
        } catch(e) {}
      }
    };

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      $rootScope.menuOpen = false;
      if(toState.data) {
          $rootScope.viewDepth = toState.data.depth;
          $rootScope.menuTitle = toState.data.menuTitle;
          if($rootScope.viewDepth <= 1) {
              $rootScope.stateHistory = [];
          }
      } else {
          $rootScope.viewDepth = 0;
      }
        $rootScope.stateHistory.push(toState);
    });

    // 디바이스 정보 저장
    try{
      $rootScope.DEVICE_MODEL = $cordovaDevice.getModel();
      $rootScope.DEVICE_VERSION = $cordovaDevice.getVersion();
    }catch(e){
      $rootScope.DEVICE_MODEL = '';
      $rootScope.DEVICE_VERSION = '';
    }

      $rootScope.UUID = ''; // I use a localStorage variable to persist the token

      if(window.env.platform !== 'browser'){
          $cordovaPushV5.initialize(  // important to initialize with the multidevice structure !!
              {
                  android: {
                      senderID: "1:1039231952564:android:ad6d8727f100fedf"
                  },
                  ios: {
                      alert: 'true',
                      badge: true,
                      sound: 'true',
                      clearBadge: true
                  },
                  windows: {}
              }
          ).then(function (result) {
              $log.log('cordovaPushV5.initialize', result)
              $cordovaPushV5.onNotification();
              $cordovaPushV5.onError();
              $cordovaPushV5.register().then(function (resultreg) {
                  $rootScope.UUID = resultreg;
                  $log.log('tokenId', resultreg)
                  // SEND THE TOKEN TO THE SERVER, best associated with your device id and user
              }, function (err) {
                  $log.error(err)
                  // handle error
              });
          });
      }

      // 푸시 이벤트
      $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, notification) {
          $log.log('Push Message', notification.message)
          if (notification.additionalData.foreground === false) {
              // do something if the app is in foreground while receiving to push - handle in app push handling
          } else {
              // handle push messages while app is in background or not started
              $cordovaToast.showShortTop(notification.message);
              $cordovaLocalNotification.schedule({
                  id: 1,
                  title: notification.title,
                  text: notification.message
              }).then(function (result) {

              });
          }
          if (window.env.platform === 'ios') {
              if (notification.additionalData.badge) {
                  $cordovaPushV5.setBadgeNumber(NewNumber).then(function (result) {
                      // OK
                  }, function (err) {
                      // handle error
                  });
              }
          }

          $cordovaPushV5.finish().then(function (result) {
              // OK finished - works only with the dev-next version of pushV5.js in ngCordova as of February 8, 2016
          }, function (err) {
              // handle error
          });

      });
      $rootScope.$on('$cordovaPushV5:errorOccurred', function(event, error) {
          $log.log("gcm error ", error);
      });
      ////////////////////////////////////////////////////////////////////////////////////////////

      // ----- "Scheduling" events

      // A local notification was scheduled
      $rootScope.$on('$cordovaLocalNotification:schedule', function(event, notification, state) {
          $log.debug('$cordovaLocalNotification:schedule', event, notification, state);
      });
      // A local notification was triggered
      $rootScope.$on('$cordovaLocalNotification:trigger', function(event, notification, state) {
          $log.debug('$cordovaLocalNotification:trigger', event, notification, state);
      });

      // ----- "Update" events

      // A local notification was updated
      $rootScope.$on('$cordovaLocalNotification:update', function(event, notification, state) {
          $log.debug('$cordovaLocalNotification:update', event, notification, state);
      });

      // ----- "Clear" events

      // A local notification was cleared from the notification center
      $rootScope.$on('$cordovaLocalNotification:clear', function(event, notification, state) {
          $log.debug('$cordovaLocalNotification:clear', event, notification, state);
      });
      // All local notifications were cleared from the notification center
      $rootScope.$on('$cordovaLocalNotification:clearall', function(event, state) {
          $log.debug('$cordovaLocalNotification:clearall', event, state);
      });

      // ----- "Cancel" events

      // A local notification was cancelled
      $rootScope.$on('$cordovaLocalNotification:cancel', function(event, notification, state) {
          $log.debug('$cordovaLocalNotification:cancel', event, notification, state);
      });
      // All local notifications were cancelled
      $rootScope.$on('$cordovaLocalNotification:cancelall', function(event, state) {
          $log.debug('$cordovaLocalNotification:cancelall', event, state);
      });

      // ----- Other events

      // A local notification was clicked
      $rootScope.$on('$cordovaLocalNotification:click', function(event, notification, state) {
          $log.debug('$cordovaLocalNotification:click', event, notification, state);
      });

      // A local notification was clicked
      $window.cordova.plugins.notification.local.on('confirm', function (notification, state) {
          $log.debug('$cordovaLocalNotification:confirm', notification, state);
      });

      $rootScope.$on('$cordovaInAppBrowser:loadstop', function(e, event){
          // 주소팝업
          $cordovaInAppBrowser.executeScript({
              code: "callbackJuso();"
          })
              .then(function(data){
                  var callbackData = data[0];
                  if(callbackData.inputYn === 'Y'){
                      $cordovaInAppBrowser.close();
                      $rootScope.callbackFunction(callbackData);
                  }
              });
          
          try{

            if(event.url.match('checkPlusSuccess') || event.url.match('checkPlusFail')){
                // 본인인증팝업
                $cordovaInAppBrowser.executeScript({
                    code: "callbackAuth();"
                })
                    .then(function(data){
                        var callbackData = data[0];
                        if(callbackData.result === 'success'){
                            $rootScope.callbackFunction(callbackData);
                        }
                        $cordovaInAppBrowser.close();
                    });
            }
          }catch(e){}
      });

    $rootScope.openAddrSearchPopup = function(callbackFunction){

          // if(window.env.platform === 'browser'){
          //     //window.open($env.endPoint.service+"/include/modal/jusoPopup2.jsp","_system","width=570,height=420, scrollbars=yes, resizable=yes");
          //     var options = {
          //         location: 'yes',
          //         // clearcache: 'yes',
          //         // toolbar: 'no'
          //         scrollbars: 'yes',
          //         resizable: 'yes'
          //     };
          //     $rootScope.callbackFunction = callbackFunction;
          //     $cordovaInAppBrowser.open($env.endPoint.service+'/include/modal/jusoPopup2.jsp', '_system', options);
          //     $cordovaInAppBrowser.executeScript({
          //         code: "callbackJuso();"
          //     })
          //         .then(function(data){
          //             var callbackData = data[0];
          //             if(callbackData.inputYn === 'Y'){
          //                 $cordovaInAppBrowser.close();
          //                 $rootScope.callbackFunction(callbackData);
          //             }
          //         });
          // } else {
              var options = {
                  location: 'yes',
                  // clearcache: 'yes',
                  // toolbar: 'no'
                  scrollbars: 'yes',
                  resizable: 'yes'
              };
              $rootScope.callbackFunction = callbackFunction;
              $cordovaInAppBrowser.open($env.endPoint.service+'/include/modal/jusoPopup2.jsp', '_blank', options);
          // }
      };

      $rootScope.openAuthCheckPopup = function(encData, callbackFunction){

          var options = {
              location: 'yes',
              // clearcache: 'yes',
              // toolbar: 'no'
              scrollbars: 'yes',
              resizable: 'yes'
          };
          $rootScope.callbackFunction = callbackFunction;
          $cordovaInAppBrowser.open($env.endPoint.service+'/gateCert.jsp?encData=' + encData, '_blank', options);
      };

    /**
     * 에러 로그 저장
     */
    $rootScope.insertErrLog = function(param){

      if(!param) return false;

      if($user.currentUser){
        param.USER_ID = $user.currentUser.getUserId();
      }else{
        param.USER_ID = "NO_LOGIN";
      }
      param.DEVICE_MODEL =  $rootScope.DEVICE_MODEL;
      param.DEVICE_OS_VER = $rootScope.DEVICE_VERSION;

      // 개발용
      $log.debug("insertErrLog param : " + JSON.stringify(param));

      if(!param.USER_ID) {$log.error("insertErrLog parameter err [USER_ID]" ); return false;}
      if(!param.DEVICE_MODEL) {$log.error("insertErrLog parameter err [DEVICE_MODEL]" ); return false;}
      if(!param.DEVICE_OS_VER) {$log.error("insertErrLog parameter err [DEVICE_OS_VER]" ); return false;}

        ComSvc.insertErrLog(param)
        .then(function(data){
          $log.debug("insertErrLog suc");
        })
        .catch(function(data){
          $log.error("insertErrLog err");
        })

    };
      /**
       * 알림발송
       */
      $rootScope.comPushSend = function(memList, msg){
          var d = $q.defer();
          // 요청
          _.forEach(memList ,function(obj, idx){

              if(obj.MEMBER_NM == '' || obj.MEMBER_NM == undefined){
                  obj.MEMBER_NM = obj.COMPANY_NM;
              }

              var param = {};
              param.memberSeq = obj.MEMBER_SEQ;						//[필수] 회원일련번호
              param.sendCompanySeq = obj.COMPANY_SEQ;				//[필수] 회사일련번호
              param.msg = msg;                      				//[필수] 전달 메시지

              CompanySvc.sendMemberPush(param)
                  .then(function(data){
                      if(idx+1 == memList.length){
                          d.resolve();
                      }
                  })
                  .catch(function(err){
                      d.reject(err);
                  });
          });

          return d.promise;
      }
      
      $rootScope.goSubWay = function(){
        
        if(_.isBlank($rootScope.subWayUrl)) return false;
        
        var options = {
                location: 'yes',
                scrollbars: 'yes',
                resizable: 'yes'
            };
            $cordovaInAppBrowser.open($rootScope.subWayUrl, '_blank', options);
            
        
      }
      
      $rootScope.goBus = function(){
        
        if(_.isBlank($rootScope.busUrl)) return false;
        
        var options = {
                location: 'yes',
                scrollbars: 'yes',
                resizable: 'yes'
            };
            $cordovaInAppBrowser.open($rootScope.busUrl, '_blank', options);
        
      }
      
      $rootScope.goAppGuide = function(){
        alert('준비중');
      }

      $rootScope.goView = function(viewId, backup, param){

          if(viewId.match('html#/')){
              if(param){
                  viewId = viewId + '?param=' + JSON.stringify(param);
              }
              $window.location.href = viewId;
          }else{
              var params = {};
              if(param){
                  params.param = param;
              }
              if(!_.isEmpty(backup)){
                  var backupData = {};
                  // _.each(backup, function(val, key){
                  //     if(!angular.isFunction(val)){backupData[key] = val;};
                  // });
                  backupData = ngspublic.getVariable(backup);
                  var fromState = $rootScope.getCurState();
                  fromState.params.backupData = backupData;
              }

              $state.go(viewId, params);
          }
      };

      // 메뉴 클릭시 화면 전환 (탭이 있는 뷰)
      $rootScope.moveView = function(viewId){
          if(_.isEmpty($user.currentUser)){
              $message.alert('로그인 후 이용 가능합니다.');
              $rootScope.goView('public.html#/login');
              return;
          }
          $window.location.href = viewId;
      };

      $rootScope.prevView = function(param){
          var length = $rootScope.stateHistory.length;
          if(length > 1){
              $rootScope.stateHistory.splice(length-1, 1);
              var toState = $rootScope.stateHistory[$rootScope.stateHistory.length-1];

              if(!_.isEmpty(param)){
                  toState.params.param = param;
              }
              $state.go(toState.name, toState.params);
          }
      };
      $rootScope.getCurState = function(){
          var length = $rootScope.stateHistory.length;
          var fromState = $rootScope.stateHistory[length-1];
          return fromState;
      };
      $rootScope.firstView = function(){
          var length = $rootScope.stateHistory.length;
          if(length > 1){
              $rootScope.stateHistory.splice(1, length-1);
              var toState = $rootScope.stateHistory[$rootScope.stateHistory.length-1];
              $state.go(toState.name, toState.params);
          }
      };

      $rootScope.menuOpenBtn = function() {
          if(_.isEmpty($user.currentUser)){
              $rootScope.loginText = 'LOGIN'
          }else {
              $rootScope.loginText = 'LOGOUT'
          }
          $(".sidenav-content").addClass("active");
      };

      $rootScope.menuCloseBtn = function() {
          $(".sidenav-content").removeClass("active");
      };

      $rootScope.prevBtn = function(){
          $rootScope.prevView();
      };

      $rootScope.closeBtn = function(){
          $rootScope.firstView();
      };

      $rootScope.myPageBtn = function(){
          if(_.isEmpty($user.currentUser)){
              $message.alert('로그인 후 이용 가능합니다.');
              $rootScope.goView('public.html#/login');
              return;
          }
          var loginKind = $user.currentUser.getUserDataInfo().loginKind;
          if(loginKind === 'L1'){
              $rootScope.goView('pesn.html#/mypage/pmMyPage');
          }else if(loginKind === 'L2'){
              $rootScope.goView('comp.html#/mypagec/cmMyPage');
          }
      };

      $rootScope.logoBtn = function(){
          if(_.isEmpty($user.currentUser)){
              return;
          }
          var loginKind = $user.currentUser.getUserDataInfo().loginKind;
          if(loginKind === 'L1'){
              $rootScope.goView('pesn.html#/mainp/pmMain');
          }else if(loginKind === 'L2'){
              $rootScope.goView('comp.html#/mainc/cmMain');
          }
      };
  }])

  .factory('ActionTimer', ["$log", function($log) {
    var logger = $log('ActionTimer');
    var actions = {};
    return {
      start: start,
      lap: lap,
      stop: stop
    };
    function start(name, message) {
      actions[name] = {
        start: Date.now(),
        laps: []
      };
      logger.info([name, '(start:0ms) ', message].join(''));
    }
    function lap(name, message) {
      var action = actions[name];
      if (!action) {
        logger.error(Error(name + ' is not registered.'));
        return;
      }
      var start = action.start;
      var laps = action.laps;
      var now = Date.now();
      laps.push(now);
      logger.info([name, '(lap', laps.length, ':', now - start,'ms) ', message].join(''));
    }
    function stop(name, message) {
      var action = actions[name];
      if (!action) {
        logger.error(Error(name + ' is not registered.'));
        return;
      }
      var start = action.start;
      var laps = action.laps;
      var now = Date.now();
      logger.info([name, '(stop:', now - start, 'ms) ', message].join(''));
      delete actions[name];
    }
  }])
    .directive("datepicker", function () {
        return {
            restrict: "A",
            require: "?ngModel",
            link: function (scope, elem, attrs, ngModelCtrl) {
                var updateModel = function (dateText) {
                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(dateText);
                    });
                };
                var options = {
                    dateFormat: "yy-mm-dd",
                    showOtherMonths: true,
                    selectOtherMonths: true,
                    closeText: "닫기",
                    prevText: "이전달",
                    nextText: "다음달",
                    currentText: "오늘",
                    monthNames: [ "1월","2월","3월","4월","5월","6월",
                        "7월","8월","9월","10월","11월","12월" ],
                    monthNamesShort: [ "1월","2월","3월","4월","5월","6월",
                        "7월","8월","9월","10월","11월","12월" ],
                    dayNames: [ "일요일","월요일","화요일","수요일","목요일","금요일","토요일" ],
                    dayNamesShort: [ "일","월","화","수","목","금","토" ],
                    dayNamesMin: [ "일","월","화","수","목","금","토" ],
                    weekHeader: "주",
                    dateFormat: "yy-mm-dd",
                    firstDay: 0,
                    isRTL: false,
                    showMonthAfterYear: true,
                    yearSuffix: "년",
                    //showOn:"button",
                    //buttonImage:"/images/bg_cal.png",
                    //buttonImageOnly:true,
                    closeText:'삭제',
                    showButtonPanel: true,
                    onClose: function () {
                        if ($(window.event.srcElement).hasClass('ui-datepicker-close')) {
                            $(this).val('');
                            updateModel('');
                        }
                    },
                    onSelect: function (dateText) {
                        updateModel(dateText);
                    }
                };
                $(elem).datepicker(options);
            }
        }
    })
  // select 사용을 위한 디랙티브.
  .directive('metaOptions', ["$compile", "$parse", "MetaSvc", "$timeout", "$log", function($compile, $parse, MetaSvc, $timeout, $log) {
    return {
      priority: 1001, // compiles first
      terminal: true, // prevent lower priority directives to compile after it
      replace : false,
      restrict : 'A',
      scope: false, // := {}
      compile: function(el, attr) {
        var metaCodeName = _.uniqueId('_mds'); // scope unique id 보장받음.
        var metaCode = el.attr('meta-options'); // copy metaCode
        var metaBind = el.attr('meta-options-bind'); // bind 에 사용될 parent model (filter 로 사용 됨) 명.
        var metaBindExists = _.has(attr, 'metaOptionsBind');
        var metaType = el.attr('meta-options-type'); // type 에 따라, [key] value / value
        var data = el.attr('meta-options-data') || null;
        var ignore = el.attr('meta-options-ignore') || null; // ignore code

        var sortOrder = el.attr('meta-options-sortorder') || 'a'; // [Asc | Desc]
        var sortKey = el.attr('meta-options-sortkey') || 'k'; // [Key | Value]

        el.removeAttr('meta-options'); // necessary to avoid infinite compile loop
        if(metaType) {
          el.attr('ng-options', "kv.k as ( (kv.k) ? ('[' + kv.k + '] ' + kv.v) : kv.v ) for kv in " + metaCodeName);
        } else {
          el.attr('ng-options', "kv.k as ( kv.v ) for kv in " + metaCodeName);
        }

        return function(scope, iel, attr, ctl) {
          var modelGetter = $parse(attr['ngModel']);

          var iofunc = function(newCode, oldCode) {
            var dval = _.isEmpty(modelGetter(scope)) ? modelGetter(scope) : undefined;
            if(metaBindExists) {
              if(_.isEmpty(newCode)) {
                scope[metaCodeName] = [{ k : dval, v : '상위조건을 선택하세요.' }]; // 조회중 표시
                modelGetter.assign(scope, undefined);
                return;
              }
            } else {
              scope[metaCodeName] = [{ k : dval, v : '조회중입니다.' }]; // 조회중 표시
            }

            // 데이터셋 준비하고~ // Service Call!!! - promise 모델
            MetaSvc.getBizCode(metaCode) //
            .then(function(kvo) {
              var kv = _.clone(kvo);
              if(_.startsWith(sortKey, 'v')) { kv = _.sortBy(kv, 'v'); }
              if(_.startsWith(sortOrder, 'd')) { kv = kv.reverse(); }

              // bind 여부 여기서 고려 (필터로 동작)
              if(metaBindExists) {
                kv = _.filter(kv, { s : newCode || undefined });
              }

              if(!_.isEmpty(data) && _.isString(data)) {
                _.chunk(data.split('$'), 2).reverse().forEach(function(d) {
                  kv.unshift({ k : d[0], v : d.length == 2 ? d[1] : d[0] });
                });
              }

              if(!_.isEmpty(ignore) && _.isString(ignore)) {
                var a = ignore.split('$');
                kv = _.remove(kv, function(d) { return !_.contains(a, d.k); });
              }

              // 바인딩 값이 없고, kv 값에 empty 가 없다면~~ 첫 번째 값으로 바인딩
              var sel = modelGetter(scope);

              if(!_.isEmpty(kv) && (_.isEmpty(sel) || !_.some(kv, { k : sel  || undefined }))) {
                modelGetter.assign(scope, kv[0].k);
              }

              // 바인딩.
              scope[metaCodeName] = kv;

              iel.removeAttr('meta-options');
              iel.empty();

              // option 중복 bug fix
              var newel = angular.element(iel[0].outerHTML);
              iel.after(newel);
              iel.remove();

              $compile(newel)(scope);

            })
            .catch(function(msg) {

              var kv = [{ v : msg }];

              // 바인딩 값이 없고, kv 값에 empty 가 없다면~~ 첫 번째 값으로 바인딩
              var sel = modelGetter(scope);
              if(_.isEmpty(sel)) {
                modelGetter.assign(scope, kv[0].k);
              }

              // 바인딩.
              scope[metaCodeName] = kv;
            })
            ;
          } // end iofunc


          // 상위코드 바인딩 여부에 따라서 watch 여부 결정.
          if(metaBindExists && !_.isEmpty(metaBind)) {
            scope.$watch(metaBind, function(n, o) { iofunc(n, o); });
          } else {
            iofunc();
          }

          $compile(iel)(scope);
        };
      }
    }
  }])

  // Table row-span 을 위한 디랙티브 선언. (ng-repeat 걸리는 곳에서 row-span="[0,1]" 등으로 td 의 index 설정
  .directive('rowSpan',["$timeout", "$parse", function($timeout, $parse) {
    return {
      restrict : 'A',
      compile : function(el, attr) {
        var incCol = _.has(attr, 'rowSpanCol');
        return function(scope, element, attrs) {
          if (scope.$last) {
            var modelGetter = $parse(attrs['rowSpan']);
            $timeout(function() {
              $(modelGetter(scope)).each(function(a, n) {
                $(element.parent()).rowspan(n);
              });
            }, 0);
          }
        };
      }
    };
  }])
  // Table row-span 을 위한 디랙티브 선언. (ng-repeat 걸리는 곳에서 row-span="[0,1]" 등으로 td 의 index 설정
  .directive('colSpan', ["$timeout", "$parse", function($timeout, $parse) {
    return {
      restrict : 'A',
      compile : function(el, attr) {
        var incCol = _.has(attr, 'rowSpanCol');
        return function(scope, element, attrs) {
          if (scope.$last) {
            $timeout(function() {
              $('tr', element.parent()).each(function(n) {
                $(element.parent()).colspan(n);
              });
            }, 0);
          }
        };
      }
    };
  }])

  .directive('ngIsolate', function() {
    return { restrict : 'A', scope: true };
  })
  .directive('maxlength', ["$log", "$message", function($log, $message){
    return {
      restrict : 'A',
      link : function(scope, element, attrs) {
        var mlen = parseInt(attrs.maxlength || 0);
        if(_.isNaN(mlen) || mlen < 1) { return; }
        var nf = _.has(attrs, 'numberFormat');

        var $input = $(element);

        $input.bind('keydown', function(e) {
          $log.debug('keydown', e.keyCode, $input.val());
          var len = 0;
          if(!_.isEmpty(window.getSelection().toString())) { return; }
          if($input.val().length >= mlen && e.keyCode != 8 && e.keyCode != 9 && e.keyCode != 46 && e.keyCode != 13) {
            //if($input.val().length > mlen) { $input.val($input.val().substr(0, mlen)); $input.trigger('input'); }
            $input.blur();
            $message.alert('최대 입력 글자('+mlen+')를 초과했습니다.');
          }
        });
        $input.bind('change keyup', function(e) {
          $log.debug('keyup', e.keyCode, $input.val());
          var len = $input.val().length;
          if(len > mlen) {
            $input.val($input.val().slice(0, mlen)); $input.trigger('input');
          }
        });
      }
    };
  }])
  .directive('inputAutoSelect', function() {
    return {
      restrict : 'A',
      link : function(scope, element, attrs) {
        var $input = $(element);
        function se() {
          $input.select();
          $input.unbind('click', se);
          $input.blur(function() {
            $input.bind('click', se);
          });
        }
        $input.bind('click', se);
      }
    };
  })

  .config(["$compileProvider", "$httpProvider", function($compileProvider, $httpProvider) {
    $httpProvider.useApplyAsync(true);
    $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|javascript|file):/);
    // Angular before v1.2 uses $compileProvider.urlSanitizationWhitelist(...)
  }]);
 ;
/* //common/common.module.js */
}()
!function () { 'use strict';
/* core/core.module.js */

angular.module('easi.core', [])
  .run(checkGlobalAcl)
;

function checkGlobalAcl($rootScope, $location) {
  $rootScope.checkSession = function() {
    $window.location.href = 'public.html#/login';
    return false;
  }
  if($rootScope._session) {
    
  }
}
checkGlobalAcl.$inject = ["$rootScope", "$location"];

/* //core/core.module.js */
}()
!function () { 'use strict';
/* api/api.module.js */
/* jshint browser:true */

var api = angular.module('easi.api', ['ma.constant', 'com.constant', 'member.constant', 'guest.constant', 'company.constant']);

window.api = api;/* //api/api.module.js */
}()
!function () { 'use strict';
/* common/legacy/legacy.module.js */
angular.module('easi.common.legacy', []);


(function($){
  
  // http://willifirulais.blogspot.kr/2007/07/jquery-table-column-break.html
  $.fn.rowspan = function(colIdx, isStats) {       
      return this.each(function(){      
          var that;     
          $('tr', this).each(function(row) {      
              $('td:eq('+colIdx+')', this).filter(':visible').each(function(col) {
                   
                  if ($(this).html() == $(that).html()
                      && (!isStats 
                              || isStats && $(this).prev().html() == $(that).prev().html()
                              )
                      ) {            
                      var rowspan = $(that).attr("rowspan") || 1;
                      rowspan = Number(rowspan)+1;
   
                      $(that).attr("rowspan",rowspan);
                       
                      // do your action for the colspan cell here            
                      $(this).hide();
                       
                      //$(this).remove(); 
                      // do your action for the old cell here
                       
                  } else {            
                      that = this;         
                  }          
                   
                  // set the that if not already set
                  that = (that == null) ? this : that;      
              });     
          });    
      });  
  };    
  $.fn.colspan = function(rowIdx) {
      return this.each(function(){
          var that;
          $('tr', this).filter(":eq("+rowIdx+")").each(function(row) {
              $(this).find('td:not([nospan])').filter(':visible').each(function(col) {
                  if ($(this).html() == $(that).html()) {
                      var colspan = $(that).attr("colSpan") || 1;
                      colspan = Number(colspan)+1;
                       
                      $(that).attr("colSpan",colspan);
                      $(this).hide(); // .remove();
                  } else {
                      that = this;
                  }
                   
                  // set the that if not already set
                  that = (that == null) ? this : that;
                   
              });
          });
      });
  }  

    /*
    2018-04 유경수
    유효성 검증 관련 함수 
    */
    String.prototype.HAS_CHAR_IDENTIFIER = {
        SPECIAL             : 0,    // 특수문자
        NUMERIC             : 1,    // 숫자
        KOREAN              : 2,    // 한글
        ENGLISH_ALL         : 3,    // 영대소문자
        ENGLISH_SMALL_CASE  : 4,    // 영소문자
        ENGLISH_LARGE_CASE  : 5,    // 영대문자
        SPACE               : 6,    // 공백 
        NEW_LINE            : 7     // 개행문자
    }

    String.prototype.has = function () {
        if (!arguments || arguments.length === 0) { return false; }
        var _str = typeof (this) === "object" ? this.valueOf() : this;
        for (var i = 0 ; i < arguments.length; i++) {
            var _regex;
            switch (arguments[i]) {
                case String.prototype.HAS_CHAR_IDENTIFIER.SPECIAL:
                    _regex = /([!\"#$%&'()*+,\-.\/:;<=>?@\[\\\]^_`{|}~])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.NUMERIC:
                    _regex = /([0-9])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.KOREAN:
                    _regex = /([ㄱ-ㅎㅏ-ㅣ가-힣])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_ALL:
                    _regex = /([a-zA-Z])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_SMALL_CASE:
                    _regex = /([a-z])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_LARGE_CASE:
                    _regex = /([A-Z])/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.SPACE:
                    _regex = /\s/g;
                    break;
                case String.prototype.HAS_CHAR_IDENTIFIER.NEW_LINE:
                    _regex = /\r\n|\n|\r/g;
                    break;
            }

            if (_regex.test(_str)) { continue; }
            else { return false; }
        }
        return true;
    }
    String.prototype.hasEng = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_ALL); }
    String.prototype.hasEngLarge = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_LARGE_CASE); }
    String.prototype.hasEngSmall = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.ENGLISH_SMALL_CASE); }
    String.prototype.hasNum = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.NUMERIC); }
    String.prototype.hasKor = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.KOREAN); }
    String.prototype.hasSpe = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.SPECIAL); }
    String.prototype.hasSpace = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.SPACE); }
    String.prototype.hasNewLine = function () { return this.valueOf().has(String.prototype.HAS_CHAR_IDENTIFIER.NEW_LINE); }
    String.prototype.hasSpaceAndNewLine = function(){ return this.valueOf().has( String.prototype.HAS_CHAR_IDENTIFIER.SPACE , String.prototype.HAS_CHAR_IDENTIFIER.NEW_LINE); }
    String.prototype.trim = String.prototype.trim || function () {
        return this.valueOf().replace(/(^\s*)|(\s*$)/gi, "");
    }
    String.prototype.largerThan = function(len, containEqual){
        if(containEqual) return this.valueOf().length >= len;
        else return this.valueOf().length > len;
    }
    String.prototype.smallerThan = function(len , containEqual){
        if(containEqual) return this.valueOf().length <= len;
        else return this.valueOf().length < len;
    }
    String.prototype.getBytes = function(containEmptyString){
        var bytes = 0;
        var charArr = this.valueOf().split('');
        charArr.forEach(function(char){
            if(char.hasEng() || char.hasNum())
            {
                bytes++;
            }
            else if(char.hasKor() || char.hasSpe()){
                bytes += 2;
            }
            else if(containEmptyString && (char.hasSpace() || char.hasNewLine())){
                bytes++;
            }

        });

        return bytes;
    }
    String.prototype.isEmpty = function(){
        if(this.valueOf() && this.valueOf().largerThan(0)){return false;}
        else return true;
    }
})(jQuery);
/* //common/legacy/legacy.module.js */
}()
!function () { 'use strict';
/* common/shared/shared.module.js */
angular.module('easi.common.shared', [])
    .run(["$rootScope", "$modal", "$window", function($rootScope, $modal, $window){
        $rootScope.showPopup = function(param){
            //모달 오픈
            return $modal.open({
                templateUrl: 'popup/showPopup/showPopup.tpl.html',
                controller: 'showPopupCtrl',
                controllerAs: 'showPopup',
                backdrop: false,
                resolve: {
                    item: function() {
                        return param;
                    }
                }
            });
        }
        $window.showPopup = $rootScope.showPopup;

        $rootScope.openPop = function(params){
            return $modal.open({
                templateUrl : params.tplUrl,
                controller : params.ctrl,
                controllerAs : params.ctrlAs,
                backdrop : !params.backdrop ? false : params.backdrop,
                resolve : {
                    item : function(){
                        return [params.data];
                    }
                }
            });
        };

        $window.openPop = $rootScope.openPop;
    }]);/* //common/shared/shared.module.js */
}()
!function () { 'use strict';
/* common/env.config.js */
/* jshint browser:true */
function getConfig(target) {
    var config = {
        /**
         * 개발계
         */
        development: {
            fileDir: 'GMK_dev/',
            endPoint: {
                msdp: 'http://clickjobkorea.com',
                service: 'http://clickjobkorea.com',
            }
        },
        /**
         * 검증계
         */
        staging: {
            fileDir: 'GMK_stg/',
            endPoint: {
                msdp: 'http://clickjobkorea.com',
                service: 'http://clickjobkorea.com',
            }
        },
        /**
         * 운영계
         */
        production: {
            fileDir: 'GMK/',
            endPoint: {
                msdp: 'http://clickjobkorea.com',
                service: 'http://clickjobkorea.com',
            }
        }

    };
    return _.deepFreeze(config[target]);
}

angular
    .module('easi.common')
    .factory('$env', ["$q", "$window", "$timeout", "$rootScope", function($q, $window, $timeout, $rootScope) {
        function Env() {}
        Env.prototype.get = function() {
            var d = $q.defer();
            var self = this;
            $window.env.target = 'development';
            $rootScope.NODE_ENV = 'development';
            _.assign($window.env, getConfig($window.env.target));
            _.merge(self, $window.env);
            $rootScope.endPoint = self.endPoint;
            d.resolve(self);
            return d.promise;
        };
        return new Env();
    }]);
/* //common/env.config.js */
}()
!function () { 'use strict';
/* core/exception.js */

angular
  .module('easi.core')
  .config($exceptionHandlerConfig);

function $exceptionHandlerConfig($provide) {
  $provide.decorator('$exceptionHandler', $exceptionHandlerDecorator);
}
$exceptionHandlerConfig.$inject = ["$provide"];

function $exceptionHandlerDecorator($delegate, $log) {
  var $exceptionHandler = $delegate;
  return function(exception, cause) {
    $log.error('%cError: ' + exception.message, 'color: red');
    return $exceptionHandler(exception, cause);
  };
}
$exceptionHandlerDecorator.$inject = ["$delegate", "$log"];
/* //core/exception.js */
}()
!function () { 'use strict';
/* core/io.js */

angular.module('easi.core')
    .provider('$io', $IoProvider);

function $IoProvider() {
    this.$get = ioFactory;
}

function ioFactory($log, $http, $q, $rootScope, $timeout, $storage, $window, $message, $advHttp, $user, $env, $bdApp) {
    var logger = $log(this);
    $rootScope.requestCount = 0;

    $rootScope.safeApply = function(fn) {
        var phase = this.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if(fn && (typeof(fn) === 'function')) {
                fn();
            }
        } else {
            this.$apply(fn);
        }
    };

    //로그인 날짜 세팅
    $storage.get('loinDtm').then(function(data) {
        if(!_.isNull(data)){
            $rootScope.loinDtm = data+'';
        }else{
            $rootScope.loinDtm = '';
        }
    })

    return {
        api: api,
        user: user
    };

    function parseMsg(msg) {
        return (msg || '').replace(/(.*)(\([가-힣]+ID\:\s?[a-zA-Z0-9-_]+\)\s?)(.*)/, '$1');
    }

    function api(url, data, config) {

        var d = $q.defer();
        var request = {
            method: 'POST',
            url: url,
            data: data || {}
        };

        config = _.pick(config || {}, 'method', 'plain', 'invalidSrvcTrtRsltTypCd', 'header');

        if (config && /^get$/i.test(config.method)) {
            config.params = data;
        }

        request = _.assign(request, config);

        $rootScope.safeApply(function() {
            $rootScope.requestCount++;
        });

        if (request.data) {
            if (!config.plain) {
                request.data = {
                    sessionParam: {} ,
                    userParam: data || {},
                    commonHeader: _.assign({ cordova: $env.cordova }, config.header)
                };
                var userId = 'ANONYMOUS';
                if (request.data.sessionParam && request.data.sessionParam.userId) {
                    userId = request.data.sessionParam.userId;
                }
                try{
                    request.data.userParam.tid = [userId, _.guid()].join('.');
                }catch(e){}
            }
        }

        logger.debug('API Request', request);

        // 앱에서는 Y보내야함
        if(false){
            request.data.commonHeader.encryptFlag = "Y";
        }

        if (request.data && !config.plain) {
            request.data = _.mapValues(request.data, function(param) {
                return JSON.stringify(param);
            });
        }

        // 에러 발생시 저장 할 파라미터 새성
        try{
            var errLogParam = {};
            errLogParam.PAGE_NAME = $window.location.href || '';
            errLogParam.ERR_MSG_DESC = '통신에러';
            errLogParam.SEVICE_URL = url;
            errLogParam.SERVICE_PARAM = JSON.stringify(request);
            errLogParam.ERR_TYPE = '1';   //'에러타입(1:전문2:스크립트3:솔루션4:기타)'
        }catch(e){}
        // $http(request)
        var httpfn = $advHttp['adv' + _.titleize(request.method)];
        httpfn.call($advHttp, request)
            .then(function(response) {
                $rootScope.requestCount--;
                var data = response;

                // 결과 값 체크
                if(!_.isEmpty(data.user)){

                    // 로그인 체크 후 비로그인시 로그인 페이지로 이동
                    if(data.user.loginChk == 'noLogin'){
                        $message.alert('로그인 후 이용 가능합니다.');
                        $rootScope.goView('public.html#/login');
                        return d.reject('로그인 후 사용 가능합니다.');
                    }

                    if(data.user.result != 'SUCCESS'){
                        var msg = data.user.resultMessage || '알수없는에러';
                        $message.alert(msg);
                        return d.reject(data);
                    }

                }else{
                    try{
                        // 에러로그 남기는 서비스는 제외
                        $message.alert('통신 오류 입니다. 잠시후 다시 시도해주세요.');
                        if(url.indexOf('insertErrLog') <= -1){
                            errLogParam.ERR_MSG = JSON.stringify(data);
                            errLogParam.ERR_MSG_DESC = 'data.user 없음';
                            errLogParam.ERR_TYPE = '1';
                            $rootScope.insertErrLog(errLogParam);
                        }
                    }catch(e){}
                    return d.reject(data);
                }
                // 정상리턴
                //return d.resolve(convertingNumtoStr(data));
                return d.resolve(data);
            })
            .catch(function(response) {
                try {
                    response = JSON.parse(response);
                } catch(e) {
                    logger.warn(e);
                }
                $rootScope.requestCount--;
                $log.error(response);
                var msg = $rootScope.$eval('header.msg', response);
                $message.alert(msg || '네트워크 오류');
                // 에러 로그 남기기
                try{
                    // 에러로그 남기는 서비스는 제외
                    if(url.indexOf('insertErrLog') <= -1 && msg){
                        errLogParam.ERR_MSG = JSON.stringify(response);
                        errLogParam.ERR_MSG_DESC = msg || '통신에러';
                        $rootScope.insertErrLog(errLogParam);
                    }
                }catch(e){}
                return d.reject(response.data);
            });

        return d.promise;
    }

    function user(url, data, config) {
        return api(url, data, config)
            .then(function(result) {
                try {
                    if (_.isEmpty(result.data)) {
                        $message.alert('알수없는 에러(관리자에게 문의하세요.)');
                        return $q.reject('result.data');
                    }
                    return _.chain(result.data.user)
                        .clone()
                        .assign({
                            $response: result
                        })
                        .value();
                } catch(error) {
                    return $q.reject(error);
                }
            });
    }
}
ioFactory.$inject = ["$log", "$http", "$q", "$rootScope", "$timeout", "$storage", "$window", "$message", "$advHttp", "$user", "$env", "$bdApp"];

function ErrorString(value) {
    String.call(this, value);
    this.value = value;
}
function c() {}
c.prototype = String.prototype;
ErrorString.prototype = new c();
ErrorString.prototype.constructor = ErrorString;
ErrorString.prototype.toString = function() {
    return this.value;
}
ErrorString.prototype.valueOf = function() {
    return this.value;
}
function convertingNumtoStr(obj){
    var newObj = {};
    Object.keys(obj).map(function(key){
        if(!isNaN(obj[key])) {
            newObj[key] = obj[key].toString();
        } else if(Array.isArray(obj[key])) {
            newObj[key] = [];
            obj[key].map(function(value){
                newObj[key].push(convertingNumtoStr(value));
            })
        } else if(typeof obj[key] === 'object'){
            newObj[key] = convertingNumtoStr(obj[key]);
        }else{
            newObj[key] = obj[key];
        }
    });
    return newObj;
}/* //core/io.js */
}()
!function () { 'use strict';
/* core/log.js */
/* jshint proto:true */

angular
  .module('easi.core')
  .config($logConfig);

function $logConfig($provide, $logProvider) {
  $logProvider.debugEnabled(true);
  $provide.decorator('$log', $logDecorator);
}
$logConfig.$inject = ["$provide", "$logProvider"];

function $logDecorator($delegate, $window) {
  var $log = $delegate;
  return _.assign(loggerFactory, $log);

  function loggerFactory(context) {
    var name = _.isString(context) ? context : context.__proto__.constructor.name;
    var logger = {
      $log: $log
    };
    _.chain($log).methods().each(function(method) {
      logger[method] = function() {
        var args = _.toArray(arguments);
        var message = args[0];
        var time = moment().format('HH:mm:ss.SSS');
        var level = _.padRight(method.toUpperCase(), 5, ' ');
        var template = '<%=time%> [<%=level%>](<%=name%>)';
        var prefix = _.template(template)({
          time: time,
          level: level,
          name: name
        });
        if (_.isString(message)) {
          args[0] = [prefix, message].join(' ');
        } else {
          args.unshift(prefix);
        }
        $log[method].apply($log, args);
      };
    }).value();
    return logger;
  }
}
$logDecorator.$inject = ["$delegate", "$window"];
/* //core/log.js */
}()
!function () { 'use strict';
/* core/router.js */

angular
  .module('easi.core')
  .run(debugStateEvents)
;

function debugStateEvents($rootScope, $state, $log, $window) {

  var states = $state.get();
  var routeMap = _.chain(states)
    .indexBy('name')
    .mapValues(function(state) {
      return state.url;
    })
    .value();
  $log.info(states);
  $log.info('ROUTE MAP', routeMap);

  $rootScope.$on('$stateChangeStart',
    function(event, toState, toParams, fromState, fromParams) {
      // $log.debug('debugStateEvents::' + event.name, {
      //   event: event,
      //   toState: toState,
      //   toParams: toParams,
      //   fromState: fromState,
      //   fromParams: fromParams,
      // });
    });

  $rootScope.$on('$stateNotFound',
    function(event, unfoundState, fromState, fromParams) {
      $log.warn('debugStateEvents::' + event.name, {
        event: event,
        unfoundState: unfoundState,
        fromState: fromState,
        fromParams: fromParams,
      });
    });

  $rootScope.$on('$stateChangeSuccess',
    function(event, toState, toParams, fromState, fromParams) {
       $log.debug('debugStateEvents::' + event.name, {
         event: event,
         toState: toState,
         toParams: toParams,
         fromState: fromState,
         fromParams: fromParams,
       });
    });

  $rootScope.$on('$stateChangeError',
    function(event, toState, toParams, fromState, fromParams, error) {
      $log.error('debugStateEvents::' + event.name, {
        event: event,
        toState: toState,
        toParams: toParams,
        fromState: fromState,
        fromParams: fromParams,
        error: error,
      });
      $log.error(error);
    });

  $rootScope.$on('$viewContentLoading',
    function(event, viewConfig) {
      // $log.debug('debugStateEvents::' + event.name, {
      //   event: event,
      //   viewConfig: viewConfig,
      // });
        $( 'html, body' ).animate( { scrollTop : 0 }, 0 );
    });

  $rootScope.$on('$viewContentLoaded',
    function(event) {
      // $log.debug('debugStateEvents::' + event.name, {
      //   event: event,
      // });
    });
}
debugStateEvents.$inject = ["$rootScope", "$state", "$log", "$window"];
/* //core/router.js */
}()
!function () { 'use strict';
/* core/storage.js */

angular.module('easi.core')
  .provider('$storage', $StorageProvider);

function $StorageProvider() {
  this.$get = $storageFactory;
}

function $storageFactory($window, $q, $doryVariable) {
  return {
    set: set,
    get: get,
    remove: remove,
    getTempData: getTempData,
    setTempData: setTempData,
    clearTempData: clearTempData
  };

  function set(key, value, nonflash) {
    if (_.isUndefined(value)) {
      value = null;
    } else if (_.isObject(value) || _.isArray(value)) {
      value = JSON.stringify(value);
    }
    if (nonflash) {
      $window.localStorage.setItem(key, value);
    } else {
      $doryVariable.setData(key, value);
    }
  }

  function get(key, nonflash) {
    return nonflash &&
      $q.when(parseValue($window.localStorage.getItem(key))) ||
        $doryVariable.getData(key) .then(parseValue);
    function parseValue(value) {
      if (!value && value === 'null') {
          value = null;
        } else {
          try { value = JSON.parse(value); } catch (e) {}
        }
        return value;
    }
  }

  function remove(key) {
      $doryVariable.removeData(key);
  }

  function getTempData() {
    var key = '##tempData##';
    var d = $q.defer();
    var rv;
    get(key).then(function(v) {
      rv = _.clone(v);
      clearTempData();
      d.resolve(rv);
    })
    .catch(function(e) {
      return $q.reject('no data');
    });
    return d.promise;
  }
  function setTempData(v) {
    var d = $q.defer();
    var key = '##tempData##';
    set(key, v);
    d.resolve();
    return d.promise;
  }
  function clearTempData() {
    var key = '##tempData##';
    remove('##tempData##');
  }
}
$storageFactory.$inject = ["$window", "$q", "$doryVariable"];
/* //core/storage.js */
}()
!function () { 'use strict';
/* common/filters/filters.js */
angular
  .module('easi.common')
  .directive('fileread', ["$filter", function format($filter) {
    return {
        scope: {
            fileread: "="
        },
        link: function (scope, element, attributes) {
            element.bind("change", function (changeEvent) {
                scope.$apply(function () {
                    scope.fileread = changeEvent.target.files[0];
                    // or all selected files:
                    // scope.fileread = changeEvent.target.files;
                });
            });
        }
    };
  }])
    .directive('format', ["$filter", function format($filter) {
        return {
            require: '?ngModel',
            link: function(scope, elem, attrs, ctrl) {
                if (!ctrl) {
                    return;
                }

                ctrl.$formatters.unshift(function(a) {
                    return $filter(attrs.format)(ctrl.$modelValue);
                });

                ctrl.$parsers.unshift(function(viewValue) {
                    var plainNumber = viewValue.replace(/[^\d|\-+|\.+]/g, '');
                    elem.val($filter(attrs.format)(plainNumber));
                    return plainNumber;
                });
            }
        };
    }])
  .directive('numberFormat', ["$filter", "$log", function format($filter, $log) {
    return {
      restrict : 'A',
      require: '?ngModel',
      link: function(scope, elem, attrs, ctrl) {
        if (!ctrl) { return; }

        // 소숫점 몇 자리까지 받을지
        var fraction = parseInt(attrs.numberFormat || '5');
        if(_.isNaN(fraction) || fraction < 0) { fraction = 0; }
        else if(fraction > 5) { fraction = 5; }

        var fstr = (fraction > 0) ? '(\\.?\\d{0,'+fraction+'})?' : '';

        var dn = /^(-?)0+(\d+\.?)/; // 앞에 있는 무의미한 0 제거.

        var fn = new RegExp('^-?(?:\\d+' + fstr + ')?'); // - 도 일단은 받습니다. - 표현 (format number)
        var pn =new RegExp('^-?(?:\\d+' + fstr + ')'); // 숫자만 받습니다. - 값 (parser number) -- model 값

        function parser(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          try {
            var cm = (v || '').replace(/[, ]/g,'').replace(dn, "$1$2");
            var fv = fn.exec(cm); // format
            var vv = pn.exec(cm);

            if(!_.isEmpty(fv)) { fv = fv[0]; }
            if(!_.isEmpty(vv)) { vv = vv[0]; }

            var mv = parseFloat(vv);
            if(_.isNaN(mv)) { mv = ''; }
            else { mv = (mv).toString(); }

            $log.debug('parser', mv, v);

            ctrl.$setViewValue(mv);
            ctrl.$render();

            var sn = fv.split('.');

            var dv = parseInt(sn[0]);
            if(_.isNaN(dv)) { dv = fv; }
            else { dv = ((dv == 0 && _.startsWith(fv, '-')) && '-' || '') + _.numberFormat(dv) + ((sn.length == 2) && ('.' + sn[1]) || ''); }
            //else { dv = ((dv == 0 && _.startsWith(fv, '-')) && '-' || '') + new Intl.NumberFormat('ko-KR').format(dv) + ((sn.length == 2) && ('.' + sn[1]) || ''); }

            elem.val(dv);

            return mv;
          } catch(e) {
            debugger;
            $log.debug(e);
            return v;
          }
        }
        function formatter(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          try {

            $log.debug('formatter', v);

            var cm = (v).replace(/[, ]/g,'').replace(dn, "$1$2");
            var fv = fn.exec(cm); // format

            if(!_.isEmpty(fv)) { fv = fv[0]; }
            var sn = fv.split('.');

            var dv = parseInt(sn[0]);
            if(_.isNaN(dv)) { dv = fv; }
            else { dv = ((dv == 0 && _.startsWith(fv, '-')) && '-' || '') + _.numberFormat(dv) + ((sn.length == 2) && ('.' + sn[1]) || ''); }
            //else { dv = ((dv == 0 && _.startsWith(fv, '-')) && '-' || '') + new Intl.NumberFormat('ko-KR').format(dv) + ((sn.length == 2) && ('.' + sn[1]) || ''); }

            return dv;
          } catch(e) {
            debugger;
            $log.debug(e);
            return v;
          }
        }
        ctrl.$parsers.push(parser);
        ctrl.$formatters.push(formatter);
      }
    };
  }])
  .directive('ssnFormat', ["$filter", "$log", function format($filter, $log) {
    // 비슷하게는 동작 하는데.....
    // model change 쪽 이벤이 이상하게 걸리는 함정이 있네...
    return {
      restrict : 'A',
      require: '?ngModel',
      link: function(scope, elem, attrs, ctrl) {
        if (!ctrl) { return; }

        // 소숫점 몇 자리까지 받을지
        var fraction = parseInt(attrs.ssnFormat || '13');
        if(_.isNaN(fraction) || fraction < 7) { fraction = 7; }
        else if(fraction > 13) { fraction = 13; }

        var fn = new RegExp('^\\d{0,'+fraction+'}'); // 표현

        function parser(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          try {
            var cm = (v || '').replace(/-/g,'');
            var fv = fn.exec(cm); // format

            if(!_.isEmpty(fv)) { fv = fv[0]; }

            $log.debug('parser', fv, v);

            ctrl.$setViewValue(fv);
            ctrl.$render();

            elem.val(fv.length > 6 ? fv.substr(0, 6) + '-' + fv.substr(6) : fv);

            return fv;
          } catch(e) {
            debugger;
            $log.debug(e);
            return v;
          }
        }
        function formatter(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          try {

            var cm = (v).replace(/-/g,'');
            var fv = fn.exec(cm); // format
            if(!_.isEmpty(fv)) { fv = fv[0]; }

            $log.debug('formatter', v, fv);

            return fv.length > 6 ? fv.substr(0, 6) + '-' + fv.substr(6) : fv;
          } catch(e) {
            debugger;
            $log.debug(e);
            return v;
          }
        }
        ctrl.$parsers.push(parser);
        ctrl.$formatters.push(formatter);
      }
    };
  }])
  .directive('onlyNumberic', function() {
      return {
        require: 'ngModel',
        link: function(scope, elem, attrs, ngModelCtrl) {
          function userAction(text) {
            var permitCharater = attrs.onlyNumberic || '';
            var regexp = new RegExp('[^0-9' + permitCharater + ']', 'g');

            if (text) {
              var transformedInput = text.replace(regexp, '');
              if ( transformedInput !== text ) {
                ngModelCtrl.$setViewValue(transformedInput);
                ngModelCtrl.$render();
              }

              return transformedInput;
            }

            return undefined;
          }
          ngModelCtrl.$parsers.push(userAction);
        }
      }
  })
  .directive('inputFilter', ["$log", function($log) {
    return {
      restrict : 'A',
      require: '?ngModel',
      link: function(scope, elem, attrs, ctrl) {
        if (!ctrl) { return; }

        var type = attrs.inputFilter || '';
        if(_.isEmpty(type)) { return; }

        var regstr = '';
        if(type.indexOf('n') >= 0) { regstr += '\\d'; }
        if(type.indexOf('e') >= 0) { regstr += 'A-Za-z'; }

        // 2015-06-25 강형원 : 천지인 키패드에서 모음이 입력 안되는 문제가 있어 \\ᆢ\\ᆞ 조건 추가
        if(type.indexOf('h') >= 0) { regstr += '가-힣ㄱ-ㅎㅏ-ㅣ\\ᆢ\\ᆞ'; }
        if(type.indexOf('s') >= 0) { regstr += ' '; }
        if(type.indexOf('t') >= 0) { regstr += '`!@#$%\\^&\\*\\(\\)\\_\\+\\-\\=\\[\\]\\;\'\\,\\.\\/\\{\\}\\|\\:\\"\\<\\>\\?"\\~'; }

        if(_.isEmpty(regstr)) { return; }

        var reg = new RegExp('^[' + regstr + ']+');

        function parser(v) {
          if(_.isNumber(v)) {
            if(_.isNaN(v)) { return undefined; }
            v = (v).toString();
          }
          if(_.isEmpty(v)) { return ''; }

          var mv = reg.exec(v);
          mv = mv && mv[0] || '';

          $log.debug('parser', mv, v, regstr);

          if(v != mv) {
            ctrl.$setViewValue(mv);
            ctrl.$render();
            elem.val(mv);

            return mv;
          }

          return v;
        }
        ctrl.$parsers.push(parser);
      }
    };
  }])
  .provider('$filterSet', $filterSetProvider)
  .config($filterSetConfig)
;

function $filterSetConfig($filterProvider, $filterSetProvider) {
  // filterSetProvider 에서 정의된 Filter list 에 대해서, 필터로 등록. -- 아래 로직 노가다보다는..
  _.forIn($filterSetProvider.getFilterList(), function(v, k) {
    $filterProvider.register(k, function() { return v; });
  });

  //  $filterProvider.register('format', function($filterSet) { return $filterSet.format; });
  //  $filterProvider.register('objArrReduce', function($filterSet) { return $filterSet.objArrReduce; });
}
$filterSetConfig.$inject = ["$filterProvider", "$filterSetProvider"];

function $filterSetProvider() {
  // getInstance
  this.$get = this.getFilterList = function() { return _filterList; };
  var naw = { getToday : function() { return moment().format('YYYYMMDD'); } }

  // Filter 및 Util 함수명.
  var _filterList = {
    objArrReduce : objArrReduce, // [ { K1 : V1 }, { K2 : V2 }, ... ] -> [ K1 : V1, K2 : V2, ... ]
    format : format, // C# style string fommater
    nullChk : nullChk,
    shortDate : shortDate, // short Date Formatter
    shortDateTime : shortDateTime,
    shortTxtDate : shortTxtDate,
    toEmpty : toEmpty,
    toNumber : toNumber,
    toMoment : toMoment,
    ltrim : ltrim,
    rtrim : rtrim,
    trim : trim,
    removeBlank : removeBlank,
    isFutureDate : isFutureDate,
    isPastDate : isPastDate,
    maskN : maskN,
    fmtDate : fmtDate,
    fmtMonth : fmtMonth,
    fmtNumber : fmtNumber,
    fmtSn : fmtSn,
    fmtBn : fmtBn,
    fmtPhone : fmtPhone,
    fmtRRN: fmtRRN,
    splitPhone : splitPhone,
    mergePhone : mergePhone,
    isEmail : isEmail,
    getBirthFromSn : getBirthFromSn,
    getGenderFromSn : getGenderFromSn,
    isForeignerFromSn : isForeignerFromSn,
    isKorAndEng : isKorAndEng,
    hasText : hasText, // 이러한 형식으로 필터에 등록합니다.
    check_ResidentNO : check_ResidentNO,
    birthDateByJumin : getBirthDateByJumin,
    sexByJumin : getSexByJumin,
    sexByJuminCode : getSexByJuminCode,
    birthDataAge : getBirthDataAge,
    insuranceAgeByBirthDate : getInsuranceAgeByBirthDate,
    checkDate : checkDate,
    insuranceAgeByJuminno : getInsuranceAgeByJuminno,
    rrnoAgetFromBirth : getRrnoAgetFromBirth,
    rrnoAgetByJumin : getRrnoAgetByJumin,
    rrnoFromAge : getRrnoFromAge,
    rrnoFromBirth : getRrnoFromBirth,
    decimalPointCalc : function(a, b, c) { return decimalPointCalc(b, a, c); },
    cnvrInjrRskRatScCd : function(arr) { return cnvrInjrRskRatScCd(arr[0], arr[1]); },
  };

  // C# style string fomatter
  // Usage: {{ "From model: {0}; and a constant: {1};" | format:model.property:"constant":...:... }}
  // https://gist.github.com/litera/9634958
  function format(input) {
    var args = arguments;
    return input.replace(/\{(\d+)\}/g, function (match, capture) {
      return args[1*capture + 1];
    });
  };

    function nullChk(s) {
        if(_.isNumber(s)) return s;
        if(s == '' || s == undefined || s == null) return '';
        return s;
    }

  function shortDate(s, f) {
      if(s == '' || s == undefined) return '';
      // YKS 사파리 moment parse 오류 수정
      if(isNaN(s) && s.indexOf('.') >= 0){ s = s.replace(/\./g, '-'); }
      var m = moment(s);
      if(!m.isValid() && _.isString(s) && s.length == 8) { m = moment(s, 'YYYYMMDD'); }
      return m.isValid() && m.format(f || "YYYY-MM-DD") || '';
  }


    function shortDateTime(s) {
        if(s == '' || s == undefined) return '';
        // YKS 사파리 moment parse 오류 수정
        if(isNaN(s) && s.indexOf('.') >= 0){ s = s.replace(/\./g, '-'); }
        var m = moment(s);
        return m.isValid() && m.format("YYYY-MM-DD HH:mm:ss") || '';
    }

  function shortTxtDate(d, str) {
      if(str == undefined) str = '-';
      if(d == '' || d == undefined || d == null) return ''
      if(d.length != '6' && d.length != '8') return d;
      if(d.length == '6') return d.substr(0,4) + str + d.substr(4,2);
      if(d.length == '8') return d.substr(0,4) + str + d.substr(4,2) + str + d.substr(6,2);
      if(d.length == '8') return d.substr(0,4) + str + d.substr(4,2) + str + d.substr(6,2);
  }

  // [ { K1 : V1 }, { K2 : V2 }, ... ] -> [ K1 : V1, K2 : V2, ... ]
  // 나중에 input 이 array 인지 등을 확인할것..
  function objArrReduce (input) {
    return _.isArray(input) && input.reduce(function(a,b) { return _.assign(a, b); }) || input;
  };


  // boolean toEmpty(input) -- input 값이 null, undefined 인 경우 empty string '' 반환
  function toEmpty(s) { return (s === undefined || s === null) ? '' : s; }

  function toNumber(s) {
    var i = parseFloat((s || 0).toString().replace(/[^\d\.-]/g, ''));
    return i;
    //return new Intl.NumberFormat('ko-KR',{maximumFractionDigits: n || 0}).format(_.isNaN(i) && 0 || i);
  }

  function toMoment(s) {
    return moment(s);
  }

  // string ltrim(input) -- trim left
  function ltrim(s) {return _.trimLeft(s); }

  // string rtrim(input) -- trim right
  function rtrim(s) {return _.trimRight(s); }

  // string trim(input) -- trim
  function trim(s) {return _.trim(s); }

  // string removeBlank(input) -- remove all blanks in input string.
  function removeBlank(s) { return s.replace(/ */g, ''); }

  //boolean isFutureDate(date, [from = now]) -- date 가 현재시점의 미래인지 확인. (시분초 없이, 날짜만 확인)
  function isFutureDate(d, f) {
    // f - from 에 대해서 현재 _.now() 쓰고 있는것 나중에 시스템 타임으로 변경
    var m = moment(d);
    if(!m.isValid() && _.isString(d) && d.length == 8) { m = moment(d, 'YYYYMMDD'); }
    var b = moment(f || _.now());
    if(!b.isValid() && _.isString(f) && f.length == 8) { b = moment(f, 'YYYYMMDD'); }
    return m.isAfter(b);
  }

  //boolean isPastDate(date, [from = now]) -- date 가 현재시점의 과거인지 확인. (시분초 없이, 날짜만 확인)
  function isPastDate(d, f) {
    // f - from 에 대해서 현재 _.now() 쓰고 있는것 나중에 시스템 타임으로 변경
    var m = moment(d);
    if(!m.isValid() && _.isString(d) && d.length == 8) { m = moment(d, 'YYYYMMDD'); }
    var b = moment(f || _.now());
    if(!b.isValid() && _.isString(f) && f.length == 8) { b = moment(f, 'YYYYMMDD'); }
    return m.isBefore(b);
  }

  // string maskN(input, len) -- input 문자열 n 길이 이후에 '*' 처리. ex) maskN('abcde', 2) - 'ab***'
  function maskN(s, n, m) {
    var ss = (s || '').toString();
    var tn = ss.length;
    return _(n ? ss.substr(0, n || 1) : ss ? '*' : '').padRight(tn, m || '*');
  }

  // string fmtDate(input) -- input 문자열을 yyyy-MM-dd string 으로 변환.
  // 6 자리 YYMMDD 는 파싱 불가. (1900 년, 2000 년 기준이 나오면 추후 판단.
  function fmtDate(s) {
    var m = moment(s);
    if(!m.isValid() && _.isString(s) && s.length == 8) { m = moment(s, 'YYYYMMDD'); }
    return m.isValid() && m.format('YYYY-MM-DD') || '';
  }

  // string fmtMonth(input) -- input 문자열을 yyyy-MM-dd string 으로 변환.
  // 6 자리 는, YYYYMM 로 판단.
  function fmtMonth(s) {
    var m = moment(s);
    if(!m.isValid() && _.isString(s)) {
      if(s.length == 8) { m = moment(s, 'YYYYMMDD'); }
      else if(s.length == 6) { m = moment(s, 'YYYYMM'); }
    }
    return m.isValid() && m.format('YYYY-MM') || '';
  }


  // string fmtNumber(input) -- input 문자열을 #,000 포멧으로 변환
  function fmtNumber(s, n) {
    var i = parseFloat((s || 0).toString().replace(/[^\d\.-]/g, ''));
    return _.numberFormat(_.isNaN(i) && 0 || i, n || 0);
    //return new Intl.NumberFormat('ko-KR',{maximumFractionDigits: n || 0}).format(_.isNaN(i) && 0 || i);
  }

  // string fmtSn(input) -- input 값을 주민번호 포멧으로 반환. (validation 은 하지 않음 - 필요한 경우 협의)
  function fmtSn(s) {
    //var x = s && s.toString().replace(/[^\d]/g,'');
    var x = (s || '').replace(/[- ]/g, '');
    if (x) {
      return /*(x.length == 13 || x.length == 7) && */x.substr(0, 6) + '-' + x.substr(6, 7) || '';
    }
    return '';
  }

  // string fmtBn(input) -- input 값을 사업자번호 포멧으로 반환. (validation 은 하지 않음 - 필요한 경우 협의)
  function fmtBn(s) {
    //var x = s && s.toString().replace(/[^\d]/g,'');
    var x = (s || '').replace(/[- ]/g, '');
    return x.length == 10 && x.substr(0, 3) + '-' + x.substr(3, 2) + '-' + x.substr(5, 5) || '';
  }

  // string fmtPhone(input) -- input 값을 전화번호 포멧으로 반환.
  function fmtPhone(s) {
    //var x = s && s.toString().replace(/[^\d]/g,'');
    var x = (s || '').replace(/[- ]/g, '');
    switch(x.length) {
    // 02xxxyyyy
    case 9 : return x.substr(0, 2) + '-' + x.substr(2,3) + '-' + x.substr(5);
    // 02xxxxyyyy || 016xxxyyyy
    case 10 : return x[1] == '2' ? x.substr(0, 2) + '-' + x.substr(2,4) + '-' + x.substr(6) : x.substr(0, 3) + '-' + x.substr(4,3) + '-' + x.substr(6);
    // 0zzxxxxyyyy
    case 11: return x.substr(0, 3) + '-' + x.substr(3,4) + '-' + x.substr(7);
    default: return s;
    }
  }

  function fmtRRN(s) {
    return `${s.substr(0, 6)}-${s.substr(6, 7)}`
  }

  // model 바인딩 이슈가 있어서, 그쪽 검증이후에 모델과 함께 사용 가능합니다.
  // array(3)  splitPhone(input) -- input 값을 전화번호 포멧으로 반환.
  function splitPhone(s) {
    var x = fmtPhone(s).split('-');
    return (x.length == 3) ? x : ['','',''];
  }

  // string mergePhone(el1, el2, el3) -- input 값을 전화번호 포멧으로 반환.
  function mergePhone(s) {
    if(!_.isArray(s) || s.length != 3) { return ''; }
    return (s[0] || '') +'-' + (s[1] || '') +'-' + (s[2] || '');
  }

  // boolean isEmail(input) - input 값이 이메일 형식인지를 판한
  function isEmail(s) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(s);
  }

  // string getBirthFromSn(input, [month = false]) -- 주민번호 형식에서, 생년월일, 또는 월 일만 조회. (기본값은 년월일)
  function getBirthFromSn(s, m) {
    if(m) { return s.substr(2,4);}
    // 성별(년도) 코드는 위키 참조했음.
    // http://ko.wikipedia.org/wiki/%EC%A3%BC%EB%AF%BC%EB%93%B1%EB%A1%9D%EB%B2%88%ED%98%B8
    var x = s && s.toString().replace(/[^\d]/g,'');
    var n = parseInt(x.substr(6,1)); // 성별코드
    var y = parseInt(((n + 1) % 10) / 2);
    var d = (18 + (y && 2 - (y & 1))).toString() + x.substr(0, 6); // bit 연산 사용 지송.

    return d; // 리턴 타입에 date 필요하면 함수 확장할것.
  }

  // string getGenderFromSn(input) -- 주민번호에서 성별 추출 ('M' / 'F')
  function getGenderFromSn(s) {
    var x = s && s.toString().replace(/[^\d]/g,'');
    var n = parseInt(x.substr(6,1)); // 성별코드
    return (n % 2) && 'M' || 'F';
  }

  // boolean isForeignerFromSn(input) -- 주민번호에서 성별 추출 ('T' / 'F')
  function isForeignerFromSn(s) {
    var returnResult = false;
    var x = s && s.toString().replace(/[^\d]/g,'');
    var n = parseInt(x.substr(6,1)); // 성별코드

    if(n === 5 || n === 6 || n === 7 || n === 8){
      returnResult = true;
    }

    return returnResult; // 5,6,7,8,9 는 외국인.
  }

  //boolean isKorAndEng(input) - input 값이 한글과영문으로 이루어졌는지 확인
  function isKorAndEng(s) {
    var re = /[가-힣ㄱ-ㅎㅏ-ㅣ\s]+$|[a-zA-Z]+$/gi;
    return re.test(s);
  }

  /**
   *
   * 공백을 제외한 문자열을 가지고 있는지 확인합니다.
   *
   * @param str : 체크할 문자열
   * @return true : 문자열의 길이가 > 0 이면 , 그밖에 false
   */
  function hasText (str) {
    if (!str)
      return false;
    str = trim(str);
    if (str == "")
      return false;
    return true;
  };

    /**
     *  //주민등록번호 체크 로직
     *
     */
    function check_ResidentNO(str_f_num,str_l_num){

        var i3=0

        for (var i=0;i<str_f_num.length;i++){

            var ch1 = str_f_num.substring(i,i+1);

            if (ch1<'0' || ch1>'9') i3=i3+1;

        }

        if ((str_f_num == '') || ( i3 != 0 )) return false;

        var i4=0;

        for (var i=0;i<str_l_num.length;i++){

            var ch1 = str_l_num.substring(i,i+1);

            if (ch1<'0' || ch1>'9') i4=i4+1;

        }

        if ((str_l_num == '') || ( i4 != 0 )) return false;

        if(str_f_num.substring(0,1) < 4) return false;

        if(str_l_num.substring(0,1) > 2) return false;

        if((str_f_num.length > 7) || (str_l_num.length > 8)) return false;

        if ((str_f_num == '72') || ( str_l_num == '18'))  return false;



        var f1=str_f_num.substring(0,1)

        var f2=str_f_num.substring(1,2)

        var f3=str_f_num.substring(2,3)

        var f4=str_f_num.substring(3,4)

        var f5=str_f_num.substring(4,5)

        var f6=str_f_num.substring(5,6)

        var hap=f1*2+f2*3+f3*4+f4*5+f5*6+f6*7

        var l1=str_l_num.substring(0,1)

        var l2=str_l_num.substring(1,2)

        var l3=str_l_num.substring(2,3)

        var l4=str_l_num.substring(3,4)

        var l5=str_l_num.substring(4,5)

        var l6=str_l_num.substring(5,6)

        var l7=str_l_num.substring(6,7)

        hap=hap+l1*8+l2*9+l3*2+l4*3+l5*4+l6*5

        hap=hap%11

        hap=11-hap

        hap=hap%10

        if (hap != l7) return false;

        return true;

    }

  /**
   * 주민번호로 생년월일 구하기
   */
   function getBirthDateByJumin(jumin_no, delimiter) {
    var birthday = "";
    jumin_no = jumin_no.replace("-", "");
    var len = jumin_no.length;

    if ((typeof (delimiter) == "undefined"))
      delimiter = "";

    if (len == 13) {
      var iBirth = parseFloat(jumin_no.substring(0, 6));
      var iSex = parseInt(jumin_no.substring(6, 7), 10);

      if (!isNaN(iBirth) && !isNaN(iSex)) {
        var pre_year = "";
        if (iSex == 1 || iSex == 2 || iSex == 5 || iSex == 6) {
          pre_year = "19";
        } else if (iSex == 3 || iSex == 4 || iSex == 7 || iSex == 8) {
          pre_year = "20";
        } else {
          pre_year = "18";
        }

        birthday = pre_year + jumin_no.substring(0, 2) + delimiter + jumin_no.substring(2, 4) + delimiter + jumin_no.substring(4, 6);
      }

    }
    return birthday;
  };

  /**
   * 주민번호로 성별 구하기.(한글)
   *
   * @param 주민번호.
   *
   */
  function getSexByJumin(jumin_no) {
    if (!jumin_no)
      return "";

    var sex = "";
    var len = jumin_no.length;
    //jumin_no = jumin_no.replace(/-/g, "");
    jumin_no = jumin_no.replace("-", "");

    if (len >= 7) {
      var iSex = parseInt(jumin_no.substring(6, 7), 10);

      if (!isNaN(iSex)) {
        if (iSex == 1 || iSex == 3 || iSex == 5 || iSex == 7 || iSex == 9) {
          sex = "남자";
        } else if (iSex == 2 || iSex == 4 || iSex == 6 || iSex == 8 || iSex == 0) {
          sex = "여자";
        } else {
          sex = "";
        }
      }
    }
    return sex;
  };

  /**
   * 주민번호로 성별 구하기.(코드값)
   *
   * @param 주민번호.
   *
   */
  function getSexByJuminCode(jumin_no) {

    if (!jumin_no)
      return "";

    var sex = "";
    var len = jumin_no.length;
    //jumin_no = jumin_no.replace(/-/g, "");
    jumin_no = jumin_no.replace( "-", "");

    if (len >= 7) {
      var iSex = parseInt(jumin_no.substring(6, 7), 10);

      if (!isNaN(iSex)) {
        if (iSex == 1 || iSex == 3 || iSex == 5 || iSex == 7 || iSex == 9) {
          sex = "1";
        } else if (iSex == 2 || iSex == 4 || iSex == 6 || iSex == 8 || iSex == 0) {
          sex = "2";
        } else {
          sex = "";
        }
      }
    }
    return sex;
  };
  /**
   * 상령일(현재년도 + 생일(MMDD) + 6개월)기준나이
   */
  function getBirthDataAge(strBirthDate){

    var insurAge = 0;
    var standardDate = naw.getToday();
    insurAge = getInsuranceAgeByBirthDate(strBirthDate, standardDate);
    return insurAge;
  };

  //------------------------------------------------------------------------------
  // 생년월일 입력받아 보험나이를 반환한다.
  //
  // @param birthDate 생년월일 (형식:yyyyMMdd, 예:20080915)
  // @param standardDate 표준날자 (형식:yyyyMMdd, 예:20080915)
  // @return
  // 보험나이(상령나이)
  //------------------------------------------------------------------------------
  function getInsuranceAgeByBirthDate(birthDate, standardDate) {
    if (!hasText(birthDate) || !hasText(standardDate))
      return 0;

    var age = 0;
    var comp_year = 0; // 나이계산용
    var comp_mm = 0; // 월

    if (birthDate.length >= 8) {
      var currentYear = standardDate.substring(0, 4); // 기준년
      var currentMonth = standardDate.substring(4, 6); // 기준월
      var currentDay = standardDate.substring(6, 8); // 기준일

      var year = birthDate.substring(0, 4); // 주민번호-년
      var month = birthDate.substring(4, 6); // 주민번호-월
      var day = birthDate.substring(6); // 주민번호-일

      comp_year = currentYear; // 현재년을 일단 셋트합니다.
      comp_mm = currentMonth; // 현재년도 월

      // 현재월 - 생월
      // 0 이면 12개월 경과
      // > 0 이면 12 + 1월 경과
      // < 0 이면 12개월 미만

      //일자를 먼저 계산
      // 현재일 - 생일
      if ((currentDay - day) < 0) {
        comp_mm = currentMonth - 1; // 계산된 월
      }

      // 계산된 월 - 생월
      // 계산값이 0 보다 작으면
      if ((comp_mm - month) < 0) {
        comp_year = currentYear - 1;
        //2013.01.02 ycson : 보험나이 계산 오류 수정
//        comp_mm += 12; // 년을 빼고 그 뺀것을 월에다 더함
        comp_mm = parseInt(comp_mm, 10) + 12
      }

      // 나이 = 계산된 년 - 생년
      age = comp_year - year;
      // 계산된 월 - 생월 >= 6
      if ((comp_mm - month) >= 6) {
        age += 1;
      }

    }

    return age;
  };

  /**
   * 입력받은 날짜의 정합성을 체크
   * @param String yyyMMdd
   * @return boolean
   */
  function checkDate(strDate) {

      if(strDate == null || strDate.length != 8 || isNaN(strDate)) { return false; }

      var year  = parseInt(strDate.substr(0, 4));
      var month   = parseInt(strDate.substr(4, 2));
      var day   = parseInt(strDate.substr(6, 2));

      if(month < 1 || month > 12) {
        // 월이 정확히 입력되지 않았다
        return false;
      }

      var end = new Array(31,28,31,30,31,30,31,31,30,31,30,31); //각 월의 마직막일자

     //윤년처리
      if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
          end[1] = 29;
      }

      //true or false로 처리
      if(day < 1 || day > end[month-1]) { return false; }

      return true;
  };
  /**
   * 주민번호를 입력받아 보험나이를 반환한다.
   *
   * @param juminNo 주민번호
   * @param standardDate 표준날자 (형식:yyyyMMdd, 예:20080915)
   * @return 보험나이(상령나이)
   */
  function getInsuranceAgeByJuminno(juminNo, standardDate) {
    juminNo = juminNo.replace(/-/g, "");
    var birthDate = getBirthDateByJumin(juminNo);
    return getInsuranceAgeByBirthDate(birthDate, standardDate);
  };
  /**
   * 생년월일로 만나이를 계산
   */
  function getRrnoAgetFromBirth(birth, date) {
    var rrnoAge = 0;
    var today = naw.getToday();
    if(date) today = date;
    var toYear = eval(today.substring(0,4));
    var mm = today.substring(4,6);
    var dd = today.substring(6,8);
    var toMD = parseInt(today.substring(4,8), 10);

    var birthYear   = parseInt(birth.substr(0, 4), 10);
    var birthMD   = parseInt(birth.substr(4, 4), 10);

      if(toMD >= birthMD ){
        rrnoAge = toYear - birthYear;
       } else {
         rrnoAge = toYear - birthYear - 1;
       }
    return rrnoAge;
  };

  function getRrnoAgetByJumin(rn, date) {
    return getRrnoAgetFromBirth(getBirthDateByJumin(rn), date);
  };
  function getRrnoFromAge(age, gndrCd) {
    var today = naw.getToday();
    var yyyy = "" + (eval(today.substring(0,4)) - age);
    var mm = today.substring(4,6);
    var dd = today.substring(6,8);
    var strDate = yyyy + "" + mm + "" + dd;
    return getRrnoFromBirth(strDate, gndrCd);
  };
  function getRrnoFromAgeServerTime(age, gndrCd, date) {

    var today = date;
    var yyyy = "" + (eval(today.substring(0,4)) - age);
    var mm = today.substring(4,6);
    var dd = today.substring(6,8);

    var month   = Number(mm);
    var end = new Array(31,28,31,30,31,30,31,31,30,31,30,31); //각 월의 마직막일자
     //윤년처리
      if ((yyyy % 4 == 0 && yyyy % 100 != 0) || yyyy % 400 == 0) {
          end[1] = 29;
      }

    if(Number(dd) > end[month-1]){
      dd = end[month-1];
    }

    var strDate = yyyy + "" + mm + "" + dd;
    return getRrnoFromBirth(strDate, gndrCd);
  };
  function getRrnoFromBirth(birth, gndrCd) {
    var rrNoPost = 1;
    rrNoPost = gndrCd ? gndrCd : 1;
    rrNoPost = (Number(birth.substring(0, 4)) < 2000 ? rrNoPost : eval(rrNoPost) + 2);
    var rrNoPre = birth.substring(2, 8);
    return rrNoPre + "" + rrNoPost + "111111";
  };
  /**
   * param : strMode  - 수식
   * param : nCalcVal - 처리할 값(소수점 이하 데이터 포함)
   * param : nDigit   - 연산 기준 자릿수  => -2:십단위, -1:원단위  , 0:소수점 1자리, 1:소수점 2자리, 2:소수점 3자리, 3:소수점 4자리, 4:소수점 5자리 처리
   */
  function decimalPointCalc(strMode, nCalcVal, nDigit){

    if(!_.isNumber(nDigit)) { nDigit = 0; }
    if(strMode == "ROUND") {        //반올림
        if(nDigit < 0) {
            nDigit = -(nDigit);
            nCalcVal = (nCalcVal / Math.pow(10, nDigit)).toFixed(0) * Math.pow(10, nDigit);
        } else {
            nCalcVal = nCalcVal.toFixed(nDigit)
        }
    } else if(strMode == "CEIL") {  //절상
        if(nDigit < 0) {
            nDigit = -(nDigit);
            nCalcVal = Math.ceil(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
        } else {
            nCalcVal = Math.ceil(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
        }
    } else if(strMode == "FLOOR") { //절하
        if(nDigit < 0) {
            nDigit = -(nDigit);
            nCalcVal = Math.floor(nCalcVal / Math.pow(10, nDigit)) * Math.pow(10, nDigit);
        } else {
            nCalcVal = Math.floor(nCalcVal * Math.pow(10, nDigit)) / Math.pow(10, nDigit);
        }
    } else {                        //그대로(무조건 소수점 첫째 자리에서 반올림)
        nCalcVal = nCalcVal.toFixed(0);
    }

    return nCalcVal;
  };
  /**
   * 생년월일 입력받아 보험나이를 반환한다.
   *
   * @param birthDate 생년월일 (형식:yyyyMMdd, 예:20080915)
   * @param standardDate 표준날자 (형식:yyyyMMdd, 예:20080915)
   * @return 보험나이(상령나이)
   */
  function getInsuranceAgeByBirthDate(birthDate, standardDate) {
    if (!hasText(birthDate) || !hasText(standardDate))
      return 0;

    var age = 0;
    var comp_year = 0; // 나이계산용
    var comp_mm = 0; // 월

    if (birthDate.length >= 8) {
      var currentYear = standardDate.substring(0, 4); // 기준년
      var currentMonth = standardDate.substring(4, 6); // 기준월
      var currentDay = standardDate.substring(6, 8); // 기준일

      var year = birthDate.substring(0, 4); // 주민번호-년
      var month = birthDate.substring(4, 6); // 주민번호-월
      var day = birthDate.substring(6); // 주민번호-일

      comp_year = currentYear; // 현재년을 일단 셋트합니다.
      comp_mm = currentMonth; // 현재년도 월

      // 현재월 - 생월
      // 0 이면 12개월 경과
      // > 0 이면 12 + 1월 경과
      // < 0 이면 12개월 미만

      //일자를 먼저 계산
      // 현재일 - 생일
      if ((currentDay - day) < 0) {
        comp_mm = currentMonth - 1; // 계산된 월
      }

      // 계산된 월 - 생월
      // 계산값이 0 보다 작으면
      if ((comp_mm - month) < 0) {
        comp_year = currentYear - 1;
        comp_mm = parseInt(comp_mm, 10) + 12;
      }

      // 나이 = 계산된 년 - 생년
      age = comp_year - year;
      // 계산된 월 - 생월 >= 6
      if ((comp_mm - month) >= 6) {
        age += 1;
      }

    }

    return age;
  };
  /**
   * 일자 구하기
   * startDate : YYYYMMDD
   * endDate : YYYYMMDD
   */
  function getDiffDate(startDate, endDate) {
    var start = _strToDate(startDate);
    var end = _strToDate(endDate);
    return parseInt(Math.ceil(end-start) / (24*3600*1E3));
  }

  /**
   * 스트링 날짜를 DATE 객체로 변경
   *
   */
  function _strToDate(dateStr) {
    var d = new Date();
    d.setFullYear(dateStr.substr(0, 4), parseInt(dateStr.substr(4, 2), 10) - 1, dateStr.substr(6, 2));
    return d;
  }
}
/* //common/filters/filters.js */
}()
!function () { 'use strict';
/* common/components/message.js */

angular
  .module('easi.common')
  .factory('$message', MessageFactory)
  .directive('easiConfirm', function() {
    return {
      priority: -1,
      restrict: 'A',
      link: function (scope, element, attrs) {
        var msg = attrs.easiConfirm;

        element.bind('click', function(e){
          var message = attrs.ngConfirmClick;
          if(msg && !confirm(msg)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    };
  });

function MessageFactory($window) {
  return {
    alert: alert,
    confirm : confirm,
    nav : {
      alert : function(opt){
        // opt.message      메시지 
        // opt.callback     콜백함수 (함수)
        // opt.title        제목
        return $window.navigator.notification.alert(opt.message , opt.callback , opt.title || '');
      },
      confirm : function(opt){
        // opt.message      메시지 
        // opt.callback     콜백함수 (함수)
        // opt.title        제목
        return $window.navigator.notification.confirm(opt.message, opt.callback, opt.title || '');
      }
    }
  };
  
  function alert(message , callback , title) {
    return $window.alert(message);
  }
  function confirm(message) {
    return $window.confirm(message);
  }
}
MessageFactory.$inject = ["$window"];
/* //common/components/message.js */
}()
!function () { 'use strict';
/* common/components/modal.js */

angular
  .module('easi.common')
  .factory('$modal', ModalFactory);

var stack = [];

function ModalFactory($log, $document, $rootScope, $controller, $templateCache, $compile, $q, $injector) {
  return {
    open: open
  };

  function open(options) {
    var container = $document.find('#layerSpaceWrap');
    var template = options.template || $templateCache.get(options.templateUrl);
    var elem = angular.element(template);
    var scope = (options.scope || $rootScope).$new();

    var resultDeferred = $q.defer();
    var openedDeferred = $q.defer();

    var modalInstance = {
      result: resultDeferred.promise,
      opened: openedDeferred.promise,
      close: function(result) {
        var modal = _.remove(stack, {instance: this})[0];
        scope.$destroy();
        resultDeferred.resolve(result);
        modal.options.elem.remove();
        modal.options.backdrop.remove();
        if (!stack.length) {
          container.hide();
        }
      },
      dismiss: function(reason) {
        var modal = _.remove(stack, {instance: this})[0];
        scope.$destroy();
        resultDeferred.reject(reason);
        modal.options.elem.remove();
        modal.options.backdrop.remove();
        if (!stack.length) {
          container.hide();
        }
      }
    };

    if (!template) {
      var reason = 'Error: Template is not valid';
      openedDeferred.reject(reason);
      resultDeferred.reject(reason);
      return modalInstance;
    }

    scope.$close = modalInstance.close;

    var resolvePromises = $q.all(getResolvePromises(options.resolve));

    resolvePromises.then(function() {
      openedDeferred.resolve(true);
    }, function() {
      openedDeferred.reject(false);
    });

    resolvePromises.then(function(resolved) {
      if (options.controller) {
        var locals = {
          $scope: scope,
          $modalInstance: modalInstance
        };
        var index = 0;
        _.each(options.resolve, function(value, key) {
          locals[key] = resolved[index++];
        });
        try {
          var controller = $controller(options.controller, locals);
        } catch(e) {
          $log.error('$modal', e);
          openedDeferred.$$reject(e);
          resultDeferred.reject(e);
          return modalInstance;
        }
        if (options.controllerAs) {
          scope[options.controllerAs] = controller;
        }
      }
      
      var backdrop = angular.element('<div class="layerSpaceSec"></div>');
      if($rootScope.lockBackgroundColorChange){
        backdrop = angular.element('<div class="layerSpaceSec lockBackgroundColorChange"></div>')
      }
      if (options.backdrop !== false) {
        container.append(backdrop);
      }
      container.append(elem);
      container.show();
      stack.push({instance: modalInstance, options: {
        elem: elem,
        backdrop: backdrop
      }});
      elem.show();
      $compile(elem)(scope);
    }, function(reason) {
      resultDeferred.reject(reason);
    });

    return modalInstance;
  }

  function getResolvePromises(resolves) {
    var promisesArr = [];
    angular.forEach(resolves, function (value) {
      if (angular.isFunction(value) || angular.isArray(value)) {
        promisesArr.push($q.when($injector.invoke(value)));
      }
    });
    return promisesArr;
  }
}
ModalFactory.$inject = ["$log", "$document", "$rootScope", "$controller", "$templateCache", "$compile", "$q", "$injector"];
/* //common/components/modal.js */
}()
!function () { 'use strict';
/* common/components/picker.js */
// angular
//   .module('easi.common')
//   .config(inputConfig)
//   ;

function inputConfig($provide, $injector) {
  $provide.decorator('inputDirective', function($delegate, $compile, $injector, $window, $doryPicker) {
    var directive = $delegate[0];
    var preLink = directive.link.pre;

    directive.link.pre = function(scope, element, attrs, ngModel) {
      if (/date|time|datetime/i.test(attrs.type) && !_.isEmpty(cordova.plugins.DateTimePicker)) {
        // element.attr('type', 'text');
        element[0].readOnly = true;
        var behavior = {
          date: {
            fn: $doryPicker.datePicker
          },
          time: {
            fn: $doryPicker.timePicker
          },
          datetime: {
            fn: $doryPicker.datetimePicker
          }
        };
        element.on('click', function(e) {
          var type = behavior[attrs.type.toLowerCase()];
          type.fn(moment(element.val() || undefined).toDate())
            .then(function(date) {
              element.val(moment(date).format(type.format)).trigger('change');
            });
          e.preventDefault();
        });
        var clientH = document.documentElement.clientHeight;
        // 달력 클릭시 이벤트
        if ($('body').hasClass('ap')){ 
          element.next('button').click(function(){
            element.click();
          });
        } 
      }
      preLink(scope, element, attrs, ngModel);
    };
    return $delegate;
  });
}
/* //common/components/picker.js */
}()
!function () { 'use strict';
/* common/legacy/ngspublic.js */
angular
  .module('easi.common.legacy')
  .factory('ngspublic', ngspublic);


function ngspublic($q, $log, $timeout, $message, $user, $cordovaFileTransfer, $modal, $io,
                   $env, MetaSvc) {

  var logger = $log(this);
  
  var fileExtensions = ["doc","docx","pdf","xls","xlsx","ppt","pptx","txt","wav","wma","csv","png","jpg","gif","jpeg","tif"];

  return{
    // setNextKey : setNextKey,
    // setNextNumKey : setNextNumKey,
    comCodeValue : comCodeValue,
    // insActHstrLog : insActHstrLog,
    // fileDown : fileDown,
    // getFileMngSvc : getFileMngSvc,
    getByteLength : getByteLength,
    substrByte : substrByte,
    // getLrckday : getLrckday,
    openRdViewer : openRdViewer,
    getAdvInfo : getAdvInfo,
    chkFileExtensions : chkFileExtensions,
    // chkVersion : chkVersion,
    //processStop : processStop,
      getVariable : getVariable,
      nullCheck : nullCheck,
      dateCheck : dateCheck
  }
  
  // /**
  //  * 다음 프로세스 막음
  //  */
  // function processStop(stDayTime, endDayTime, msg, exitFlag, loginPass, loginId){
  //
  //   var d = $q.defer();
  //   var passIdList = ['68094252'];
  //   var selId = '';
  //
  //   if(!loginPass){
  //     selId = $user.currentUser.getUserId() || '';
  //   }else{
  //     selId = loginId;
  //   }
  //
  //
  //   var returnParam = {};
  //   var loginStopYn = '';
  //
  //   // 현재 시간 조회
  //   MetaSvc.getServerTime().then(function(t) {
  //     returnParam.t = t;
  //     return t;
  //   })
  //   .then(function(t){
  //
  //     returnParam.ingFlag = true;
  //
  //     // 파라미터 존재시 프로퍼티조회는 패스 전달 받은 파라미터로 처리
  //     if(!_.isUndefined(stDayTime) && !_.isUndefined(endDayTime) && !_.isUndefined(msg)){
  //       returnParam.stDayTime =  stDayTime;
  //       returnParam.endDayTime = endDayTime;
  //       returnParam.msg = msg;
  //       returnParam.exitFlag = exitFlag;
  //       return returnParam;
  //     }
  //     // 청약제어 프로퍼티 조회
  //     var propertyParams = {'propertyKeys' : ['plStopDtm','plStopMsg','plStopAppExitYn','loginStopYn','stopExcetEno']};
  //     return cmDBService.selectMsdpPropertys(propertyParams).then(function(propertyData){
  //       _.forEach(propertyData, function(obj){
  //         switch (obj.user.propertyKey) {
  //         case 'plStopDtm':
  //           if(obj.user.propertyValue.length != 29 || obj.user.propertyValue.indexOf('~') == -1){
  //             returnParam.ingFlag = false;
  //           }else{
  //             returnParam.stDayTime =  obj.user.propertyValue.split('~')[0];
  //             returnParam.endDayTime = obj.user.propertyValue.split('~')[1];
  //           }
  //           break;
  //         case 'plStopMsg':
  //           returnParam.msg = obj.user.propertyValue;
  //           break;
  //         case 'plStopAppExitYn':
  //           returnParam.exitFlag = obj.user.propertyValue;
  //           break;
  //         case 'loginStopYn':
  //           loginStopYn = obj.user.propertyValue;
  //           break;
  //         case 'stopExcetEno':
  //           if(!_.isUndefined(obj.user.propertyValue)){
  //             passIdList = obj.user.propertyValue.split(',');
  //           }
  //           break;
  //         default:
  //           break;
  //         }
  //       });
  //     })
  //
  //   })
  //   .then(function(result){
  //     // 프로퍼티 값이 없으면 패스
  //     if(!returnParam.ingFlag) {
  //       d.resolve(false);
  //       return d.promise;
  //     }
  //
  //     // 예외사번은 SKIP
  //     if (passIdList.indexOf(selId) > -1 ) {
  //       d.resolve(false);
  //       return d.promise;
  //     }
  //
  //     var toDay = moment(returnParam.t).format('YYYYMMDDHHmm');
  //     if( toDay >= returnParam.stDayTime && toDay < returnParam.endDayTime){
  //
  //       if(loginStopYn == "true"){
  //         // 로그인제어가 true일경우
  //         $message.alert(returnParam.msg);
  //         $bdApp.exit();
  //       }else{
  //         // 로그인제어가 아닐경우
  //         if(_.isUndefined(loginPass)){
  //           $message.alert(returnParam.msg);
  //           if(returnParam.exitFlag == "true" || returnParam.exitFlag == true){
  //             $bdApp.exit();
  //           }
  //         }
  //       }
  //
  //       d.resolve(true);
  //     }else{
  //       d.resolve(false);
  //     }
  //
  //   })
  //   .catch(function(){
  //     d.resolve(false);
  //   });
  //
  //   return d.promise;
  // }
  

  /**
   * 파일 확장자 검증
   */
  function chkFileExtensions(fileName){
    if( _.isEmpty(fileName) || fileName.lastIndexOf(".") < -1 ) return false;
    
    var fileEx = fileName.substr(fileName.lastIndexOf(".")+1);
    
    var ckCnt = _.size(_.chain(fileExtensions).filter(function(v){ return v.toLowerCase() == fileEx; }).value());
    
    if(ckCnt > 0) return true;
    else return false;
    
  }

  // /**
  //  * 다음데이터 조회를 위한 조회키 세팅(이전다음_페이지번호)
  //  */
  // function setNextNumKey(pageRowCnt,nextPageNum){
  //   return (((pageRowCnt) * (nextPageNum-1)) + 1)+"";
  // }
  //
  //  /**
  //  * 다음데이터 조회를 위한 조회키 세팅(헤더 조회)
  //  */
  // function setNextKey(header){
  //   return header.stndKeyList[0].stndKeyVal;
  // }


  /**
   * 공통코드에서 매칭된 값 구하기
   */
  function comCodeValue(COM_CODE, KEY){
    try{
      for(var i = 0; i < COM_CODE.length; i++){
        if(COM_CODE[i].k.trim() == KEY.trim()){
          return COM_CODE[i].v;
        }
      }
      return ''
    }catch(e){return ''}
  }

  // /**
  //  * 파일 다운로드
  //  */
  // function fileDown(param, hitoryParam, totCnt, curCnt) {
  //   var d = $q.defer();
  //   logger.debug("file Param : " + JSON.stringify(param));
  //   logger.debug("file hitoryParam : " + JSON.stringify(hitoryParam));
  //
  //   // 히스토리 저장
  //   if(!_.isEmpty(hitoryParam)){
  //     insActHstrLog(hitoryParam);
  //   }
  //   //네이티브 연동
  //   $cordovaFileTransfer.download(param)
  //     .then(function(data) {
  //       logger.debug(data);
  //       if(!totCnt){
  //         $bdFile.execute(data.nativeURL);
  //       }else{
  //         if(totCnt == curCnt){
  //           if($message.confirm("다운로드가 완료되었습니다.\n다운로드 목록을 확인하시겠습니까?")){
  //
  //             $bdFilechoose.open(param.downSubFPath)
  //             .then(function (path) {
  //               $bdFile.execute(path);
  //             });
  //
  //           }
  //         }
  //       }
  //
  //       // 다운로드 완료로 아이콘 변경
  //       $timeout(function() {
  //         param.downYn = true;
  //         param.nativePath = data.nativeURL;
  //         return d.resolve(param);
  //       }, 0);
  //
  //     })
  //     .catch(function(data) {
  //       $message.alert("파일 다운로드 에러!");
  //       // 다운로드 완료로 아이콘 변경
  //       param.downYn = false;
  //       logger.error(data);
  //     });
  //
  //   return d.promise;
  // }


  // /**
  //  * 파일 매니져
  //  */
  // function getFileMngSvc(a_sFnCd, a_sFileKey, a_oFileInfo, a_subPath, na_savePath){
  //   var fsno, fname, fsavename,fsize, fpath;
  //   var d = $q.defer();
  //
  //   if(a_sFnCd == spConstant.FILE_GUBUN_S || a_sFnCd == spConstant.FILE_GUBUN_D1){
  //     if(_.isEmpty(a_sFileKey)){
  //       $message.alert("[매개변수오류]\n조회(삭제) 대상 첨부파일키 매개변수가 누락되었습니다.");
  //       return d.reject("");
  //     }
  //   }else if(a_sFnCd=="I" && !a_oFileInfo){
  //     $message.alert("[매개변수오류]\n저장 할 첨부파일정보 매개변수가 누락되었습니다.");
  //     return d.reject("");
  //   }
  //
  //   // 파라미터 생성
  //   var param = {};
  //       param.apndFileDtlList = [];
  //       param.apndFileId = a_sFileKey;
  //   if(a_sFnCd == spConstant.FILE_GUBUN_I){
  //     param.apndFileId = '';
  //   }
  //   switch(a_sFnCd){
  //     case spConstant.FILE_GUBUN_S:
  //       param.apndFileId = a_sFileKey;        // 첨부파일아이디
  //       break;
  //     case spConstant.FILE_GUBUN_D1:
  //       param.apndFileId = a_sFileKey;        // 첨부파일아이디
  //       break;
  //     case spConstant.FILE_GUBUN_D2:
  //       //==============================================================
  //       // 첨부파일정보 리스트 데이터 세팅
  //       //==============================================================
  //       var oTmp = null;
  //       fsno = fname = fsavename = fsize = fpath = "";
  //
  //       for(var i=0; i<a_oFileInfo.length; i++){
  //         oTmp = a_oFileInfo[i];
  //         fsno = oTmp.fsno;                     // 첨부파일일련번호
  //         param.apndFileDtlList.push({"apndFileSno": fsno});
  //       }
  //       break;
  //     case spConstant.FILE_GUBUN_U:
  //     case spConstant.FILE_GUBUN_I:
  //       if(a_sFnCd == spConstant.FILE_GUBUN_I){
  //         param.apndFileId = "";              // 첨부파일아이디
  //       }else{
  //         param.apndFileId = a_sFileKey;      // 첨부파일아이디
  //       }
  //
  //       //==============================================================
  //       // 첨부파일정보 리스트 데이터 세팅
  //       //==============================================================
  //       var oTmp = null;
  //       fsno = fname = fsavename = fsize = fpath = "";
  //
  //       for(var i=0; i<a_oFileInfo.length; i++){
  //
  //         oTmp = a_oFileInfo[i];
  //         fsno = (typeof(oTmp.fsno)!="undefined")? oTmp.fsno : "";  // 첨부파일일련번호
  //         fname = oTmp.fname;                                       // 첨부파일명
  //         if(oTmp.fext != "") fname += "." + oTmp.fext;
  //         fsavename = oTmp.fsavename;                               // 첨부파일저장명
  //         fsize = oTmp.fsize;                                       // 첨부파일크기
  //         fpath = oTmp.fpath;                                       // 첨부파일경로
  //
  //         param.apndFileDtlList.push({
  //           "apndFileSno": fsno,
  //           "apndFileNm":fname,
  //           "apndFileStoreNm":fsavename,
  //           "apndFileSize":fsize,
  //           "apndFilePathNm":fpath
  //         });
  //
  //       }
  //       break;
  //   }
  //
  //   var sBizCode = '';
  //   if(!_.isNull(a_subPath))  sBizCode = a_subPath;
  //   param.trtrEno = $user.currentUser.getTrtrEno();  // 처리자사번
  //   param.untyBusnScCd = sBizCode;                         // 통합업무구분
  //
  //   // 서비스 호출
  //   var retVal = null;
  //   switch(a_sFnCd){
  //     case spConstant.FILE_GUBUN_S:
  //       CIApndFileMngUtilSvc.selListApndFileInfo(param)
  //         .then(function(data){
  //           retVal = data.serviceBody.data.BackEndBody[0];
  //
  //           if(retVal){
  //             var userData = retVal.user;
  //             var sFileKey = userData.apndFileId;     // 리턴매개변수[0] - 파일키
  //             var nListData = _.isEmpty(userData.apndFileDtlList)?[]:userData.apndFileDtlList;
  //             var nListCnt = nListData.length;
  //             var oTmpArray = new Array();
  //
  //             for(var i=0; i<nListCnt; i++){
  //
  //               var fsno = nListData[i].apndFileSno;
  //               var fpath = nListData[i].apndFilePathNm;
  //               var fname = nListData[i].apndFileNm;
  //               var fsavename = nListData[i].apndFileStoreNm;
  //               var fsize = nListData[i].apndFileSize;
  //
  //               oTmpArray[i] = new Object();
  //               oTmpArray[i].fsno = fsno;               // 첨부파일일련번호
  //               oTmpArray[i].fpath = fpath;             // 첨부파일경로
  //               oTmpArray[i].fname = fname;             // 첨부파일명
  //               oTmpArray[i].fsavename = fsavename;     // 첨부파일저장명
  //               oTmpArray[i].fsize = fsize;             // 첨부파일크기
  //               oTmpArray[i].fkey = userData.apndFileId;// 첨부파일키
  //               oTmpArray[i].allDownload = false;       // 전체 다운로드 여부
  //
  //             }
  //
  //             var fileResult = {};
  //             fileResult.a_sFnCd = a_sFnCd;
  //             fileResult.sFileKey = sFileKey;
  //             fileResult.oFileInfoList = oTmpArray;
  //
  //             // 파일 존재 여부 확인
  //             logger.debug("na_savePath : " + na_savePath)
  //             if(!_.isEmpty(na_savePath)){
  //               try{
  //                 // 파일 다운로드 여부
  //                 if(fileResult.oFileInfoList.length > 0){
  //                   var allFlag = true;
  //                   $bdExtension.getFileList(na_savePath)
  //                   .then(function(files) {
  //                     angular.forEach(fileResult.oFileInfoList, function(obj,idx){
  //                       var exists = _.filter(files, {name: obj.fname});
  //                       if (exists.length) {
  //                         if(allFlag){
  //                           allFlag = true;
  //                         }
  //                         obj.downYn = true;
  //                         obj.nativePath = exists[0].path;
  //                       }else{
  //                         allFlag =  false;
  //                         obj.downYn = false;
  //                       }
  //                     });
  //                     // 전체 다운로드 여부
  //                     if(allFlag){
  //                       angular.forEach(fileResult.oFileInfoList, function(obj,idx){
  //                         obj.allDownload = true;
  //                       });
  //                     }
  //                     return d.resolve(fileResult);
  //                   })
  //                   .catch(function(){
  //                     return d.resolve(fileResult);
  //                   })
  //                 }else{
  //                   return d.resolve(fileResult);
  //                 }
  //               }catch(e){return d.resolve(fileResult);}
  //             }else{
  //               return d.resolve(fileResult);
  //             }
  //           }
  //
  //         })
  //         .catch(function(data){
  //           logger.error(data);
  //         })
  //       break;
  //     case spConstant.FILE_GUBUN_D1:
  //       CIApndFileMngUtilSvc.delApndFileInfo(param)
  //         .then(function(data){
  //           retVal = data.serviceBody.data.BackEndBody[0];
  //           return d.resolve(fileMngResult(a_sFnCd, retVal));
  //         })
  //         .catch(function(data){
  //           logger.error(data);
  //         })
  //       break;
  //     case spConstant.FILE_GUBUN_D2:
  //       CIApndFileMngUtilSvc.delApndFileInfoCse(param)
  //       .then(function(data){
  //         retVal = data.serviceBody.data.BackEndBody[0];
  //         return d.resolve(fileMngResult(a_sFnCd, retVal));
  //       })
  //       .catch(function(data){
  //         logger.error(data);
  //       })
  //       break;
  //     case spConstant.FILE_GUBUN_U:
  //       CIApndFileMngUtilSvc.updApndFileInfo(param)
  //         .then(function(data){
  //           retVal = data.serviceBody.data.BackEndBody[0];
  //           return d.resolve(fileMngResult(a_sFnCd, retVal));
  //         })
  //         .catch(function(data){
  //           logger.error(data);
  //         })
  //       break;
  //     case spConstant.FILE_GUBUN_I:
  //       CIApndFileMngUtilSvc.insApndFileInfo(param)
  //         .then(function(data){
  //           retVal = data.serviceBody.data.BackEndBody[0];
  //           return d.resolve(fileMngResult(a_sFnCd, retVal));
  //         })
  //         .catch(function(data){
  //           logger.error(data);
  //         })
  //       break;
  //   }
  //
  //   return d.promise;
  //
  // }

  // /**
  //  * 파일 삭제
  //  */
  // function delServerFile(oDeleteFileArray){
  //
  //
  //   $cordovaFileTransfer.remove(oDeleteFileArray)
  //   .then(function(result){
  //       if(result!="FAIL"){
  //         try{
  //           var oArrReponseMsg = result.split("|");
  //           var sReturnCd = oArrReponseMsg[0];                                          // "0000":Success, 그 외:Failed
  //           var sReturnMsg = oArrReponseMsg[1];                                         // 정상/에러 메시지
  //           var oDeleteFailInfo = (sReturnCd!="0000")? eval(oArrReponseMsg[2].substr(0,oArrReponseMsg[2].length-1)) : null;
  //
  //           if(sReturnMsg == "DELETE OK!!!") sReturnMsg = "정상적으로 삭제처리 되었습니다.";      //메세지 변경요청 2010.1.12 jsh
  //           $message.alert(sReturnMsg);                                                 // 메시지출력
  //           return(oDeleteFailInfo);                                                    // 삭제완료된 파일정보를 리턴한다
  //         }catch(e){$message.alert(e.description);return(null);}
  //       }else{
  //         $message.alert("파일 삭제 시 오류가 발생하였습니다. 확인 후 재전송 하십시오.");
  //       }
  //     })
  //     .catch(function(data){
  //       $message.alert("파일 삭제 시 오류가 발생하였습니다. 확인 후 재전송 하십시오.");
  //     })
  //
  // }

  /**
   * 바이트 계산
   */
  function getByteLength(str){
    var cnt = 0;
    for(var i=0;i<str.length;i++){

      if(str.charCodeAt(i) > 127){
        cnt += 3;
      }else{
        cnt++;
      }

    }

    return cnt
  }

  /**
   * 바이트 만큼 자르기
   */
  function substrByte(str,start,len){
    var ch,sPos=0,nowLen=0,chLen,buf="";
    for(var i=0;i<str.length;i++){
      ch= str.charAt(i);
      chLen= getByteLength(ch);
      sPos+=chLen;
      if(!_.isNull(len) && nowLen+chLen>len)break;
      if(sPos<=start||isKorAndEng(ch)&&sPos-1==start)continue;
      buf+=ch;
      nowLen+=chLen
    }
    return buf
  }

  function isKorAndEng(s) {
    var re = /[가-힣]/gi;
    return re.test(s);
  }

  // /*
  //  * 음력년월일과 음력여부를 받아 해당 일자의 정보를 넘겨주는 함수
  //  * a_ilnrcYmYmd  : (String) 음력년월일자
  //  * a_ipmntYn  : (String) 윤달여부
  //  *
  // */
  // function getLrckday(a_ilnrcYmYmd, a_ipmntYn) {
  //   var userParam = {
  //     lnrcYmYmd : a_ilnrcYmYmd,
  //     lpmntYn : a_ipmntYn
  //   };
  //
  //   return $io.api('/cm/CISalesWkdaySrvcUtilSvc/selLrckday', userParam)
  //     .then(function(response) {
  //       var backEndBody = response.serviceBody.data.BackEndBody[0];
  //       if (backEndBody.naf) {
  //         return $q.reject(backEndBody.naf);
  //       }
  //       return backEndBody.user.cISalesWkdayVO;
  //     });
  // }

  /**
   * Open Report Designer Viewer
   *
   * rdOptions
   *   mrdPath : mrd 파일 위치
   *   mrdData : RD 데이터 (XML)
   *
   * return : 뷰어에 넘겨진 최종 RD 데이터
   */
  function openRdViewer(rdOptions){
    var reportingServerHost = $env.endPoint.report;
    var serverUrl = reportingServerHost + '/ReportingServer2/service';    

    /*
      네트워크에서 /service 호출 시 오류 분석.
        - java.lang.Exception과 함께 "문서를 실행할 수 없습니다."가 뜨면 mrdPath에서 mrd 파일을 가져오지 못한 것임.
          이와 관련하여 RD에서 loopback IP는 처리못함. (mrdPath에 localhost 또는 127.0.0.1 사용 불가)

        - NumberFormatException 응답의 한가지 경우는 UTF-8 without BOM 관련처리하면 됨. 레거시에서 xml 읽어올 때 주의됨. (AS-IS는 파일이 ASCII 혹은 UTF-8 with BOM으로 저장되어 있음)
     */
    var mrdPath;
    if (rdOptions.mrdPath && rdOptions.mrdPath.match(/^http.*/)) {
      mrdPath = rdOptions.mrdPath;
    } else {
      mrdPath = reportingServerHost + '/' + rdOptions.mrdPath;
    }

    var mrdData = rdOptions.mrdData.replace('&lt;![CDATA[', '', 'g').replace(']]&gt;', '', 'g'); // regex was so slow in javascript
    mrdData = "<?xml version='1.0' encoding='utf-8'?><root>" + mrdData + "</root>"; // wrapping xml required by RD

    /*
    mrdParam 관련 레거시 소스 내 설명
    rimgindexing : 보고서내의 여러페이지에 동일한 이미지 사용시 메모리 사용.
    rusedevcopies : 인쇄매수가 2매 이상일 경우, 프린터의 스풀에 1매에 대한 스풀만 생성되도록 함.
   */
    // var mrdParam = ' /rpdfusesvg /rexportchartratio [100] /riprnmargin /rusedevcopies '; // azlk-qu-ext/../ngspublic.js 에서 쓰던 mrdParam
    // var mrdParam = '/rusecxlib /rpdfusesvg /rexportchartratio [100] /riprnmargin /rusedevcopies '; // ngs-comm-ui/../ngspublic.js 에서 쓰던 mrdParam
    var mrdParam = ' /rmmloverlapobj /rignorecert /rxmlreportopt [2] /rusecxlib /rpdfusesvg /rexportchartratio [100] /riprnmargin /rusedevcopies /rv keeper[1] /rmmlfooteropt [1] ';        
  
    // RD overlay 방식
    //var viewer = new m2soft.crownix.Viewer(serverUrl);
    var viewer = new m2soft.crownix.Viewer(serverUrl, { viewerWidth:1.0, viewerHeight : 1.0, enableCloseButton:false });
    m2soft.crownix.Layout.setTheme('white'); // 색상 변경
    
    
    if($env.cordova){
      // 디바이스에서
      viewer.hideToolbarItem(['print_pdf']);
      viewer.hideToolbarItem(['inquery']);  
      viewer.hideToolbarItem(['cancel']);     // 취소
      viewer.hideToolbarItem(['save']);       // 저장
      viewer.showToolbarItem(['close']);      // 닫기
    }else{
      // 크롬에서
      viewer.hideToolbarItem(['inquery']);  
      viewer.hideToolbarItem(['cancel']);     // 취소
      viewer.showToolbarItem(['save']);   // 저장
      viewer.showToolbarItem(['close']);      // 닫기
    }
    
    /*
    if($env.target == 'production') {   
      // 운영(저장버튼 닫기)
      viewer.hideToolbarItem(['save']);  
    }
    else {
      // 개발/검증..기타..(PDF만 저장)
      viewer.showToolbarItem(['save']);   // 저장
      viewer.hideToolbarItem(['doc']);    // 워드
      viewer.hideToolbarItem(['hwp']);    // 한글
      viewer.hideToolbarItem(['xls']);    // 엑셀
      viewer.hideToolbarItem(['ppt']);    // 파워포인트
    }
    */
    
//    viewer.openFile('http://125.129.124.44/report/kmns5/allianz/mrd/0529/sfqu102rIdb568.mrd', '/rfn [http://125.129.124.44/report/kmns5/allianz/mrd/0529/sfqu102rIdb467XML.xml] /rxmlreportopt [2] /rmmlfooteropt [1] /rmmloverlapobj /rignorecert /rusecxlib /rpdfusesvg /rexportchartratio [100] /riprnmargin /rusedevcopies',{enableNote : true, includeNoteInExport : false, includeNoteInPrint : true, imageDrawOption:{useCanvas:false}});
//    viewer.openFile(mrdPath, '/rdata [' + mrdData + ']' + mrdParam, {imageDrawOption:{useCanvas:false}});
    viewer.openFile(mrdPath, '/rdata [' + mrdData + ']' + mrdParam, {imageDrawOption:{useCanvas:false}, useScrollTransition:false});
    
    // 서브리포트명 전달 콜백
    if( rdOptions.subReportCallback != undefined ){
      viewer.bind('report-finished', rdOptions.subReportCallback );
    }

    return mrdData;
  }

  /*
   * 설계사 전문 자격증 조회
   */
  function getAdvInfo() {
    var userParam = {
      'sMQulfdMngSrchVO' : {
        'pesnNo' : $user.currentUser.getUserEno()
      }
    };

    return $io.api('/cm/SMQulfdMngSvc/selListQulfdMng', userParam)
      .then(function(response) {
        var backEndBody = response.serviceBody.data.BackEndBody[0];
        if (backEndBody.naf) {
          return $q.reject(backEndBody.naf);
        }
        return backEndBody.user.sMQulfdMngRespVO;
      })
      .catch(function(response) {
        return $q.reject(response.data);
      });
  }

  // function chkVersion(){
  //   var d = $q.defer();
  //   var msg = '사용중인 알로탭은 최신버전이 아닙니다. 이 경우 심각한 문제가 발생될 수 있습니다.\n알로탭을 종료한 후 재실행하시기 바랍니다. 재실행하시면 자동으로 최신버전으로 업데이트됩니다.\n종료하시려면 확인을, 일단 업무를 정리하고 잠시 후 종료하시려면 취소를 누르세요.';
  //   $bdresource.isUpdateAPKFlag()
  //   .then(function(result){
  //     if(result){
  //       if($message.confirm(msg)){
  //         $bdApp.exit();
  //       }
  //     }
  //     return $bdresource.isUpdateResouceFlag();
  //   })
  //   .then(function(result){
  //     if(result){
  //       if($message.confirm(msg)){
  //         $bdApp.exit();
  //       }
  //       $bdresource.checkUpdate().then(function(dd){alert(dd)})
  //     }
  //   })
  //
  //   return d.promise;
  //
  // }

  function getVariable (obj){
      var data = {};
      _.each(obj, function(val, key){
          if(!angular.isFunction(val)){

              //data[key] = val;

              if(!isNaN(val)) {
                  data[key] = val;
              } else if(Array.isArray(val)) {
                  data[key] = [];
                  val.map(function(value){
                      data[key].push(getVariable(value));
                  })
              } else if(typeof val === 'object'){
                  //data[key] = getVariable(val);
                  var getData = getVariable(val);
                  if(!angular.isFunction(val)){
                      data[key] = getData;
                  }
              }else{
                  data[key] = val;
              }
          }
      });
      var isNotFunction = false;
      _.each(data, function(val, key){
          if(!angular.isFunction(val)){
              isNotFunction = true;
          }
      });
      if(!isNotFunction){
          data = function empty(){}
      }
      return data;
  }

    /**
     * null체크
     * @param val
     * @param msg
     * @param defaultVal
     * @returns {boolean}
     */
  function nullCheck(val, msg, defaultVal){
      if(_.isNumber(val)) return true;

      if(_.isEmpty(val) || _.isNull(val) || val === '' || val === defaultVal){
          $message.alert(msg);
          return false;
      }
      return true;
  }

    /**
     * 날짜 체크
     * @param startDateVal
     * @param endDateVal
     * @param msg
     * @returns {boolean}
     */
  function dateCheck(startDateVal, endDateVal, msg){
      if(_.isEmpty(startDateVal)){
          $message.alert(msg+'시작일자를 정확하게 입력해주세요.');
          return false;
      }
      if(_.isEmpty(endDateVal)){
          $message.alert(msg+'종료일자를 정확하게 입력해주세요.');
          return false;
      }
      if(moment(startDateVal) > moment(endDateVal)){
          $message.alert('시작일자보나 종료일자가 작습니다.');
          return false;
      }
      return true;
  }
}
ngspublic.$inject = ["$q", "$log", "$timeout", "$message", "$user", "$cordovaFileTransfer", "$modal", "$io", "$env", "MetaSvc"];/* //common/legacy/ngspublic.js */
}()
!function () { 'use strict';
/* core/native/plugins.js */
angular
  .module('easi.common')
  .config($cordovaFileTransferConfig)
  .factory('$bdApp', $bdAppFactory)
  .factory('$advHttp', $advHttpFactory)
  .factory('$doryPicker', $doryPickerFactory)
  .factory('$doryVariable', $doryVariableFactory)
  .factory('$UUID', $UUIDFactory)
  .factory('$bdFilechoose', $bdFilechooseFactory)
  .factory('$bdStack', $bdStackFactory)
  .factory('$bdExtension', $bdExtensionFactory)
  .factory('$bdPhone', $bdPhoneFactory)
  .factory('$bdresource', $bdresourceFactory)

function $cordovaFileTransferConfig($provide) {
  $provide.decorator('$cordovaFileTransfer', ["$delegate", "$window", "$q", "$log", function($delegate, $window, $q, $log) {
    var decorator = {};
    var retryMsg = {
      download: '다운로드 실패하였습니다. 재시도 하겠습니까?',
      upload: '업로드 실패하였습니다. 재시도 하겠습니까?'
    };
    _.each(['download', 'upload'], function(fn) {
      decorator[fn] = function() {
        var args = arguments;
        return $delegate[fn].apply($delegate, args)
          .then(null, error, _.debounce(notify, 500));

        function error(rejection) {
          $log.warn(rejection);
          if ($window.confirm(retryMsg[fn])) {
            return decorator[fn].apply(null, args);
          } else {
            return $q.reject(rejection);
          }
        }

        function notify(progress) {
          if (progress.lengthComputable) {
            $log.debug(['[FileTransfer Progress] loaded:', progress.loaded, 'total:', progress.total, 'percentage:', (progress.loaded / progress.total) * 100 + '%'].join(' '), progress);
          } else {
            $log.debug('[FileTransfer Progress]', progress);
          }
        }
      };
    });
    return decorator;
  }]);
}
$cordovaFileTransferConfig.$inject = ["$provide"];

function $bdAppFactory($window) {
  return {
    exit: exit
  };
  function exit() {
    if ($window.Extension) {
      $window.Extension.exit(/*type*/null, success, fail);
    } else {
      logger.warn('Extension Not Found');
    }
    function success(data) {
      logger.debug('success ', data);
    }
    function fail(data) {
    }
  }
}
$bdAppFactory.$inject = ["$window"];

function $advHttpFactory($window, $http, $q, $log, $env) {
  /* global http */
  var logger = $log('$advHttp');
  return {
    advPost: advPost,
    advGet: advGet,
      advDownload : advDownload,
      advUpload : advUpload
  };
  
  function request(options) {
    var host = $env.endPoint.service;
    var startTime = Date.now();
    var timerName = '(Stopwatch) {' + _.guid().substr(0, 8) + '} ' + options.url;
    logger.log('request:', timerName);
    console.time(timerName);
    if (window.env.platform === 'browser') {
      var request = _.merge({
          headers: {
            'Content-Type': undefined
          },
          url: '',
          data: {}
        }, options);
        if (/\.json$/.test(request.url)) {
            request.method = 'GET';
        } else {
            request.url = host + request.url;
        }
      return $http(request)
        .then(function(response) {
          logger.log('response:', timerName, Date.now() - startTime);
          console.timeEnd(timerName);
          return response.data;
        })
        .catch(function(rejection) {
          logger.error('responseError:', timerName, Date.now() - startTime);
            console.timeEnd(timerName);
          return $q.reject(rejection);
        });
    }

    var d = $q.defer();
    var defaults = {
      url: '',
      data: {},
      options: {
        headers: {
            'Accept' : 'application/json, text/plain, */*', 'Content-Type' : 'application/json'
        }
      },
      method: ''
    };
    options = _.merge(defaults, _.pick(options, _.keys(defaults)));
    options.headers = options.options.headers;

    options.url = $env.endPoint.service + options.url;

    if(options.method === 'POST'){
      logger.debug('call advHttp.post', options);
      // cordovaHTTP.post(options.url, options.data, options.headers)
      //   .then(success)
      //   .catch(error);
      cordova.plugin.http.setDataSerializer('json');
      cordova.plugin.http.post(options.url, options.data, options.headers, success, error);
    }else if(options.method === 'GET'){
      logger.debug('call advHttp.get', options);
      // cordovaHTTP.get(options.url, options.data, options.headers)
      //   .then(success)
      //   .catch(error);
        cordova.plugin.http.setDataSerializer('json');
        cordova.plugin.http.get(options.url, options.data, options.headers, success, error);
    }else if(options.method === 'DOWNLOAD'){
        logger.debug('call advHttp.downloadFile', options);
        // cordovaHTTP.downloadFile(options.url, options.data, options.headers, options.filePath)
        //     .then(success)
        //     .catch(error);
        cordova.plugin.http.get(options.url, options.data, options.headers, success, error);
    }else if(options.method === 'UPLOAD'){
        logger.debug('call advHttp.uploadFile', options);
        // cordovaHTTP.uploadFile(options.url, options.data, options.headers, options.filePath, options.name)
        //     .then(success)
        //     .catch(error);
        cordova.plugin.http.get(options.url, options.data, options.headers, success, error);
    }else{

    }

    function success(obj) {
      logger.debug(obj);
      try {
        obj.data = JSON.parse(obj.data);
      } catch(e) {
        logger.warn(e);
      }
      logger.debug('advHttp success', obj);
      d.resolve(obj.data);
    }

    function error(obj) {
      try {
          obj.data = JSON.parse(obj.data);
      } catch(e) {
        logger.warn(e);
      }
      logger.error('advHttp error ', obj);
      d.reject(obj.data);
    }

    return d.promise
      .then(function(data) {
        logger.debug('response:', timerName, Date.now() - startTime);
        console.timeEnd(timerName);
        return data;
      })
      .catch(function(rejection) {
        logger.error('responseError:', timerName, Date.now() - startTime);
        console.timeEnd(timerName);
        return $q.reject(rejection);
      });
  }



  function advPost(options) {
    // 병렬
    options.method = 'POST'
    return request(options);
    
    // 순차
//    var d = $q.defer();
//    var id = _.uniqueId('e2e#');
//    e2eQueue.push(id);
//    e2eMap[id] = {
//      defer: d,
//      options: options
//    };
//    
//    (function next() {
//      if (!e2eQueue.length || busy) return;
//      var id = e2eQueue.shift();
//      var config = e2eMap[id];
//      busy = true;
//      request(config.options)
//        .then(config.defer.resolve, config.defer.reject)
//        .finally(function() {
//          delete e2eMap[id];
//          busy = false;
//          next();
//        });
//    })();
//    
//    return d.promise;
  }

  function advGet(options) {
    // 병렬
    options.method = 'GET'
    return request(options);
  }

    function advDownload(options) {
        // 병렬
        options.method = 'DOWNLOAD'
        return request(options);
    }

    function advUpload(options) {
        // 병렬
        options.method = 'UPLOAD'
        return request(options);
    }
}
$advHttpFactory.$inject = ["$window", "$http", "$q", "$log", "$env"];

function $doryPickerFactory($log, $window, $q) {
  var logger = $log('$doryPicker');
  return {
    datePicker: datePicker,
    timePicker: timePicker,
    datetimePicker: datetimePicker
  };
  
  function datePicker(date){
    return showDateTimePicker(date, 'date');
  }
  
  function timePicker(date){
    return showDateTimePicker(date, 'time');
  }
  
  function datetimePicker(date){
    return showDateTimePicker(date, 'datetime');
  }
  
  function showDateTimePicker(date, mode) {
    var d = $q.defer();
    if (cordova.plugins.DateTimePicker) {
      var options = {
          mode: mode,
          date: date,
          allowOldDates: true,
          allowFutureDates: true,
          locale: 'KO',
          okText: '선택',
          cancelText: '취소',
          android: {
            theme: 3, // If omitted/undefined, default theme will be used.
            calendar: false,
            is24HourView: false
          }
      };

      function onSuccess(date) {
        logger.debug('Selected date: ' + date);
        d.resolve(date);
      }

      function onError(error) { // Android only
        logger.error('Error: ' + error);
        d.reject(date);
      }
      cordova.plugins.DateTimePicker.show(options, onSuccess, onError);
    } else {
      logger.warn('Picker Not Found');
      d.reject('Picker Not Found');
    }
    return d.promise;
  }
}
$doryPickerFactory.$inject = ["$log", "$window", "$q"];


function $doryVariableFactory($log, $q, $window) {
  var logger = $log('$doryVariable');
    var secureStorage = null;
    if(cordova.plugins){
        _init();
    }
  return {
    setData: setData,
    getData: getData,
    removeData: removeData,
      _init : _init
  };

  function _init(){
      secureStorage = new cordova.plugins.SecureStorage(
          function(){
              logger.warn('SecureStorage On');
          },
          function(error){
              logger.warn('SecureStorage Error : ' + error);
              navigator.notification.alert(
                  'Please enable the screen lock on your device. This app cannot operate securely without it.',
                  function () {
                      secureStorage.secureDevice(
                          function () {
                              _init();
                          },
                          function () {
                              _init();
                          }
                      );
                  },
                  'Screen lock is disabled'
              );
          },
          'clickjobkorea');
  }

  function setData(key, value) {
    logger.debug('setData', key, value);
    if (secureStorage) {
        secureStorage.set(
          function (key) {
              logger.warn('set : ' + key);
          },
          function (error) {
              logger.warn('set error : ' + error);
          },
          key, value);
    } else {
      $window.localStorage.setItem(key, value);
    }
  }

  function getData(key) {
    logger.debug('getData', key);
    var d = $q.defer();
    if (secureStorage) {
        secureStorage.get(
          function (value) {
              logger.warn('get : ' + value);
              d.resolve(value);
          },
          function (error) {
              logger.warn('get error : ' + error);
              d.resolve(null);
          },
          key);
    } else {
      d.resolve($window.localStorage.getItem(key));
    }
    return d.promise;
  }

  function removeData(key) {
    if (secureStorage) {
        secureStorage.remove(
          function (key) {
              logger.warn('remove : ' + key);
          },
          function (error) {
              logger.warn('remove error : ' + error);
          },
          key);
    } else {
      $window.localStorage.setItem(key, '');
      $window.localStorage.removeItem(key);
    }
  }
}
$doryVariableFactory.$inject = ["$log", "$q", "$window"];

function $bdStackFactory($log, $window, $q) {
  var logger = $log('$bdStack');
  return {
    main: main,
    move: move,
    jump: jump,
    pre: pre,
    getData: getData,
    getParam: getParam
  };

  function main(pageUrl) {
    logger.debug('main', arguments);
    if ($window.Stack) {
      $window.Stack.main(pageUrl);
    } else {
      logger.warn('Stack Not Found');
    }
  }

  function move(pageUrl, params, data) {
    logger.debug('move', arguments);
    if ($window.Stack) {
      $window.Stack.move(pageUrl, params, data);
    } else {
      logger.warn('Stack Not Found');
      $window.location.href = pageUrl;
    }
  }

  function jump(pageUrl) {
    logger.debug('jump', arguments);
    if ($window.Stack) {
      $window.Stack.jump(pageUrl);
    } else {
      logger.warn('Stack Not Found');
      $window.location.href = pageUrl;
    }
  }

  function pre() {
    logger.debug('pre');
    if ($window.Stack) {
      $window.Stack.pre();
    } else {
      logger.warn('Stack Not Found');
      $window.history.back();
    }
  }

  function getData() {
    var d = $q.defer();
    if ($window.Stack) {
      $window.Stack.getData(success, fail);
    } else {
      logger.warn('Stack Not Found');
      d.reject('Stack Not Found');
    }
    function success(data) {
      d.resolve(data);
    }
    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function getParam() {
    var d = $q.defer();
    if ($window.Stack) {
      $window.Stack.getParam(success, fail);
    } else {
      logger.warn('Stack Not Found');
      d.reject('Stack Not Found');
    }
    function success(data) {
      d.resolve(data);
    }
    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }
}
$bdStackFactory.$inject = ["$log", "$window", "$q"];

function $bdFilechooseFactory($log, $q, $window, $env) {
  var logger = $log('$bdFilechoose');
  return {
    open: open,
    pick: pick
  };

  function open(dir) {
    var d = $q.defer();

    if ($window.FileChoose) {
      dir = $env.fileDir + dir;// 'Allotab/' + dir;
      $window.FileChoose.open(dir, success, fail);
    } else {
      logger.warn('FileChoose Not Found');
      d.reject('FileChoose Not Found');
    }

    function success(path) {
      logger.debug(path);
      d.resolve(path);
    }

    function fail(data) {
      d.reject(data);
    }

    return d.promise;
  }

  function pick() {
    var d = $q.defer();

    if ($window.FileChoose) {
      $window.FileChoose.pick(success, fail);
    } else {
      logger.warn('FileChoose Not Found');
      d.reject('FileChoose Not Found');
    }

    function success(fileInfo) {
      try {
        fileInfo = JSON.parse(fileInfo);
      } catch(e) {}

      logger.debug(fileInfo);

      fileInfo = _.assign({
        path: '',
        mimeType: 'application/octet-stream'
      }, fileInfo);

      var ext = fileInfo.path.match(/\.[09-az]+/i);
      if (!ext) {
        $window.alert('선택할 수 없는 파일입니다.');
        return d.reject();
      }

      d.resolve(fileInfo);

    }

    function fail(data) {
      d.reject(data);
    }

    return d.promise;
  }
}
$bdFilechooseFactory.$inject = ["$log", "$q", "$window", "$env"];

function $bdExtensionFactory($log, $q, $window, $message, $env) {
  var logger = $log('$bdExtension');
  return {
    getTotalMemory: getTotalMemory,
    getFreeDiskSpace: getFreeDiskSpace,
    runKakao: runKakao,
    sms: sms,
    email: email,
    call: call,
    getFileList: getFileList,
    getImage: getImage,
    browser: browser,
    postBrowser: postBrowser
  };

  function getTotalMemory() {
    var d = $q.defer();
    if ($window.Extension) {
      $window.Extension.getTotalMemory(/*type*/null, success, fail);
    } else {
      logger.warn('Extension Not Found');
      d.reject('Extension Not Found');
    }

    function success(data) {
      logger.debug('success ', data);
      d.resolve(data);
    }

    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function getFreeDiskSpace() {
    var d = $q.defer();
    if ($window.cordova) {
      $window.cordova.exec(function(result) {
        d.resolve(result);
      }, function(error) {
        d.reject(error);
      }, 'File', 'getFreeDiskSpace', []);
    } else {
      logger.warn('cordova Not Found');
    }
    return d.promise;
  }

  function runKakao() {
    var d = $q.defer();
    if ($window.Extension) {
      $window.Extension.runKakao(success, fail);
    } else {
      logger.warn('Extension Not Found');
    }
    function success(data) {
      logger.debug('success ', data);
      d.resolve(data);
    }

    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function sms(addr, msg) {
    if ($window.Extension) {
      $window.Extension.sms(addr, msg);
    } else {
      logger.warn('Extension Not Found');
    }
  }

  function email(tos, title, message) {
    var d = $q.defer();
    if ($window.Extension) {
      $window.Extension.email(tos, title, message, success, fail);
    } else {
      logger.warn('Extension Not Found');
      d.reject('Extension Not Found');
    }
    function success(data) {
      d.resolve(data);
    }
    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function call(number) {
    logger.debug('call', number);
    if ($window.Extension) {
      $window.Extension.call(number);
    } else {
      logger.warn('Extension Not Found');
    }
  }

  function getFileList(folderName) {
    var d = $q.defer();
    if ($window.Extension) {
      var uri = $env.fileDir + (folderName || '');//'Allotab/' + (folderName || '');
      $window.Extension.getFileList(uri, success, fail);
    } else {
      logger.warn('Extension Not Found');
      d.reject('Extension Not Found');
    }

    function success(data) {
      logger.debug('success ', data);
      d.resolve(data);
    }

    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function getImage(url, headers) {
    var d = $q.defer();
    if ($window.Extension) {
      $window.Extension.getImage(url, headers, success, fail);
    } else {
      logger.warn('Extension Not Found');
      d.reject('Extension Not Found');
    }
    function success(data) {
      logger.debug('success ', data);
      d.resolve(data);
    }

    function fail(data) {
      d.reject(data);
    }
    return d.promise;
  }

  function browser(url) {
    if ($window.Extension) {
      $window.Extension.browser(url);
    } else {
      $window.open(url);
    }
  }

  function postBrowser(script) {
    if ($window.Extension) {
      $window.Extension.postBrowser(script);
    } else {
      $message.alert("디바이스에서 실행해주세요.");
    }
  }
}
$bdExtensionFactory.$inject = ["$log", "$q", "$window", "$message", "$env"];

function $bdPhoneFactory($log, $window, $bdExtension) {
  var logger = $log(this);
  return {
    call: call
  };
  function call(number) {
    $bdExtension.call(number);
  }
}
$bdPhoneFactory.$inject = ["$log", "$window", "$bdExtension"];

function $UUIDFactory($log, $window, $q) {
  var logger = $log('$UUIDFactory');
  return {
    get: get
  };
  function get() {
      var d = $q.defer();
      window.plugins.uniqueDeviceID.get(function(uuid){
          logger.log('get UUID Success', uuid);
          d.resolve(uuid);
      }, function(error){
          logger.log('get UUID Fail', uuid);
          d.reject(error);
      });
      return d.promise;
  }
}
$UUIDFactory.$inject = ["$log", "$window", "$q"];


function $bdresourceFactory($log, $window, $q) {
  
  return {
    isUpdateAPKFlag: isUpdateAPKFlag,
    isUpdateResouceFlag : isUpdateResouceFlag,
    checkApplication : checkApplication,
    checkUpdate : checkUpdate
  };
  
  function isUpdateAPKFlag() {
    
    var logger = $log(this);
    var d = $q.defer();
    
    if ($window.bdresource) {
      $window.bdresource.isUpdateAPKFlag(success, fail, '', '');
    } else {
      logger.warn('bdresource Not Found');
      d.reject('bdresource Not Found');
    }
    
    function success(data) {
      logger.debug('bdresource success ', data);
      d.resolve(data);
    }

    function fail(data) {
      logger.debug('bdresource fail ', data);
      d.reject(data);
    }
    return d.promise;
  }
  
  function isUpdateResouceFlag() {
    
    var logger = $log(this);
    var d = $q.defer();
    
    if ($window.bdresource) {
      $window.bdresource.isUpdateResouceFlag(success, fail, '', '');
    } else {
      logger.warn('bdresource Not Found');
      d.reject('bdresource Not Found');
    }
    
    function success(data) {
      logger.debug('bdresource success ', data);
      d.resolve(data);
    }

    function fail(data) {
      logger.debug('bdresource fail ', data);
      d.reject(data);
    }
    return d.promise;
  }
  
  function checkApplication() {
      
      var logger = $log(this);
      var d = $q.defer();
      
      if ($window.bdresource) {
        $window.bdresource.checkApplication(success, fail, '', '');
      } else {
        logger.warn('bdresource Not Found');
        d.reject('bdresource Not Found');
      }
      
      function success(data) {
        logger.debug('bdresource success ', data);
        d.resolve(data);
      }
  
      function fail(data) {
        logger.debug('bdresource fail ', data);
        d.reject(data);
      }
      return d.promise;
    }
  
  function checkUpdate() {
    
    var logger = $log(this);
    var d = $q.defer();
    
    if ($window.bdresource) {
      $window.bdresource.checkUpdate(success, fail, '', '');
    } else {
      logger.warn('bdresource Not Found');
      d.reject('bdresource Not Found');
    }
    
    function success(data) {
      logger.debug('bdresource success ', data);
      d.resolve(data);
    }
  
    function fail(data) {
      logger.debug('bdresource fail ', data);
      d.reject(data);
    }
    return d.promise;
  }
  
  
}
$bdresourceFactory.$inject = ["$log", "$window", "$q"];

// function $batteryFactory($log, $window, $q) {
  
//   return {
//     batterystatus: batterystatus,
//     batterycritical : batterycritical,
//     batterylow : batterylow
//   };
  
//   function batterystatus() {
    
//     var logger = $log(this);
//     var d = $q.defer();
    
//     if ($window.navigator.battery) {
//       $window.navigator.battery.batterystatus(success, fail, '', '');
//     } else {
//       logger.warn('bdresource Not Found');
//       d.reject('bdresource Not Found');
//     }
    
//     function success(data) {
//       logger.debug('bdresource success ', data);
//       d.resolve(data);
//     }

//     function fail(data) {
//       logger.debug('bdresource fail ', data);
//       d.reject(data);
//     }
//     return d.promise;
//   }
// }
/* //core/native/plugins.js */
}()
!function () { 'use strict';
/* core/security/auth.js */

angular
  .module('easi.core')
  .provider('$auth', $AuthProvider);

function $AuthProvider() {
  this.$get = authServiceFactory;
}

function authServiceFactory() {
  function AuthService() {
    return this;
  }

  AuthService.prototype.hasRole = function(user, roleOrSceneId) {
  	// 화면권한ID가 없거나 "ts"로 시작하지 않으면 권한이 있는 것으로 간주.
  	if (!roleOrSceneId || roleOrSceneId.length < 1 || roleOrSceneId.match(/^(?!ts).*/)) {
  		return true;
  	}

  	var rsList = user.getRoleServiceList();
  	if (!rsList || rsList.indexOf('NUM-0*') != -1) {
  		return true;
  	}
  	if (rsList.indexOf(roleOrSceneId) != -1) {
  		return true;
  	}
    return false;
  };

  var authService = new AuthService();
  return authService;
}
/* //core/security/auth.js */
}()
!function () { 'use strict';
/* core/security/user.js */

angular
  .module('easi.core')
  .provider('$user', $UserProvider);

var testUser = null;

function $UserProvider() {
  this.setTestUser = setTestUser;
  this.$get = userServiceFactory;
}

function setTestUser(value) {
  throw new Error('사용 중지된 함수입니다.');
}

function userServiceFactory($rootScope, $q, $storage, $window, $auth) {
  function UserService() {
    var curThis = this;
    this.cachedUserDeferred = null;
    this.currentUser = null;
    this.resetCurrentUser = function(){
      $storage.get('userDataInfo')
        .then(function(seData){
          curThis.currentUser = new User(seData);
        })
    };
    return this;
  }

  UserService.prototype.destroy = function() {
    this.cachedUserDeferred = null;
    this.currentUser = null;
    $storage.remove('userDataInfo');
  };

  UserService.prototype.get = function() {
    var self = this;
    var d = $q.defer();
    if (self.cachedUserDeferred) {
      return self.cachedUserDeferred.promise;
    } else if (self.currentUser) {
      d.resolve(self.currentUser);
      return d.promise;
    } else {
      self.cachedUserDeferred = d;


      var dd = $q.defer();
      var dp = dd.promise;
      var rv = $storage.get('userDataInfo');
      if(!rv) {
        dd.reject('로그인이 필요합니다.');
      } else if(rv.then) {
        dp = rv;
      } else {
        dd.resolve(rv);
      }

      dp
      .then(function(token) {
        if (!token) {
          //alert('세션정보가 유효하지 않습니다.');
          return $q.reject('세션정보가 유효하지 않습니다.');
        }
        var user = new User(token);
        self.currentUser = user;
        self.cachedUserDeferred = null;
        d.resolve(user);

      })
      .catch(function(e) {
        d.reject();
//        $window.location.href = 'public.html#/login';
      });
      return d.promise;
    }
  };

  function User(data) {
    var userDataInfo = _.clone(data);
    var self = this;

    _.assign(self, {
      getUserDataInfo : function() {
        return userDataInfo;
      },
      getAdminYn: function() {
        return data.adminYn;
      },
      getComAdminYn: function() {
        return data.comAdminYn;
      },
      getAgreeInfoList: function() {
        return data.agreeInfo;
      },
      getAuthInfoList: function() {
        return data.authInfo;
      },
      getLoginKind: function() {
        return data.loginKind;
      },
      getMenuInfoList: function() {
        return data.menuInfo;
      },
      getUserInfoList: function() {
        return data.userInfo;
      }
    });
  }

  var userService = new UserService();
  return userService;
}
userServiceFactory.$inject = ["$rootScope", "$q", "$storage", "$window", "$auth"];
/* //core/security/user.js */
}()
!function () { 'use strict';
/* api/cm/DBService.js */
'use strict';

api.factory('cmService', cmService);

function cmService($q, $timeout, $io) {

  var path = '/cm/cmService';
              
  return {
    insertErrLog : insertErrLog,
    selectMsdpProperty : selectMsdpProperty,
    selectMsdpPropertys : selectMsdpPropertys
  };
  
  
  /**
   * 에러 로그 저장
   */
  function insertErrLog(param,header) {
    var config = {}
    if(header){
      config.header = header;
    }
    
    var d = $q.defer();
    $io.apiExe(path +  '/insertErrLog',param,config)
      .then(function(response) {
        // success handler
        var header = response.serviceHeader;
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(body.user == undefined || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            return d.reject(JSON.stringify(response));
          }
        }catch(e){}
        return d.resolve(body);
      })
      .catch(function(reason) {
        // fail handler
        return d.reject(reason);
      });
    return d.promise;
  }
  
  /**
   * 프로퍼티 조회
   */
  function selectMsdpProperty(param,header) {
    var config = {}
    if(header){
      config.header = header;
    }
    
    var d = $q.defer();
    $io.apiExe(path +  '/selectMsdpProperty',param,config)
      .then(function(response) {
        // success handler
        var header = response.serviceHeader;
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(body.user == undefined || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            return d.reject(JSON.stringify(response));
          }
        }catch(e){}
        return d.resolve(body);
      })
      .catch(function(reason) {
        // fail handler
        return d.reject(reason);
      });
    return d.promise;
  }
  
  /**
   * 프로퍼티 조회(다건)
   */
  function selectMsdpPropertys(param) {
    
    var promisesArray = [];
    _.forEach(param.propertyKeys,function(obj){
      var propertyParam = {'propertyKey' : obj};
      promisesArray.push(selectMsdpProperty(propertyParam))
    });
      
    return $q.all(promisesArray)
    .then(function(result){
     return result
    });
    
  }
  
}
cmService.$inject = ["$q", "$timeout", "$io"];
/* //api/cm/DBService.js */
}()
!function () { 'use strict';
/* api/com/ComSvc.js */
'use strict';

api.factory('ComSvc', ComSvc);

function ComSvc($q, $timeout, $io, $message) {

    var path = '/api/com';

    return {
        selectCommonInfo: selectCommonInfo,
        insertErrLog : insertErrLog,
        fileDelete : fileDelete,
        fileUpload : fileUpload,
        selectBannerList : selectBannerList,
        updateBannerCntUp : updateBannerCntUp
    };

    /**
     * 공통코드 조회
     */
    function selectCommonInfo(code) {
        var param = {};
        param.commonInfoCode = code;					// 공통코드 코드
        return $io.api(path + '/selectCommonInfo.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 에러 메시지 저장
     */
    function insertErrLog(param) {
        // var param = {};
        // param.busnLinkKey = 'admin';							// [필수] 로그인 아이디
        // param.busnLinkKeyType = 'B1';							// [필수][공코:BUSN_LINK_KEY_TYPE] 관련회원종류
        // param.deviceModel = 'SM-P580';						// [선택] 모델명
        // param.deviceOsVer = '6.0';							// [선택] OS버전정보
        // param.svcUrl = '/login/login';						// [필수] 호출 URL
        // param.errMsgDesc = '에러메시지';							// [선택] 에러메시지
        // param.pageName = '/page.html';						// [선택] 호출 페이지
        // param.errType = '1';									// [선택] 에러 타입 [1: APP, 2: WEB]
        // param.errMsg = '상세에러내용';							// [필수] 에러 상세 내용
        // param.svcParam = '{param :22}';						// [필수] 호출 파라미터
        return $io.api(path + '/insertErrLog.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 파일삭제
     */
    function fileDelete(param) {
        // var param = {};
        // param.attachSeq = '1';			// [필수]파일등록 후 결과값에 파일 일련번호
        // param.fileOrgName = "test.jpg";	// [필수]파일등록 후 결과값에 파일 원본파일이름
        return $io.api(path + '/fileDelete.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 파일등록
     */
    function fileUpload(param) {
        // <form action="/api/com/fileUpload.login" method="post" enctype="multipart/form-data" >
        //     <input type = "file" name="uploadFile">
        //     <input type="hidden" name="busnLinkKey" value="-1"/>	<!-- 로그인 회원 일련번호 없을경우 -1-->
        //     <input type="submit">
        // </form>
        return $io.api(path + '/fileUpload.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 배너조회
     */
    function selectBannerList(param) {
        return $io.api('/api/selectBannerList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 배너수정
     */
    function updateBannerCntUp(param) {
        return $io.api('/api/updateBannerCntUp.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
}
ComSvc.$inject = ["$q", "$timeout", "$io", "$message"];
/* //api/com/ComSvc.js */
}()
!function () { 'use strict';
/* api/com/MetaService.js */
api.factory('MetaSvc', MetaSvc);

api.factory('MetaSvcCache', ["$cacheFactory", function($cacheFactory) {
  return $cacheFactory('MetaSvcData');
}]);

function MetaSvc($log, $q, $io, $timeout, $storage, MetaSvcCache, $rootScope) {
  var logger = $log('MetaSvc');
  var retry = 3;
  var getStQueue = [];
  var getStMap = {};
  var busy;

  // service end point.
  var path = '/api/com/selectCommonInfo.login';

  var _funcList = {
    getBizCode : getBizCode_sync, // 한번에 여러개 모아서 가져옴니다.
    getBizCodeAsync : getBizCode_async, // 한번에 한개씩 가져옵니다. JavaScript thread-safe 문제 생기면.  이 버전을 써야 합니다.

    // getServerTime : getServerTime

    // 기타 함수들 삭제. (실 서비스에서 코드는 조회 합니다!!)
  };

  var _bizCodeQueue = []; // code caching 용.
  var _bizCodeCallback = _.debounce(_fetchBizCode, 500, { maxWait: 1000}); // 500ms 기다린 후, 서버 요청


  return _funcList;

  // function getServerTimeApi(id) {
  //   var d = $q.defer();
  //   // $rootScope에 저장된 값이 없으면 서버시간을 조회하고, 있다면 저장된 값을 리턴한다.
  //   if( _.isUndefined( $rootScope.saveServerTime ) ) {
  //     $io.apiExe('/cm/TimeSvc/getServerTime', {})
  //     .then(function(res) {
  //       var rs = $rootScope.$eval('serviceBody.data.timestamp', res);
  //       if(!_.isEmpty($rootScope.entplanImsiDate)){
  //         rs = moment(parseInt($rootScope.entplanImsiDate)).valueOf();
  //         logger.log("기준일 변경 날짜 : " + moment(rs).format("YYYYMMDD HHmmss"));
  //       }
  //       $rootScope.saveServerTime = rs;
  //       d.resolve(rs);
  //     });
  //   } else {
  //     d.resolve($rootScope.saveServerTime);
  //   }
  //   return d.promise;
  // }

  /**
   * getServerTime 함수를 동시에 호출당할 경우 첫번째건만 실제 조회하도록 수정
   */
  // function getServerTime() {
  //   var d = $q.defer();
  //   var id = _.uniqueId('getServerTime#');
  //   getStQueue.push(id);
  //   getStMap[id] = {
  //     defer: d
  //   };
  //
  //   (function next() {
  //     if (!getStQueue.length || busy) {
  //       $rootScope.saveServerTime = undefined;    // 저장된 값 초기화
  //       return;
  //     }
  //     var id = getStQueue.shift();
  //     var config = getStMap[id];
  //     busy = true;
  //     getServerTimeApi(id)
  //       .then(config.defer.resolve, config.defer.reject)
  //       .finally(function() {
  //         delete getStMap[id];
  //         busy = false;
  //         next();
  //       });
  //   })();
  //
  //  return d.promise;
  //}

  // Promise 객체
  // function getServerDate() { return _.now(); }

  function getBizCode_sync(cd) {
    var cache = MetaSvcCache.get(cd);
    if(cache == null) {
      logger.debug('MetaCode cache not exists! - ['+cd+']');

      cache = $io.api(path, {commonInfoCode: cd})
      .then(function(res) {
        var rs = $rootScope.$eval('user.resultData.commonData', res);
        if(!_.isEmpty(rs)) {
          return _.chain(rs).map(function(d) { return { k : d.COMMON_INFO_VALUE1, v : d.COMMON_INFO_VALUE2}; }).value();
        }
        MetaSvcCache.remove(cd);
        getBizCode_sync(cd); // retry
        return [{ k : '', v : '코드오류'}];
      })
      .catch(function(rs) {
        MetaSvcCache.remove(cd);
        getBizCode_sync(cd); // retry
        return [{ k : '', v : '코드오류'}];
      })
      ;
      MetaSvcCache.put(cd, cache);
    } else {
      logger.debug('MetaCode cache hit! - ['+cd+']');
    }
    return cache;
  }

  // debounce 사용.
  function getBizCode_async(cd) {
    var cache = MetaSvcCache.get(cd);
    if(cache == null) {
      logger.debug('MetaCode cache not exists! - ['+cd+']');
      var d = $q.defer();

      // queue 에 밀어넣고~ async call
      _bizCodeQueue.unshift({ code : cd, cb : d });
      setTimeout(_bizCodeCallback(), 0);

      cache = d.promise;
      MetaSvcCache.put(cd, cache);
    } else {

      logger.debug('MetaCode cache hit! - ['+cd+']');
    }
    return cache;
  }

  function _fetchBizCode() {
    var set = [];
    var cached = [];
    while(_bizCodeQueue.length > 0) {
      set.unshift(_bizCodeQueue.shift());
    }

    $storage.get('commonInfoCode')
    .then(function(list) {
      var notFounds = [];
      cached = _.chain(set)
        .map(function(item) {
          var found = _.find(list, {commonInfoCode: item.code});
          if (!found) {
            notFounds.push(item.code);
          }
          return found;
        })
        .compact()
        .value();
      return notFounds;
    })
    .then(function(notFounds) {
      if (!notFounds.length) {
        return cached;
      }
      var qry = _.map(notFounds, function(d) { return {commonInfoCode: d}; }); // set := { code, defer }
      return  $io.api(path, qry[0])
      .then(function(res) {
        return cached.concat($rootScope.$eval('user.resultData.commonData', res));
      });
    })
    .then(function(rs) {
      if(!_.isEmpty(rs)) { // 그룹목록 취득 완료

        _.forEach(set, function(s) {

          var c = _.find(rs, function(r) { return !_.isEmpty(r.commonCodeValue) &&  r.commonCodeValue[0].commonInfoCode == s.code; });

          if(!_.isEmpty(c) && !_.isEmpty(c.commonCodeValue)) {
            var rv = _.chain(c.commonCodeValue)
              .map(function(d) { return { k : d.cdcValt, v : d.cdcValNm, s : d.uppCdcValt}; }).value();
            s.cb.resolve(rv);
          }
          else { s.cb.reject('Code 조회 오류'); }
        });

      }
      retry = 3;
    })
    .catch(function(res) {
      if(retry-- > 0) {
        _.forEach(set, function(s) {
          _bizCodeQueue.unshift(s);
        });
        setTimeout(_bizCodeCallback(), 0);
      } else {
        _.forEach(set, function(s) {
          s.cb.reject(res);
          MetaSvcCache.remove(s.code);
        });
        retry = 3;
      }
    })
    ;
  }

}
MetaSvc.$inject = ["$log", "$q", "$io", "$timeout", "$storage", "MetaSvcCache", "$rootScope"];/* //api/com/MetaService.js */
}()
!function () { 'use strict';
/* api/com/com.constant.js */

angular
  .module('com.constant', [])
  .constant('comConstant', _.deepFreeze({

      CP_USE_USER             : 'CP_USE_USER',              // 쿠폰사용자
      CP_BUY_KIND             : 'CP_BUY_KIND',              // 쿠폰구매종류
      CP_BUY_PAY_KIND         : 'CP_BUY_PAY_KIND',          // 결제방식
      CP_USE_STATE            : 'CP_USE_STATE',             // 쿠폰사용상태
      MESSAGE_TYPE            : 'MESSAGE_TYPE',             // 푸쉬종류타입
      STATE_CODE              : 'STATE_CODE',               // 푸쉬발송상태
      MSG_TYPE                : 'MSG_TYPE',                 // 푸쉬메시지타입
      RESULT_CODE             : 'RESULT_CODE',              // 푸쉬결과코드
      AUTH_INFO_KIND          : 'AUTH_INFO_KIND',           // 권한종류
      HP_KIND                 : 'HP_KIND',                  // 휴대전화회사종류
      COMPANY_STATE           : 'COMPANY_STATE',            // 기업회원상태
      PUSH_TYPE               : 'PUSH_TYPE',                // PSIDORCUID
      QNA_KIND                : 'QNA_KIND',                 // QNA종류
      REQUEST_KIND            : 'REQUEST_KIND',             // 요청종류
      REQUEST_STATE           : 'REQUEST_STATE',            // 요청상태
      CANCEL_KIND             : 'CANCEL_KIND',              // 취소종류
      ATTEND_NO_STATE         : 'ATTEND_NO_STATE',          // 미출석상태
      ATTEND_NO_LEAVE_STATE   : 'ATTEND_NO_LEAVE_STATE',    // 출석,무단이탈상태
      OPEN_YN                 : 'OPEN_YN',                  // 푸쉬오픈여부
      RCUT_BASE_STATE         : 'RCUT_BASE_STATE',          // 모집상태값
      HIS_KIND                : 'HIS_KIND',                 // 이력종류
      PGM_REUT_FIELD          : 'PGM_REUT_FIELD',           // 지원분야
      MEMBER_KIND             : 'MEMBER_KIND',              // 회원종류
      JOB                     : 'JOB',                      // 직업
      SUPPORT_FIELD           : 'SUPPORT_FIELD',            // 지원분야
      HOPE_CAST               : 'HOPE_CAST',                // 희방배역
      BODY_INFO               : 'BODY_INFO',                // 신체사항
      HAIR_STATE              : 'HAIR_STATE',               // 헤어상태
      HAIR_HEIGHT             : 'HAIR_HEIGHT',              // 헤어길이
      TOOTH_CORREC            : 'TOOTH_CORREC',             // 치아교정
      EXPOSURE_INFO           : 'EXPOSURE_INFO',            // 노출가능여부
      TATOO_INFO              : 'TATOO_INFO',               // 문신
      PIER_INFO               : 'PIER_INFO',                // 피어싱
      BEARD_INFO              : 'BEARD_INFO',               // 수염
      DIMPLE_INFO             : 'DIMPLE_INFO',              // 보조개
      DOUBLE_EYE_INFO         : 'DOUBLE_EYE_INFO',          // 눈쌍꺼플
      BOTTOMS_SIZE            : 'BOTTOMS_SIZE',             // 하의사이즈
      TOP_SIZE                : 'TOP_SIZE',                 // 상의사이즈
      DRIVE_BICYCLE_YN        : 'DRIVE_BICYCLE_YN',         // 자전거운전
      DRIVE_MOTERCYCLE_YN     : 'DRIVE_MOTERCYCLE_YN',      // 오토바이운전
      SMOKE_YN                : 'SMOKE_YN',                 // 흡연여부
      FIELD                   : 'FIELD',                    // 경력 분류
      BROADCASTING_STATION    : 'BROADCASTING_STATION',     // 방송국
      PART                    : 'PART',                     // 배역
      PENALTY_CODE            : 'PENALTY_CODE',             // 패널티종류코드
      PENALTY_KIND            : 'PENALTY_KIND',             // 패널티종류
      AGREE_KIND              : 'AGREE_KIND',               // 정보동의종류
      SAMPLE_VIDEO            : 'SAMPLE_VIDEO',             // 샘플영상정보
      MAP_INFO_URL            : 'MAP_INFO_URL',             // 노선 검색
      PHOTO_INFO_URL          : 'PHOTO_INFO_URL',           // 사진 및 영상 용량 축소 방법 가이드 주소
      AGREE_INFO              : 'AGREE_INFO',               // 약관
      DRESS_INFO              : 'DRESS_INFO',               // 보유 의상
      GENDER                  : 'GENDER',                   // 성별
      BANK_NAME               : 'BANK_NAME',                // 은행명
      CLASS_INFO              : 'CLASS_INFO',               // 보유방송등급
      PUSH_ADMIN_NO           : 'PUSH_ADMIN_NO',            // [푸쉬발송용도]총관리자일련번호
      CHARGE_KIND             : 'CHARGE_KIND',              // 담당자종류
      MY_RCUT_SEL             : 'MY_RCUT_SEL'               // 내게맞춪정보조회조건

  }))
/* //api/com/com.constant.js */
}()
!function () { 'use strict';
/* api/company/CompanySvc.js */
'use strict';

api.factory('CompanySvc', CompanySvc);

function CompanySvc($q, $timeout, $io, $message) {

    var path = '/api/comp';

    return {
        insertPgm: insertPgm,
        insertRcut : insertRcut,
        selectMemberList : selectMemberList,
        selectPgmInfo : selectPgmInfo,
        selectPgmRcutListInfo : selectPgmRcutListInfo,
        setInterset : setInterset,
        requestMember : requestMember,
        selectMemberDetailInfo : selectMemberDetailInfo,
        selectPgmLstInfo : selectPgmLstInfo,
        selectPgmDetailInfo : selectPgmDetailInfo,
        modifyPgm : modifyPgm,
        deletePgm : deletePgm,
        modifyRcut : modifyRcut,
        deleteRcut : deleteRcut,
        selectRcutList : selectRcutList,
        selectRcutDetailInfo : selectRcutDetailInfo,
        selectPgmHopeCast : selectPgmHopeCast,
        selectRcutrRequestMemberList : selectRcutrRequestMemberList,
        selectRcutrRequestLastSchedulePushInfo : selectRcutrRequestLastSchedulePushInfo,
        sendMemberPush : sendMemberPush,
        procRequest : procRequest,
        updateAttendEndInfo : updateAttendEndInfo,
        updateNoLeaveState : updateNoLeaveState,
        insertMemberReview : insertMemberReview,
        rcutEndProc : rcutEndProc,
        selectMemberJoinInfolist : selectMemberJoinInfolist,
        procJoin : procJoin,
        selectIntersetMemberlist : selectIntersetMemberlist,
        selectRcutMemberlist : selectRcutMemberlist,
        selectRcutEndMemberlist : selectRcutEndMemberlist,
        selectRcutSelelctBoxList : selectRcutSelelctBoxList,
        insertNewCompanyMember : insertNewCompanyMember,
        selectCompanyAuthInfoList : selectCompanyAuthInfoList,
        selectCompanyMemberListInfo : selectCompanyMemberListInfo,
        updateCompanyMember : updateCompanyMember,
        deleteCompanyMember : deleteCompanyMember,
        insertAuthInfo : insertAuthInfo,
        updateAuthInfo : updateAuthInfo,
        selectCalculateAllPgmList : selectCalculateAllPgmList,
        selectCalculatePgmList : selectCalculatePgmList,
        procCalculate : procCalculate,
        selectCalculateDetailList : selectCalculateDetailList,
        selectQnaList : selectQnaList,
        selectQnaDetailInfo : selectQnaDetailInfo,
        insertQnaAnswer : insertQnaAnswer,
        selectRequstPushList : selectRequstPushList,
        selectCompanyCeoInfo : selectCompanyCeoInfo,
        updateCompanyCeoInfo : updateCompanyCeoInfo,
        selectMenuList : selectMenuList,
    };

    /**
     * 프로그램 등록
     */
    function insertPgm(param) {
        // var param = {};
        // param.companySeq = "100000000";							//[필수] 회사 일련번호
        // param.pgmStartDtm = "2018-04-20 00:00:00";		//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmEndDtm = "2018-08-20 00:00:00";			//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmReutField = "P1";						//[필수][공코:PGM_REUT_FIELD] 지원분야
        // param.pgmName = "니혼자사나?";						//[필수] 프로그램명
        // param.companyMemberSeq = "300000000";					//[필수] 로그인한 담당자 일련번호
        // param.mainInfo = "혼자사시는분만";						//[필수] 주요사항
        //
        // param.etcInfo = "연기잘하시는분";						//[선택] 기타사항
        // param.pgmImgAttachSeq = "1";						//[선택] 대표이미지 파일일련번호
        //
        // // 담당자정보
        // var chargeList = [];
        // var chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000000"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 		 		//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000001"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "3000000000"; 				//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // param.chargeList = chargeList;					//[필수] 담당자정보
        return $io.api(path + '/insertPgm.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 모집 등록
     */
    function insertRcut(param) {
        // var param = {};
        // param.pgmBaseSeq = "5";							//[필수] 프로그램 일련번호
        // param.companySeq = "100000000";							//[필수] 회사 일련번호
        // param.pgmStartDtm = "2018-04-20 00:00:00";		//[필수] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmEndDtm = "2018-08-20 00:00:00";			//[필수] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmHopeCast = "H1";							//[필수][공코 : HOPE_CAST] 배역
        // param.pgmPoint = "경기도 안양시 공원";					//[필수] 촬영장소
        // param.pgmMeetingHour = "07";						//[필수] 촬영시간 (시)
        // param.pgmMeetingMinite = "30";					//[필수] 촬영시간 (분)
        // param.pgmSupplies = "도시락";						//[필수] 준비물
        // param.pgmMainScene = "공원에선 산책하는 씬";				//[필수] 주요장면
        // param.pgmEtcInfo = "없음";							//[필수] 기타사항
        // param.companyMemberSeq = "300000000";				//[필수] 로그인한 담당자 일련번호
        // param.pgmImgAttachSeq = "1";						//[선택] 대표이미지 파일일련번호
        //
        // // 출연인원정보
        // var rcutSubInfoList = [];
        // var rcutSbuInfo = {};
        // rcutSbuInfo.ageMin = '20';						//[필수] 시작나이
        // rcutSbuInfo.ageMax = '60';						//[필수] 종료나이
        // rcutSbuInfo.gender = 'G2';						//[필수][공코: GENDER] 성별
        // rcutSbuInfo.peopleNum = '10';						//[필수] 인원수
        // rcutSbuInfo.dressInfo = 'D1';						//[필수][공코 : DRESS_INFO] 의상
        // rcutSbuInfo.partInfo = '지나가는사람1';				//[필수] 역활
        // rcutSubInfoList.push(rcutSbuInfo);
        //
        // rcutSbuInfo = {};
        // rcutSbuInfo.ageMin = '50';						//[필수] 시작나이
        // rcutSbuInfo.ageMax = '80';						//[필수] 종료나이
        // rcutSbuInfo.gender = 'G2';						//[필수][공코: GENDER] 성별
        // rcutSbuInfo.peopleNum = '20';						//[필수] 인원수
        // rcutSbuInfo.dressInfo = 'D3';						//[필수][공코 : DRESS_INFO] 의상
        // rcutSbuInfo.partInfo = '공원에서산책하는사람';			//[필수] 역활
        // rcutSubInfoList.push(rcutSbuInfo);
        // param.rcutSubInfoList = rcutSubInfoList;			//[필수] 모집연령/성별/인원정보
        //
        //
        // // 담당자정보
        // var chargeList = [];
        // var chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000001"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 		 	//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000002"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 			//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        // param.chargeList = chargeList;							//[필수] 담당자정보
        return $io.api(path + '/insertRcut.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 조회(조회 조건 긴급 포함)
     */
    function selectMemberList(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.emergYn = 'N';									//[선택] 긴급여부
        // param.startDate = '2018-05-21 00:00:00';				//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';				//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchKind = '2';								//[선택] 조회조건 [1:지역 2:이름 3:역명]
        // param.searchData = '홍';								//[선택] 검색어
        //
        // param.supportField = '';								//[선택][공코 : SUPPORT_FIELD] 지원분야
        // param.hopeCast = '';									//[선택][공코 : HOPE_CAST] 희망배역
        // param.startAge = '';									//[선택] 연령 시작
        // param.endAge = '';									//[선택] 연령 끝
        // param.memberGender = '';								//[선택][공코 : MEMBER_GENDER] 성별
        // param.heightStart = '';								//[선택] 키 시작
        // param.heightEnd = '';									//[선택] 키 끝
        // param.weightStart = '';								//[선택] 몸무게 시작
        // param.weightEnd = '';									//[선택] 몸무게 끝
        // param.requestCount = '';								//[선택] 출연횟수
        // param.bodyInfo = '';									//[선택][공코 : BODY_INFO] 신체사항
        // param.classInfo = '';									//[선택][공코 : CLASS_INFO] 보유 방송등급
        // param.hairHeight = '';								//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // param.tatooInfo = '';									//[선택][공코 : TATOO_INFO] 타투여부
        // param.pierInfo = '';									//[선택][공코 : PIER_INFO] 피어싱여부
        return $io.api(path + '/selectMemberList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 진행중 프로그램 조회
     */
    function selectPgmInfo(param) {
        // var param = {};
        // param.pgmReutField= newVal;						//[필수][공코: PGM_REUT_FIELD] 분야
        // param.pgmEndDtmSet = 'Y';							//진행중프로그램만 조회
        return $io.api(path + '/selectPgmInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램일련번호로 모집정보조회
     */
    function selectPgmRcutListInfo(param) {
        // var param = {};
        // param.pgmBaseSeq = '5';
        return $io.api(path + '/selectPgmRcutListInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 관심회원 설정
     */
    function setInterset(param) {
        // var param = {};
        // param.procKind = 'D';									//[필수] (I : 등록, D : 삭제)
        // param.memberSeq = 5;									//[필수] 관심회원 일련번호
        return $io.api(path + '/setInterset.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업 출연자 출연요청
     */
    function requestMember(param) {
        // var param = {};
        // param.memberSeq = '5';									//[필수] 요청 회원 일련번호
        // param.rcutBaseSeq = '17';								//[필수] 요청 모집 일련번호
        return $io.api(path + '/requestMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 상세조회
     */
    function selectMemberDetailInfo(param) {
        // var param = {};
        // param.memberSeq = '5';									//[필수] 요청 회원 일련번호
        // param.rcutBaseSeq = '17';								//[선택] 모집상세에서 출연자상세조회시 추가항목(출연자확인사항,관리자확인사항)에 필요
        //                                                         // 모집일련번호
        return $io.api(path + '/selectMemberDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 리스트 조회
     */
    function selectPgmLstInfo(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.pgmReutField = '';								//[선택][공코:PGM_REUT_FIELD] 분야
        // param.pgmStateKind = '';								//[선택] '' : 전체 ,  'P1' : 상태 대기, 'P2' : 상태 진행중, 'P3' : 상태 종료
        // param.startDate = '';									//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchData = '';								//[선택] 검색어(프로그램명)
        return $io.api(path + '/selectPgmLstInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 상세 조회
     */
    function selectPgmDetailInfo(param){
        // var param = {};
        // param.pgmBaseSeq = '5';								//[필수] 프로그램 일련번호
        return $io.api(path + '/selectPgmDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 수정
     */
    function modifyPgm(param) {
        // var param = {};
        // param.pgmBaseSeq = '5';								//[필수] 프로그램 일련번호
        //
        // param.pgmStartDtm = "2018-04-20 00:00:00";			//[선택] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmEndDtm = "2018-09-20 00:00:00";				//[선택] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmReutField = "P2";							//[선택][공코:PGM_REUT_FIELD] 지원분야
        // param.pgmName = "니혼자사나?(수정)";						//[선택] 프로그램명
        // param.mainInfo = "혼자사시는분만(수정)";					//[선택] 주요사항
        // param.etcInfo = "연기잘하시는분(수정)";						//[선택] 기타사항
        // param.pgmImgAttachSeq = "1";							//[선택] 대표이미지 파일일련번호
        //
        // // 담당자정보( 기존 정보 모두 삭제후 재입력입니다. 모든 정보 담아서 보내주세요)
        // // 수정이 없을경우에는 [chargeList]담지마세요.
        // var chargeList = [];
        // var chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000001"; 		//[필수] 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000002"; 		//[필수] 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // param.chargeList = chargeList;							//[필수] 담당자정보
        return $io.api(path + '/modifyPgm.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 삭제
     */
    function deletePgm(param) {
        // var param = {};
        // param.pgmBaseSeq = '6';								//[필수] 프로그램 일련번호
        return $io.api(path + '/deletePgm.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 수정
     */
    function modifyRcut(param) {
        // var param = {};
        // param.rcutBaseSeq = '18';								//[필수] 모집 일련번호
        //
        // param.pgmStartDtm = "2018-04-20 00:00:00";		//[선택] 촬영시작시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmEndDtm = "2018-10-20 00:00:00";			//[선택] 촬영종료시간 [일시 패턴 : 0000-00-00 00:00:00]
        // param.pgmHopeCast = "H2";							//[선택][공코 : HOPE_CAST] 배역
        // param.pgmPoint = "경기도 안양시 공원(수정)";				//[선택] 촬영장소
        // param.pgmMeetingHour = "08";						//[선택] 촬영시간 (시)
        // param.pgmMeetingMinite = "40";					//[선택] 촬영시간 (분)
        // param.pgmSupplies = "도시락(수정)";					//[선택] 준비물
        // param.pgmMainScene = "공원에선 산책하는 씬(수정)";			//[선택] 주요장면
        // param.pgmEtcInfo = "없음(수정)";						//[선택] 기타사항
        // param.pgmImgAttachSeq = "1";						//[선택] 대표이미지 파일일련번호
        //
        // // 출연인원정보(수정이 있을 경우 모든 정보를 담아주세요. 삭제 후 새로 입력합니다.)
        // // 수정이 없을 경우 rcutSubInfoList 를 담지 마세요
        // var rcutSubInfoList = [];
        // var rcutSbuInfo = {};
        // rcutSbuInfo.ageMin = '20';						//[필수] 시작나이
        // rcutSbuInfo.ageMax = '60';						//[필수] 종료나이
        // rcutSbuInfo.gender = 'G2';						//[필수][공코: GENDER] 성별
        // rcutSbuInfo.peopleNum = '10';						//[필수] 인원수
        // rcutSbuInfo.dressInfo = 'D1';						//[필수][공코 : DRESS_INFO] 의상
        // rcutSbuInfo.partInfo = '지나가는사람1';				//[필수] 역활
        // rcutSubInfoList.push(rcutSbuInfo);
        //
        // rcutSbuInfo = {};
        // rcutSbuInfo.ageMin = '50';						//[필수] 시작나이
        // rcutSbuInfo.ageMax = '80';						//[필수] 종료나이
        // rcutSbuInfo.gender = 'G2';						//[필수][공코: GENDER] 성별
        // rcutSbuInfo.peopleNum = '20';						//[필수] 인원수
        // rcutSbuInfo.dressInfo = 'D3';						//[필수][공코 : DRESS_INFO] 의상
        // rcutSbuInfo.partInfo = '공원에서산책하는사람';			//[필수] 역활
        // rcutSubInfoList.push(rcutSbuInfo);
        // param.rcutSubInfoList = rcutSubInfoList;			//[필수] 모집연령/성별/인원정보
        //
        //
        // // 담당자정보( 기존 정보 모두 삭제후 재입력입니다. 모든 정보 담아서 보내주세요)
        // // 수정이 없을경우에는 [chargeList]담지마세요.
        // var chargeList = [];
        // var chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000000"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 		 		//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        //
        //
        // chargeMember = {};
        // chargeMember.chargeCompanyMemberSeq = "300000001"; 		//[필수] 담당자 일련번호
        // chargeMember.companyMemberSeq = "300000000"; 				//[필수] 로그인한 담당자 일련번호
        // chargeList.push(chargeMember);
        // param.chargeList = chargeList;					//[필수] 담당자정보
        return $io.api(path + '/modifyRcut.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 삭제
     */
    function deleteRcut(param) {
        // var param = {};
        // param.rcutBaseSeq = '13';								//[필수] 출연자 모집 일련번호
        return $io.api(path + '/deleteRcut.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 리스트 조회
     */
    function selectRcutList(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.stateKind = '';									//[선택] '' : 전체 ,  'S1' : 예정(모집중)프로그램, 'S2' : 예정(모집완료)프로그램, 'S3' : 진행중프로그램, 'S4' : 종료프로그램
        // param.startDate = '';									//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영기간 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchData = '';								//[선택] 검색어(프로그램명)
        return $io.api(path + '/selectRcutList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자모집상세조회
     */
    function selectRcutDetailInfo(param) {
        // var param = {};
        // param.rcutBaseSeq = "1";					//[필수] 모집일련번호
        return $io.api('/api/mem' + '/selectRcutDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램 분야에 따른 배역 조회
     */
    function selectPgmHopeCast(param) {
        // var param = {};
        // param.pgmReutField= 'P1';			//[필수][공코: PGM_REUT_FIELD] 분야
        return $io.api(path + '/selectPgmHopeCast.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출연자 리스트 조회 )
     */
    function selectRcutrRequestMemberList(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        // param.rcutBaseSeq = "10";								//[필수] 모집일련번호
        //
        // param.requestState = '';								//[선택] '' : 전체 ,  'R1' : (확정), 'R2' : (신청), 'R3' :  (요청중)
        // param.memberStateCode = '';							//[선택] '' : 전체 ,  출석(지각):A1/무단이탈:A2/기상:A3/출발:A4/출석:A5/미출석:A6/확정:A7/신청:A8/요청중:A9
        // param.searchData = '';								//[선택] 검색어(이름)
        return $io.api(path + '/selectRcutrRequestMemberList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출연자 리스트 조회  - 스케줄변경알림조회)
     */
    function selectRcutrRequestLastSchedulePushInfo(param) {
        // var param = {};
        // param.rcutBaseSeq = "20";								//[필수] 모집일련번호
        return $io.api(path + '/selectRcutrRequestLastSchedulePushInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출연자 리스트 조회  - 알림발송)
     */
    function sendMemberPush(param) {
        // var param = {};
        // param.memberSeq = "5";								//[필수] 회원일련번호
        // param.rcutBaseSeq = "20";								//[필수] 모집일련번호
        // param.msg = "늦지말고 모임장소에 오세요.";						//[필수] 전달 메시지
        return $io.api(path + '/sendMemberPush.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(모집에 대한 요청 취소, 요청수락 ,수락취소)
     */
    function procRequest(param) {
        // var param = {};
        // param.proKind = "P3";									//[필수] 처리종류 (P1: 요청 취소 , P2: 요청수락, P3: 수락취소)
        // param.requestInfoSeq = "15";							//[필수] 모집신청일련번호
        return $io.api(path + '/procRequest.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출석, 종료, 지각 입력)
     */
    function updateAttendEndInfo(param) {
        // var param = {};
        // param.companyMemberAttendDtm = "2018-05-16 18:30:00";		//[필수] 출석일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.companyMemberAttendPoint = "용산";					    //[필수] 출석장소
        //
        // param.companyMemberEndDtm = "2018-05-16 23:30:00";			//[필수] 종료일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.companyMemberEndPoint = "용산";						    //[필수] 종료장소
        //
        // param.attendNoState = "A1";									//[필수] 출석,지각 코드 ( 출석 : A1, 지각: A3)
        // param.requestInfoSeq = "15";								    //[필수] 모집신청일련번호
        return $io.api(path + '/updateAttendEndInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(미출석, 무단이탈 처리)
     */
    function updateNoLeaveState(param) {
        // var param = {};
        // param.attendNoLeaveState = "A1";							//[필수][공코:ATTEND_NO_LEAVE_STATE] 출석,지각 코드 ( 미출석 : A1, 무단이탈: A2)
        // param.attendNoLeaveNote = "이사람 도망감";					//[필수] 미출석 및 무단이탈 관리자 확인사항
        // param.requestInfoSeq = "15";								//[필수] 모집신청일련번호
        return $io.api(path + '/updateNoLeaveState.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(평가입력)
     */
    function insertMemberReview(param) {
        // var param = {};
        // param.requestInfoSeq = "15";									//[필수] 모집신청일련번호
        // param.reviewCntn = "연기잘하고 이쁨";							//[필수] 평가 내용
        return $io.api(path + '/insertMemberReview.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 상세(출연자 모집 촬영 종료 처리, 출연자 모집 완료 처리 )
     */
    function rcutEndProc(param) {
        // var param = {};
        // param.rcutBaseSeq = "20";									//[필수] 모집일련번호
        // param.rcutBaseState = "R2";									//[필수][공코:RCUT_BASE_STATE] R2 : 모집종료 R3 : 촬영종료
        return $io.api(path + '/rcutEndProc.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 회원가입 승인 , 반려 목록
     */
    function selectMemberJoinInfolist(param) {
        // var param = {};
        // param.memberState = 'M1';								//[필수][공코:MEMBER_STATE] (M1 : 승인대기, M3 : 반려 )
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';									//[선택] 검색어(이름)
        // param.startDate = '';									//[선택] 가입일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';										//[선택] 가입일시 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectMemberJoinInfolist.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 회원가입 승인 반려 처리
     */
    function procJoin(param) {
        // var param = {};
        // param.memberSeq = "5";									//[필수] 회원일련번호
        // param.memberState = "M2";								//[필수][공코:MEMBER_STATE] (M2 : 승인처리 M3: 반려처리)
        return $io.api(path + '/procJoin.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 관심출연자 조회
     */
    function selectIntersetMemberlist(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.startDate = '2018-05-21 00:00:00';				    //[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';					//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchKind = '2';									//[선택] 조회조건 [1:지역 2:이름 3:역명]
        // param.searchData = '홍';								    //[선택] 검색어
        //
        // param.supportField = '';								    //[선택][공코 : SUPPORT_FIELD] 지원분야
        // param.hopeCast = '';									    //[선택][공코 : HOPE_CAST] 희망배역
        // param.startAge = '';									    //[선택] 연령 시작
        // param.endAge = '';										//[선택] 연령 끝
        // param.memberGender = '';								    //[선택][공코 : MEMBER_GENDER] 성별
        // param.heightStart = '';									//[선택] 키 시작
        // param.heightEnd = '';									//[선택] 키 끝
        // param.weightStart = '';									//[선택] 몸무게 시작
        // param.weightEnd = '';									//[선택] 몸무게 끝
        // param.requestCount = '';								    //[선택] 출연횟수
        // param.bodyInfo = '';									    //[선택][공코 : BODY_INFO] 신체사항
        // param.classInfo = '';									//[선택][공코 : CLASS_INFO] 보유 방송등급
        // param.hairHeight = '';									//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // param.tatooInfo = '';									//[선택][공코 : TATOO_INFO] 타투여부
        // param.pierInfo = '';									    //[선택][공코 : PIER_INFO] 피어싱여부
        return $io.api(path + '/selectIntersetMemberlist.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연수락/요청중 출연자 조회
     */
    function selectRcutMemberlist(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '홍';								    //[선택] 검색어
        // param.rcutBaseSeq = '';									//[선택] 프로그램 일련번호
        return $io.api(path + '/selectRcutMemberlist.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연완료 회원 조회
     */
    function selectRcutEndMemberlist(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.rcutBaseSeq = '';									//[선택] 프로그램 일련번호
        //
        // param.startDate = '2018-05-21 00:00:00';				    //[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';					//[선택] 촬영가능 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.searchKind = '2';									//[선택] 조회조건 [1:지역 2:이름 3:역명]
        // param.searchData = '홍';								    //[선택] 검색어
        //
        // param.supportField = '';								    //[선택][공코 : SUPPORT_FIELD] 지원분야
        // param.hopeCast = '';									    //[선택][공코 : HOPE_CAST] 희망배역
        // param.startAge = '';									    //[선택] 연령 시작
        // param.endAge = '';										//[선택] 연령 끝
        // param.memberGender = '';								    //[선택][공코 : MEMBER_GENDER] 성별
        // param.heightStart = '';									//[선택] 키 시작
        // param.heightEnd = '';									//[선택] 키 끝
        // param.weightStart = '';									//[선택] 몸무게 시작
        // param.weightEnd = '';									//[선택] 몸무게 끝
        // param.requestCount = '';								    //[선택] 출연횟수
        // param.bodyInfo = '';									    //[선택][공코 : BODY_INFO] 신체사항
        // param.classInfo = '';									//[선택][공코 : CLASS_INFO] 보유 방송등급
        // param.hairHeight = '';									//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // param.tatooInfo = '';									//[선택][공코 : TATOO_INFO] 타투여부
        // param.pierInfo = '';									    //[선택][공코 : PIER_INFO] 피어싱여부
        return $io.api(path + '/selectRcutEndMemberlist.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 조회 화면  모집 리스트 조회
     */
    function selectRcutSelelctBoxList() {
        var param = {};
        return $io.api(path + '/selectRcutSelelctBoxList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 신규 담당자 등록
     */
    function insertNewCompanyMember(param) {
        // var param = {};
        //
        // param.companyMemberName = '길순이';							//[필수] 담당자이름
        // param.companyMemberPosition = '과장';						    //[필수] 담당자직책
        // param.companyMemberId = 'test1';							    //[필수] 담당자아이디
        // param.companyMemberPwd = '12345';							//[필수] 담당자패스워드
        // param.companyMemberPhoneKind = 'H1';						    //[필수][공코:HP_KIND] 담당자휴대폰회사정보
        // param.companyMemberPhoneNo = '01012341234';					//[필수] 담당자휴대폰번호
        // param.autnInfoSeq = '5';									    //[필수] 권한일련번호
        //
        //
        //
        // param.companyMemberTel = '0212341234';						//[선택] 담당자전화번호
        // param.companyMemberEmail = 'test@naver.com';			    	//[선택] 담당자이메일
        return $io.api(path + '/insertNewCompanyMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 소속(권한)정보
     */
    function selectCompanyAuthInfoList() {
        var param = {};
        return $io.api(path + '/selectCompanyAuthInfoList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 담당자 리스트조회(상세포함)
     */
    function selectCompanyMemberListInfo(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어(이름)
        // param.autnInfoSeq = '';								//[선택] 권한일련번호
        return $io.api(path + '/selectCompanyMemberListInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 담당자 수정
     */
    function updateCompanyMember(param) {
        // var param = {};
        //
        // param.modifyCompanyMemberSeq = '300000002';					//[필수] 수정담당자일련번호
        //
        // param.companyMemberPosition = '과장(수정)';					    //[선택] 담당자직책
        // param.companyMemberPwd = '12345';							//[선택] 담당자패스워드
        // param.companyMemberPhoneKind = 'H2';						    //[선택][공코:HP_KIND] 담당자휴대폰회사정보
        // param.companyMemberPhoneNo = '01011111111';					//[선택] 담당자휴대폰번호
        // param.autnInfoSeq = '5';									    //[선택] 권한일련번호
        // param.companyMemberTel = '0212341234';						//[선택] 담당자전화번호
        // param.companyMemberEmail = 'tes1t@naver.com';				//[선택] 담당자이메일
        return $io.api(path + '/updateCompanyMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 담당자 삭제
     */
    function deleteCompanyMember(param) {
        // var param = {};
        //
        // param.deleteCompanyMemberSeq = '300000005';				//[필수] 삭제담당자일련번호
        return $io.api(path + '/deleteCompanyMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 권한 등록
     */
    function insertAuthInfo(param) {
        // var param = {};
        //
        // param.authInfoName = '총무부';						    //[필수] 권한이름
        // param.authInfoMenuCode = 'M0_M1';					//[필수] 메뉴권한코드
        // param.authInfoAlarmCode = 'M0_M1';					//[필수] 알림권한코드
        return $io.api(path + '/insertAuthInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 권한 수정
     */
    function updateAuthInfo(param) {
        // var param = {};
        //
        // param.authInfoSeq = '5';								//[필수]권한일련번호
        //
        // param.authInfoName = '총무부(수정';						//[선택] 권한이름
        // param.authInfoMenuCode = 'M0_M2';						//[선택] 메뉴권한코드
        // param.authInfoAlarmCode = 'M0_M2';						//[선택] 알림권한코드
        return $io.api(path + '/updateAuthInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 종료된 프로그램 목록 조회
     */
    function selectCalculateAllPgmList() {
        var param = {};
        return $io.api(path + '/selectCalculateAllPgmList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정산 필요한 리스트 조회
     */
    function selectCalculatePgmList(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.startDate = '2018-05-21 00:00:00';				    //[선택] 촬영 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';					//[선택] 촬영 일시 [일시 패턴 : 0000-00-00 00:00:00]
        //
        // param.pgmBaseSeq = '';									//[선택] 프로그램일련번호
        // param.searchData = '';									//[선택] 검색어(프로그램명)
        return $io.api(path + '/selectCalculatePgmList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정산 완료 처리
     */
    function procCalculate(param) {
        // var param = {};
        // param.memberSeq = 0;								    	//[필수] 회원 일련번호
        // param.requestInfoSeq = 5;								//[필수] 모집 신청 일련번호
        return $io.api(path + '/procCalculate.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정산 목록 상세 조회
     */
    function selectCalculateDetailList(param) {
        // var param = {};
        // param.rcutBaseSeq = 5;									//[필수] 모집 일련번호
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectCalculateDetailList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * QnA리스트
     */
    function selectQnaList(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;								    		//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectQnaList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * QnA상세
     */
    function selectQnaDetailInfo(param) {
        // var param = {};
        // param.qnaQueSeq = 0;									    //[필수] Qna질문 일련번호
        return $io.api(path + '/selectQnaDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * QnA 답변처리
     */
    function insertQnaAnswer(param) {
        // var param = {};
        // param.qnaQueSeq = '0';										//[필수] Qna질문 일련번호
        // param.detailCntn = '답변내용';								    //[필수] 답변내용
        return $io.api(path + '/insertQnaAnswer.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 보낸 알림 조회
     */
    function selectRequstPushList(param) {
        // var param = {};
        // param.startNo = 0;										//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										    //[필수] 페이징 페이지당보여질수
        //
        // param.messageType = '5';								    //[선택][공코: MESSAGE_TYPE] 메시지타입
        // param.rcutBaseSeq = '5';								    //[선택] 출연자 모집일련번호
        return $io.api(path + '/selectRequstPushList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업 정보(대표담당자) 조회
     */
    function selectCompanyCeoInfo() {
        var param = {};
        return $io.api(path + '/selectCompanyCeoInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업 정보(대표담당자) 수정
     */
    function updateCompanyCeoInfo(param) {
        // var param = {};
        // param.companyNoAttachSeq = 0;								//[선택] 사업자등록증첨부번호
        // param.companyAccAttachSeq = 0;								//[선택] 통장사본첨부번호
        // param.companyPermissionAttachSeq = 0;						//[선택] 대중문화예술기획업 허가증 첨부번호
        // param.companyJobProveAttachSeq = 0;							//[선택] 직업소개소 증빙자료 첨부번호
        // param.companyEtcAttachSeq = 0;								//[선택] 기타증빙서류첨부번호
        // param.accNo = 0;											    //[선택]계좌번호('-'제거)
        // param.accBankName = '';										//택][공코 : BANK_NAME]계좌은행명
        // param.accName = '';											//[선택]계좌예금주
        //
        // param.companyTel1 = '';										//[선택] 연락처1
        // param.companyTel2 = '';										//[선택] 연락처2
        // param.companyEmail = '';									    //[선택] 회사(담당자) 이메일 주소
        // param.companyMemeberPwd= '';								    //[선택] 대표담당자패스워드
        return $io.api(path + '/updateCompanyCeoInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 메뉴 정보 조회
     */
    function selectMenuList(param) {
        return $io.api('/api/selectMenuList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
}
CompanySvc.$inject = ["$q", "$timeout", "$io", "$message"];
/* //api/company/CompanySvc.js */
}()
!function () { 'use strict';
/* api/company/company.constant.js */

angular
  .module('company.constant', [])
  .constant('companyConstant', _.deepFreeze({

      CP_USE_USER             : 'CP_USE_USER',              // 쿠폰사용자
      CP_BUY_KIND             : 'CP_BUY_KIND',              // 쿠폰구매종류
      CP_BUY_PAY_KIND         : 'CP_BUY_PAY_KIND',          // 결제방식
      CP_USE_STATE            : 'CP_USE_STATE',             // 쿠폰사용상태
      MESSAGE_TYPE            : 'MESSAGE_TYPE',             // 푸쉬종류타입
      STATE_CODE              : 'STATE_CODE',               // 푸쉬발송상태
      MSG_TYPE                : 'MSG_TYPE',                 // 푸쉬메시지타입
      RESULT_CODE             : 'RESULT_CODE',              // 푸쉬결과코드
      AUTH_INFO_KIND          : 'AUTH_INFO_KIND',           // 권한종류
      HP_KIND                 : 'HP_KIND',                  // 휴대전화회사종류
      COMPANY_STATE           : 'COMPANY_STATE',            // 기업회원상태
      PUSH_TYPE               : 'PUSH_TYPE',                // PSIDORCUID
      QNA_KIND                : 'QNA_KIND',                 // QNA종류
      REQUEST_KIND            : 'REQUEST_KIND',             // 요청종류
      REQUEST_STATE           : 'REQUEST_STATE',            // 요청상태
      CANCEL_KIND             : 'CANCEL_KIND',              // 취소종류
      ATTEND_NO_STATE         : 'ATTEND_NO_STATE',          // 미출석상태
      ATTEND_NO_LEAVE_STATE   : 'ATTEND_NO_LEAVE_STATE',    // 출석,무단이탈상태
      OPEN_YN                 : 'OPEN_YN',                  // 푸쉬오픈여부
      RCUT_BASE_STATE         : 'RCUT_BASE_STATE',          // 모집상태값
      HIS_KIND                : 'HIS_KIND',                 // 이력종류
      PGM_REUT_FIELD          : 'PGM_REUT_FIELD',           // 지원분야
      MEMBER_KIND             : 'MEMBER_KIND',              // 회원종류
      JOB                     : 'JOB',                      // 직업
      SUPPORT_FIELD           : 'SUPPORT_FIELD',            // 지원분야
      HOPE_CAST               : 'HOPE_CAST',                // 희방배역
      BODY_INFO               : 'BODY_INFO',                // 신체사항
      HAIR_STATE              : 'HAIR_STATE',               // 헤어상태
      HAIR_HEIGHT             : 'HAIR_HEIGHT',              // 헤어길이
      TOOTH_CORREC            : 'TOOTH_CORREC',             // 치아교정
      EXPOSURE_INFO           : 'EXPOSURE_INFO',            // 노출가능여부
      TATOO_INFO              : 'TATOO_INFO',               // 문신
      PIER_INFO               : 'PIER_INFO',                // 피어싱
      BEARD_INFO              : 'BEARD_INFO',               // 수염
      DIMPLE_INFO             : 'DIMPLE_INFO',              // 보조개
      DOUBLE_EYE_INFO         : 'DOUBLE_EYE_INFO',          // 눈쌍꺼플
      BOTTOMS_SIZE            : 'BOTTOMS_SIZE',             // 하의사이즈
      TOP_SIZE                : 'TOP_SIZE',                 // 상의사이즈
      DRIVE_BICYCLE_YN        : 'DRIVE_BICYCLE_YN',         // 자전거운전
      DRIVE_MOTERCYCLE_YN     : 'DRIVE_MOTERCYCLE_YN',      // 오토바이운전
      SMOKE_YN                : 'SMOKE_YN',                 // 흡연여부
      FIELD                   : 'FIELD',                    // 경력 분류
      BROADCASTING_STATION    : 'BROADCASTING_STATION',     // 방송국
      PART                    : 'PART',                     // 배역
      PENALTY_CODE            : 'PENALTY_CODE',             // 패널티종류코드
      PENALTY_KIND            : 'PENALTY_KIND',             // 패널티종류
      AGREE_KIND              : 'AGREE_KIND',               // 정보동의종류
      SAMPLE_VIDEO            : 'SAMPLE_VIDEO',             // 샘플영상정보
      MAP_INFO_URL            : 'MAP_INFO_URL',             // 노선 검색
      PHOTO_INFO_URL          : 'PHOTO_INFO_URL',           // 사진 및 영상 용량 축소 방법 가이드 주소
      AGREE_INFO              : 'AGREE_INFO',               // 약관
      DRESS_INFO              : 'DRESS_INFO',               // 보유 의상
      GENDER                  : 'GENDER',                   // 성별
      BANK_NAME               : 'BANK_NAME',                // 은행명
      CLASS_INFO              : 'CLASS_INFO',               // 보유방송등급
      PUSH_ADMIN_NO           : 'PUSH_ADMIN_NO',            // [푸쉬발송용도]총관리자일련번호
      CHARGE_KIND             : 'CHARGE_KIND',              // 담당자종류
      MY_RCUT_SEL             : 'MY_RCUT_SEL'               // 내게맞춪정보조회조건

  }))
/* //api/company/company.constant.js */
}()
!function () { 'use strict';
/* api/guest/GuestSvc.js */
'use strict';

api.factory('GuestSvc', GuestSvc);

function GuestSvc($q, $timeout, $io, $message) {

    var path = '/api/mem';

    return {
        updatePushInfo: updatePushInfo,
        insertNoSchedule : insertNoSchedule,
        selectEmerRcutList : selectEmerRcutList,
        selectMyRcutList : selectMyRcutList,
        selectAllRcutList : selectAllRcutList,
        selectRcutDetailInfo : selectRcutDetailInfo,
        requestRcutMember : requestRcutMember,
        selectRequestMemberSubInfo : selectRequestMemberSubInfo,
        insertRequestMemberSubInfo : insertRequestMemberSubInfo,
        updateRequestMemberSubInfo : updateRequestMemberSubInfo,
        cancelRequestRcutMember : cancelRequestRcutMember,
        updateRequestCompanyInfo : updateRequestCompanyInfo,
        updateRequestCompanyCancel : updateRequestCompanyCancel,
        insertQna : insertQna,
        selectMyQna : selectMyQna,
        selectCompanyRequestInfo : selectCompanyRequestInfo,
        selectMyRequestInfo : selectMyRequestInfo,
        selectMyCancelRequestInfo : selectMyCancelRequestInfo,
        selectNoSchedule : selectNoSchedule,
        selectWatingSchedule : selectWatingSchedule,
        selectIngSchedule : selectIngSchedule,
        selectEndSchedule : selectEndSchedule,
        selectScheduleDetail : selectScheduleDetail,
        updateMeInfo : updateMeInfo,
        selectPushInfo : selectPushInfo,
        updatePushOpen : updatePushOpen,
        selectCouponList : selectCouponList,
        selectPaymentKind : selectPaymentKind,
        insertPaymentInfo : insertPaymentInfo,
        insertBuyCoupon : insertBuyCoupon,
        selectMyCouponList : selectMyCouponList,
        selectMyPenaltyInfo : selectMyPenaltyInfo,
        selectMyInfo : selectMyInfo,
        updateMyInfo : updateMyInfo,
        selectAgreeInfo : selectAgreeInfo,
        selectPgmList : selectPgmList
    };

    /**
     * 푸쉬정보입력
     */
    function updatePushInfo(param) {
        // var param = {};
        // param.pushId = '1234564564646544564';				// [필수] 푸쉬아이디
        // param.pushType = 'P1';							// [필수][공코:PUSH_TYPE] 푸쉬타입
        // param.memberSeq = '1';							// [필수] 회원 일련번호
        return $io.api(path + '/updatePushInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 불가일정 등록
     */
    function insertNoSchedule(param) {
        // var scheduleInfo = [];
        // var data = {};
        // data.busnLinkKey = '1';						//[필수] 회원 일련번호
        // data.delIns = 'I';							//[필수] 삭제,입력 여부 [I: 입력, D: 삭제][2018-05-08수정]
        // data.scheduleDtm = '2018-05-10 00:00:00';		//[필수] 불가일정 [일시 패턴 : 0000-00-00 00:00:00]
        // scheduleInfo.push(data);
        //
        // var data = {};
        // data.busnLinkKey = '1';						//[필수] 회원 일련번호
        // data.delIns = 'D';							//[필수] 삭제,입력 여부 [I: 입력, D: 삭제][2018-05-08수정]
        // data.scheduleDtm = '2018-05-13 00:00:00';		//[필수] 불가일정 [일시 패턴 : 0000-00-00 00:00:00]
        // scheduleInfo.push(data);
        //
        // var param = {};
        // param.scheduleInfo = scheduleInfo;				// [필수] 불가 일정 정보
        return $io.api(path + '/insertNoSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 급구 출연자 모집 조회
     */
    function selectEmerRcutList() {
        var param = {};
        return $io.api(path + '/selectEmerRcutList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 내게맞춤정보모집조회
     */
    function selectMyRcutList(param) {
        // var param = {};
        // param.memberSeq = '1';					//[필수] 회원 일련번호
        // param.orderType = "1";					//[필수] 정렬 순서[1,2,3,4] 랜덤으로 보냄(앱실행시마다변경)
        // param.startNo = 0;						//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;						//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectMyRcutList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 추천모집조회
     */
    function selectAllRcutList(param) {
        // var param = {};
        // param.orderType = "1";					//[필수] 정렬 순서[1,2,3,4] 랜덤으로 보냄(앱실행시마다변경)
        // param.startNo = 0;						//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;						//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectAllRcutList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자모집상세조회
     */
    function selectRcutDetailInfo(param) {
        // var param = {};
        // param.rcutBaseSeq = "1";					//[필수] 모집일련번호
        return $io.api(path + '/selectRcutDetailInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자모집신청
     */
    function requestRcutMember(param) {
        // var param = {};
        // param.memberSeq = "1";						//[필수] 회원 일련번호
        // param.rcutBaseSeq = "1";					//[필수] 모집일련번호
        return $io.api(path + '/requestRcutMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집  신청 후 알림 조회
     */
    function selectRequestMemberSubInfo(param) {
        // var param = {};
        // param.memberSeq = "1";					//[필수] 회원 일련번호
        // param.requestInfoSeq = "1";				//[필수] 모집신청일련번호
        return $io.api(path + '/selectRequestMemberSubInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 알림 등록(기상시간,  출발 장소)
     */
    function insertRequestMemberSubInfo(param) {
        // var param = {};
        // param.memberSeq = "1";					//[필수] 회원 일련번호
        // param.requestInfoSeq = "1";				//[필수] 모집신청일련번호
        // param.getupHour = "06";					//[필수] 기상시간 시(2자리)
        // param.getupMinite = "00";					//[필수] 기상시간 분(2자리)
        // param.startHour = "07";					//[필수] 출발시간 시(2자리)
        // param.startMinite = "00";					//[필수] 출발시간 분(2자리)
        // param.startPoint = "부천시 송내대로";			//[필수] 출발장소(GPS현재장소)
        return $io.api(path + '/insertRequestMemberSubInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 알림 수정(기상시간,  출발 장소)
     */
    function updateRequestMemberSubInfo(param) {
        // var param = {};
        // param.memberSeq = "1";					//[필수] 회원 일련번호
        // param.memberSubInfoSeq = "3";				//[필수] 알림일련번호
        // param.getupHour = "08";					//[필수] 기상시간 시(2자리)
        // param.getupMinite = "30";					//[필수] 기상시간 분(2자리)
        // param.startHour = "09";					//[필수] 출발시간 시(2자리)
        // param.startMinite = "30";					//[필수] 출발시간 분(2자리)
        // param.startPoint = "부천시 송내대로 1";			//[필수] 출발장소(GPS현재장소)
        return $io.api(path + '/updateRequestMemberSubInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 출연자 모집 신청 후 취소
     */
    function cancelRequestRcutMember(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 회원일련번호
        // param.requestInfoSeq = "2";							//[필수] 취소 신청 일련번호
        return $io.api(path + '/cancelRequestRcutMember.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업요청 수락
     */
    function updateRequestCompanyInfo(param) {
        // var param = {};
        // param.memberSeq = "2";								//[필수] 회원일련번호
        // param.requestInfoSeq = "5";							//[필수] 신청 일련번호
        // param.rcutBaseSeq = "10";								//[필수] 모집 일련번호
        return $io.api(path + '/updateRequestCompanyInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업요청 수락 후 취소
     */
    function updateRequestCompanyCancel(param) {
        // var param = {};
        // param.memberSeq = "2";								//[필수] 회원일련번호
        // param.requestInfoSeq = "5";							//[필수] 신청 일련번호
        // param.rcutBaseSeq = "10";								//[필수] 모집 일련번호
        return $io.api(path + '/updateRequestCompanyCancel.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 1대1문의 등록
     */
    function insertQna(param) {
        // var param = {};
        // param.companySeq = "1";								//[필수][회원정보 :MEMBER_COMPANY_INFO]회원소속회사일련번호
        // param.qnaKind = "Q1";									//[필수][공코 : QNA_KIND] QNA유형
        // param.pgmBaseSeq = "5";								//[필수] 프로그램 일련번호
        // param.title = "문의드립니다.";							//[필수] 문의제목
        // param.detailCntn = "이프로그램 담당자 이름은??";				//[필수] 문의내용
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.writeName = "홍길동";								//[필수] 작성자 이름
        // param.writePhone = "01012411234";						//[필수] 작성자전화번호
        // param.writeEmail = "test@naver.com";					//[필수] 작성자이메일
        return $io.api(path + '/insertQna.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 나의문의내역조회
     */
    function selectMyQna(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectMyQna.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 요청현황조회
     */
    function selectCompanyRequestInfo(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어
        return $io.api(path + '/selectCompanyRequestInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 신청현황조회
     */
    function selectMyRequestInfo(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어
        // param.startDate = '';								//[선택] 촬영시작일 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영종료일 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectMyRequestInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 취소현황조회
     */
    function selectMyCancelRequestInfo(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectMyCancelRequestInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 촬영불가일정조회
     */
    function selectNoSchedule(param) {
        // var param = {};
        // param.busnLinkKey = "1";								//[필수] 회원일련번호
        return $io.api(path + '/selectNoSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 대기중스케줄조회
     */
    function selectWatingSchedule(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어
        // param.startDate = '';									//[선택] 촬영시작일 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영종료일 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectWatingSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 진행중스케줄조회
     */
    function selectIngSchedule(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        return $io.api(path + '/selectIngSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 완료스케줄조회
     */
    function selectEndSchedule(param) {
        // var param = {};
        // param.memberSeq = "1";								//[필수] 작성자 일련번호
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.searchData = '';								//[선택] 검색어
        // param.startDate = '';									//[선택] 촬영시작일 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '';									//[선택] 촬영종료일 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectEndSchedule.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 스케줄상세조회
     */
    function selectScheduleDetail(param) {
        // var param = {};
        // param.rcutBaseSeq = '10';										//[필수] 모집일련번호
        // param.requestInfoSeq = '3';									//[필수] 모집신청일련번호
        return $io.api(path + '/selectScheduleDetail.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 본인 출석,종료,기상,출발 입력
     */
    function updateMeInfo(param) {
        // var param = {};
        // param.updateKind = 'U4';										//[필수] 입력종류[U1 : 출석, U2:종료, U3:기상, U4:출발]
        // param.memberSeq = '1';										//[필수] 회원일련번호
        // param.requestInfoSeq = '3';									//[필수] 모집신청일련번호
        //
        // // 출석 필수 파라미터
        // if(param.updateKind == 'U1'){
        //     param.meAttendPoint = '서울 강남';							//[필수] 출석 클릭한 장소(GPS연동결과값)
        // }
        //
        // // 종료 필수 파라미터
        // if(param.updateKind == 'U2'){
        //     param.meEndPoint = '서울 홍대';								//[필수] 종료 클릭한 장소(GPS연동결과값)
        // }
        //
        // // 기상 필수 파라미터
        // if(param.updateKind == 'U3'){
        //     param.meGetupPoint = '서울 신도림';							//[필수] 기상 클릭한 장소(GPS연동결과값)
        // }
        //
        // // 출발 필수 파라미터
        // if(param.updateKind == 'U4'){
        //     param.meStartPoint = '서울 신도림';							//[필수] 출발 클릭한 장소(GPS연동결과값)
        // }
        return $io.api(path + '/updateMeInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 미확인,확인 알림 조회
     */
    function selectPushInfo(param) {
        // var param = {};
        // param.busnLinkKey = "1";								//[필수] 조회 회원 일련번호
        // param.openYn = "P2";									//[필수][공코:OPEN_YN] 미확인,알림여부
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.messageType = '';								//[선택][공코:MESSAGE_TYPE] 푸쉬종류
        return $io.api(path + '/selectPushInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 알림 확인 처리
     */
    function updatePushOpen(param) {
        // var param = {};
        // param.pushResultSeq = "1";								//[필수] 푸쉬결과일련번호
        return $io.api(path + '/updatePushOpen.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 판매쿠폰리스트 조회
     */
    function selectCouponList(param) {
        // var param = {};
        // param.cpUseUser = "C1";								//[필수][공코:CP_USE_USER] 쿠폰사용자
        return $io.api(path + '/selectCouponList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 결제수단 조회
     */
    function selectPaymentKind(param) {
        // var param = {};
        // param.cpUseUser = "C1";								//[필수][공코:CP_USE_USER] 쿠폰사용자
        return $io.api(path + '/selectPaymentKind.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 결제 이력 입력
     */
    function insertPaymentInfo(param) {
        // var param = {};
        // param.paymentStatus = 'P1';			//[필수][공코:PAYMENT_STATUS] 결제결과
        // param.cpBuyPayKind = 'C2';			//[필수][공코:CP_BUY_PAY_KIND] 결제방식
        // param.cpListSeq = '2';				//[필수] 판매상품일련번호
        return $io.api(path + '/insertPaymentInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 쿠폰구매
     */
    function insertBuyCoupon(param) {
        // var param = {};
        // param.cpPaymentSeq = '2';			//[필수] 결제이력 일련번호
        // param.cpListSeq = '2';				//[필수] 판매상품일련번호
        return $io.api(path + '/insertBuyCoupon.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 쿠폰구매사용내역 조회
     */
    function selectMyCouponList(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;									//[필수] 페이징 페이지당보여질수
        //
        // param.startDate = '2018-05-01 00:00:00';			//[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.endDate = '2018-05-31 00:00:00';				//[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectMyCouponList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 패널티 정보 조회
     */
    function selectMyPenaltyInfo(param) {
        // var param = {};
        // param.startNo = 0;									//[필수] (현재페이지 -1) * 페이지당보여질수
        // param.endNo = 5;										//[필수] 페이징 페이지당보여질수
        //
        // param.penaltyKind = 'P1';								//[선택][공코 : PENALTY_KIND]
        // param.penaltyStDtm = '2018-05-01 00:00:00';			//[선택] 일시 [일시 패턴 : 0000-00-00 00:00:00]
        // param.penaltyEndDtm = '2018-05-31 00:00:00';			//[선택] ㅍ [일시 패턴 : 0000-00-00 00:00:00]
        return $io.api(path + '/selectMyPenaltyInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 나의 정보 조회
     */
    function selectMyInfo() {
        var param = {};
        return $io.api(path + '/selectMyInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 나의 정보 수정
     */
    function updateMyInfo(param) {
        // var memberBaseInfo = {};
        // var memberDetailInfo = {};
        // var memberCareerInfo = [];
        //
        // // 회원 기본
        // memberBaseInfo.memberHpKind = 'H2';						//[선택][공코 : HP_KIND] 휴대전화회사종류
        // memberBaseInfo.memberHp = '0101111111';					//[선택]휴대폰번호 ('-'제거)
        // memberBaseInfo.memberAddr = '경기도 부천시(수정)';			//[선택]기본 주소
        // memberBaseInfo.memberAddrDetail = '푸르지오 아파트(수정)';		//[선택]상세 주소
        // memberBaseInfo.memberAddrNo = '01111';					//[선택]우편번호
        // memberBaseInfo.memberSubwayName = '송내역(수정)';			//[선택]가까운지하철
        // memberBaseInfo.memberSubwayTime = '10';					//[선택]소요시간
        // memberBaseInfo.height = '170';							//[선택]키
        // memberBaseInfo.weight = '55';							//[선택]몸무게
        // memberBaseInfo.email = 'mo@naver.com';					//[선택]이메일
        // memberBaseInfo.photoAttachSeqs = '2|3|4';				//[선택]사진 첨부파일 일련번호
        //
        // memberBaseInfo.accNo = '1234564545';					//[선택]계좌번호('-'제거)
        // memberBaseInfo.accBankName = '국민';						//[선택][공코 : BANK_NAME]계좌은행명
        // memberBaseInfo.accName = '수정자';							//[선택]계좌예금주
        // memberBaseInfo.companyNoAttachSeqs = '5';				//[선택]사업자등록증 및 증빙서류 첨부파일 일련번호
        // memberBaseInfo.videoAttachSeqs = '6';					//[선택]동영상 첨부파일 일련번호
        //
        // // 회원 상세
        // memberDetailInfo.supportField = 'S2';					//[선택][공코 : SUPPORT_FIELD] 지원분야
        // memberDetailInfo.hopeCast = 'H2';						//[선택][공코 : HOPE_CAST] 희망배역
        //
        // memberDetailInfo.job = 'J2';							//[선택][공코 : JOB] 직업
        // memberDetailInfo.hopeCastEtc = '희망배역기타';				//[선택]희망배역기타
        // memberDetailInfo.bodyInfo = 'B2';						//[선택][공코 : BODY_INFO] 신체사항
        // memberDetailInfo.classInfo = 'C2';						//[선택][공코 : CLASS_INFO] 보유방송등급
        // memberDetailInfo.hairState = 'H2';						//[선택][공코 : HAIR_STATE] 헤어상태
        // memberDetailInfo.hairHeight = 'H2';						//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // memberDetailInfo.toothCorrec = 'T2';					//[선택][공코 : TOOTH_CORREC] 치아교정
        // memberDetailInfo.exposureInfo = 'E2';					//[선택][공코 : EXPOSURE_INFO] 노출가능여부
        // memberDetailInfo.tatooInfo = 'T2';						//[선택][공코 : TATOO_INFO] 문신
        // memberDetailInfo.pierInfo = 'P2';						//[선택][공코 : PIER_INFO] 피어싱
        // memberDetailInfo.beardInfo = 'B2';						//[선택][공코 : BEARD_INFO] 수염
        // memberDetailInfo.dimpleInfo = 'D2';						//[선택][공코 : DIMPLE_INFO] 보조개
        // memberDetailInfo.doubleEyeInfo = 'D2';					//[선택][공코 : DOUBLE_EYE_INFO] 눈쌍꺼플
        // memberDetailInfo.bottomsSize = 'B2';					//[선택][공코 : BOTTOMS_SIZE] 하의사이즈
        // memberDetailInfo.topSize = 'T2';						//[선택][공코 : TOP_SIZE] 상의사이즈
        // memberDetailInfo.footSize = '260';						//[선택]발사이즈 [숫자만]
        // memberDetailInfo.driveBicycleYn = 'D2';					//[선택][공코 : DRIVE_BICYCLE_YN] 자전거운전
        // memberDetailInfo.driveCarYn = 'D2';						//[선택][공코 : DRIVE_CAR_YN] 자동차운전
        // memberDetailInfo.driveMotercycleYn = 'D2';				//[선택][공코 : DRIVE_MOTERCYCLE_YN] 오토바이운전
        // memberDetailInfo.smokeYn = 'D2';						//[선택][공코 : SMOKE_YN] 흡연여부
        // memberDetailInfo.dressBaseCount = '1';					//[선택] 기본정장 벌수 [숫자만]
        // memberDetailInfo.dressBaseColor = '검정';					//[선택] 기본정장 색상
        // memberDetailInfo.dressSemiCount = '1';					//[선택] 세미정장 벌수 [숫자만]
        // memberDetailInfo.dressSemiColor = '검정';					//[선택] 세미정장 색상
        // memberDetailInfo.dressBlackShoes = 'N';					//[선택] 검정구두
        // memberDetailInfo.dressSwimsuit = 'N';					//[선택] 수영복
        // memberDetailInfo.dressSchoolUniform = 'N';				//[선택] 교복
        // memberDetailInfo.dressMilitaryBoots = 'N';				//[선택] 군화
        //
        // // 회원 경력(수정이 있을경우 모든 Data 보내야합니다. 모두 지우고 다시 입력함)
        // var careerInfo = {};
        // careerInfo.field = 'F2';								//[필수][공코 : FIELD] 경력 분류
        // careerInfo.pgmName = '무한도전(수정)';						//[필수]프로그램 명
        // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
        // careerInfo.year = '2018';								//[필수]년도
        // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
        // careerInfo.linesYn = 'Y';								//[필수]대사
        // memberCareerInfo.push(careerInfo);
        //
        // var careerInfo = {};
        // careerInfo.field = 'F2';								//[필수][공코 : FIELD] 경력 분류
        // careerInfo.pgmName = '1박2일(수정)';						//[필수]프로그램 명
        // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
        // careerInfo.year = '2018';								//[필수]년도
        // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
        // careerInfo.linesYn = 'N';								//[필수]대사
        // memberCareerInfo.push(careerInfo);
        //
        //
        // var param = {};
        // param.memberBaseInfo = memberBaseInfo;				// 회원 기본 정보
        // param.memberDetailInfo = memberDetailInfo;			// 회원 상세 정보
        // param.memberCareerInfo = memberCareerInfo;			// 회원 경력 정보
        return $io.api(path + '/updateMyInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정보동의조회(알림설정)
     */
    function selectAgreeInfo() {
        var param = {};
        return $io.api(path + '/selectAgreeInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 프로그램조회 (1:1 문의에서 필요한 목록)
     */
    function selectPgmList() {
        var param = {};
        return $io.api(path + '/selectPgmList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
}
GuestSvc.$inject = ["$q", "$timeout", "$io", "$message"];
/* //api/guest/GuestSvc.js */
}()
!function () { 'use strict';
/* api/guest/guest.constant.js */

angular
  .module('guest.constant', [])
  .constant('guestConstant', _.deepFreeze({

      CP_USE_USER             : 'CP_USE_USER',              // 쿠폰사용자
      CP_BUY_KIND             : 'CP_BUY_KIND',              // 쿠폰구매종류
      CP_BUY_PAY_KIND         : 'CP_BUY_PAY_KIND',          // 결제방식
      CP_USE_STATE            : 'CP_USE_STATE',             // 쿠폰사용상태
      MESSAGE_TYPE            : 'MESSAGE_TYPE',             // 푸쉬종류타입
      STATE_CODE              : 'STATE_CODE',               // 푸쉬발송상태
      MSG_TYPE                : 'MSG_TYPE',                 // 푸쉬메시지타입
      RESULT_CODE             : 'RESULT_CODE',              // 푸쉬결과코드
      AUTH_INFO_KIND          : 'AUTH_INFO_KIND',           // 권한종류
      HP_KIND                 : 'HP_KIND',                  // 휴대전화회사종류
      COMPANY_STATE           : 'COMPANY_STATE',            // 기업회원상태
      PUSH_TYPE               : 'PUSH_TYPE',                // PSIDORCUID
      QNA_KIND                : 'QNA_KIND',                 // QNA종류
      REQUEST_KIND            : 'REQUEST_KIND',             // 요청종류
      REQUEST_STATE           : 'REQUEST_STATE',            // 요청상태
      CANCEL_KIND             : 'CANCEL_KIND',              // 취소종류
      ATTEND_NO_STATE         : 'ATTEND_NO_STATE',          // 미출석상태
      ATTEND_NO_LEAVE_STATE   : 'ATTEND_NO_LEAVE_STATE',    // 출석,무단이탈상태
      OPEN_YN                 : 'OPEN_YN',                  // 푸쉬오픈여부
      RCUT_BASE_STATE         : 'RCUT_BASE_STATE',          // 모집상태값
      HIS_KIND                : 'HIS_KIND',                 // 이력종류
      PGM_REUT_FIELD          : 'PGM_REUT_FIELD',           // 지원분야
      MEMBER_KIND             : 'MEMBER_KIND',              // 회원종류
      JOB                     : 'JOB',                      // 직업
      SUPPORT_FIELD           : 'SUPPORT_FIELD',            // 지원분야
      HOPE_CAST               : 'HOPE_CAST',                // 희방배역
      BODY_INFO               : 'BODY_INFO',                // 신체사항
      HAIR_STATE              : 'HAIR_STATE',               // 헤어상태
      HAIR_HEIGHT             : 'HAIR_HEIGHT',              // 헤어길이
      TOOTH_CORREC            : 'TOOTH_CORREC',             // 치아교정
      EXPOSURE_INFO           : 'EXPOSURE_INFO',            // 노출가능여부
      TATOO_INFO              : 'TATOO_INFO',               // 문신
      PIER_INFO               : 'PIER_INFO',                // 피어싱
      BEARD_INFO              : 'BEARD_INFO',               // 수염
      DIMPLE_INFO             : 'DIMPLE_INFO',              // 보조개
      DOUBLE_EYE_INFO         : 'DOUBLE_EYE_INFO',          // 눈쌍꺼플
      BOTTOMS_SIZE            : 'BOTTOMS_SIZE',             // 하의사이즈
      TOP_SIZE                : 'TOP_SIZE',                 // 상의사이즈
      DRIVE_BICYCLE_YN        : 'DRIVE_BICYCLE_YN',         // 자전거운전
      DRIVE_MOTERCYCLE_YN     : 'DRIVE_MOTERCYCLE_YN',      // 오토바이운전
      SMOKE_YN                : 'SMOKE_YN',                 // 흡연여부
      FIELD                   : 'FIELD',                    // 경력 분류
      BROADCASTING_STATION    : 'BROADCASTING_STATION',     // 방송국
      PART                    : 'PART',                     // 배역
      PENALTY_CODE            : 'PENALTY_CODE',             // 패널티종류코드
      PENALTY_KIND            : 'PENALTY_KIND',             // 패널티종류
      AGREE_KIND              : 'AGREE_KIND',               // 정보동의종류
      SAMPLE_VIDEO            : 'SAMPLE_VIDEO',             // 샘플영상정보
      MAP_INFO_URL            : 'MAP_INFO_URL',             // 노선 검색
      PHOTO_INFO_URL          : 'PHOTO_INFO_URL',           // 사진 및 영상 용량 축소 방법 가이드 주소
      AGREE_INFO              : 'AGREE_INFO',               // 약관
      DRESS_INFO              : 'DRESS_INFO',               // 보유 의상
      GENDER                  : 'GENDER',                   // 성별
      BANK_NAME               : 'BANK_NAME',                // 은행명
      CLASS_INFO              : 'CLASS_INFO',               // 보유방송등급
      PUSH_ADMIN_NO           : 'PUSH_ADMIN_NO',            // [푸쉬발송용도]총관리자일련번호
      CHARGE_KIND             : 'CHARGE_KIND',              // 담당자종류
      MY_RCUT_SEL             : 'MY_RCUT_SEL'               // 내게맞춪정보조회조건

  }))
/* //api/guest/guest.constant.js */
}()
!function () { 'use strict';
/* api/ma/ma.constant.js */

angular
  .module('ma.constant', [])
  .constant('maConstant', _.deepFreeze({
    //Y
    COMMON_Y  : 'Y',
    //N
    COMMON_N  : 'N',
    //Delete
    COMMON_D  : 'D',
    //Update
    COMMON_U  : 'U',
    //Insert
    COMMON_I  : 'I',
    //ERROR
    COMMON_ERROR  : 'E'
    
  }))
/* //api/ma/ma.constant.js */
}()
!function () { 'use strict';
/* api/member/MemberSvc.js */
'use strict';

api.factory('MemberSvc', MemberSvc);

function MemberSvc($q, $timeout, $io, $message) {

    var path = '/api';

    return {
        selectMenuList: selectMenuList,
        login: login,
        logOut : logOut,
        findId : findId,
        requestPwFindAuth : requestPwFindAuth,
        authNoCheck : authNoCheck,
        updatePw : updatePw,
        insertMemberRequest : insertMemberRequest,
        selectCompanyList : selectCompanyList,
        selectIdCheck : selectIdCheck,
        selectRrnCheck : selectRrnCheck,
        selectSampleVideo : selectSampleVideo,
        selectTermInfo : selectTermInfo,
        insertCompanyInfo : insertCompanyInfo,
        insertAgree : insertAgree,
        selectAgreeInfo : selectAgreeInfo,
        selectCouponList : selectCouponList
    };

    /**
     * 로그인 전 메뉴 조회
     */
    function selectMenuList() {
        var param = {};
        param.menuKind = 'M1';					// M1:출연자 전체메뉴, M2:기업전체메뉴
        return $io.api(path + '/selectMenuList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }


    /**
     * 로그인
     */
    function login(param) {
        // var param = {};
        // param.loginKind = "L2";		// L1 : 개인, L2 : 기업
        // param.userId = "admin";		// 회원 아이디
        // param.userPw = "1234";		// 회원 패스워드
        // param.siteKind = 'APP';		// 웹/APP 구분
        return $io.api(path + '/login.login', param)
            .then(function(data){
                // ################출연자#####################
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                //     API : 정보동의 조회
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                //         로그인 결과 값에 존재(agreeInfo)
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                //     API : 메뉴 정보
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                //         로그인 결과 값에 존재(menuInfo)
                // +++++++++++++++++++++++++++++++++++++++++++++++++++++
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 로그아웃
     */
    function logOut() {
        var param = {};
        return $io.api(path + '/logOut.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 아이디 찾기(개인,기업)
     */
    function findId(param) {
        // var param = {};
        // param.findKind = '3';				// 1: 개인휴대폰번호찾기 2: 기업휴대폰번호찾기 3 : 기업사업자번호로대표자아이디찾기
        //
        // if(param.findKind == '1'){
        //     param.userName = '정준서';		// 회원이름
        //     param.userHpkind = 'H1';		// 휴대폰회사[공코:HP_KIND]
        //     param.userHp = '01031214545'; // 휴대폰 번호
        // }else if(param.findKind == '2'){
        //     param.companyNm = '총관리자회사';	// 회사이름
        //     param.userName = '정준서';		// 담당자이름
        //     param.userHpkind = 'H1';		// 담당자휴대폰회사[공코:HP_KIND]
        //     param.userHp = '01031214545';	// 휴대폰 번호
        // }else{
        //     param.companyNm = '총관리자회사';	// 회사이름
        //     param.companyNo = '021234';		// 사업자번호
        //     param.companyCeoNm = '대표자';	// 대표자이름
        // }
        return $io.api(path + '/findId.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 패스워드 찾기(개인,기업) 인증번호발송
     */
    function requestPwFindAuth(param) {
        // var param = {};
        // param.findKind = '2';				// 1: 개인비밀번호찾기 2: 기업비밀번호찾기
        //
        // if(param.findKind == '1'){
        //     param.userId = 'admin';		// 아이디정보
        //     param.userName = '정준서';		// 회원이름
        //     param.userHpkind = 'H1';		// 휴대폰회사[공코:HP_KIND]
        //     param.userHp = '01031214545'; // 휴대폰 번호
        // }else{
        //     param.companyNm = '총관리자회사';	// 회사이름
        //     param.userId = 'admin';		// 아이디정보
        //     param.userName = '정준서';		// 회사이름
        //     param.userHpkind = 'H1';		// 휴대폰회사[공코:HP_KIND]
        //     param.userHp = '01031214545'; // 휴대폰 번호
        // }
        return $io.api(path + '/requestPwFindAuth.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 패스워드 찾기(개인,기업) 인증번호확인
     */
    function authNoCheck(param) {
        // var param = {};
        // param.authKey = 'abcdedf';			// 인증요청 후 결과값에 인증키값
        // param.authNo = '12345';				// 화면에서 입력한 인증번호
        return $io.api(path + '/authNoCheck.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 패스워드 변경처리
     */
    function updatePw(param) {
        // var param = {};
        // param.authKey = 'abcdedf';			// 인증요청 후 결과값에 인증키값
        // param.kind = '2';						// 1 : 개인 , 2 : 기업
        // param.userId = 'admin';				// 아이디
        // param.userPw = '12345';			// 변경 패스워드
        return $io.api(path + '/updatePw.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 개인 회원 요청
     */
    function insertMemberRequest(param) {
        // var memberBaseInfo = {};
        // var memberDetailInfo = {};
        // var memberCareerInfo = [];
        //
        // // 회원 기본
        // memberBaseInfo.memberId = 'memberId';					//[필수] 아이디
        // memberBaseInfo.memberPw = '12345';						//[필수] 패스워드
        // memberBaseInfo.memberNm = '홍길동';						//[필수] 이름
        // memberBaseInfo.memberGender = 'G1';						//[필수][공코 : GENDER] 성별
        // memberBaseInfo.memberKind = 'M1';						//[필수][공코 : MEMBER_KIND] 회원종류
        // memberBaseInfo.memberRrn = '8402251212121';				//[필수]주민번호 ('-'제거)
        // memberBaseInfo.memberHpKind = 'H1';						//[필수][공코 : HP_KIND] 휴대전화회사종류
        // memberBaseInfo.memberHp = '01012341234';				//[필수]휴대폰번호 ('-'제거)
        // memberBaseInfo.memberAddr = '경기도 부천시';				//[필수]기본 주소
        // memberBaseInfo.memberAddrDetail = '푸르지오 아파트';		//[필수]상세 주소
        // memberBaseInfo.memberAddrNo = '0321211';				//[필수]우편번호
        // memberBaseInfo.memberSubwayName = '송내역';				//[필수]가까운지하철
        // memberBaseInfo.memberSubwayTime = '05';					//[필수]소요시간
        // memberBaseInfo.height = '180';							//[필수]키
        // memberBaseInfo.weight = '65';							//[필수]몸무게
        // memberBaseInfo.email = 'test@naver.com';				//[필수]이메일
        // memberBaseInfo.photoAttachSeqs = '1|2|3|4';				//[필수]사진 첨부파일 일련번호
        //
        // memberBaseInfo.memberEntercompayInfo = '엔터기업명';	//[선택] memberKind 종류가 M2 일때 소속 회사명 [20180502수정]
        // memberBaseInfo.memberCompayInfo = '1';					//[필수] 기업일련번호 [20180502수정][개인,소속일때 메인회사(어플관리자)로 연결[코드 : 1]]
        // memberBaseInfo.accNo = '1234564545';					//[선택]계좌번호('-'제거)
        // memberBaseInfo.accBankName = '농협';						//[선택][공코 : BANK_NAME]계좌은행명
        // memberBaseInfo.accName = '예금주';						//[선택]계좌예금주
        // memberBaseInfo.companyNoAttachSeqs = '5';				//[선택]사업자등록증 및 증빙서류 첨부파일 일련번호
        // memberBaseInfo.videoAttachSeqs = '6';					//[선택]동영상 첨부파일 일련번호
        //
        // // 회원 상세
        // memberDetailInfo.supportField = 'S1';					//[필수][공코 : SUPPORT_FIELD] 지원분야
        // memberDetailInfo.hopeCast = 'H1';						//[필수][공코 : HOPE_CAST] 희망배역
        // memberDetailInfo.visitDtm = '2018-04-20 18:00:00';		//[필수] 방문가능일 [일시 패턴 : 0000-00-00 00:00:00]
        //
        // memberDetailInfo.job = 'J1';							//[선택][공코 : JOB] 직업
        // memberDetailInfo.hopeCastEtc = '희망배역기타';				//[선택]희망배역기타
        // memberDetailInfo.bodyInfo = 'B1';						//[선택][공코 : BODY_INFO] 신체사항
        // memberDetailInfo.classInfo = 'C1';						//[선택][공코 : CLASS_INFO] 보유방송등급
        // memberDetailInfo.hairState = 'H1';						//[선택][공코 : HAIR_STATE] 헤어상태
        // memberDetailInfo.hairHeight = 'H1';						//[선택][공코 : HAIR_HEIGHT] 헤어길이
        // memberDetailInfo.toothCorrec = 'T1';					//[선택][공코 : TOOTH_CORREC] 치아교정
        // memberDetailInfo.exposureInfo = 'E1';					//[선택][공코 : EXPOSURE_INFO] 노출가능여부
        // memberDetailInfo.tatooInfo = 'T1';						//[선택][공코 : TATOO_INFO] 문신
        // memberDetailInfo.pierInfo = 'P1';						//[선택][공코 : PIER_INFO] 피어싱
        // memberDetailInfo.beardInfo = 'B1';						//[선택][공코 : BEARD_INFO] 수염
        // memberDetailInfo.dimpleInfo = 'D1';						//[선택][공코 : DIMPLE_INFO] 보조개
        // memberDetailInfo.doubleEyeInfo = 'D1';					//[선택][공코 : DOUBLE_EYE_INFO] 눈쌍꺼플
        // memberDetailInfo.bottomsSize = 'B1';					//[선택][공코 : BOTTOMS_SIZE] 하의사이즈
        // memberDetailInfo.topSize = 'T1';						//[선택][공코 : TOP_SIZE] 상의사이즈
        // memberDetailInfo.footSize = '265';						//[선택]발사이즈 [숫자만]
        // memberDetailInfo.driveBicycleYn = 'D1';					//[선택][공코 : DRIVE_BICYCLE_YN] 자전거운전
        // memberDetailInfo.driveCarYn = 'D1';						//[선택][공코 : DRIVE_CAR_YN] 자동차운전
        // memberDetailInfo.driveMotercycleYn = 'D1';				//[선택][공코 : DRIVE_MOTERCYCLE_YN] 오토바이운전
        // memberDetailInfo.smokeYn = 'D1';						//[선택][공코 : SMOKE_YN] 흡연여부
        // memberDetailInfo.dressBaseCount = '2';					//[선택] 기본정장 벌수 [숫자만]
        // memberDetailInfo.dressBaseColor = '검정';				//[선택] 기본정장 색상
        // memberDetailInfo.dressSemiCount = '2';					//[선택] 세미정장 벌수 [숫자만]
        // memberDetailInfo.dressSemiColor = '검정';				//[선택] 세미정장 색상
        // memberDetailInfo.dressBlackShoes = 'Y';					//[선택] 검정구두
        // memberDetailInfo.dressSwimsuit = 'Y';					//[선택] 수영복
        // memberDetailInfo.dressSchoolUniform = 'Y';				//[선택] 교복
        // memberDetailInfo.dressMilitaryBoots = 'Y';				//[선택] 군화
        //
        // // 회원 경력
        // var careerInfo = {};
        // careerInfo.field = 'F1';								//[필수][공코 : FIELD] 경력 분류
        // careerInfo.pgmName = '무한도전';							//[필수]프로그램 명
        // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
        // careerInfo.year = '2018';								//[필수]년도
        // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
        // careerInfo.linesYn = 'Y';								//[필수]대사
        // memberCareerInfo.push(careerInfo);
        //
        // var careerInfo = {};
        // careerInfo.field = 'F2';								//[필수][공코 : FIELD] 경력 분류
        // careerInfo.pgmName = '1박2일';							//[필수]프로그램 명
        // careerInfo.broadcastingStation = 'B1';					//[필수][공코 : BROADCASTING_STATION] 방송국
        // careerInfo.year = '2018';								//[필수]년도
        // careerInfo.part = 'P1';									//[필수][공코 : PART] 배역
        // careerInfo.linesYn = 'N';								//[필수]대사
        // memberCareerInfo.push(careerInfo);
        //
        //
        // var param = {};
        // param.memberBaseInfo = memberBaseInfo;				// 회원 기본 정보
        // param.memberDetailInfo = memberDetailInfo;			// 회원 상세 정보
        // param.memberCareerInfo = memberCareerInfo;			// 회원 경력 정보
        return $io.api(path + '/insertMemberRequest.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업 리스트 조회
     */
    function selectCompanyList() {
        var param = {};
        return $io.api(path + '/selectCompanyList.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 아이디 중복 조회
     */
    function selectIdCheck(param) {
        // var param = {};
        // param.memberId = 'admin1';
        return $io.api(path + '/selectIdCheck.login', param)
            .then(function(data){
                // if(data.user.resultData.IDCHEK == 'Y'){
                //     alert('중복');
                // }else{
                //     alert('등록가능');
                // }
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 주민번호 중복 조회
     */
    function selectRrnCheck(param) {
        // var param = {};
        // param.memberRrn = '8402251212121';
        return $io.api(path + '/selectRrnCheck.login', param)
            .then(function(data){
                // if(data.user.resultData.RRNCHEK == 'Y'){
                //     alert('중복');
                // }else{
                //     alert('등록가능');
                // }
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 샘플동영상정보 조회
     */
    function selectSampleVideo() {
        var param = {};
        return $io.api(path + '/selectSampleVideo.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 약관정보 조회
     */
    function selectTermInfo() {
        var param = {};
        return $io.api(path + '/selectTermInfo.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 기업회원등록
     */
    function insertCompanyInfo(param) {
        // var param = {};
        // param.companyNm = '(주)테스트';							// [필수] 회사명
        // param.companyCeoNm = '홍순이';							// [필수] 대표자명
        // param.companyCeoHpKind = 'H1';							// [필수][공코 :HP_KIND] 대표자 휴대폰 회사
        // param.companyCeoHp = '01012341234';						// [필수][- 제거] 대표자 휴대폰 번호
        // param.companyNo = '123456';								// [필수][- 제거] 회사 사업자번호
        // param.companyMemberName = '담당자';						// [필수] 담당자명
        // param.companyTel1 = '0212341234';						// [필수] 연락처1
        // param.companyTel2 = '0212341234';						// [선택] 연락처2
        // param.companyEmail = 'admin@email.com';					// [필수] 회사(담당자) 이메일 주소
        // param.companyNoAttachSeq = '1';							// [선택] 사업자등록증 첨부일련번호
        // param.companyAccAttachSeq = '1';						// [선택] 통장사본 첨부일련번호
        // param.companyPermissionAttachSeq = '1';					// [선택] 대중문화예술기획업 허가증 첨부일련번호
        // param.companyJobProveAttachSeq = '1';					// [선택] 직업소개소 증빙자료 첨부일련번호
        // param.companyEtcAttachSeq = '1';						// [선택] 기타 증빙서류 첨부일련번호
        // param.accNo = '30212512252';							// [선택][- 제거]계좌번호
        // param.accName = '예금주';									// [선택]예금주
        // param.accBankName = 'H1';								// [선택][공코 : BANK_NAME] 은행명
        // param.companyMemberId = 'mine4001';						// [필수]아이디
        // param.companyMemberPwd = '1234';						// [필수]패스워드
        return $io.api(path + '/insertCompanyInfo.login', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }

    /**
     * 정보동의 입력
     */
    function insertAgree(param) {
        // var param = {};
        // param.busnLinkKey = '1';			// 회원,기업 일련번호
        // param.agreeKind = 'A1';				// 정보 동의 종류[공코:AGREE_KIND]
        // param.agreeYn = 'Y';				// 동의 여부
        return $io.api(path + '/insertAgree.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 정보동의 조회
     */
    function selectAgreeInfo(param) {
        return $io.api(path + '/mem/selectAgreeInfo.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    /**
     * 쿠폰 리스트
     */
    function selectCouponList(param) {
        return $io.api(path + '/mem/selectCouponList.api', param)
            .then(function(data){
                return data;
            })
            .catch(function(err){
                return $q.reject(err);
            })
    }
    
    
    
}
MemberSvc.$inject = ["$q", "$timeout", "$io", "$message"];
/* //api/member/MemberSvc.js */
}()
!function () { 'use strict';
/* api/member/member.constant.js */

angular
  .module('member.constant', [])
  .constant('memberConstant', _.deepFreeze({

      CP_USE_USER             : 'CP_USE_USER',              // 쿠폰사용자
      CP_BUY_KIND             : 'CP_BUY_KIND',              // 쿠폰구매종류
      CP_BUY_PAY_KIND         : 'CP_BUY_PAY_KIND',          // 결제방식
      CP_USE_STATE            : 'CP_USE_STATE',             // 쿠폰사용상태
      MESSAGE_TYPE            : 'MESSAGE_TYPE',             // 푸쉬종류타입
      STATE_CODE              : 'STATE_CODE',               // 푸쉬발송상태
      MSG_TYPE                : 'MSG_TYPE',                 // 푸쉬메시지타입
      RESULT_CODE             : 'RESULT_CODE',              // 푸쉬결과코드
      AUTH_INFO_KIND          : 'AUTH_INFO_KIND',           // 권한종류
      HP_KIND                 : 'HP_KIND',                  // 휴대전화회사종류
      COMPANY_STATE           : 'COMPANY_STATE',            // 기업회원상태
      PUSH_TYPE               : 'PUSH_TYPE',                // PSIDORCUID
      QNA_KIND                : 'QNA_KIND',                 // QNA종류
      REQUEST_KIND            : 'REQUEST_KIND',             // 요청종류
      REQUEST_STATE           : 'REQUEST_STATE',            // 요청상태
      CANCEL_KIND             : 'CANCEL_KIND',              // 취소종류
      ATTEND_NO_STATE         : 'ATTEND_NO_STATE',          // 미출석상태
      ATTEND_NO_LEAVE_STATE   : 'ATTEND_NO_LEAVE_STATE',    // 출석,무단이탈상태
      OPEN_YN                 : 'OPEN_YN',                  // 푸쉬오픈여부
      RCUT_BASE_STATE         : 'RCUT_BASE_STATE',          // 모집상태값
      HIS_KIND                : 'HIS_KIND',                 // 이력종류
      PGM_REUT_FIELD          : 'PGM_REUT_FIELD',           // 지원분야
      MEMBER_KIND             : 'MEMBER_KIND',              // 회원종류
      JOB                     : 'JOB',                      // 직업
      SUPPORT_FIELD           : 'SUPPORT_FIELD',            // 지원분야
      HOPE_CAST               : 'HOPE_CAST',                // 희방배역
      BODY_INFO               : 'BODY_INFO',                // 신체사항
      HAIR_STATE              : 'HAIR_STATE',               // 헤어상태
      HAIR_HEIGHT             : 'HAIR_HEIGHT',              // 헤어길이
      TOOTH_CORREC            : 'TOOTH_CORREC',             // 치아교정
      EXPOSURE_INFO           : 'EXPOSURE_INFO',            // 노출가능여부
      TATOO_INFO              : 'TATOO_INFO',               // 문신
      PIER_INFO               : 'PIER_INFO',                // 피어싱
      BEARD_INFO              : 'BEARD_INFO',               // 수염
      DIMPLE_INFO             : 'DIMPLE_INFO',              // 보조개
      DOUBLE_EYE_INFO         : 'DOUBLE_EYE_INFO',          // 눈쌍꺼플
      BOTTOMS_SIZE            : 'BOTTOMS_SIZE',             // 하의사이즈
      TOP_SIZE                : 'TOP_SIZE',                 // 상의사이즈
      DRIVE_BICYCLE_YN        : 'DRIVE_BICYCLE_YN',         // 자전거운전
      DRIVE_MOTERCYCLE_YN     : 'DRIVE_MOTERCYCLE_YN',      // 오토바이운전
      SMOKE_YN                : 'SMOKE_YN',                 // 흡연여부
      FIELD                   : 'FIELD',                    // 경력 분류
      BROADCASTING_STATION    : 'BROADCASTING_STATION',     // 방송국
      PART                    : 'PART',                     // 배역
      PENALTY_CODE            : 'PENALTY_CODE',             // 패널티종류코드
      PENALTY_KIND            : 'PENALTY_KIND',             // 패널티종류
      AGREE_KIND              : 'AGREE_KIND',               // 정보동의종류
      SAMPLE_VIDEO            : 'SAMPLE_VIDEO',             // 샘플영상정보
      MAP_INFO_URL            : 'MAP_INFO_URL',             // 노선 검색
      PHOTO_INFO_URL          : 'PHOTO_INFO_URL',           // 사진 및 영상 용량 축소 방법 가이드 주소
      AGREE_INFO              : 'AGREE_INFO',               // 약관
      DRESS_INFO              : 'DRESS_INFO',               // 보유 의상
      GENDER                  : 'GENDER',                   // 성별
      BANK_NAME               : 'BANK_NAME',                // 은행명
      CLASS_INFO              : 'CLASS_INFO',               // 보유방송등급
      PUSH_ADMIN_NO           : 'PUSH_ADMIN_NO',            // [푸쉬발송용도]총관리자일련번호
      CHARGE_KIND             : 'CHARGE_KIND',              // 담당자종류
      MY_RCUT_SEL             : 'MY_RCUT_SEL'               // 내게맞춪정보조회조건

  }))
/* //api/member/member.constant.js */
}()
!function () { 'use strict';
/* api/smpl/SmplSFSpCnslSvc.js */
'use strict';

api.factory('SmplSFSpCnslSvc', SmplSFSpCnslSvc);

function SmplSFSpCnslSvc($q, $timeout, $io, $message, spConstant) {

  var path = '/smpl/sp/SFSpCnslSvc';

  return {
    selListCnsl: selListCnsl,
    insCnsl : insCnsl,
    delCnsl : delCnsl,
    updCnsl : updCnsl,
    selListCnslInfo : selListCnslInfo,
    selListAdmr : selListAdmr,
    insAdmr : insAdmr,
    selListAdmrInfo : selListAdmrInfo
  };

  /**
   * 목록 조회
   */
  function selListCnsl(param, header) {

    var config = {
      header: header
    };

    return $io.api(path +  '/selListCnsl', param, config)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try {
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        } catch (e) {}
        return body;
      });
  }

  /**
   * 등록
   */
  function insCnsl(param) {
    return $io.api(path + '/insCnsl', param)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try {
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        } catch (e) {}
        return body;
      });
  }

  /**
   * 삭제
   */
  function delCnsl(param) {
    return $io.api(path +  '/delCnsl', param)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      });
  }

  /**
   * 수정
   */
  function updCnsl(param) {
    return $io.api(path +  '/updCnsl',param)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert("서비스 오류 입니다.");
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      });
  }

  /**
   * 상세 정보 조회
   */
  function selListCnslInfo(param) {
    return $io.api(path + '/selListCnslInfo',param)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      });
  }

  /**
   * 칭찬하기 조회
   */
  function selListAdmr(param, header) {

    var config = {
      header: header
    };

    return $io.api(path +  '/selListAdmr', param, config)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      });
  }

  /**
   * 칭찬하기 등록
   */
  function insAdmr(param, header) {

    var config = {
      header: header
    };

    return $io.api(path +  '/insAdmr', param, config)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return $q.resolve(body);
      });
  }

  /**
   * 칭찬하기 조회
   */
  function selListAdmrInfo(param, header) {

    var config = {
      header: header
    };

    return $io.api(path +  '/selListAdmrInfo', param, config)
      .then(function(response) {
        var body = response.serviceBody.data.BackEndBody[0];
        try{
          if(!body.user || body.srvcTrtRsltTypCd == spConstant.COMMON_ERROR){
            $message.alert('서비스 오류 입니다.');
            return $q.reject(JSON.stringify(response));
          }
        }catch(e){}
        return body;
      })
  }

}
SmplSFSpCnslSvc.$inject = ["$q", "$timeout", "$io", "$message", "spConstant"];
/* //api/smpl/SmplSFSpCnslSvc.js */
}()
!function () { 'use strict';
/* api/smpl/SmplSvc.js */

api.factory('SmplSvc', SmplSvc);

function SmplSvc($log, $q, $io) {
  var logger = $log('SmplSvc');

  return {
    selReprPrdtList: selReprPrdtList
  };

  // SFQuEntplPrdtLstSvc.selReprPrdtList
  function selReprPrdtList(data) {
    // var param = {
    //   stndYmd: '20150127',
    //   entplSystmChnlScCd: '01',
    //   entplChnlScCd: '0201',
    //   cnsltNo: '15573426'
    // };
    return $io.user('/qu/SFQuEntplPrdtLstSvc/selReprPrdtList', data);
  }
}
SmplSvc.$inject = ["$log", "$q", "$io"];
/* //api/smpl/SmplSvc.js */
}()
!function () { 'use strict';
/* common/components/editor/editor.js */
/* global EditorJSLoader, Editor, Trex */

angular
  .module('easi.common')
  .directive('editor', editor);

function editor($document, $timeout) {
  return {
    templateUrl: 'components/editor/editor.tpl.html',
    restrict: 'EA',
    require: 'ngModel',
    scope: {
      submit: '&onSubmit',
      triggers: '@submitTriggers',
      initHeight: '=initHeight'
    },
    link: {
      pre: function preLink() {
      },
      post: function postLink(scope, element, attrs, ngModel) {
        var id = _.uniqueId();
        var config = {
          txHost: '', /* 런타임 시 리소스들을 로딩할 때 필요한 부분으로, 경로가 변경되면 이 부분 수정이 필요. ex) http://xxx.xxx.com */
          txPath: '', /* 런타임 시 리소스들을 로딩할 때 필요한 부분으로, 경로가 변경되면 이 부분 수정이 필요. ex) /xxx/xxx/ */
          txService: 'sample', /* 수정필요없음. */
          txProject: 'sample', /* 수정필요없음. 프로젝트가 여러개일 경우만 수정한다. */
          initializedId: '', /* 대부분의 경우에 빈문자열 */
          wrapper: 'tx_trex_container' + '', /* 에디터를 둘러싸고 있는 레이어 이름(에디터 컨테이너) */
          form: 'tx_editor_form' + '', /* 등록하기 위한 Form 이름 */
          txIconPath: '../daumeditor/images/icon/editor/', /*에디터에 사용되는 이미지 디렉터리, 필요에 따라 수정한다. */
          txDecoPath: '../daumeditor/images/deco/contents/', /*본문에 사용되는 이미지 디렉터리, 서비스에서 사용할 때는 완성된 컨텐츠로 배포되기 위해 절대경로로 수정한다. */
          canvas: {
            initHeight: scope.initHeight || 800,
            styles: {
              color: '#123456', /* 기본 글자색 */
              fontFamily: '굴림', /* 기본 글자체 */
              fontSize: '10pt', /* 기본 글자크기 */
              backgroundColor: '#fff', /*기본 배경색 */
              lineHeight: '1.5', /*기본 줄간격 */
              padding: '8px' /* 위지윅 영역의 여백 */
            },
            showGuideArea: false
          },
          events: {
            preventUnload: false
          },
          sidebar: {
            attachbox: {
              show: true
            }
          },
          size: {
            // contentWidth: '100%' /* 지정된 본문영역의 넓이가 있을 경우에 설정 */
          },
          save: {
            onSave: angular.noop
          }
        };

        scope.editorConfig = config;

        EditorJSLoader.ready(function(Editor) {
          var editor = new Editor(config);
          observeJobs(editor);
          initTriggers();

          $timeout(function() {
            var meta = $document.find('meta[name="viewport"]');
            var iframe = $document.find('#' + config.wrapper + ' iframe');
            var contentDocument = angular.element(iframe.prop('contentDocument'));
            var title = contentDocument.find('head title');
            
            iframe.css("overflow", "hidden");
            title.after(meta.clone());
          }, 100);

          // Editor.switchEditor(id);

          ngModel.$render = function() {
            Editor.modify({
              content: ngModel.$viewValue || ''
            });
          };

          scope.$watch('initHeight', function(newVal, oldVal) {
            Editor.canvas.setCanvasSize({height: newVal});
          });
        });

        function observeJobs(editor) {
          var handler = _.debounce(modelChange, 0);
          _.chain(Trex.Ev)
            .omit('__CANVAS_PANEL_MOUSEMOVE', '__CANVAS_PANEL_MOUSEOVER', '__ON_SUBMIT')
            .each(function (ev) {
              Editor.canvas.observeJob(ev, handler);
            }).value();

          Editor.canvas.editor.observeJob(Trex.Ev.__ON_SUBMIT, function (editor) {
            scope.submit({editor: editor});
          });
        }

        function modelChange() {
          var newValue = Editor.getContent();
          if (ngModel.$viewValue === newValue) {
            return;
          }
          ngModel.$setViewValue(newValue);
        }

        function initTriggers() {
          var triggerNames = [];
          if(!_.isEmpty(scope.triggers)){
            triggerNames = scope.triggers.split(',');
          }
          var triggerSelectors;
          var triggerEls;

          triggerSelectors = _.map(triggerNames, function (name) {
            return '[editor-submit-trigger="' + name + '"]';
          });
          triggerEls = $document.find(triggerSelectors.join(','));
          triggerEls.on('click', function (e) {
            Editor.save();
          });
        }
      }
    }
  };
}
editor.$inject = ["$document", "$timeout"];/* //common/components/editor/editor.js */
}()
!function () { 'use strict';
/* common/layout/menu/menu.js */
angular
  .module('easi.common')
  .directive('leftMenu', ["$io", "$user", "$q", "$log", "$modal", "$message", "$env", "$window", "$location", "$rootScope", "$storage", "doryConst", "$bdExtension", "$state", "MemberSvc", "$timeout", function($io, $user, $q, $log, $modal, $message
         , $env, $window, $location, $rootScope, $storage, doryConst, $bdExtension, $state, MemberSvc, $timeout) {
    return {
      replace : true,
      restrict : 'E',
      templateUrl: 'layout/menu/menu.tpl.html',
      link: function(scope, iel, attr, ctl) {

          scope.loginBtn = function(){
              if(_.isEmpty($user.currentUser)){
                  $rootScope.goView('public.html#/login');
              }else{
                  if($message.confirm('로그아웃 하시겠습니까?')){

                      MemberSvc.logOut()
                          .then(function(data){
                              $message.alert('로그아웃 되었습니다.');
                              $storage.remove('AUTO_LOGIN');
                              $user.destroy();
                              $rootScope.goView('main.html#/main');
                          })
                          .catch(function(err){
                              $message.alert(err);
                              $storage.remove('AUTO_LOGIN');
                              $user.destroy();
                              $rootScope.goView('main.html#/main');
                          });
                  }
              }
          };

          scope.checkReload = function(e, $event) {
              var tu = (e || '').replace(/^[^#]*#/, '');
              if($location.path() == tu) {
                  $state.reload(); }
          };

          scope.toggle = function($event) {
              if(_.isEmpty($user.currentUser)){
                  $message.alert('로그인 후 이용 가능합니다.');
                  $rootScope.goView('public.html#/login');
                  return;
              }
              $($event.target).parent().parent().children("dd").slideToggle();
          };

          // User/Env 객체 있는 경우에만... :)
          $q.all([$user.get(), $env.get()]).then(function() {
              scope.loginKind = $user.currentUser.getLoginKind();
              $storage.remove('##menuRaw##');
              $storage.get('##menuRaw##').then(function(data) {
                  if(_.isEmpty(data)) {
                      var menuData = $user.currentUser.getMenuInfoList();
                      $storage.set('##menuRaw##', menuData);
                      buildMenu(menuData);
                  } else {
                      buildMenu(data);
                  }
              })
                  .then(function() {
                      selectMenu($state.current);
                  });
          }).catch(function(err){
              MemberSvc.selectMenuList()
                  .then(function(data){
                      buildMenu(data.user.resultData.menuList);
                  })
                  .catch(function(error){

                  });
          });

          function buildMenu(result) {
                  //var loginKind = $user.currentUser.getUserDataInfo().loginKind;
                  // if(loginKind === 'L1'){
                  //     tempMenu = [
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/resume/prList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"1","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":442,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C0","COMMON_INFO_VALUE2":"신청(요청)현황","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/schedule/psMng","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"2","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":443,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C1","COMMON_INFO_VALUE2":"일정관리","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/resume/prList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"3","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":444,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C2","COMMON_INFO_VALUE2":"촬영정보관리","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/notify/pnList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"4","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":445,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C3","COMMON_INFO_VALUE2":"알림확인","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/coupon/pcShop","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"5","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":446,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C4","COMMON_INFO_VALUE2":"쿠폰구매","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/coupon/pcList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"6","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":447,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C5","COMMON_INFO_VALUE2":"쿠폰현황","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/schedule/psPnlty","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"7","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":448,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C6","COMMON_INFO_VALUE2":"패널티현황","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/mypage/pmMyPage","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"8","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":449,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C7","COMMON_INFO_VALUE2":"정보수정","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/notify/pnConfig","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"9","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":450,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C8","COMMON_INFO_VALUE2":"알림설정","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"pesn.html#/inquiry/piList","COMMON_INFO_CODE":"MENU_LIST_MEMBER","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"10","CREATE_DTM":1525595146000,"COMMON_INFO_SEQ":451,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_출연자","COMMON_INFO_VALUE1":"C9","COMMON_INFO_VALUE2":"1:1문의하기","TRT_DTM":1525595146000,"COMMON_INFO_VALUE3":"L0"}
                  //     ];
                  // }else{
                  //     tempMenu = [
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"1","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":390,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0","COMMON_INFO_VALUE2":"프로그램","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/progmc/cpProgmReg","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"2","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":391,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0_M1_M2","COMMON_INFO_VALUE2":"프로그램등록","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L2"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/progmc/cpProgmList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"3","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":392,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0_M1_M1","COMMON_INFO_VALUE2":"프로그램관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L2"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/progmc/cpRcrutReg","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"4","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":393,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0_M2_M2","COMMON_INFO_VALUE2":"출연자모집정보등록","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L2"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/progmc/cpRcrutList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"5","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":394,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M0_M2_M1","COMMON_INFO_VALUE2":"출연자모집정보관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L2"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"6","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":395,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1","COMMON_INFO_VALUE2":"출연자","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/mngpesn/cmSignupList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"7","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":396,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1_M2","COMMON_INFO_VALUE2":"회원가입요청승인","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/mngpesn/cmMngList?m=1","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"8","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":397,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1_M3","COMMON_INFO_VALUE2":"관심출연자","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/mngpesn/cmMngList?m=2","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"9","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":398,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1_M4","COMMON_INFO_VALUE2":"출연요청중","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/mngpesn/cmMngList?m=3","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"10","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":399,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M1_M5","COMMON_INFO_VALUE2":"출연완료","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"11","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":408,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_WEB","COMMON_INFO_VALUE1":"M5","COMMON_INFO_VALUE2":"쿠폰관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/couponc/ccCopnShop","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"12","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":409,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_WEB","COMMON_INFO_VALUE1":"M5_M1","COMMON_INFO_VALUE2":"쿠폰구매","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/couponc/ccCopnList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"13","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":410,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_WEB","COMMON_INFO_VALUE1":"M5_M2","COMMON_INFO_VALUE2":"쿠폰현황","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"14","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":400,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M2","COMMON_INFO_VALUE2":"권한","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/authc/caNewReg","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"15","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":401,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M2_M2","COMMON_INFO_VALUE2":"신규등록","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/authc/caMenuMng","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"16","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":402,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M2_M1","COMMON_INFO_VALUE2":"권한별메뉴관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/authc/caPesnMng","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"17","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":403,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M2_M2","COMMON_INFO_VALUE2":"권한별사용자관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"18","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":404,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M3","COMMON_INFO_VALUE2":"정산","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/paymt/cpPaymtList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"19","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":405,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M3_M1","COMMON_INFO_VALUE2":"정산관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"6","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"20","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":406,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M4","COMMON_INFO_VALUE2":"1:1문의답변관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L0"},
                  //         {"COMMON_INFO_VALUE10":"10","COMMON_INFO_VALUE7":"7","COMMON_INFO_VALUE6":"comp.html#/qna/cqQnaList","COMMON_INFO_CODE":"MENU_LIST_COM_APP","COMMON_INFO_VALUE5":"5","COMMON_INFO_VALUE4":"21","CREATE_DTM":1525595145000,"COMMON_INFO_SEQ":407,"COMMON_INFO_VALUE9":"9","COMMON_INFO_VALUE8":"8","CREATE_NO":"99999999","TRT_NO":"99999999","COMMON_INFO_NAME":"메뉴리스트_기업_APP","COMMON_INFO_VALUE1":"M4_M1","COMMON_INFO_VALUE2":"1:1문의답변관리","TRT_DTM":1525595145000,"COMMON_INFO_VALUE3":"L1"}
                  //     ];
                  // }
              $rootScope.menu = [];
              result = _.each(result, function(val, key, obj) {
                      var idx = val.COMMON_INFO_VALUE1.split('_');
                      if(idx.length === 1){
                          val.idx = val.COMMON_INFO_VALUE1;
                          val.parentIdx = null;
                      }else{
                          val.idx = val.COMMON_INFO_VALUE1;
                          val.parentIdx = idx[0];
                      }
                  })
                  //트리 변환 메서드
                  var treeModel = function (result, rootId) {
                      var rootNodes = [];
                      var traverse = function (nodes, item, index) {
                          if (nodes instanceof Array) {
                              return nodes.some(function (node) {
                                  var idx = node.idx.split('_');
                                  if (idx[0] === item.parentIdx) {
                                      node.menu = node.menu || [];
                                      return node.menu.push(result.splice(index, 1)[0]);
                                  }

                                  return traverse(node.menu, item, index);
                              });
                          }
                      };

                      while (result.length > 0) {
                          result.some(function (item, index) {
                              if (item.parentIdx === rootId) {
                                  return rootNodes.push(result.splice(index, 1)[0]);
                              }

                              return traverse(rootNodes, item, index);
                          });
                      }

                      return rootNodes;
                  };
                  var menuJson = treeModel(result, null);

                  $rootScope.menu = menuJson; // result.serviceBody.data.menu;

                  $rootScope.menuRaw = md($rootScope.menu);
              // 한번 더~ 검증
              // if($rootScope.menuTitle == '') { $rootScope.setMenuTitle(); }
          }

      }
    }
  }])

;
function clearAll(m, n) {
  var un = n || 0;
  if(un > 3) { return; }
  if(un == 0) {  }
  _.forEach(m, function(a) {
    a._show = false;
    if(a.menu) { clearAll(a.menu, un+1); }
  })
}

function selectMenu(l, m, n) {
  var un = n || 0;
  var show = false;
  
  _.forEach(un > 0 ? m.menu :  m, function(a) {
    if(_.has(a, 'uri') && _.endsWith(a.uri, l)) { return !(a._show = show = true); }
    if((show = selectMenu(l, a, un + 1))) { return false; }
  });
  return ((un && (m._show = show)), show);
}

function selectMenuById(l, m, n) {
  var un = n || 0;
  var show = false;
  
  _.forEach(un > 0 ? m.menu :  m, function(a) {
    if(_.has(a, 'id') && a.id == l) { return !(a._show = show = true); }
    if((show = selectMenuById(l, a, un + 1))) { return false; }
  });
  return ((un && (m._show = show)), show);
}



function md(v, d) {
  if(d > 10) { return []; }
  var rs = [];
  _.forEach(v, function(w) {
    if(_.has(w, 'uri')) { rs.push({id : w.id, name : w.name, uri : w.uri }); }
    if(_.has(w, 'menu') && w.menu.length) { rs = rs.concat(md(w.menu, (d || 0) + 1)); }
  });
  return rs;
}/* //common/layout/menu/menu.js */
}()
!function () { 'use strict';
/* common/shared/menu/menuCtrl.js */
angular
  .module('easi.common.shared')
  .controller('menuCtrl', menuCtrl)
;

function menuCtrl($log, $scope, $storage, $rootScope, $message, $modalInstance) {
  var logger = $log(this);
  var vm = this;

  _.assign(vm, {
  });
  
  _.assign($scope, {
    filterGroupA : function(m) { return _.contains(['cu'], m.id); },
    filterGroupB : function(m) { return _.contains(['ac', 'sc'], m.id); },
    filterGroupC : function(m) { return _.contains(['fp','ot'], m.id); },
    filterGroupD : function(m) { return _.contains(['qu', 'pl', 'qu-aircraft'], m.id); },
    filterGroupE : function(m) { return _.contains(['sp', 'sa'], m.id); },
    filterExceptMe : function(m) { return  m.id != "tsot2104"; }
  });
  
  $scope.closeMainMenu = function() { $modalInstance.close(); }
  
  init();

  
  
  function init() {
    logger.debug('init', vm);
        
    $scope.$on('$destroy', destroy);
    
    // hardcoded - lane 에 따라 메뉴 노출이 달라요~    
  }

  function destroy(event) {
    logger.debug(event);
  }  
}
menuCtrl.$inject = ["$log", "$scope", "$storage", "$rootScope", "$message", "$modalInstance"];
/* //common/shared/menu/menuCtrl.js */
}()
!function () { 'use strict';
/* common/shared/popup/iaGeoP/iaGeoPCtrl.js */

angular
.module('easi.common.shared')
.controller('iaGeoPCtrl', iaGeoPCtrl);

function iaGeoPCtrl($log, $rootScope , $storage, $scope, $modal, $modalInstance, $message, item, GuestSvc,MemberSvc, ngspublic, comConstant , $window , $q) {
  /**
  * Private variables
  */
  var logger = $log(this);
  var vm = this;
  let _fn = {
    set : function(agreeYn){
        let params = {};
        params.busnLinkKey = vm.meta.busnLinkKey + '';
        params.agreeKind = vm.meta.AGREE_KIND;
        params.agreeYn = agreeYn;

        return MemberSvc.insertAgree(params)
        .then(function(d){
            let deferred = $q.defer();
            if(d.user.result === 'SUCCESS'){
                deferred.resolve(d);
            }
            else{ deferred.reject(d); }
            return deferred.promise;
        })
        .catch(function(d){
            let deferred = $q.defer();
            deferred.reject(d);
            return deferred.promise;
        });
    }
  };

  /**
   * Public variables
   */
  _.assign(vm, {
    meta : {
        AGREE_KIND : 'A2',
        busnLinkKey : -1
    }
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    dismiss : dismiss,
    confirm : confirm,
    agree : function(){
        if($window.confirm('동의하시겠습니까')){
            _fn.set('Y')
            .then(function(){
                $storage.set('IA_GEO_AGREE_KIND' , 'Y');
                $modalInstance.dismiss('Y');
            });
        }
    },
    disagree : function(){
        if($window.confirm('미동의시 서비스 이용이 불가합니다. 미동의 하시겠습니까')){
            _fn.set('N')
            .then(function(){
                $storage.set('IA_GEO_AGREE_KIND' , 'N');
                $modalInstance.dismiss('N');
            });
        }
    }
  });

  /**
  * Initialize
  */
  init();

  function init() {
    logger.debug('init', vm);

    console.log(item);
    
    if(!item[0].busnLinkKey){
        $modalInstance.close();
    }
    else{
        vm.meta.busnLinkKey = item[0].busnLinkKey;
    }

    $scope.$on('$destroy', destroy);
  }

  /**
  * Event handlers
  */
  function destroy(event) {
    logger.debug(event);
  }

  /**
  * Custom functions
  */

  /**
   * 모달 닫기
   */
  function dismiss() {
    $modalInstance.dismiss(vm);
  }


  /**
   * 정상 종료
   */
  function confirm(){
    $modalInstance.close(vm);
  }


}
iaGeoPCtrl.$inject = ["$log", "$rootScope", "$storage", "$scope", "$modal", "$modalInstance", "$message", "item", "GuestSvc", "MemberSvc", "ngspublic", "comConstant", "$window", "$q"];
/* //common/shared/popup/iaGeoP/iaGeoPCtrl.js */
}()
!function () { 'use strict';
/* common/shared/popup/iaPersonInfP/iaPersonInfPCtrl.js */

angular
.module('easi.common.shared')
.controller('iaPersonInfPCtrl', iaPersonInfPCtrl);

function iaPersonInfPCtrl($log, $rootScope , $storage, $scope, $modal, $modalInstance, $message, item, GuestSvc,MemberSvc, ngspublic, comConstant , $window , $q) {
  /**
  * Private variables
  */
  var logger = $log(this);
  var vm = this;
  let _fn = {
    set : function(agreeYn){
        let params = {};
        params.busnLinkKey = vm.meta.busnLinkKey + '';
        params.agreeKind = vm.meta.AGREE_KIND;
        params.agreeYn = agreeYn;

        return MemberSvc.insertAgree(params)
        .then(function(d){
            let deferred = $q.defer();
            if(d.user.result === 'SUCCESS'){
                deferred.resolve(d);
            }
            else{ deferred.reject(d); }
            return deferred.promise;
        })
        .catch(function(d){
            let deferred = $q.defer();
            deferred.reject(d);
            return deferred.promise;
        });
    }
  };

  /**
   * Public variables
   */
  _.assign(vm, {
    meta : {
        AGREE_KIND : 'A4',
        busnLinkKey : -1
    }
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    dismiss : dismiss,
    confirm : confirm,
    agree : function(){
        if($window.confirm('동의하시겠습니까')){
            _fn.set('Y')
            .then(function(){
                $storage.set('IA_PERSON_INF_AGREE_KIND' , 'Y');
                $modalInstance.dismiss('Y');
            });
        }
    },
    disagree : function(){
        if($window.confirm('미동의시 서비스 이용이 불가합니다. 미동의 하시겠습니까')){
            _fn.set('N')
            .then(function(){
                $storage.set('IA_PERSON_INF_AGREE_KIND' , 'N');
                $modalInstance.dismiss('N');
            });
        }
    }
  });

  /**
  * Initialize
  */
  init();

  function init() {
    logger.debug('init', vm);

    console.log(item);
    
    if(!item[0].busnLinkKey){
        $modalInstance.close();
    }
    else{
        vm.meta.busnLinkKey = item[0].busnLinkKey;
    }

    $scope.$on('$destroy', destroy);
  }

  /**
  * Event handlers
  */
  function destroy(event) {
    logger.debug(event);
  }

  /**
  * Custom functions
  */

  /**
   * 모달 닫기
   */
  function dismiss() {
    $modalInstance.dismiss(vm);
  }


  /**
   * 정상 종료
   */
  function confirm(){
    $modalInstance.close(vm);
  }


}
iaPersonInfPCtrl.$inject = ["$log", "$rootScope", "$storage", "$scope", "$modal", "$modalInstance", "$message", "item", "GuestSvc", "MemberSvc", "ngspublic", "comConstant", "$window", "$q"];
/* //common/shared/popup/iaPersonInfP/iaPersonInfPCtrl.js */
}()
!function () { 'use strict';
/* common/shared/popup/iaPrivacyP/iaPrivacyPCtrl.js */

angular
.module('easi.common.shared')
.controller('iaPrivacyPCtrl', iaPrivacyPCtrl);

function iaPrivacyPCtrl($log, $rootScope , $storage, $scope, $modal, $modalInstance, $message, item, GuestSvc,MemberSvc, ngspublic, comConstant , $window , $q) {
  /**
  * Private variables
  */
  var logger = $log(this);
  var vm = this;
  let _fn = {
    set : function(agreeYn){
        let params = {};
        params.busnLinkKey = vm.meta.busnLinkKey + '';
        params.agreeKind = vm.meta.AGREE_KIND;
        params.agreeYn = agreeYn;

        return MemberSvc.insertAgree(params)
        .then(function(d){
            let deferred = $q.defer();
            if(d.user.result === 'SUCCESS'){
                deferred.resolve(d);
            }
            else{ deferred.reject(d); }
            return deferred.promise;
        })
        .catch(function(d){
            let deferred = $q.defer();
            deferred.reject(d);
            return deferred.promise;
        });
    }
  };

  /**
   * Public variables
   */
  _.assign(vm, {
    meta : {
        AGREE_KIND : 'A1',
        busnLinkKey : -1
    }
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    dismiss : dismiss,
    confirm : confirm,
    agree : function(){
        if($window.confirm('동의하시겠습니까')){
            _fn.set('Y')
            .then(function(){
                $storage.set('IA_PRIVACY_AGREE_KIND' , 'Y');
                $modalInstance.dismiss('Y');
            });
        }
    },
    disagree : function(){
        if($window.confirm('미동의시 서비스 이용이 불가합니다. 미동의 하시겠습니까')){
            _fn.set('N')
            .then(function(){
                $storage.set('IA_PRIVACY_AGREE_KIND' , 'N');
                $modalInstance.dismiss('N');
            });
        }
    }
  });

  /**
  * Initialize
  */
  init();

  function init() {
    logger.debug('init', vm);

    console.log(item);
    
    if(!item[0].busnLinkKey){
        $modalInstance.close();
    }
    else{
        vm.meta.busnLinkKey = item[0].busnLinkKey;
    }

    $scope.$on('$destroy', destroy);
  }

  /**
  * Event handlers
  */
  function destroy(event) {
    logger.debug(event);
  }

  /**
  * Custom functions
  */

  /**
   * 모달 닫기
   */
  function dismiss() {
    $modalInstance.dismiss(vm);
  }


  /**
   * 정상 종료
   */
  function confirm(){
    $modalInstance.close(vm);
  }


}
iaPrivacyPCtrl.$inject = ["$log", "$rootScope", "$storage", "$scope", "$modal", "$modalInstance", "$message", "item", "GuestSvc", "MemberSvc", "ngspublic", "comConstant", "$window", "$q"];
/* //common/shared/popup/iaPrivacyP/iaPrivacyPCtrl.js */
}()
!function () { 'use strict';
/* common/shared/popup/iaPushP/iaPushPCtrl.js */

angular
.module('easi.common.shared')
.controller('iaPushPCtrl', iaPushPCtrl);

function iaPushPCtrl($log, $rootScope , $storage, $scope, $modal, $modalInstance, $message, item, GuestSvc,MemberSvc, ngspublic, comConstant , $window , $q) {
  /**
  * Private variables
  */
  var logger = $log(this);
  var vm = this;
  let _fn = {
    set : function(agreeYn){
        let params = {};
        params.busnLinkKey = vm.meta.busnLinkKey + '';
        params.agreeKind = vm.meta.AGREE_KIND;
        params.agreeYn = agreeYn;

        return MemberSvc.insertAgree(params)
        .then(function(d){
            let deferred = $q.defer();
            if(d.user.result === 'SUCCESS'){
                deferred.resolve(d);
            }
            else{ deferred.reject(d); }
            return deferred.promise;
        })
        .catch(function(d){
            let deferred = $q.defer();
            deferred.reject(d);
            return deferred.promise;
        });
    }
  };

  /**
   * Public variables
   */
  _.assign(vm, {
    meta : {
        AGREE_KIND : 'A3',
        busnLinkKey : -1
    }
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    dismiss : dismiss,
    confirm : confirm,
    agree : function(){
        console.log('!!!!');
        if($window.confirm('동의하시겠습니까')){
            _fn.set('Y')
            .then(function(){
                $storage.set('IA_PUSH_AGREE_KIND' , 'Y');
                $modalInstance.dismiss('Y');
            });
        }
    },
    disagree : function(){
        if($window.confirm('미동의시 서비스 이용이 불가합니다. 미동의 하시겠습니까')){
            _fn.set('N')
            .then(function(){
                $storage.set('IA_PUSH_AGREE_KIND' , 'N');
                $modalInstance.dismiss('N');
            });
        }
    }
  });

  /**
  * Initialize
  */
  init();

  function init() {
    logger.debug('init', vm);

    console.log(item);
    
    if(!item[0].busnLinkKey){
        $modalInstance.close();
    }
    else{
        vm.meta.busnLinkKey = item[0].busnLinkKey;
    }

    $scope.$on('$destroy', destroy);
  }

  /**
  * Event handlers
  */
  function destroy(event) {
    logger.debug(event);
  }

  /**
  * Custom functions
  */

  /**
   * 모달 닫기
   */
  function dismiss() {
    $modalInstance.dismiss(vm);
  }


  /**
   * 정상 종료
   */
  function confirm(){
    $modalInstance.close(vm);
  }


}
iaPushPCtrl.$inject = ["$log", "$rootScope", "$storage", "$scope", "$modal", "$modalInstance", "$message", "item", "GuestSvc", "MemberSvc", "ngspublic", "comConstant", "$window", "$q"];
/* //common/shared/popup/iaPushP/iaPushPCtrl.js */
}()
!function () { 'use strict';
/* common/shared/popup/pnInsertP/pnInsertPCtrl.js */

angular
.module('easi.common.shared')
.controller('pnInsertPCtrl', pnInsertPCtrl);

function pnInsertPCtrl($log, $rootScope, $scope, $modal, $modalInstance, $message, item, GuestSvc,ComSvc, ngspublic, comConstant , $window , $q, $cordovaLocalNotification) {
  /**
  * Private variables
  */
  var logger = $log(this);
  var vm = this;
  let _fn = {
    make : { 
      hours : function(){
        let result = [];
        for(let i = 0 ; i < 25 ;i ++){
          let mockup = { v: -1 , t : ''};
          mockup.v = i;
          if(i < 10){ mockup.t += '0'; }
          mockup.t += i;
          result.push(mockup);
        }
        return result;
      },
      mins : function(){
        let result = [];
        for(let i = 0 ; i < 60 ;i ++){
          let mockup = { v: -1 , t : ''};
          mockup.v = i;
          if(i < 10){ mockup.t += '0'; }
          mockup.t += i;
          result.push(mockup);
        }

        return result;
      }
    },
    init : function(){
      if(vm.mod.hours.length <= 0){
        vm.mod.hours = _fn.make.hours();
      }
      if(vm.mod.mins.length <= 0){
        vm.mod.mins = _fn.make.mins();
      }
      
      if(!_.isUndefined(item[0])){
        item = item[0];
      }

      vm.mod.meta.ris = item.REQUST_INFO_SEQ;
      vm.mod.meta.memberSeq = item.MEMBER_SEQ;
      vm.mod.meta.rbs = item.RCUT_BASE_SEQ;
      
      if(item.START_DATE){
        vm.mod.meta.startDate = item.START_DATE;
        vm.mod.meta.endDate = item.END_DATE;
        vm.mod.meta.pgmName = item.PGM_NAME;
      }

      console.log(item);
      
      vm.mod.departure.hour = vm.mod.hours[0];
      vm.mod.wakeup.hour = vm.mod.hours[0];
      vm.mod.departure.min = vm.mod.mins[0];
      vm.mod.wakeup.min = vm.mod.mins[0];
      
      _fn.data.get()
        .then(function(data){
          if(data.user.resultData.requstMemberSubInfo.length > 0){
            var findObj = _.find(vm.mod.hours, {t : data.user.resultData.requstMemberSubInfo[0].START_HOUR});
            vm.mod.departure.hour = findObj;
            findObj = _.find(vm.mod.hours, {t : data.user.resultData.requstMemberSubInfo[0].GETUP_HOUR});
            vm.mod.wakeup.hour = findObj;
            findObj = _.find(vm.mod.mins, {t : data.user.resultData.requstMemberSubInfo[0].START_MINITE});
            vm.mod.departure.min = findObj;
            findObj = _.find(vm.mod.mins, {t : data.user.resultData.requstMemberSubInfo[0].GETUP_MINITE});
            vm.mod.wakeup.min = findObj;
            
            vm.mod.departure.location = data.user.resultData.requstMemberSubInfo[0].START_POINT;
           
            vm.mod.meta.memberSubInfoSeq = data.user.resultData.requstMemberSubInfo[0].MEMBER_SUB_INFO_SEQ;
          }
        })

    },
    data : {
      valid : {
        get : function(){
          let result = true;
          if(vm.mod.meta.memberSeq === -1){ result = false; }
          if(vm.mod.meta.ris === -1){ result = false; }

          return result;
        },
        submit : function(){
          let result = _fn.data.valid.get();
          if(!vm.mod.wakeup.hour || !vm.mod.wakeup.hour.t){ result = false; }
          if(!vm.mod.wakeup.min || !vm.mod.wakeup.min.t){ result = false; }
          if(!vm.mod.departure.hour || !vm.mod.departure.hour.t){ result = false; }
          if(!vm.mod.departure.min || !vm.mod.departure.min.t){ result = false; }
          if(!vm.mod.departure.location || vm.mod.departure.location === ''){ result = false; }

          return result;
        },
        cancel : function(){
          let result = _fn.data.valid.submit();
          if(!vm.mod.meta.memberSubInfoSeq || vm.mod.meta.memberSubInfoSeq === -1){ result = false; }

          return result;
        }
      },
      get : function(){
        let params = {};
        params.memberSeq = vm.mod.meta.memberSeq + '';
        params.requestInfoSeq = vm.mod.meta.ris;

        return GuestSvc.selectRequestMemberSubInfo(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            // WHATEVER 
            deferred.resolve(d);
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      },
      submit: function(){
        let params = {};
        params.memberSeq = vm.mod.meta.memberSeq + '';
        params.requestInfoSeq = vm.mod.meta.ris + '';
        params.getupHour = vm.mod.wakeup.hour.t;
        params.getupMinite = vm.mod.wakeup.min.t;
        params.startHour = vm.mod.departure.hour.t;
        params.startMinite = vm.mod.departure.min.t;
        params.startPoint = vm.mod.departure.location;

        return GuestSvc.insertRequestMemberSubInfo(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            // WHATEVER 
            deferred.resolve(d);
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      },
      update : function(){
        let params = {};
        params.memberSeq = vm.mod.meta.memberSeq + '';
        params.memberSubInfoSeq = vm.mod.meta.memberSubInfoSeq + '';
        params.getupHour = vm.mod.wakeup.hour.t;
        params.getupMinite = vm.mod.wakeup.min.t;
        params.startHour = vm.mod.departure.hour.t;
        params.startMinite = vm.mod.departure.min.t;
        params.startPoint = vm.mod.departure.location;

        return GuestSvc.updateRequestMemberSubInfo(params)
        .then(function(d){
          let deferred = $q.defer();
          if(d.user.result === 'SUCCESS'){
            // WHATEVER 
            deferred.resolve(d);
          }
          else{
            deferred.reject(d);
          }
          return deferred.promise;
        })
        .catch(function(d){
          let deferred = $q.defer();
          deferred.reject(d);
          return deferred.promise;
        });
      }
    }
  };

  /**
   * Public variables
   */
  _.assign(vm, {
    mod : {
      meta : {
        // REQUST_INFO_SEQ
        ris : -1,
        memberSeq : -1,
        memberSubInfoSeq : -1,
        startDate : -1,
        endDate : -1,
        pgmName : ''
      },
      hours : [],
      mins : [],
      wakeup : {
        hour : '',
        min : ''
      },
      departure : {
        hour : '',
        min : '',
        date : '',
        location : ''
      }
    }
  });

  /**
   * Public methods
   */
  _.assign(vm, {
    dismiss : dismiss,
    confirm : confirm,
    evt : {
      pop : {
        close : function(){
          vm.dismiss();
        }
      },
      submit : function(){
        if(_fn.data.valid.submit()){
          _fn.data.submit()
          .then(function(){
            $window.alert('저장되었습니다');

            _fn.data.get()
            .then(function(d){
              
              if(d.user.resultData.requstMemberSubInfo.length >= 0){
                let alrmSeq = d.user.resultData.requstMemberSubInfo[0].MEMBER_SUB_INFO_SEQ;
                let wakeupDt = moment(vm.mod.meta.startDate);
                  wakeupDt.hour(vm.mod.wakeup.hour);
                  wakeupDt.minute(vm.mod.wakeup.min);

                let departureDt = moment(vm.mod.meta.startDate);
                  departureDt.hour(vm.mod.departure.hour);
                  departureDt.minute(vm.mod.departure.min);

                // $cordovaLocalNotification.schedule({
                //   id : parseInt('10' + alrmSeq),
                //   at : dt.toDate(),
                //   title : '[기상] [' + vm.mod.meta.pgmName + ']',
                //   text : '['+ vm.mod.meta.pgMName +'] 기상 시간입니다.'
                // } , vm.confirm);

                  $cordovaLocalNotification.schedule([{
                      id : parseInt('10' + alrmSeq),
                      at : wakeupDt.toDate(),
                      title : '[기상] [' + vm.mod.meta.pgmName + ']',
                      text : '['+ vm.mod.meta.pgMName +'] 기상 시간입니다.'
                  },
                  {
                      id : parseInt('20' + alrmSeq),
                      at : departureDt.toDate(),
                      title : '[출발] [' + vm.mod.meta.pgmName + ']',
                      text : '['+ vm.mod.meta.pgMName +'] 출발 시간입니다.'
                  }]).then(function (result) {
                      confirm();
                  });
              }
            });
          });
        }
        else{
          $window.alert('입력값을 확인해주세요');
        }
      },
      update : function(){
        if(_fn.data.valid.submit()){
          _fn.data.update()
          .then(function(){
            $window.alert('수정되었습니다');
            
            _fn.data.get()
            .then(function(d){
              let alrmSeq = d.user.resultData.requstMemberSubInfo[0].MEMBER_SUB_INFO_SEQ;
              // let dt = moment(vm.mod.meta.startDate);
              // dt.hour(vm.mod.wakeup.hour);
              // dt.minute(vm.mod.wakeup.min);
              // $cordovaLocalNotification.cancel(alrmSeq , function(){
              //   $cordovaLocalNotification.schedule({
              //     id : parseInt('10' + alrmSeq),
              //     at : dt.toDate(),
              //     title : '[기상] [' + vm.mod.meta.pgmName + ']',
              //     text : '['+ vm.mod.meta.pgMName +'] 기상 시간입니다.'
              //   } , vm.confirm);
              // });
                let wakeupDt = moment(vm.mod.meta.startDate);
                wakeupDt.hour(vm.mod.wakeup.hour);
                wakeupDt.minute(vm.mod.wakeup.min);

                let departureDt = moment(vm.mod.meta.startDate);
                departureDt.hour(vm.mod.departure.hour);
                departureDt.minute(vm.mod.departure.min);
                $cordovaLocalNotification.update([{
                        id : parseInt('10' + alrmSeq),
                        at : wakeupDt.toDate(),
                        title : '[기상] [' + vm.mod.meta.pgmName + ']',
                        text : '['+ vm.mod.meta.pgMName +'] 기상 시간입니다.'
                    },
                    {
                        id : parseInt('20' + alrmSeq),
                        at : departureDt.toDate(),
                        title : '[출발] [' + vm.mod.meta.pgmName + ']',
                        text : '['+ vm.mod.meta.pgMName +'] 출발 시간입니다.'
                    }]).then(function (result) {
                    confirm();
                });
            });
          });
        }
      }
    }
  });

  /**
  * Initialize
  */
  init();

  function init() {
      logger.debug('init', vm);
      _fn.init();
      $scope.$on('$destroy', destroy);
  }

  /**
  * Event handlers
  */
  function destroy(event) {
    logger.debug(event);
  }

  /**
  * Custom functions
  */

  /**
   * 모달 닫기
   */
  function dismiss() {
    _fn.init();
    $modalInstance.dismiss(vm);
  }


  /**
   * 정상 종료
   */
  function confirm(){
    $modalInstance.close(vm);
  }


}
pnInsertPCtrl.$inject = ["$log", "$rootScope", "$scope", "$modal", "$modalInstance", "$message", "item", "GuestSvc", "ComSvc", "ngspublic", "comConstant", "$window", "$q", "$cordovaLocalNotification"];
/* //common/shared/popup/pnInsertP/pnInsertPCtrl.js */
}()


angular.module("easi.common").run(["$templateCache", function($templateCache) {$templateCache.put("components/editor/editor.tpl.html","<form name=tx_editor_form id=tx_editor_form autocomplete=off><div id=tx_trex_container class=tx-editor-container><div id=tx_toolbar_basic class=\"tx-toolbar tx-toolbar-basic\" style=\"zoom: 2;\"><div class=tx-toolbar-boundary><ul class=\"tx-bar tx-bar-left tx-group-font\"><li class=tx-list><div unselectable=on class=\"tx-btn-lbg tx-bold\" id=tx_bold><a href=javascript:; class=tx-icon title=\"굵게 (Ctrl+B)\">굵게</a></div></li><li class=tx-list><div unselectable=on class=\"tx-btn-bg tx-underline\" id=tx_underline><a href=javascript:; class=tx-icon title=\"밑줄 (Ctrl+U)\">밑줄</a></div></li><li class=tx-list><div unselectable=on class=\"tx-btn-bg tx-italic\" id=tx_italic><a href=javascript:; class=tx-icon title=\"기울임 (Ctrl+I)\">기울임</a></div></li><li class=tx-list><div unselectable=on class=\"tx-btn-bg tx-strike\" id=tx_strike><a href=javascript:; class=tx-icon title=\"취소선 (Ctrl+D)\">취소선</a></div></li></ul><ul class=\"tx-bar tx-bar-left tx-group-align\"><li class=tx-list><div unselectable=on class=\"tx-btn-lbg tx-alignleft\" id=tx_alignleft><a href=javascript:; class=tx-icon title=\"왼쪽정렬 (Ctrl+,)\">왼쪽정렬</a></div></li><li class=tx-list><div unselectable=on class=\"tx-btn-bg tx-aligncenter\" id=tx_aligncenter><a href=javascript:; class=tx-icon title=\"가운데정렬 (Ctrl+.)\">가운데정렬</a></div></li><li class=tx-list><div unselectable=on class=\"tx-btn-bg tx-alignright\" id=tx_alignright><a href=javascript:; class=tx-icon title=\"오른쪽정렬 (Ctrl+/)\">오른쪽정렬</a></div></li><li class=tx-list><div unselectable=on class=\"tx-btn-rbg tx-alignfull\" id=tx_alignfull><a href=javascript:; class=tx-icon title=양쪽정렬>양쪽정렬</a></div></li></ul><ul class=\"tx-bar tx-bar-left tx-group-tab\"><li class=tx-list><div unselectable=on class=\"tx-btn-lbg tx-indent\" id=tx_indent><a href=javascript:; title=\"들여쓰기 (Tab)\" class=tx-icon>들여쓰기</a></div></li><li class=tx-list><div unselectable=on class=\"tx-btn-rbg tx-outdent\" id=tx_outdent><a href=javascript:; title=\"내어쓰기 (Shift+Tab)\" class=tx-icon>내어쓰기</a></div></li></ul><ul class=\"tx-bar tx-bar-left tx-group-undo\"><li class=tx-list><div unselectable=on class=\"tx-btn-lbg tx-undo\" id=tx_undo><a href=javascript:; class=tx-icon title=\"실행취소 (Ctrl+Z)\">실행취소</a></div></li><li class=tx-list><div unselectable=on class=\"tx-btn-rbg tx-redo\" id=tx_redo><a href=javascript:; class=tx-icon title=\"다시실행 (Ctrl+Y)\">다시실행</a></div></li></ul></div></div><div id=tx_canvas class=tx-canvas><div id=tx_loading class=tx-loading><div><img src=../daumeditor/images/icon/editor/loading2.png width=113 height=21 align=absmiddle></div></div><div id=tx_canvas_wysiwyg_holder class=tx-holder style=display:block;><iframe id=tx_canvas_wysiwyg name=tx_canvas_wysiwyg allowtransparency=true frameborder=0 style=width:100%></iframe></div><div class=tx-source-deco><div id=tx_canvas_source_holder class=tx-holder><textarea id=tx_canvas_source rows=30 cols=30>\n                    </textarea></div></div><div id=tx_canvas_text_holder class=tx-holder><textarea id=tx_canvas_text rows=30 cols=30>\n                </textarea></div></div><div class=tx-side-bi id=tx_side_bi><div style=\"text-align: right;\"><img hspace=4 height=14 width=78 align=absmiddle src=../daumeditor/images/icon/editor/editor_bi.png></div></div><div id=tx_attach_div class=tx-attach-div><div id=tx_attach_txt class=tx-attach-txt>파일 첨부</div><div id=tx_attach_box class=tx-attach-box><div class=tx-attach-box-inner><div id=tx_attach_preview class=tx-attach-preview><p></p><img src=../daumeditor/images/icon/editor/pn_preview.gif width=147 height=108 unselectable=on></div><div class=tx-attach-main><div id=tx_upload_progress class=tx-upload-progress><div>0%</div><p>파일을 업로드하는 중입니다.</p></div><ul class=tx-attach-top><li id=tx_attach_delete class=tx-attach-delete><a>전체삭제</a></li><li id=tx_attach_size class=tx-attach-size>파일: <span id=tx_attach_up_size class=tx-attach-size-up></span>/<span id=tx_attach_max_size></span></li><li id=tx_attach_tools class=tx-attach-tools></li></ul><ul id=tx_attach_list class=tx-attach-list></ul></div></div></div></div></div></form>");
$templateCache.put("layout/menu/menu.tpl.html","<div id=total_menu_wrap class=sidenav-content><div class=sidenav-box><div class=header-top><a class=loginBtn ng-click=loginBtn()>{{loginText}}</a> <a class=home><span class=hide>HOME</span></a> <a id=close-button class=close-btn ng-click=menuCloseBtn()><span class=hidden>닫기</span></a></div><div id=iscroller class=iscroller><div class=sidebar-content><ul class=bot-menu ng-model=menu><li ng-repeat=\"dep1 in menu\"><dl ng-if=\"dep1.COMMON_INFO_VALUE1 !== \'C9\' && dep1.COMMON_INFO_VALUE1 !== \'M4\'\"><dt ng-if=!dep1.menu><a ng-click=\"menuCloseBtn(); moveView(dep1.COMMON_INFO_VALUE6);\">{{dep1.COMMON_INFO_VALUE2}}</a></dt><dt ng-if=dep1.menu><a ng-click=toggle($event)>{{dep1.COMMON_INFO_VALUE2}}</a></dt><dd ng-if=dep1.menu ng-repeat=\"dep2 in dep1.menu\"><a ng-click=\"menuCloseBtn(); moveView(dep2.COMMON_INFO_VALUE6);\">{{dep2.COMMON_INFO_VALUE2}}</a></dd></dl><div ng-if=\"dep1.COMMON_INFO_VALUE1 === \'C9\'\" class=btn_wrap><div class=inner><button type=button class=\"btn skyblue\" ng-click=\"menuCloseBtn(); moveView(dep1.COMMON_INFO_VALUE6);\">{{dep1.COMMON_INFO_VALUE2}}</button></div></div><div ng-if=\"dep1.COMMON_INFO_VALUE1 === \'M4\'\" class=btn_wrap><div class=inner><button type=button class=\"btn skyblue\" ng-click=\"menuCloseBtn(); moveView(dep1.menu[0].COMMON_INFO_VALUE6);\">{{dep1.menu[0].COMMON_INFO_VALUE2}}</button></div></div></li><li ng-if=\"loginKind === \'L1\'\"><div class=btn_wrap><div class=inner><button type=button class=\"btn red\" ng-click=\"menuCloseBtn(); moveView(\'pesn.html#/alarm/paList\');\">기상 출발 알람</button></div></div></li></ul></div></div></div></div>");
$templateCache.put("layout/footer/footer.tpl.html","<div class=footer><ul><li><a href=#>이용약관</a></li><li><a href=#>개인정보처리방침</a></li></ul><p>Copyright ©Clickjob., Ltd. All Rights Reserved.</p></div>");}]);
angular.module("easi.common.shared").run(["$templateCache", function($templateCache) {$templateCache.put("menu/menu.tpl.html","<div class=\"inputLayerType allMenu\" id=tsot2104p style=display:block;><div class=popupHeading><h3>전체메뉴 보기</h3><div class=close><a ng-click=closeMainMenu()><img src=../images/popup/close.png alt=닫기></a></div></div><div class=popupContentWrapCase><div class=popupContentWrap><div class=scrollBox><div class=siteMap><div class=menuLine><ul class=depth1 ng-repeat=\"dep1 in menu | filter:filterGroupA\"><li><strong>{{dep1.name}}</strong><ul class=depth2><li ng-repeat=\"dep2 in dep1.menu\"><strong ng-if=!dep2.uri>{{dep2.name}}</strong> <a href={{dep2.uri}} ng-if=dep2.uri ng-click=closeMainMenu()><strong>{{dep2.name}}</strong></a><ul class=depth3><li ng-repeat=\"dep3 in dep2.menu\"><a href={{dep3.uri}} ng-click=closeMainMenu()>{{dep3.name}}</a></li></ul></li></ul></li></ul></div><div class=menuLine><ul class=depth1 ng-repeat=\"dep1 in menu | filter:filterGroupB\"><li><strong>{{dep1.name}}</strong><ul class=depth2><li ng-repeat=\"dep2 in dep1.menu | filter:filterExceptMe\"><strong ng-if=!dep2.uri>{{dep2.name}}</strong> <a href={{dep2.uri}} ng-if=dep2.uri ng-click=closeMainMenu()><strong>{{dep2.name}}</strong></a><ul class=depth3><li ng-repeat=\"dep3 in dep2.menu\"><a href={{dep3.uri}} ng-click=closeMainMenu()>{{dep3.name}}</a></li></ul></li></ul></li></ul></div><div class=menuLine><ul class=depth1 ng-repeat=\"dep1 in menu | filter:filterGroupC\"><li><strong>{{dep1.name}}</strong><ul class=depth2><li ng-repeat=\"dep2 in dep1.menu\"><strong ng-if=!dep2.uri>{{dep2.name}}</strong> <a href={{dep2.uri}} ng-if=dep2.uri ng-click=closeMainMenu()><strong>{{dep2.name}}</strong></a><ul class=depth3><li ng-repeat=\"dep3 in dep2.menu\"><a href={{dep3.uri}} ng-click=closeMainMenu()>{{dep3.name}}</a></li></ul></li></ul></li></ul></div><div class=menuLine><ul class=depth1 ng-repeat=\"dep1 in menu | filter:filterGroupD\"><li><strong>{{dep1.name}}</strong><ul class=depth2><li ng-repeat=\"dep2 in dep1.menu\"><strong ng-if=!dep2.uri>{{dep2.name}}</strong> <a href={{dep2.uri}} ng-if=dep2.uri ng-click=closeMainMenu()><strong>{{dep2.name}}</strong></a><ul class=depth3><li ng-repeat=\"dep3 in dep2.menu\"><a href={{dep3.uri}} ng-click=closeMainMenu()>{{dep3.name}}</a></li></ul></li></ul></li></ul></div><div class=menuLine><ul class=depth1 ng-repeat=\"dep1 in menu | filter:filterGroupE\"><li><strong>{{dep1.name}}</strong><ul class=depth2><li ng-repeat=\"dep2 in dep1.menu\"><strong ng-if=!dep2.uri class=ellipsis>{{dep2.name}}</strong> <a href={{dep2.uri}} ng-if=dep2.uri ng-click=closeMainMenu()><strong class=ellipsis>{{dep2.name}}</strong></a><ul class=depth3><li ng-repeat=\"dep3 in dep2.menu\"><a href={{dep3.uri}} ng-click=closeMainMenu() class=ellipsis>{{dep3.name}}</a></li></ul></li></ul></li></ul></div></div></div></div></div></div>");
$templateCache.put("popup/iaGeoP/iaGeoP.tpl.html","<section class=infoAcceptPop><h3 class=hide>정보 동의 팝업</h3><div><h4 class=hide>위치정보 동의</h4><p>관리를 위하여 GPS활용에 동의를 받고 있습니다. 미 동의 시 이용에 제약이 있을 실 수 있습니다.</p><div class=infoCheck><input ng-click=iaGeoP.agree() checked type=radio name=locationInfo id=locationInfo_ok><label for=locationInfo_ok class=mr12>동의</label> <input ng-click=iaGeoP.disagree() type=radio name=locationInfo id=locationInfo_no><label for=locationInfo_no>미동의</label></div></div></section>");
$templateCache.put("popup/iaPersonInfP/iaPersonInfP.tpl.html","<section class=infoAcceptPop><h3 class=hide>정보 동의 팝업</h3><div><h4 class=hide>개인정보 동의</h4><p>출연자의 개인정보를 출연자 구인 목적 이외에는 활용 할 수 없습니다. 이에 동의 하십니까?</p><div class=infoCheck><input ng-click=iaPersonInfP.agree() checked type=radio name=locationInfo id=locationInfo_ok><label for=locationInfo_ok class=mr12>동의</label> <input ng-click=iaPersonInfP.disagree() type=radio name=locationInfo id=locationInfo_no><label for=locationInfo_no>미동의</label></div></div></section>");
$templateCache.put("popup/iaPrivacyP/iaPrivacyP.tpl.html","<section class=infoAcceptPop><h3 class=hide>정보 동의 팝업</h3><div><h4 class=hide>개인정보 동의</h4><p>본인의 인적 사항 등을 구인 기업, 제작사 등이 열람하시는데 동의하십니까? 미동의 시 0000(앱명칭) 이용이 불가 합니다.</p><div class=infoCheck><input ng-click=iaPrivacyP.agree() checked type=radio name=locationInfo id=locationInfo_ok><label for=locationInfo_ok class=mr12>동의</label> <input ng-click=iaPrivacyP.disagree() type=radio name=locationInfo id=locationInfo_no><label for=locationInfo_no>미동의</label></div></div></section>");
$templateCache.put("popup/iaPushP/iaPushP.tpl.html","<section class=infoAcceptPop><h3 class=hide>정보 동의 팝업</h3><div><h4 class=hide>푸시알람 동의</h4><p>ClickJobKorea의 원할한 이용을 위하여 PUSH 알림에 동의를 받고 있습니다. 미 동의 시 이용에 제약이 있실 수 있습니다.</p><div class=infoCheck><input ng-click=iaPushP.agree() checked type=radio name=locationInfo id=locationInfo_ok><label for=locationInfo_ok class=mr12>동의</label> <input ng-click=iaPushP.disagree() type=radio name=locationInfo id=locationInfo_no><label for=locationInfo_no>미동의</label></div></div></section>");
$templateCache.put("popup/pnInsertP/pnInsertP.tpl.html","<section class=\"callPop guestpop\"><h3 class=hide>출석/종료 관리 팝업</h3><div><h4>기상 시간 및 출발장소 입력</h4><div class=innerForm><dl><dt>기상시간</dt><dd><div class=\"ms two\"><span class=\"select-box select-box2\"><select class=sel_m title=\"시간 선택\" ng-model=pnInsertP.mod.wakeup.hour ng-options=\"item.t for item in pnInsertP.mod.hours\"></select></span> <span class=\"space-block txt pdr10\">시</span> <span class=\"select-box select-box2\"><select class=sel_m title=\"분 선택\" ng-model=pnInsertP.mod.wakeup.min ng-options=\"item.t for item in pnInsertP.mod.mins\"><option value=0>분</option></select></span> <span class=\"space-block txt\">분</span></div></dd></dl><dl class=mt10><dt>출발시간</dt><dd><div class=\"ms two\"><span class=\"select-box select-box2\"><select class=sel_m title=\"시간 선택\" ng-model=pnInsertP.mod.departure.hour ng-options=\"item.t for item in pnInsertP.mod.hours\"><option value=0>시</option></select></span> <span class=\"space-block txt pdr10\">시</span> <span class=\"select-box select-box2\"><select class=sel_m title=\"분 선택\" ng-model=pnInsertP.mod.departure.min ng-options=\"item.t for item in pnInsertP.mod.mins\"><option value=0>분</option></select></span> <span class=\"space-block txt\">분</span></div></dd></dl><dl class=mt10><dt>출발장소</dt><dd><div class=\"ms col2\"><span class=\"rel text-input\"><input type=text placeholder=장소입력 ng-model=pnInsertP.mod.departure.location> <label class=hide>장소입력</label></span></div></dd></dl><span class=chk-box><input type=checkbox name=preferredYn id=preferredYn ng-click=pnInsertP.evt.pop.close()> <label for=preferredYn><span>다음에 입력하기</span></label></span><div class=btn_wrap><div class=row><button type=button class=btn ng-click=pnInsertP.evt.submit() ng-if=\"pnInsertP.mod.meta.memberSubInfoSeq === -1\">등록</button> <button type=button class=btn ng-click=pnInsertP.evt.update() ng-if=\"pnInsertP.mod.meta.memberSubInfoSeq !== -1\">수정</button></div></div><a href=javascript: ng-click=pnInsertP.evt.pop.close() class=closePop><span class=hide>닫기</span></a></div></div></section>");}]);