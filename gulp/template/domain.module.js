
angular
  .module('<%= domain %>', [<% subdomains.forEach(function(subdomain) { %>
    '<%= domain %>.<%= subdomain %>',<% }) %>
  ])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('<%= domain %>', {
      url: '',
      abstract: false,
      template: '<div ui-view><h1><%= domain %></h1></div>',
      controller: '<%= domain %>Ctrl as <%= domain %>',
      resolve: {
        Env: function($env) {
          return $env.get();
        },
        User: function($user) {
          return $user.get();
        },
        TempData: function($storage) {
          return $storage.getTempData();
        }
      }
    });

  // $urlRouterProvider.otherwise('/<%= subdomains[0] %>');
}