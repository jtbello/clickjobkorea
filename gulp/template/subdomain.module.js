
angular
  .module('<%= domain %>.<%= subdomain %>', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('<%= domain %>.<%= subdomain %>', {
      url: '/<%= subdomain %>',
      abstract: false,
      template: '<div ui-view><h1><%= domain %>.<%= subdomain %></h1></div>',
      controller: '<%= subdomain %>Ctrl as <%= subdomain %>'
    })
<% var first;views.forEach(function(view) { if (!/^[a-zA-Z]{4}[0-9]{4}m/.test(view)) { return }first = first || view%>
    .state('<%= domain %>.<%= subdomain %>.<%= view %>', {
      url: '/<%= view %>',
      templateUrl: '<%= subdomain %>/<%= view %>/<%= view %>.tpl.html',
      controller: '<%= view %>Ctrl as <%= view %>'
    })
<% }) %>
    ;

  // $urlRouterProvider.when('/<%= subdomain %>', '/<%= subdomain %>/<%= first %>')
}
