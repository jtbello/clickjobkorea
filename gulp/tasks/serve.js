'use strict';

var fs = require('fs');
var path = require('path');

var gulp = require('gulp-help')(require('gulp'));
var $ = require('gulp-load-plugins')();
var browserSync = require('browser-sync');

var domains = getFolders('src/app');

gulp.task('serve', function() {
  browserSync({
    browser: 'Chrome',
    startPath: '/app/public.html#/login',
    server: {
      baseDir: ['.tmp', 'bower_components', 'vendor', 'assets', 'cordova'],
      routes: {
        '/bower_components': './bower_components',
        '/vendor': './vendor'
      }
    },
    open: false,
    online: false,
    ghostMode: false,
    logLevel: 'debug',
    notify: false,
    ui: false
  });
});

gulp.task('serve:deploy', function() {
  browserSync({
    browser: 'Chrome',
    startPath: '/app/smpl/',
    server: {
      baseDir: global.buildDir + ''
    },
    open: false,
    ghostMode: false,
    logLevel: 'info',
    notify: false,
    ui: false
  });
});

gulp.task('watch', function() {
  $.watch('src/{core,common,api}/**/*.js', function() {
    gulp.start('script:common');
  });
  $.watch(['src/common/**/*.tpl.html', '!src/common/shared/**/*.tpl.html'], function() {
    gulp.start('script:common:template');
  });
  $.watch('src/common/shared/**/*.tpl.html', function() {
    gulp.start('script:shared:template');
  });
  $.watch('src/app/app.js', function() {
    gulp.start('script:app');
  });
  domains.forEach(function(domain) {
    $.watch('src/app/' + domain + '/**/*.js', function() {
      gulp.start('script:app:' + domain);
    });
  });
  $.watch('src/app/**/*.tpl.html', function() {
    gulp.start('script:app:template');
  });
  $.watch('src/domain.html', function() {
    gulp.start('html:app');
  });

  $.watch('src/public/**/*.js', function() {
    gulp.start('script:public');
  });
  $.watch('src/public/**/*.tpl.html', function() {
    gulp.start('script:public:template');
  });
  $.watch('src/public.html', function() {
    gulp.start('html:public');
  });
});

function getFolders(dir) {
  return fs.readdirSync(dir)
    .filter(function(file) {
      return fs.statSync(path.join(dir, file)).isDirectory();
    });
}
