'use strict';

var gulp = require('gulp-help')(require('gulp'));
var del = require('del');
var vinylPaths = require('vinyl-paths');

gulp.task('clean:ut', function () {
  return gulp.src('ut')
    .pipe(vinylPaths(del));
});

gulp.task('clean:tmp', function () {
  return gulp.src('.tmp')
    .pipe(vinylPaths(del));
});

gulp.task('clean:www', function () {
  return gulp.src(global.buildDir)
    .pipe(vinylPaths(del));
});

gulp.task('clean:generate', function () {
  return gulp.src('.src')
    .pipe(vinylPaths(del));
});