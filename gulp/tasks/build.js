'use strict';

var fs = require('fs');
var path = require('path');
var merge = require('merge-stream');

var gulp = require('gulp-help')(require('gulp'));
var $ = require('gulp-load-plugins')();
var $$ = $.loadUtils(['colors', 'env', 'log', 'pipeline']);
var runSequence = require('run-sequence');
var wiredep = require('wiredep').stream;

var header = '!function () { \'use strict\';\n/* ${filename} */\n';
var footer = '/* //${filename} */\n}()';

var domains = getFolders('src/app');

var onError = function(err) {
  $$.beep([0, 0, 0]);
  $$.log($$.colors.red('[ERROR] [' + err.plugin + '] ' + err.message));
};

gulp.task('build', ['clean:tmp'], function(cb) {
  return runSequence('script:common', 'script:common:template', 'script:shared:template', 'script:app', 'script:app:template', 'html:app', 'script:public', 'script:public:template', 'html:public', cb);
});

gulp.task('script:common', function() {
  return gulp.src([
      'src/{common,core,api}/**/*.module.js',
      'src/{common,core,api}/**/*.js'
    ])
    .pipe($.plumber({errorHandler: $$.log}))
    .pipe($.if('**/env.config.js', $.preprocess()))
    .pipe($.if('**/common.module.js', $.preprocess({
      context: {
        EDU: !!$$.env.edu,
        NODE_ENV: $$.env.NODE_ENV
      }
    })))
    .pipe($.ngAnnotate())
    .pipe($.wrapper({
      header: header,
      footer: footer
    }))
    .pipe($.sourcemaps.init())
    .pipe($.concat('common.js'))
    .pipe($.sourcemaps.write('./source'))
    .pipe($.plumber.stop())
    .pipe(gulp.dest('.tmp/scripts'));
});

gulp.task('script:app', function(cb) {
  var domainTasks = domains.map(function(domain) {
    return 'script:app:' + domain;
  });
  domainTasks.push(cb);
  return runSequence.apply(null, domainTasks);
});

gulp.task('script:app:template', function() {
  var dir = 'src/app';
  var tasks = domains.map(function(domain) {
    return gulp.src(path.join(dir, domain, '**/*.tpl.html'))
      .pipe($.plumber({errorHandler: $$.log}))
      .pipe($.minifyHtml())
      .pipe($.angularTemplatecache({
        module: domain,
        filename: domain + '-templates.js'
      }))
      .pipe($.replace(/[\s]on[A-Za-z]*?\=\".*?\"/g, ''))
      .pipe($.replace(/[\s]href*?\=\"#none\"/g, ''))
      .pipe($.replace(/[\s]href*?\=\"#\"/g, ''))
      .pipe($.plumber.stop())
      .pipe(gulp.dest('.tmp/scripts'));
  });
  return merge(tasks);
});


(function() {
  var dir = 'src/app';
  domains.forEach(function(module) {
    gulp.task('script:app:' + module, function() {
      return gulp.src(['src/app/app.js', path.join(dir, module, '/**/*.module.js'), path.join(dir, module, '/**/*.js')])
        .pipe($.plumber({errorHandler: onError}))
        .pipe($.ngAnnotate())
        .pipe($.wrapper({
          header: header,
          footer: footer
        }))
        .pipe($.concat(module + '.js'))
        .pipe($.preprocess({
          context: {
            MODULE_NAME: module
          }
        }))
        .pipe($.plumber.stop())
        .pipe(gulp.dest('.tmp/scripts'));
    });
  });
})();

gulp.task('script:public:template', function() {
  return gulp.src('src/public/**/*.tpl.html')
    .pipe($.plumber({errorHandler: $$.log}))
    .pipe($.minifyHtml())
    .pipe($.angularTemplatecache({
      module: 'easi.public',
      filename: 'public-templates.js'
    }))
    .pipe($.replace(/[\s]on[A-Za-z]*?\=\".*?\"/g, ''))
    .pipe($.replace(/[\s]href*?\=\"#none\"/g, ''))
    .pipe($.replace(/[\s]href*?\=\"#\"/g, ''))
    .pipe($.plumber.stop())
    .pipe(gulp.dest('.tmp/scripts'));
});

gulp.task('script:common:template', function() {
  return gulp.src(['src/common/**/*.tpl.html', '!src/common/shared/**/*.tpl.html'])
    .pipe($.plumber({errorHandler: $$.log}))
    .pipe($.minifyHtml())
    .pipe($.angularTemplatecache({
      module: 'easi.common',
      filename: 'common-templates.js'
    }))
    .pipe($.replace(/[\s]on[A-Za-z]*?\=\".*?\"/g, ''))
    .pipe($.replace(/[\s]href*?\=\"#none\"/g, ''))
    .pipe($.replace(/[\s]href*?\=\"#\"/g, ''))
    .pipe($.plumber.stop())
    .pipe(gulp.dest('.tmp/scripts'));
});

gulp.task('script:shared:template', function() {
  return gulp.src('src/common/shared/**/*.tpl.html')
    .pipe($.plumber({errorHandler: $$.log}))
    .pipe($.minifyHtml())
    .pipe($.angularTemplatecache({
      module: 'easi.common.shared',
      filename: 'shared-templates.js'
    }))
    .pipe($.replace(/[\s]on[A-Za-z]*?\=\".*?\"/g, ''))
    .pipe($.replace(/[\s]href*?\=\"#none\"/g, ''))
    .pipe($.replace(/[\s]href*?\=\"#\"/g, ''))
    .pipe($.plumber.stop())
    .pipe(gulp.dest('.tmp/scripts'));
});

gulp.task('script:public', function() {
  return gulp.src([
      'src/public/**/*.module.js',
      'src/public/**/*.js'
    ])
    .pipe($.plumber({errorHandler: $$.log}))
    .pipe($.ngAnnotate())
    .pipe($.wrapper({
      header: header,
      footer: footer
    }))
    .pipe($.sourcemaps.init())
    .pipe($.concat('public.js'))
    .pipe($.sourcemaps.write('./source'))
    .pipe($.plumber.stop())
    .pipe(gulp.dest('.tmp/scripts'));
});

gulp.task('html:app', function() {
  var tasks = domains.map(function(domain) {
    return gulp.src('src/domain.html')
      .pipe($.plumber({errorHandler: $$.log}))
      .pipe(wiredep())
      .pipe($.rename(domain + '.html'))
      .pipe(gulp.dest('.tmp/app'))
      .pipe($.inject(gulp.src(['.tmp/scripts/common.js', '.tmp/scripts/common-templates.js', '.tmp/scripts/shared-templates.js'], {
        read: false
      }), {
        relative: true,
        starttag: '<!-- inject:common:{{ext}} -->'
      }))
      .pipe($.inject(gulp.src(['.tmp/scripts/' + domain + '.js', '.tmp/scripts/' + domain + '-templates.js'], {
        read: false
      }), {
        relative: true
      }))
      .pipe($.replace(/[\s]on[A-Za-z]*?\=\".*?\"/g, ''))
      .pipe($.replace(/[\s]href*?\=\"#none\"/g, ''))
      .pipe($.replace(/[\s]href*?\=\"#\"/g, ''))
      .pipe($.replace('domain.js', domain + '.js'))
      .pipe($.rename(domain + '.html'))
      .pipe($.plumber.stop())
      .pipe(gulp.dest('.tmp/app'));
  });
  return merge(tasks);
});


gulp.task('html:public', function() {
  return gulp.src('src/public.html')
    .pipe($.plumber({errorHandler: $$.log}))
    .pipe(wiredep())
    .pipe(gulp.dest('.tmp/app'))
    .pipe($.inject(gulp.src(['.tmp/scripts/common.js', '.tmp/scripts/common-templates.js', '.tmp/scripts/shared-templates.js'], {
        read: false
      }), {
        relative: true,
        starttag: '<!-- inject:common:{{ext}} -->'
      }))
    .pipe($.inject(gulp.src(['.tmp/scripts/public.js', '.tmp/scripts/public-templates.js'], {
      read: false
    }), {
      relative: true
    }))
    .pipe($.replace(/[\s]on[A-Za-z]*?\=\".*?\"/g, ''))
    .pipe($.replace(/[\s]href*?\=\"#none\"/g, ''))
    .pipe($.replace(/[\s]href*?\=\"#\"/g, ''))
    .pipe(gulp.dest('.tmp/app'));
});

function getFolders(dir) {
  return fs.readdirSync(dir)
    .filter(function(file) {
      return fs.statSync(path.join(dir, file)).isDirectory();
    });
}
