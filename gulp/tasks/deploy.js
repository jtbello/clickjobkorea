'use strict';

var gulp = require('gulp-help')(require('gulp'));
var $ = require('gulp-load-plugins')();
var $$ = $.loadUtils(['colors', 'env', 'log', 'pipeline']);
var runSequence = require('run-sequence');

var onError = function(err) {
  $$.beep([0, 0, 0]);
  $$.log($$.colors.red('[ERROR] [' + err.plugin + '] ' + err.message));
};

gulp.task('build:deploy', ['clean:www'], function(cb) {
  return runSequence('build', 'html:deploy', 'image:deploy', 'style:deploy', 'editor:deploy', 'chart:deploy', 'chzero:deploy', 'AES:deploy', 'index:deploy', 'cordova:deploy', 'manifest', 'manifest:deploy', cb);
});

gulp.task('html:deploy', function() {
  var assets = $.useref.assets();
  return gulp.src('.tmp/**/*.html')
    .pipe($.plumber({errorHandler: onError}))
    .pipe($.replace(/\"..\/(bower_components|vendor)\//g, '"'))
    .pipe($.replace(/\"..\/(scripts|styles)\//g, '"$1/'))
    .pipe(assets)
      .pipe($.replace(/\/\/# sourceMappingURL=.*.js.map/g, ''))
      .pipe($.replace(/..\/vendor\//g, '../'))
    .pipe(assets.restore())
    .pipe($.useref())
    .pipe($.replace(/"(scripts|styles|daumeditor|FusionCharts|chzero|AES)\//g, '"../$1/'))
    .pipe($.plumber.stop())
    //.pipe($.if('*.css', $.replace('.css', '.css?_=' + Date.now())))
    //.pipe($.if('*.html', $.replace('.css', '.css?_=' + Date.now())))
    //.pipe($.if('*.html', $.replace('.js', '.js?_=' + Date.now())))
    .pipe(gulp.dest(global.buildDir + ''));
});

gulp.task('manifest', function() {
  return gulp.src(global.buildDir + '/**/*')
    .pipe($.manifest({
      hash: true,
      preferOnline: true,
      network: ['http://*', 'https://*', '*'],
      filename: 'app.manifest',
      exclude: 'app.manifest'
    }))
    .pipe(gulp.dest(global.buildDir + ''));
});

gulp.task('manifest:deploy', function() {
    return gulp.src('cordova/manifest.json')
        .pipe(gulp.dest(global.buildDir + ''));
});
// generate chcp.manifest
// gulp.task('manifest', ['index:deploy'], function() {
//   return gulp.src(['**/*', '!app.*'], { cwd: global.buildDir + '' })
//     .pipe($.buster({ fileName: 'app.manifest', algo: 'md5', transform: function(hashes) {
//       var transformed = [];
//       for (var file in hashes) {
//         transformed.push({ file: file, hash: hashes[file] });
//       }
//       return transformed;
//       }, formatter: function(transformed) {
//         return JSON.stringify(transformed, null, '\t')
//       }
//     }))
//     .pipe(gulp.dest(global.buildDir));
// });

gulp.task('image:deploy', function() {
  return gulp.src('assets/images/**/*.*')
    // .pipe($.plumber({errorHandler: onError}))
    // .pipe($.imagemin({
    //   optimizationLevel: 3,
    //   progressive: true,
    //   interlaced: true
    // }))
    // .pipe($.plumber.stop())
    .pipe(gulp.dest(global.buildDir + '/images'));
});

gulp.task('style:deploy', function() {
  var source = ['assets/styles/**/*.*', '!**/common.css'];
  if (!$$.env['include-fonts']) {
    source.push('!assets/styles/font/**.*');
  }
  return gulp.src(source)
    // .pipe($.csso())
    .pipe(gulp.dest(global.buildDir + '/styles'));
});

gulp.task('editor:deploy', function() {
  return gulp.src('vendor/daumeditor/**/*.*')
    .pipe(gulp.dest(global.buildDir + '/daumeditor'));
});

gulp.task('chart:deploy', function() {
  return gulp.src('vendor/FusionCharts/**/*.*')
    .pipe(gulp.dest(global.buildDir + '/FusionCharts'));
});

gulp.task('chzero:deploy', function() {
  return gulp.src('vendor/chzero/**/*.*')
    .pipe(gulp.dest(global.buildDir + '/chzero'));
});

gulp.task('AES:deploy', function() {
  return gulp.src('vendor/AES/**/*.*')
    .pipe(gulp.dest(global.buildDir + '/AES'));
});

gulp.task('index:deploy', function() {
  return gulp.src('src/index.html')
    .pipe($.replace('../', ''))
    .pipe(gulp.dest(global.buildDir + ''));
});

gulp.task('cordova:deploy', function() {
  return gulp.src('cordova/**/*.*')
    .pipe(gulp.dest(global.buildDir + ''));
});