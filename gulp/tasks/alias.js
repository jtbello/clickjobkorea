'use strict';

var gulp = require('gulp-help')(require('gulp'));
var runSequence = require('run-sequence');

gulp.task('dev', function(cb) {
  return runSequence('build', 'serve', 'watch', cb);
});

gulp.task('deploy', ['build:deploy'], function(cb) {
  console.log('');
  console.log('Deployed to the build folder [' + global.buildDir + ']');
});