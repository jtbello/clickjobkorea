'use strict';

var fs = require('fs');
var path = require('path');
var merge = require('merge-stream');
var _ = require('lodash');

var gulp = require('gulp-help')(require('gulp'));
var $ = require('gulp-load-plugins')();

gulp.task('generate', ['clean:generate'], function() {
  var domains = _.without(getFolders('src/app'), 'smpl');
  var domainTasks = domains.map(function(domain) {
    var subdomains = getFolders(path.join('src/app', domain));
    var subdomainTasks = subdomains.map(function(subdomain) {
      var taskViews = generateViews(domain, subdomain);
      var taskSubdomains = generateSubdomain(domain, subdomain);
      return merge(taskSubdomains, taskViews);
    });
    var taskDomains = generateDomain(domain);
    return merge(taskDomains, subdomainTasks);
  });
  return merge(domainTasks);
});

function generateViews(domain, subdomain) {
  var subdomainPath = path.join('src/app', domain, subdomain);
  var views = getFolders(subdomainPath);
  return views.map(function(view) {
    var ctrlFilter = $.filter('**/*Ctrl.js');
    var tplFilter = $.filter('**/*.tpl.html');
    return gulp.src(['gulp/template/viewCtrl.js', 'gulp/template/view.tpl.html'])
      .pipe(ctrlFilter)
        .pipe($.template({
          moduleName: domain + '.' + subdomain,
          controllerName: view + 'Ctrl'
        }))
        .pipe($.rename(view + 'Ctrl.js'))
      .pipe(ctrlFilter.restore())
      .pipe(tplFilter)
        .pipe($.template({
          viewName: view
        }))
        .pipe($.rename(view + '.tpl.html'))
      .pipe(tplFilter.restore())
      .pipe(gulp.dest(path.join('.' + subdomainPath, view)));
  });
}

function generateSubdomain(domain, subdomain) {
  var subdomainPath = path.join('src/app', domain, subdomain);
  var views = getFolders(subdomainPath);
  var moduleFilter = $.filter('**/*.module.js');
  var ctrlFilter = $.filter('**/*Ctrl.js');
  return gulp.src(['gulp/template/subdomain.module.js', 'gulp/template/viewCtrl.js'])
    .pipe(moduleFilter)
      .pipe($.template({
        domain: domain,
        subdomain: subdomain,
        views: views
      }))
      .pipe($.rename(subdomain + '.module.js'))
    .pipe(moduleFilter.restore())
    .pipe(ctrlFilter)
      .pipe($.template({
        moduleName: domain + '.' + subdomain,
        controllerName: subdomain + 'Ctrl'
      }))
      .pipe($.rename(subdomain + 'Ctrl.js'))
    .pipe(ctrlFilter.restore())
    .pipe(gulp.dest('.' + subdomainPath));
}

function generateDomain(domain) {
  var domainPath = path.join('src/app', domain);
  var subdomains = getFolders(domainPath);
  var moduleFilter = $.filter('**/*.module.js');
  var ctrlFilter = $.filter('**/*Ctrl.js');
  return gulp.src(['gulp/template/domain.module.js', 'gulp/template/viewCtrl.js'])
    .pipe(moduleFilter)
      .pipe($.template({
        domain: domain,
        subdomains: subdomains
      }))
      .pipe($.rename(domain + '.module.js'))
    .pipe(moduleFilter.restore())
    .pipe(ctrlFilter)
      .pipe($.template({
        moduleName: domain,
        controllerName: domain + 'Ctrl'
      }))
      .pipe($.rename(domain + 'Ctrl.js'))
    .pipe(ctrlFilter.restore())
    .pipe(gulp.dest('.' + domainPath));
}

function getFolders(dir) {
  return fs.readdirSync(dir)
    .filter(function(file) {
      return fs.statSync(path.join(dir, file)).isDirectory();
    });
}