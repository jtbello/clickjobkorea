'use strict';

var gulp = require('gulp-help')(require('gulp'));
var $ = require('gulp-load-plugins')();
var $$ = $.loadUtils(['colors', 'env', 'log', 'pipeline']);

process.env.NODE_ENV = $$.env.target || 'development';
console.log('NODE_ENV', process.env.NODE_ENV);

global.buildDir = 'www';
// global.buildDir = 'ios/www';
switch(process.env.NODE_ENV) {
case 'staging':
  global.buildDir = 'www-stg'; 
  break;
case 'production':
  global.buildDir = 'www-prd'; 
  break;
case 'uat':
  global.buildDir = 'www-uat'; 
  break;
case 'edu':
  global.buildDir = 'www-edu'; 
  break;
}

require('require-dir')('./gulp/tasks');

gulp.task('default', ['dev']);

