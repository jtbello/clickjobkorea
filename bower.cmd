@IF EXIST "C:\ngsdev-front\tools\nodejs\node.exe" (
  "C:\ngsdev-front\tools\nodejs\node.exe"  "C:\ngsdev-front\workspace\easi-sfa-front\node_modules\bower\bin\bower" %*
) ELSE (
  @SETLOCAL
  @SET PATHEXT=%PATHEXT:;.JS;=;%
  node  "C:\ngsdev-front\workspace\easi-sfa-front\node_modules\bower\bin\bower" %*
)