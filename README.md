# Development Guides

## Files/Directory Structure
```
easi-sfa-front/
  assets/
  bower_components/
  gulp/
  node_modules/
  publish/
  src/
    api/
    app/
    common/
    core/
    public/
  vendor/
  bower.json
  gulpfile.js
  package.json
  README.md
```

## Style Guides

### Domain Module
```javascript
angular
  .module('smpl', [
    'smpl.guides',
    'smpl.boards',
    'smpl.subdomain'
  ])
  .config(routerConfig)
  .config(testConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('smpl', {
      url: '',
      abstract: true, // 컨텐츠 영역이 비어있는 경우 true 설정
      template: '<div ui-view>smpl</div>', // 컨테이너는 ui-view 필수
      controller: 'smplCtrl as smpl',
    });

  // state 전환 실패시 redirect
  $urlRouterProvider.otherwise('/guides');
}

function testConfig($ioProvider, $userProvider) {
  // Ajax 호출시 리모트 호스트
  $ioProvider.setHost('http://21.111.161.232:25201/service');
  $ioProvider.setHttpMethod('POST');
  // 임시 유저 정보
  // assets/data/users/{testUser}.json 파일명
  $userProvider.setTestUser('user');
}
```

### Sub Domain Module

```javascript
angular
  .module('smpl.guides', [])
  .config(routerConfig);

function routerConfig($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('smpl.guides', {
      url: '/guides',
      abstract: true,
      template: '<div ui-view>smpl.guides</div>',
      controller: 'guidesCtrl as guides',
    })
    .state('smpl.guides.tsid1001m', {
      url: '/tsid1001m',
      templateUrl: 'guides/tsid1001m/tsid1001m.tpl.html',
      controller: 'tsid1001mCtrl as tsid1001m'
    })
    .state('smpl.guides.tsid1002m', {
      url: '/tsid1002m',
      templateUrl: 'guides/tsid1002m/tsid1002m.tpl.html',
      controller: 'tsid1002mCtrl as tsid1002m'
    });

  // 특정 URL 접근시 redirect
  $urlRouterProvider.when('/guides', '/guides/tsid1001m');
}
```

### Controller

```javascript
angular
  .module('smpl.guides')
  .controller('guidesCtrl', guidesCtrl);

function guidesCtrl($log, $scope, SmplSvc) {
  /**
   * 지역 변수
   */
  var logger = $log(this);
  var vm = $log(this);
  var privateVar = 'Sample Guides';

  /**
   * 맴버 변수
   */
  vm.publicVar = privateVar;
  vm.sampleData = null;

  /**
   * 맴버 메소드
   */
  vm.publicMethod = myPublicMethod;

  /**
   * 초기화
   */
  init();

  function init() {
    logger.debug('init', vm);

    // $scope 소멸시 $destroy 이벤트 발생
    $scope.$on('$destroy', destroy);

    loadSampleData();
  }

  /**
   * 이벤트 핸들러
   */
  function destroy(event) {
    logger.debug(event);
  }

  /**
   * 사용자 정의 함수
   */
  function myPublicMethod() {
    // ...
  }

  function myPrivateMethod() {
    // ...
  }

  function loadSampleData() {
    SmplSvc.methodD()
      .then(function(result) {
        logger.debug(result);
        vm.sampleData = result;
      })
  }
}

```

### View

```html
<div>
	<h1>{{smpl.title}}</h1>
	<h2>{{guides.title}}</h2>
	<h3>{{tsid1001m.title}}</h3>
</div>
```

### Factory

```javascript
api.factory('SmplSvc', SmplSvc)

function SmplSvc($log, $q, $io) {
  var logger = $log('SmplSvc');

  return {
    methodA: methodA,
    methodB: methodB,
    methodC: methodC,
    methodD: methodD
  };

  function methodA() {
    return $io.api('/smpl/SmplSvc/methodA');
  }

  function methodB(data) {
    // data 가공 및 validation 처리
    if (!data.requiredVar) {
      return $q.reject('validation 실패');
    }

    return $io.api('/smpl/SmplSvc/methodB', data);
  }

  function methodC(data) {
    /**
     * $io.api(url, data, [config])
     * $http 대체 서비스
     */
    var config = {
      method: 'POST', // 'POST' (default)
      plain: false    // false (default) SSOLogin.login에서만 true
    }
    return $io.api('/smpl/SmplSvc/methodC', data, config)
      .then(function(response) {
        var backEndBody = response.serviceBody.data.BackEndBody[0];
        var naf = backEndBody.naf;
        if (naf) {
          // 정상 데이터가 아닌 경우 reject
          return $q.reject(naf);
        }
        return backEndBody.user;
      });
  }

  function methodD(data) {
    /**
     * $io.user(url, data, [config])
     * 내부에서 $io.api()를 호출
     * response.serviceBody.data.BackEndBody[0].user 객체 반환
     */
    return $io.user('/smpl/SmplSvc/methodD', data)
      .then(function(result) {
        /**
         * result.$response
         * serviceBody, serviceHeader 포함하는 response 원형
         */
        logger.debug(result.$response);

        return result;
      });
  }
}
```

## Custom Service

### $log

### $io

### $user

### $storage

### $modal
