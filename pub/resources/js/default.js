/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
*/
//jQuery.easing.jswing=jQuery.easing.swing;jQuery.extend(jQuery.easing,{def:"easeOutQuad",swing:function(e,f,a,h,g){return jQuery.easing[jQuery.easing.def](e,f,a,h,g)},easeInQuad:function(e,f,a,h,g){return h*(f/=g)*f+a},easeOutQuad:function(e,f,a,h,g){return -h*(f/=g)*(f-2)+a},easeInOutQuad:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f+a}return -h/2*((--f)*(f-2)-1)+a},easeInCubic:function(e,f,a,h,g){return h*(f/=g)*f*f+a},easeOutCubic:function(e,f,a,h,g){return h*((f=f/g-1)*f*f+1)+a},easeInOutCubic:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f+a}return h/2*((f-=2)*f*f+2)+a},easeInQuart:function(e,f,a,h,g){return h*(f/=g)*f*f*f+a},easeOutQuart:function(e,f,a,h,g){return -h*((f=f/g-1)*f*f*f-1)+a},easeInOutQuart:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f+a}return -h/2*((f-=2)*f*f*f-2)+a},easeInQuint:function(e,f,a,h,g){return h*(f/=g)*f*f*f*f+a},easeOutQuint:function(e,f,a,h,g){return h*((f=f/g-1)*f*f*f*f+1)+a},easeInOutQuint:function(e,f,a,h,g){if((f/=g/2)<1){return h/2*f*f*f*f*f+a}return h/2*((f-=2)*f*f*f*f+2)+a},easeInSine:function(e,f,a,h,g){return -h*Math.cos(f/g*(Math.PI/2))+h+a},easeOutSine:function(e,f,a,h,g){return h*Math.sin(f/g*(Math.PI/2))+a},easeInOutSine:function(e,f,a,h,g){return -h/2*(Math.cos(Math.PI*f/g)-1)+a},easeInExpo:function(e,f,a,h,g){return(f==0)?a:h*Math.pow(2,10*(f/g-1))+a},easeOutExpo:function(e,f,a,h,g){return(f==g)?a+h:h*(-Math.pow(2,-10*f/g)+1)+a},easeInOutExpo:function(e,f,a,h,g){if(f==0){return a}if(f==g){return a+h}if((f/=g/2)<1){return h/2*Math.pow(2,10*(f-1))+a}return h/2*(-Math.pow(2,-10*--f)+2)+a},easeInCirc:function(e,f,a,h,g){return -h*(Math.sqrt(1-(f/=g)*f)-1)+a},easeOutCirc:function(e,f,a,h,g){return h*Math.sqrt(1-(f=f/g-1)*f)+a},easeInOutCirc:function(e,f,a,h,g){if((f/=g/2)<1){return -h/2*(Math.sqrt(1-f*f)-1)+a}return h/2*(Math.sqrt(1-(f-=2)*f)+1)+a},easeInElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return -(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e},easeOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k)==1){return e+l}if(!j){j=k*0.3}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}return g*Math.pow(2,-10*h)*Math.sin((h*k-i)*(2*Math.PI)/j)+l+e},easeInOutElastic:function(f,h,e,l,k){var i=1.70158;var j=0;var g=l;if(h==0){return e}if((h/=k/2)==2){return e+l}if(!j){j=k*(0.3*1.5)}if(g<Math.abs(l)){g=l;var i=j/4}else{var i=j/(2*Math.PI)*Math.asin(l/g)}if(h<1){return -0.5*(g*Math.pow(2,10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j))+e}return g*Math.pow(2,-10*(h-=1))*Math.sin((h*k-i)*(2*Math.PI)/j)*0.5+l+e},easeInBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*(f/=h)*f*((g+1)*f-g)+a},easeOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}return i*((f=f/h-1)*f*((g+1)*f+g)+1)+a},easeInOutBack:function(e,f,a,i,h,g){if(g==undefined){g=1.70158}if((f/=h/2)<1){return i/2*(f*f*(((g*=(1.525))+1)*f-g))+a}return i/2*((f-=2)*f*(((g*=(1.525))+1)*f+g)+2)+a},easeInBounce:function(e,f,a,h,g){return h-jQuery.easing.easeOutBounce(e,g-f,0,h,g)+a},easeOutBounce:function(e,f,a,h,g){if((f/=g)<(1/2.75)){return h*(7.5625*f*f)+a}else{if(f<(2/2.75)){return h*(7.5625*(f-=(1.5/2.75))*f+0.75)+a}else{if(f<(2.5/2.75)){return h*(7.5625*(f-=(2.25/2.75))*f+0.9375)+a}else{return h*(7.5625*(f-=(2.625/2.75))*f+0.984375)+a}}}},easeInOutBounce:function(e,f,a,h,g){if(f<g/2){return jQuery.easing.easeInBounce(e,f*2,0,h,g)*0.5+a}return jQuery.easing.easeOutBounce(e,f*2-g,0,h,g)*0.5+h*0.5+a}});


/*!
  * Bonzo: DOM Utility (c) Dustin Diaz 2012
  * https://github.com/ded/bonzo
  * License MIT
  */
//(function(e,t,n){typeof module!="undefined"&&module.exports?module.exports=n():typeof define=="function"&&define.amd?define(n):t[e]=n()})("bonzo",this,function(){function L(e,n){var r=null,i=t.defaultView.getComputedStyle(e,"");return i&&(r=i[n]),e.style[n]||r}function A(e){return e&&e.nodeName&&(e.nodeType==1||e.nodeType==11)}function O(e,t,n){var r,i,s;if(typeof e=="string")return Q.create(e);A(e)&&(e=[e]);if(n){s=[];for(r=0,i=e.length;r<i;r++)s[r]=V(t,e[r]);return s}return e}function M(e){return new RegExp("(^|\\s+)"+e+"(\\s+|$)")}function _(e,t,n,r){var i,s=0,o=e.length;for(;s<o;s++)i=r?e.length-s-1:s,t.call(n||e[i],e[i],i,e);return e}function D(e,t,n){for(var r=0,i=e.length;r<i;r++)A(e[r])&&(D(e[r].childNodes,t,n),t.call(n||e[r],e[r],r,e));return e}function P(e){return e.replace(/-(.)/g,function(e,t){return t.toUpperCase()})}function H(e){return e?e.replace(/([a-z])([A-Z])/g,"$1-$2").toLowerCase():e}function B(e){e[S]("data-node-uid")||e[E]("data-node-uid",++g);var t=e[S]("data-node-uid");return m[t]||(m[t]={})}function j(e){var t=e[S]("data-node-uid");t&&delete m[t]}function F(e){var t;try{return e===null||e===undefined?undefined:e==="true"?!0:e==="false"?!1:e==="null"?null:(t=parseFloat(e))==e?t:e}catch(n){}return undefined}function I(e,t,n){for(var r=0,i=e.length;r<i;++r)if(t.call(n||null,e[r],r,e))return!0;return!1}function q(e){return e=="transform"&&(e=x.transform)||/^transform-?[Oo]rigin$/.test(e)&&(e=x.transform+"Origin"),e?P(e):null}function R(e,t,n,r){var i=0,s=t||this,o=[],u=k&&typeof e=="string"&&e.charAt(0)!="<"?k(e):e;return _(O(u),function(e,t){_(s,function(r){n(e,o[i++]=t>0?V(s,r):r)},null,r)},this,r),s.length=i,_(o,function(e){s[--i]=e},null,!r),s}function U(e,t,n){var r=Q(e),i=r.css("position"),s=r.offset(),o="relative",u=i==o,a=[parseInt(r.css("left"),10),parseInt(r.css("top"),10)];i=="static"&&(r.css("position",o),i=o),isNaN(a[0])&&(a[0]=u?0:e.offsetLeft),isNaN(a[1])&&(a[1]=u?0:e.offsetTop),t!=null&&(e.style.left=t-s.left+a[0]+w),n!=null&&(e.style.top=n-s.top+a[1]+w)}function z(e,t){return typeof t=="function"?t.call(e,e):t}function W(t,n,r){var i=this[0];return i?t==null&&n==null?($(i)?J():{x:i.scrollLeft,y:i.scrollTop})[r]:($(i)?e.scrollTo(t,n):(t!=null&&(i.scrollLeft=t),n!=null&&(i.scrollTop=n)),this):this}function X(e){this.length=0;if(e){e=typeof e!="string"&&!e.nodeType&&typeof e.length!="undefined"?e:[e],this.length=e.length;for(var t=0;t<e.length;t++)this[t]=e[t]}}function V(e,t){var n=t.cloneNode(!0),r,i,s;if(e.$&&typeof e.cloneEvents=="function"){e.$(n).cloneEvents(t),r=e.$(n).find("*"),i=e.$(t).find("*");for(s=0;s<i.length;s++)e.$(r[s]).cloneEvents(i[s])}return n}function $(t){return t===e||/^(?:body|html)$/i.test(t.tagName)}function J(){return{x:e.pageXOffset||n.scrollLeft,y:e.pageYOffset||n.scrollTop}}function K(e){var t=document.createElement("script"),n=e.match(o);return t.src=n[1],t}function Q(e){return new X(e)}var e=window,t=e.document,n=t.documentElement,r="parentNode",i=/^(checked|value|selected|disabled)$/i,s=/^(select|fieldset|table|tbody|tfoot|td|tr|colgroup)$/i,o=/\s*<script +src=['"]([^'"]+)['"]>/,u=["<table>","</table>",1],a=["<table><tbody><tr>","</tr></tbody></table>",3],f=["<select>","</select>",1],l=["_","",0,1],c={thead:u,tbody:u,tfoot:u,colgroup:u,caption:u,tr:["<table><tbody>","</tbody></table>",2],th:a,td:a,col:["<table><colgroup>","</colgroup></table>",2],fieldset:["<form>","</form>",1],legend:["<form><fieldset>","</fieldset></form>",2],option:f,optgroup:f,script:l,style:l,link:l,param:l,base:l},h=/^(checked|selected|disabled)$/,p,d,v,m={},g=0,y=/^-?[\d\.]+$/,b=/^data-(.+)$/,w="px",E="setAttribute",S="getAttribute",x=function(){var e=t.createElement("p");return{transform:function(){var t=["transform","webkitTransform","MozTransform","OTransform","msTransform"],n;for(n=0;n<t.length;n++)if(t[n]in e.style)return t[n]}(),classList:"classList"in e}}(),T=/\s+/,N=String.prototype.toString,C={lineHeight:1,zoom:1,zIndex:1,opacity:1,boxFlex:1,WebkitBoxFlex:1,MozBoxFlex:1},k=t.querySelectorAll&&function(e){return t.querySelectorAll(e)};return x.classList?(p=function(e,t){return e.classList.contains(t)},d=function(e,t){e.classList.add(t)},v=function(e,t){e.classList.remove(t)}):(p=function(e,t){return M(t).test(e.className)},d=function(e,t){e.className=(e.className+" "+t).trim()},v=function(e,t){e.className=e.className.replace(M(t)," ").trim()}),X.prototype={get:function(e){return this[e]||null},each:function(e,t){return _(this,e,t)},deepEach:function(e,t){return D(this,e,t)},map:function(e,t){var n=[],r,i;for(i=0;i<this.length;i++)r=e.call(this,this[i],i),t?t(r)&&n.push(r):n.push(r);return n},html:function(e,t){var n=t?"textContent":"innerHTML",r=this,i=function(t,n){_(O(e,r,n),function(e){t.appendChild(e)})},o=function(r,o){try{if(t||typeof e=="string"&&!s.test(r.tagName))return r[n]=e}catch(u){}i(r,o)};return typeof e!="undefined"?this.empty().each(o):this[0]?this[0][n]:""},text:function(e){return this.html(e,!0)},append:function(e){var t=this;return this.each(function(n,r){_(O(e,t,r),function(e){n.appendChild(e)})})},prepend:function(e){var t=this;return this.each(function(n,r){var i=n.firstChild;_(O(e,t,r),function(e){n.insertBefore(e,i)})})},appendTo:function(e,t){return R.call(this,e,t,function(e,t){e.appendChild(t)})},prependTo:function(e,t){return R.call(this,e,t,function(e,t){e.insertBefore(t,e.firstChild)},1)},before:function(e){var t=this;return this.each(function(n,i){_(O(e,t,i),function(e){n[r].insertBefore(e,n)})})},after:function(e){var t=this;return this.each(function(n,i){_(O(e,t,i),function(e){n[r].insertBefore(e,n.nextSibling)},null,1)})},insertBefore:function(e,t){return R.call(this,e,t,function(e,t){e[r].insertBefore(t,e)})},insertAfter:function(e,t){return R.call(this,e,t,function(e,t){var n=e.nextSibling;n?e[r].insertBefore(t,n):e[r].appendChild(t)},1)},replaceWith:function(e){var t=this;return this.each(function(n,i){_(O(e,t,i),function(e){n[r]&&n[r].replaceChild(e,n)})})},clone:function(e){var t=[],n,r;for(r=0,n=this.length;r<n;r++)t[r]=V(e||this,this[r]);return Q(t)},addClass:function(e){return e=N.call(e).split(T),this.each(function(t){_(e,function(e){e&&!p(t,z(t,e))&&d(t,z(t,e))})})},removeClass:function(e){return e=N.call(e).split(T),this.each(function(t){_(e,function(e){e&&p(t,z(t,e))&&v(t,z(t,e))})})},hasClass:function(e){return e=N.call(e).split(T),I(this,function(t){return I(e,function(e){return e&&p(t,e)})})},toggleClass:function(e,t){return e=N.call(e).split(T),this.each(function(n){_(e,function(e){e&&(typeof t!="undefined"?t?!p(n,e)&&d(n,e):v(n,e):p(n,e)?v(n,e):d(n,e))})})},show:function(e){return e=typeof e=="string"?e:"",this.each(function(t){t.style.display=e})},hide:function(){return this.each(function(e){e.style.display="none"})},toggle:function(e,t){return t=typeof t=="string"?t:"",typeof e!="function"&&(e=null),this.each(function(n){n.style.display=n.offsetWidth||n.offsetHeight?"none":t,e&&e.call(n)})},first:function(){return Q(this.length?this[0]:[])},last:function(){return Q(this.length?this[this.length-1]:[])},next:function(){return this.related("nextSibling")},previous:function(){return this.related("previousSibling")},parent:function(){return this.related(r)},related:function(e){return Q(this.map(function(t){t=t[e];while(t&&t.nodeType!==1)t=t[e];return t||0},function(e){return e}))},focus:function(){return this.length&&this[0].focus(),this},blur:function(){return this.length&&this[0].blur(),this},css:function(n,r){function o(e,t,n){for(var r in s)if(s.hasOwnProperty(r)){n=s[r],(t=q(r))&&y.test(n)&&!(t in C)&&(n+=w);try{e.style[t]=z(e,n)}catch(i){}}}var i,s=n;return r===undefined&&typeof n=="string"?(r=this[0],r?r===t||r===e?(i=r===t?Q.doc():Q.viewport(),n=="width"?i.width:n=="height"?i.height:""):(n=q(n))?L(r,n):null:null):(typeof n=="string"&&(s={},s[n]=r),this.each(o))},offset:function(e,n){if(!e||typeof e!="object"||typeof e.top!="number"&&typeof e.left!="number"){if(typeof e=="number"||typeof n=="number")return this.each(function(t){U(t,e,n)});if(!this[0])return{top:0,left:0,height:0,width:0};var r=this[0],i=r.ownerDocument.documentElement,s=r.getBoundingClientRect(),o=J(),u=r.offsetWidth,a=r.offsetHeight,f=s.top+o.y-Math.max(0,i&&i.clientTop,t.body.clientTop),l=s.left+o.x-Math.max(0,i&&i.clientLeft,t.body.clientLeft);return{top:f,left:l,height:a,width:u}}return this.each(function(t){U(t,e.left,e.top)})},dim:function(){if(!this.length)return{height:0,width:0};var e=this[0],t=e.nodeType==9&&e.documentElement,n=!t&&!!e.style&&!e.offsetWidth&&!e.offsetHeight?function(t){var n={position:e.style.position||"",visibility:e.style.visibility||"",display:e.style.display||""};return t.first().css({position:"absolute",visibility:"hidden",display:"block"}),n}(this):null,r=t?Math.max(e.body.scrollWidth,e.body.offsetWidth,t.scrollWidth,t.offsetWidth,t.clientWidth):e.offsetWidth,i=t?Math.max(e.body.scrollHeight,e.body.offsetHeight,t.scrollHeight,t.offsetHeight,t.clientHeight):e.offsetHeight;return n&&this.first().css(n),{height:i,width:r}},attr:function(e,t){var n=this[0],r;if(typeof e=="string"||e instanceof String)return typeof t=="undefined"?n?i.test(e)?h.test(e)&&typeof n[e]=="string"?!0:n[e]:n[S](e):null:this.each(function(n){i.test(e)?n[e]=z(n,t):n[E](e,z(n,t))});for(r in e)e.hasOwnProperty(r)&&this.attr(r,e[r]);return this},removeAttr:function(e){return this.each(function(t){h.test(e)?t[e]=!1:t.removeAttribute(e)})},val:function(e){return typeof e=="string"||typeof e=="number"?this.attr("value",e):this.length?this[0].value:null},data:function(e,t){var n=this[0],r,i;return typeof t=="undefined"?n?(r=B(n),typeof e=="undefined"?(_(n.attributes,function(e){(i=(""+e.name).match(b))&&(r[P(i[1])]=F(e.value))}),r):(typeof r[e]=="undefined"&&(r[e]=F(this.attr("data-"+H(e)))),r[e])):null:this.each(function(n){B(n)[e]=t})},remove:function(){return this.deepEach(j),this.detach()},empty:function(){return this.each(function(e){D(e.childNodes,j);while(e.firstChild)e.removeChild(e.firstChild)})},detach:function(){return this.each(function(e){e[r]&&e[r].removeChild(e)})},scrollTop:function(e){return W.call(this,null,e,"y")},scrollLeft:function(e){return W.call(this,e,null,"x")}},Q.setQueryEngine=function(e){k=e,delete Q.setQueryEngine},Q.aug=function(e,t){for(var n in e)e.hasOwnProperty(n)&&((t||X.prototype)[n]=e[n])},Q.create=function(e){return typeof e=="string"&&e!==""?function(){if(o.test(e))return[K(e)];var n=e.match(/^\s*<([^\s>]+)/),i=t.createElement("div"),s=[],u=n?c[n[1].toLowerCase()]:null,a=u?u[2]+1:1,f=u&&u[3],l=r;i.innerHTML=u?u[0]+e+u[1]:e;while(a--)i=i.firstChild;f&&i&&i.nodeType!==1&&(i=i.nextSibling);do(!n||i.nodeType==1)&&s.push(i);while(i=i.nextSibling);return _(s,function(e){e[l]&&e[l].removeChild(e)}),s}():A(e)?[e.cloneNode(!0)]:[]},Q.doc=function(){var e=Q.viewport();return{width:Math.max(t.body.scrollWidth,n.scrollWidth,e.width),height:Math.max(t.body.scrollHeight,n.scrollHeight,e.height)}},Q.firstChild=function(e){for(var t=e.childNodes,n=0,r=t&&t.length||0,i;n<r;n++)t[n].nodeType===1&&(i=t[r=n]);return i},Q.viewport=function(){return{width:e.innerWidth,height:e.innerHeight}},Q.isAncestor="compareDocumentPosition"in n?function(e,t){return(e.compareDocumentPosition(t)&16)==16}:function(e,t){return e!==t&&e.contains(t)},Q});


/*!
Waypoints - 4.0.1
Copyright © 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
//!function(){"use strict";function t(o){if(!o)throw new Error("No options passed to Waypoint constructor");if(!o.element)throw new Error("No element option passed to Waypoint constructor");if(!o.handler)throw new Error("No handler option passed to Waypoint constructor");this.key="waypoint-"+e,this.options=t.Adapter.extend({},t.defaults,o),this.element=this.options.element,this.adapter=new t.Adapter(this.element),this.callback=o.handler,this.axis=this.options.horizontal?"horizontal":"vertical",this.enabled=this.options.enabled,this.triggerPoint=null,this.group=t.Group.findOrCreate({name:this.options.group,axis:this.axis}),this.context=t.Context.findOrCreateByElement(this.options.context),t.offsetAliases[this.options.offset]&&(this.options.offset=t.offsetAliases[this.options.offset]),this.group.add(this),this.context.add(this),i[this.key]=this,e+=1}var e=0,i={};t.prototype.queueTrigger=function(t){this.group.queueTrigger(this,t)},t.prototype.trigger=function(t){this.enabled&&this.callback&&this.callback.apply(this,t)},t.prototype.destroy=function(){this.context.remove(this),this.group.remove(this),delete i[this.key]},t.prototype.disable=function(){return this.enabled=!1,this},t.prototype.enable=function(){return this.context.refresh(),this.enabled=!0,this},t.prototype.next=function(){return this.group.next(this)},t.prototype.previous=function(){return this.group.previous(this)},t.invokeAll=function(t){var e=[];for(var o in i)e.push(i[o]);for(var n=0,r=e.length;r>n;n++)e[n][t]()},t.destroyAll=function(){t.invokeAll("destroy")},t.disableAll=function(){t.invokeAll("disable")},t.enableAll=function(){t.Context.refreshAll();for(var e in i)i[e].enabled=!0;return this},t.refreshAll=function(){t.Context.refreshAll()},t.viewportHeight=function(){return window.innerHeight||document.documentElement.clientHeight},t.viewportWidth=function(){return document.documentElement.clientWidth},t.adapters=[],t.defaults={context:window,continuous:!0,enabled:!0,group:"default",horizontal:!1,offset:0},t.offsetAliases={"bottom-in-view":function(){return this.context.innerHeight()-this.adapter.outerHeight()},"right-in-view":function(){return this.context.innerWidth()-this.adapter.outerWidth()}},window.Waypoint=t}(),function(){"use strict";function t(t){window.setTimeout(t,1e3/60)}function e(t){this.element=t,this.Adapter=n.Adapter,this.adapter=new this.Adapter(t),this.key="waypoint-context-"+i,this.didScroll=!1,this.didResize=!1,this.oldScroll={x:this.adapter.scrollLeft(),y:this.adapter.scrollTop()},this.waypoints={vertical:{},horizontal:{}},t.waypointContextKey=this.key,o[t.waypointContextKey]=this,i+=1,n.windowContext||(n.windowContext=!0,n.windowContext=new e(window)),this.createThrottledScrollHandler(),this.createThrottledResizeHandler()}var i=0,o={},n=window.Waypoint,r=window.onload;e.prototype.add=function(t){var e=t.options.horizontal?"horizontal":"vertical";this.waypoints[e][t.key]=t,this.refresh()},e.prototype.checkEmpty=function(){var t=this.Adapter.isEmptyObject(this.waypoints.horizontal),e=this.Adapter.isEmptyObject(this.waypoints.vertical),i=this.element==this.element.window;t&&e&&!i&&(this.adapter.off(".waypoints"),delete o[this.key])},e.prototype.createThrottledResizeHandler=function(){function t(){e.handleResize(),e.didResize=!1}var e=this;this.adapter.on("resize.waypoints",function(){e.didResize||(e.didResize=!0,n.requestAnimationFrame(t))})},e.prototype.createThrottledScrollHandler=function(){function t(){e.handleScroll(),e.didScroll=!1}var e=this;this.adapter.on("scroll.waypoints",function(){(!e.didScroll||n.isTouch)&&(e.didScroll=!0,n.requestAnimationFrame(t))})},e.prototype.handleResize=function(){n.Context.refreshAll()},e.prototype.handleScroll=function(){var t={},e={horizontal:{newScroll:this.adapter.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.adapter.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};for(var i in e){var o=e[i],n=o.newScroll>o.oldScroll,r=n?o.forward:o.backward;for(var s in this.waypoints[i]){var a=this.waypoints[i][s];if(null!==a.triggerPoint){var l=o.oldScroll<a.triggerPoint,h=o.newScroll>=a.triggerPoint,p=l&&h,u=!l&&!h;(p||u)&&(a.queueTrigger(r),t[a.group.id]=a.group)}}}for(var c in t)t[c].flushTriggers();this.oldScroll={x:e.horizontal.newScroll,y:e.vertical.newScroll}},e.prototype.innerHeight=function(){return this.element==this.element.window?n.viewportHeight():this.adapter.innerHeight()},e.prototype.remove=function(t){delete this.waypoints[t.axis][t.key],this.checkEmpty()},e.prototype.innerWidth=function(){return this.element==this.element.window?n.viewportWidth():this.adapter.innerWidth()},e.prototype.destroy=function(){var t=[];for(var e in this.waypoints)for(var i in this.waypoints[e])t.push(this.waypoints[e][i]);for(var o=0,n=t.length;n>o;o++)t[o].destroy()},e.prototype.refresh=function(){var t,e=this.element==this.element.window,i=e?void 0:this.adapter.offset(),o={};this.handleScroll(),t={horizontal:{contextOffset:e?0:i.left,contextScroll:e?0:this.oldScroll.x,contextDimension:this.innerWidth(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:e?0:i.top,contextScroll:e?0:this.oldScroll.y,contextDimension:this.innerHeight(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};for(var r in t){var s=t[r];for(var a in this.waypoints[r]){var l,h,p,u,c,d=this.waypoints[r][a],f=d.options.offset,w=d.triggerPoint,y=0,g=null==w;d.element!==d.element.window&&(y=d.adapter.offset()[s.offsetProp]),"function"==typeof f?f=f.apply(d):"string"==typeof f&&(f=parseFloat(f),d.options.offset.indexOf("%")>-1&&(f=Math.ceil(s.contextDimension*f/100))),l=s.contextScroll-s.contextOffset,d.triggerPoint=Math.floor(y+l-f),h=w<s.oldScroll,p=d.triggerPoint>=s.oldScroll,u=h&&p,c=!h&&!p,!g&&u?(d.queueTrigger(s.backward),o[d.group.id]=d.group):!g&&c?(d.queueTrigger(s.forward),o[d.group.id]=d.group):g&&s.oldScroll>=d.triggerPoint&&(d.queueTrigger(s.forward),o[d.group.id]=d.group)}}return n.requestAnimationFrame(function(){for(var t in o)o[t].flushTriggers()}),this},e.findOrCreateByElement=function(t){return e.findByElement(t)||new e(t)},e.refreshAll=function(){for(var t in o)o[t].refresh()},e.findByElement=function(t){return o[t.waypointContextKey]},window.onload=function(){r&&r(),e.refreshAll()},n.requestAnimationFrame=function(e){var i=window.requestAnimationFrame||window.mozRequestAnimationFrame||window.webkitRequestAnimationFrame||t;i.call(window,e)},n.Context=e}(),function(){"use strict";function t(t,e){return t.triggerPoint-e.triggerPoint}function e(t,e){return e.triggerPoint-t.triggerPoint}function i(t){this.name=t.name,this.axis=t.axis,this.id=this.name+"-"+this.axis,this.waypoints=[],this.clearTriggerQueues(),o[this.axis][this.name]=this}var o={vertical:{},horizontal:{}},n=window.Waypoint;i.prototype.add=function(t){this.waypoints.push(t)},i.prototype.clearTriggerQueues=function(){this.triggerQueues={up:[],down:[],left:[],right:[]}},i.prototype.flushTriggers=function(){for(var i in this.triggerQueues){var o=this.triggerQueues[i],n="up"===i||"left"===i;o.sort(n?e:t);for(var r=0,s=o.length;s>r;r+=1){var a=o[r];(a.options.continuous||r===o.length-1)&&a.trigger([i])}}this.clearTriggerQueues()},i.prototype.next=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints),o=i===this.waypoints.length-1;return o?null:this.waypoints[i+1]},i.prototype.previous=function(e){this.waypoints.sort(t);var i=n.Adapter.inArray(e,this.waypoints);return i?this.waypoints[i-1]:null},i.prototype.queueTrigger=function(t,e){this.triggerQueues[e].push(t)},i.prototype.remove=function(t){var e=n.Adapter.inArray(t,this.waypoints);e>-1&&this.waypoints.splice(e,1)},i.prototype.first=function(){return this.waypoints[0]},i.prototype.last=function(){return this.waypoints[this.waypoints.length-1]},i.findOrCreate=function(t){return o[t.axis][t.name]||new i(t)},n.Group=i}(),function(){"use strict";function t(t){this.$element=e(t)}var e=window.jQuery,i=window.Waypoint;e.each(["innerHeight","innerWidth","off","offset","on","outerHeight","outerWidth","scrollLeft","scrollTop"],function(e,i){t.prototype[i]=function(){var t=Array.prototype.slice.call(arguments);return this.$element[i].apply(this.$element,t)}}),e.each(["extend","inArray","isEmptyObject"],function(i,o){t[o]=e[o]}),i.adapters.push({name:"jquery",Adapter:t}),i.Adapter=t}(),function(){"use strict";function t(t){return function(){var i=[],o=arguments[0];return t.isFunction(arguments[0])&&(o=t.extend({},arguments[1]),o.handler=arguments[0]),this.each(function(){var n=t.extend({},o,{element:this});"string"==typeof n.context&&(n.context=t(this).closest(n.context)[0]),i.push(new e(n))}),i}}var e=window.Waypoint;window.jQuery&&(window.jQuery.fn.waypoint=t(window.jQuery)),window.Zepto&&(window.Zepto.fn.waypoint=t(window.Zepto))}();

/*!
Waypoints Infinite Scroll Shortcut - 4.0.1
Copyright © 2011-2016 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/waypoints/blob/master/licenses.txt
*/
//!function(){"use strict";function t(n){this.options=i.extend({},t.defaults,n),this.container=this.options.element,"auto"!==this.options.container&&(this.container=this.options.container),this.$container=i(this.container),this.$more=i(this.options.more),this.$more.length&&(this.setupHandler(),this.waypoint=new o(this.options))}var i=window.jQuery,o=window.Waypoint;t.prototype.setupHandler=function(){this.options.handler=i.proxy(function(){this.options.onBeforePageLoad(),this.destroy(),this.$container.addClass(this.options.loadingClass),i.get(i(this.options.more).attr("href"),i.proxy(function(t){var n=i(i.parseHTML(t)),e=n.find(this.options.more),s=n.find(this.options.items);s.length||(s=n.filter(this.options.items)),this.$container.append(s),this.$container.removeClass(this.options.loadingClass),e.length||(e=n.filter(this.options.more)),e.length?(this.$more.replaceWith(e),this.$more=e,this.waypoint=new o(this.options)):this.$more.remove(),this.options.onAfterPageLoad(s)},this))},this)},t.prototype.destroy=function(){this.waypoint&&this.waypoint.destroy()},t.defaults={container:"auto",items:".infinite-item",more:".infinite-more-link",offset:"bottom-in-view",loadingClass:"infinite-loading",onBeforePageLoad:i.noop,onAfterPageLoad:i.noop},o.Infinite=t}();

//!function(t,i,s){function e(s,e){this.wrapper="string"==typeof s?i.querySelector(s):s,this.scroller=this.wrapper.children[0],this.scrollerStyle=this.scroller.style,this.options={resizeScrollbars:!0,mouseWheelSpeed:20,snapThreshold:.334,disablePointer:!h.hasPointer,disableTouch:h.hasPointer||!h.hasTouch,disableMouse:h.hasPointer||h.hasTouch,startX:0,startY:0,scrollY:!0,directionLockThreshold:5,momentum:!0,bounce:!0,bounceTime:600,bounceEasing:"",preventDefault:!0,preventDefaultException:{tagName:/^(INPUT|TEXTAREA|BUTTON|SELECT)$/},HWCompositing:!0,useTransition:!0,useTransform:!0,bindToWrapper:"undefined"==typeof t.onmousedown};for(var o in e)this.options[o]=e[o];this.translateZ=this.options.HWCompositing&&h.hasPerspective?" translateZ(0)":"",this.options.useTransition=h.hasTransition&&this.options.useTransition,this.options.useTransform=h.hasTransform&&this.options.useTransform,this.options.eventPassthrough=this.options.eventPassthrough===!0?"vertical":this.options.eventPassthrough,this.options.preventDefault=!this.options.eventPassthrough&&this.options.preventDefault,this.options.scrollY="vertical"==this.options.eventPassthrough?!1:this.options.scrollY,this.options.scrollX="horizontal"==this.options.eventPassthrough?!1:this.options.scrollX,this.options.freeScroll=this.options.freeScroll&&!this.options.eventPassthrough,this.options.directionLockThreshold=this.options.eventPassthrough?0:this.options.directionLockThreshold,this.options.bounceEasing="string"==typeof this.options.bounceEasing?h.ease[this.options.bounceEasing]||h.ease.circular:this.options.bounceEasing,this.options.resizePolling=void 0===this.options.resizePolling?60:this.options.resizePolling,this.options.tap===!0&&(this.options.tap="tap"),"scale"==this.options.shrinkScrollbars&&(this.options.useTransition=!1),this.options.invertWheelDirection=this.options.invertWheelDirection?-1:1,this.x=0,this.y=0,this.directionX=0,this.directionY=0,this._events={},this._init(),this.refresh(),this.scrollTo(this.options.startX,this.options.startY),this.enable()}function o(t,s,e){var o=i.createElement("div"),n=i.createElement("div");return e===!0&&(o.style.cssText="position:absolute;z-index:9999",n.style.cssText="-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;background:rgba(0,0,0,0.5);border:1px solid rgba(255,255,255,0.9);border-radius:3px"),n.className="iScrollIndicator","h"==t?(e===!0&&(o.style.cssText+=";height:7px;left:2px;right:2px;bottom:0",n.style.height="100%"),o.className="iScrollHorizontalScrollbar"):(e===!0&&(o.style.cssText+=";width:7px;bottom:2px;top:2px;right:1px",n.style.width="100%"),o.className="iScrollVerticalScrollbar"),o.style.cssText+=";overflow:hidden",s||(o.style.pointerEvents="none"),o.appendChild(n),o}function n(s,e){this.wrapper="string"==typeof e.el?i.querySelector(e.el):e.el,this.wrapperStyle=this.wrapper.style,this.indicator=this.wrapper.children[0],this.indicatorStyle=this.indicator.style,this.scroller=s,this.options={listenX:!0,listenY:!0,interactive:!1,resize:!0,defaultScrollbars:!1,shrink:!1,fade:!1,speedRatioX:0,speedRatioY:0};for(var o in e)this.options[o]=e[o];if(this.sizeRatioX=1,this.sizeRatioY=1,this.maxPosX=0,this.maxPosY=0,this.options.interactive&&(this.options.disableTouch||(h.addEvent(this.indicator,"touchstart",this),h.addEvent(t,"touchend",this)),this.options.disablePointer||(h.addEvent(this.indicator,h.prefixPointerEvent("pointerdown"),this),h.addEvent(t,h.prefixPointerEvent("pointerup"),this)),this.options.disableMouse||(h.addEvent(this.indicator,"mousedown",this),h.addEvent(t,"mouseup",this))),this.options.fade){this.wrapperStyle[h.style.transform]=this.scroller.translateZ;var n=h.style.transitionDuration;this.wrapperStyle[n]=h.isBadAndroid?"0.0001ms":"0ms";var a=this;h.isBadAndroid&&r(function(){"0.0001ms"===a.wrapperStyle[n]&&(a.wrapperStyle[n]="0s")}),this.wrapperStyle.opacity="0"}}var r=t.requestAnimationFrame||t.webkitRequestAnimationFrame||t.mozRequestAnimationFrame||t.oRequestAnimationFrame||t.msRequestAnimationFrame||function(i){t.setTimeout(i,1e3/60)},h=function(){function e(t){return r===!1?!1:""===r?t:r+t.charAt(0).toUpperCase()+t.substr(1)}var o={},n=i.createElement("div").style,r=function(){for(var t,i=["t","webkitT","MozT","msT","OT"],s=0,e=i.length;e>s;s++)if(t=i[s]+"ransform",t in n)return i[s].substr(0,i[s].length-1);return!1}();o.getTime=Date.now||function(){return(new Date).getTime()},o.extend=function(t,i){for(var s in i)t[s]=i[s]},o.addEvent=function(t,i,s,e){t.addEventListener(i,s,!!e)},o.removeEvent=function(t,i,s,e){t.removeEventListener(i,s,!!e)},o.prefixPointerEvent=function(i){return t.MSPointerEvent?"MSPointer"+i.charAt(7).toUpperCase()+i.substr(8):i},o.momentum=function(t,i,e,o,n,r){var h,a,l=t-i,c=s.abs(l)/e;return r=void 0===r?6e-4:r,h=t+c*c/(2*r)*(0>l?-1:1),a=c/r,o>h?(h=n?o-n/2.5*(c/8):o,l=s.abs(h-t),a=l/c):h>0&&(h=n?n/2.5*(c/8):0,l=s.abs(t)+h,a=l/c),{destination:s.round(h),duration:a}};var h=e("transform");return o.extend(o,{hasTransform:h!==!1,hasPerspective:e("perspective")in n,hasTouch:"ontouchstart"in t,hasPointer:!(!t.PointerEvent&&!t.MSPointerEvent),hasTransition:e("transition")in n}),o.isBadAndroid=function(){var i=t.navigator.appVersion;if(/Android/.test(i)&&!/Chrome\/\d/.test(i)){var s=i.match(/Safari\/(\d+.\d)/);return s&&"object"==typeof s&&s.length>=2?parseFloat(s[1])<535.19:!0}return!1}(),o.extend(o.style={},{transform:h,transitionTimingFunction:e("transitionTimingFunction"),transitionDuration:e("transitionDuration"),transitionDelay:e("transitionDelay"),transformOrigin:e("transformOrigin")}),o.hasClass=function(t,i){var s=new RegExp("(^|\\s)"+i+"(\\s|$)");return s.test(t.className)},o.addClass=function(t,i){if(!o.hasClass(t,i)){var s=t.className.split(" ");s.push(i),t.className=s.join(" ")}},o.removeClass=function(t,i){if(o.hasClass(t,i)){var s=new RegExp("(^|\\s)"+i+"(\\s|$)","g");t.className=t.className.replace(s," ")}},o.offset=function(t){for(var i=-t.offsetLeft,s=-t.offsetTop;t=t.offsetParent;)i-=t.offsetLeft,s-=t.offsetTop;return{left:i,top:s}},o.preventDefaultException=function(t,i){for(var s in i)if(i[s].test(t[s]))return!0;return!1},o.extend(o.eventType={},{touchstart:1,touchmove:1,touchend:1,mousedown:2,mousemove:2,mouseup:2,pointerdown:3,pointermove:3,pointerup:3,MSPointerDown:3,MSPointerMove:3,MSPointerUp:3}),o.extend(o.ease={},{quadratic:{style:"cubic-bezier(0.25, 0.46, 0.45, 0.94)",fn:function(t){return t*(2-t)}},circular:{style:"cubic-bezier(0.1, 0.57, 0.1, 1)",fn:function(t){return s.sqrt(1- --t*t)}},back:{style:"cubic-bezier(0.175, 0.885, 0.32, 1.275)",fn:function(t){var i=4;return(t-=1)*t*((i+1)*t+i)+1}},bounce:{style:"",fn:function(t){return(t/=1)<1/2.75?7.5625*t*t:2/2.75>t?7.5625*(t-=1.5/2.75)*t+.75:2.5/2.75>t?7.5625*(t-=2.25/2.75)*t+.9375:7.5625*(t-=2.625/2.75)*t+.984375}},elastic:{style:"",fn:function(t){var i=.22,e=.4;return 0===t?0:1==t?1:e*s.pow(2,-10*t)*s.sin((t-i/4)*(2*s.PI)/i)+1}}}),o.tap=function(t,s){var e=i.createEvent("Event");e.initEvent(s,!0,!0),e.pageX=t.pageX,e.pageY=t.pageY,t.target.dispatchEvent(e)},o.click=function(t){var s,e=t.target;/(SELECT|INPUT|TEXTAREA)/i.test(e.tagName)||(s=i.createEvent("MouseEvents"),s.initMouseEvent("click",!0,!0,t.view,1,e.screenX,e.screenY,e.clientX,e.clientY,t.ctrlKey,t.altKey,t.shiftKey,t.metaKey,0,null),s._constructed=!0,e.dispatchEvent(s))},o}();e.prototype={version:"5.2.0",_init:function(){this._initEvents(),(this.options.scrollbars||this.options.indicators)&&this._initIndicators(),this.options.mouseWheel&&this._initWheel(),this.options.snap&&this._initSnap(),this.options.keyBindings&&this._initKeys()},destroy:function(){this._initEvents(!0),clearTimeout(this.resizeTimeout),this.resizeTimeout=null,this._execEvent("destroy")},_transitionEnd:function(t){t.target==this.scroller&&this.isInTransition&&(this._transitionTime(),this.resetPosition(this.options.bounceTime)||(this.isInTransition=!1,this._execEvent("scrollEnd")))},_start:function(t){if(1!=h.eventType[t.type]){var i;if(i=t.which?t.button:t.button<2?0:4==t.button?1:2,0!==i)return}if(this.enabled&&(!this.initiated||h.eventType[t.type]===this.initiated)){!this.options.preventDefault||h.isBadAndroid||h.preventDefaultException(t.target,this.options.preventDefaultException)||t.preventDefault();var e,o=t.touches?t.touches[0]:t;this.initiated=h.eventType[t.type],this.moved=!1,this.distX=0,this.distY=0,this.directionX=0,this.directionY=0,this.directionLocked=0,this.startTime=h.getTime(),this.options.useTransition&&this.isInTransition?(this._transitionTime(),this.isInTransition=!1,e=this.getComputedPosition(),this._translate(s.round(e.x),s.round(e.y)),this._execEvent("scrollEnd")):!this.options.useTransition&&this.isAnimating&&(this.isAnimating=!1,this._execEvent("scrollEnd")),this.startX=this.x,this.startY=this.y,this.absStartX=this.x,this.absStartY=this.y,this.pointX=o.pageX,this.pointY=o.pageY,this._execEvent("beforeScrollStart")}},_move:function(t){if(this.enabled&&h.eventType[t.type]===this.initiated){this.options.preventDefault&&t.preventDefault();var i,e,o,n,r=t.touches?t.touches[0]:t,a=r.pageX-this.pointX,l=r.pageY-this.pointY,c=h.getTime();if(this.pointX=r.pageX,this.pointY=r.pageY,this.distX+=a,this.distY+=l,o=s.abs(this.distX),n=s.abs(this.distY),!(c-this.endTime>300&&10>o&&10>n)){if(this.directionLocked||this.options.freeScroll||(o>n+this.options.directionLockThreshold?this.directionLocked="h":n>=o+this.options.directionLockThreshold?this.directionLocked="v":this.directionLocked="n"),"h"==this.directionLocked){if("vertical"==this.options.eventPassthrough)t.preventDefault();else if("horizontal"==this.options.eventPassthrough)return void(this.initiated=!1);l=0}else if("v"==this.directionLocked){if("horizontal"==this.options.eventPassthrough)t.preventDefault();else if("vertical"==this.options.eventPassthrough)return void(this.initiated=!1);a=0}a=this.hasHorizontalScroll?a:0,l=this.hasVerticalScroll?l:0,i=this.x+a,e=this.y+l,(i>0||i<this.maxScrollX)&&(i=this.options.bounce?this.x+a/3:i>0?0:this.maxScrollX),(e>0||e<this.maxScrollY)&&(e=this.options.bounce?this.y+l/3:e>0?0:this.maxScrollY),this.directionX=a>0?-1:0>a?1:0,this.directionY=l>0?-1:0>l?1:0,this.moved||this._execEvent("scrollStart"),this.moved=!0,this._translate(i,e),c-this.startTime>300&&(this.startTime=c,this.startX=this.x,this.startY=this.y)}}},_end:function(t){if(this.enabled&&h.eventType[t.type]===this.initiated){this.options.preventDefault&&!h.preventDefaultException(t.target,this.options.preventDefaultException)&&t.preventDefault();var i,e,o=(t.changedTouches?t.changedTouches[0]:t,h.getTime()-this.startTime),n=s.round(this.x),r=s.round(this.y),a=s.abs(n-this.startX),l=s.abs(r-this.startY),c=0,p="";if(this.isInTransition=0,this.initiated=0,this.endTime=h.getTime(),!this.resetPosition(this.options.bounceTime)){if(this.scrollTo(n,r),!this.moved)return this.options.tap&&h.tap(t,this.options.tap),this.options.click&&h.click(t),void this._execEvent("scrollCancel");if(this._events.flick&&200>o&&100>a&&100>l)return void this._execEvent("flick");if(this.options.momentum&&300>o&&(i=this.hasHorizontalScroll?h.momentum(this.x,this.startX,o,this.maxScrollX,this.options.bounce?this.wrapperWidth:0,this.options.deceleration):{destination:n,duration:0},e=this.hasVerticalScroll?h.momentum(this.y,this.startY,o,this.maxScrollY,this.options.bounce?this.wrapperHeight:0,this.options.deceleration):{destination:r,duration:0},n=i.destination,r=e.destination,c=s.max(i.duration,e.duration),this.isInTransition=1),this.options.snap){var d=this._nearestSnap(n,r);this.currentPage=d,c=this.options.snapSpeed||s.max(s.max(s.min(s.abs(n-d.x),1e3),s.min(s.abs(r-d.y),1e3)),300),n=d.x,r=d.y,this.directionX=0,this.directionY=0,p=this.options.bounceEasing}return n!=this.x||r!=this.y?((n>0||n<this.maxScrollX||r>0||r<this.maxScrollY)&&(p=h.ease.quadratic),void this.scrollTo(n,r,c,p)):void this._execEvent("scrollEnd")}}},_resize:function(){var t=this;clearTimeout(this.resizeTimeout),this.resizeTimeout=setTimeout(function(){t.refresh()},this.options.resizePolling)},resetPosition:function(t){var i=this.x,s=this.y;return t=t||0,!this.hasHorizontalScroll||this.x>0?i=0:this.x<this.maxScrollX&&(i=this.maxScrollX),!this.hasVerticalScroll||this.y>0?s=0:this.y<this.maxScrollY&&(s=this.maxScrollY),i==this.x&&s==this.y?!1:(this.scrollTo(i,s,t,this.options.bounceEasing),!0)},disable:function(){this.enabled=!1},enable:function(){this.enabled=!0},refresh:function(){this.wrapper.offsetHeight;this.wrapperWidth=this.wrapper.clientWidth,this.wrapperHeight=this.wrapper.clientHeight,this.scrollerWidth=this.scroller.offsetWidth,this.scrollerHeight=this.scroller.offsetHeight,this.maxScrollX=this.wrapperWidth-this.scrollerWidth,this.maxScrollY=this.wrapperHeight-this.scrollerHeight,this.hasHorizontalScroll=this.options.scrollX&&this.maxScrollX<0,this.hasVerticalScroll=this.options.scrollY&&this.maxScrollY<0,this.hasHorizontalScroll||(this.maxScrollX=0,this.scrollerWidth=this.wrapperWidth),this.hasVerticalScroll||(this.maxScrollY=0,this.scrollerHeight=this.wrapperHeight),this.endTime=0,this.directionX=0,this.directionY=0,this.wrapperOffset=h.offset(this.wrapper),this._execEvent("refresh"),this.resetPosition()},on:function(t,i){this._events[t]||(this._events[t]=[]),this._events[t].push(i)},off:function(t,i){if(this._events[t]){var s=this._events[t].indexOf(i);s>-1&&this._events[t].splice(s,1)}},_execEvent:function(t){if(this._events[t]){var i=0,s=this._events[t].length;if(s)for(;s>i;i++)this._events[t][i].apply(this,[].slice.call(arguments,1))}},scrollBy:function(t,i,s,e){t=this.x+t,i=this.y+i,s=s||0,this.scrollTo(t,i,s,e)},scrollTo:function(t,i,s,e){e=e||h.ease.circular,this.isInTransition=this.options.useTransition&&s>0;var o=this.options.useTransition&&e.style;!s||o?(o&&(this._transitionTimingFunction(e.style),this._transitionTime(s)),this._translate(t,i)):this._animate(t,i,s,e.fn)},scrollToElement:function(t,i,e,o,n){if(t=t.nodeType?t:this.scroller.querySelector(t)){var r=h.offset(t);r.left-=this.wrapperOffset.left,r.top-=this.wrapperOffset.top,e===!0&&(e=s.round(t.offsetWidth/2-this.wrapper.offsetWidth/2)),o===!0&&(o=s.round(t.offsetHeight/2-this.wrapper.offsetHeight/2)),r.left-=e||0,r.top-=o||0,r.left=r.left>0?0:r.left<this.maxScrollX?this.maxScrollX:r.left,r.top=r.top>0?0:r.top<this.maxScrollY?this.maxScrollY:r.top,i=void 0===i||null===i||"auto"===i?s.max(s.abs(this.x-r.left),s.abs(this.y-r.top)):i,this.scrollTo(r.left,r.top,i,n)}},_transitionTime:function(t){t=t||0;var i=h.style.transitionDuration;if(this.scrollerStyle[i]=t+"ms",!t&&h.isBadAndroid){this.scrollerStyle[i]="0.0001ms";var s=this;r(function(){"0.0001ms"===s.scrollerStyle[i]&&(s.scrollerStyle[i]="0s")})}if(this.indicators)for(var e=this.indicators.length;e--;)this.indicators[e].transitionTime(t)},_transitionTimingFunction:function(t){if(this.scrollerStyle[h.style.transitionTimingFunction]=t,this.indicators)for(var i=this.indicators.length;i--;)this.indicators[i].transitionTimingFunction(t)},_translate:function(t,i){if(this.options.useTransform?this.scrollerStyle[h.style.transform]="translate("+t+"px,"+i+"px)"+this.translateZ:(t=s.round(t),i=s.round(i),this.scrollerStyle.left=t+"px",this.scrollerStyle.top=i+"px"),this.x=t,this.y=i,this.indicators)for(var e=this.indicators.length;e--;)this.indicators[e].updatePosition()},_initEvents:function(i){var s=i?h.removeEvent:h.addEvent,e=this.options.bindToWrapper?this.wrapper:t;s(t,"orientationchange",this),s(t,"resize",this),this.options.click&&s(this.wrapper,"click",this,!0),this.options.disableMouse||(s(this.wrapper,"mousedown",this),s(e,"mousemove",this),s(e,"mousecancel",this),s(e,"mouseup",this)),h.hasPointer&&!this.options.disablePointer&&(s(this.wrapper,h.prefixPointerEvent("pointerdown"),this),s(e,h.prefixPointerEvent("pointermove"),this),s(e,h.prefixPointerEvent("pointercancel"),this),s(e,h.prefixPointerEvent("pointerup"),this)),h.hasTouch&&!this.options.disableTouch&&(s(this.wrapper,"touchstart",this),s(e,"touchmove",this),s(e,"touchcancel",this),s(e,"touchend",this)),s(this.scroller,"transitionend",this),s(this.scroller,"webkitTransitionEnd",this),s(this.scroller,"oTransitionEnd",this),s(this.scroller,"MSTransitionEnd",this)},getComputedPosition:function(){var i,s,e=t.getComputedStyle(this.scroller,null);return this.options.useTransform?(e=e[h.style.transform].split(")")[0].split(", "),i=+(e[12]||e[4]),s=+(e[13]||e[5])):(i=+e.left.replace(/[^-\d.]/g,""),s=+e.top.replace(/[^-\d.]/g,"")),{x:i,y:s}},_initIndicators:function(){function t(t){if(h.indicators)for(var i=h.indicators.length;i--;)t.call(h.indicators[i])}var i,s=this.options.interactiveScrollbars,e="string"!=typeof this.options.scrollbars,r=[],h=this;this.indicators=[],this.options.scrollbars&&(this.options.scrollY&&(i={el:o("v",s,this.options.scrollbars),interactive:s,defaultScrollbars:!0,customStyle:e,resize:this.options.resizeScrollbars,shrink:this.options.shrinkScrollbars,fade:this.options.fadeScrollbars,listenX:!1},this.wrapper.appendChild(i.el),r.push(i)),this.options.scrollX&&(i={el:o("h",s,this.options.scrollbars),interactive:s,defaultScrollbars:!0,customStyle:e,resize:this.options.resizeScrollbars,shrink:this.options.shrinkScrollbars,fade:this.options.fadeScrollbars,listenY:!1},this.wrapper.appendChild(i.el),r.push(i))),this.options.indicators&&(r=r.concat(this.options.indicators));for(var a=r.length;a--;)this.indicators.push(new n(this,r[a]));this.options.fadeScrollbars&&(this.on("scrollEnd",function(){t(function(){this.fade()})}),this.on("scrollCancel",function(){t(function(){this.fade()})}),this.on("scrollStart",function(){t(function(){this.fade(1)})}),this.on("beforeScrollStart",function(){t(function(){this.fade(1,!0)})})),this.on("refresh",function(){t(function(){this.refresh()})}),this.on("destroy",function(){t(function(){this.destroy()}),delete this.indicators})},_initWheel:function(){h.addEvent(this.wrapper,"wheel",this),h.addEvent(this.wrapper,"mousewheel",this),h.addEvent(this.wrapper,"DOMMouseScroll",this),this.on("destroy",function(){clearTimeout(this.wheelTimeout),this.wheelTimeout=null,h.removeEvent(this.wrapper,"wheel",this),h.removeEvent(this.wrapper,"mousewheel",this),h.removeEvent(this.wrapper,"DOMMouseScroll",this)})},_wheel:function(t){if(this.enabled){t.preventDefault();var i,e,o,n,r=this;if(void 0===this.wheelTimeout&&r._execEvent("scrollStart"),clearTimeout(this.wheelTimeout),this.wheelTimeout=setTimeout(function(){r.options.snap||r._execEvent("scrollEnd"),r.wheelTimeout=void 0},400),"deltaX"in t)1===t.deltaMode?(i=-t.deltaX*this.options.mouseWheelSpeed,e=-t.deltaY*this.options.mouseWheelSpeed):(i=-t.deltaX,e=-t.deltaY);else if("wheelDeltaX"in t)i=t.wheelDeltaX/120*this.options.mouseWheelSpeed,e=t.wheelDeltaY/120*this.options.mouseWheelSpeed;else if("wheelDelta"in t)i=e=t.wheelDelta/120*this.options.mouseWheelSpeed;else{if(!("detail"in t))return;i=e=-t.detail/3*this.options.mouseWheelSpeed}if(i*=this.options.invertWheelDirection,e*=this.options.invertWheelDirection,this.hasVerticalScroll||(i=e,e=0),this.options.snap)return o=this.currentPage.pageX,n=this.currentPage.pageY,i>0?o--:0>i&&o++,e>0?n--:0>e&&n++,void this.goToPage(o,n);o=this.x+s.round(this.hasHorizontalScroll?i:0),n=this.y+s.round(this.hasVerticalScroll?e:0),this.directionX=i>0?-1:0>i?1:0,this.directionY=e>0?-1:0>e?1:0,o>0?o=0:o<this.maxScrollX&&(o=this.maxScrollX),n>0?n=0:n<this.maxScrollY&&(n=this.maxScrollY),this.scrollTo(o,n,0)}},_initSnap:function(){this.currentPage={},"string"==typeof this.options.snap&&(this.options.snap=this.scroller.querySelectorAll(this.options.snap)),this.on("refresh",function(){var t,i,e,o,n,r,h=0,a=0,l=0,c=this.options.snapStepX||this.wrapperWidth,p=this.options.snapStepY||this.wrapperHeight;if(this.pages=[],this.wrapperWidth&&this.wrapperHeight&&this.scrollerWidth&&this.scrollerHeight){if(this.options.snap===!0)for(e=s.round(c/2),o=s.round(p/2);l>-this.scrollerWidth;){for(this.pages[h]=[],t=0,n=0;n>-this.scrollerHeight;)this.pages[h][t]={x:s.max(l,this.maxScrollX),y:s.max(n,this.maxScrollY),width:c,height:p,cx:l-e,cy:n-o},n-=p,t++;l-=c,h++}else for(r=this.options.snap,t=r.length,i=-1;t>h;h++)(0===h||r[h].offsetLeft<=r[h-1].offsetLeft)&&(a=0,i++),this.pages[a]||(this.pages[a]=[]),l=s.max(-r[h].offsetLeft,this.maxScrollX),n=s.max(-r[h].offsetTop,this.maxScrollY),e=l-s.round(r[h].offsetWidth/2),o=n-s.round(r[h].offsetHeight/2),this.pages[a][i]={x:l,y:n,width:r[h].offsetWidth,height:r[h].offsetHeight,cx:e,cy:o},l>this.maxScrollX&&a++;this.goToPage(this.currentPage.pageX||0,this.currentPage.pageY||0,0),this.options.snapThreshold%1===0?(this.snapThresholdX=this.options.snapThreshold,this.snapThresholdY=this.options.snapThreshold):(this.snapThresholdX=s.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].width*this.options.snapThreshold),this.snapThresholdY=s.round(this.pages[this.currentPage.pageX][this.currentPage.pageY].height*this.options.snapThreshold))}}),this.on("flick",function(){var t=this.options.snapSpeed||s.max(s.max(s.min(s.abs(this.x-this.startX),1e3),s.min(s.abs(this.y-this.startY),1e3)),300);this.goToPage(this.currentPage.pageX+this.directionX,this.currentPage.pageY+this.directionY,t)})},_nearestSnap:function(t,i){if(!this.pages.length)return{x:0,y:0,pageX:0,pageY:0};var e=0,o=this.pages.length,n=0;if(s.abs(t-this.absStartX)<this.snapThresholdX&&s.abs(i-this.absStartY)<this.snapThresholdY)return this.currentPage;for(t>0?t=0:t<this.maxScrollX&&(t=this.maxScrollX),i>0?i=0:i<this.maxScrollY&&(i=this.maxScrollY);o>e;e++)if(t>=this.pages[e][0].cx){t=this.pages[e][0].x;break}for(o=this.pages[e].length;o>n;n++)if(i>=this.pages[0][n].cy){i=this.pages[0][n].y;break}return e==this.currentPage.pageX&&(e+=this.directionX,0>e?e=0:e>=this.pages.length&&(e=this.pages.length-1),t=this.pages[e][0].x),n==this.currentPage.pageY&&(n+=this.directionY,0>n?n=0:n>=this.pages[0].length&&(n=this.pages[0].length-1),i=this.pages[0][n].y),{x:t,y:i,pageX:e,pageY:n}},goToPage:function(t,i,e,o){o=o||this.options.bounceEasing,t>=this.pages.length?t=this.pages.length-1:0>t&&(t=0),i>=this.pages[t].length?i=this.pages[t].length-1:0>i&&(i=0);var n=this.pages[t][i].x,r=this.pages[t][i].y;e=void 0===e?this.options.snapSpeed||s.max(s.max(s.min(s.abs(n-this.x),1e3),s.min(s.abs(r-this.y),1e3)),300):e,this.currentPage={x:n,y:r,pageX:t,pageY:i},this.scrollTo(n,r,e,o)},next:function(t,i){var s=this.currentPage.pageX,e=this.currentPage.pageY;s++,s>=this.pages.length&&this.hasVerticalScroll&&(s=0,e++),this.goToPage(s,e,t,i)},prev:function(t,i){var s=this.currentPage.pageX,e=this.currentPage.pageY;s--,0>s&&this.hasVerticalScroll&&(s=0,e--),this.goToPage(s,e,t,i)},_initKeys:function(i){var s,e={pageUp:33,pageDown:34,end:35,home:36,left:37,up:38,right:39,down:40};if("object"==typeof this.options.keyBindings)for(s in this.options.keyBindings)"string"==typeof this.options.keyBindings[s]&&(this.options.keyBindings[s]=this.options.keyBindings[s].toUpperCase().charCodeAt(0));else this.options.keyBindings={};for(s in e)this.options.keyBindings[s]=this.options.keyBindings[s]||e[s];h.addEvent(t,"keydown",this),this.on("destroy",function(){h.removeEvent(t,"keydown",this)})},_key:function(t){if(this.enabled){var i,e=this.options.snap,o=e?this.currentPage.pageX:this.x,n=e?this.currentPage.pageY:this.y,r=h.getTime(),a=this.keyTime||0,l=.25;switch(this.options.useTransition&&this.isInTransition&&(i=this.getComputedPosition(),this._translate(s.round(i.x),s.round(i.y)),this.isInTransition=!1),this.keyAcceleration=200>r-a?s.min(this.keyAcceleration+l,50):0,t.keyCode){case this.options.keyBindings.pageUp:this.hasHorizontalScroll&&!this.hasVerticalScroll?o+=e?1:this.wrapperWidth:n+=e?1:this.wrapperHeight;break;case this.options.keyBindings.pageDown:this.hasHorizontalScroll&&!this.hasVerticalScroll?o-=e?1:this.wrapperWidth:n-=e?1:this.wrapperHeight;break;case this.options.keyBindings.end:o=e?this.pages.length-1:this.maxScrollX,n=e?this.pages[0].length-1:this.maxScrollY;break;case this.options.keyBindings.home:o=0,n=0;break;case this.options.keyBindings.left:o+=e?-1:5+this.keyAcceleration>>0;break;case this.options.keyBindings.up:n+=e?1:5+this.keyAcceleration>>0;break;case this.options.keyBindings.right:o-=e?-1:5+this.keyAcceleration>>0;break;case this.options.keyBindings.down:n-=e?1:5+this.keyAcceleration>>0;break;default:return}if(e)return void this.goToPage(o,n);o>0?(o=0,this.keyAcceleration=0):o<this.maxScrollX&&(o=this.maxScrollX,this.keyAcceleration=0),n>0?(n=0,this.keyAcceleration=0):n<this.maxScrollY&&(n=this.maxScrollY,this.keyAcceleration=0),this.scrollTo(o,n,0),this.keyTime=r}},_animate:function(t,i,s,e){function o(){var d,u,m,f=h.getTime();return f>=p?(n.isAnimating=!1,n._translate(t,i),void(n.resetPosition(n.options.bounceTime)||n._execEvent("scrollEnd"))):(f=(f-c)/s,m=e(f),d=(t-a)*m+a,u=(i-l)*m+l,n._translate(d,u),void(n.isAnimating&&r(o)))}var n=this,a=this.x,l=this.y,c=h.getTime(),p=c+s;this.isAnimating=!0,o()},handleEvent:function(t){switch(t.type){case"touchstart":case"pointerdown":case"MSPointerDown":case"mousedown":this._start(t);break;case"touchmove":case"pointermove":case"MSPointerMove":case"mousemove":this._move(t);break;case"touchend":case"pointerup":case"MSPointerUp":case"mouseup":case"touchcancel":case"pointercancel":case"MSPointerCancel":case"mousecancel":this._end(t);break;case"orientationchange":case"resize":this._resize();break;case"transitionend":case"webkitTransitionEnd":case"oTransitionEnd":case"MSTransitionEnd":this._transitionEnd(t);break;case"wheel":case"DOMMouseScroll":case"mousewheel":this._wheel(t);break;case"keydown":this._key(t);break;case"click":this.enabled&&!t._constructed&&(t.preventDefault(),t.stopPropagation())}}},n.prototype={handleEvent:function(t){switch(t.type){case"touchstart":case"pointerdown":case"MSPointerDown":case"mousedown":this._start(t);break;case"touchmove":case"pointermove":case"MSPointerMove":case"mousemove":this._move(t);break;case"touchend":case"pointerup":case"MSPointerUp":case"mouseup":case"touchcancel":case"pointercancel":case"MSPointerCancel":case"mousecancel":this._end(t)}},destroy:function(){this.options.fadeScrollbars&&(clearTimeout(this.fadeTimeout),this.fadeTimeout=null),this.options.interactive&&(h.removeEvent(this.indicator,"touchstart",this),h.removeEvent(this.indicator,h.prefixPointerEvent("pointerdown"),this),h.removeEvent(this.indicator,"mousedown",this),h.removeEvent(t,"touchmove",this),h.removeEvent(t,h.prefixPointerEvent("pointermove"),this),h.removeEvent(t,"mousemove",this),h.removeEvent(t,"touchend",this),h.removeEvent(t,h.prefixPointerEvent("pointerup"),this),h.removeEvent(t,"mouseup",this)),this.options.defaultScrollbars&&this.wrapper.parentNode.removeChild(this.wrapper)},_start:function(i){var s=i.touches?i.touches[0]:i;i.preventDefault(),i.stopPropagation(),this.transitionTime(),this.initiated=!0,this.moved=!1,this.lastPointX=s.pageX,this.lastPointY=s.pageY,this.startTime=h.getTime(),this.options.disableTouch||h.addEvent(t,"touchmove",this),this.options.disablePointer||h.addEvent(t,h.prefixPointerEvent("pointermove"),this),this.options.disableMouse||h.addEvent(t,"mousemove",this),this.scroller._execEvent("beforeScrollStart")},_move:function(t){var i,s,e,o,n=t.touches?t.touches[0]:t;h.getTime();this.moved||this.scroller._execEvent("scrollStart"),this.moved=!0,i=n.pageX-this.lastPointX,this.lastPointX=n.pageX,s=n.pageY-this.lastPointY,this.lastPointY=n.pageY,e=this.x+i,o=this.y+s,this._pos(e,o),t.preventDefault(),t.stopPropagation()},_end:function(i){if(this.initiated){if(this.initiated=!1,i.preventDefault(),i.stopPropagation(),h.removeEvent(t,"touchmove",this),h.removeEvent(t,h.prefixPointerEvent("pointermove"),this),h.removeEvent(t,"mousemove",this),this.scroller.options.snap){var e=this.scroller._nearestSnap(this.scroller.x,this.scroller.y),o=this.options.snapSpeed||s.max(s.max(s.min(s.abs(this.scroller.x-e.x),1e3),s.min(s.abs(this.scroller.y-e.y),1e3)),300);this.scroller.x==e.x&&this.scroller.y==e.y||(this.scroller.directionX=0,this.scroller.directionY=0,this.scroller.currentPage=e,this.scroller.scrollTo(e.x,e.y,o,this.scroller.options.bounceEasing))}this.moved&&this.scroller._execEvent("scrollEnd")}},transitionTime:function(t){t=t||0;var i=h.style.transitionDuration;if(this.indicatorStyle[i]=t+"ms",!t&&h.isBadAndroid){this.indicatorStyle[i]="0.0001ms";var s=this;r(function(){"0.0001ms"===s.indicatorStyle[i]&&(s.indicatorStyle[i]="0s")})}},transitionTimingFunction:function(t){this.indicatorStyle[h.style.transitionTimingFunction]=t},refresh:function(){this.transitionTime(),this.options.listenX&&!this.options.listenY?this.indicatorStyle.display=this.scroller.hasHorizontalScroll?"block":"none":this.options.listenY&&!this.options.listenX?this.indicatorStyle.display=this.scroller.hasVerticalScroll?"block":"none":this.indicatorStyle.display=this.scroller.hasHorizontalScroll||this.scroller.hasVerticalScroll?"block":"none",this.scroller.hasHorizontalScroll&&this.scroller.hasVerticalScroll?(h.addClass(this.wrapper,"iScrollBothScrollbars"),h.removeClass(this.wrapper,"iScrollLoneScrollbar"),this.options.defaultScrollbars&&this.options.customStyle&&(this.options.listenX?this.wrapper.style.right="8px":this.wrapper.style.bottom="8px")):(h.removeClass(this.wrapper,"iScrollBothScrollbars"),h.addClass(this.wrapper,"iScrollLoneScrollbar"),this.options.defaultScrollbars&&this.options.customStyle&&(this.options.listenX?this.wrapper.style.right="2px":this.wrapper.style.bottom="2px"));this.wrapper.offsetHeight;this.options.listenX&&(this.wrapperWidth=this.wrapper.clientWidth,this.options.resize?(this.indicatorWidth=s.max(s.round(this.wrapperWidth*this.wrapperWidth/(this.scroller.scrollerWidth||this.wrapperWidth||1)),8),this.indicatorStyle.width=this.indicatorWidth+"px"):this.indicatorWidth=this.indicator.clientWidth,this.maxPosX=this.wrapperWidth-this.indicatorWidth,"clip"==this.options.shrink?(this.minBoundaryX=-this.indicatorWidth+8,this.maxBoundaryX=this.wrapperWidth-8):(this.minBoundaryX=0,this.maxBoundaryX=this.maxPosX),this.sizeRatioX=this.options.speedRatioX||this.scroller.maxScrollX&&this.maxPosX/this.scroller.maxScrollX),this.options.listenY&&(this.wrapperHeight=this.wrapper.clientHeight,this.options.resize?(this.indicatorHeight=s.max(s.round(this.wrapperHeight*this.wrapperHeight/(this.scroller.scrollerHeight||this.wrapperHeight||1)),8),this.indicatorStyle.height=this.indicatorHeight+"px"):this.indicatorHeight=this.indicator.clientHeight,this.maxPosY=this.wrapperHeight-this.indicatorHeight,"clip"==this.options.shrink?(this.minBoundaryY=-this.indicatorHeight+8,this.maxBoundaryY=this.wrapperHeight-8):(this.minBoundaryY=0,this.maxBoundaryY=this.maxPosY),this.maxPosY=this.wrapperHeight-this.indicatorHeight,this.sizeRatioY=this.options.speedRatioY||this.scroller.maxScrollY&&this.maxPosY/this.scroller.maxScrollY),this.updatePosition()},updatePosition:function(){var t=this.options.listenX&&s.round(this.sizeRatioX*this.scroller.x)||0,i=this.options.listenY&&s.round(this.sizeRatioY*this.scroller.y)||0;this.options.ignoreBoundaries||(t<this.minBoundaryX?("scale"==this.options.shrink&&(this.width=s.max(this.indicatorWidth+t,8),this.indicatorStyle.width=this.width+"px"),t=this.minBoundaryX):t>this.maxBoundaryX?"scale"==this.options.shrink?(this.width=s.max(this.indicatorWidth-(t-this.maxPosX),8),this.indicatorStyle.width=this.width+"px",t=this.maxPosX+this.indicatorWidth-this.width):t=this.maxBoundaryX:"scale"==this.options.shrink&&this.width!=this.indicatorWidth&&(this.width=this.indicatorWidth,this.indicatorStyle.width=this.width+"px"),i<this.minBoundaryY?("scale"==this.options.shrink&&(this.height=s.max(this.indicatorHeight+3*i,8),this.indicatorStyle.height=this.height+"px"),i=this.minBoundaryY):i>this.maxBoundaryY?"scale"==this.options.shrink?(this.height=s.max(this.indicatorHeight-3*(i-this.maxPosY),8),this.indicatorStyle.height=this.height+"px",i=this.maxPosY+this.indicatorHeight-this.height):i=this.maxBoundaryY:"scale"==this.options.shrink&&this.height!=this.indicatorHeight&&(this.height=this.indicatorHeight,this.indicatorStyle.height=this.height+"px")),
//this.x=t,this.y=i,this.scroller.options.useTransform?this.indicatorStyle[h.style.transform]="translate("+t+"px,"+i+"px)"+this.scroller.translateZ:(this.indicatorStyle.left=t+"px",this.indicatorStyle.top=i+"px")},_pos:function(t,i){0>t?t=0:t>this.maxPosX&&(t=this.maxPosX),0>i?i=0:i>this.maxPosY&&(i=this.maxPosY),t=this.options.listenX?s.round(t/this.sizeRatioX):this.scroller.x,i=this.options.listenY?s.round(i/this.sizeRatioY):this.scroller.y,this.scroller.scrollTo(t,i)},fade:function(t,i){if(!i||this.visible){clearTimeout(this.fadeTimeout),this.fadeTimeout=null;var s=t?250:500,e=t?0:300;t=t?"1":"0",this.wrapperStyle[h.style.transitionDuration]=s+"ms",this.fadeTimeout=setTimeout(function(t){this.wrapperStyle.opacity=t,this.visible=+t}.bind(this,t),e)}}},e.utils=h,"undefined"!=typeof module&&module.exports?module.exports=e:"function"==typeof define&&define.amd?define(function(){return e}):t.IScroll=e}(window,document,Math);
//# sourceMappingURL=./iscroll.min.js.map


/*!
 * Smooth Scroll - v1.4.12 - 2013-09-19
 * https://github.com/kswedberg/jquery-smooth-scroll
 * Copyright (c) 2013 Karl Swedberg
 * Licensed MIT (https://github.com/kswedberg/jquery-smooth-scroll/blob/master/LICENSE-MIT)
 */
//(function(t){function l(t){return t.replace(/(:|\.)/g,"\\$1")}var e="1.4.12",o={exclude:[],excludeWithin:[],offset:0,direction:"top",scrollElement:null,scrollTarget:null,beforeScroll:function(){},afterScroll:function(){},easing:"swing",speed:400,autoCoefficent:2,preventDefault:!0},n=function(l){var e=[],o=!1,n=l.dir&&"left"==l.dir?"scrollLeft":"scrollTop";return this.each(function(){if(this!=document&&this!=window){var l=t(this);l[n]()>0?e.push(this):(l[n](1),o=l[n]()>0,o&&e.push(this),l[n](0))}}),e.length||this.each(function(){"BODY"===this.nodeName&&(e=[this])}),"first"===l.el&&e.length>1&&(e=[e[0]]),e};t.fn.extend({scrollable:function(t){var l=n.call(this,{dir:t});return this.pushStack(l)},firstScrollable:function(t){var l=n.call(this,{el:"first",dir:t});return this.pushStack(l)},smoothScroll:function(e){e=e||{};var o=t.extend({},t.fn.smoothScroll.defaults,e),n=t.smoothScroll.filterPath(location.pathname);return this.unbind("click.smoothscroll").bind("click.smoothscroll",function(e){var r=this,s=t(this),c=o.exclude,i=o.excludeWithin,a=0,f=0,h=!0,u={},d=location.hostname===r.hostname||!r.hostname,m=o.scrollTarget||(t.smoothScroll.filterPath(r.pathname)||n)===n,p=l(r.hash);if(o.scrollTarget||d&&m&&p){for(;h&&c.length>a;)s.is(l(c[a++]))&&(h=!1);for(;h&&i.length>f;)s.closest(i[f++]).length&&(h=!1)}else h=!1;h&&(o.preventDefault&&e.preventDefault(),t.extend(u,o,{scrollTarget:o.scrollTarget||p,link:r}),t.smoothScroll(u))}),this}}),t.smoothScroll=function(l,e){var o,n,r,s,c=0,i="offset",a="scrollTop",f={},h={};"number"==typeof l?(o=t.fn.smoothScroll.defaults,r=l):(o=t.extend({},t.fn.smoothScroll.defaults,l||{}),o.scrollElement&&(i="position","static"==o.scrollElement.css("position")&&o.scrollElement.css("position","relative"))),o=t.extend({link:null},o),a="left"==o.direction?"scrollLeft":a,o.scrollElement?(n=o.scrollElement,/^(?:HTML|BODY)$/.test(n[0].nodeName)||(c=n[a]())):n=t("html, body").firstScrollable(o.direction),o.beforeScroll.call(n,o),r="number"==typeof l?l:e||t(o.scrollTarget)[i]()&&t(o.scrollTarget)[i]()[o.direction]||0,f[a]=r+c+o.offset,s=o.speed,"auto"===s&&(s=f[a]||n.scrollTop(),s/=o.autoCoefficent),h={duration:s,easing:o.easing,complete:function(){o.afterScroll.call(o.link,o)}},o.step&&(h.step=o.step),n.length?n.stop().animate(f,h):o.afterScroll.call(o.link,o)},t.smoothScroll.version=e,t.smoothScroll.filterPath=function(t){return t.replace(/^\//,"").replace(/(?:index|default).[a-zA-Z]{3,4}$/,"").replace(/\/$/,"")},t.fn.smoothScroll.defaults=o})(jQuery);


$(function() {
    var $head = $('#header');
    $('.waypoint').each(function(i) {
        var $el = $(this),
            animClassDown = $el.data('animateDown'),
            animClassUp = $el.data('animateUp');

        $el.waypoint(function(direction) {
            if( direction === 'down' && animClassDown ) {
                $head.attr('class', 'header ' + animClassDown);
            } else if( direction === 'up' && animClassUp ) {
                $head.attr('class', 'header ' + animClassUp);
            }
        }, {
            // offset: '100%'
            offset: '70px'
        });
    });
});

$(function() {
	
	
	var $body = document.body,
		sidenav_back = document.querySelector('.backdrop'),
		sidenav_open = document.getElementById('open-sidenav'),
		sidenav_close = document.getElementById('close-button'),
		isOpen = false;

    if(sidenav_open) sidenav_open.addEventListener('click', sidenav_toggleMenu);
    // if(sidenav_open) sidenav_open.addEventListener('focusin', sidenav_toggleMenu);
	
    //if(sidenav_close) sidenav_close.addEventListener('click', sidenav_toggleMenu);
    if(sidenav_close) sidenav_close.addEventListener('click', sidenav_toggleMenu_close);
    if(sidenav_back) sidenav_back.addEventListener('click', function(e) {
        var target = e.target;
        if (isOpen && target !== sidenav_open) sidenav_toggleMenu();
    });
	
	
	/*
	 * 좌측메뉴 열기
	 */
    var iscroller;
	var _target_side;
	
	function sidenav_toggleMenu() {
		
		_target_side = event.target;
		
		if(isOpen) {
            bonzo($body).removeClass('sidenav-active');
            $("body").css({overflow:'inherit'}).unbind('touchmove');
            // myScroll.destroy();
        } else {
			bonzo($body).addClass('sidenav-active');
			
			// $('.sidenav-content').show(function(){
			// 	bonzo($body).addClass('sidenav-active');
			// 	$("body").css({overflow:'hidden'}).bind('touchmove', function(e){
			// 		e.preventDefault()
			// 	});
			// 	
			// 	$('.header-top').attr('tabindex', '0').focus();
			// });

            // myScroll = new IScroll('#total_menu_wrap', {
            //     click: true,
            //     scrollbars: false,
            //     mouseWheel: true,
            //     // interactiveScrollbars: true,
            //     // shrinkScrollbars: 'scale',
            //     // fadeScrollbars: true,
            // });
			
			
			
			/*
				$('.sidebar-container .swiper-container').show();  
				var sidebarBanner = new Swiper('.sidebar-container .swiper-container', {
					nextButton: '.sidebar-container .swiper-button-next',
					prevButton: '.sidebar-container .swiper-button-prev',
					pagination: '.sidebar-container .swiper-pagination',
					paginationClickable: true,
					paginationClickable: true,
					onInit: function (swiper) {
						// console.log('.sidebar-container .swiper-container');
						
						if (swiper.slides.length == 1) {
							$('.sidebar-container .swiper-pagination').hide();
						} else {
							$('.sidebar-container .swiper-button-next').show();
							$('.sidebar-container .swiper-button-prev').show();
						}
					}
				});

				
				$('.iscroller').wrapInner('<div class="swiper-wrapper"></div>');
				$('.iscroller > .swiper-wrapper').wrapInner('<div class="swiper-slide"></div>');
				iscroller = new Swiper('.iscroller', {
					// scrollbar: '.swiper-scrollbar',
					direction: 'vertical',
					slidesPerView: 'auto',
					// mousewheelControl: true,
					freeMode: true,
					// autoHeight: true,
				});
			*/
        }
		isOpen = !isOpen;
	}
	
	/*
	 * 좌측메뉴 닫기
	 */
	function sidenav_toggleMenu_close() {
		if(isOpen) {
			bonzo($body).removeClass('sidenav-active');
			$("body").css({overflow:'inherit'}).unbind('touchmove');
			// $('.sidenav-content').hide();
			$(_target_side).fadeIn().focus();
			
            // myScroll.destroy();
        }
		isOpen = !isOpen;
	}
	
	
	
	
	
	
	/*
	 * sidebar toggle button
	 */
	$('.sidebar-content .bot-menu > li > a').on('click', function (e) {
		e.preventDefault();
		var _this = $(this).closest('li');
		var _li = $('.sidebar-content .bot-menu > li');
		
		if (_this.hasClass('active')) {
			_li.removeClass('active');
		} else {
			_li.removeClass('active');
			_this.addClass('active');
		}
		
		
		/*
		iscroller.destroy();
		var iscroller2 = new Swiper('.iscroller', {
			direction: 'vertical',
			slidesPerView: 'auto',
			freeMode: true
		});
		*/
	});
	
	
	
	
	//var sel_box_height = $('.sel_cnt_box_txt.bmx').attr('box-height');	
	//$('.sel_cnt_box_txt.bmx').css('height', sel_box_height);
	
	//console.log('sel_box_height : ', sel_box_height);
	
	/*
	 * toggleTap table
	 */
	$('.toggle-tap-table a').on('click', function (e) {
		// e.preventDefault();
		var _this = $(this);
		if (_this.hasClass('active')) {
			_this.removeClass('active');
			$('.sel_cnt_box_txt.bmx').css('height', sel_box_height);
		} else {
			_this.removeClass('active');
			_this.addClass('active');
			$('.sel_cnt_box_txt.bmx').css('height', 'inherit');
		}
	});
	
	
	/*
	 * toggleTap vertical
	 */
	$('.toggle-tap-vertical dl dt a').on('click', function (e) {
		e.preventDefault();
		var _this = $(this).closest('dt');
		var _dt = $('.toggle-tap-vertical dl dt');
		
		if (_this.hasClass('active')) {
			_dt.removeClass('active');
		} else {
			_dt.removeClass('active');
			_this.addClass('active');
		}
	});
	
	
	
	
	
	/*
	 * page tabMenu 1
	 */
	$('.content-info-view .tabMenu-control a').on('click', function (e) {
		e.preventDefault();
		
		var _this = $(this),
			_data = _this.data('tab-area');
			_slice = _data.slice(0, -1);
		
		_this.closest('.tabMenu-control').find('a').removeClass('active');
		_this.toggleClass('active');
		
		$('[class*="' + _slice + '"]').hide();
		$('.' + _data).show();
		
		var ea_top = $('.content-info-view').offset().top;
		$.smoothScroll({ easing: 'easeOutExpo', speed: 500, offset: ea_top - 100});
		
		$('.content-info-view .tabMenu-control a').attr('title', '');
		$(this).attr('title', '선택');
	});
	
	
	/*
	 * page tabMenu 2
	 */
	$('.tabMenu-control.vs-view a').on('click', function (e) {
		e.preventDefault();
		
		var _this = $(this),
			_data = _this.data('tab-area');
			_slice = _data.slice(0, -1);
		
		_this.closest('.tabMenu-control').find('a').removeClass('active');
		_this.toggleClass('active');
		
		$('[class*="' + _slice + '"]').hide();
		$('.' + _data).show();

		$('.tabMenu-control.vs-view a').attr('title', '');
		$(this).attr('title', '선택');
	});
	
	
	/*
	 * rdo .radio-box label
	 */
	$('.rdo .radio-box input').on('click', function (e) {
		$('.rdo .radio-box label').attr('title', '');
		$(this).next().attr('title', '선택');
	});
	
	$('.cko .checkbox-box input').on('click', function (e) {
		$('.cko .checkbox-box label').attr('title', '');
		$(this).next().attr('title', '선택');
	});
	
	
	/*
	 * page toggleMenu
	 */
	$('.toggleMenu-control a').on('click', function (e) {
		e.preventDefault();
		var _this = $(this),
			_data = _this.data('tab-area');
			_slice = _data.slice(0, -1);
		
		_this.closest('.tabMenu-control').find('a').removeClass('active');
		
		if (_this.hasClass('active')) {
			_this.removeClass('active');
			$('[class*="' + _slice + '"]').show();
		} else {
			_this.addClass('active');
			$('[class*="' + _slice + '"]').hide();
		}
	});
	
	
	/*
	 * 상단바 스크롤 고정
	 */
	var $info_tab = $('.info-view');
	$('.content-info-view').each(function (i) {
		var $el = $(this),
			animClassDown = $el.data('animateDown'),
			animClassUp = $el.data('animateUp');

		$el.waypoint(function(direction) {
			
			// console.log('waypoint direction 2 : ', direction);
			
			if( direction === 'down' && animClassDown ) {
				$info_tab.attr('class', 'tabMenu-control info-view ' + animClassDown);
			} else if( direction === 'up' && animClassUp ){
				$info_tab.attr('class', 'tabMenu-control info-view ' + animClassUp);
			}
		}, {
			offset: '70px'
		});
	});
	
	
	/*
	 * 적합성진단 관련
	 */
	if ($('.sp-ui-fm-box').length) {
		$('.sp-ui-fm-box').first().show();
		$('.sp-ui-fm-box').each(function (i, k) {
			var _k = $(k);
			_k.attr('data-no', i);
		});
	} 
	
	
	if ($('.nts-box-bar').length) {		
		$('.nts-box-bar input').on('click', function () {
			var _this = $(this);
			var nts = _this.closest('.nts-box-bar');			
			nts.find('.radio-box').removeClass('active active-lv');
			_this.closest('.radio-box').addClass('active-lv').prevAll().addClass('active');
		});
		
		$('.sp-ui-fm-box input').on('click', function () {			
			var _this = $(this);
			var sp = _this.closest('.sp-ui-fm-box');
			var no = sp.data('no');
			$('.sp-ui-fm-box[data-no="' + (no + 1) + '"]').show();
		});
	}
	

	// 20180312 Lee Gwang Il 모바일 가입설계 특양추가 슬라이드
	//$(".content.xnt").css("display", "none");
	
	$(".trtySlideBtn .tit.xnt").on("click", function(){
		$(".content.xnt").slideToggle("fast");
		$(this).toggleClass("bottomBar");
	});
});



/*
 * popup
 */
var popup = (function ($) {
	
	var _target;
	
	var popup_show = function (elem) {
		
		if (event) _target = event.target;		
		// console.log('popup_show : ', _target);
		
		var _popname = $('[data-popup-name="'+elem+'"]');
	
		// event.preventDefault ? event.preventDefault() : event.returnValue = false;
		// var top  = ($(window).scrollTop() + ($(window).height() - _popname.outerHeight()) / 2);
		// var left = ($(window).scrollLeft() + ($(window).width() - _popname.outerWidth()) / 2);
		// _popname.css({'left':left,'top':top}).show();
		
		var w_height = $(window).height();
		$('.wrap').css({ height: w_height, overflow: 'hidden'});
		
		_popname.show();
		_popname.find('h2.title').attr('tabindex', '0').focus();
		
		
		/*
		setTimeout(function () {
			// 임시설계함 팝업 스와프 호출
			// if ($('.my-swiper-box').length) mySwiperBox();
			$('.my-swiper-box .swiper-container').show();
			var my_banner_st = new Swiper('.my-swiper-box .swiper-container', {
				autoHeight: true,
				spaceBetween: 30,
				effect: 'coverflow',
				grabCursor: true,
				centeredSlides: true,
				coverflow: {
					rotate: 0,
					stretch: 0,
					depth: 80,
					modifier: 1,
					slideShadows : true
				},
				freeMode:true,
				paginationType: 'fraction',
				pagination: '.my-swiper-box .swiper-pagination',
				onTransitionStart: function (swiper) {
					$('.naoTooltip-wrap').naoTooltip();
				}
			});
			// console.log('.my-swiper-box .swiper-container');
			
			// my_banner_st.updateContainerSize()
		}, 1000);
		
		*/
	};
		
	var popup_hide = function (elem) {
		
		// event.preventDefault ? event.preventDefault() : event.returnValue = false;
		
		var _popname = $('[data-popup-name="' + elem + '"]');
		_popname.hide();
		
		$('.wrap').css({ height:'auto', overflow: 'auto'});
		
		$(_target).fadeIn().focus();
		// console.log('popup_hide : ', _target);
	};

	return {
		show: function (elem) {
		   return popup_show(elem);
		},
		hide : function (elem) {
			return popup_hide(elem);
		}
	}
}(jQuery));


/*
 * alert popup 
 */
var alert_popup = (function ($) {
	var _target;
	
	var popup_show = function (elem) {
		
		if (event) _target = event.target;
		// console.log('alert_popup_show : ', _target);
		
		var _popname = $('[data-popup-name="'+elem+'"]');
		var top  = ($(window).scrollTop() + ($(window).height() - _popname.outerHeight()) / 2);
		var left = ($(window).scrollLeft() + ($(window).width() - _popname.outerWidth()) / 2);
		_popname.css({'left':left, 'top':top}).show();
		_popname.show();
		
		// _popname.find('h2.title').attr('tabindex', '0').fadeIn().focus();
				
		$('.backdrop').addClass('active');
		var w_height = $(window).height();
		$('.wrap').css({ height: w_height, overflow: 'hidden'});
	};
		
	var popup_hide = function (elem) {
		var _popname = $('[data-popup-name="' + elem + '"]');
		_popname.hide();
		
		$('.wrap').css({ height:'auto', overflow: 'auto'});
				
		if (!$('.insurance-cal').hasClass('active')) {
			$('.backdrop').removeClass('active');
		}
		
		$(_target).fadeIn().focus();
		// console.log('alert_popup_hide : ', _target);
	};

	return {
		show: function (elem) {
		   return popup_show(elem);
		},
		hide : function (elem) {
			return popup_hide(elem);
		}
	}
}(jQuery));



/**
 * @name: naoTooltips.js
 * @author: Noemi Losada <info@noemilosada.com>
 * @date: 09/07/2016
 * Creative Commons License <http://creativecommons.org/licenses/by-sa/3.0/>
 */
(function($) {

    var defaults = { speed: 0, delay: 0}; // speed: 200 / 400 / 600 / 'slow' / 'fast'
    var config = { tooltip: 'naoTooltip', arrowSize: 16};

    /**
     * $('.naoTooltip-wrap').naoTooltip({ speed: 200 });
     */
    $.fn.naoTooltip = function () {
        init.apply(this, arguments);
        return this;
    };

    function init(opts) {
        var options = $.extend({}, defaults, opts);
		
        this.data(options);
        this.each(function(i, e) {
            animateNaoTooltip($(e), options);
        });
    }

    function animateNaoTooltip(selector, opts) {
        var tooltip = selector.find('.' + config.tooltip),
            delayHappened = false,
            timer;

        setLeftOffset(selector, tooltip);
        setTopOffset(selector, tooltip);
		
        $(window).resize(function() {
            setLeftOffset(selector, tooltip);
            setTopOffset(selector, tooltip);
        });
		
		selector.on('mouseenter', function () {
			if (delayHappened === false) {
				timer = setTimeout(function () {
					delayHappened = true;
					showTooltip(tooltip, opts.speed);
				}, opts.delay);
			}
		}).on('mouseleave', function () {
			clearTimeout(timer);
			delayHappened = false;
			hideTooltip(tooltip, opts.speed);
		});

		selector.find('.close').on('mouseenter', function (e) {
			e.preventDefault();
			clearTimeout(timer);
			delayHappened = false;
			hideTooltip(tooltip, opts.speed);
		});
    }

    function setLeftOffset (selector, tooltip) {
        var leftOffset = 'auto';

        if (tooltip.hasClass('nt-right-top') || tooltip.hasClass('nt-right') || tooltip.hasClass('nt-right-bottom')) {
            leftOffset = selector.outerWidth() + config.arrowSize;
        }

        if (tooltip.hasClass('nt-left-top') || tooltip.hasClass('nt-left') || tooltip.hasClass('nt-left-bottom')) {
            leftOffset = '-' + (tooltip.outerWidth() + config.arrowSize);
        }

        if (tooltip.hasClass('nt-top') || tooltip.hasClass('nt-bottom')) {
            leftOffset = (selector.outerWidth() / 2) - (tooltip.outerWidth() / 2);
        }

        if (tooltip.hasClass('nt-top-right') || tooltip.hasClass('nt-bottom-right')) {
            leftOffset = selector.outerWidth() - tooltip.outerWidth();
        }

        if (tooltip.hasClass('nt-top-left') || tooltip.hasClass('nt-bottom-left')) {
            leftOffset = 0;
        }

        tooltip.css({ left: leftOffset});
    }

    function setTopOffset (selector, tooltip) {
        var topOffset = 'auto';

        if (tooltip.hasClass('nt-top-left') || tooltip.hasClass('nt-top') || tooltip.hasClass('nt-top-right')) {
            topOffset = '-' + (selector.outerHeight() + tooltip.outerHeight());
        }

        if (tooltip.hasClass('nt-bottom-left') || tooltip.hasClass('nt-bottom') || tooltip.hasClass('nt-bottom-right')) {
            topOffset = selector.outerHeight() + config.arrowSize;
        }

        if (tooltip.hasClass('nt-right') || tooltip.hasClass('nt-left')) {
            topOffset = (selector.outerHeight() / 2) - (tooltip.outerHeight() / 2);
        }

        if (tooltip.hasClass('nt-left-bottom') || tooltip.hasClass('nt-right-bottom')) {
            topOffset = selector.outerHeight() - tooltip.outerHeight();
        }

        if (tooltip.hasClass('nt-left-top') || tooltip.hasClass('nt-right-top')) {
            topOffset = 0;
        }

        tooltip.css({ top: topOffset});
    }

    function showTooltip (tooltip, speed) {
        tooltip.css({ visibility: 'visible' }).animate({ opacity: 1}, speed);
    }

    function hideTooltip (tooltip, speed) {
        tooltip.animate({ opacity: 0}, speed, function() {
            tooltip.css({ visibility: 'hidden' });
        });
    }
	
})(jQuery);

$(function() {
    $('.naoTooltip-wrap').naoTooltip();
});


/*
 * 청약 진행 step
 */
function stepEventPrev (no) {
	$.smoothScroll({ easing: 'easeOutExpo', speed: 0, offset:0});
	
	$('.step-box-' + no + 'et').show().find('.tit').addClass('active');	
	$('.step-box-' + (+no + 1) + 'et').hide().prev('.tit').removeClass('active');
}

function stepEventNext (no) {
	$.smoothScroll({ easing: 'easeOutExpo', speed: 0, offset:0});
	
	$('.step-box-' + no + 'et').show().find('.tit').addClass('active');	
	$('.step-box-' + (+no - 1) + 'et').hide().prev('.tit').removeClass('active');
}




var cancer_cont_banner;

function abl_toggle () {
	// console.log('abl_toggle');

	setTimeout(function () {
		/*
		 * 패키지 설계 부분 swiper
		 */
		cancer_cont_banner = new Swiper('.cont-swiper-box .swiper-container', {
			autoHeight: true,
			spaceBetween: 30,
			effect: 'coverflow',
			grabCursor: true,
			centeredSlides: true,
			slidesPerView: 'auto',
			coverflow: {
				rotate: 0,
				stretch: 0,
				depth: 80,
				modifier: 1,
				slideShadows : true
			},
			autoHeight: true,
			onTransitionStart: function (swiper) {
				// console.log('abl_toggle onTransitionStart');
				
				$('.init-view li a').removeClass('active');
				$('.init-view li').eq(swiper.realIndex).find('a').addClass('active');
			}
		});
		
		// console.log('abl_toggle setTimeout 3');
		
		var $info_tab = $('.info-view');
		$('.content-info-view').each(function (i) {
			var $el = $(this),
				animClassDown = $el.data('animateDown'),
				animClassUp = $el.data('animateUp');

			$el.waypoint(function(direction) {
				
				// console.log('waypoint direction 2 : ', direction);
				
				if( direction === 'down' && animClassDown ) {
					$info_tab.attr('class', 'tabMenu-control info-view ' + animClassDown);
				} else if( direction === 'up' && animClassUp ){
					$info_tab.attr('class', 'tabMenu-control info-view ' + animClassUp);
				}
			}, {
				offset: '70px'
			});
		});
		
		$('.info-view').removeClass('bin-view-subshow');
		
	}, 1000);

	
	/*
	 * 내 보험료 계산하기
	 */
	$('.insurance-cal').removeClass('active');
	$('.backdrop').removeClass('active');
	
	
	/*
	 * 설계 탭메뉴
	 */
	$('.init-view li a').on('click', function (e) {
		e.preventDefault();
		var _this = $(this),
			_data = _this.data('num');
		cancer_cont_banner.slideTo(_data);
		
		$('.init-view li a').attr('title', '');
		$(this).attr('title', '선택');
	});
	
	
	$.smoothScroll({ easing: 'easeOutExpo', speed: 500, offset:340});
	$('.naoTooltip-wrap').naoTooltip();
	
	/*
	 * 탭메뉴 초기화
	 */
	// console.log('naoTooltip : abl_toggle 20170701');
}

function insurance_open () {
	$('.insurance-cal').attr('tabindex', '0').fadeIn().focus();
	$('.insurance-cal').addClass('active');
	$('.backdrop').addClass('active');
}




function showGlobalLoaing22 () {
	
	var str = '';
		str += '<div class="global_loading">';
		str += '    <div class="global_loading_wrap">';
		str += '        <div class="logo"></div>';
		str += '        <div class="progress"><span class="bar"></span></div>';
		str += '        <p class="msg">잠시만 기다려 주시기 바랍니다.</p>';
		str += '    </div>';
		str += '</div>';
	var $obj = $(str);
	$('body').append($obj);
	
	// 진행바 모션
	$bar = $obj.find('.bar').animate({width: '85%'}, 1000).animate({width: '90%'}, {
		complete: function () {
			showGlobalLoaing2();
		}
	});

	// 로고 모션
	// var $logo = $obj.find('.logo');
	// var logoWidth = 130;
	// var logoPos = 0;
	// var loop = function loop() {
	// 	logoPos = logoPos - logoWidth;
	// 	if(logoPos < -260) { logoPos = 0; }
	// 	$logo.css('background-position', logoPos + 'px 0');
		// setTimeout(loop, 500);		
	// }();
}


function showGlobalLoaing2() {
	 alert(3);
}

// 로딩 표시
function showGlobalLoaing() {
	// console.log('showGlobalLoaing');	
}

// 로딩 제거
function hideGlobalLoaing() {
	$('.global_loading').remove();
}

function mySwiperBox () {
	// console.log('mySwiperBox');
	
	setTimeout(function () {
		$('.my-swiper-box .swiper-container').show();
		var my_banner = new Swiper('.my-swiper-box .swiper-container', {
			autoHeight: true,
			spaceBetween: 30,
			effect: 'coverflow',
			grabCursor: true,
			centeredSlides: true,
			slidesPerView: 'auto',
			coverflow: {
				rotate: 0,
				stretch: 0,
				depth: 80,
				modifier: 1,
				slideShadows : true
			},
			pagination: '.my-swiper-box .swiper-pagination',
			paginationType: 'fraction'
		});				
	}, 1000);	
}


function mySwiperBox2 () {
	// console.log('mySwiperBox2');
	
	setTimeout(function () {
		$('.my-swiper-box2 .swiper-container').show();
		var my_banner2 = new Swiper('.my-swiper-box2 .swiper-container', {
			autoHeight: true,
			spaceBetween: 30,
			effect: 'coverflow',
			grabCursor: true,
			centeredSlides: true,
			slidesPerView: 'auto',
			coverflow: {
				rotate: 0,
				stretch: 0,
				depth: 80,
				modifier: 1,
				slideShadows : true
			},
			pagination: '.my-swiper-box2 .swiper-pagination',
			paginationType: 'fraction'
		});		
	}, 1000);		
}



var progressGlobalBar;
var progressLogoWidth = 130;
var progressLogoPos = 0;
function progressLoaing (callback) {
	
	$('#globalLoadingDiv').show();
	var $obj = $('#globalLoadingDiv');
	
	$bar = $obj.find('.bar').animate({width: '85%'}, 1000).animate({width: '90%'}, {
		complete: function () {
			
			// 진행바 모션
			progressGlobalBar=setInterval('progressTimer()',500);
			
			if (typeof callback == 'function') {
				callback();
			
			} else {
				if( callback ) {
					if( callback.indexOf("(") == -1 ) eval( callback +"()");
					else eval( callback );
				} else {
					return;
				}
			}
			
		}
	});
}

function progressTimer() {
	
	// 로고 모션
	progressLogoPos = progressLogoPos - progressLogoWidth;
	if(progressLogoPos < -260) { progressLogoPos = 0; }
	$('#globalLoadingDiv').find('.logo').css('background-position', progressLogoPos + 'px 0');
}

function progressHidden() {
	clearInterval(progressGlobalBar);
	$bar = $('#globalLoadingDiv').find('.bar').animate({width: '10%'});
	$('#globalLoadingDiv').hide();
};








