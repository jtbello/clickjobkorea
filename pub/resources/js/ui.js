/* 달력 한글 셋팅 */
$.datepicker.setDefaults({
	dateFormat: 'yy-mm-dd',
	prevText: '이전 달',
	nextText: '다음 달',
	monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
	dayNames: ['일', '월', '화', '수', '목', '금', '토'],
	dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
	dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
	showMonthAfterYear: true,
	yearSuffix: '년'
});

/* 달력 실행 */
$(".dateSelect").datepicker({
});

/* 옵션 더 보기 */
$(".viewAll").on("click", function(){
	if($(".optionAdd").hasClass("view") === false) {
		$(".optionAdd").addClass("view");
	}else{
		$(".optionAdd").removeClass("view");
	};
});

/* 팝업 닫기 */
$(".closePop").on("click", function(){
	$(".callPop").css("display", "none");
	$("body").css("overflow", "auto"); //이 값이 팝업닫을 시 실행되야 스크롤이 생김
});

/* 팝업 열기 */
$(".popCall").on("click", function(){
	$( 'html, body' ).animate( { scrollTop : 0 }, 0 );
	$(".callPop").css("display", "block");
	$("body").css("overflow", "hidden"); //이 값이 팝업열때 실행되야 여백스크롤이 안생김 
});

/* 메뉴 뎁스 보기 */
$(".bot-menu dt a").on("click", function(){
	$(this).parent().parent().children("dd").slideToggle();
});

/* 메뉴 열기 닫기 */
$(".full-menu button").on("click", function(){
	$(".sidenav-content").addClass("active");
});

$(".sidenav-box .header-top .close-btn").on("click", function(){
	$(".sidenav-content").removeClass("active");
});



/* 탭버튼01 */
$(".tabBtn .view01").on("click", function(){
	$(".tabBtn a").removeClass("active");
	$(this).addClass("active");
	$(".tab02").css("display", "none");
	$(".tab03").css("display", "none");
	$(".tab04").css("display", "none");
	$(".tab01").css("display", "block");
});

$(".tabBtn .view02").on("click", function(){
	$(".tabBtn a").removeClass("active");
	$(this).addClass("active");
	$(".tab01").css("display", "none");
	$(".tab03").css("display", "none");
	$(".tab04").css("display", "none");
	$(".tab02").css("display", "block");
});

$(".tabBtn .view03").on("click", function(){
	$(".tabBtn a").removeClass("active");
	$(this).addClass("active");
	$(".tab01").css("display", "none");
	$(".tab02").css("display", "none");
	$(".tab04").css("display", "none");
	$(".tab03").css("display", "block");
});

$(".tabBtn .view04").on("click", function(){
	$(".tabBtn a").removeClass("active");
	$(this).addClass("active");
	$(".tab01").css("display", "none");
	$(".tab02").css("display", "none");
	$(".tab03").css("display", "none");
	$(".tab04").css("display", "block");
});

/* 파일 업로드 시 이름바꿔주는 스크립트, 작동이 안되네??? */
var fileTarget = $('.filebox .upload-hidden');

fileTarget.on('change', function(){
	if(window.FileReader){
		var filename = $(this)[0].files[0].name;
	} else {
		var filename = $(this).val().split('/').pop().split('\\').pop();
	}

	$(this).siblings('.upload-name').text(filename);
});

/* 동의 팝업 */
$(".popCall2").on("click", function(){
	$( 'html, body' ).animate( { scrollTop : 0 }, 0 );
	$(".infoAcceptPop").css("display", "block");
	$("body").css("overflow", "hidden"); //이 값이 팝업열때 실행되야 여백스크롤이 안생김 
});

/* 위 동의 팝업이 닫힐 시 $("body").css("overflow", "auto"); //이 값이 팝업닫을 시 실행되야 스크롤이 생김 */

/* 알림설정 버튼 on, off */
/* gps */
$(".gpsAccept .btnStyle02").on("click", function(){
	$(".gpsAccept input").click();
	$(this).removeClass("active");
	$(".gpsAccept .btnStyle01").addClass("active");
});

$(".gpsAccept .btnStyle01").on("click", function(){
	$(".gpsAccept input").click();
	$(this).removeClass("active");
	$(".gpsAccept .btnStyle02").addClass("active");
});

/* push */
$(".pushAccept .btnStyle02").on("click", function(){
	$(".pushAccept input").click();
	$(this).removeClass("active");
	$(".pushAccept .btnStyle01").addClass("active");
});

$(".pushAccept .btnStyle01").on("click", function(){
	$(".pushAccept input").click();
	$(this).removeClass("active");
	$(".pushAccept .btnStyle02").addClass("active");
});


/* 이미지 슬라이더 실행 */
$(function(){
	$('.bxslider').bxSlider({
		auto: false,
		pager: false,
		slideWidth: 800,
		pagerType:'full',
		buildPager:''
	});
});


