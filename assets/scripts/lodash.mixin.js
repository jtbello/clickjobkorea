(function() {

  _.mixin(_.assign({
    numeric: numeric,
    deepFreeze: deepFreeze,
    compactObject: compactObject,
    unflatten: unflatten,
    guid: guid
  }, s.exports()));

  function numeric(input) {
    if (!input) {
      return null;
    }
    return input.replace(/\D/g, '');
  }

  function deepFreeze(o) {
    Object.freeze(o);

    var oIsFunction = typeof o === 'function';
    var hasOwnProp = Object.prototype.hasOwnProperty;

    Object.getOwnPropertyNames(o).forEach(function(prop) {
      if (hasOwnProp.call(o, prop) && (oIsFunction ? prop !== 'caller' && prop !== 'callee' && prop !== 'arguments' : true) && o[prop] !== null && (typeof o[prop] === 'object' || typeof o[prop] === 'function') && !Object.isFrozen(o[prop])) {
        deepFreeze(o[prop]);
      }
    });

    return o;
  }

  function compactObject(object) {
    _.map(object, function(value, key, object) {
      if (!!!value || (_.isString(value) && _.trim(value).length === 0)) {
        delete object[key];
      }
    });
    return object;
  }
  
  function unflatten(list, rootIds, idKey, parentIdKey, childrenKey) {
    var data = _.cloneDeep(list), result;
    var composed = _.groupBy(data, parentIdKey);
    _.each(composed, function(children, parentId) {
      if (_.isArray(rootIds) && _.include(rootIds, parentId)) {
        return;
      }
      if (_.isString(rootIds) && rootIds === parentId) {
        return;
      }
      var query = {}, parent;
      query[idKey] = parentId;
      parent = _.find(data, query);
      if (!parent) {
        debugger;
      }
      parent[childrenKey] = children;
    });
    if (_.isArray(rootIds)) {
      result = [];
      _.each(rootIds, function(id) {
        var root = composed[id];
        if (!root) return;
        result.push(composed[id]);
      });
    } else {
      result = composed[rootIds];
    }
    return result;
  }
  
  function guid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0;
      var v = c === 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }
})();