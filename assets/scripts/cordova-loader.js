/* jshint browser:true */

(function() {

  var target, platform;
  if(navigator.userAgent.match(/Android/)) {
    target = platform = 'android';
  } else if(navigator.userAgent.match(/(iPhone|iPod|iPad)/)) {
    target = platform = 'ios';
  } else {
    platform = 'browser';
  }

  // TEST CODE
  // platform = 'browser';
  // TEST CODE

  window.env = {
    // cordova: /^file:/.test(document.URL) && !!target
      cordova: !!platform,
      platform : platform
  };

  if (window.env.cordova) {
    document.writeln('<script type="text/javascript" src="../'+platform+'/cordova.js"></script>');
  }

  $(document).on('hidekeyboard', function(e) {
    $('#fixLayout').height(1);
  });
  $(document).on('showkeyboard', function(e) {
    setTimeout(function() {
      $('#fixLayout').height(0);
    }, 100);
 });
})();