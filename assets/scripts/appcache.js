/* jshint browser:true */

(function() {
  var appCache = window.applicationCache;

  // addEvents();

  // appCache.update();

  function addEvents() {
    var events = ['cached', 'checking', 'downloading', 'error', 'noupdate',
    'obsolete', 'progress', 'updateready'];
    events.forEach(function(type) {
      var handler = handleCacheEvent;
      if (/error/.test(type)) {
        handler = handleCacheError;
      }
      appCache.addEventListener(type, handler, false);
    });
  }

  function handleCacheEvent(e) {
    console.debug(e);
    if (e.type === 'updateready' && appCache.status === appCache.UPDATEREADY) {
      appCache.swapCache();
      if (window.confirm('A new version of this site is available. Load it?')) {
        window.location.reload();
      }
    }
  }

  function handleCacheError(e) {
    console.error('Error: Cache failed to update!');
  }
})();