cordova.define("clickjobkorea-plugin-wakeup-alarm.WakeupAlarm", function(require, exports, module) {

    var exec = require("cordova/exec");

    /**
     * This is a global variable called wakeup exposed by cordova
     */
    var WakeupAlarm = function(){};

    WakeupAlarm.prototype.wakeup = function(success, error, options) {
        exec(success, error, "WakeupAlarmPlugin", "wakeup", [options]);
    };

    WakeupAlarm.prototype.getAlarm = function(success, error, options) {
            exec(success, error, "WakeupAlarmPlugin", "getAlarm", [options]);
    };

    WakeupAlarm.prototype.removeAlarm = function(success, error, options) {
            exec(success, error, "WakeupAlarmPlugin", "removeAlarm", [options]);
    };

    WakeupAlarm.prototype.snooze = function(success, error, options) {
        exec(success, error, "WakeupAlarmPlugin", "snooze", [options]);
    };

    module.exports = new WakeupAlarm();

});